package pe.gob.minedu.escale.eol.domain;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import pe.gob.minedu.escale.eol.enums.admin.EstadoActividadEnum;


/**
 * 
 * @author WARODRIGUEZ
 *
 */

@XmlRootElement(name = "actividad")
public class EolActividadConverter {
	private Integer id;
	private String nombre;
	private String descripcion;
	private Date fechaInicio;
	private Date fechaTermino;
	private Date fechaTerminosigied;
	private Date fechaTerminohuelga;
	private Date fechaLimite;
	private Date fechaLimitesigied;
	private Date fechaLimitehuelga;
	private Integer estadoActividad;
	private Integer estadoActividadsigied;
	private Integer estadoActividadhuelga;
	private boolean estadoFormato;

	private boolean estadoConstancia;
	private boolean estadoCobertura;
	private boolean estadoSituacion;
	private boolean estadoOmisos;
	private boolean estadoSie;

	private String urlFormato;
	private String urlOmisos;
	private String urlCobertura;
	private String urlConstancia;
	private String urlSituacion;

	private String strFechaEnvio;
	private String strFechaInicio;
	private String strFechaTermino;
	private String strFechaTerminosigied;
	private String strFechaTerminohuelga;
	private String strFechaLimite;
	private String strFechaLimitesigied;
	private String strFechaLimitehuelga;

	private String nombrePeriodo;

	public EolActividadConverter() {
	}

	public EolActividadConverter(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public Date getFechaTerminosigied() {
		return fechaTerminosigied;
	}

	public void setFechaTerminosigied(Date fechaTerminosigied) {
		this.fechaTerminosigied = fechaTerminosigied;
	}

	public Date getFechaTerminohuelga() {
		return fechaTerminohuelga;
	}

	public void setFechaTerminohuelga(Date fechaTerminohuelga) {
		this.fechaTerminohuelga = fechaTerminohuelga;
	}

	public Date getFechaLimite() {
		return fechaLimite;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public boolean getEstadoFormato() {
		return isEstadoFormato();
	}

	public Integer getEstadoActividad() {
		Date ahora = new Date();
		if (ahora.before(getFechaInicio())) {
			this.estadoActividad = EstadoActividadEnum.SIN_INICIAR.getCod();
		}
		if (ahora.after(getFechaLimite())) {
			this.estadoActividad = EstadoActividadEnum.FINALIZADO.getCod();
		}
		if (ahora.after(getFechaTermino()) && (ahora.before(getFechaLimite()) || ahora.equals(getFechaLimite()))) {
			this.estadoActividad = EstadoActividadEnum.EXTENPORANEO.getCod();
		}
		if (ahora.after(getFechaInicio()) && ahora.before(getFechaTermino())) {
			this.estadoActividad = EstadoActividadEnum.INICIADO.getCod();
		}
		return this.estadoActividad;
	}

	public Integer getEstadoActividadsigied() {
		Date ahora = new Date();
		if (ahora.before(getFechaInicio())) {
			this.estadoActividadsigied = EstadoActividadEnum.SIN_INICIAR.getCod();
		}
		if (ahora.after(getFechaLimitesigied())) {
			this.estadoActividadsigied = EstadoActividadEnum.FINALIZADO.getCod();
		}
		if (ahora.after(getFechaTerminosigied())
				&& (ahora.before(getFechaLimitesigied()) || ahora.equals(getFechaLimitesigied()))) {
			this.estadoActividadsigied = EstadoActividadEnum.EXTENPORANEO.getCod();
		}
		if (ahora.after(getFechaInicio()) && ahora.before(getFechaTerminosigied())) {
			this.estadoActividadsigied = EstadoActividadEnum.INICIADO.getCod();
		}
		return this.estadoActividadsigied;
	}

	public void setEstadoActividadsigied(Integer estadoActividadsigied) {
		this.estadoActividadsigied = estadoActividadsigied;
	}

	public Integer getEstadoActividadhuelga() {

		Date ahora = new Date();
		if (ahora.before(getFechaInicio())) {
			this.estadoActividadhuelga = EstadoActividadEnum.SIN_INICIAR.getCod();
		}
		if (ahora.after(getFechaLimitehuelga())) {
			this.estadoActividadhuelga = EstadoActividadEnum.FINALIZADO.getCod();
		}
		if (ahora.after(getFechaTerminohuelga())
				&& (ahora.before(getFechaLimitehuelga()) || ahora.equals(getFechaLimitehuelga()))) {
			this.estadoActividadhuelga = EstadoActividadEnum.EXTENPORANEO.getCod();
		}
		if (ahora.after(getFechaInicio()) && ahora.before(getFechaTerminohuelga())) {
			this.estadoActividadhuelga = EstadoActividadEnum.INICIADO.getCod();
		}
		return this.estadoActividadhuelga;
	}

	public void setEstadoActividadhuelga(Integer estadoActividadhuelga) {
		this.estadoActividadhuelga = estadoActividadhuelga;
	}

	public String getUrlFormato() {
		return urlFormato;
	}

	public String getUrlConstancia() {
		return urlConstancia;
	}

	public String getUrlCobertura() {
		return urlCobertura;
	}

	public String getUrlSituacion() {
		return urlSituacion;
	}

	public String getUrlOmisos() {
		return urlOmisos;
	}

	public void setEstadoActividad(Integer estadoActividad) {
		this.estadoActividad = estadoActividad;
	}

	public boolean isEstadoFormato() {
		return estadoFormato;
	}

	public void setEstadoFormato(boolean estadoFormato) {
		this.estadoFormato = estadoFormato;
	}

	public void setUrlFormato(String urlFormato) {
		this.urlFormato = urlFormato;
	}

	public void setUrlOmisos(String urlOmisos) {
		this.urlOmisos = urlOmisos;
	}

	public void setUrlCobertura(String urlCobertura) {
		this.urlCobertura = urlCobertura;
	}

	public void setUrlConstancia(String urlConstancia) {
		this.urlConstancia = urlConstancia;
	}

	public void setUrlSituacion(String urlSituacion) {
		this.urlSituacion = urlSituacion;
	}

	public boolean isEstadoConstancia() {
		return estadoConstancia;
	}

	public void setEstadoConstancia(boolean estadoConstancia) {
		this.estadoConstancia = estadoConstancia;
	}

	public boolean isEstadoCobertura() {
		return estadoCobertura;
	}

	public void setEstadoCobertura(boolean estadoCobertura) {
		this.estadoCobertura = estadoCobertura;
	}

	public boolean isEstadoSituacion() {
		return estadoSituacion;
	}

	public void setEstadoSituacion(boolean estadoSituacion) {
		this.estadoSituacion = estadoSituacion;
	}

	public boolean isEstadoOmisos() {
		return estadoOmisos;
	}

	public void setEstadoOmisos(boolean estadoOmisos) {
		this.estadoOmisos = estadoOmisos;
	}

	/**
	 * @return the strFechaEnvio
	 */
	public String getStrFechaEnvio() {
		return strFechaEnvio;
	}

	/**
	 * @param strFechaEnvio
	 *            the strFechaEnvio to set
	 */
	public void setStrFechaEnvio(String strFechaEnvio) {
		this.strFechaEnvio = strFechaEnvio;
	}

	/**
	 * @return the strFechaInicio
	 */
	public String getStrFechaInicio() {
		return strFechaInicio;
	}

	/**
	 * @param strFechaInicio
	 *            the strFechaInicio to set
	 */
	public void setStrFechaInicio(String strFechaInicio) {
		this.strFechaInicio = strFechaInicio;
	}

	/**
	 * @return the strFechaTermino
	 */
	public String getStrFechaTermino() {
		return strFechaTermino;
	}

	/**
	 * @param strFechaTermino
	 *            the strFechaTermino to set
	 */
	public void setStrFechaTermino(String strFechaTermino) {
		this.strFechaTermino = strFechaTermino;
	}

	public String getStrFechaTerminosigied() {
		return strFechaTerminosigied;
	}

	public void setStrFechaTerminosigied(String strFechaTerminosigied) {
		this.strFechaTerminosigied = strFechaTerminosigied;
	}

	public String getStrFechaTerminohuelga() {
		return strFechaTerminohuelga;
	}

	public void setStrFechaTerminohuelga(String strFechaTerminohuelga) {
		this.strFechaTerminohuelga = strFechaTerminohuelga;
	}

	/**
	 * @return the strFechaLimite
	 */
	public String getStrFechaLimite() {
		return strFechaLimite;
	}

	/**
	 * @param strFechaLimite
	 *            the strFechaLimite to set
	 */
	public void setStrFechaLimite(String strFechaLimite) {
		this.strFechaLimite = strFechaLimite;
	}

	public String getNombrePeriodo() {
		return nombrePeriodo;
	}

	public void setNombrePeriodo(String nombrePeriodo) {
		this.nombrePeriodo = nombrePeriodo;
	}

	public boolean isEstadoSie() {
		return estadoSie;
	}

	public void setEstadoSie(boolean estadoSie) {
		this.estadoSie = estadoSie;
	}

	public Date getFechaLimitehuelga() {
		return fechaLimitehuelga;
	}

	public void setFechaLimitehuelga(Date fechaLimitehuelga) {
		this.fechaLimitehuelga = fechaLimitehuelga;
	}

	public Date getFechaLimitesigied() {
		return fechaLimitesigied;
	}

	public void setFechaLimitesigied(Date fechaLimitesigied) {
		this.fechaLimitesigied = fechaLimitesigied;
	}

	public String getStrFechaLimitehuelga() {
		return strFechaLimitehuelga;
	}

	public void setStrFechaLimitehuelga(String strFechaLimitehuelga) {
		this.strFechaLimitehuelga = strFechaLimitehuelga;
	}

	public String getStrFechaLimitesigied() {
		return strFechaLimitesigied;
	}

	public void setStrFechaLimitesigied(String strFechaLimitesigied) {
		this.strFechaLimitesigied = strFechaLimitesigied;
	}

}
