package pe.gob.minedu.escale.eol.rest.censo2019;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import pe.gob.minedu.escale.eol.converter.RespuestaConverter;
import pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.eol.domain.PadronClient;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Fila;
import pe.gob.minedu.escale.eol.ejb.censo2019.Resultado2019Facade;

@Path("/censo2019/resultado")
@Stateless
public class CedulaResultado2010Service {
	private static final Logger LOGGER = Logger.getLogger(CedulaResultado2010Service.class.getName());
	
	@EJB
	private Resultado2019Facade facade;
	
	@EJB
	private CedulaResultado2019ValidResources facadeValid;
	
	 /**
     * Registra una cédula de resultado vía REST
     * @param cedula La cédula de resultado
     * @return un estado de respuesta según sea la situación de la recepción
     */
    @POST
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    public Response registrar(Resultado2019Cabecera cedula) {

        RespuestaConverter rpta = new RespuestaConverter();
        String msg = "";

        if (cedula.getToken() == 0 || cedula.getToken() == -1L) {
            return Response.status(Response.Status.UNAUTHORIZED).build(); //401 -- No tiene autorización
        }
        PadronClient padronClient = new PadronClient(cedula.getCodMod(), cedula.getAnexo());

//        InstitucionEducativaIE ie = null;
        InstitucionEducativaIE ie = new InstitucionEducativaIE();
        try {
            //ie = padronClient.get_JSON(InstitucionEducativaIE.class);
        	//ie = padronClient.getClass();
            
            if (ie == null) {
                msg=String.format("No se encontró ninguna Institución Educativa con código modular %1s y anexo %2s  .", cedula.getCodMod(), cedula.getAnexo());
                rpta.setMensaje(msg);
                rpta.setStatus("ERROR");
                return Response.ok(rpta).build();
            }

            if(!ie.getMcenso().equals("1")){
                msg = "La Institución Educativa no se encuentra incluida en el marco censal, para mayor información consulte con el estadístico de su UGEL.";
                rpta.setMensaje(msg);
                rpta.setStatus("ERROR");
                return Response.ok(rpta).build();
            }

        } catch (Exception e) {
            msg="Esta cédula presenta un error interno en el servicio de busqueda del padron de IIEE."
                    + "\nPor favor vuelva a intentar más tarde."
                    + "\nDisculpe las molestias ocasionadas. ";
            rpta.setMensaje(msg);
            rpta.setStatus("ERROR");
            return Response.ok(rpta).build();
        }

        cedula.setValido(false);
        rpta=facadeValid.validar(cedula, ie);
        cedula.setCodooii(ie.getIgel().getIdIgel());
        cedula.setCodgeo(ie.getDistrito().getIdDistrito());

        /*VALIDACION DE LA CONSISTENCIA MATRICULA*/
        String msgobs ="" ;
        List<Object[]> listObj = null;
        if(!cedula.getVersion().equals("9") && (cedula.getNroced().equals("c03bp") || cedula.getNroced().equals("c03bs"))){

            List<Resultado2019Fila> filas = cedula.getDetalle().get("C101").getFilas();

            for (Resultado2019Fila rsfil : filas) {
                if(rsfil.getTipdato().equals("00")){
                    listObj = facade.verificarObservacionCedula(cedula.getCodMod(), cedula.getAnexo(), cedula.getNivMod(), rsfil.getTotalH(), rsfil.getTotalM(), rsfil.getDato01h(), rsfil.getDato01m(), rsfil.getDato02h(), rsfil.getDato02m(), rsfil.getDato03h(), rsfil.getDato03m(), rsfil.getDato04h(), rsfil.getDato04m(), rsfil.getDato05h(), rsfil.getDato05m(), rsfil.getDato06h(), rsfil.getDato06m(), rsfil.getDato07h(), rsfil.getDato07m(), rsfil.getDato08h(), rsfil.getDato08m(), rsfil.getDato09h(), rsfil.getDato09m(), rsfil.getDato10h(), rsfil.getDato10m());
                    break;
                }
            }

            if(listObj != null){
                for (int i = 0; i < listObj.size(); i++) {
                    Integer flag = Integer.parseInt(listObj.get(i)[0].toString());
                    if(flag.intValue() == 1){
                        cedula.setValido(true);

                        msgobs = "¡Los datos se grabaron con éxito!\n\n"
                                + "Existe una variación significativa entre los datos reportados al inicio del año y los resultados del ejercicio educativo.\n\n"
                                + "Imprima su constancia de envío y verifique si la información declarada es correcta.";
//                                + "Los datos reportados fueron grabados con una variación significativa respecto a la información registrada al inicio del año escolar.\n"
//                                + "Matrícula al inicio "+ listObj.get(i)[1].toString() +" alumnos, al finalizar  "+listObj.get(i)[2].toString()+" alumnos. \n\n"
//                                + "Verifique sus datos registrados en la constancia de envío o pongase en contacto con el estadístico de su jurisdicción.";

                    }
                }
            }
        }

        //validacion de variable
        cedula.setVarrec("0");
        if(cedula.getNroced().equals("c03bp") || cedula.getNroced().equals("c03bs")
                || cedula.getNroced().equals("c4bi") || cedula.getNroced().equals("c4ba")){
                Set<String> keys = cedula.getDetalle().keySet();

                for (Iterator<String> it = keys.iterator(); it.hasNext();) {
                    String key = it.next();

                    if(key.equals("C101") || key.equals("P101")){

                        List<Resultado2019Fila> filas = cedula.getDetalle().get(key).getFilas();

                        for (Resultado2019Fila f : filas) {
                            if(f.getTipdato().equals("02") && (f.getTotalH() > 0 || f.getTotalM() > 0 )){
                                cedula.setVarrec("1");
                            }
                        }

                    }

                }
        }

        if(rpta.getMensaje().isEmpty())
        {  facade.create(cedula);
           if(cedula.getValido()){
               rpta.setMensaje(msgobs);
               rpta.setStatus("WARNING");
           }else{
               rpta.setMensaje("Su mensaje se envio correctamente, por favor verifique sus datos imprimiento su constancia desde la pagina http://escale.minedu.gob.pe");
               rpta.setStatus("OK");
           }

        }else{
            rpta.setStatus("ERROR");
        }

        //...registra...
        return Response.ok(rpta).build();//.. y termina
    }

    public Integer IntN(Integer num) {
        return num != null ? num : 0;
    }

    @GET
    @Produces({"application/json"})
    public Resultado2019Cabecera getCedula(@QueryParam("idEnvio") String idEnvio) {
        Resultado2019Cabecera cedula = facade.find(new Long(idEnvio));
        return cedula;
    }

	
	
	
}
