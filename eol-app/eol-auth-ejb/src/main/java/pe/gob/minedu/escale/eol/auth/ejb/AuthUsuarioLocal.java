/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.ejb;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright (C) 2018
 * Ministerio de Educacion 
 * Unidad de Estadistica Educativa
 * (Lima - Peru)
 *  
 * PROYECTO EOL
 *
 * All rights reserved. Used by permission This software is the confidential and
 * proprietary information of MINEDU ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with MIMP.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */
/**
 * @objetivo: Contener la declaracion de los metodos necesarios para las Auth
 * @autor: Ing. Oscar Mateo
 * @fecha: 01/11/2018
 *
 *         ------------------------------------------------------------------------
 *         Modificaciones Fecha Nombre Descripción
 *         ------------------------------------------------------------------------
 *
 */
@Local
public interface AuthUsuarioLocal {

	void create(AuthUsuario authUsuario);

	void edit(AuthUsuario authUsuario);

	void remove(AuthUsuario authUsuario);

	AuthUsuario find(Object id);

	int getRecordCount(Map<String, Object> parameters);

	List<AuthUsuario> findByParams(Map<String, Object> parameters, String orderBy);

}
