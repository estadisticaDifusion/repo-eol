/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.ejb.facade;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import pe.gob.minedu.escale.eol.auth.domain.AuthUsuarioHistorial;
import pe.gob.minedu.escale.eol.auth.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.auth.ejb.AuthUsuarioHistorialLocal;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.utils.Funciones;

/**
 * 
 * @objetivo: 
 * @autor: Martin Huamani Mendoza
 * @fecha: 10/03/2019
 *
 */
@Stateless
public class AuthUsuarioHistorialFacade extends AbstractFacade<AuthUsuarioHistorial> implements AuthUsuarioHistorialLocal {

    private Logger logger = Logger.getLogger(this.getClass().getName());
    
    @PersistenceContext(unitName = "eol-PU-Auth")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuthUsuarioHistorialFacade() {
        super(AuthUsuarioHistorial.class);
    }
    
    @Override
	public int createUserHistory(PasswordEncoder passwordEncoder, AuthUsuarioHistorial authUsuarioHistorial) {
		int returnValidation = 0;
		if (isValidationPasswordHistory(authUsuarioHistorial, passwordEncoder)) {
			authUsuarioHistorial.setContrasenia(passwordEncoder.encode(authUsuarioHistorial.getContraseniaText()));
			super.create(authUsuarioHistorial);
		} else {
			returnValidation = 1;
		}
		return returnValidation;
	}
    
    private boolean isValidationPasswordHistory(final AuthUsuarioHistorial authUsuarioHistorialRq, final PasswordEncoder passwordEncoder ) {
    	List<AuthUsuarioHistorial> authUsuarioHistorialList = findAllByField("usuario", authUsuarioHistorialRq.getUsuario());
    	for (AuthUsuarioHistorial authUsuarioHistorial : authUsuarioHistorialList) {
    		if(passwordEncoder.matches(authUsuarioHistorialRq.getContrasenia(), authUsuarioHistorial.getContrasenia())) {
    			return false;
    		}
    	}
    	return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AuthUsuarioHistorial> findByParams(Map<String, Object> parameters, String orderBy) {
        logger.info(":: AuthUsuarioFacade.findByParams :: Starting execution...");

        StringBuilder hSql = new StringBuilder();
        hSql.append("select b ");
        hSql.append(buildSelectClause());

        String whereClause = buildWhereClause(parameters);
        if (!Funciones.esVacio(whereClause)) {
            whereClause = " where " + whereClause;
        }
        hSql.append(whereClause);

        String orderClause = buildOrderByClause(orderBy);
        if (!Funciones.esVacio(orderClause)) {
            orderClause = " order by " + orderClause;
        } else {
            orderClause = "";
        }
        hSql.append(orderClause);

        logger.info("   beneficencia HQL: " + hSql);

        Query q = getEntityManager().createQuery(hSql.toString());
        for (Map.Entry<String, Object> map : parameters.entrySet()) {
            q.setParameter(map.getKey(), map.getValue());
        }
        logger.info(":: BeneficenciaFacade.findByParams :: Execution finish.");
        return q.getResultList();
    }

    @Override
    public int getRecordCount(Map<String, Object> parameters) {
        logger.info(":: BeneficenciaFacade.getRecordCount :: Starting execution...");
        String selectClause = "select count(b.beneficenciaId) " + buildSelectClause();
        selectClause = selectClause.replaceAll("fetch ", CoreConstant.BLANCO);
        String whereClause = buildWhereClause(parameters);
        if (!Funciones.esVacio(whereClause)) {
            whereClause = " where " + whereClause;
        }

        String hql = selectClause + whereClause;

        logger.info("getRecordCount HQL: " + hql);

        Query q = getEntityManager().createQuery(hql);

        for (Map.Entry<String, Object> map : parameters.entrySet()) {
            q.setParameter(map.getKey(), map.getValue());
        }
        logger.info(":: BeneficenciaFacade.getRecordCount :: Execution finish.");
        return ((Long) q.getSingleResult()).intValue();
    }

    private String buildSelectClause() {
        StringBuilder selectClause = new StringBuilder();
        selectClause.append("from Beneficencia b ");
        selectClause.append("  left join fetch b.ambito a ");
        selectClause.append("  left join fetch b.condicion c ");
        selectClause.append("  left join fetch b.estado e  ");
        return selectClause.toString();
    }

    private String buildWhereClause(Map<String, Object> parameters) {
        StringBuilder whereClause = new StringBuilder();

        if (parameters.get("beneficenciaId") != null) {
            whereClause.append("b.beneficenciaId = :beneficenciaId");
        }
        if (parameters.get("numeroRuc") != null) {
            whereClause.append(!CoreConstant.VACIO.equals(whereClause.toString()) ? " " + CoreConstant.CONDITION_AND + " " : CoreConstant.VACIO);
            whereClause.append("b.numeroRuc = :numeroRuc");
        }
        if (parameters.get("usuarioRegistroId") != null) {
            whereClause.append(!CoreConstant.VACIO.equals(whereClause.toString()) ? " " + CoreConstant.CONDITION_AND + " " : CoreConstant.VACIO);
            whereClause.append("b.usuarioRegistroId = :usuarioRegistroId");
        }
        if (parameters.get("activo") != null) {
            whereClause.append(!CoreConstant.VACIO.equals(whereClause.toString()) ? " " + CoreConstant.CONDITION_AND + " " : CoreConstant.VACIO);
            whereClause.append("b.activo = :activo");
        }

        return whereClause.toString();
    }

    private String buildOrderByClause(String orderBy) {
        if (Funciones.esVacio(orderBy)) {
            return null;
        }

        String orderByClause = "";
        String[] orderByArray = orderBy.split(CoreConstant.SEPARATOR_COMA);
        for (String orderByElement : orderByArray) {
            if ("beneficenciaId".equals(orderByElement)) {
                orderByClause = (!"".equals(orderByClause) ? orderByClause + " " + CoreConstant.SEPARATOR_COMA + " "
                        : "");
                orderByClause = orderByClause + "b.beneficenciaId";
            }
            if ("razonSocial".equals(orderByElement)) {
                orderByClause = (!"".equals(orderByClause) ? orderByClause + " " + CoreConstant.SEPARATOR_COMA + " "
                        : "");
                orderByClause = orderByClause + "b.razonSocial";
            }
            if ("fechaRegistro".equals(orderByElement)) {
                orderByClause = (!"".equals(orderByClause) ? orderByClause + " " + CoreConstant.SEPARATOR_COMA + " "
                        : "");
                orderByClause = orderByClause + "b.fechaRegistro desc";
            }

        }

        return orderByClause;
    }

}
