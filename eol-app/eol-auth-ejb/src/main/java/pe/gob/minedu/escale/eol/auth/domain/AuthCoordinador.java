/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.auth.domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "auth_coordinador")
@NamedQueries({ @NamedQuery(name = "AuthCoordinador.findAll", query = "SELECT a FROM AuthCoordinador a"),
		@NamedQuery(name = "AuthCoordinador.findById", query = "SELECT a FROM AuthCoordinador a WHERE a.id = :id") })
public class AuthCoordinador implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "ID")
	private String id;
	@JoinTable(name = "auth_coordinador_auth_usuario", joinColumns = {
			@JoinColumn(name = "Coordinador_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
					@JoinColumn(name = "ugels_usuario", referencedColumnName = "usuario") })
	@ManyToMany
	private Collection<AuthUsuario> authUsuarioCollection;

	public AuthCoordinador() {
	}

	public AuthCoordinador(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Collection<AuthUsuario> getAuthUsuarioCollection() {
		return authUsuarioCollection;
	}

	public void setAuthUsuarioCollection(Collection<AuthUsuario> authUsuarioCollection) {
		this.authUsuarioCollection = authUsuarioCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof AuthCoordinador)) {
			return false;
		}
		AuthCoordinador other = (AuthCoordinador) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.domainAuth.AuthCoordinador[id=" + id + "]";
	}

}
