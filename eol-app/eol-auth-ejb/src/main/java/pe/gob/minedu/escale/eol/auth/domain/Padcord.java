/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.auth.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "padcord")
@NamedQueries({ @NamedQuery(name = "Padcord.findAll", query = "SELECT p FROM Padcord p"),
		@NamedQuery(name = "Padcord.findByUsuario", query = "SELECT p FROM Padcord p WHERE p.usuario = :usuario"),
		@NamedQuery(name = "Padcord.findByContrasenia", query = "SELECT p FROM Padcord p WHERE p.contrasenia = :contrasenia"),
		@NamedQuery(name = "Padcord.findByNombre", query = "SELECT p FROM Padcord p WHERE p.nombre = :nombre"),
		@NamedQuery(name = "Padcord.findByEmail", query = "SELECT p FROM Padcord p WHERE p.email = :email"),
		@NamedQuery(name = "Padcord.findByTipo", query = "SELECT p FROM Padcord p WHERE p.tipo = :tipo"),
		@NamedQuery(name = "Padcord.findByIdRegistrador", query = "SELECT p FROM Padcord p WHERE p.idRegistrador = :idRegistrador"),
		@NamedQuery(name = "Padcord.findByFechaRegistro", query = "SELECT p FROM Padcord p WHERE p.fechaRegistro = :fechaRegistro"),
		@NamedQuery(name = "Padcord.findByCambiarContrasenia", query = "SELECT p FROM Padcord p WHERE p.cambiarContrasenia = :cambiarContrasenia"),
		@NamedQuery(name = "Padcord.findByEnviarBienvenida", query = "SELECT p FROM Padcord p WHERE p.enviarBienvenida = :enviarBienvenida") })
public class Padcord implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "usuario")
	private String usuario;
	@Column(name = "contrasenia")
	private String contrasenia;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "email")
	private String email;
	@Column(name = "tipo")
	private String tipo;
	@Column(name = "id_registrador")
	private String idRegistrador;
	@Column(name = "fecha_registro")
	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;
	@Column(name = "cambiar_contrasenia")
	private Boolean cambiarContrasenia;
	@Column(name = "enviar_bienvenida")
	private Boolean enviarBienvenida;

	public Padcord() {
	}

	public Padcord(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getIdRegistrador() {
		return idRegistrador;
	}

	public void setIdRegistrador(String idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Boolean getCambiarContrasenia() {
		return cambiarContrasenia;
	}

	public void setCambiarContrasenia(Boolean cambiarContrasenia) {
		this.cambiarContrasenia = cambiarContrasenia;
	}

	public Boolean getEnviarBienvenida() {
		return enviarBienvenida;
	}

	public void setEnviarBienvenida(Boolean enviarBienvenida) {
		this.enviarBienvenida = enviarBienvenida;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (usuario != null ? usuario.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Padcord)) {
			return false;
		}
		Padcord other = (Padcord) object;
		if ((this.usuario == null && other.usuario != null)
				|| (this.usuario != null && !this.usuario.equals(other.usuario))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.domainAuth.Padcord[usuario=" + usuario + "]";
	}

}
