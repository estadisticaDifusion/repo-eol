/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.ejb;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.springframework.security.crypto.password.PasswordEncoder;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuarioHistorial;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright (C) 2019
 * Ministerio de Educacion 
 * Unidad de Estadistica Educativa
 * (Lima - Peru)
 *  
 * PROYECTO EOL
 *
 * All rights reserved. Used by permission This software is the confidential and
 * proprietary information of MINEDU ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with MIMP.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */
/**
 * @objetivo: Contener la declaracion de los metodos necesarios para las Auth
 * @autor: Martin Huamani Mendoza
 * @since 10/03/2019
 *
 */
@Local
public interface AuthUsuarioHistorialLocal {

	int createUserHistory(PasswordEncoder passwordEncoder, AuthUsuarioHistorial authUsuarioHistorial);

	void edit(AuthUsuarioHistorial authUsuarioHistorial);

	void remove(AuthUsuarioHistorial authUsuarioHistorial);
	
	AuthUsuarioHistorial find(Object id);
	
	List<AuthUsuarioHistorial> findAllByField(Object field, Object value);
	
	List<AuthUsuarioHistorial> findByParams(Map<String, Object> parameters, String orderBy);

	int getRecordCount(Map<String, Object> parameters);
	
}
