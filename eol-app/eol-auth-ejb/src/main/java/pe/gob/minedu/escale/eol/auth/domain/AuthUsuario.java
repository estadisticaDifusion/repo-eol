/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.auth.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "auth_usuario")
@NamedQueries({
    @NamedQuery(name = "AuthUsuario.findAll", query = "SELECT a FROM AuthUsuario a"),
    @NamedQuery(name = "AuthUsuario.findByUsuario", query = "SELECT a FROM AuthUsuario a WHERE a.usuario = :usuario"),
    @NamedQuery(name = "AuthUsuario.findByContrasenia", query = "SELECT a FROM AuthUsuario a WHERE a.contrasenia = :contrasenia"),
    @NamedQuery(name = "AuthUsuario.findByNombre", query = "SELECT a FROM AuthUsuario a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "AuthUsuario.findByEmail", query = "SELECT a FROM AuthUsuario a WHERE a.email = :email"),
    @NamedQuery(name = "AuthUsuario.findByTipo", query = "SELECT a FROM AuthUsuario a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "AuthUsuario.findByIdRegistrador", query = "SELECT a FROM AuthUsuario a WHERE a.idRegistrador = :idRegistrador"),
    @NamedQuery(name = "AuthUsuario.findByFechaRegistro", query = "SELECT a FROM AuthUsuario a WHERE a.fechaRegistro = :fechaRegistro"),
    @NamedQuery(name = "AuthUsuario.findByCambiarContrasenia", query = "SELECT a FROM AuthUsuario a WHERE a.cambiarContrasenia = :cambiarContrasenia"),
    @NamedQuery(name = "AuthUsuario.findByEnviarBienvenida", query = "SELECT a FROM AuthUsuario a WHERE a.enviarBienvenida = :enviarBienvenida"),
    @NamedQuery(name = "AuthUsuario.findByCorreoConfirmado", query = "SELECT a FROM AuthUsuario a WHERE a.correoConfirmado = :correoConfirmado")})
public class AuthUsuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    @Column(name = "contrasenia")
    private String contrasenia;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "email")
    private String email;
    @Column(name = "tipo")
    private String tipo;
    @Basic(optional = false)
    @Column(name = "id_registrador")
    private String idRegistrador;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "cambiar_contrasenia")
    private Boolean cambiarContrasenia;
    @Column(name = "enviar_bienvenida")
    private Boolean enviarBienvenida;
    @Column(name = "correo_confirmado")
    private Boolean correoConfirmado;
    @ManyToMany(mappedBy = "authUsuarioCollection")
    private Collection<AuthCoordinador> authCoordinadorCollection;

    public AuthUsuario() {
    }

    public AuthUsuario(String usuario) {
        this.usuario = usuario;
    }

    public AuthUsuario(String usuario, String idRegistrador) {
        this.usuario = usuario;
        this.idRegistrador = idRegistrador;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getIdRegistrador() {
        return idRegistrador;
    }

    public void setIdRegistrador(String idRegistrador) {
        this.idRegistrador = idRegistrador;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Boolean getCambiarContrasenia() {
        return cambiarContrasenia;
    }

    public void setCambiarContrasenia(Boolean cambiarContrasenia) {
        this.cambiarContrasenia = cambiarContrasenia;
    }

    public Boolean getEnviarBienvenida() {
        return enviarBienvenida;
    }

    public void setEnviarBienvenida(Boolean enviarBienvenida) {
        this.enviarBienvenida = enviarBienvenida;
    }

    public Boolean getCorreoConfirmado() {
        return correoConfirmado;
    }

    public void setCorreoConfirmado(Boolean correoConfirmado) {
        this.correoConfirmado = correoConfirmado;
    }

    public Collection<AuthCoordinador> getAuthCoordinadorCollection() {
        return authCoordinadorCollection;
    }

    public void setAuthCoordinadorCollection(Collection<AuthCoordinador> authCoordinadorCollection) {
        this.authCoordinadorCollection = authCoordinadorCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuario != null ? usuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuthUsuario)) {
            return false;
        }
        AuthUsuario other = (AuthUsuario) object;
        if ((this.usuario == null && other.usuario != null) || (this.usuario != null && !this.usuario.equals(other.usuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.domainAuth.AuthUsuario[usuario=" + usuario + "]";
    }

}
