/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.auth.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 
 *
 * @author MHUAMANI
 * @version 1.0
 */
@Entity
@Table(name = "auth_usuario_hist")
@NamedQueries({
    @NamedQuery(name = "AuthUsuarioHistorial.findAll", query = "SELECT a FROM AuthUsuarioHistorial a"),
    @NamedQuery(name = "AuthUsuarioHistorial.findByUsuario", query = "SELECT a FROM AuthUsuarioHistorial a WHERE a.usuario = :usuario"),
    @NamedQuery(name = "AuthUsuarioHistorial.findByUsuarioRegistro", query = "SELECT a FROM AuthUsuarioHistorial a WHERE a.usuarioRegistro = :usuarioRegistro")})
public class AuthUsuarioHistorial implements Serializable {
    private static final long serialVersionUID = 1L;
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_contrasenia")
    private Long id;
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    //@Basic(optional = false)
    //@Column(name = "contrasenia_text")
    @Transient
    private String contraseniaText;
    @Basic(optional = false)
    @Column(name = "contrasenia")
    private String contrasenia;
    @Basic(optional = false)
    @Column(name = "usuario_registro")
    private String usuarioRegistro;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    public AuthUsuarioHistorial() {}

    public AuthUsuarioHistorial(String usuario) {
        this.usuario = usuario;
    }

    public AuthUsuarioHistorial(String usuario, String contraseniaText) {
        this.usuario = usuario;
        this.contraseniaText = contraseniaText;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseniaText() {
		return contraseniaText;
	}

	public void setContraseniaText(String contraseniaText) {
		this.contraseniaText = contraseniaText;
	}

	public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
    
	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.domainAuth.AuthUsuario[usuario=" + usuario + "]";
    }

}
