/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.padron.ejb.facade;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.domain.domainPadron.CentroEducativo;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Local
public interface PadronLocal {
	public CentroEducativo obtainByCodMod(String codMod, String anexo);

	public List<CentroEducativo> getCodigosModularesByCodLocal(String codLocal);

	public List<Object[]> getCuentaCentros(String idUgel, String act);

	public List<Object[]> getCuentaCentros2014(String idUgel, String act);

	public List<Object[]> getCuentaCentrosByLocal(String idUgel);

	public List<Object[]> getCuentaCentrosGroupNivMod(String idUgel, String tipo);

	public List<Object[]> getCuentaCentrosGroupNivModMCenso(String idUgel, String tipo);

	public List<Object[]> getCuentaCentrosByUGEL(String idUgel, String tipo, String act);

	public List<Object[]> getCuentaCentrosByUGELMarcoRecuperacion(String idUgel, String tipo, String act, String anio);

	public List<Object[]> getCuentaCentrosByUGEL2012(String idUgel, String tipo, String act);

	public List<Object[]> getCuentaCentrosByUGEL2013(String idUgel, String tipo, String act);

	public List<Object[]> getCuentaCentrosByLocal(String idUgel, String tipo);

	public List<Object[]> getCuentaCentrosByLocal(String idUgel, String tipo, String act);

	public List<CentroEducativo> getCentrosByUgel(String ugel);

	public List<CentroEducativo> getCentrosByCodInst(String codInst);
}
