/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.domain.domainPadron;


import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Entity;

import pe.gob.minedu.escale.eol.domain.domainPadron.Codigo;


/**
 *
 * @author DSILVA
 */
@Entity
@XmlRootElement(name = "gestionDependencia")
@Table(name="v_ges_dep")
@NamedQueries({
    @NamedQuery(name = "GestionDependencia.findAll", query = "SELECT c FROM GestionDependencia c ORDER BY c.orden"),
    @NamedQuery(name = "GestionDependencia.findById", query = "SELECT c FROM GestionDependencia c WHERE c.idCodigo=:id")})
public class GestionDependencia extends Codigo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7341342769773856493L;
}
