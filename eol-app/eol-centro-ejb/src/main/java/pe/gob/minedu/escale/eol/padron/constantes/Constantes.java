/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.padron.constantes;

/**
 *
 * @author JMATAMOROS
 */
public class Constantes {

	public static final String USUARIO_UGEL = "UGEL";
	public static final String USUARIO_CE = "CE";
	public static final String USUARIO_AGP = "AGP";
	public static final String USUARIO_DRE = "DRE";

	public static final String USUARIO_DRE_LIMA_METRO = "150101";
	public static final String USUARIO_DRE_LIMA_PROV = "150200";
	public static final String USUARIO_DRE_CALLAO = "070101";

}
