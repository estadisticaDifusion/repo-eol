/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.domain.domainPadron;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "padron")
@NamedQueries({
    @NamedQuery(name = "CentroEducativo.findAll", query = "SELECT p FROM CentroEducativo p"),
    @NamedQuery(name = "CentroEducativo.selectCodmodsByCodlocal", query = "SELECT p FROM CentroEducativo p WHERE p.codlocal=:codlocal"),
    @NamedQuery(name = "CentroEducativo.findByIds", query = "SELECT p FROM CentroEducativo p WHERE p.codMod=:codMod AND p.anexo=:anexo"),
    @NamedQuery(name = "CentroEducativo.findByCodigoModular", query = "SELECT p FROM CentroEducativo p WHERE p.codMod=:codMod "),
    @NamedQuery(name = "CentroEducativo.findByUgels", query = "SELECT p FROM CentroEducativo p WHERE p.codigel=:codooii"),
    @NamedQuery(name = "CentroEducativo.findByIgels", query = "SELECT p FROM CentroEducativo p WHERE p.codigel=:codigel")
})
public class CentroEducativo implements Serializable {
	
    public static String SI = "1";
    public static String NO = "2";
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "cod_mod")
    private String codMod;
    @Id
    @Column(name = "anexo")
    private String anexo;
    @Column(name = "formas")
    private String formas;
    @Column(name = "codlocal")
    private String codlocal;
    @Column(name = "cen_edu")
    private String cenEdu;
    @Column(name = "dir_cen")
    private String dirCen;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "email")
    private String email;
    @Column(name = "pagweb")
    private String pagweb;
    @Column(name = "director")
    private String director;
    @Column(name = "situadir")
    private String unidcost;
    @Column(name = "codgeo")
    private String codgeo;
    @Column(name = "codooii")
    private String codooii;
    @Column(name = "codigel")
    private String codigel;
    @Column(name = "cen_pob")
    private String cenPob;
    @Column(name = "codccpp")
    private String codccpp;
    @Column(name = "cod_area")
    private String codArea;
    @Column(name = "ges_dep")
    private String gesDep;
    @Column(name = "niv_mod")
    private String nivMod;
    @Column(name = "cod_tur")
    private String codTur;
    @Column(name = "tipssexo")
    private String tipssexo;
    @Column(name = "cod_car")
    private String codCar;
    @Column(name = "tipoprog")
    private String tipoprog;
    @Column(name = "tiponee")
    private String tiponee;
    @Column(name = "progdist")
    private String progdist;
    @Column(name = "progarti")
    private String progarti;
    @Column(name = "estado")
    private String estado;
    @Lob
    @Column(name = "comenta")
    private String comenta;
    @Column(name = "fechareg")
    private String fechareg;
    @Column(name = "fecharet")
    private String fecharet;
    @Column(name = "area_sig")
    private String areaSig;
    @Column(name = "registro")
    private String registro;
    @Column(name = "tipoice")
    private String tipoice;
    @Column(name = "progise")
    private String progise;
    @Column(name = "Mcenso")
    private String mcenso;
    @Column(name = "gestion")
    private String gestion;
    //imendoza 20170317 inicio
    @Column(name = "codinst")
    private String codinst;
    //imendoza 20170317 fin
    //imendoza 20170421 inicio
    @Column(name = "sienvio")
    private String sienvio;
    //imendoza 20170421 fin

    public CentroEducativo() {
    }

    public CentroEducativo(String codMod, String anexo) {
        this.codMod = codMod;
        this.anexo = anexo;
    }

    public String getFormas() {
        return formas;
    }

    public void setFormas(String formas) {
        this.formas = formas;
    }

    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    public String getDirCen() {
        return dirCen;
    }

    public void setDirCen(String dirCen) {
        this.dirCen = dirCen;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPagweb() {
        return pagweb;
    }

    public void setPagweb(String pagweb) {
        this.pagweb = pagweb;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getUnidcost() {
        return unidcost;
    }

    public void setUnidcost(String unidcost) {
        this.unidcost = unidcost;
    }

    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    public String getCodooii() {
        return codooii;
    }

    public void setCodooii(String codooii) {
        this.codooii = codooii;
    }

    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    public String getCodccpp() {
        return codccpp;
    }

    public void setCodccpp(String codccpp) {
        this.codccpp = codccpp;
    }

    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    public String getGesDep() {
        return gesDep;
    }

    public void setGesDep(String gesDep) {
        this.gesDep = gesDep;
    }

    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    public String getCodTur() {
        return codTur;
    }

    public void setCodTur(String codTur) {
        this.codTur = codTur;
    }

    public String getTipssexo() {
        return tipssexo;
    }

    public void setTipssexo(String tipssexo) {
        this.tipssexo = tipssexo;
    }

    public String getCodCar() {
        return codCar;
    }

    public void setCodCar(String codCar) {
        this.codCar = codCar;
    }

    public String getTipoprog() {
        return tipoprog;
    }

    public void setTipoprog(String tipoprog) {
        this.tipoprog = tipoprog;
    }

    public String getTiponee() {
        return tiponee;
    }

    public void setTiponee(String tiponee) {
        this.tiponee = tiponee;
    }

    public String getProgdist() {
        return progdist;
    }

    public void setProgdist(String progdist) {
        this.progdist = progdist;
    }

    public String getProgarti() {
        return progarti;
    }

    public void setProgarti(String progarti) {
        this.progarti = progarti;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getComenta() {
        return comenta;
    }

    public void setComenta(String comenta) {
        this.comenta = comenta;
    }

    public String getFechareg() {
        return fechareg;
    }

    public void setFechareg(String fechareg) {
        this.fechareg = fechareg;
    }

    public String getFecharet() {
        return fecharet;
    }

    public void setFecharet(String fecharet) {
        this.fecharet = fecharet;
    }

    public String getAreaSig() {
        return areaSig;
    }

    public void setAreaSig(String areaSig) {
        this.areaSig = areaSig;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CentroEducativo other = (CentroEducativo) obj;
        if ((this.codMod == null) ? (other.codMod != null) : !this.codMod.equals(other.codMod)) {
            return false;
        }
        if ((this.anexo == null) ? (other.anexo != null) : !this.anexo.equals(other.anexo)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.codMod != null ? this.codMod.hashCode() : 0);
        hash = 43 * hash + (this.anexo != null ? this.anexo.hashCode() : 0);
        return hash;
    }

    public String getCodigel() {
        return codigel;
    }

    public void setCodigel(String codigel) {
        this.codigel = codigel;
    }

    public String getTipoice() {
        return tipoice;
    }

    public void setTipoice(String tipoice) {
        this.tipoice = tipoice;
    }

    public String getProgise() {
        return progise;
    }

    public void setProgise(String progise) {
        this.progise = progise;
    }

    public String getMcenso() {
        return mcenso;
    }

    public void setMcenso(String mcenso) {
        this.mcenso = mcenso;
    }

    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }
    //imendoza 20170317 inicio

    public String getCodinst() {
        return codinst;
    }

    public void setCodinst(String codinst) {
        this.codinst = codinst;
    }

    public String getSienvio() {
        return sienvio;
    }

    public void setSienvio(String sienvio) {
        this.sienvio = sienvio;
    }
    //imendoza 20170317 fin
}
