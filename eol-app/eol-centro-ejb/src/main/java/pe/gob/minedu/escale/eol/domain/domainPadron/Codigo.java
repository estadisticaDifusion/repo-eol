package pe.gob.minedu.escale.eol.domain.domainPadron;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 *
 * @author DSILVA 
 */
@Entity
//@Table(name = "codigos")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@DiscriminatorColumn(name = "campo")
public abstract class Codigo implements Serializable {

    @Id
    @Column(name = "id")
    private String idCodigo;
    @Column(name = "valor")
    private String valor;
    @Column(name = "orden")
    private int orden;

    public String getIdCodigo() {
        return idCodigo;
    }

    public void setIdCodigo(String idCodigo) {
        this.idCodigo = idCodigo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Codigo other = (Codigo) obj;
        if ((this.idCodigo == null) ? (other.idCodigo != null) : !this.idCodigo.equals(other.idCodigo)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.idCodigo != null ? this.idCodigo.hashCode() : 0);
        return hash;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    @Override
    public String toString() {
        return valor;
    }


}
