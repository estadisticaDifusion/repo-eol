/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.importar.dao.domain;

/**
 *
 * @author DSILVA
 */
public class TableField {

    String name;
    String type;
    String nulleable;
    String key;
    String defaultValue;

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNulleable() {
        return nulleable;
    }

    public void setNulleable(String nulleable) {
        this.nulleable = nulleable;
    }
}
