/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.importar.dao.domain;

import java.io.Serializable;

/**
 *
 * @author DSILVA
 */

public class InstitucionEducativa implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String codigoModular;
    private String anexo;
    private String codigoLocal;
    private String nombreIE;
    private String codDreUgel;
    private String nomDreUgel;
    private String ubigeo;
    private String codCentroPoblado;
    private String centroPoblado;
    private String codNivelModalidad;
    private String codGestion;
    private String descGestion;
    private String forma;
    private String descForma;
    private String codTurno;
    private String codArea;
    private String estado;
    private String descEstado;    
    private String direccion;
    private String telefono;
    private String paginaWeb;
    private String correoElectronico;
    private String director;
    private String tipoSexo;
    private float coordX;
    private float coordY;
    private String departamento;
    private String provincia;
    private String distrito;
    private String nivelModalidad;
    private int alumnos;
    private int docentes;
    private String descTipoSexo;
    private String descTurno;
    
    
    private String descArea;
    private String altCP;
    private String disCrecer;
    private String disJuntos;
    private String disVrae;

    private String codDependencia;
    private String descDependencia;

    private String codCaracteristica;
    private String descCaracteristica;
    public String getDescArea() {
        return descArea;
    }

    public void setDescArea(String descArea) {
        this.descArea = descArea;
    }

    public String getDescGestion() {
        return descGestion;
    }

    public void setDescGestion(String descGestion) {
        this.descGestion = descGestion;
    }

   
    public String getDescTurno() {
        return descTurno;
    }

    public void setDescTurno(String descTurno) {
        this.descTurno = descTurno;
    }

    public String getDescTipoSexo() {
        return descTipoSexo;
    }

    public void setDescTipoSexo(String descTipoSexo) {
        this.descTipoSexo = descTipoSexo;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getCodigoModular() {
        return codigoModular;
    }

    public void setCodigoModular(String codigoModular) {
        this.codigoModular = codigoModular;
    }

    public String getCentroPoblado() {
        return centroPoblado;
    }

    public void setCentroPoblado(String centroPoblado) {
        this.centroPoblado = centroPoblado;
    }

    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    public String getCodCentroPoblado() {
        return codCentroPoblado;
    }

    public void setCodCentroPoblado(String codCentroPoblado) {
        this.codCentroPoblado = codCentroPoblado;
    }

    public String getCodDreUgel() {
        return codDreUgel;
    }

    public void setCodDreUgel(String codDreUgel) {
        this.codDreUgel = codDreUgel;
    }

    public String getCodGestion() {
        return codGestion;
    }

    public void setCodGestion(String codGestion) {
        this.codGestion = codGestion;
    }

    public String getCodNivelModalidad() {
        return codNivelModalidad;
    }

    public void setCodNivelModalidad(String codNivelModalidad) {
        this.codNivelModalidad = codNivelModalidad;
    }

    public String getCodTurno() {
        return codTurno;
    }

    public void setCodTurno(String codTurno) {
        this.codTurno = codTurno;
    }

    public String getCodigoLocal() {
        return codigoLocal;
    }

    public void setCodigoLocal(String codigoLocal) {
        this.codigoLocal = codigoLocal;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    public String getNombreIE() {
        return nombreIE;
    }

    public void setNombreIE(String nombreIE) {
        this.nombreIE = nombreIE;
    }

 

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

   
    public String getTipoSexo() {
        return tipoSexo;
    }

    public void setTipoSexo(String tipoSexo) {
        this.tipoSexo = tipoSexo;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public float getCoordX() {
        return coordX;
    }

    public void setCoordX(float coordX) {
        this.coordX = coordX;
    }

    public float getCoordY() {
        return coordY;
    }

    public void setCoordY(float coordY) {
        this.coordY = coordY;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getNivelModalidad() {
        return nivelModalidad;
    }

    public void setNivelModalidad(String nivelModalidad) {
        this.nivelModalidad = nivelModalidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public int getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(int alumnos) {
        this.alumnos = alumnos;
    }

    public int getDocentes() {
        return docentes;
    }

    public void setDocentes(int docentes) {
        this.docentes = docentes;
    }

    public String getDescEstado() {
        return descEstado;
    }

    public void setDescEstado(String descEstado) {
        this.descEstado = descEstado;
    }

   

    public String getDisCrecer() {
        if (disCrecer != null && disCrecer.trim().equals("9")) {
            return "NO";
        } else if (disCrecer != null && !disCrecer.isEmpty()) {
            return "SI";
        }
        return disCrecer;
    }


    public void setDisCrecer(String disCrecer) {
        this.disCrecer = disCrecer;
    }
    public String getDisJuntos() {
        if (disJuntos != null && disJuntos.trim().equals("9")) {
            return "NO";
        } else if (disJuntos != null && !disJuntos.isEmpty()) {
            return "SI";
        }
        return disJuntos;
    }

    public void setDisJuntos(String disJuntos) {
        this.disJuntos = disJuntos;
    }

    public String getDisVrae() {
        if (disVrae != null && disVrae.trim().equals("9")) {
            return "NO";
        } else if (disVrae != null && !disVrae.isEmpty()) {
            return "SI";
        }
        return disVrae;
    }

    public void setDisVrae(String disVrae) {
        this.disVrae = disVrae;
    }

 @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InstitucionEducativa other = (InstitucionEducativa) obj;
        if ((this.codigoModular == null) ? (other.codigoModular != null) : !this.codigoModular.equals(other.codigoModular)) {
            return false;
        }
        if ((this.anexo == null) ? (other.anexo != null) : !this.anexo.equals(other.anexo)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.codigoModular != null ? this.codigoModular.hashCode() : 0);
        hash = 89 * hash + (this.anexo != null ? this.anexo.hashCode() : 0);
        return hash;
    }

    /**
     * @return the altCP
     */
    public String getAltCP() {
        return altCP;
    }

    /**
     * @param altCP the altCP to set
     */
    public void setAltCP(String altCP) {
        this.altCP = altCP;
    }

    public String getCodDependencia() {
        return codDependencia;
    }

    public void setCodDependencia(String codDependencia) {
        this.codDependencia = codDependencia;
    }

    public String getDescDependencia() {
        return descDependencia;
    }

    public void setDescDependencia(String descDependencia) {
        this.descDependencia = descDependencia;
    }

    public String getNomDreUgel() {
        return nomDreUgel;
    }

    public void setNomDreUgel(String nomDreUgel) {
        this.nomDreUgel = nomDreUgel;
    }

    /**
     * @return the codCaracteristica
     */
    public String getCodCaracteristica() {
        return codCaracteristica;
    }

    /**
     * @param codCaracteristica the codCaracteristica to set
     */
    public void setCodCaracteristica(String codCaracteristica) {
        this.codCaracteristica = codCaracteristica;
    }

    /**
     * @return the descCaracteristica
     */
    public String getDescCaracteristica() {
        return descCaracteristica;
    }

    /**
     * @param descCaracteristica the descCaracteristica to set
     */
    public void setDescCaracteristica(String descCaracteristica) {
        this.descCaracteristica = descCaracteristica;
    }

    /**
     * @return the descForma
     */
    public String getDescForma() {
        return descForma;
    }

    /**
     * @param descForma the descForma to set
     */
    public void setDescForma(String descForma) {
        this.descForma = descForma;
    }

}
