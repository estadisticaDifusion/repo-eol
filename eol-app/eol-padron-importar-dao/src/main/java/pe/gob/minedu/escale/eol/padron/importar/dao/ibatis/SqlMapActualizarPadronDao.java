/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.importar.dao.ibatis;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import pe.gob.minedu.escale.eol.padron.importar.dao.ActualizarPadronDao;
import pe.gob.minedu.escale.eol.padron.importar.dao.domain.TableField;
import pe.gob.minedu.escale.eol.padron.importar.dao.domain.TableStructure;

/**
 *
 * @author DSILVA
 */
public class SqlMapActualizarPadronDao extends SqlMapClientTemplate implements ActualizarPadronDao {

  //  private static final Logger logger = Logger.getLogger(SqlMapActualizarPadronDao.class);
    private static final long serialVersionUID = -2923004356023465825L;
    
    private String tempTable;
    private String tabla;

    @Override
    public void agregar(Map<String, String> fila) {
        //if (fila.get("FECHARES").equals("")) {
        //    fila.remove("FECHARES");
        //}
        
        fila.put("tabla", tempTable);        
        insert("insert-insert_into_padron", fila);

    }

    @Override
    public void nuevaTabla() {
        logger.debug("nueva tabla");

        Date date = new Date();
        tempTable = ("TMP" + date.getTime());
        logger.debug("table tmp:" + tempTable);
        Map<String, String> param = new HashMap<String, String>();
        param.put("tabla", tabla);
        TableStructure ret = (TableStructure) queryForObject(
                "select-show_create_table", param);
        logger.debug("returned:" + ret.getSql());
        String sqlNuevaTabla = ret.getSql().replaceFirst(
                "CREATE TABLE `" + tabla + "` ", "CREATE TABLE " + tempTable);
        logger.debug("nuevo create:" + sqlNuevaTabla);
        update("update-create_table", sqlNuevaTabla);
    // String createSQL=ret.get("")
    }

    @Override
    public void actualizar() {
        Map<String, String> param = new HashMap<String, String>();
        Calendar cal = Calendar.getInstance();

        String hist = tabla + "_" + String.valueOf(cal.get(Calendar.YEAR)) + String.format("%1$02d", cal.get(Calendar.MONTH) + 1) + String.format("%1$02d", cal.get(Calendar.DAY_OF_MONTH) ) + "_" + String.format("%1$02d", cal.get(Calendar.HOUR_OF_DAY)) + String.format("%1$02d", cal.get(Calendar.MINUTE)) + String.format("%1$02d", cal.get(Calendar.SECOND));
        param.put("nuevo", hist);
        param.put("anterior", tabla);
        update("update-alter_table_rename", param);

        param.put("nuevo", tabla);
        param.put("anterior", tempTable);
        update("update-alter_table_rename", param);
        //update("update_local");

    }

    @Override
    public void borrarTabla() {
        // TODO Auto-generated method stub
    }

    @Override
    public void setTabla(String tabla) {
        this.tabla = tabla;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TableField> getFields() {
        logger.debug("obteniendo campos de " + tabla);
        Map<String, String> param = new HashMap<String, String>();
        param.put("tabla", tabla);
        List<TableField> campos = queryForList("select-show_columns", param);
        logger.debug("campos:" + campos);
        //TEST

        return campos;
    }
}
