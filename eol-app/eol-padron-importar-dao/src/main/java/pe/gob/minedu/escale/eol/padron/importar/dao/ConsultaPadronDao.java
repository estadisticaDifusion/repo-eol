/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.importar.dao;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.eol.padron.importar.dao.domain.InstitucionEducativa;
import pe.gob.minedu.escale.eol.padron.importar.dao.domain.InstitucionEducativaInfo;

/**
 *
 * @author DSILVA
 */
public interface ConsultaPadronDao {

    List<InstitucionEducativa> getInstitucionesList(Map<String, Object> params);

    InstitucionEducativa getInstitucionByCodmodAnexo(String codigoModular, String anexo);

    List<InstitucionEducativaInfo> getInstitucionesInfoList(Map<String, Object> params);

    InstitucionEducativaInfo getInstitucionInfoByCodmodAnexo(String codigoModular,String anexo);
}
