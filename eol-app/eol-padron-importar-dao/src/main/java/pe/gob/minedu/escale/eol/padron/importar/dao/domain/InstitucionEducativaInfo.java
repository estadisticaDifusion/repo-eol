/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.padron.importar.dao.domain;
import java.io.Serializable;
/**
 *
 * @author JMATAMOROS
 */
public class InstitucionEducativaInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String codMod;
    private String anexo;
    private String codLocal;
    private String metodo;
    private String cenEdu;
    private String nivMod;
    private String formas;
    private String tipoProg;
    private String gestion;
    private String gesDep;
    private String telefono;
    private String email;
    private String pagweb;
    private String codCar;
    private String tipsSexo;
    private String codTur;
    private String contjesc;
    private String progDist;
    private String progArti;
    private String fechaReg;
    private String fechaRet;
    private String fechaRea;
    private String estado;
    private String dirCen;
    private String codGeo;
    private String dpto;
    private String prov;
    private String dist;
    private String codcpMed;//codccpp
    private String codcpInei;
    private String cenPob;
    private String areaMed;//area_sig
    private String codIgel;
    private String tipoIgel;
    private String igel;
    private String clongCP;
    private String clatCP;
    private String clongIE;
    private String clatIE;
    private String altCP;
    private String fteCP;

    /**
     * @return the codMod
     */
    public String getCodMod() {
        return codMod;
    }

    /**
     * @param codMod the codMod to set
     */
    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    /**
     * @return the anexo
     */
    public String getAnexo() {
        return anexo;
    }

    /**
     * @param anexo the anexo to set
     */
    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    /**
     * @return the codLocal
     */
    public String getCodLocal() {
        return codLocal;
    }

    /**
     * @param codLocal the codLocal to set
     */
    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    /**
     * @return the metodo
     */
    public String getMetodo() {
        return metodo;
    }

    /**
     * @param metodo the metodo to set
     */
    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    /**
     * @return the cenEdu
     */
    public String getCenEdu() {
        return cenEdu;
    }

    /**
     * @param cenEdu the cenEdu to set
     */
    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    /**
     * @return the nivMod
     */
    public String getNivMod() {
        return nivMod;
    }

    /**
     * @param nivMod the nivMod to set
     */
    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    /**
     * @return the formas
     */
    public String getFormas() {
        return formas;
    }

    /**
     * @param formas the formas to set
     */
    public void setFormas(String formas) {
        this.formas = formas;
    }

    /**
     * @return the tipoProg
     */
    public String getTipoProg() {
        return tipoProg;
    }

    /**
     * @param tipoProg the tipoProg to set
     */
    public void setTipoProg(String tipoProg) {
        this.tipoProg = tipoProg;
    }

    /**
     * @return the gestion
     */
    public String getGestion() {
        return gestion;
    }

    /**
     * @param gestion the gestion to set
     */
    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    /**
     * @return the gesDep
     */
    public String getGesDep() {
        return gesDep;
    }

    /**
     * @param gesDep the gesDep to set
     */
    public void setGesDep(String gesDep) {
        this.gesDep = gesDep;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the pagweb
     */
    public String getPagweb() {
        return pagweb;
    }

    /**
     * @param pagweb the pagweb to set
     */
    public void setPagweb(String pagweb) {
        this.pagweb = pagweb;
    }

    /**
     * @return the codCar
     */
    public String getCodCar() {
        return codCar;
    }

    /**
     * @param codCar the codCar to set
     */
    public void setCodCar(String codCar) {
        this.codCar = codCar;
    }

    /**
     * @return the tipsSexo
     */
    public String getTipsSexo() {
        return tipsSexo;
    }

    /**
     * @param tipsSexo the tipsSexo to set
     */
    public void setTipsSexo(String tipsSexo) {
        this.tipsSexo = tipsSexo;
    }

    /**
     * @return the codTur
     */
    public String getCodTur() {
        return codTur;
    }

    /**
     * @param codTur the codTur to set
     */
    public void setCodTur(String codTur) {
        this.codTur = codTur;
    }

    /**
     * @return the contjesc
     */
    public String getContjesc() {
        return contjesc;
    }

    /**
     * @param contjesc the contjesc to set
     */
    public void setContjesc(String contjesc) {
        this.contjesc = contjesc;
    }

    /**
     * @return the progDist
     */
    public String getProgDist() {
        return progDist;
    }

    /**
     * @param progDist the progDist to set
     */
    public void setProgDist(String progDist) {
        this.progDist = progDist;
    }

    /**
     * @return the progArti
     */
    public String getProgArti() {
        return progArti;
    }

    /**
     * @param progArti the progArti to set
     */
    public void setProgArti(String progArti) {
        this.progArti = progArti;
    }

    /**
     * @return the fechaReg
     */
    public String getFechaReg() {
        return fechaReg;
    }

    /**
     * @param fechaReg the fechaReg to set
     */
    public void setFechaReg(String fechaReg) {
        this.fechaReg = fechaReg;
    }

    /**
     * @return the fechaRet
     */
    public String getFechaRet() {
        return fechaRet;
    }

    /**
     * @param fechaRet the fechaRet to set
     */
    public void setFechaRet(String fechaRet) {
        this.fechaRet = fechaRet;
    }

    /**
     * @return the fechaRea
     */
    public String getFechaRea() {
        return fechaRea;
    }

    /**
     * @param fechaRea the fechaRea to set
     */
    public void setFechaRea(String fechaRea) {
        this.fechaRea = fechaRea;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the dirCen
     */
    public String getDirCen() {
        return dirCen;
    }

    /**
     * @param dirCen the dirCen to set
     */
    public void setDirCen(String dirCen) {
        this.dirCen = dirCen;
    }

    /**
     * @return the codGeo
     */
    public String getCodGeo() {
        return codGeo;
    }

    /**
     * @param codGeo the codGeo to set
     */
    public void setCodGeo(String codGeo) {
        this.codGeo = codGeo;
    }

    /**
     * @return the dpto
     */
    public String getDpto() {
        return dpto;
    }

    /**
     * @param dpto the dpto to set
     */
    public void setDpto(String dpto) {
        this.dpto = dpto;
    }

    /**
     * @return the prov
     */
    public String getProv() {
        return prov;
    }

    /**
     * @param prov the prov to set
     */
    public void setProv(String prov) {
        this.prov = prov;
    }

    /**
     * @return the dist
     */
    public String getDist() {
        return dist;
    }

    /**
     * @param dist the dist to set
     */
    public void setDist(String dist) {
        this.dist = dist;
    }

    /**
     * @return the codcpMed
     */
    public String getCodcpMed() {
        return codcpMed;
    }

    /**
     * @param codcpMed the codcpMed to set
     */
    public void setCodcpMed(String codcpMed) {
        this.codcpMed = codcpMed;
    }

    /**
     * @return the codcpInei
     */
    public String getCodcpInei() {
        return codcpInei;
    }

    /**
     * @param codcpInei the codcpInei to set
     */
    public void setCodcpInei(String codcpInei) {
        this.codcpInei = codcpInei;
    }

    /**
     * @return the cenPob
     */
    public String getCenPob() {
        return cenPob;
    }

    /**
     * @param cenPob the cenPob to set
     */
    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    /**
     * @return the areaMed
     */
    public String getAreaMed() {
        return areaMed;
    }

    /**
     * @param areaMed the areaMed to set
     */
    public void setAreaMed(String areaMed) {
        this.areaMed = areaMed;
    }

    /**
     * @return the codIgel
     */
    public String getCodIgel() {
        return codIgel;
    }

    /**
     * @param codIgel the codIgel to set
     */
    public void setCodIgel(String codIgel) {
        this.codIgel = codIgel;
    }

    /**
     * @return the tipoIgel
     */
    public String getTipoIgel() {
        return tipoIgel;
    }

    /**
     * @param tipoIgel the tipoIgel to set
     */
    public void setTipoIgel(String tipoIgel) {
        this.tipoIgel = tipoIgel;
    }

    /**
     * @return the igel
     */
    public String getIgel() {
        return igel;
    }

    /**
     * @param igel the igel to set
     */
    public void setIgel(String igel) {
        this.igel = igel;
    }

    /**
     * @return the clongCP
     */
    public String getClongCP() {
        return clongCP;
    }

    /**
     * @param clongCP the clongCP to set
     */
    public void setClongCP(String clongCP) {
        this.clongCP = clongCP;
    }

    /**
     * @return the clatCP
     */
    public String getClatCP() {
        return clatCP;
    }

    /**
     * @param clatCP the clatCP to set
     */
    public void setClatCP(String clatCP) {
        this.clatCP = clatCP;
    }

    /**
     * @return the clongIE
     */
    public String getClongIE() {
        return clongIE;
    }

    /**
     * @param clongIE the clongIE to set
     */
    public void setClongIE(String clongIE) {
        this.clongIE = clongIE;
    }

    /**
     * @return the clatIE
     */
    public String getClatIE() {
        return clatIE;
    }

    /**
     * @param clatIE the clatIE to set
     */
    public void setClatIE(String clatIE) {
        this.clatIE = clatIE;
    }

    /**
     * @return the altCP
     */
    public String getAltCP() {
        return altCP;
    }

    /**
     * @param altCP the altCP to set
     */
    public void setAltCP(String altCP) {
        this.altCP = altCP;
    }

    /**
     * @return the fteCP
     */
    public String getFteCP() {
        return fteCP;
    }

    /**
     * @param fteCP the fteCP to set
     */
    public void setFteCP(String fteCP) {
        this.fteCP = fteCP;
    }














}
