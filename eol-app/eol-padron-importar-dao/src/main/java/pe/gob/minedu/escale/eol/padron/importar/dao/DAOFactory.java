/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.importar.dao;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author DSILVA
 */
public class DAOFactory {

    private BeanFactory factory;

    private DAOFactory() {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                new String[]{"pe/gob/minedu/escale/padron/resources/spring.xml"});

        // of course, an ApplicationContext is just a BeanFactory
        factory = (BeanFactory) context;
    }
    private static DAOFactory instance;

    public static DAOFactory getInstance() {
        if (instance == null) {
            instance = new DAOFactory();
        }
        return instance;
    }

    public ActualizarPadronDao getActualizarPadronDAO(String tabla) {
        ActualizarPadronDao ret = (ActualizarPadronDao) factory.getBean("actualizarPadronDao");
        ret.setTabla(tabla);
        return ret;
    }

    public ConsultaPadronDao getConsultaPadronDao() {
        return (ConsultaPadronDao) factory.getBean("consultaPadronDao");
    }

    public ActualizarPadronSigDao getActualizarPadronSigDAO() {
        ActualizarPadronSigDao ret = (ActualizarPadronSigDao) factory.getBean("actualizarPadronSigDao");
        return ret;
    }
}
