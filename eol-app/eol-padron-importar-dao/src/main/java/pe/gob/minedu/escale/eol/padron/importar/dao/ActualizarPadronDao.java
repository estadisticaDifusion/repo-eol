/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.importar.dao;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.eol.padron.importar.dao.domain.TableField;

/**
 *
 * @author DSILVA
 */
public interface ActualizarPadronDao {

    void nuevaTabla();

    void agregar(Map<String, String> fila);

    void borrarTabla();

    void actualizar();

    void setTabla(String tabla);

    List<TableField> getFields();
}
