/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.importar.dao.domain;

/**
 *
 * @author DSILVA
 */
public class CampoComparado {

    private String dbf;
    private String db;

    public CampoComparado() {
    }

    public CampoComparado(String dbf, String db) {
        this.dbf = dbf;
        this.db = db;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getDbf() {
        return dbf;
    }

    public void setDbf(String dbf) {
        this.dbf = dbf;
    }
}
