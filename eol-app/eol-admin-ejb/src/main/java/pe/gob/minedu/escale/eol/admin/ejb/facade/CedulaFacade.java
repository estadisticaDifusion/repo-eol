/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.ejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.admin.domain.EolActividad;
import pe.gob.minedu.escale.eol.admin.domain.EolCedula;
import pe.gob.minedu.escale.eol.admin.domain.EolPeriodo;
import pe.gob.minedu.escale.eol.admin.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.admin.ejb.CedulaLocal;
import pe.gob.minedu.escale.eol.dto.estadistica.EolActividadDTO;
import pe.gob.minedu.escale.eol.dto.estadistica.EolCedulaDTO;
import pe.gob.minedu.escale.eol.dto.estadistica.EolPeriodoDTO;

/**
 *
 * @author JMATAMOROS
 */
@Stateless
public class CedulaFacade extends AbstractFacade<EolCedula> implements CedulaLocal {

	@PersistenceContext(unitName = "eol-adminPU")
	private EntityManager em;

	public CedulaFacade() {
		super(EolCedula.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public List<EolCedula> findByActividad(Integer idActividad) {
		Query q = em.createQuery("SELECT C FROM EolCedula C WHERE C.eolActividad.id=:id");
		q.setParameter("id", idActividad);
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<EolCedula> findCedulas(Integer idActividad, String nombreActividad, String nivel, String estado,
			String periodo) {
		StringBuffer strQ = new StringBuffer("SELECT c FROM EolCedula c ");

		if (idActividad != null) {
			strQ.append(" WHERE c.eolActividad.id=:idActividad");
		}

		if (nivel != null && !nivel.isEmpty() && strQ.toString().indexOf("WHERE") != -1) {
			strQ.append(" AND c.nivel=:nivel ");
		} else if (nivel != null && !nivel.isEmpty()) {
			strQ.append(" WHERE c.nivel=:nivel ");
		}

		if (estado != null && !estado.isEmpty() && strQ.toString().indexOf("WHERE") != -1) {
			strQ.append(" AND c.estado=:estado ");
		} else if (estado != null && !estado.isEmpty()) {
			strQ.append(" WHERE c.estado=:estado ");
		}

		if (nombreActividad != null && !nombreActividad.isEmpty() && strQ.toString().indexOf("WHERE") != -1) {
			strQ.append(" AND c.eolActividad.nombre=:nombreActividad ");
		} else if (nombreActividad != null && !nombreActividad.isEmpty()) {
			strQ.append(" WHERE c.eolActividad.nombre=:nombreActividad ");
		}

		if (periodo != null && !periodo.isEmpty() && strQ.toString().indexOf("WHERE") != -1) {
			strQ.append(" AND c.eolActividad.eolPeriodo.anio=:periodo ");
		} else if (periodo != null && !periodo.isEmpty()) {
			strQ.append(" WHERE c.eolActividad.eolPeriodo.anio=:periodo ");
		}

		strQ.append(" ORDER BY c.eolActividad.orden ");

		Query q = em.createQuery(strQ.toString());

		if (nivel != null && !nivel.isEmpty()) {
			q.setParameter("nivel", nivel);
		}

		if (estado != null && !estado.isEmpty()) {
			q.setParameter("estado", estado);
		}

		if (nombreActividad != null && !nombreActividad.isEmpty()) {
			q.setParameter("nombreActividad", nombreActividad);
		}

		if (periodo != null && !periodo.isEmpty()) {
			q.setParameter("periodo", periodo);
		}

		return q.getResultList();

	}

	public List<EolCedulaDTO> findCedulaList(Integer idActividad, String nombreActividad, String nivel, String estado,
			String periodo) {
		List<EolCedulaDTO> cedulaDTOList = new ArrayList<EolCedulaDTO>();
		List<EolCedula> listCedula = findCedulas(idActividad, nombreActividad, nivel, estado, periodo);
		if (listCedula != null) {
			EolCedulaDTO eolCedulaDTO = null;
			for (EolCedula eolCedula : listCedula) {
				eolCedulaDTO = new EolCedulaDTO();
				eolCedulaDTO.setId(eolCedula.getId());
				eolCedulaDTO.setNombre(eolCedula.getNombre());
				eolCedulaDTO.setArcXls(eolCedula.getArcXls());
				eolCedulaDTO.setArcPdf(eolCedula.getArcPdf());
				eolCedulaDTO.setSqlBase(eolCedula.getSqlBase());
				eolCedulaDTO.setNivel(eolCedula.getNivel());
				eolCedulaDTO.setEstado(eolCedula.getEstado());
				eolCedulaDTO.setVersion(eolCedula.getVersion());
				eolCedulaDTO.setActividad(setActividadDto(eolCedula.getEolActividad()));
				cedulaDTOList.add(eolCedulaDTO);
			}
		}
		return cedulaDTOList;
	}
	
	private EolActividadDTO setActividadDto(EolActividad eolActividad) {
		EolActividadDTO eolActividadDTO = new EolActividadDTO();
		if (eolActividad != null) {
			eolActividadDTO.setId(eolActividad.getId());
			eolActividadDTO.setNombre(eolActividad.getNombre());
			eolActividadDTO.setDescripcion(eolActividad.getDescripcion());
			eolActividadDTO.setFechaInicio(eolActividad.getFechaInicio());
			eolActividadDTO.setFechaTermino(eolActividad.getFechaTermino());
			eolActividadDTO.setFechaLimite(eolActividad.getFechaLimite());
			eolActividadDTO.setFechaTerminosigied(eolActividad.getFechaTerminosigied());
			eolActividadDTO.setFechaLimitesigied(eolActividad.getFechaLimitesigied());
			eolActividadDTO.setFechaTerminohuelga(eolActividad.getFechaTerminohuelga());
			eolActividadDTO.setFechaLimitehuelga(eolActividad.getFechaLimitehuelga());
			eolActividadDTO.setUrlFormato(eolActividad.getUrlFormato());
			eolActividadDTO.setUrlConstancia(eolActividad.getUrlConstancia());
			eolActividadDTO.setUrlCobertura(eolActividad.getUrlCobertura());
			eolActividadDTO.setUrlSituacion(eolActividad.getUrlSituacion());
			eolActividadDTO.setUrlOmisos(eolActividad.getUrlOmisos());
			eolActividadDTO.setSqlCenso(eolActividad.getSqlCenso());
			eolActividadDTO.setEstado(eolActividad.getEstado());
			eolActividadDTO.setSituacion(eolActividad.getSituacion());
			eolActividadDTO.setNombreCedula(eolActividad.getNombreCedula());
			eolActividadDTO.setTipo(eolActividad.getTipo());
			eolActividadDTO.setEstadoSie(eolActividad.getEstadoSie());
			eolActividadDTO.setEstadoConstancia(eolActividad.getEstadoConstancia());
			eolActividadDTO.setEstadoFormato(eolActividad.getEstadoFormato());
			eolActividadDTO.setEstadoCobertura(eolActividad.getEstadoCobertura());
			eolActividadDTO.setEstadoSituacion(eolActividad.getEstadoSituacion());
			eolActividadDTO.setEstadoOmisos(eolActividad.getEstadoOmisos());
			eolActividadDTO.setOrden(eolActividad.getOrden());
			eolActividadDTO.setPeriodo(setPeriodoDto(eolActividad.getEolPeriodo()));
		}
		return eolActividadDTO;
	}
	
	private EolPeriodoDTO setPeriodoDto(EolPeriodo eolPeriodo) {
		EolPeriodoDTO eolPeriodoDTO = new EolPeriodoDTO();
		if (eolPeriodo != null) {
			eolPeriodoDTO.setId(eolPeriodo.getId());
			eolPeriodoDTO.setAnio(eolPeriodo.getAnio());
			eolPeriodoDTO.setDescripcion(eolPeriodo.getDescripcion());
		}
		return eolPeriodoDTO;
	}
}
