/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.ejb;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;
import pe.gob.minedu.escale.eol.admin.domain.EolActividad;
import pe.gob.minedu.escale.eol.converter.EolActividadConverter;

/**
 *
 * @author cym
 */
@Local
public interface ActividadLocal {

    public Collection<EolActividad> findActividades(String nombre, String periodo);
    public List<EolActividad> findAllActividades(String periodo);
    public EolActividad findById(int id);
    public void update(EolActividad entity);
    public List<EolActividadConverter> findActividadByParams(String nombre, String periodo);
}
