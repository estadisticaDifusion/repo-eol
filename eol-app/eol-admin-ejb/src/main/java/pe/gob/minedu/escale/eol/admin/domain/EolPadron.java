/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author JMATAMOROS
 */
@Table(schema = "padron", name = "padron")
@Entity(name = "EolPadron")
public class EolPadron implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "COD_MOD")
    private String codMod;

    //@Id
    @Column(name = "ANEXO")
    private String anexo;

    //@EmbeddedId
    //private EolPadronPK id;
    @Column(name = "FORMAS")
    private String formas;
    
    @Column(name = "CODLOCAL")
    private String codlocal;
    
    @Column(name = "CEN_EDU")
    private String cenEdu;
    
    @Column(name = "DIR_CEN")
    private String dirCen;
    
    @Column(name = "TELEFONO")
    private String telefono;
    
    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "PAGWEB")
    private String pagweb;
    
    @Column(name = "DIRECTOR")
    private String director;
    @Column(name = "SITUADIR")
    private String situadir;

    //@Column(name = "CODOOII")
    //private String codooii;
    @Column(name = "CODIGEL")
    private String codigel;
    @Column(name = "TIPOIGEL")
    private String tipoigel;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "CODCCPP")
    private String codccpp;
    @Column(name = "COD_AREA")
    private String codArea;
    @Column(name = "GES_DEP")
    private String gesDep;
    @Column(name = "NIV_MOD")
    private String nivMod;
    @Column(name = "COD_TUR")
    private String codTur;
    @Column(name = "TIPSSEXO")
    private String tipssexo;
    @Column(name = "COD_CAR")
    private String codCar;
    @Column(name = "TIPOPROG")
    private String tipoprog;
    @Column(name = "GESTION")
    private String gestion;
    @Column(name = "AREA_SIG")
    private String areaSig;
    @Column(name = "LOCALIDAD")
    private String localidad;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "DRE")
    private String dre;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id_distrito", name = "CODGEO")
    private Distrito distrito;

    /*@ManyToOne
    @JoinColumn(referencedColumnName = "id_ugel", name = "CODOOII")
    private Ugel ugel;*/
    @ManyToOne
    @JoinColumn(referencedColumnName = "codigo", name = "CODOOII")
    private DreUgel dreUgel;

    @OneToMany(mappedBy = "eolPadron", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Collection<EolCenso> envios;

    public EolPadron() {
    }


    /*    public EolPadron(EolPadronPK id) {
        this.id=id;
        
    }
     */
    public String getFormas() {
        return formas;
    }

    public void setFormas(String formas) {
        this.formas = formas;
    }

    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    public String getDirCen() {
        return dirCen;
    }

    public void setDirCen(String dirCen) {
        this.dirCen = dirCen;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPagweb() {
        return pagweb;
    }

    public void setPagweb(String pagweb) {
        this.pagweb = pagweb;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getSituadir() {
        return situadir;
    }

    public void setSituadir(String situadir) {
        this.situadir = situadir;
    }

    public String getCodigel() {
        return codigel;
    }

    public void setCodigel(String codigel) {
        this.codigel = codigel;
    }

    public String getTipoigel() {
        return tipoigel;
    }

    public void setTipoigel(String tipoigel) {
        this.tipoigel = tipoigel;
    }

    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    public String getCodccpp() {
        return codccpp;
    }

    public void setCodccpp(String codccpp) {
        this.codccpp = codccpp;
    }

    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    public String getGesDep() {
        return gesDep;
    }

    public void setGesDep(String gesDep) {
        this.gesDep = gesDep;
    }

    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    public String getCodTur() {
        return codTur;
    }

    public void setCodTur(String codTur) {
        this.codTur = codTur;
    }

    public String getTipssexo() {
        return tipssexo;
    }

    public void setTipssexo(String tipssexo) {
        this.tipssexo = tipssexo;
    }

    public String getCodCar() {
        return codCar;
    }

    public void setCodCar(String codCar) {
        this.codCar = codCar;
    }

    public String getTipoprog() {
        return tipoprog;
    }

    public void setTipoprog(String tipoprog) {
        this.tipoprog = tipoprog;
    }

    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public Collection<EolCenso> getEnvios() {
        return envios;
    }

    public void setEnvios(Collection<EolCenso> envios) {
        this.envios = envios;
    }

    /**
     * @return the gestion
     */
    public String getGestion() {
        return gestion;
    }

    /**
     * @param gestion the gestion to set
     */
    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    /**
     * @return the areaSig
     */
    public String getAreaSig() {
        return areaSig;
    }

    /**
     * @param areaSig the areaSig to set
     */
    public void setAreaSig(String areaSig) {
        this.areaSig = areaSig;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    /**
     * @return the localidad
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * @param localidad the localidad to set
     */
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDre() {
        return dre;
    }

    public void setDre(String dre) {
        this.dre = dre;
    }

    /*
    public EolPadronPK getId() {
        return id;
    }
    public void setId(EolPadronPK id) {
        this.id = id;
    }
     */
    /**
     * @return the dreUgel
     */
    public DreUgel getDreUgel() {
        return dreUgel;
    }

    /**
     * @param dreUgel the dreUgel to set
     */
    public void setDreUgel(DreUgel dreUgel) {
        this.dreUgel = dreUgel;
    }

}
