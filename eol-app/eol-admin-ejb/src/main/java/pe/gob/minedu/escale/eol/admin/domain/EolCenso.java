/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "eol_censo")
public class EolCenso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NIV_MOD")
    private String nivMod;
    
    @Column(name = "NRO_ENVIO")
    private Integer nroEnvio;
    
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    
    @Column(name = "ULTIMO")
    private boolean ultimo;
    
    @Column(name = "ESTADO")
    private String estado;
    
    @Column(name = "CODOOII")
    private String codooii;
    
    @Column(name = "AREA")
    private String area;
    
    @Column(name = "GESTION")
    private String gestion;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id_distrito", name = "CODGEO")
    private Distrito distrito;

    @JoinColumns({
        @JoinColumn(name = "COD_MOD", referencedColumnName = "COD_MOD")
        ,
        @JoinColumn(name = "ANEXO", referencedColumnName = "ANEXO")})
    @ManyToOne
    private EolPadron eolPadron;

    @JoinColumn(name = "CODLOCAL", referencedColumnName = "CODLOCAL")
    @ManyToOne
    private EolLocal eolLocal;

    @JoinColumn(name = "ID_ACTIVIDAD", referencedColumnName = "ID")
    @ManyToOne(cascade = CascadeType.ALL)
    private EolActividad eolActividad;

    public EolCenso() {
    }

    public EolCenso(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    public Integer getNroEnvio() {
        return nroEnvio;
    }

    public void setNroEnvio(Integer nroEnvio) {
        this.nroEnvio = nroEnvio;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(boolean ultimo) {
        this.ultimo = ultimo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodooii() {
        return codooii;
    }

    public void setCodooii(String codooii) {
        this.codooii = codooii;
    }

    public EolPadron getEolPadron() {
        return eolPadron;
    }

    public void setEolPadron(EolPadron eolPadron) {
        this.eolPadron = eolPadron;
    }

    public EolLocal getEolLocal() {
        return eolLocal;
    }

    public void setEolLocal(EolLocal eolLocal) {
        this.eolLocal = eolLocal;
    }

    public EolActividad getEolActividad() {
        return eolActividad;
    }

    public void setEolActividad(EolActividad eolActividad) {
        this.eolActividad = eolActividad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EolCenso)) {
            return false;
        }
        EolCenso other = (EolCenso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.portlets.eol.domain.EolCenso[id=" + id + "]";
    }

    /**
     * @return the area
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * @return the gestion
     */
    public String getGestion() {
        return gestion;
    }

    /**
     * @param gestion the gestion to set
     */
    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

}
