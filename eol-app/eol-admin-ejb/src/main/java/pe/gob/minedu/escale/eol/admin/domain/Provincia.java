/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(schema = "padron", name = "provincias")
public class Provincia implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "id_provincia")
    private String idProvincia;

    @Column(name = "provincia")
    private String nombreProvincia;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id_region", name = "id_region")
    private Region region;

    @Column(name = "point_x")
    private double pointX;

    @Column(name = "point_y")
    private double pointY;

    @Column(name = "zoom")
    private int zoom;

    public Provincia() {
    }

    public Provincia(String idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(String idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getNombreProvincia() {
        return nombreProvincia;
    }

    public void setNombreProvincia(String nombreProvincia) {
        this.nombreProvincia = nombreProvincia;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public double getPointX() {
        return pointX;
    }

    public void setPointX(double pointX) {
        this.pointX = pointX;
    }

    public double getPointY() {
        return pointY;
    }

    public void setPointY(double pointY) {
        this.pointY = pointY;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Provincia other = (Provincia) obj;
        if ((this.idProvincia == null) ? (other.idProvincia != null) : !this.idProvincia.equals(other.idProvincia)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.idProvincia != null ? this.idProvincia.hashCode() : 0);
        return hash;
    }

}
