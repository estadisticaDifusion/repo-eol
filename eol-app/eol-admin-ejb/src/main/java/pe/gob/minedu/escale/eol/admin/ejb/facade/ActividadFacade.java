/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.ejb.facade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;


import pe.gob.minedu.escale.eol.admin.domain.EolActividad;
import pe.gob.minedu.escale.eol.admin.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.admin.ejb.ActividadLocal;
import pe.gob.minedu.escale.eol.converter.EolActividadConverter;


/**
 *
 * @author Ing Oscar Mateo
 * 
 */

@Stateless
public class ActividadFacade extends AbstractFacade<EolActividad> implements ActividadLocal {

	private Logger logger = Logger.getLogger(ActividadFacade.class);
	
	@PersistenceContext(unitName = "eol-adminPU")
	private EntityManager em;

	public ActividadFacade() {
		super(EolActividad.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public Collection<EolActividad> findActividades(String nombre, String periodo) {
		StringBuffer strQ = new StringBuffer("SELECT a FROM EolActividad a ");

		if (nombre != null && !nombre.isEmpty()) {
			strQ.append(" WHERE a.nombre=:nombre ");
		}

		if (periodo != null && !periodo.isEmpty() && strQ.toString().indexOf("WHERE") != -1) {
			strQ.append(" AND a.eolPeriodo.anio=:periodo ");
		} else if (periodo != null && !periodo.isEmpty()) {
			strQ.append(" WHERE a.eolPeriodo.anio=:periodo ");
		}

		strQ.append(" AND a.estado = '1' ORDER BY a.eolPeriodo.anio DESC, a.orden");  ///////////////////////// CAMBIADO /////////////
		Query q = em.createQuery(strQ.toString());

		if (nombre != null && !nombre.isEmpty()) {
			q.setParameter("nombre", nombre);
		}
		if (periodo != null && !periodo.isEmpty()) {
			q.setParameter("periodo", periodo);
		}

		return q.getResultList();
//        StringBuffer strQ= new StringBuffer("SELECT a FROM EolActividad a ");
//
//        if(nombre!=null && !nombre.isEmpty())
//            strQ.append(" WHERE a.nombre=:nombre ");
//
//        if(periodo!=null && !periodo.isEmpty() && strQ.toString().indexOf("WHERE")!=-1)
//               strQ.append(" AND a.eolPeriodo.anio=:periodo ");
//        else if(periodo!=null && !periodo.isEmpty())
//                strQ.append(" WHERE a.eolPeriodo.anio=:periodo ");
//
//
//        strQ.append(" ORDER BY a.eolPeriodo.anio DESC, a.orden");
//        Query q = em.createQuery(strQ.toString());
//
//        if(nombre!=null && !nombre.isEmpty())
//            q.setParameter("nombre", nombre);
//        if(periodo!=null && !periodo.isEmpty())
//            q.setParameter("periodo", periodo);
//
//
//        return q.getResultList();
	}

	// imendoza
	@SuppressWarnings("unchecked")
	public List<EolActividad> findAllActividades(String periodo) {
		StringBuffer strQ = new StringBuffer("SELECT a FROM EolActividad a ");

		if (periodo != null && !periodo.isEmpty())
			strQ.append(" WHERE a.eolPeriodo.anio=:periodo ");

		strQ.append(" ORDER BY a.eolPeriodo.anio DESC");
		Query q = em.createQuery(strQ.toString());

		if (periodo != null && !periodo.isEmpty())
			q.setParameter("periodo", periodo);

		// System.out.println("LISTA TOTAL ACT : "+strQ);
		return q.getResultList();
	}

	public EolActividad findById(int id) {
		EolActividad eol = new EolActividad();

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT * FROM estadistica.eol_actividad e ");
		sbQuery.append(" WHERE ID= ?1");
		Query query = getEntityManager().createNativeQuery(sbQuery.toString(), EolActividad.class);
		query = (id != 0) ? query.setParameter(1, id) : query;

		eol = (EolActividad) query.getSingleResult();
		// System.out.println("INDICUAL ACT : "+sbQuery);
		return eol;
//        StringBuffer strQ= new StringBuffer("SELECT a FROM EolActividad a ");
//        if(id>0)
//          strQ.append(" WHERE a.id=:id ");
//        Query q = em.createQuery(strQ.toString());
//        if(id>0)
//          q.setParameter("id", id);
//        return (EolActividad) q.getSingleResult();
	}

	public void update(EolActividad entity) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" UPDATE estadistica.eol_actividad SET ");
		sbQuery.append(" NOMBRE= ?1, ");
		sbQuery.append(" DESCRIPCION= ?2, ");
		sbQuery.append(" FECHA_INICIO= ?3, ");
		sbQuery.append(" FECHA_TERMINO= ?4, ");
		sbQuery.append(" FECHA_LIMITE= ?5, ");
		sbQuery.append(" FECHA_TERMINOSIGIED= ?6, ");
		sbQuery.append(" FECHA_LIMITESIGIED= ?7, ");
		sbQuery.append(" FECHA_TERMINOHUELGA= ?8, ");
		sbQuery.append(" FECHA_LIMITEHUELGA= ?9 ");

		sbQuery.append(" WHERE ID = ?10 ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query = (entity.getNombre() != null) ? query.setParameter(1, entity.getNombre()) : query;
		query = (entity.getDescripcion() != null) ? query.setParameter(2, entity.getDescripcion()) : query;
		query = (entity.getFechaInicio() != null) ? query.setParameter(3, entity.getFechaInicio()) : query;
		query = (entity.getFechaTermino() != null) ? query.setParameter(4, entity.getFechaTermino()) : query;
		query = (entity.getFechaLimite() != null) ? query.setParameter(5, entity.getFechaLimite()) : query;
		query = (entity.getFechaTerminosigied() != null) ? query.setParameter(6, entity.getFechaTerminosigied())
				: query;
		query = (entity.getFechaLimitesigied() != null) ? query.setParameter(7, entity.getFechaLimitesigied()) : query;
		query = (entity.getFechaTerminohuelga() != null) ? query.setParameter(8, entity.getFechaTerminohuelga())
				: query;
		query = (entity.getFechaLimitehuelga() != null) ? query.setParameter(9, entity.getFechaLimitehuelga()) : query;

		query = (entity.getId() != null) ? query.setParameter(10, entity.getId()) : query;

		// System.out.println("UPDATE : "+sbQuery);
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	public List<EolActividadConverter> findActividadByParams(String nombre, String periodo) {
		logger.info(":: ActividadFacade.findActividadByParams :: Starting execution...");
		List<EolActividadConverter> listActividadConvert = new ArrayList<EolActividadConverter>();
		Query q = getActividadList(nombre, periodo);
		List<EolActividad> listActividad = q.getResultList();
		logger.info("listActividad = " + listActividad);
		EolActividadConverter eolActividadConverter = null;
		for (EolActividad actividad : listActividad) {
			eolActividadConverter = new EolActividadConverter();
			eolActividadConverter.setId(actividad.getId());
			eolActividadConverter.setNombre(actividad.getNombre());
			eolActividadConverter.setDescripcion(actividad.getDescripcion());
			eolActividadConverter.setFechaInicio(actividad.getFechaInicio());
			eolActividadConverter.setFechaTermino(actividad.getFechaTermino());
			eolActividadConverter.setFechaTerminosigied(actividad.getFechaTerminosigied());
			eolActividadConverter.setFechaTerminohuelga(actividad.getFechaTerminohuelga());
			eolActividadConverter.setFechaLimite(actividad.getFechaLimite());
			eolActividadConverter.setFechaLimitesigied(actividad.getFechaLimitesigied());
			eolActividadConverter.setFechaLimitehuelga(actividad.getFechaLimitehuelga());
			eolActividadConverter.setEstadoFormato(actividad.getEstadoFormato());
			
			eolActividadConverter.setEstadoConstancia(actividad.getEstadoConstancia());
			eolActividadConverter.setEstadoCobertura(actividad.getEstadoCobertura());
			eolActividadConverter.setEstadoSituacion(actividad.getEstadoSituacion());
			eolActividadConverter.setEstadoOmisos(actividad.getEstadoOmisos());
			eolActividadConverter.setEstadoSie(actividad.getEstadoSie());

			eolActividadConverter.setUrlFormato(actividad.getUrlFormato());
			eolActividadConverter.setUrlOmisos(actividad.getUrlOmisos());
			eolActividadConverter.setUrlCobertura(actividad.getUrlCobertura());
			eolActividadConverter.setUrlConstancia(actividad.getUrlConstancia());
			eolActividadConverter.setUrlSituacion(actividad.getUrlSituacion());

			eolActividadConverter.setNombrePeriodo(actividad.getEolPeriodo().getAnio());
			listActividadConvert.add(eolActividadConverter);
		}
		logger.info(":: ActividadFacade.findActividadByParams :: Execution finish.");
		return listActividadConvert; //q.getResultList();

	}
	
	private Query getActividadList(String nombre, String periodo) {
		StringBuffer strQ = new StringBuffer("SELECT a FROM EolActividad a ");

		if (nombre != null && !nombre.isEmpty()) {
			strQ.append(" WHERE a.nombre=:nombre ");
		}

		if (periodo != null && !periodo.isEmpty() && strQ.toString().indexOf("WHERE") != -1) {
			strQ.append(" AND a.eolPeriodo.anio=:periodo ");
		} else if (periodo != null && !periodo.isEmpty()) {
			strQ.append(" WHERE a.eolPeriodo.anio=:periodo ");
		}

		strQ.append(" AND a.estado = '1' ORDER BY a.eolPeriodo.anio DESC, a.orden");  ///////////////////////// CAMBIADO /////////////
		Query q = em.createQuery(strQ.toString());

		if (nombre != null && !nombre.isEmpty()) {
			q.setParameter("nombre", nombre);
		}
		if (periodo != null && !periodo.isEmpty()) {
			q.setParameter("periodo", periodo);
		}
		return q;
	}


}
