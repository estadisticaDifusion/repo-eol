/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(schema = "padron", name = "dres")
public class DireccionRegional implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_dre")
	private String id;

	@Column(name = "dre")
	private String nombreDre;

	@Column(name = "point_x")
	private double pointX;

	@Column(name = "point_y")
	private double pointY;

	@Column(name = "zoom")
	private int zoom;

	public DireccionRegional() {
	}

	public DireccionRegional(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombreDre() {
		return nombreDre;
	}

	public void setNombreDre(String nombreDre) {
		this.nombreDre = nombreDre;
	}

	public double getPointX() {
		return pointX;
	}

	public void setPointX(double pointX) {
		this.pointX = pointX;
	}

	public double getPointY() {
		return pointY;
	}

	public void setPointY(double pointY) {
		this.pointY = pointY;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = zoom;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DireccionRegional other = (DireccionRegional) obj;
		if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

}
