/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "eol_cedula")
public class EolCedula implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "ARC_XLS")
    private String arcXls;

    @Column(name = "ARC_PDF")
    private String arcPdf;

    @Column(name = "SQL_BASE")
    private String sqlBase;

    @Column(name = "NIVEL")
    private String nivel;

    @Column(name = "ESTADO")
    private boolean estado;

    @Column(name = "VERSION")
    private String version;

    @JoinColumn(name = "ID_ACTIVIDAD", referencedColumnName = "ID")
    @ManyToOne(cascade = CascadeType.ALL)
    private EolActividad eolActividad;

    public EolCedula() {
    }

    public EolCedula(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArcXls() {
        return arcXls;
    }

    public void setArcXls(String arcXls) {
        this.arcXls = arcXls;
    }

    public String getArcPdf() {
        return arcPdf;
    }

    public void setArcPdf(String arcPdf) {
        this.arcPdf = arcPdf;
    }

    public String getSqlBase() {
        return sqlBase;
    }

    public void setSqlBase(String sqlBase) {
        this.sqlBase = sqlBase;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public EolActividad getEolActividad() {
        return eolActividad;
    }

    public void setEolActividad(EolActividad eolActividad) {
        this.eolActividad = eolActividad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EolCedula)) {
            return false;
        }
        EolCedula other = (EolCedula) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.portlets.eol.domain.EolCedula[id=" + id + "]";
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
