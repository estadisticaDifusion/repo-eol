/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.admin.domain.EolCedula;
import pe.gob.minedu.escale.eol.dto.estadistica.EolCedulaDTO;

/**
 *
 * @author omateo
 */
@Local
public interface CedulaLocal {

    public List<EolCedula> findByActividad(Integer idActividad);

    public List<EolCedula> findCedulas(Integer idActividad, String nombreActividad, String nivel, String estado, String periodo);
    
    List<EolCedulaDTO> findCedulaList(Integer idActividad, String nombreActividad, String nivel, String estado, String periodo);
}
