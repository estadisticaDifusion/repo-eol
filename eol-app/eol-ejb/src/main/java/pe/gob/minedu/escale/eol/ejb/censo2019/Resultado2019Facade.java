package pe.gob.minedu.escale.eol.ejb.censo2019;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.sql.DataSource;

import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Detalle;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Resp;
import pe.gob.minedu.escale.eol.ejb.AbstractFacade;

/**
 * 
 * @author WARODRIGUEZ
 *
 */
@Singleton
public class Resultado2019Facade extends AbstractFacade<Resultado2019Cabecera> implements Resultado2019Local {

	private static final Logger logger = Logger.getLogger(Resultado2019Facade.class.getName());

	@PersistenceContext(unitName = "eol-PUAux")
	private EntityManager em;

	@Resource(lookup = "java:jboss/jdbc/eolDS")
	private DataSource dataSource;

	public static final String CEDULA_RESULTADO = "CEDULA-RESULTADO";

	public Resultado2019Facade() {
		super(Resultado2019Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public void create(Resultado2019Cabecera cedula) {
		try {
			String sql = "UPDATE Resultado2019Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo and a.nroced=:nroced ";
			Query query = em.createQuery(sql);
			query.setParameter("codMod", cedula.getCodMod());
			query.setParameter("anexo", cedula.getAnexo());
			query.setParameter("nroced", cedula.getNroced());

			query.executeUpdate();

			if (cedula.getResultado2019RespList() != null) {
				for (Resultado2019Resp resp : cedula.getResultado2019RespList()) {
					resp.setResultado2019Cabecera(cedula);
				}
			}

			cedula.setUltimo(true);
			if (cedula.getFechaEnvio() == null)
				cedula.setFechaEnvio(new Date());

			em.persist(cedula);

			em.flush();

		} catch (PersistenceException e) {
			logger.info("ERROR POR PERSISTENCE " + e.getStackTrace());
		}
	}

	//@SuppressWarnings({ "unchecked", "unused" })
	@SuppressWarnings("unchecked")
	public void updatedetail(Resultado2019Cabecera ceddiciembre, Resultado2019Cabecera cedfebrero) {
//		 Set<String> keys = cedfebrero.getDetalle().keySet();
//		 Set<String> keys =
//		 cedfebrero.getResultado2019ResultadoList().keySet();
//		 for (Iterator<String> it = keys.iterator(); it.hasNext();) {
//		 String key = it.next();
//		 Resultado2019Detalle det =
//		 cedfebrero.getResultado2019ResultadoList().get(key);
//		 det.setCedula(ceddiciembre);
//		
//		
//		 em.persist(det);
//		 em.flush();
//		
//		
//		
//		 Query q = null;
//		
//		 try {
//		 String query = "CALL actualizar_datos_resultado_2019(?,?,?,?,?)";
//		 q = (Query) em.createNativeQuery(query);
//		
//		 q.setParameter("1", ceddiciembre.getIdEnvio());
//		 q.setParameter("2", key);
//		 q.setParameter("3", ceddiciembre.getCodMod());
//		 q.setParameter("4", ceddiciembre.getAnexo());
//		 q.setParameter("5", ceddiciembre.getNivMod());
//		
//		 List<Object[]> listObj = q.getResultList();
//		
//		 } catch (Exception e) {
//		 e.printStackTrace();
//		 }
//		
//		 }
		

		for (Resultado2019Detalle mat : cedfebrero.getResultado2019ResultadoList()) {
			mat.setCedula(ceddiciembre);

			em.persist(mat);
			em.flush();

			Query q = null;

			try {
				String query = "CALL actualizar_datos_resultado_2019(?,?,?,?,?)";
				q = (Query) em.createNativeQuery(query);
				
				q.setParameter(1, ceddiciembre.getIdEnvio());
				q.setParameter(2, cedfebrero.getResultado2019ResultadoList().get(0));
				q.setParameter(3, ceddiciembre.getCodMod());
				q.setParameter(4, ceddiciembre.getAnexo());
				q.setParameter(5, ceddiciembre.getNivMod());

				List<Object[]> listObj = q.getResultList();

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public Resultado2019Cabecera findByCodModAnNiv(String codmod, String anexo, String nivmod) {
		Query q = em.createQuery(
				"SELECT R FROM Resultado2019Cabecera R WHERE R.codMod=:codmod AND R.anexo=:anexo AND R.nivMod=:nivmod AND R.ultimo=true");
		q.setParameter("codmod", codmod);
		q.setParameter("anexo", anexo);
		q.setParameter("nivmod", nivmod);
		try {
			return (Resultado2019Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Object[]> verificarObservacionCedula(String codmod, String anexo, String nroced, Integer totalH,
			Integer totalM, Integer dato01h, Integer dato01m, Integer dato02h, Integer dato02m, Integer dato03h,
			Integer dato03m, Integer dato04h, Integer dato04m, Integer dato05h, Integer dato05m, Integer dato06h,
			Integer dato06m, Integer dato07h, Integer dato07m, Integer dato08h, Integer dato08m, Integer dato09h,
			Integer dato09m, Integer dato10h, Integer dato10m) {
		Query q = null;

		try {


			String query = "CALL verificar_consistencia_2019(:codmod, :anexo, :nroced, :totalH, :totalM, :dato01h, :dato01m, :dato02h, :dato02m, :dato03h, :dato03m, :dato04h, :dato04m, :dato05h, :dato05m, :dato06h, :dato06m, :dato07h, :dato07m, :dato08h, :dato08m, :dato09h, :dato09m, :dato10h, :dato10m)";
			q = (Query) em.createNativeQuery(query);

			q.setParameter("codmod", codmod);
			q.setParameter("anexo", anexo);
			q.setParameter("nroced", nroced);
			q.setParameter("totalH", totalH);
			q.setParameter("totalM", totalM);
			q.setParameter("dato01h", dato01h);
			q.setParameter("dato01m", dato01m);
			q.setParameter("dato02h", dato02h);
			q.setParameter("dato02m", dato02m);
			q.setParameter("dato03h", dato03h);
			q.setParameter("dato03m", dato03m);
			q.setParameter("dato04h", dato04h);
			q.setParameter("dato04m", dato04m);
			q.setParameter("dato05h", dato05h);
			q.setParameter("dato05m", dato05m);
			q.setParameter("dato06h", dato06h);
			q.setParameter("dato06m", dato06m);
			q.setParameter("dato07h", dato07h);
			q.setParameter("dato07m", dato07m);
			q.setParameter("dato08h", dato08h);
			q.setParameter("dato08m", dato08m);
			q.setParameter("dato09h", dato09h);
			q.setParameter("dato09m", dato09m);
			q.setParameter("dato10h", dato10h);
			q.setParameter("dato10m", dato10m);
//		    q.setParameter("1", codmod);
//            q.setParameter("2", anexo);
//            q.setParameter("3", nroced);
//            q.setParameter("4", totalH);
//            q.setParameter("5", totalM);
//            q.setParameter("6", dato01h);
//            q.setParameter("7", dato01m);
//            q.setParameter("8", dato02h);
//            q.setParameter("9", dato02m);
//            q.setParameter("10", dato03h);
//            q.setParameter("11", dato03m);
//            q.setParameter("12", dato04h);
//            q.setParameter("13", dato04m);
//            q.setParameter("14", dato05h);
//            q.setParameter("15", dato05m);
//            q.setParameter("16", dato06h);
//            q.setParameter("17", dato06m);
//            q.setParameter("18", dato07h);
//            q.setParameter("19", dato07m);
//            q.setParameter("20", dato08h);
//            q.setParameter("21", dato08m);
//            q.setParameter("22", dato09h);
//            q.setParameter("23", dato09m);
//            q.setParameter("24", dato10h);
//            q.setParameter("25", dato10m);

			List<Object[]> listObj = q.getResultList();

			return listObj;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// para que se visualize en el tablero la descarga del PDF se necesitaria enviar
	// el ID de envio de resultado cabecera
	@SuppressWarnings("unchecked")
	public List<Object[]> registrarEnvioResultadoIdentificacion(String codmod, String anexo, String nivmod) {
		Query q = null;

		try {
			String query = "CALL registrar_envio_resultado_2019(?,?,?)";
			q = (Query) em.createNativeQuery(query);

			q.setParameter("1", codmod);
			q.setParameter("2", anexo);
			q.setParameter("3", nivmod);

			List<Object[]> listObj = q.getResultList();

			return listObj;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Resultado2019Cabecera findCedNivel(Long idEnvio) {
		Query q = em.createQuery("SELECT R FROM Resultado2019Cabecera R WHERE R.idEnvio=:idEnvio");
		q.setParameter("idEnvio", idEnvio);
		try {
			return (Resultado2019Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
