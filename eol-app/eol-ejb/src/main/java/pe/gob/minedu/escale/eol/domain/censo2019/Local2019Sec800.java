package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2019_sec800")
public class Local2019Sec800 implements Serializable  {
private static final long serialVersionUID = 1L;
	 
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Basic(optional = false)
@Column(name = "ID_ENVIO")
private Long idEnvio;

@Column(name = "TIPDATO")
private String tipdato;
@Column(name = "P801_1")
private String p8011;
@Column(name = "P801_2")
private String p8012;
@Column(name = "P801_3")
private String p8013;
@Column(name = "P801_4")
private String p8014;
@Column(name = "P801_5")
private String p8015;
@Column(name = "P801_6")
private String p8016;
@Column(name = "P801_7")
private String p8017;
@Column(name = "P801_8")
private String p8018;
@Column(name = "P801_9")
private String p8019;

@JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
@ManyToOne(optional = false)
private Local2019Cabecera local2019Cabecera;

public Local2019Sec800() {
}

public Local2019Sec800(Long idEnvio) {
    this.idEnvio = idEnvio;
}

public Long getIdEnvio() {
    return idEnvio;
}

public void setIdEnvio(Long idEnvio) {
    this.idEnvio = idEnvio;
}
@XmlElement(name = "TIPDATO")
public String getTipdato() {
    return tipdato;
}

public void setTipdato(String tipdato) {
    this.tipdato = tipdato;
}
@XmlElement(name = "P801_1")
public String getP8011() {
    return p8011;
}

public void setP8011(String p8011) {
    this.p8011 = p8011;
}
@XmlElement(name = "P801_2")
public String getP8012() {
    return p8012;
}

public void setP8012(String p8012) {
    this.p8012 = p8012;
}
@XmlElement(name = "P801_3")
public String getP8013() {
    return p8013;
}

public void setP8013(String p8013) {
    this.p8013 = p8013;
}
@XmlElement(name = "P801_4")
public String getP8014() {
    return p8014;
}

public void setP8014(String p8014) {
    this.p8014 = p8014;
}
@XmlElement(name = "P801_5")
public String getP8015() {
    return p8015;
}

public void setP8015(String p8015) {
    this.p8015 = p8015;
}
@XmlElement(name = "P801_6")
public String getP8016() {
    return p8016;
}

public void setP8016(String p8016) {
    this.p8016 = p8016;
}
@XmlElement(name = "P801_7")
public String getP8017() {
    return p8017;
}

public void setP8017(String p8017) {
    this.p8017 = p8017;
}
@XmlElement(name = "P801_8")
public String getP8018() {
    return p8018;
}

public void setP8018(String p8018) {
    this.p8018 = p8018;
}
@XmlElement(name = "P801_9")
public String getP8019() {
    return p8019;
}

public void setP8019(String p8019) {
    this.p8019 = p8019;
}

@XmlTransient
public Local2019Cabecera getLocal2019Cabecera() {
    return local2019Cabecera;
}

public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
    this.local2019Cabecera = local2019Cabecera;
}

@Override
public int hashCode() {
    int hash = 0;
    hash += (idEnvio != null ? idEnvio.hashCode() : 0);
    return hash;
}

@Override
public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Local2019Sec800)) {
        return false;
    }
    Local2019Sec800 other = (Local2019Sec800) object;
    if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
        return false;
    }
    return true;
}

@Override
public String toString() {
    return "pe.gob.minedu.entidad2019.Local2019Sec800[idEnvio=" + idEnvio + "]";
} 
	
	
}
