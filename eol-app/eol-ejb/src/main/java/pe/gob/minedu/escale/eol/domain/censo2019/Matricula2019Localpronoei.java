/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "matricula2019_localpronoei")    
public class Matricula2019Localpronoei implements Serializable {
    private static final long serialVersionUID = 1L;
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P601")
    private String p601;
    @Column(name = "P602")
    private String p602;
    @Column(name = "P602_ESP")
    private String p602Esp;
    @Column(name = "P603")
    private String p603;
    @Column(name = "P603_SI")
    private String p603Si;
    @Column(name = "P604")
    private String p604;
    @Column(name = "P604_CM")
    private String p604Cm;
    @Column(name = "P604_NM")
    private String p604Nm;
    @Column(name = "P604_ESP")
    private String p604Esp;
    @Column(name = "P605")
    private String p605;
    @Column(name = "P605_SUM")
    private String p605Sum;
    @Column(name = "P606")
    private String p606;
    @Column(name = "P606_SUM")
    private String p606Sum;
    @Column(name = "P606_ESP")
    private String p606Esp;
    @Column(name = "P607")
    private String p607;
    @Column(name = "P608")
    private String p608;
    @Column(name = "P609")
    private String p609;
    @Column(name = "P609_ESP")
    private String p609Esp;
    @Column(name = "P610")
    private String p610;
    @Column(name = "P610_ESP")
    private String p610Esp;
    @Column(name = "P611")
    private String p611;
    @Column(name = "P611_ESP")
    private String p611Esp;
    @Column(name = "P612")
    private String p612;
    @Column(name = "P613")
    private String p613;
    @Column(name = "P614")
    private String p614;
    @Column(name = "P615")
    private String p615;
    @Column(name = "P616")
    private String p616;
    @Column(name = "P618")
    private Integer p618;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2019Cabecera matricula2019Cabecera;

    public Matricula2019Localpronoei() {
    }

    public Matricula2019Localpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    
    @XmlElement(name = "P601")
    public String getP601() {
		return p601;
	}

	public void setP601(String p601) {
		this.p601 = p601;
	}

    @XmlElement(name = "P602")
    public String getP602() {
        return p602;
    }

    public void setP602(String p602) {
        this.p602 = p602;
    }

    @XmlElement(name = "P603")
    public String getP603() {
        return p603;
    }

    public void setP603(String p603) {
        this.p603 = p603;
    }

    @XmlElement(name = "P604")
    public String getP604() {
        return p604;
    }

    public void setP604(String p604) {
        this.p604 = p604;
    }

    @XmlElement(name = "P605")
    public String getP605() {
        return p605;
    }

    public void setP605(String p605) {
        this.p605 = p605;
    }



    @XmlElement(name = "P606")
    public String getP606() {
        return p606;
    }

    public void setP606(String p606) {
        this.p606 = p606;
    }

    @XmlElement(name = "P606_SUM")
    public String getP606Sum() {
        return p606Sum;
    }

    public void setP606Sum(String p606Sum) {
        this.p606Sum = p606Sum;
    }

    @XmlElement(name = "P607")
    public String getP607() {
        return p607;
    }

    public void setP607(String p607) {
        this.p607 = p607;
    }



    @XmlElement(name = "P608")
    public String getP608() {
        return p608;
    }

    public void setP608(String p608) {
        this.p608 = p608;
    }

    @XmlElement(name = "P609")
    public String getP609() {
        return p609;
    }

    public void setP609(String p609) {
        this.p609 = p609;
    }

    @XmlElement(name = "P610")
    public String getP610() {
        return p610;
    }

    public void setP610(String p610) {
        this.p610 = p610;
    }

    @XmlElement(name = "P611")
    public String getP611() {
        return p611;
    }

    public void setP611(String p611) {
        this.p611 = p611;
    }

    @XmlElement(name = "P611_ESP")
    public String getP611Esp() {
        return p611Esp;
    }

    public void setP611Esp(String p611Esp) {
        this.p611Esp = p611Esp;
    }

    @XmlElement(name = "P612")
    public String getP612() {
        return p612;
    }

    public void setP612(String p612) {
        this.p612 = p612;
    }


    @XmlElement(name = "P613")
    public String getP613() {
        return p613;
    }

    public void setP613(String p613) {
        this.p613 = p613;
    }


    @XmlElement(name = "P614")
    public String getP614() {
        return p614;
    }

    public void setP614(String p614) {
        this.p614 = p614;
    }

    @XmlElement(name = "P615")
    public String getP615() {
        return p615;
    }

    public void setP615(String p615) {
        this.p615 = p615;
    }

    @XmlElement(name = "P616")
    public String getP616() {
        return p616;
    }

    public void setP616(String p616) {
        this.p616 = p616;
    }


    @XmlElement(name = "P618")
    public Integer getP618() {
        return p618;
    }

    public void setP618(Integer p618) {
        this.p618 = p618;
    }


    
    
    

	@XmlElement(name = "P602_ESP")
	public String getP602Esp() {
		return p602Esp;
	}

	public void setP602Esp(String p602Esp) {
		this.p602Esp = p602Esp;
	}
	@XmlElement(name = "P603_SI")
	public String getP603Si() {
		return p603Si;
	}

	public void setP603Si(String p603Si) {
		this.p603Si = p603Si;
	}
	@XmlElement(name = "P604_CM")
	public String getP604Cm() {
		return p604Cm;
	}

	public void setP604Cm(String p604Cm) {
		this.p604Cm = p604Cm;
	}
	@XmlElement(name = "P604_NM")
	public String getP604Nm() {
		return p604Nm;
	}

	public void setP604Nm(String p604Nm) {
		this.p604Nm = p604Nm;
	}
	@XmlElement(name = "P604_ESP")
	public String getP604Esp() {
		return p604Esp;
	}

	public void setP604Esp(String p604Esp) {
		this.p604Esp = p604Esp;
	}
	@XmlElement(name = "P605_SUM")
	public String getP605Sum() {
		return p605Sum;
	}

	public void setP605Sum(String p605Sum) {
		this.p605Sum = p605Sum;
	}
	@XmlElement(name = "P606_ESP")
	public String getP606Esp() {
		return p606Esp;
	}

	public void setP606Esp(String p606Esp) {
		this.p606Esp = p606Esp;
	}
	@XmlElement(name = "P609_ESP")
	public String getP609Esp() {
		return p609Esp;
	}

	public void setP609Esp(String p609Esp) {
		this.p609Esp = p609Esp;
	}
	@XmlElement(name = "P610_ESP")
	public String getP610Esp() {
		return p610Esp;
	}

	public void setP610Esp(String p610Esp) {
		this.p610Esp = p610Esp;
	}

	@XmlTransient
    public Matricula2019Cabecera getMatricula2019Cabecera() {
        return matricula2019Cabecera;
    }

    public void setMatricula2019Cabecera(Matricula2019Cabecera matricula2019Cabecera) {
        this.matricula2019Cabecera = matricula2019Cabecera;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2019Localpronoei)) {
            return false;
        }
        Matricula2019Localpronoei other = (Matricula2019Localpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

 
    
    
    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Localpronoei[idEnvio=" + idEnvio + "]";
       
    }
 

}
