/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.client.constantes;

/**
 *
 * @author DSILVA 
 */
public interface Constantes {

    public static final String BASE_URI_EOL_WS = "http://localhost:8080/eol-ws/rest/";
    public static final String BASE_URI_PADRON_WS = "http://localhost:8080/eol-padron-ws/api/resources/";
    public static final String BASE_URI_EOL_ADMIN = "http://localhost:8080/eol-admin-ws/api/resources/";

    public static final String BASE_URI_EOL_WS_API = "http://localhost:8080/eol-ws/api/resources/";
	
    public static final String BASE_URI = "http://escale.minedu.gob.pe/padron/rest/";
    //public static final String BASE_URI = "http://localhost:8080/padron/rest/";
    //public static final String BASE_URI_EOL_ADMIN = "http://localhost:8080/eol-admin/rest/";
    
    //Escale1
    //  public static final String BASE_URI = "http://escale1.minedu.gob.pe/padron/rest/";
    //  public static final String BASE_URI_EOL_ADMIN = "http://escale1.minedu.gob.pe/eol-admin/rest/";
    //  public static final String BASE_URI_EOL_WS  = "http://escale1.minedu.gob.pe/eol-ws/rest/";
}
