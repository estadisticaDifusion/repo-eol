/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.xml;

/**
 *
 * @author JMATAMOROS
 */
public class EstadoPadron {
    public static  String ESTADO_ACTIVO="1";
    public static  String ESTADO_NO_HABIDO="2";
    public static  String ESTADO_CERRADO="3";
    public static  String ESTADO_INACTIVO="4";
    public static  String ESTADO_CODMOD_NO_VIGENTE="5";
    

    private String idCodigo;
    private String valor;

    public String getIdCodigo() {
        return idCodigo;
    }

    public void setIdCodigo(String idCodigo) {
        this.idCodigo = idCodigo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
