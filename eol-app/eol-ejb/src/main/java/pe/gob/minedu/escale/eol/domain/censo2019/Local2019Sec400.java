/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "local2019_sec400")
public class Local2019Sec400 implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;

	@Column(name = "P400_NRO")
	private Integer p400Nro;
	@Column(name = "P400_1")
	private String p4001;
	@Column(name = "P400_2")
	private Integer p4002;
	@Column(name = "P400_31")
	private String p40031;
	@Column(name = "P400_32")
	private String p40032;
	@Column(name = "P400_41")
	private BigDecimal p40041;
	@Column(name = "P400_42")
	private BigDecimal p40042;
	@Column(name = "P400_43")
	private BigDecimal p40043;
	@Column(name = "P400_5")
	private String p4005;
	@Column(name = "P400_6")
	private String p4006;
	@Column(name = "P400_7")
	private String p4007;
	@Column(name = "P400_81PA")
	private String p40081pa;
	@Column(name = "P400_82PA")
	private String p40082pa;
	@Column(name = "P400_83PA")
	private String p40083pa;
	@Column(name = "P400_81TE")
	private String p40081te;
	@Column(name = "P400_82TE")
	private String p40082te;
	@Column(name = "P400_83TE")
	private String p40083te;
	@Column(name = "P400_81PI")
	private String p40081pi;
	@Column(name = "P400_82PI")
	private String p40082pi;
	@Column(name = "P400_83PI")
	private String p40083pi;
	@Column(name = "P400_91PU")
	private String p40091pu;
	@Column(name = "P400_92PU")
	private String p40092pu;
	@Column(name = "P400_93PU")
	private String p40093pu;
	@Column(name = "P400_91VE")
	private String p40091ve;
	@Column(name = "P400_92VE")
	private String p40092ve;
	@Column(name = "P400_93VE")
	private String p40093ve;
	@Column(name = "P400_91IE")
	private String p40091ie;
	@Column(name = "P400_92IE")
	private String p40092ie;
	@Column(name = "P400_101IS")
	private String p400101is;
	@Column(name = "P400_102IS")
	private String p400102is;
	@Column(name = "P400_101AS")
	private String p400101as;
	@Column(name = "P400_102AS")
	private String p400102as;
	@Column(name = "P400_111PC")
	private String p400111pc;
	@Column(name = "P400_112PC")
	private Integer p400112pc;
	@Column(name = "P400_113PC")
	private Integer p400113pc;
	@Column(name = "P400_114PC")
	private Integer p400114pc;
	@Column(name = "P400_115PC")
	private Integer p400115pc;
	@Column(name = "P400_111LA")
	private String p400111la;
	@Column(name = "P400_112LA")
	private Integer p400112la;
	@Column(name = "P400_113LA")
	private Integer p400113la;
	@Column(name = "P400_114LA")
	private Integer p400114la;
	@Column(name = "P400_115LA")
	private Integer p400115la;
	@Column(name = "P400_111PR")
	private String p400111pr;
	@Column(name = "P400_112PR")
	private Integer p400112pr;
	@Column(name = "P400_113PR")
	private Integer p400113pr;
	@Column(name = "P400_114PR")
	private Integer p400114pr;
	@Column(name = "P400_115PR")
	private Integer p400115pr;

	@JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	@ManyToOne
	private Local2019Cabecera local2019Cabecera;

	public Local2019Sec400() {
	}

	public Local2019Sec400(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "P400_NRO")
	public Integer getP400Nro() {
		return p400Nro;
	}

	public void setP400Nro(Integer p400Nro) {
		this.p400Nro = p400Nro;
	}

	@XmlElement(name = "P400_1")
	public String getP4001() {
		return p4001;
	}

	public void setP4001(String p4001) {
		this.p4001 = p4001;
	}

	//////////////////////////////////////////////////

	@XmlElement(name = "P400_2")
	public Integer getP4002() {
		return p4002;
	}

	public void setP4002(Integer p4002) {
		this.p4002 = p4002;
	}

	@XmlElement(name = "P400_31")
	public String getP40031() {
		return p40031;
	}

	public void setP40031(String p40031) {
		this.p40031 = p40031;
	}

	@XmlElement(name = "P400_32")
	public String getP40032() {
		return p40032;
	}

	public void setP40032(String p40032) {
		this.p40032 = p40032;
	}

	@XmlElement(name = "P400_41")
	public BigDecimal getP40041() {
		return p40041;
	}

	public void setP40041(BigDecimal p40041) {
		this.p40041 = p40041;
	}

	@XmlElement(name = "P400_42")
	public BigDecimal getP40042() {
		return p40042;
	}

	public void setP40042(BigDecimal p40042) {
		this.p40042 = p40042;
	}

	@XmlElement(name = "P400_43")
	public BigDecimal getP40043() {
		return p40043;
	}

	public void setP40043(BigDecimal p40043) {
		this.p40043 = p40043;
	}

	@XmlElement(name = "P400_5")
	public String getP4005() {
		return p4005;
	}

	public void setP4005(String p4005) {
		this.p4005 = p4005;
	}

	@XmlElement(name = "P400_6")
	public String getP4006() {
		return p4006;
	}

	public void setP4006(String p4006) {
		this.p4006 = p4006;
	}

	@XmlElement(name = "P400_7")
	public String getP4007() {
		return p4007;
	}

	public void setP4007(String p4007) {
		this.p4007 = p4007;
	}

	@XmlElement(name = "P400_81PA")
	public String getP40081pa() {
		return p40081pa;
	}

	public void setP40081pa(String p40081pa) {
		this.p40081pa = p40081pa;
	}

	@XmlElement(name = "P400_82PA")
	public String getP40082pa() {
		return p40082pa;
	}

	public void setP40082pa(String p40082pa) {
		this.p40082pa = p40082pa;
	}

	@XmlElement(name = "P400_83PA")
	public String getP40083pa() {
		return p40083pa;
	}

	public void setP40083pa(String p40083pa) {
		this.p40083pa = p40083pa;
	}

	@XmlElement(name = "P400_81TE")
	public String getP40081te() {
		return p40081te;
	}

	public void setP40081te(String p40081te) {
		this.p40081te = p40081te;
	}

	@XmlElement(name = "P400_82TE")
	public String getP40082te() {
		return p40082te;
	}

	public void setP40082te(String p40082te) {
		this.p40082te = p40082te;
	}

	@XmlElement(name = "P400_83TE")
	public String getP40083te() {
		return p40083te;
	}

	public void setP40083te(String p40083te) {
		this.p40083te = p40083te;
	}

	@XmlElement(name = "P400_81PI")
	public String getP40081pi() {
		return p40081pi;
	}

	public void setP40081pi(String p40081pi) {
		this.p40081pi = p40081pi;
	}

	@XmlElement(name = "P400_82PI")
	public String getP40082pi() {
		return p40082pi;
	}

	public void setP40082pi(String p40082pi) {
		this.p40082pi = p40082pi;
	}

	@XmlElement(name = "P400_83PI")
	public String getP40083pi() {
		return p40083pi;
	}

	public void setP40083pi(String p40083pi) {
		this.p40083pi = p40083pi;
	}

	@XmlElement(name = "P400_91PU")
	public String getP40091pu() {
		return p40091pu;
	}

	public void setP40091pu(String p40091pu) {
		this.p40091pu = p40091pu;
	}

	@XmlElement(name = "P400_92PU")
	public String getP40092pu() {
		return p40092pu;
	}

	public void setP40092pu(String p40092pu) {
		this.p40092pu = p40092pu;
	}

	@XmlElement(name = "P400_93PU")
	public String getP40093pu() {
		return p40093pu;
	}

	public void setP40093pu(String p40093pu) {
		this.p40093pu = p40093pu;
	}

	@XmlElement(name = "P400_91VE")
	public String getP40091ve() {
		return p40091ve;
	}

	public void setP40091ve(String p40091ve) {
		this.p40091ve = p40091ve;
	}

	@XmlElement(name = "P400_92VE")
	public String getP40092ve() {
		return p40092ve;
	}

	public void setP40092ve(String p40092ve) {
		this.p40092ve = p40092ve;
	}

	@XmlElement(name = "P400_93VE")
	public String getP40093ve() {
		return p40093ve;
	}

	public void setP40093ve(String p40093ve) {
		this.p40093ve = p40093ve;
	}

	@XmlElement(name = "P400_91IE")
	public String getP40091ie() {
		return p40091ie;
	}

	public void setP40091ie(String p40091ie) {
		this.p40091ie = p40091ie;
	}

	@XmlElement(name = "P400_92IE")
	public String getP40092ie() {
		return p40092ie;
	}

	public void setP40092ie(String p40092ie) {
		this.p40092ie = p40092ie;
	}

	@XmlElement(name = "P400_101IS")
	public String getP400101is() {
		return p400101is;
	}

	public void setP400101is(String p400101is) {
		this.p400101is = p400101is;
	}

	@XmlElement(name = "P400_102IS")
	public String getP400102is() {
		return p400102is;
	}

	public void setP400102is(String p400102is) {
		this.p400102is = p400102is;
	}

	@XmlElement(name = "P400_111PC")
	public String getP400111pc() {
		return p400111pc;
	}

	public void setP400111pc(String p400111pc) {
		this.p400111pc = p400111pc;
	}

	@XmlElement(name = "P400_112PC")
	public Integer getP400112pc() {
		return p400112pc;
	}

	public void setP400112pc(Integer p400112pc) {
		this.p400112pc = p400112pc;
	}

	@XmlElement(name = "P400_113PC")
	public Integer getP400113pc() {
		return p400113pc;
	}

	public void setP400113pc(Integer p400113pc) {
		this.p400113pc = p400113pc;
	}

	@XmlElement(name = "P400_114PC")
	public Integer getP400114pc() {
		return p400114pc;
	}

	public void setP400114pc(Integer p400114pc) {
		this.p400114pc = p400114pc;
	}

	@XmlElement(name = "P400_115PC")
	public Integer getP400115pc() {
		return p400115pc;
	}

	public void setP400115pc(Integer p400115pc) {
		this.p400115pc = p400115pc;
	}

	@XmlElement(name = "P400_111LA")
	public String getP400111la() {
		return p400111la;
	}

	public void setP400111la(String p400111la) {
		this.p400111la = p400111la;
	}

	@XmlElement(name = "P400_112LA")
	public Integer getP400112la() {
		return p400112la;
	}

	public void setP400112la(Integer p400112la) {
		this.p400112la = p400112la;
	}

	@XmlElement(name = "P400_113LA")
	public Integer getP400113la() {
		return p400113la;
	}

	public void setP400113la(Integer p400113la) {
		this.p400113la = p400113la;
	}

	@XmlElement(name = "P400_114LA")
	public Integer getP400114la() {
		return p400114la;
	}

	public void setP400114la(Integer p400114la) {
		this.p400114la = p400114la;
	}

	@XmlElement(name = "P400_115LA")
	public Integer getP400115la() {
		return p400115la;
	}

	public void setP400115la(Integer p400115la) {
		this.p400115la = p400115la;
	}

	@XmlElement(name = "P400_111PR")
	public String getP400111pr() {
		return p400111pr;
	}

	public void setP400111pr(String p400111pr) {
		this.p400111pr = p400111pr;
	}

	@XmlElement(name = "P400_112PR")
	public Integer getP400112pr() {
		return p400112pr;
	}

	public void setP400112pr(Integer p400112pr) {
		this.p400112pr = p400112pr;
	}

	@XmlElement(name = "P400_113PR")
	public Integer getP400113pr() {
		return p400113pr;
	}

	public void setP400113pr(Integer p400113pr) {
		this.p400113pr = p400113pr;
	}

	@XmlElement(name = "P400_114PR")
	public Integer getP400114pr() {
		return p400114pr;
	}

	public void setP400114pr(Integer p400114pr) {
		this.p400114pr = p400114pr;
	}

	@XmlElement(name = "P400_115PR")
	public Integer getP400115pr() {
		return p400115pr;
	}

	public void setP400115pr(Integer p400115pr) {
		this.p400115pr = p400115pr;
	}

	@XmlElement(name = "P400_101AS")
	public String getP400101as() {
		return p400101as;
	}

	public void setP400101as(String p400101as) {
		this.p400101as = p400101as;
	}

	@XmlElement(name = "P400_102AS")
	public String getP400102as() {
		return p400102as;
	}

	public void setP400102as(String p400102as) {
		this.p400102as = p400102as;
	}

	@XmlTransient
	public Local2019Cabecera getLocal2019Cabecera() {
		return local2019Cabecera;
	}

	public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
		this.local2019Cabecera = local2019Cabecera;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Local2019Sec400)) {
			return false;
		}
		Local2019Sec400 other = (Local2019Sec400) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Local2019Sec400 [idEnvio=" + idEnvio + "]";
	}

}
