package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "matricula2019_sede")
public class Matricula2019Sede implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;
	@Basic(optional = false)
	@Column(name = "NUMERO")
	private String numero;
	@Basic(optional = false)
	@Column(name = "NROCED")
	private String nroced;
	@Basic(optional = false)
	@Column(name = "CUADRO")
	private String cuadro;
	@Column(name = "CODSEDE")
	private String codsede;
	@Basic(optional = false)
	@Column(name = "NOM_SEDE")
	private String nomSede;
	@Column(name = "LICENCIA")
	private String licencia;
	@Column(name = "NO_SITUA")
	private String noSitua;
	@Column(name = "UBI_GEO")
	private String ubiGeo;
	@Column(name = "NOM_DPTO")
	private String nomDpto;
	@Column(name = "NOM_PROV")
	private String nomProv;
	@Column(name = "NOM_DIST")
	private String nomDist;
	@Column(name = "DIR_SEDE")
	private String dirSede;
	@JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	@ManyToOne(optional = false)
	private Matricula2019Cabecera matricula2019Cabecera;

	public Matricula2019Sede() {
	}

	public Matricula2019Sede(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Matricula2019Sede(Long idEnvio, String numero, String nroced, String cuadro, String nomSede) {
		this.idEnvio = idEnvio;
		this.numero = numero;
		this.nroced = nroced;
		this.cuadro = cuadro;
		this.nomSede = nomSede;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "NUMERO")
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@XmlElement(name = "NROCED")
	public String getNroced() {
		return nroced;
	}

	public void setNroced(String nroced) {
		this.nroced = nroced;
	}

	@XmlElement(name = "CUADRO")
	public String getCuadro() {
		return cuadro;
	}

	public void setCuadro(String cuadro) {
		this.cuadro = cuadro;
	}

	@XmlElement(name = "CODSEDE")
	public String getCodsede() {
		return codsede;
	}

	public void setCodsede(String codsede) {
		this.codsede = codsede;
	}

	@XmlElement(name = "NOM_SEDE")
	public String getNomSede() {
		return nomSede;
	}

	public void setNomSede(String nomSede) {
		this.nomSede = nomSede;
	}

	@XmlElement(name = "LICENCIA")
	public String getLicencia() {
		return licencia;
	}

	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	@XmlElement(name = "NO_SITUA")
	public String getNoSitua() {
		return noSitua;
	}

	public void setNoSitua(String noSitua) {
		this.noSitua = noSitua;
	}

	@XmlElement(name = "UBI_GEO")
	public String getUbiGeo() {
		return ubiGeo;
	}

	public void setUbiGeo(String ubiGeo) {
		this.ubiGeo = ubiGeo;
	}

	@XmlElement(name = "NOM_DPTO")
	public String getNomDpto() {
		return nomDpto;
	}

	public void setNomDpto(String nomDpto) {
		this.nomDpto = nomDpto;
	}

	@XmlElement(name = "NOM_PROV")
	public String getNomProv() {
		return nomProv;
	}

	public void setNomProv(String nomProv) {
		this.nomProv = nomProv;
	}

	@XmlElement(name = "NOM_DIST")
	public String getNomDist() {
		return nomDist;
	}

	public void setNomDist(String nomDist) {
		this.nomDist = nomDist;
	}

	@XmlElement(name = "DIR_SEDE")
	public String getDirSede() {
		return dirSede;
	}

	public void setDirSede(String dirSede) {
		this.dirSede = dirSede;
	}

	@XmlTransient
	public Matricula2019Cabecera getMatricula2019Cabecera() {
		return matricula2019Cabecera;
	}

	public void setMatricula2019Cabecera(Matricula2019Cabecera matricula2019Cabecera) {
		this.matricula2019Cabecera = matricula2019Cabecera;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2019Sede)) {
			return false;
		}
		Matricula2019Sede other = (Matricula2019Sede) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.entidad2019.Matricula2019Sede[idEnvio=" + idEnvio + "]";
	}

}
