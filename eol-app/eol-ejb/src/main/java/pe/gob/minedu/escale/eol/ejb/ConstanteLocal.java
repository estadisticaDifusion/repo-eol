package pe.gob.minedu.escale.eol.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.enums.estadistica.ConstanteEnum;
import pe.gob.minedu.escale.eol.domain.Constantes;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Local
public interface ConstanteLocal {
	List<Constantes> listarConstXTipo(ConstanteEnum tipConst);

	Constantes devConstante(ConstanteEnum tipConst, String codConst);
}
