/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.ejb.censo2018;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Carreras;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Dlen;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Eba;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Localpronoei;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Matricula;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018MatriculaFila;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Personal;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Recursos;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018RecursosFila;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Resp;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Saanee;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Seccion;
import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018SeccionFila;
import pe.gob.minedu.escale.eol.ejb.AbstractFacade;

@Singleton
public class Matricula2018Facade extends AbstractFacade<Matricula2018Cabecera> {

	@PersistenceContext(unitName = "eol-PUAux")
	private EntityManager em;

	public static final String CEDULA_MATRICULA = "CENSO-MATRICULA";
	static final Logger LOGGER = Logger.getLogger(Matricula2018Facade.class.getName());
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03AP = "c03ap";
	public final static String CEDULA_03AS = "c03as";
	public final static String CEDULA_04AI = "c04ai";
	public final static String CEDULA_04AA = "c04aa";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08AI = "c08ai";
	public final static String CEDULA_08AP = "c08ap";
	public final static String CEDULA_09A = "c09a";

	public Matricula2018Facade() {
		super(Matricula2018Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Matricula2018Cabecera cedula) {
		String sql = "UPDATE Matricula2018Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroced=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroced());
		query.executeUpdate();

		if (cedula.getDetalleMatricula() != null) {

			for (Matricula2018Matricula mat : cedula.getDetalleMatricula()) {

				mat.setMatricula2018Cabecera(cedula);
				for (Matricula2018MatriculaFila matf : mat.getMatricula2018MatriculaFilaList()) {
					matf.setMatricula2018Matricula(mat);
				}
			}

			/*
			 * Set<String> keys = cedula.getDetalleMatricula().keySet(); for
			 * (Iterator<String> it = keys.iterator(); it.hasNext();) { String key =
			 * it.next(); Matricula2018Matricula detalle =
			 * cedula.getDetalleMatricula().get(key);
			 * detalle.setMatricula2018Cabecera(cedula); List<Matricula2018MatriculaFila>
			 * filas = detalle.getMatricula2018MatriculaFilaList(); for
			 * (Matricula2018MatriculaFila fila : filas) {
			 * fila.setMatricula2018Matricula(detalle); } }
			 */
		}

		if (cedula.getDetalleSeccion() != null) {

			for (Matricula2018Seccion sec : cedula.getDetalleSeccion()) {

				sec.setMatricula2018Cabecera(cedula);
				for (Matricula2018SeccionFila secf : sec.getMatricula2018SeccionFilaList()) {
					secf.setMatricula2018Seccion(sec);
					;
				}
			}

			/*
			 * Set<String> keys = cedula.getDetalleSeccion().keySet(); for (Iterator<String>
			 * it = keys.iterator(); it.hasNext();) { String key = it.next();
			 * Matricula2018Seccion detalle = cedula.getDetalleSeccion().get(key);
			 * detalle.setMatricula2018Cabecera(cedula); List<Matricula2018SeccionFila>
			 * filas = detalle.getMatricula2018SeccionFilaList(); for
			 * (Matricula2018SeccionFila fila : filas) {
			 * fila.setMatricula2018Seccion(detalle); } }
			 */
		}

		if (cedula.getDetalleRecursos() != null) {

			for (Matricula2018Recursos rec : cedula.getDetalleRecursos()) {

				rec.setMatricula2018Cabecera(cedula);
				for (Matricula2018RecursosFila recf : rec.getMatricula2018RecursosFilaList()) {
					recf.setMatricula2018Recursos(rec);
				}
			}

			/*
			 * Set<String> keys = cedula.getDetalleRecursos().keySet(); for
			 * (Iterator<String> it = keys.iterator(); it.hasNext();) { String key =
			 * it.next(); Matricula2018Recursos detalle =
			 * cedula.getDetalleRecursos().get(key);
			 * detalle.setMatricula2018Cabecera(cedula); List<Matricula2018RecursosFila>
			 * filas = detalle.getMatricula2018RecursosFilaList(); for
			 * (Matricula2018RecursosFila fila : filas) {
			 * fila.setMatricula2018Recursos(detalle); } }
			 */
		}

		if (cedula.getMatricula2018PersonalList() != null) {
			for (Matricula2018Personal per : cedula.getMatricula2018PersonalList()) {
				per.setMatricula2018Cabecera(cedula);
			}
		}
		if (cedula.getMatricula2018LocalpronoeiList() != null) {
			for (Matricula2018Localpronoei loc : cedula.getMatricula2018LocalpronoeiList()) {
				loc.setMatricula2018Cabecera(cedula);
			}
		}
		if (cedula.getMatricula2018CarrerasList() != null) {
			for (Matricula2018Carreras carr : cedula.getMatricula2018CarrerasList()) {
				carr.setMatricula2018Cabecera(cedula);
			}
		}
		if (cedula.getMatricula2018SaaneeList() != null) {
			for (Matricula2018Saanee saan : cedula.getMatricula2018SaaneeList()) {
				saan.setMatricula2018Cabecera(cedula);
			}
		}
		if (cedula.getMatricula2018EbaList() != null) {
			for (Matricula2018Eba eba : cedula.getMatricula2018EbaList()) {
				eba.setMatricula2018Cabecera(cedula);
			}
		}
		if (cedula.getMatricula2018DlenList() != null) {
			for (Matricula2018Dlen dlen : cedula.getMatricula2018DlenList()) {
				dlen.setMatricula2018Cabecera(cedula);
			}
		}
		if (cedula.getMatricula2018RespList() != null) {
			for (Matricula2018Resp resp : cedula.getMatricula2018RespList()) {
				resp.setMatricula2018Cabecera(cedula);
			}
		}

		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

	public Matricula2018Cabecera findByCodModAnNiv(String codmod, String anexo, String nivmod) {
		Query q = em.createQuery(
				"SELECT M FROM Matricula2018Cabecera M WHERE M.codMod=:codmod AND M.anexo=:anexo AND M.nivMod=:nivmod AND M.ultimo=true");
		q.setParameter("codmod", codmod);
		q.setParameter("anexo", anexo);
		q.setParameter("nivmod", nivmod);
		try {
			return (Matricula2018Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> registrarEnvioIdentificacion(String codmod, String anexo, String nivmod) {
		Query q = null;

		try {
			String query = "CALL registrar_envio_2018(?,?,?)";
			q = (Query) em.createNativeQuery(query);

			q.setParameter("1", codmod);
			q.setParameter("2", anexo);
			q.setParameter("3", nivmod);

			List<Object[]> listObj = q.getResultList();

			return listObj;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
