/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "local2018_sec114")
public class Local2018Sec114 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P114_NRO")
    private Integer p114Nro;
    @Column(name = "P114_CM")
    private String p114Cm;
    @Column(name = "P114_ANX")
    private String p114Anx;
    @Column(name = "P114_IE")
    private String p114Ie;
    @Column(name = "P114_GES")
    private String p114Ges;
    @Column(name = "P114_NM")
    private String p114Nm;
    @Column(name = "P114_TUR")
    private String p114Tur;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2018Cabecera local2018Cabecera;

    public Local2018Sec114() {
    }

    public Local2018Sec114(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "P114_NRO")
    public Integer getP114Nro() {
        return p114Nro;
    }

    public void setP114Nro(Integer p114Nro) {
        this.p114Nro = p114Nro;
    }

    @XmlElement(name = "P114_CM")
    public String getP114Cm() {
        return p114Cm;
    }

    public void setP114Cm(String p114Cm) {
        this.p114Cm = p114Cm;
    }

    @XmlElement(name = "P114_ANX")
    public String getP114Anx() {
        return p114Anx;
    }

    public void setP114Anx(String p114Anx) {
        this.p114Anx = p114Anx;
    }

    @XmlElement(name = "P114_IE")
    public String getP114Ie() {
        return p114Ie;
    }

    public void setP114Ie(String p114Ie) {
        this.p114Ie = p114Ie;
    }

    @XmlElement(name = "P114_GES")
    public String getP114Ges() {
        return p114Ges;
    }

    public void setP114Ges(String p114Ges) {
        this.p114Ges = p114Ges;
    }

    @XmlElement(name = "P114_NM")
    public String getP114Nm() {
        return p114Nm;
    }

    public void setP114Nm(String p114Nm) {
        this.p114Nm = p114Nm;
    }

    @XmlElement(name = "P114_TUR")
    public String getP114Tur() {
        return p114Tur;
    }

    public void setP114Tur(String p114Tur) {
        this.p114Tur = p114Tur;
    }

    @XmlTransient
    public Local2018Cabecera getLocal2018Cabecera() {
        return local2018Cabecera;
    }

    public void setLocal2018Cabecera(Local2018Cabecera local2018Cabecera) {
        this.local2018Cabecera = local2018Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2018Sec114)) {
            return false;
        }
        Local2018Sec114 other = (Local2018Sec114) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Local2018Sec114[idEnvio=" + idEnvio + "]";
    }

}
