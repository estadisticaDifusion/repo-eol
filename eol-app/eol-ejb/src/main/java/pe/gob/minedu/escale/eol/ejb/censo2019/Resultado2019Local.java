package pe.gob.minedu.escale.eol.ejb.censo2019;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera;



@Local
public interface Resultado2019Local {

	public void create(Resultado2019Cabecera cedula);

	public void updatedetail(Resultado2019Cabecera ceddiciembre, Resultado2019Cabecera cedfebrero);

	public Resultado2019Cabecera findByCodModAnNiv(String codmod, String anexo, String nivmod);
	
	public Resultado2019Cabecera findCedNivel(Long idEnvio);
	
	public List<Object[]> verificarObservacionCedula(String codmod, String anexo, String nroced, Integer totalH,
				Integer totalM, Integer dato01h, Integer dato01m, Integer dato02h, Integer dato02m, Integer dato03h,
				Integer dato03m, Integer dato04h, Integer dato04m, Integer dato05h, Integer dato05m, Integer dato06h,
				Integer dato06m, Integer dato07h, Integer dato07m, Integer dato08h, Integer dato08m, Integer dato09h,
				Integer dato09m, Integer dato10h, Integer dato10m);

	public List<Object[]> registrarEnvioResultadoIdentificacion(String codmod, String anexo, String nivmod);
	
	
	
}
