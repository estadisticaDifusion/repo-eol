/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.domain.xml;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "igel")
public class Igel {

    private String idIgel;
    private String nombreUgel;

    public String getIdIgel() {
        return idIgel;
    }

    public void setIdIgel(String idIgel) {
        this.idIgel = idIgel;
    }

    public String getNombreUgel() {
        return nombreUgel;
    }

    public void setNombreUgel(String nombreUgel) {
        this.nombreUgel = nombreUgel;
    }
}
