/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "matricula2019_matricula")
public class Matricula2019Matricula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;
	
	@Column(name = "CUADRO")
	private String cuadro;
	
	@JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	@ManyToOne(optional = false)
	private Matricula2019Cabecera matricula2019Cabecera;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Matricula", fetch = FetchType.LAZY)
	private List<Matricula2019MatriculaFila> matricula2019MatriculaFilaList;

	public Matricula2019Matricula() {
	}

	public Matricula2019Matricula(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "CUADRO")
	public String getCuadro() {
		return cuadro;
	}

	public void setCuadro(String cuadro) {
		this.cuadro = cuadro;
	}

	@XmlElement(name = "MATRICULA_FILAS")
	public List<Matricula2019MatriculaFila> getMatricula2019MatriculaFilaList() {
		return matricula2019MatriculaFilaList;
	}

	public void setMatricula2019MatriculaFilaList(List<Matricula2019MatriculaFila> matricula2019MatriculaFilaList) {
		this.matricula2019MatriculaFilaList = matricula2019MatriculaFilaList;
	}

	@XmlTransient
	public Matricula2019Cabecera getMatricula2019Cabecera() {
		return matricula2019Cabecera;
	}

	public void setMatricula2019Cabecera(Matricula2019Cabecera matricula2019Cabecera) {
		this.matricula2019Cabecera = matricula2019Cabecera;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2019Matricula)) {
			return false;
		}
		Matricula2019Matricula other = (Matricula2019Matricula) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Matricula[idEnvio=" + idEnvio + "]";
	}

}
