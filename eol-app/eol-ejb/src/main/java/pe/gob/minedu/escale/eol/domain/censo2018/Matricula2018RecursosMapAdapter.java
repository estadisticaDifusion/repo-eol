package pe.gob.minedu.escale.eol.domain.censo2018;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018RecursosMapAdapter.Matricula2018RecursosList;

public class Matricula2018RecursosMapAdapter  extends XmlAdapter<Matricula2018RecursosList, Map<String, Matricula2018Recursos>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2018RecursosMapAdapter.class.getName());

    static class Matricula2018RecursosList {

        private List<Matricula2018Recursos> detalle;

        private Matricula2018RecursosList(ArrayList<Matricula2018Recursos> lista) {
            detalle = lista;
        }

        public Matricula2018RecursosList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2018Recursos> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2018Recursos> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2018Recursos> unmarshal(Matricula2018RecursosList v) throws Exception {

        Map<String, Matricula2018Recursos> map = new HashMap<String, Matricula2018Recursos>();
        for (Matricula2018Recursos detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2018RecursosList marshal(Map<String, Matricula2018Recursos> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2018Recursos> lista = new ArrayList<Matricula2018Recursos>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2018Recursos $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2018RecursosList list = new Matricula2018RecursosList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
