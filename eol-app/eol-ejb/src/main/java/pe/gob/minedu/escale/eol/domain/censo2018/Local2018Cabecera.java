/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2018;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JBEDRILLANA
 */
@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2018_cabecera")
public class Local2018Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "TELEFONO")
    private String telefono;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "DPTO")
    private String dpto;
    @Column(name = "PROV")
    private String prov;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "CATEG_CP")
    private String categCp;
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Column(name = "NOMBVIA")
    private String nombvia;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "MANZANA")
    private String manzana;
    @Column(name = "LOTE")
    private String lote;
    @Column(name = "SECTOR")
    private String sector;
    @Column(name = "ZONA")
    private String zona;
    @Column(name = "ETAPA")
    private String etapa;
    @Column(name = "OTROS")
    private String otros;
    @Column(name = "REFERENCIA")
    private String referencia;
    @Column(name = "P101")
    private String p101;
    @Column(name = "P101_ESP")
    private String p101Esp;
    @Column(name = "P102")
    private String p102;
    @Column(name = "P102_ESP")
    private String p102Esp;
    @Column(name = "P103")
    private String p103;
    @Column(name = "P103_FREG")
    private String p103Freg;
    @Column(name = "P103_OREG")
    private String p103Oreg;
    @Column(name = "P104")
    private String p104;
    @Column(name = "P104_EST")
    private String p104Est;
    @Column(name = "P104_TER")
    private String p104Ter;
    @Column(name = "P105")
    private String p105;
    @Column(name = "P106")
    private String p106;
    @Column(name = "P106_ESP")
    private String p106Esp;
    @Column(name = "P107_AT")
    private Integer p107At;
    @Column(name = "P107_AC")
    private Integer p107Ac;
    @Column(name = "P108")
    private String p108;
    @Column(name = "P108_RES")
    private String p108Res;
    @Column(name = "P109")
    private String p109;
    @Column(name = "P110")
    private String p110;
    @Column(name = "P110_SI")
    private String p110Si;
    @Column(name = "P111")
    private String p111;
    @Column(name = "P111_SI")
    private String p111Si;
    @Column(name = "P112_EST")
    private String p112Est;
    @Column(name = "P112_MAT")
    private String p112Mat;
    @Column(name = "P112_ESP")
    private String p112Esp;
    @Column(name = "P113_1")
    private String p1131;
    @Column(name = "P113_2")
    private String p1132;
    @Column(name = "P113_3")
    private String p1133;
    @Column(name = "P113_4")
    private String p1134;
    @Column(name = "P113_5")
    private String p1135;
    @Column(name = "P113_6")
    private String p1136;
    @Column(name = "P113_ESP")
    private String p113Esp;
    @Column(name = "P201")
    private Integer p201;
    @Column(name = "P202")
    private Integer p202;
    @Column(name = "P301_1")
    private Integer p3011;
    @Column(name = "P301_2")
    private Integer p3012;
    @Column(name = "P301_3")
    private Integer p3013;
    @Column(name = "P301_4")
    private Integer p3014;
    @Column(name = "P306")
    private String p306;
    @Column(name = "P306_SI")
    private String p306Si;
    @Column(name = "P307_1")
    private Integer p3071;
    @Column(name = "P307_2")
    private Integer p3072;
    @Column(name = "P308")
    private String p308;
    @Column(name = "P309_1")
    private String p3091;
    @Column(name = "P309_2")
    private String p3092;
    @Column(name = "P309_3")
    private String p3093;
    @Column(name = "P309_4")
    private String p3094;
    @Column(name = "P309_5")
    private String p3095;
    @Column(name = "P309_6")
    private String p3096;
    @Column(name = "P309_7")
    private String p3097;
    @Column(name = "P309_8")
    private String p3098;
    @Column(name = "P309_9")
    private String p3099;
    @Column(name = "P309_10")
    private String p30910;
    @Column(name = "P309_11")
    private String p30911;
    @Column(name = "P309_12")
    private String p30912;
    @Column(name = "P309_13")
    private String p30913;
    @Column(name = "P309_14")
    private String p30914;
    @Column(name = "P310")
    private String p310;
    @Column(name = "P311")
    private String p311;
    @Column(name = "P312")
    private String p312;
    @Column(name = "P312_NO")
    private String p312No;
    @Column(name = "P312_ESP")
    private String p312Esp;
    @Column(name = "P313_1")
    private String p3131;
    @Column(name = "P313_2")
    private String p3132;
    @Column(name = "P313_3")
    private String p3133;
    @Column(name = "P313_4")
    private String p3134;
    @Column(name = "P313_5")
    private String p3135;
    @Column(name = "P313_6")
    private String p3136;
    @Column(name = "P313_7")
    private String p3137;
    @Column(name = "P313_8")
    private String p3138;
    @Column(name = "P313_9")
    private String p3139;
    @Column(name = "P314")
    private String p314;
    @Column(name = "P315_1")
    private String p3151;
    @Column(name = "P315_2")
    private String p3152;
    @Column(name = "P315_3")
    private String p3153;
    @Column(name = "P315_4")
    private String p3154;
    @Column(name = "P315_5")
    private String p3155;
    @Column(name = "P315_6")
    private String p3156;
    @Column(name = "P315_7")
    private String p3157;
    @Column(name = "P400")
    private Integer p400;
    @Column(name = "P504")
    private Integer p504;
    @Column(name = "P505_1")
    private String p5051;
    @Column(name = "P505_2")
    private String p5052;
    @Column(name = "P505_3")
    private String p5053;
    @Column(name = "P505_4")
    private String p5054;
    @Column(name = "P505_5")
    private String p5055;
    @Column(name = "P505_6")
    private String p5056;
    @Column(name = "P505_7")
    private String p5057;
    @Column(name = "P505_8")
    private String p5058;
    @Column(name = "P505_9")
    private String p5059;
    @Column(name = "P505_10")
    private String p50510;
    @Column(name = "P505_11")
    private String p50511;
    @Column(name = "P506_1")
    private String p5061;
    @Column(name = "P506_2")
    private String p5062;
    @Column(name = "P506_3")
    private String p5063;
    @Column(name = "P506_4")
    private String p5064;
    @Column(name = "P507")
    private String p507;
    @Column(name = "P507_ESP")
    private String p507Esp;
    @Column(name = "P508")
    private String p508;
    @Column(name = "P508_SI")
    private Integer p508Si;
    @Column(name = "P508_NO1")
    private Integer p508No1;
    @Column(name = "P508_NO2")
    private Integer p508No2;
    @Column(name = "P509")
    private String p509;
    @Column(name = "P510")
    private String p510;
    @Column(name = "P510_1")
    private String p5101;
    @Column(name = "P510_2")
    private String p5102;
    @Column(name = "P510_2ESP")
    private String p5102esp;
    @Column(name = "P511")
    private String p511;
    @Column(name = "P511_ESP")
    private String p511Esp;
    @Column(name = "P512")
    private String p512;
    @Column(name = "P512_SI")
    private Integer p512Si;
    @Column(name = "P512_NO1")
    private Integer p512No1;
    @Column(name = "P512_NO2")
    private Integer p512No2;
    @Column(name = "P513")
    private String p513;
    @Column(name = "P514")
    private String p514;
    @Column(name = "P514_1")
    private String p5141;
    @Column(name = "P514_2")
    private String p5142;
    @Column(name = "P514_2ESP")
    private String p5142esp;
    @Column(name = "P515_11")
    private String p51511;
    @Column(name = "P515_12")
    private String p51512;
    @Column(name = "P515_13")
    private String p51513;
    @Column(name = "P515_21")
    private String p51521;
    @Column(name = "P515_22")
    private String p51522;
    @Column(name = "P515_23")
    private String p51523;
    @Column(name = "P515_31")
    private String p51531;
    @Column(name = "P515_32")
    private String p51532;
    @Column(name = "P515_33")
    private String p51533;
    @Column(name = "P516")
    private String p516;
    @Column(name = "P517")
    private String p517;
    @Column(name = "P518")
    private String p518;
    @Column(name = "P518_QN")
    private Integer p518Qn;
    @Column(name = "P521")
    private String p521;
    @Column(name = "P522")
    private String p522;
    @Column(name = "P522_ESP")
    private String p522Esp;
    @Column(name = "P523")
    private String p523;
    @Column(name = "P523_ESP")
    private String p523Esp;
    @Column(name = "P525")
    private String p525;
    @Column(name = "P701")
    private String p701;
    @Column(name = "P701_1")
    private String p7011;
    @Column(name = "P701_2")
    private String p7012;
    @Column(name = "P701_3")
    private String p7013;
    @Column(name = "P701_4")
    private String p7014;
    @Column(name = "P701_5")
    private String p7015;
    @Column(name = "P701_6")
    private String p7016;
    @Column(name = "P701_7")
    private String p7017;
    @Column(name = "P701_8")
    private String p7018;
    @Column(name = "P701_9")
    private String p7019;
    @Column(name = "P701_10")
    private String p70110;
    @Column(name = "P701_11")
    private String p70111;
    @Column(name = "P701_12")
    private String p70112;
    @Column(name = "P701_13")
    private String p70113;
    @Column(name = "P701_14")
    private String p70114;
    @Column(name = "P701_15")
    private String p70115;
    @Column(name = "P701_16")
    private String p70116;
    @Column(name = "P701_17")
    private String p70117;
    @Column(name = "P701_18")
    private String p70118;
    @Column(name = "P701_19")
    private String p70119;
    @Column(name = "P701_20")
    private String p70120;
    @Column(name = "P701_ESP")
    private String p701Esp;
    @Column(name = "P702")
    private String p702;
    @Column(name = "P703_1")
    private String p7031;
    @Column(name = "P703_2")
    private String p7032;
    @Column(name = "P703_3")
    private String p7033;
    @Column(name = "P703_4")
    private String p7034;
    @Column(name = "P703_5")
    private String p7035;
    @Column(name = "P703_6")
    private String p7036;
    @Column(name = "P703_ESP")
    private String p703Esp;
    @Column(name = "P704")
    private String p704;
    @Column(name = "P705_1")
    private String p7051;
    @Column(name = "P705_2")
    private String p7052;
    @Column(name = "P705_3")
    private String p7053;
    @Column(name = "P705_4")
    private String p7054;
    @Column(name = "P705_5")
    private String p7055;
    @Column(name = "P705_ESP")
    private String p705Esp;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @OneToMany(mappedBy = "local2018Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2018Sec400> local2018Sec400List;
    @OneToMany(mappedBy = "local2018Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2018Sec500> local2018Sec500List;
    @OneToMany(mappedBy = "local2018Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2018Sec203> local2018Sec203List;
    @OneToMany(mappedBy = "local2018Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2018Sec114> local2018Sec114List;
    @OneToMany(mappedBy = "local2018Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2018Sec600> local2018Sec600List;
    @OneToMany(mappedBy = "local2018Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2018Sec300> local2018Sec300List;
    @OneToMany(mappedBy = "local2018Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2018Resp> local2018RespList;

    //fin
    @Transient
    private long token;
    @Transient
    private String msg;
    @Transient
    private String estadoRpt;

    public Local2018Cabecera() {
    }

    public Local2018Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    @XmlElement(name = "TELEFONO")
    public String getTelefono() {
        return telefono;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "DPTO")
    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }

    @XmlElement(name = "PROV")
    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name = "CATEG_CP")
    public String getCategCp() {
        return categCp;
    }

    public void setCategCp(String categCp) {
        this.categCp = categCp;
    }

    @XmlElement(name = "NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name = "CATEGORIA")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlElement(name = "TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name = "NOMBVIA")
    public String getNombvia() {
        return nombvia;
    }

    public void setNombvia(String nombvia) {
        this.nombvia = nombvia;
    }

    @XmlElement(name = "NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name = "MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name = "LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name = "SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name = "ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name = "ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name = "OTROS")
    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    @XmlElement(name = "REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name = "P101")
    public String getP101() {
        return p101;
    }

    public void setP101(String p101) {
        this.p101 = p101;
    }

    @XmlElement(name = "P101_ESP")
    public String getP101Esp() {
        return p101Esp;
    }

    public void setP101Esp(String p101Esp) {
        this.p101Esp = p101Esp;
    }

    @XmlElement(name = "P102")
    public String getP102() {
        return p102;
    }

    public void setP102(String p102) {
        this.p102 = p102;
    }

    @XmlElement(name = "P102_ESP")
    public String getP102Esp() {
        return p102Esp;
    }

    public void setP102Esp(String p102Esp) {
        this.p102Esp = p102Esp;
    }

    @XmlElement(name = "P103")
    public String getP103() {
        return p103;
    }

    public void setP103(String p103) {
        this.p103 = p103;
    }

    @XmlElement(name = "P103_FREG")
    public String getP103Freg() {
        return p103Freg;
    }

    public void setP103Freg(String p103Freg) {
        this.p103Freg = p103Freg;
    }

    @XmlElement(name = "P103_OREG")
    public String getP103Oreg() {
        return p103Oreg;
    }

    public void setP103Oreg(String p103Oreg) {
        this.p103Oreg = p103Oreg;
    }

    @XmlElement(name = "P104")
    public String getP104() {
        return p104;
    }

    public void setP104(String p104) {
        this.p104 = p104;
    }

    @XmlElement(name = "P104_EST")
    public String getP104Est() {
        return p104Est;
    }

    public void setP104Est(String p104Est) {
        this.p104Est = p104Est;
    }

    @XmlElement(name = "P104_TER")
    public String getP104Ter() {
        return p104Ter;
    }

    public void setP104Ter(String p104Ter) {
        this.p104Ter = p104Ter;
    }

    @XmlElement(name = "P105")
    public String getP105() {
        return p105;
    }

    public void setP105(String p105) {
        this.p105 = p105;
    }

    @XmlElement(name = "P106")
    public String getP106() {
        return p106;
    }

    public void setP106(String p106) {
        this.p106 = p106;
    }

    @XmlElement(name = "P106_ESP")
    public String getP106Esp() {
        return p106Esp;
    }

    public void setP106Esp(String p106Esp) {
        this.p106Esp = p106Esp;
    }

    @XmlElement(name = "P107_AT")
    public Integer getP107At() {
        return p107At;
    }

    public void setP107At(Integer p107At) {
        this.p107At = p107At;
    }

    @XmlElement(name = "P107_AC")
    public Integer getP107Ac() {
        return p107Ac;
    }

    public void setP107Ac(Integer p107Ac) {
        this.p107Ac = p107Ac;
    }

    @XmlElement(name = "P108")
    public String getP108() {
        return p108;
    }

    public void setP108(String p108) {
        this.p108 = p108;
    }

    @XmlElement(name = "P108_RES")
    public String getP108Res() {
        return p108Res;
    }

    public void setP108Res(String p108Res) {
        this.p108Res = p108Res;
    }

    @XmlElement(name = "P109")
    public String getP109() {
        return p109;
    }

    public void setP109(String p109) {
        this.p109 = p109;
    }

    @XmlElement(name = "P110")
    public String getP110() {
        return p110;
    }

    public void setP110(String p110) {
        this.p110 = p110;
    }

    @XmlElement(name = "P110_SI")
    public String getP110Si() {
        return p110Si;
    }

    public void setP110Si(String p110Si) {
        this.p110Si = p110Si;
    }

    @XmlElement(name = "P111")
    public String getP111() {
        return p111;
    }

    public void setP111(String p111) {
        this.p111 = p111;
    }

    @XmlElement(name = "P111_SI")
    public String getP111Si() {
        return p111Si;
    }

    public void setP111Si(String p111Si) {
        this.p111Si = p111Si;
    }

    @XmlElement(name = "P112_EST")
    public String getP112Est() {
        return p112Est;
    }

    public void setP112Est(String p112Est) {
        this.p112Est = p112Est;
    }

    @XmlElement(name = "P112_MAT")
    public String getP112Mat() {
        return p112Mat;
    }

    public void setP112Mat(String p112Mat) {
        this.p112Mat = p112Mat;
    }

    @XmlElement(name = "P112_ESP")
    public String getP112Esp() {
        return p112Esp;
    }

    public void setP112Esp(String p112Esp) {
        this.p112Esp = p112Esp;
    }

    @XmlElement(name = "P113_1")
    public String getP1131() {
        return p1131;
    }

    public void setP1131(String p1131) {
        this.p1131 = p1131;
    }

    @XmlElement(name = "P113_2")
    public String getP1132() {
        return p1132;
    }

    public void setP1132(String p1132) {
        this.p1132 = p1132;
    }

    @XmlElement(name = "P113_3")
    public String getP1133() {
        return p1133;
    }

    public void setP1133(String p1133) {
        this.p1133 = p1133;
    }

    @XmlElement(name = "P113_4")
    public String getP1134() {
        return p1134;
    }

    public void setP1134(String p1134) {
        this.p1134 = p1134;
    }

    @XmlElement(name = "P113_5")
    public String getP1135() {
        return p1135;
    }

    public void setP1135(String p1135) {
        this.p1135 = p1135;
    }

    @XmlElement(name = "P113_6")
    public String getP1136() {
        return p1136;
    }

    public void setP1136(String p1136) {
        this.p1136 = p1136;
    }

    @XmlElement(name = "P113_ESP")
    public String getP113Esp() {
        return p113Esp;
    }

    public void setP113Esp(String p113Esp) {
        this.p113Esp = p113Esp;
    }

    @XmlElement(name = "P201")
    public Integer getP201() {
        return p201;
    }

    public void setP201(Integer p201) {
        this.p201 = p201;
    }

    @XmlElement(name = "P202")
    public Integer getP202() {
        return p202;
    }

    public void setP202(Integer p202) {
        this.p202 = p202;
    }

    @XmlElement(name = "P301_1")
    public Integer getP3011() {
        return p3011;
    }

    public void setP3011(Integer p3011) {
        this.p3011 = p3011;
    }

    @XmlElement(name = "P301_2")
    public Integer getP3012() {
        return p3012;
    }

    public void setP3012(Integer p3012) {
        this.p3012 = p3012;
    }

    @XmlElement(name = "P301_3")
    public Integer getP3013() {
        return p3013;
    }

    public void setP3013(Integer p3013) {
        this.p3013 = p3013;
    }

    @XmlElement(name = "P301_4")
    public Integer getP3014() {
        return p3014;
    }

    public void setP3014(Integer p3014) {
        this.p3014 = p3014;
    }

    @XmlElement(name = "P306")
    public String getP306() {
        return p306;
    }

    public void setP306(String p306) {
        this.p306 = p306;
    }

    @XmlElement(name = "P306_SI")
    public String getP306Si() {
        return p306Si;
    }

    public void setP306Si(String p306Si) {
        this.p306Si = p306Si;
    }

    @XmlElement(name = "P307_1")
    public Integer getP3071() {
        return p3071;
    }

    public void setP3071(Integer p3071) {
        this.p3071 = p3071;
    }

    @XmlElement(name = "P307_2")
    public Integer getP3072() {
        return p3072;
    }

    public void setP3072(Integer p3072) {
        this.p3072 = p3072;
    }

    @XmlElement(name = "P308")
    public String getP308() {
        return p308;
    }

    public void setP308(String p308) {
        this.p308 = p308;
    }

    @XmlElement(name = "P309_1")
    public String getP3091() {
        return p3091;
    }

    public void setP3091(String p3091) {
        this.p3091 = p3091;
    }

    @XmlElement(name = "P309_2")
    public String getP3092() {
        return p3092;
    }

    public void setP3092(String p3092) {
        this.p3092 = p3092;
    }

    @XmlElement(name = "P309_3")
    public String getP3093() {
        return p3093;
    }

    public void setP3093(String p3093) {
        this.p3093 = p3093;
    }

    @XmlElement(name = "P309_4")
    public String getP3094() {
        return p3094;
    }

    public void setP3094(String p3094) {
        this.p3094 = p3094;
    }

    @XmlElement(name = "P309_5")
    public String getP3095() {
        return p3095;
    }

    public void setP3095(String p3095) {
        this.p3095 = p3095;
    }

    @XmlElement(name = "P309_6")
    public String getP3096() {
        return p3096;
    }

    public void setP3096(String p3096) {
        this.p3096 = p3096;
    }

    @XmlElement(name = "P309_7")
    public String getP3097() {
        return p3097;
    }

    public void setP3097(String p3097) {
        this.p3097 = p3097;
    }

    @XmlElement(name = "P309_8")
    public String getP3098() {
        return p3098;
    }

    public void setP3098(String p3098) {
        this.p3098 = p3098;
    }

    @XmlElement(name = "P309_9")
    public String getP3099() {
        return p3099;
    }

    public void setP3099(String p3099) {
        this.p3099 = p3099;
    }

    @XmlElement(name = "P309_10")
    public String getP30910() {
        return p30910;
    }

    public void setP30910(String p30910) {
        this.p30910 = p30910;
    }

    @XmlElement(name = "P309_11")
    public String getP30911() {
        return p30911;
    }

    public void setP30911(String p30911) {
        this.p30911 = p30911;
    }

    @XmlElement(name = "P309_12")
    public String getP30912() {
        return p30912;
    }

    public void setP30912(String p30912) {
        this.p30912 = p30912;
    }

    @XmlElement(name = "P309_13")
    public String getP30913() {
        return p30913;
    }

    public void setP30913(String p30913) {
        this.p30913 = p30913;
    }

    @XmlElement(name = "P309_14")
    public String getP30914() {
        return p30914;
    }

    public void setP30914(String p30914) {
        this.p30914 = p30914;
    }

    @XmlElement(name = "P310")
    public String getP310() {
        return p310;
    }

    public void setP310(String p310) {
        this.p310 = p310;
    }

    @XmlElement(name = "P311")
    public String getP311() {
        return p311;
    }

    public void setP311(String p311) {
        this.p311 = p311;
    }

    @XmlElement(name = "P312")
    public String getP312() {
        return p312;
    }

    public void setP312(String p312) {
        this.p312 = p312;
    }

    @XmlElement(name = "P312_NO")
    public String getP312No() {
        return p312No;
    }

    public void setP312No(String p312No) {
        this.p312No = p312No;
    }

    @XmlElement(name = "P312_ESP")
    public String getP312Esp() {
        return p312Esp;
    }

    public void setP312Esp(String p312Esp) {
        this.p312Esp = p312Esp;
    }

    @XmlElement(name = "P313_1")
    public String getP3131() {
        return p3131;
    }

    public void setP3131(String p3131) {
        this.p3131 = p3131;
    }

    @XmlElement(name = "P313_2")
    public String getP3132() {
        return p3132;
    }

    public void setP3132(String p3132) {
        this.p3132 = p3132;
    }

    @XmlElement(name = "P313_3")
    public String getP3133() {
        return p3133;
    }

    public void setP3133(String p3133) {
        this.p3133 = p3133;
    }

    @XmlElement(name = "P313_4")
    public String getP3134() {
        return p3134;
    }

    public void setP3134(String p3134) {
        this.p3134 = p3134;
    }

    @XmlElement(name = "P313_5")
    public String getP3135() {
        return p3135;
    }

    public void setP3135(String p3135) {
        this.p3135 = p3135;
    }

    @XmlElement(name = "P313_6")
    public String getP3136() {
        return p3136;
    }

    public void setP3136(String p3136) {
        this.p3136 = p3136;
    }

    @XmlElement(name = "P313_7")
    public String getP3137() {
        return p3137;
    }

    public void setP3137(String p3137) {
        this.p3137 = p3137;
    }

    @XmlElement(name = "P313_8")
    public String getP3138() {
        return p3138;
    }

    public void setP3138(String p3138) {
        this.p3138 = p3138;
    }

    @XmlElement(name = "P313_9")
    public String getP3139() {
        return p3139;
    }

    public void setP3139(String p3139) {
        this.p3139 = p3139;
    }

    @XmlElement(name = "P314")
    public String getP314() {
        return p314;
    }

    public void setP314(String p314) {
        this.p314 = p314;
    }

    @XmlElement(name = "P315_1")
    public String getP3151() {
        return p3151;
    }

    public void setP3151(String p3151) {
        this.p3151 = p3151;
    }

    @XmlElement(name = "P315_2")
    public String getP3152() {
        return p3152;
    }

    public void setP3152(String p3152) {
        this.p3152 = p3152;
    }

    @XmlElement(name = "P315_3")
    public String getP3153() {
        return p3153;
    }

    public void setP3153(String p3153) {
        this.p3153 = p3153;
    }

    @XmlElement(name = "P315_4")
    public String getP3154() {
        return p3154;
    }

    public void setP3154(String p3154) {
        this.p3154 = p3154;
    }

    @XmlElement(name = "P315_5")
    public String getP3155() {
        return p3155;
    }

    public void setP3155(String p3155) {
        this.p3155 = p3155;
    }

    @XmlElement(name = "P315_6")
    public String getP3156() {
        return p3156;
    }

    public void setP3156(String p3156) {
        this.p3156 = p3156;
    }

    @XmlElement(name = "P315_7")
    public String getP3157() {
        return p3157;
    }

    public void setP3157(String p3157) {
        this.p3157 = p3157;
    }

    @XmlElement(name = "P400")
    public Integer getP400() {
        return p400;
    }

    public void setP400(Integer p400) {
        this.p400 = p400;
    }

    @XmlElement(name = "P504")
    public Integer getP504() {
        return p504;
    }

    public void setP504(Integer p504) {
        this.p504 = p504;
    }

    @XmlElement(name = "P505_1")
    public String getP5051() {
        return p5051;
    }

    public void setP5051(String p5051) {
        this.p5051 = p5051;
    }

    @XmlElement(name = "P505_2")
    public String getP5052() {
        return p5052;
    }

    public void setP5052(String p5052) {
        this.p5052 = p5052;
    }

    @XmlElement(name = "P505_3")
    public String getP5053() {
        return p5053;
    }

    public void setP5053(String p5053) {
        this.p5053 = p5053;
    }

    @XmlElement(name = "P505_4")
    public String getP5054() {
        return p5054;
    }

    public void setP5054(String p5054) {
        this.p5054 = p5054;
    }

    @XmlElement(name = "P505_5")
    public String getP5055() {
        return p5055;
    }

    public void setP5055(String p5055) {
        this.p5055 = p5055;
    }

    @XmlElement(name = "P505_6")
    public String getP5056() {
        return p5056;
    }

    public void setP5056(String p5056) {
        this.p5056 = p5056;
    }

    @XmlElement(name = "P505_7")
    public String getP5057() {
        return p5057;
    }

    public void setP5057(String p5057) {
        this.p5057 = p5057;
    }

    @XmlElement(name = "P505_8")
    public String getP5058() {
        return p5058;
    }

    public void setP5058(String p5058) {
        this.p5058 = p5058;
    }

    @XmlElement(name = "P505_9")
    public String getP5059() {
        return p5059;
    }

    public void setP5059(String p5059) {
        this.p5059 = p5059;
    }

    @XmlElement(name = "P505_10")
    public String getP50510() {
        return p50510;
    }

    public void setP50510(String p50510) {
        this.p50510 = p50510;
    }

    @XmlElement(name = "P505_11")
    public String getP50511() {
        return p50511;
    }

    public void setP50511(String p50511) {
        this.p50511 = p50511;
    }

    @XmlElement(name = "P506_1")
    public String getP5061() {
        return p5061;
    }

    public void setP5061(String p5061) {
        this.p5061 = p5061;
    }

    @XmlElement(name = "P506_2")
    public String getP5062() {
        return p5062;
    }

    public void setP5062(String p5062) {
        this.p5062 = p5062;
    }

    @XmlElement(name = "P506_3")
    public String getP5063() {
        return p5063;
    }

    public void setP5063(String p5063) {
        this.p5063 = p5063;
    }

    @XmlElement(name = "P506_4")
    public String getP5064() {
        return p5064;
    }

    public void setP5064(String p5064) {
        this.p5064 = p5064;
    }

    @XmlElement(name = "P507")
    public String getP507() {
        return p507;
    }

    public void setP507(String p507) {
        this.p507 = p507;
    }

    @XmlElement(name = "P507_ESP")
    public String getP507Esp() {
        return p507Esp;
    }

    public void setP507Esp(String p507Esp) {
        this.p507Esp = p507Esp;
    }

    @XmlElement(name = "P508")
    public String getP508() {
        return p508;
    }

    public void setP508(String p508) {
        this.p508 = p508;
    }

    @XmlElement(name = "P508_SI")
    public Integer getP508Si() {
        return p508Si;
    }

    public void setP508Si(Integer p508Si) {
        this.p508Si = p508Si;
    }

    @XmlElement(name = "P508_NO1")
    public Integer getP508No1() {
        return p508No1;
    }

    public void setP508No1(Integer p508No1) {
        this.p508No1 = p508No1;
    }

    @XmlElement(name = "P508_NO2")
    public Integer getP508No2() {
        return p508No2;
    }

    public void setP508No2(Integer p508No2) {
        this.p508No2 = p508No2;
    }

    @XmlElement(name = "P509")
    public String getP509() {
        return p509;
    }

    public void setP509(String p509) {
        this.p509 = p509;
    }

    @XmlElement(name = "P510")
    public String getP510() {
        return p510;
    }

    public void setP510(String p510) {
        this.p510 = p510;
    }

    @XmlElement(name = "P510_1")
    public String getP5101() {
        return p5101;
    }

    public void setP5101(String p5101) {
        this.p5101 = p5101;
    }

    @XmlElement(name = "P510_2")
    public String getP5102() {
        return p5102;
    }

    public void setP5102(String p5102) {
        this.p5102 = p5102;
    }

    @XmlElement(name = "P510_2ESP")
    public String getP5102esp() {
        return p5102esp;
    }

    public void setP5102esp(String p5102esp) {
        this.p5102esp = p5102esp;
    }

    @XmlElement(name = "P511")
    public String getP511() {
        return p511;
    }

    public void setP511(String p511) {
        this.p511 = p511;
    }

    @XmlElement(name = "P511_ESP")
    public String getP511Esp() {
        return p511Esp;
    }

    public void setP511Esp(String p511Esp) {
        this.p511Esp = p511Esp;
    }

    @XmlElement(name = "P512")
    public String getP512() {
        return p512;
    }

    public void setP512(String p512) {
        this.p512 = p512;
    }

    @XmlElement(name = "P512_SI")
    public Integer getP512Si() {
        return p512Si;
    }

    public void setP512Si(Integer p512Si) {
        this.p512Si = p512Si;
    }

    @XmlElement(name = "P512_NO1")
    public Integer getP512No1() {
        return p512No1;
    }

    public void setP512No1(Integer p512No1) {
        this.p512No1 = p512No1;
    }

    @XmlElement(name = "P512_NO2")
    public Integer getP512No2() {
        return p512No2;
    }

    public void setP512No2(Integer p512No2) {
        this.p512No2 = p512No2;
    }

    @XmlElement(name = "P513")
    public String getP513() {
        return p513;
    }

    public void setP513(String p513) {
        this.p513 = p513;
    }

    @XmlElement(name = "P514")
    public String getP514() {
        return p514;
    }

    public void setP514(String p514) {
        this.p514 = p514;
    }

    @XmlElement(name = "P514_1")
    public String getP5141() {
        return p5141;
    }

    public void setP5141(String p5141) {
        this.p5141 = p5141;
    }

    @XmlElement(name = "P514_2")
    public String getP5142() {
        return p5142;
    }

    public void setP5142(String p5142) {
        this.p5142 = p5142;
    }

    @XmlElement(name = "P514_2ESP")
    public String getP5142esp() {
        return p5142esp;
    }

    public void setP5142esp(String p5142esp) {
        this.p5142esp = p5142esp;
    }

    @XmlElement(name = "P515_11")
    public String getP51511() {
        return p51511;
    }

    public void setP51511(String p51511) {
        this.p51511 = p51511;
    }

    @XmlElement(name = "P515_12")
    public String getP51512() {
        return p51512;
    }

    public void setP51512(String p51512) {
        this.p51512 = p51512;
    }

    @XmlElement(name = "P515_13")
    public String getP51513() {
        return p51513;
    }

    public void setP51513(String p51513) {
        this.p51513 = p51513;
    }

    @XmlElement(name = "P515_21")
    public String getP51521() {
        return p51521;
    }

    public void setP51521(String p51521) {
        this.p51521 = p51521;
    }

    @XmlElement(name = "P515_22")
    public String getP51522() {
        return p51522;
    }

    public void setP51522(String p51522) {
        this.p51522 = p51522;
    }

    @XmlElement(name = "P515_23")
    public String getP51523() {
        return p51523;
    }

    public void setP51523(String p51523) {
        this.p51523 = p51523;
    }

    @XmlElement(name = "P515_31")
    public String getP51531() {
        return p51531;
    }

    public void setP51531(String p51531) {
        this.p51531 = p51531;
    }

    @XmlElement(name = "P515_32")
    public String getP51532() {
        return p51532;
    }

    public void setP51532(String p51532) {
        this.p51532 = p51532;
    }

    @XmlElement(name = "P515_33")
    public String getP51533() {
        return p51533;
    }

    public void setP51533(String p51533) {
        this.p51533 = p51533;
    }

    @XmlElement(name = "P516")
    public String getP516() {
        return p516;
    }

    public void setP516(String p516) {
        this.p516 = p516;
    }

    @XmlElement(name = "P517")
    public String getP517() {
        return p517;
    }

    public void setP517(String p517) {
        this.p517 = p517;
    }

    @XmlElement(name = "P518")
    public String getP518() {
        return p518;
    }

    public void setP518(String p518) {
        this.p518 = p518;
    }

    @XmlElement(name = "P518_QN")
    public Integer getP518Qn() {
        return p518Qn;
    }

    public void setP518Qn(Integer p518Qn) {
        this.p518Qn = p518Qn;
    }

    @XmlElement(name = "P521")
    public String getP521() {
        return p521;
    }

    public void setP521(String p521) {
        this.p521 = p521;
    }

    @XmlElement(name = "P522")
    public String getP522() {
        return p522;
    }

    public void setP522(String p522) {
        this.p522 = p522;
    }

    @XmlElement(name = "P522_ESP")
    public String getP522Esp() {
        return p522Esp;
    }

    public void setP522Esp(String p522Esp) {
        this.p522Esp = p522Esp;
    }

    @XmlElement(name = "P523")
    public String getP523() {
        return p523;
    }

    public void setP523(String p523) {
        this.p523 = p523;
    }

    @XmlElement(name = "P523_ESP")
    public String getP523Esp() {
        return p523Esp;
    }

    public void setP523Esp(String p523Esp) {
        this.p523Esp = p523Esp;
    }

    @XmlElement(name = "P525")
    public String getP525() {
        return p525;
    }

    public void setP525(String p525) {
        this.p525 = p525;
    }

    @XmlElement(name = "P701")
    public String getP701() {
        return p701;
    }

    public void setP701(String p701) {
        this.p701 = p701;
    }

    @XmlElement(name = "P701_1")
    public String getP7011() {
        return p7011;
    }

    public void setP7011(String p7011) {
        this.p7011 = p7011;
    }

    @XmlElement(name = "P701_2")
    public String getP7012() {
        return p7012;
    }

    public void setP7012(String p7012) {
        this.p7012 = p7012;
    }

    @XmlElement(name = "P701_3")
    public String getP7013() {
        return p7013;
    }

    public void setP7013(String p7013) {
        this.p7013 = p7013;
    }

    @XmlElement(name = "P701_4")
    public String getP7014() {
        return p7014;
    }

    public void setP7014(String p7014) {
        this.p7014 = p7014;
    }

    @XmlElement(name = "P701_5")
    public String getP7015() {
        return p7015;
    }

    public void setP7015(String p7015) {
        this.p7015 = p7015;
    }

    @XmlElement(name = "P701_6")
    public String getP7016() {
        return p7016;
    }

    public void setP7016(String p7016) {
        this.p7016 = p7016;
    }

    @XmlElement(name = "P701_7")
    public String getP7017() {
        return p7017;
    }

    public void setP7017(String p7017) {
        this.p7017 = p7017;
    }

    @XmlElement(name = "P701_8")
    public String getP7018() {
        return p7018;
    }

    public void setP7018(String p7018) {
        this.p7018 = p7018;
    }

    @XmlElement(name = "P701_9")
    public String getP7019() {
        return p7019;
    }

    public void setP7019(String p7019) {
        this.p7019 = p7019;
    }

    @XmlElement(name = "P701_10")
    public String getP70110() {
        return p70110;
    }

    public void setP70110(String p70110) {
        this.p70110 = p70110;
    }

    @XmlElement(name = "P701_11")
    public String getP70111() {
        return p70111;
    }

    public void setP70111(String p70111) {
        this.p70111 = p70111;
    }

    @XmlElement(name = "P701_12")
    public String getP70112() {
        return p70112;
    }

    public void setP70112(String p70112) {
        this.p70112 = p70112;
    }

    @XmlElement(name = "P701_13")
    public String getP70113() {
        return p70113;
    }

    public void setP70113(String p70113) {
        this.p70113 = p70113;
    }

    @XmlElement(name = "P701_14")
    public String getP70114() {
        return p70114;
    }

    public void setP70114(String p70114) {
        this.p70114 = p70114;
    }

    @XmlElement(name = "P701_15")
    public String getP70115() {
        return p70115;
    }

    public void setP70115(String p70115) {
        this.p70115 = p70115;
    }

    @XmlElement(name = "P701_16")
    public String getP70116() {
        return p70116;
    }

    public void setP70116(String p70116) {
        this.p70116 = p70116;
    }

    @XmlElement(name = "P701_17")
    public String getP70117() {
        return p70117;
    }

    public void setP70117(String p70117) {
        this.p70117 = p70117;
    }

    @XmlElement(name = "P701_18")
    public String getP70118() {
        return p70118;
    }

    public void setP70118(String p70118) {
        this.p70118 = p70118;
    }

    @XmlElement(name = "P701_19")
    public String getP70119() {
        return p70119;
    }

    public void setP70119(String p70119) {
        this.p70119 = p70119;
    }

    @XmlElement(name = "P701_20")
    public String getP70120() {
        return p70120;
    }

    public void setP70120(String p70120) {
        this.p70120 = p70120;
    }

    @XmlElement(name = "P701_ESP")
    public String getP701Esp() {
        return p701Esp;
    }

    public void setP701Esp(String p701Esp) {
        this.p701Esp = p701Esp;
    }

    @XmlElement(name = "P702")
    public String getP702() {
        return p702;
    }

    public void setP702(String p702) {
        this.p702 = p702;
    }

    @XmlElement(name = "P703_1")
    public String getP7031() {
        return p7031;
    }

    public void setP7031(String p7031) {
        this.p7031 = p7031;
    }

    @XmlElement(name = "P703_2")
    public String getP7032() {
        return p7032;
    }

    public void setP7032(String p7032) {
        this.p7032 = p7032;
    }

    @XmlElement(name = "P703_3")
    public String getP7033() {
        return p7033;
    }

    public void setP7033(String p7033) {
        this.p7033 = p7033;
    }

    @XmlElement(name = "P703_4")
    public String getP7034() {
        return p7034;
    }

    public void setP7034(String p7034) {
        this.p7034 = p7034;
    }

    @XmlElement(name = "P703_5")
    public String getP7035() {
        return p7035;
    }

    public void setP7035(String p7035) {
        this.p7035 = p7035;
    }

    @XmlElement(name = "P703_6")
    public String getP7036() {
        return p7036;
    }

    public void setP7036(String p7036) {
        this.p7036 = p7036;
    }

    @XmlElement(name = "P703_ESP")
    public String getP703Esp() {
        return p703Esp;
    }

    public void setP703Esp(String p703Esp) {
        this.p703Esp = p703Esp;
    }

    @XmlElement(name = "P704")
    public String getP704() {
        return p704;
    }

    public void setP704(String p704) {
        this.p704 = p704;
    }

    @XmlElement(name = "P705_1")
    public String getP7051() {
        return p7051;
    }

    public void setP7051(String p7051) {
        this.p7051 = p7051;
    }

    @XmlElement(name = "P705_2")
    public String getP7052() {
        return p7052;
    }

    public void setP7052(String p7052) {
        this.p7052 = p7052;
    }

    @XmlElement(name = "P705_3")
    public String getP7053() {
        return p7053;
    }

    public void setP7053(String p7053) {
        this.p7053 = p7053;
    }

    @XmlElement(name = "P705_4")
    public String getP7054() {
        return p7054;
    }

    public void setP7054(String p7054) {
        this.p7054 = p7054;
    }

    @XmlElement(name = "P705_5")
    public String getP7055() {
        return p7055;
    }

    public void setP7055(String p7055) {
        this.p7055 = p7055;
    }

    @XmlElement(name = "P705_ESP")
    public String getP705Esp() {
        return p705Esp;
    }

    public void setP705Esp(String p705Esp) {
        this.p705Esp = p705Esp;
    }

    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    //@XmlElement(name = "ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    //@XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @XmlElement(name = "RESPONSABLE")
    public List<Local2018Resp> getLocal2018RespList() {
        return local2018RespList;
    }

    public void setLocal2018RespList(List<Local2018Resp> local2018RespList) {
        this.local2018RespList = local2018RespList;
    }

    @XmlElement(name = "SEC400")
    public List<Local2018Sec400> getLocal2018Sec400List() {
        return local2018Sec400List;
    }

    public void setLocal2018Sec400List(List<Local2018Sec400> local2018Sec400List) {
        this.local2018Sec400List = local2018Sec400List;
    }

    @XmlElement(name = "SEC500")
    public List<Local2018Sec500> getLocal2018Sec500List() {
        return local2018Sec500List;
    }

    public void setLocal2018Sec500List(List<Local2018Sec500> local2018Sec500List) {
        this.local2018Sec500List = local2018Sec500List;
    }

    @XmlElement(name = "SEC203")
    public List<Local2018Sec203> getLocal2018Sec203List() {
        return local2018Sec203List;
    }

    public void setLocal2018Sec203List(List<Local2018Sec203> local2018Sec203List) {
        this.local2018Sec203List = local2018Sec203List;
    }

    @XmlElement(name = "SEC114")
    public List<Local2018Sec114> getLocal2018Sec114List() {
        return local2018Sec114List;
    }

    public void setLocal2018Sec114List(List<Local2018Sec114> local2018Sec114List) {
        this.local2018Sec114List = local2018Sec114List;
    }

    @XmlElement(name = "SEC600")
    public List<Local2018Sec600> getLocal2018Sec600List() {
        return local2018Sec600List;
    }

    public void setLocal2018Sec600List(List<Local2018Sec600> local2018Sec600List) {
        this.local2018Sec600List = local2018Sec600List;
    }

    @XmlElement(name = "SEC300")
    public List<Local2018Sec300> getLocal2018Sec300List() {
        return local2018Sec300List;
    }

    public void setLocal2018Sec300List(List<Local2018Sec300> local2018Sec300List) {
        this.local2018Sec300List = local2018Sec300List;
    }

    @XmlAttribute(name = "ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlAttribute(name = "MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2018Cabecera)) {
            return false;
        }
        Local2018Cabecera other = (Local2018Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Local2018Cabecera[idEnvio=" + idEnvio + "]";
    }

}
