/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "matricula2019_carreras")
public class Matricula2019Carreras implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NRO")
    private String nro;
    @Basic(optional = false)
    @Column(name = "NROCED")
    private String nroced;
    @Basic(optional = false)
    @Column(name = "CUADRO")
    private String cuadro;
    @Column(name = "CODCARR")
    private String codcarr;
    @Column(name = "CODFAMPRO")
    private String codfampro;
    @Basic(optional = false)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "NIVEL_CAR")
    private String nivelCar;
    @Column(name = "NRO_RES")
    private String nroRes;
    @Column(name = "FECHA_RES")
    private String fechaRes;
    @Column(name = "DURACION")
    private String duracion;
    @Column(name = "NHORAS")
    private Integer nhoras;
    @Column(name = "NCREDITOS")
    private Integer ncreditos;
    @Column(name = "NSEC_MA")
    private Integer nsecMa;
    @Column(name = "NSEC_TA")
    private Integer nsecTa;
    @Column(name = "NSEC_NA")
    private Integer nsecNa;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2019Cabecera matricula2019Cabecera;
    
 
    
    
    
    
    
    public Matricula2019Carreras() {
    }

    public Matricula2019Carreras(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2019Carreras(Long idEnvio, String nro, String nroced, String cuadro, String descrip) {
        this.idEnvio = idEnvio;
        this.nro = nro;
        this.nroced = nroced;
        this.cuadro = cuadro;
        this.descrip = descrip;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlElement(name = "CODCARR")
    public String getCodcarr() {
        return codcarr;
    }

    public void setCodcarr(String codcarr) {
        this.codcarr = codcarr;
    }

    @XmlElement(name = "DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name = "NRO_RES")
    public String getNroRes() {
        return nroRes;
    }

    public void setNroRes(String nroRes) {
        this.nroRes = nroRes;
    }

    @XmlElement(name = "FECHA_RES")
    public String getFechaRes() {
        return fechaRes;
    }

    public void setFechaRes(String fechaRes) {
        this.fechaRes = fechaRes;
    }
    
    @XmlElement(name = "DURACION")
    public String getDuracion() {
        return duracion;
    }

    
    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

   
    @XmlElement(name = "CODFAMPRO")
    public String getCodfampro() {
		return codfampro;
	}

	public void setCodfampro(String codfampro) {
		this.codfampro = codfampro;
	}
	@XmlElement(name = "NIVEL_CAR")
	public String getNivelCar() {
		return nivelCar;
	}

	public void setNivelCar(String nivelCar) {
		this.nivelCar = nivelCar;
	}
	@XmlElement(name = "NHORAS")
	public Integer getNhoras() {
		return nhoras;
	}

	public void setNhoras(Integer nhoras) {
		this.nhoras = nhoras;
	}
	@XmlElement(name = "NCREDITOS")
	public Integer getNcreditos() {
		return ncreditos;
	}

	public void setNcreditos(Integer ncreditos) {
		this.ncreditos = ncreditos;
	}
	@XmlElement(name = "NSEC_MA")
	public Integer getNsecMa() {
		return nsecMa;
	}

	public void setNsecMa(Integer nsecMa) {
		this.nsecMa = nsecMa;
	}
	@XmlElement(name = "NSEC_TA")
	public Integer getNsecTa() {
		return nsecTa;
	}

	public void setNsecTa(Integer nsecTa) {
		this.nsecTa = nsecTa;
	}
	@XmlElement(name = "NSEC_NA")
	public Integer getNsecNa() {
		return nsecNa;
	}

	public void setNsecNa(Integer nsecNa) {
		this.nsecNa = nsecNa;
	}

	@XmlTransient
    public Matricula2019Cabecera getMatricula2019Cabecera() {
        return matricula2019Cabecera;
    }

    public void setMatricula2019Cabecera(Matricula2019Cabecera matricula2019Cabecera) {
        this.matricula2019Cabecera = matricula2019Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2019Carreras)) {
            return false;
        }
        Matricula2019Carreras other = (Matricula2019Carreras) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Carreras[idEnvio=" + idEnvio + "]";
    }

}
