package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2019_sec112")
public class Local2019Sec112 implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P112_NRO")
    private Integer p112Nro;
    @Column(name = "P112_CM")
    private String p112Cm;
    @Column(name = "P112_ANX")
    private String p112Anx;
    @Column(name = "P112_IE")
    private String p112Ie;
    @Column(name = "P112_GES")
    private String p112Ges;
    @Column(name = "P112_NM")
    private String p112Nm;
    @Column(name = "P112_TUR")
    private String p112Tur;
    
    
    
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2019Cabecera local2019Cabecera;

    public Local2019Sec112() {
    }

    public Local2019Sec112(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    @XmlElement(name = "P112_NRO")
    public Integer getP112Nro() {
        return p112Nro;
    }

    public void setP112Nro(Integer p112Nro) {
        this.p112Nro = p112Nro;
    }
    @XmlElement(name = "P112_CM")
    public String getP112Cm() {
        return p112Cm;
    }

    public void setP112Cm(String p112Cm) {
        this.p112Cm = p112Cm;
    }
    @XmlElement(name = "P112_ANX")
    public String getP112Anx() {
        return p112Anx;
    }

    public void setP112Anx(String p112Anx) {
        this.p112Anx = p112Anx;
    }
    @XmlElement(name = "P112_IE")
    public String getP112Ie() {
        return p112Ie;
    }

    public void setP112Ie(String p112Ie) {
        this.p112Ie = p112Ie;
    }
    @XmlElement(name = "P112_GES")
    public String getP112Ges() {
        return p112Ges;
    }

    public void setP112Ges(String p112Ges) {
        this.p112Ges = p112Ges;
    }
    @XmlElement(name = "P112_NM")
    public String getP112Nm() {
        return p112Nm;
    }

    public void setP112Nm(String p112Nm) {
        this.p112Nm = p112Nm;
    }
    @XmlElement(name = "P112_TUR")
    public String getP112Tur() {
        return p112Tur;
    }

    public void setP112Tur(String p112Tur) {
        this.p112Tur = p112Tur;
    }

    
    @XmlTransient
    public Local2019Cabecera getLocal2019Cabecera() {
        return local2019Cabecera;
    }

    public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
        this.local2019Cabecera = local2019Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2019Sec112)) {
            return false;
        }
        Local2019Sec112 other = (Local2019Sec112) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.entidad2019.Local2019Sec112[idEnvio=" + idEnvio + "]";
    }
	
}
