package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2019_sec611")
public class Local2019Sec611 implements Serializable {
	private static final long serialVersionUID = 1L;
	

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;

	@Column(name = "CUADRO")
    private String cuadro;
	
    @Column(name = "P611_1")
    private Integer p6111;
    @Column(name = "P611_2")
    private String p6112;
    @Column(name = "P611_3")
    private String p6113;
    @Column(name = "P611_4")
    private Integer p6114;
    @Column(name = "P611_5")
    private String p6115;
    @Column(name = "P611_6")
    private Integer p6116;
    @Column(name = "P611_7")
    private String p6117;
    @Column(name = "P611_8")
    private Integer p6118;
    
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2019Cabecera local2019Cabecera;

    public Local2019Sec611() {
    }

    public Local2019Sec611(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }
    
    @XmlElement(name = "P611_1")
    public Integer getP6111() {
        return p6111;
    }

    public void setP6111(Integer p6111) {
        this.p6111 = p6111;
    }
    @XmlElement(name = "P611_2")
    public String getP6112() {
        return p6112;
    }

    public void setP6112(String p6112) {
        this.p6112 = p6112;
    }
    @XmlElement(name = "P611_3")
    public String getP6113() {
        return p6113;
    }

    public void setP6113(String p6113) {
        this.p6113 = p6113;
    }
    @XmlElement(name = "P611_4")
    public Integer getP6114() {
        return p6114;
    }

    public void setP6114(Integer p6114) {
        this.p6114 = p6114;
    }
    @XmlElement(name = "P611_5")
    public String getP6115() {
        return p6115;
    }

    public void setP6115(String p6115) {
        this.p6115 = p6115;
    }
    @XmlElement(name = "P611_6")
    public Integer getP6116() {
        return p6116;
    }

    public void setP6116(Integer p6116) {
        this.p6116 = p6116;
    }
    @XmlElement(name = "P611_7")
    public String getP6117() {
        return p6117;
    }

    public void setP6117(String p6117) {
        this.p6117 = p6117;
    }
    @XmlElement(name = "P611_8")
    public Integer getP6118() {
        return p6118;
    }

    public void setP6118(Integer p6118) {
        this.p6118 = p6118;
    }

    @XmlTransient
    public Local2019Cabecera getLocal2019Cabecera() {
        return local2019Cabecera;
    }

    public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
        this.local2019Cabecera = local2019Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2019Sec611)) {
            return false;
        }
        Local2019Sec611 other = (Local2019Sec611) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.entidad2019.Local2019Sec611[idEnvio=" + idEnvio + "]";
    }
	
}
