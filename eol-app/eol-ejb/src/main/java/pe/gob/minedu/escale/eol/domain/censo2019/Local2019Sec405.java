package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2019_sec405")
public class Local2019Sec405 implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;

	@Column(name = "P405_NRO")
	private int p405Nro;
	@Column(name = "P405_ESP")
	private String p405Esp;
	@Column(name = "P405_1")
	private String p4051;
	@Column(name = "P405_2")
	private Integer p4052;
	@Column(name = "P405_3")
	private Integer p4053;
	@Column(name = "P405_4")
	private Integer p4054;
	@Column(name = "P405_5")
	private Integer p4055;
	@Column(name = "P405_6")
	private Integer p4056;

	@JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	@ManyToOne
	private Local2019Cabecera local2019Cabecera;

	public Local2019Sec405() {
	}

	public Local2019Sec405(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Local2019Sec405(Long idEnvio, int p405Nro) {
		this.idEnvio = idEnvio;
		this.p405Nro = p405Nro;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "P405_NRO")
	public int getP405Nro() {
		return p405Nro;
	}

	public void setP405Nro(int p405Nro) {
		this.p405Nro = p405Nro;
	}

	@XmlElement(name = "P405_ESP")
	public String getP405Esp() {
		return p405Esp;
	}

	public void setP405Esp(String p405Esp) {
		this.p405Esp = p405Esp;
	}

	@XmlElement(name = "P405_1")
	public String getP4051() {
		return p4051;
	}

	public void setP4051(String p4051) {
		this.p4051 = p4051;
	}

	@XmlElement(name = "P405_2")
	public Integer getP4052() {
		return p4052;
	}

	public void setP4052(Integer p4052) {
		this.p4052 = p4052;
	}

	@XmlElement(name = "P405_3")
	public Integer getP4053() {
		return p4053;
	}

	public void setP4053(Integer p4053) {
		this.p4053 = p4053;
	}

	@XmlElement(name = "P405_4")
	public Integer getP4054() {
		return p4054;
	}

	public void setP4054(Integer p4054) {
		this.p4054 = p4054;
	}

	@XmlElement(name = "P405_5")
	public Integer getP4055() {
		return p4055;
	}

	public void setP4055(Integer p4055) {
		this.p4055 = p4055;
	}

	@XmlElement(name = "P405_6")
	public Integer getP4056() {
		return p4056;
	}

	public void setP4056(Integer p4056) {
		this.p4056 = p4056;
	}

	@XmlTransient
	public Local2019Cabecera getLocal2019Cabecera() {
		return local2019Cabecera;
	}

	public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
		this.local2019Cabecera = local2019Cabecera;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Local2019Sec405)) {
			return false;
		}
		Local2019Sec405 other = (Local2019Sec405) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Local2019Sec405 [idEnvio=" + idEnvio + "]";
	}

}
