package pe.gob.minedu.escale.eol.domain;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

//import pe.gob.minedu.escale.rest.client.Constantes;

import org.apache.log4j.Logger;

import net.sf.jasperreports.web.util.WebResource;
import pe.gob.minedu.escale.eol.client.constantes.Constantes;




/**
 * Jersey REST client generated for REST resource:application
 * [instituciones/{cod_mod}/{anexo}/]<br>
 * USAGE:
 * 
 * <pre>
 *        PadronClient client = new PadronClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 * 
 * @author DSILVA
 */
public class PadronClient {
	// private WebResource webResource;

	private Logger logger = Logger.getLogger(PadronClient.class);
	
	
	
	private WebTarget target;
	private Client client;

	public PadronClient() {
		client = ClientBuilder.newClient();
		String resourcePath = "instituciones";
//		target = client.target(Constantes.BASE_URI).path(resourcePath);

		logger.info("  Constantes.BASE_URI=" + Constantes.BASE_URI + "; resourcePath: " + resourcePath);

		logger.info(":: PadronClient.constructor :: Execution finish.");
	}

	public PadronClient(String cod_mod, String anexo) {
		logger.info("CONSTRUCTOR");
		client = ClientBuilder.newClient();
//		Object[] arguments =new Object[] {cod_mod , anexo };
//		String resourcePath = java.text.MessageFormat.format("instituciones/{0}/{1}", arguments );
//		target = client.target(Constantes.BASE_URI).path("instituciones");
//		logger.info(" getUri 1111: " + target);
//		target=client.target(Constantes.BASE_URI);
//		target = target.queryParam("codMod", cod_mod);
////		logger.info(" getUri  2222: " + target.getUri());
//		target = target.queryParam("anexo", anexo);
//		logger.info(" getUri 3333: " + target.getUri());
//		logger.info(" getUri 44444: " + target.request(MediaType.APPLICATION_XML));
//		logger.info(" getUri 55555: " + target.request(MediaType.APPLICATION_JSON));
		
		
		
//		logger.info(" getUri 666666: " + 
//		client.target(Constantes.BASE_URI).path("instituciones").request(MediaType.APPLICATION_JSON)
//		.get(InstitucionEducativaIE.class) + " " );
//		target = client.target(Constantes.BASE_URI).path("instituciones");//.request(MediaType.APPLICATION_JSON).get(InstitucionEducativaIE.class);
		
		WebTarget target=client.target(Constantes.BASE_URI).path("instituciones");
		target = target.queryParam("codMod", cod_mod);
		target = target.queryParam("anexo", anexo);
//		target.request(MediaType.APPLICATION_XML).get(new javax.ws.rs.core.GenericType<InstitucionesConverter>() {
//		});
 
	}

//	public EolCedulasConverter getCedulasConverter(String nivel, String periodo, String nombreActividad) {
//		WebTarget target = client.target(Constantes.BASE_URI_EOL_ADMIN).path("/get/actividades");
//		target = target.queryParam("nivel", nivel);
//		target = target.queryParam("periodo", periodo);
//		target = target.queryParam("nombreActividad", nombreActividad);
//		logger.info(" URL: " + target.getUri().toString());
//		return target.request(MediaType.APPLICATION_XML).get(new javax.ws.rs.core.GenericType<EolCedulasConverter>() {
//		});
//	}

	// //private static final String BASE_URI =
	// "http://escale.minedu.gob.pe/padron/rest/";
	// // private static final String BASE_URI =
	// "http://localhost:8080/padron/rest/";
	// public PadronClient() {
	// com.sun.jersey.api.client.config.ClientConfig config = new
	// com.sun.jersey.api.client.config.DefaultClientConfig();
	// client = Client.create(config);
	// //String resourcePath =
	// java.text.MessageFormat.format("instituciones/{0}/{1}", new
	// Object[]{cod_mod, anexo});
	// String resourcePath="instituciones";
	// webResource = client.resource(Constantes.BASE_URI).path(resourcePath);
	// }
	// public PadronClient(String cod_mod, String anexo) {
	// com.sun.jersey.api.client.config.ClientConfig config = new
	// com.sun.jersey.api.client.config.DefaultClientConfig();
	// client = Client.create(config);
	// String resourcePath =
	// java.text.MessageFormat.format("instituciones/{0}/{1}", new
	// Object[]{cod_mod, anexo});
	// webResource = client.resource(Constantes.BASE_URI).path(resourcePath);
	// }
	//
	// public void setResourcePath(String cod_mod, String anexo) {
	// String resourcePath =
	// java.text.MessageFormat.format("instituciones/{0}/{1}", new
	// Object[]{cod_mod, anexo});
	// webResource = client.resource(Constantes.BASE_URI).path(resourcePath);
	// }
	//
	// /**
	// * @param responseType Class representing the response
	// * @param optionalQueryParams List of optional query parameters in the
	// form of "param_name=param_value",...<br>
	// * List of optional query parameters:
	// * <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "3"]
	// * @return response object (instance of responseType class)
	// */
	// public <T> T get_XML(Class<T> responseType, String...
	// optionalQueryParams) throws UniformInterfaceException {
	// return
	// webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
	// }
	//
	/**
	 * @param responseType        Class representing the response
	 * @param optionalQueryParams List of optional query parameters in the form of
	 *                            "param_name=param_value",...<br>
	 *                            List of optional query parameters:
	 *                            <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "3"]
	 * @return response object (instance of responseType class)
	 */
	public <T> T get_JSON(Class<T> responseType, String... optionalQueryParams) throws Exception {
		logger.info("JSON : ----> " + responseType     ); //pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE
		client = ClientBuilder.newClient();
		logger.info("JSON 0000000 : ----> "   + client   );
		String resourcePath = "instituciones";
		
//		T y=client.target(Constantes.BASE_URI).path(resourcePath).request(MediaType.APPLICATION_XML).get(responseType);
//		logger.info("JSON 11111: --->" + y );
		
		T x=client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath).request(MediaType.APPLICATION_JSON).get(responseType);
		logger.info("JSON 22222: --->" + x );
		return x;

	}
//	   public <T> T get_JSON(Class<T> responseType, String... optionalQueryParams) throws Exception {
//
//	        return webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
//
//
//	    }

	private MultivaluedMap getQParams(String... optionalParams) {
		MultivaluedMap<String, String> qParams = new MultivaluedHashMap<>();
		for (String qParam : optionalParams) {
			String[] qPar = qParam.split("=");
			if (qPar.length > 1) {
				qParams.add(qPar[0], qPar[1]);
			}
		}
		return qParams;
	}

}
