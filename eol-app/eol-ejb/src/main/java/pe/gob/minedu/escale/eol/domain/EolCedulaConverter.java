package pe.gob.minedu.escale.eol.domain;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author WARODRIGUEZ
 *
 */
@XmlRootElement(name = "cedula")
public class EolCedulaConverter {
	private Long id;
	private String nombre;
	private boolean estado;
	private String nivel;
	private String version;
	private boolean envio = false;
	private Date fechaEnvio;
	private String strFechaEnvio;
	private Integer nroEnvio;

	private EolActividadConverter actividad;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public EolActividadConverter getActividad() {
		return actividad;
	}

	public void setActividad(EolActividadConverter actividad) {
		this.actividad = actividad;
	}

	public boolean isEnvio() {
		return envio;
	}

	public void setEnvio(boolean envio) {
		this.envio = envio;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Integer getNroEnvio() {
		return nroEnvio;
	}

	public void setNroEnvio(Integer nroEnvio) {
		this.nroEnvio = nroEnvio;
	}

	public String getStrFechaEnvio() {
		return strFechaEnvio;
	}

	public void setStrFechaEnvio(String strFechaEnvio) {
		this.strFechaEnvio = strFechaEnvio;
	}

}
