/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2018;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JBEDRILLANA
 */
@XmlRootElement(name = "cedulaMatricula")
//@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "matricula2018_cabecera")
public class Matricula2018Cabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03AP = "c03ap";
	public final static String CEDULA_03AS = "c03as";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_04AI = "c04ai";
	public final static String CEDULA_04AA = "c04aa";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08AI = "c08ai";
	public final static String CEDULA_08AP = "c08ap";
	public final static String CEDULA_09A = "c09a";
	public final static String CEDULA_CIRE = "cire";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;
	
	@Column(name = "NROCED")
	private String nroced;
	
	@Column(name = "COD_MOD")
	private String codMod;
	
	@Column(name = "ANEXO")
	private String anexo;
	
	@Column(name = "DNI_CORD")
	private String dniCord;
	
	@Column(name = "CEN_EDU")
	private String cenEdu;
	
	@Column(name = "CODLOCAL")
	private String codlocal;
	
	@Column(name = "DISTRITO")
	private String distrito;
	
	@Column(name = "METATEN_1")
	private String metaten1;
	
	@Column(name = "METATEN_2")
	private String metaten2;
	
	@Column(name = "METATEN_3")
	private String metaten3;
	
	@Column(name = "METATEN_4")
	private String metaten4;
	
	@Column(name = "METATEN_5")
	private String metaten5;
	
	@Column(name = "MODATEN")
	private String modaten;
	
	@Column(name = "STUTORIAL")
	private String stutorial;
	
	@Column(name = "LOCALIDAD")
	private String localidad;
	
	@Column(name = "EGESTORA")
	private String egestora;
	
	@Column(name = "TIPOFINA_1")
	private String tipofina1;
	
	@Column(name = "TIPOFINA_2")
	private String tipofina2;
	
	@Column(name = "TIPOFINA_3")
	private String tipofina3;
	
	@Column(name = "TIPOFINA_4")
	private String tipofina4;
	
	@Column(name = "TIPOPROG")
	private String tipoprog;
	
	@Column(name = "NRORD_CRE")
	private String nrordCre;
	
	@Column(name = "FECRES_C_D")
	private String fecresCD;
	
	@Column(name = "FECRES_C_M")
	private String fecresCM;
	
	@Column(name = "FECRES_C_A")
	private String fecresCA;
	
	@Column(name = "NRORD_REN")
	private String nrordRen;
	@Column(name = "FECRES_R_D")
	private String fecresRD;
	
	@Column(name = "FECRES_R_M")
	private String fecresRM;
	
	@Column(name = "FECRES_R_A")
	private String fecresRA;
	
	@Column(name = "NIV_MOD")
	private String nivMod;
	
	@Column(name = "FORMATEN")
	private String formaten;
	
	@Column(name = "ALUM_02IC")
	private String alum02ic;
	
	@Column(name = "GESTION")
	private String gestion;
	
	@Column(name = "CODUGEL")
	private String codugel;
	
	@Column(name = "TIPOIESUP")
	private String tipoiesup;
	
	@Column(name = "PESTAB_C6A")
	private Short pestabC6a;
	
	@Column(name = "P101_C5A")
	private String p101C5a;
	
	@Column(name = "P101_C5A_I")
	private String p101C5aI;
	
	@Column(name = "P101_C5A_P")
	private String p101C5aP;
	
	@Column(name = "P101_C5A_S")
	private String p101C5aS;
	
	@Column(name = "P102_C5A")
	private String p102C5a;
	
	@Column(name = "P102_C5A_1")
	private String p102C5a1;
	
	@Column(name = "P1021_C6A")
	private String p1021C6a;
	@Column(name = "P1021_C6A1")
	private String p1021C6a1;
	
	@Column(name = "P1021_C6A2")
	private String p1021C6a2;
	
	@Column(name = "P1021_C6A3")
	private String p1021C6a3;
	
	@Column(name = "P1022_C6A1")
	private String p1022C6a1;
	
	@Column(name = "P1022_C6A2")
	private String p1022C6a2;
	
	@Column(name = "P1022_C6A3")
	private String p1022C6a3;
	
	@Column(name = "P102_C7A")
	private String p102C7a;
	
	@Column(name = "P102_C7A_1")
	private String p102C7a1;
	
	@Column(name = "P102_C7A_2")
	private String p102C7a2;
	
	@Column(name = "P102_C7A_3")
	private String p102C7a3;
	
	@Column(name = "P102_C7A_4")
	private String p102C7a4;
	
	@Column(name = "P102_C7A_5")
	private String p102C7a5;
	
	@Column(name = "P102_C7A_6")
	private String p102C7a6;
	
	@Column(name = "P102_C7A_E")
	private String p102C7aE;
	
	@Column(name = "P401_C8A")
	private String p401C8a;
	
	@Column(name = "P101_C1")
	private String p101C1;
	
	@Column(name = "P101_C1_A")
	private String p101C1A;
	
	@Column(name = "INI_CLAS_D")
	private String iniClasD;
	
	@Column(name = "INI_CLAS_M")
	private String iniClasM;
	
	@Column(name = "P101_C2_1")
	private String p101C21;
	
	@Column(name = "P101_C2_2")
	private String p101C22;
	
	@Column(name = "P101_C2_3")
	private String p101C23;
	
	@Column(name = "P101_C2_4")
	private String p101C24;
	@Column(name = "FIN_CLAS_D")
	private String finClasD;
	@Column(name = "FIN_CLAS_M")
	private String finClasM;
	@Column(name = "P103_C2_1")
	private String p103C21;
	@Column(name = "P103_C2_2")
	private String p103C22;
	@Column(name = "P103_C2_3")
	private String p103C23;
	@Column(name = "P103_C2_4")
	private String p103C24;
	@Column(name = "P103_C2_5")
	private String p103C25;
	@Column(name = "P103_C2_6")
	private String p103C26;
	@Column(name = "PL_TUTORIA")
	private String plTutoria;
	@Column(name = "P104_C3P3S")
	private String p104C3p3s;
	@Column(name = "P105_C3P3S")
	private String p105C3p3s;
	@Column(name = "P106_C3P3S")
	private String p106C3p3s;
	@Column(name = "P107_C3_01")
	private String p107C301;
	@Column(name = "P107_C3_02")
	private String p107C302;
	@Column(name = "P107_C3_03")
	private String p107C303;
	@Column(name = "P107_C3_04")
	private String p107C304;
	@Column(name = "P107_C3_05")
	private String p107C305;
	@Column(name = "P107_C3_06")
	private String p107C306;
	@Column(name = "P107_C3_07")
	private String p107C307;
	@Column(name = "P107_C3_08")
	private String p107C308;
	@Column(name = "P107_C3_09")
	private String p107C309;
	@Column(name = "P107_C3_10")
	private String p107C310;
	@Column(name = "P107_C3_11")
	private String p107C311;
	@Column(name = "P107_C3_12")
	private String p107C312;
	@Column(name = "P107_C3_13")
	private String p107C313;
	@Column(name = "P107_C3_14")
	private String p107C314;
	@Column(name = "P107_C314E")
	private String p107C314e;
	@Column(name = "PAT_2018")
	private String pat2018;
	@Column(name = "P106_C1")
	private String p106C1;
	@Column(name = "P106_C1_QN")
	private Short p106C1Qn;
	@Column(name = "N_CONV_1")
	private String nConv1;
	@Column(name = "N_CONV_2")
	private String nConv2;
	@Column(name = "N_CONV_3")
	private String nConv3;
	@Column(name = "LIBRO_IN_1")
	private String libroIn1;
	@Column(name = "LIBRO_IN_2")
	private String libroIn2;
	@Column(name = "LIBRO_IN_3")
	private String libroIn3;
	@Column(name = "LIBRO_IN_D")
	private String libroInD;
	@Column(name = "LIBRO_IN_M")
	private String libroInM;
	@Column(name = "LIBRO_IN_A")
	private String libroInA;
	@Column(name = "P116")
	private String p116;
	@Column(name = "SERV_NE_1")
	private String servNe1;
	@Column(name = "SERV_NE_2")
	private String servNe2;
	@Column(name = "SERV_NE_3")
	private String servNe3;
	@Column(name = "SERV_NE_4")
	private String servNe4;
	@Column(name = "SERV_NE_5")
	private String servNe5;
	@Column(name = "SERV_NE_6")
	private String servNe6;
	@Column(name = "SERV_NE_6E")
	private String servNe6e;
	@Column(name = "SERV_NE_7")
	private String servNe7;
	@Column(name = "LC_ANEMIA")
	private String lcAnemia;
	@Column(name = "CENT_APP")
	private String centApp;
	@Column(name = "DOC_PRACT")
	private String docPract;
	@Column(name = "DOC_PRA_Q")
	private Short docPraQ;
	@Column(name = "SISEVE")
	private String siseve;
	@Column(name = "AF_SISEVE")
	private String afSiseve;
	@Column(name = "RPT_SISEVE")
	private String rptSiseve;

	@Column(name = "G_ETNICO_1")
	private String gEtnico1;
	@Column(name = "G_ETNICO_2")
	private String gEtnico2;
	@Column(name = "G_ETNICO_3")
	private String gEtnico3;
	@Column(name = "G_ETNICO_4")
	private String gEtnico4;
	@Column(name = "G_ETNICO_5")
	private String gEtnico5;
	@Column(name = "G_ETNICO_6")
	private String gEtnico6;
	@Column(name = "G_ETNICO_7")
	private String gEtnico7;
	@Column(name = "G_ETNICO_8")
	private String gEtnico8;
	@Column(name = "G_ETNICO_9")
	private String gEtnico9;
	@Column(name = "G_ETNICO_E")
	private String gEtnicoE;

	@Column(name = "GRUPO_ETN")
	private String grupoEtn;
	@Column(name = "GRP_ETN_Q")
	private Short grpEtnQ;
	@Column(name = "ES_EIB")
	private String esEib;
	@Column(name = "EIB_EST")
	private String eibEst;
	@Column(name = "EIB_RES")
	private String eibRes;
	@Column(name = "CAS_2_LEN")
	private String cas2Len;
	@Column(name = "LEN_ORIG")
	private String lenOrig;
	@Column(name = "HLEN_ORIG")
	private String hlenOrig;
	@Column(name = "COD_LORIG")
	private String codLorig;
	@Column(name = "EHLEN_ORI")
	private String ehlenOri;
	@Column(name = "SI_LENORIG")
	private String siLenorig;
	@Column(name = "SI_CODLEN")
	private String siCodlen;
	@Column(name = "P130")
	private String p130;
	@Column(name = "P131_3AS_1")
	private String p1313as1;
	@Column(name = "P131_3AS_2")
	private String p1313as2;
	@Column(name = "P131_3AS_3")
	private String p1313as3;
	@Column(name = "P131_3AS_4")
	private String p1313as4;
	@Column(name = "P131_3AS_5")
	private String p1313as5;
	@Column(name = "P131_3AS_6")
	private String p1313as6;
	@Column(name = "P131_3AS_7")
	private String p1313as7;
	@Column(name = "P131_3AS_8")
	private String p1313as8;
	@Column(name = "P131_3AS_E")
	private String p1313asE;
	@Column(name = "P131")
	private String p131;
	@Column(name = "P331")
	private String p331;
	@Column(name = "P331_QN")
	private String p331Qn;
	@Column(name = "P401")
	private String p401;
	@Column(name = "P402_C12_4")
	private Short p402C124;
	@Column(name = "P402_C12_5")
	private Short p402C125;
	@Column(name = "P402_C1_4O")
	private Short p402C14o;
	@Column(name = "P402_C1_5O")
	private Short p402C15o;
	@Column(name = "P403_C3AP")
	private String p403C3ap;
	@Column(name = "CUAD_TRAB")
	private String cuadTrab;
	@Column(name = "SUF_CUAD")
	private String sufCuad;
	@Column(name = "CANT_CUAD")
	private Short cantCuad;
	@Column(name = "CUAD_AULA")
	private String cuadAula;
	@Column(name = "P408_C12")
	private String p408C12;
	@Column(name = "P408_I")
	private String p408I;
	@Column(name = "P408_II")
	private String p408Ii;
	@Column(name = "P502_C12")
	private String p502C12;
	@Column(name = "P502_C12_D")
	private String p502C12D;
	@Column(name = "P502_C12_M")
	private String p502C12M;
	@Column(name = "P502_C12_A")
	private String p502C12A;
	@Column(name = "P503_C2")
	private String p503C2;
	@Column(name = "RUTA_SBI")
	private String rutaSbi;
	@Column(name = "RUTA_SBI_1")
	private Short rutaSbi1;
	@Column(name = "RUTA_SBI_2")
	private String rutaSbi2;
	@Column(name = "RUTA_SLA")
	private String rutaSla;
	@Column(name = "RUTA_SLA_1")
	private Short rutaSla1;
	@Column(name = "RUTA_SLA_2")
	private String rutaSla2;
	@Column(name = "P601_PS")
	private String p601Ps;
	@Column(name = "P601_QH")
	private Integer p601Qh;
	@Column(name = "P601_QM")
	private Integer p601Qm;
	@Column(name = "P602_PS")
	private String p602Ps;
	@Column(name = "P603_PS")
	private String p603Ps;
	@Column(name = "P604_PS")
	private String p604Ps;
	@Column(name = "P605_E_PS")
	private String p605EPs;
	@Column(name = "P605_QE_PS")
	private Short p605QePs;
	@Column(name = "P605_D_PS")
	private String p605DPs;
	@Column(name = "P605_QD_PS")
	private Short p605QdPs;
	@Column(name = "P606_PS")
	private String p606Ps;
	@Column(name = "P606_HS_PS")
	private Short p606HsPs;
	@Column(name = "P606_DA_PS")
	private Short p606DaPs;
	@Column(name = "P607_PS")
	private String p607Ps;
	@Column(name = "P608_PS")
	private String p608Ps;
	@Column(name = "P609_PS_1")
	private String p609Ps1;
	@Column(name = "P609_PS_2")
	private String p609Ps2;
	@Column(name = "P609_PS_3")
	private String p609Ps3;
	@Column(name = "P609_PS_4")
	private String p609Ps4;
	@Column(name = "P609_PS_5")
	private String p609Ps5;
	@Column(name = "P609_PS_6")
	private String p609Ps6;
	@Column(name = "P609_PS_7")
	private String p609Ps7;
	@Column(name = "P609_PS_7E")
	private String p609Ps7e;
	@Column(name = "P102_C4")
	private String p102C4;
	@Column(name = "P103_C4")
	private Integer p103C4;
	@Column(name = "P104_C4_LU")
	private Integer p104C4Lu;
	@Column(name = "P104_C4_MA")
	private Integer p104C4Ma;
	@Column(name = "P104_C4_MI")
	private Integer p104C4Mi;
	@Column(name = "P104_C4_JU")
	private Integer p104C4Ju;
	@Column(name = "P104_C4_VI")
	private Integer p104C4Vi;
	@Column(name = "P104_C4_SA")
	private Integer p104C4Sa;
	@Column(name = "P104_C4_DO")
	private Integer p104C4Do;
	@Column(name = "P105_C4A_1")
	private String p105C4a1;
	@Column(name = "P105_C4A_2")
	private String p105C4a2;
	@Column(name = "P105_C4A_3")
	private String p105C4a3;
	@Column(name = "P105_C4A_4")
	private String p105C4a4;
	@Column(name = "P105_C4A_5")
	private String p105C4a5;
	@Column(name = "P105_C4A_5E")
	private String p105C4a5e;
	@Column(name = "P105_C4I")
	private String p105C4i;
	@Column(name = "P106_C4I")
	private Integer p106C4i;
	@Column(name = "P107_C4_LU")
	private Integer p107C4Lu;
	@Column(name = "P107_C4_MA")
	private Integer p107C4Ma;
	@Column(name = "P107_C4_MI")
	private Integer p107C4Mi;
	@Column(name = "P107_C4_JU")
	private Integer p107C4Ju;
	@Column(name = "P107_C4_VI")
	private Integer p107C4Vi;
	@Column(name = "P107_C4_SA")
	private Integer p107C4Sa;
	@Column(name = "P107_C4_DO")
	private Integer p107C4Do;
	@Column(name = "PCONV_C4_1")
	private String pconvC41;
	@Column(name = "PCONV_C4_2")
	private String pconvC42;
	@Column(name = "PCONV_C4_3")
	private String pconvC43;
	@Column(name = "PCONV_C4_4")
	private String pconvC44;
	@Column(name = "PCONV_C4_5")
	private String pconvC45;
	@Column(name = "PCONV_C4_6")
	private String pconvC46;
	@Column(name = "PCONV_C4_7")
	private String pconvC47;
	@Column(name = "PCONV_C4_8")
	private String pconvC48;
	@Column(name = "PCONV_C4_E")
	private String pconvC4E;
	@Column(name = "PCOPAE_C4")
	private String pcopaeC4;
	@Column(name = "PCOPAE_RD")
	private String pcopaeRd;
	@Column(name = "PCONEI_C4")
	private String pconeiC4;
	@Column(name = "PCONEI_RD")
	private String pconeiRd;
	@Column(name = "P111_109_1")
	private String p1111091;
	@Column(name = "P111_109_2")
	private String p1111092;
	@Column(name = "P111_109_3")
	private String p1111093;
	@Column(name = "P111_109_4")
	private String p1111094;
	@Column(name = "P111_109E")
	private String p111109e;
	@Column(name = "P112_110_1")
	private String p1121101;
	@Column(name = "P112_110_2")
	private String p1121102;
	@Column(name = "P112_110_3")
	private String p1121103;
	@Column(name = "P112_110_4")
	private String p1121104;
	@Column(name = "P112_110_E")
	private String p112110E;
	@Column(name = "P113_111_R")
	private String p113111R;
	@Column(name = "P113_111_E")
	private String p113111E;
	@Column(name = "P114_112_R")
	private String p114112R;
	@Column(name = "P114_112_E")
	private String p114112E;
	@Column(name = "P402_C4")
	private String p402C4;
	@Column(name = "P403_C4_1")
	private String p403C41;
	@Column(name = "P403_C4_2")
	private String p403C42;
	@Column(name = "P403_C4_3")
	private String p403C43;
	@Column(name = "P403_C4_4")
	private String p403C44;
	@Column(name = "P403_C4_5")
	private String p403C45;
	@Column(name = "P403_C4_6")
	private String p403C46;
	@Column(name = "P403_C4_7")
	private String p403C47;
	@Column(name = "P403_C4_8")
	private String p403C48;
	@Column(name = "P403_C4_9")
	private String p403C49;
	@Column(name = "P403_C4_9E")
	private String p403C49e;
	@Column(name = "P403_C4_10")
	private String p403C410;
	@Column(name = "P404_C4_11")
	private String p404C411;
	@Column(name = "P404_C4_12")
	private Integer p404C412;
	@Column(name = "P404_C4_21")
	private String p404C421;
	@Column(name = "P404_C4_22")
	private Integer p404C422;
	@Column(name = "P404_C4_31")
	private String p404C431;
	@Column(name = "P404_C4_32")
	private Integer p404C432;
	@Column(name = "P404_C4_41")
	private String p404C441;
	@Column(name = "P405_C4_1")
	private Integer p405C41;
	@Column(name = "P405_C4_2")
	private Integer p405C42;
	@Column(name = "P405_C4_3")
	private Integer p405C43;
	@Column(name = "TDOCENTES")
	private Integer tdocentes;
	@Column(name = "TDOCAULA")
	private Integer tdocaula;
	@Column(name = "TAUXILIAR")
	private Integer tauxiliar;
	@Column(name = "TADMINIST")
	private Integer tadminist;
	@Column(name = "ANOTACIONES")
	private String anotaciones;
	@Column(name = "FUENTE")
	private String fuente;
	@Column(name = "FECHA_ENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;
	@Column(name = "VERSION")
	private String version;
	@Column(name = "TIPO_ENVIO")
	private String tipoEnvio;
	@Column(name = "ULTIMO")
	private Boolean ultimo;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2018Saanee> matricula2018SaaneeList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2018Carreras> matricula2018CarrerasList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) //EAGER
	private List<Matricula2018Personal> matricula2018PersonalList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) //EAGER
	private List<Matricula2018Localpronoei> matricula2018LocalpronoeiList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) //EAGER
	private List<Matricula2018Eba> matricula2018EbaList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) //EAGER
	private List<Matricula2018Dlen> matricula2018DlenList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) //EAGER
	private List<Matricula2018Resp> matricula2018RespList;

	// cambio
	// @MapKeyColumn(name = "cuadro", length = 5)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) //EAGER
	private List<Matricula2018Matricula> detalleMatricula;

	// @MapKeyColumn(name = "cuadro", length = 5)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) //EAGER
	private List<Matricula2018Seccion> detalleSeccion;

	// @MapKeyColumn(name = "cuadro", length = 5)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2018Recursos> detalleRecursos;

	@Transient
	private long token;

	@Transient
	private String msg;
	@Transient
	private String estadoRpt;

	public Matricula2018Cabecera() {

	}

	public Matricula2018Cabecera(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Matricula2018Cabecera(Long idEnvio, String nroced, String codMod, String anexo, String cenEdu,
			String codlocal, String nivMod) {
		this.idEnvio = idEnvio;
		this.nroced = nroced;
		this.codMod = codMod;
		this.anexo = anexo;
		this.cenEdu = cenEdu;
		this.codlocal = codlocal;
		this.nivMod = nivMod;
	}

	@XmlAttribute
	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	@XmlAttribute(name = "MSG")
	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@XmlAttribute(name = "ESTADO_RPT")
	public String getEstadoRpt() {
		return this.estadoRpt;
	}

	public void setEstadoRpt(String estadoRpt) {
		this.estadoRpt = estadoRpt;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "NROCED")
	public String getNroced() {
		return nroced;
	}

	public void setNroced(String nroced) {
		this.nroced = nroced;
	}

	@XmlElement(name = "COD_MOD")
	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	@XmlElement(name = "ANEXO")
	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	@XmlElement(name = "DNI_CORD")
	public String getDniCord() {
		return dniCord;
	}

	public void setDniCord(String dniCord) {
		this.dniCord = dniCord;
	}

	@XmlElement(name = "CEN_EDU")
	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	@XmlElement(name = "CODLOCAL")
	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	@XmlElement(name = "DISTRITO")
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@XmlElement(name = "METATEN_1")
	public String getMetaten1() {
		return metaten1;
	}

	public void setMetaten1(String metaten1) {
		this.metaten1 = metaten1;
	}

	@XmlElement(name = "METATEN_2")
	public String getMetaten2() {
		return metaten2;
	}

	public void setMetaten2(String metaten2) {
		this.metaten2 = metaten2;
	}

	@XmlElement(name = "METATEN_3")
	public String getMetaten3() {
		return metaten3;
	}

	public void setMetaten3(String metaten3) {
		this.metaten3 = metaten3;
	}

	@XmlElement(name = "METATEN_4")
	public String getMetaten4() {
		return metaten4;
	}

	public void setMetaten4(String metaten4) {
		this.metaten4 = metaten4;
	}

	@XmlElement(name = "METATEN_5")
	public String getMetaten5() {
		return metaten5;
	}

	public void setMetaten5(String metaten5) {
		this.metaten5 = metaten5;
	}

	@XmlElement(name = "MODATEN")
	public String getModaten() {
		return modaten;
	}

	public void setModaten(String modaten) {
		this.modaten = modaten;
	}

	@XmlElement(name = "STUTORIAL")
	public String getStutorial() {
		return stutorial;
	}

	public void setStutorial(String stutorial) {
		this.stutorial = stutorial;
	}

	@XmlElement(name = "LOCALIDAD")
	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	@XmlElement(name = "EGESTORA")
	public String getEgestora() {
		return egestora;
	}

	public void setEgestora(String egestora) {
		this.egestora = egestora;
	}

	@XmlElement(name = "TIPOFINA_1")
	public String getTipofina1() {
		return tipofina1;
	}

	public void setTipofina1(String tipofina1) {
		this.tipofina1 = tipofina1;
	}

	@XmlElement(name = "TIPOFINA_2")
	public String getTipofina2() {
		return tipofina2;
	}

	public void setTipofina2(String tipofina2) {
		this.tipofina2 = tipofina2;
	}

	@XmlElement(name = "TIPOFINA_3")
	public String getTipofina3() {
		return tipofina3;
	}

	public void setTipofina3(String tipofina3) {
		this.tipofina3 = tipofina3;
	}

	@XmlElement(name = "TIPOFINA_4")
	public String getTipofina4() {
		return tipofina4;
	}

	public void setTipofina4(String tipofina4) {
		this.tipofina4 = tipofina4;
	}

	@XmlElement(name = "TIPOPROG")
	public String getTipoprog() {
		return tipoprog;
	}

	public void setTipoprog(String tipoprog) {
		this.tipoprog = tipoprog;
	}

	@XmlElement(name = "NRORD_CRE")
	public String getNrordCre() {
		return nrordCre;
	}

	public void setNrordCre(String nrordCre) {
		this.nrordCre = nrordCre;
	}

	@XmlElement(name = "FECRES_C_D")
	public String getFecresCD() {
		return fecresCD;
	}

	public void setFecresCD(String fecresCD) {
		this.fecresCD = fecresCD;
	}

	@XmlElement(name = "FECRES_C_M")
	public String getFecresCM() {
		return fecresCM;
	}

	public void setFecresCM(String fecresCM) {
		this.fecresCM = fecresCM;
	}

	@XmlElement(name = "FECRES_C_A")
	public String getFecresCA() {
		return fecresCA;
	}

	public void setFecresCA(String fecresCA) {
		this.fecresCA = fecresCA;
	}

	@XmlElement(name = "NRORD_REN")
	public String getNrordRen() {
		return nrordRen;
	}

	public void setNrordRen(String nrordRen) {
		this.nrordRen = nrordRen;
	}

	@XmlElement(name = "FECRES_R_D")
	public String getFecresRD() {
		return fecresRD;
	}

	public void setFecresRD(String fecresRD) {
		this.fecresRD = fecresRD;
	}

	@XmlElement(name = "FECRES_R_M")
	public String getFecresRM() {
		return fecresRM;
	}

	public void setFecresRM(String fecresRM) {
		this.fecresRM = fecresRM;
	}

	@XmlElement(name = "FECRES_R_A")
	public String getFecresRA() {
		return fecresRA;
	}

	public void setFecresRA(String fecresRA) {
		this.fecresRA = fecresRA;
	}

	@XmlElement(name = "NIV_MOD")
	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	@XmlElement(name = "FORMATEN")
	public String getFormaten() {
		return formaten;
	}

	public void setFormaten(String formaten) {
		this.formaten = formaten;
	}

	@XmlElement(name = "ALUM_02IC")
	public String getAlum02ic() {
		return alum02ic;
	}

	public void setAlum02ic(String alum02ic) {
		this.alum02ic = alum02ic;
	}

	@XmlElement(name = "GESTION")
	public String getGestion() {
		return gestion;
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	@XmlElement(name = "CODUGEL")
	public String getCodugel() {
		return codugel;
	}

	public void setCodugel(String codugel) {
		this.codugel = codugel;
	}

	@XmlElement(name = "TIPOIESUP")
	public String getTipoiesup() {
		return tipoiesup;
	}

	public void setTipoiesup(String tipoiesup) {
		this.tipoiesup = tipoiesup;
	}

	@XmlElement(name = "PESTAB_C6A")
	public Short getPestabC6a() {
		return pestabC6a;
	}

	public void setPestabC6a(Short pestabC6a) {
		this.pestabC6a = pestabC6a;
	}

	@XmlElement(name = "P101_C5A")
	public String getP101C5a() {
		return p101C5a;
	}

	public void setP101C5a(String p101C5a) {
		this.p101C5a = p101C5a;
	}

	@XmlElement(name = "P101_C5A_I")
	public String getP101C5aI() {
		return p101C5aI;
	}

	public void setP101C5aI(String p101C5aI) {
		this.p101C5aI = p101C5aI;
	}

	@XmlElement(name = "P101_C5A_P")
	public String getP101C5aP() {
		return p101C5aP;
	}

	public void setP101C5aP(String p101C5aP) {
		this.p101C5aP = p101C5aP;
	}

	@XmlElement(name = "P101_C5A_S")
	public String getP101C5aS() {
		return p101C5aS;
	}

	public void setP101C5aS(String p101C5aS) {
		this.p101C5aS = p101C5aS;
	}

	@XmlElement(name = "P102_C5A")
	public String getP102C5a() {
		return p102C5a;
	}

	public void setP102C5a(String p102C5a) {
		this.p102C5a = p102C5a;
	}

	@XmlElement(name = "P102_C5A_1")
	public String getP102C5a1() {
		return p102C5a1;
	}

	public void setP102C5a1(String p102C5a1) {
		this.p102C5a1 = p102C5a1;
	}

	@XmlElement(name = "P1021_C6A")
	public String getP1021C6a() {
		return p1021C6a;
	}

	public void setP1021C6a(String p1021C6a) {
		this.p1021C6a = p1021C6a;
	}

	@XmlElement(name = "P1021_C6A1")
	public String getP1021C6a1() {
		return p1021C6a1;
	}

	public void setP1021C6a1(String p1021C6a1) {
		this.p1021C6a1 = p1021C6a1;
	}

	@XmlElement(name = "P1021_C6A2")
	public String getP1021C6a2() {
		return p1021C6a2;
	}

	public void setP1021C6a2(String p1021C6a2) {
		this.p1021C6a2 = p1021C6a2;
	}

	@XmlElement(name = "P1021_C6A3")
	public String getP1021C6a3() {
		return p1021C6a3;
	}

	public void setP1021C6a3(String p1021C6a3) {
		this.p1021C6a3 = p1021C6a3;
	}

	@XmlElement(name = "P1022_C6A1")
	public String getP1022C6a1() {
		return p1022C6a1;
	}

	public void setP1022C6a1(String p1022C6a1) {
		this.p1022C6a1 = p1022C6a1;
	}

	@XmlElement(name = "P1022_C6A2")
	public String getP1022C6a2() {
		return p1022C6a2;
	}

	public void setP1022C6a2(String p1022C6a2) {
		this.p1022C6a2 = p1022C6a2;
	}

	@XmlElement(name = "P1022_C6A3")
	public String getP1022C6a3() {
		return p1022C6a3;
	}

	public void setP1022C6a3(String p1022C6a3) {
		this.p1022C6a3 = p1022C6a3;
	}

	@XmlElement(name = "P102_C7A")
	public String getP102C7a() {
		return p102C7a;
	}

	public void setP102C7a(String p102C7a) {
		this.p102C7a = p102C7a;
	}

	@XmlElement(name = "P102_C7A_1")
	public String getP102C7a1() {
		return p102C7a1;
	}

	public void setP102C7a1(String p102C7a1) {
		this.p102C7a1 = p102C7a1;
	}

	@XmlElement(name = "P102_C7A_2")
	public String getP102C7a2() {
		return p102C7a2;
	}

	public void setP102C7a2(String p102C7a2) {
		this.p102C7a2 = p102C7a2;
	}

	@XmlElement(name = "P102_C7A_3")
	public String getP102C7a3() {
		return p102C7a3;
	}

	public void setP102C7a3(String p102C7a3) {
		this.p102C7a3 = p102C7a3;
	}

	@XmlElement(name = "P102_C7A_4")
	public String getP102C7a4() {
		return p102C7a4;
	}

	public void setP102C7a4(String p102C7a4) {
		this.p102C7a4 = p102C7a4;
	}

	@XmlElement(name = "P102_C7A_5")
	public String getP102C7a5() {
		return p102C7a5;
	}

	public void setP102C7a5(String p102C7a5) {
		this.p102C7a5 = p102C7a5;
	}

	@XmlElement(name = "P102_C7A_6")
	public String getP102C7a6() {
		return p102C7a6;
	}

	public void setP102C7a6(String p102C7a6) {
		this.p102C7a6 = p102C7a6;
	}

	@XmlElement(name = "P102_C7A_E")
	public String getP102C7aE() {
		return p102C7aE;
	}

	public void setP102C7aE(String p102C7aE) {
		this.p102C7aE = p102C7aE;
	}

	@XmlElement(name = "P401_C8A")
	public String getP401C8a() {
		return p401C8a;
	}

	public void setP401C8a(String p401C8a) {
		this.p401C8a = p401C8a;
	}

	@XmlElement(name = "P101_C1")
	public String getP101C1() {
		return p101C1;
	}

	public void setP101C1(String p101C1) {
		this.p101C1 = p101C1;
	}

	@XmlElement(name = "P101_C1_A")
	public String getP101C1A() {
		return p101C1A;
	}

	public void setP101C1A(String p101C1A) {
		this.p101C1A = p101C1A;
	}

	@XmlElement(name = "INI_CLAS_D")
	public String getIniClasD() {
		return iniClasD;
	}

	public void setIniClasD(String iniClasD) {
		this.iniClasD = iniClasD;
	}

	@XmlElement(name = "INI_CLAS_M")
	public String getIniClasM() {
		return iniClasM;
	}

	public void setIniClasM(String iniClasM) {
		this.iniClasM = iniClasM;
	}

	@XmlElement(name = "P101_C2_1")
	public String getP101C21() {
		return p101C21;
	}

	public void setP101C21(String p101C21) {
		this.p101C21 = p101C21;
	}

	@XmlElement(name = "P101_C2_2")
	public String getP101C22() {
		return p101C22;
	}

	public void setP101C22(String p101C22) {
		this.p101C22 = p101C22;
	}

	@XmlElement(name = "P101_C2_3")
	public String getP101C23() {
		return p101C23;
	}

	public void setP101C23(String p101C23) {
		this.p101C23 = p101C23;
	}

	@XmlElement(name = "P101_C2_4")
	public String getP101C24() {
		return p101C24;
	}

	public void setP101C24(String p101C24) {
		this.p101C24 = p101C24;
	}

	@XmlElement(name = "FIN_CLAS_D")
	public String getFinClasD() {
		return finClasD;
	}

	public void setFinClasD(String finClasD) {
		this.finClasD = finClasD;
	}

	@XmlElement(name = "FIN_CLAS_M")
	public String getFinClasM() {
		return finClasM;
	}

	public void setFinClasM(String finClasM) {
		this.finClasM = finClasM;
	}

	@XmlElement(name = "P103_C2_1")
	public String getP103C21() {
		return p103C21;
	}

	public void setP103C21(String p103C21) {
		this.p103C21 = p103C21;
	}

	@XmlElement(name = "P103_C2_2")
	public String getP103C22() {
		return p103C22;
	}

	public void setP103C22(String p103C22) {
		this.p103C22 = p103C22;
	}

	@XmlElement(name = "P103_C2_3")
	public String getP103C23() {
		return p103C23;
	}

	public void setP103C23(String p103C23) {
		this.p103C23 = p103C23;
	}

	@XmlElement(name = "P103_C2_4")
	public String getP103C24() {
		return p103C24;
	}

	public void setP103C24(String p103C24) {
		this.p103C24 = p103C24;
	}

	@XmlElement(name = "P103_C2_5")
	public String getP103C25() {
		return p103C25;
	}

	public void setP103C25(String p103C25) {
		this.p103C25 = p103C25;
	}

	@XmlElement(name = "P103_C2_6")
	public String getP103C26() {
		return p103C26;
	}

	public void setP103C26(String p103C26) {
		this.p103C26 = p103C26;
	}

	@XmlElement(name = "PL_TUTORIA")
	public String getPlTutoria() {
		return plTutoria;
	}

	public void setPlTutoria(String plTutoria) {
		this.plTutoria = plTutoria;
	}

	@XmlElement(name = "P104_C3P3S")
	public String getP104C3p3s() {
		return p104C3p3s;
	}

	public void setP104C3p3s(String p104C3p3s) {
		this.p104C3p3s = p104C3p3s;
	}

	@XmlElement(name = "P105_C3P3S")
	public String getP105C3p3s() {
		return p105C3p3s;
	}

	public void setP105C3p3s(String p105C3p3s) {
		this.p105C3p3s = p105C3p3s;
	}

	@XmlElement(name = "P106_C3P3S")
	public String getP106C3p3s() {
		return p106C3p3s;
	}

	public void setP106C3p3s(String p106C3p3s) {
		this.p106C3p3s = p106C3p3s;
	}

	@XmlElement(name = "P107_C3_01")
	public String getP107C301() {
		return p107C301;
	}

	public void setP107C301(String p107C301) {
		this.p107C301 = p107C301;
	}

	@XmlElement(name = "P107_C3_02")
	public String getP107C302() {
		return p107C302;
	}

	public void setP107C302(String p107C302) {
		this.p107C302 = p107C302;
	}

	@XmlElement(name = "P107_C3_03")
	public String getP107C303() {
		return p107C303;
	}

	public void setP107C303(String p107C303) {
		this.p107C303 = p107C303;
	}

	@XmlElement(name = "P107_C3_04")
	public String getP107C304() {
		return p107C304;
	}

	public void setP107C304(String p107C304) {
		this.p107C304 = p107C304;
	}

	@XmlElement(name = "P107_C3_05")
	public String getP107C305() {
		return p107C305;
	}

	public void setP107C305(String p107C305) {
		this.p107C305 = p107C305;
	}

	@XmlElement(name = "P107_C3_06")
	public String getP107C306() {
		return p107C306;
	}

	public void setP107C306(String p107C306) {
		this.p107C306 = p107C306;
	}

	@XmlElement(name = "P107_C3_07")
	public String getP107C307() {
		return p107C307;
	}

	public void setP107C307(String p107C307) {
		this.p107C307 = p107C307;
	}

	@XmlElement(name = "P107_C3_08")
	public String getP107C308() {
		return p107C308;
	}

	public void setP107C308(String p107C308) {
		this.p107C308 = p107C308;
	}

	@XmlElement(name = "P107_C3_09")
	public String getP107C309() {
		return p107C309;
	}

	public void setP107C309(String p107C309) {
		this.p107C309 = p107C309;
	}

	@XmlElement(name = "P107_C3_10")
	public String getP107C310() {
		return p107C310;
	}

	public void setP107C310(String p107C310) {
		this.p107C310 = p107C310;
	}

	@XmlElement(name = "P107_C3_11")
	public String getP107C311() {
		return p107C311;
	}

	public void setP107C311(String p107C311) {
		this.p107C311 = p107C311;
	}

	@XmlElement(name = "P107_C3_12")
	public String getP107C312() {
		return p107C312;
	}

	public void setP107C312(String p107C312) {
		this.p107C312 = p107C312;
	}

	@XmlElement(name = "P107_C3_13")
	public String getP107C313() {
		return p107C313;
	}

	public void setP107C313(String p107C313) {
		this.p107C313 = p107C313;
	}

	@XmlElement(name = "P107_C3_14")
	public String getP107C314() {
		return p107C314;
	}

	public void setP107C314(String p107C314) {
		this.p107C314 = p107C314;
	}

	@XmlElement(name = "P107_C314E")
	public String getP107C314e() {
		return p107C314e;
	}

	public void setP107C314e(String p107C314e) {
		this.p107C314e = p107C314e;
	}

	@XmlElement(name = "PAT_2018")
	public String getPat2018() {
		return pat2018;
	}

	public void setPat2018(String pat2018) {
		this.pat2018 = pat2018;
	}

	@XmlElement(name = "P106_C1")
	public String getP106C1() {
		return p106C1;
	}

	public void setP106C1(String p106C1) {
		this.p106C1 = p106C1;
	}

	@XmlElement(name = "P106_C1_QN")
	public Short getP106C1Qn() {
		return p106C1Qn;
	}

	public void setP106C1Qn(Short p106C1Qn) {
		this.p106C1Qn = p106C1Qn;
	}

	@XmlElement(name = "N_CONV_1")
	public String getNConv1() {
		return nConv1;
	}

	public void setNConv1(String nConv1) {
		this.nConv1 = nConv1;
	}

	@XmlElement(name = "N_CONV_2")
	public String getNConv2() {
		return nConv2;
	}

	public void setNConv2(String nConv2) {
		this.nConv2 = nConv2;
	}

	@XmlElement(name = "N_CONV_3")
	public String getNConv3() {
		return nConv3;
	}

	public void setNConv3(String nConv3) {
		this.nConv3 = nConv3;
	}

	@XmlElement(name = "LIBRO_IN_1")
	public String getLibroIn1() {
		return libroIn1;
	}

	public void setLibroIn1(String libroIn1) {
		this.libroIn1 = libroIn1;
	}

	@XmlElement(name = "LIBRO_IN_2")
	public String getLibroIn2() {
		return libroIn2;
	}

	public void setLibroIn2(String libroIn2) {
		this.libroIn2 = libroIn2;
	}

	@XmlElement(name = "LIBRO_IN_3")
	public String getLibroIn3() {
		return libroIn3;
	}

	public void setLibroIn3(String libroIn3) {
		this.libroIn3 = libroIn3;
	}

	@XmlElement(name = "LIBRO_IN_D")
	public String getLibroInD() {
		return libroInD;
	}

	public void setLibroInD(String libroInD) {
		this.libroInD = libroInD;
	}

	@XmlElement(name = "LIBRO_IN_M")
	public String getLibroInM() {
		return libroInM;
	}

	public void setLibroInM(String libroInM) {
		this.libroInM = libroInM;
	}

	@XmlElement(name = "LIBRO_IN_A")
	public String getLibroInA() {
		return libroInA;
	}

	public void setLibroInA(String libroInA) {
		this.libroInA = libroInA;
	}

	@XmlElement(name = "P116")
	public String getP116() {
		return p116;
	}

	public void setP116(String p116) {
		this.p116 = p116;
	}

	@XmlElement(name = "SERV_NE_1")
	public String getServNe1() {
		return servNe1;
	}

	public void setServNe1(String servNe1) {
		this.servNe1 = servNe1;
	}

	@XmlElement(name = "SERV_NE_2")
	public String getServNe2() {
		return servNe2;
	}

	public void setServNe2(String servNe2) {
		this.servNe2 = servNe2;
	}

	@XmlElement(name = "SERV_NE_3")
	public String getServNe3() {
		return servNe3;
	}

	public void setServNe3(String servNe3) {
		this.servNe3 = servNe3;
	}

	@XmlElement(name = "SERV_NE_4")
	public String getServNe4() {
		return servNe4;
	}

	public void setServNe4(String servNe4) {
		this.servNe4 = servNe4;
	}

	@XmlElement(name = "SERV_NE_5")
	public String getServNe5() {
		return servNe5;
	}

	public void setServNe5(String servNe5) {
		this.servNe5 = servNe5;
	}

	@XmlElement(name = "SERV_NE_6")
	public String getServNe6() {
		return servNe6;
	}

	public void setServNe6(String servNe6) {
		this.servNe6 = servNe6;
	}

	@XmlElement(name = "SERV_NE_6E")
	public String getServNe6e() {
		return servNe6e;
	}

	public void setServNe6e(String servNe6e) {
		this.servNe6e = servNe6e;
	}

	@XmlElement(name = "SERV_NE_7")
	public String getServNe7() {
		return servNe7;
	}

	public void setServNe7(String servNe7) {
		this.servNe7 = servNe7;
	}

	@XmlElement(name = "LC_ANEMIA")
	public String getLcAnemia() {
		return lcAnemia;
	}

	public void setLcAnemia(String lcAnemia) {
		this.lcAnemia = lcAnemia;
	}

	@XmlElement(name = "CENT_APP")
	public String getCentApp() {
		return centApp;
	}

	public void setCentApp(String centApp) {
		this.centApp = centApp;
	}

	@XmlElement(name = "DOC_PRACT")
	public String getDocPract() {
		return docPract;
	}

	public void setDocPract(String docPract) {
		this.docPract = docPract;
	}

	@XmlElement(name = "DOC_PRA_Q")
	public Short getDocPraQ() {
		return docPraQ;
	}

	public void setDocPraQ(Short docPraQ) {
		this.docPraQ = docPraQ;
	}

	@XmlElement(name = "SISEVE")
	public String getSiseve() {
		return siseve;
	}

	public void setSiseve(String siseve) {
		this.siseve = siseve;
	}

	@XmlElement(name = "AF_SISEVE")
	public String getAfSiseve() {
		return afSiseve;
	}

	public void setAfSiseve(String afSiseve) {
		this.afSiseve = afSiseve;
	}

	@XmlElement(name = "RPT_SISEVE")
	public String getRptSiseve() {
		return rptSiseve;
	}

	public void setRptSiseve(String rptSiseve) {
		this.rptSiseve = rptSiseve;
	}

	@XmlElement(name = "GRUPO_ETN")
	public String getGrupoEtn() {
		return grupoEtn;
	}

	public void setGrupoEtn(String grupoEtn) {
		this.grupoEtn = grupoEtn;
	}

	@XmlElement(name = "GRP_ETN_Q")
	public Short getGrpEtnQ() {
		return grpEtnQ;
	}

	public void setGrpEtnQ(Short grpEtnQ) {
		this.grpEtnQ = grpEtnQ;
	}

	@XmlElement(name = "ES_EIB")
	public String getEsEib() {
		return esEib;
	}

	public void setEsEib(String esEib) {
		this.esEib = esEib;
	}

	@XmlElement(name = "EIB_RES")
	public String getEibRes() {
		return eibRes;
	}

	public void setEibRes(String eibRes) {
		this.eibRes = eibRes;
	}

	@XmlElement(name = "CAS_2_LEN")
	public String getCas2Len() {
		return cas2Len;
	}

	public void setCas2Len(String cas2Len) {
		this.cas2Len = cas2Len;
	}

	@XmlElement(name = "LEN_ORIG")
	public String getLenOrig() {
		return lenOrig;
	}

	public void setLenOrig(String lenOrig) {
		this.lenOrig = lenOrig;
	}

	@XmlElement(name = "HLEN_ORIG")
	public String getHlenOrig() {
		return hlenOrig;
	}

	public void setHlenOrig(String hlenOrig) {
		this.hlenOrig = hlenOrig;
	}

	@XmlElement(name = "COD_LORIG")
	public String getCodLorig() {
		return codLorig;
	}

	public void setCodLorig(String codLorig) {
		this.codLorig = codLorig;
	}

	@XmlElement(name = "EHLEN_ORI")
	public String getEhlenOri() {
		return ehlenOri;
	}

	public void setEhlenOri(String ehlenOri) {
		this.ehlenOri = ehlenOri;
	}

	@XmlElement(name = "SI_LENORIG")
	public String getSiLenorig() {
		return siLenorig;
	}

	public void setSiLenorig(String siLenorig) {
		this.siLenorig = siLenorig;
	}

	@XmlElement(name = "SI_CODLEN")
	public String getSiCodlen() {
		return siCodlen;
	}

	public void setSiCodlen(String siCodlen) {
		this.siCodlen = siCodlen;
	}

	@XmlElement(name = "P130")
	public String getP130() {
		return p130;
	}

	public void setP130(String p130) {
		this.p130 = p130;
	}

	@XmlElement(name = "P131")
	public String getP131() {
		return p131;
	}

	public void setP131(String p131) {
		this.p131 = p131;
	}

	@XmlElement(name = "P331")
	public String getP331() {
		return p331;
	}

	public void setP331(String p331) {
		this.p331 = p331;
	}

	@XmlElement(name = "P331_QN")
	public String getP331Qn() {
		return p331Qn;
	}

	public void setP331Qn(String p331Qn) {
		this.p331Qn = p331Qn;
	}

	@XmlElement(name = "P401")
	public String getP401() {
		return p401;
	}

	public void setP401(String p401) {
		this.p401 = p401;
	}

	@XmlElement(name = "P402_C12_4")
	public Short getP402C124() {
		return p402C124;
	}

	public void setP402C124(Short p402C124) {
		this.p402C124 = p402C124;
	}

	@XmlElement(name = "P402_C12_5")
	public Short getP402C125() {
		return p402C125;
	}

	public void setP402C125(Short p402C125) {
		this.p402C125 = p402C125;
	}

	@XmlElement(name = "P402_C1_4O")
	public Short getP402C14o() {
		return p402C14o;
	}

	public void setP402C14o(Short p402C14o) {
		this.p402C14o = p402C14o;
	}

	@XmlElement(name = "P402_C1_5O")
	public Short getP402C15o() {
		return p402C15o;
	}

	public void setP402C15o(Short p402C15o) {
		this.p402C15o = p402C15o;
	}

	@XmlElement(name = "P403_C3AP")
	public String getP403C3ap() {
		return p403C3ap;
	}

	public void setP403C3ap(String p403C3ap) {
		this.p403C3ap = p403C3ap;
	}

	@XmlElement(name = "CUAD_TRAB")
	public String getCuadTrab() {
		return cuadTrab;
	}

	public void setCuadTrab(String cuadTrab) {
		this.cuadTrab = cuadTrab;
	}

	@XmlElement(name = "SUF_CUAD")
	public String getSufCuad() {
		return sufCuad;
	}

	public void setSufCuad(String sufCuad) {
		this.sufCuad = sufCuad;
	}

	@XmlElement(name = "CANT_CUAD")
	public Short getCantCuad() {
		return cantCuad;
	}

	public void setCantCuad(Short cantCuad) {
		this.cantCuad = cantCuad;
	}

	@XmlElement(name = "CUAD_AULA")
	public String getCuadAula() {
		return cuadAula;
	}

	public void setCuadAula(String cuadAula) {
		this.cuadAula = cuadAula;
	}

	@XmlElement(name = "P408_C12")
	public String getP408C12() {
		return p408C12;
	}

	public void setP408C12(String p408C12) {
		this.p408C12 = p408C12;
	}

	@XmlElement(name = "P502_C12")
	public String getP502C12() {
		return p502C12;
	}

	public void setP502C12(String p502C12) {
		this.p502C12 = p502C12;
	}

	@XmlElement(name = "P502_C12_D")
	public String getP502C12D() {
		return p502C12D;
	}

	public void setP502C12D(String p502C12D) {
		this.p502C12D = p502C12D;
	}

	@XmlElement(name = "P502_C12_M")
	public String getP502C12M() {
		return p502C12M;
	}

	public void setP502C12M(String p502C12M) {
		this.p502C12M = p502C12M;
	}

	@XmlElement(name = "P502_C12_A")
	public String getP502C12A() {
		return p502C12A;
	}

	public void setP502C12A(String p502C12A) {
		this.p502C12A = p502C12A;
	}

	@XmlElement(name = "P503_C2")
	public String getP503C2() {
		return p503C2;
	}

	public void setP503C2(String p503C2) {
		this.p503C2 = p503C2;
	}

	@XmlElement(name = "RUTA_SBI")
	public String getRutaSbi() {
		return rutaSbi;
	}

	public void setRutaSbi(String rutaSbi) {
		this.rutaSbi = rutaSbi;
	}

	@XmlElement(name = "RUTA_SBI_1")
	public Short getRutaSbi1() {
		return rutaSbi1;
	}

	public void setRutaSbi1(Short rutaSbi1) {
		this.rutaSbi1 = rutaSbi1;
	}

	@XmlElement(name = "RUTA_SBI_2")
	public String getRutaSbi2() {
		return rutaSbi2;
	}

	public void setRutaSbi2(String rutaSbi2) {
		this.rutaSbi2 = rutaSbi2;
	}

	@XmlElement(name = "RUTA_SLA")
	public String getRutaSla() {
		return rutaSla;
	}

	public void setRutaSla(String rutaSla) {
		this.rutaSla = rutaSla;
	}

	@XmlElement(name = "RUTA_SLA_1")
	public Short getRutaSla1() {
		return rutaSla1;
	}

	public void setRutaSla1(Short rutaSla1) {
		this.rutaSla1 = rutaSla1;
	}

	@XmlElement(name = "RUTA_SLA_2")
	public String getRutaSla2() {
		return rutaSla2;
	}

	public void setRutaSla2(String rutaSla2) {
		this.rutaSla2 = rutaSla2;
	}

	@XmlElement(name = "P601_PS")
	public String getP601Ps() {
		return p601Ps;
	}

	public void setP601Ps(String p601Ps) {
		this.p601Ps = p601Ps;
	}

	@XmlElement(name = "P601_QH")
	public Integer getP601Qh() {
		return p601Qh;
	}

	public void setP601Qh(Integer p601Qh) {
		this.p601Qh = p601Qh;
	}

	@XmlElement(name = "P601_QM")
	public Integer getP601Qm() {
		return p601Qm;
	}

	public void setP601Qm(Integer p601Qm) {
		this.p601Qm = p601Qm;
	}

	@XmlElement(name = "P602_PS")
	public String getP602Ps() {
		return p602Ps;
	}

	public void setP602Ps(String p602Ps) {
		this.p602Ps = p602Ps;
	}

	@XmlElement(name = "P603_PS")
	public String getP603Ps() {
		return p603Ps;
	}

	public void setP603Ps(String p603Ps) {
		this.p603Ps = p603Ps;
	}

	@XmlElement(name = "P604_PS")
	public String getP604Ps() {
		return p604Ps;
	}

	public void setP604Ps(String p604Ps) {
		this.p604Ps = p604Ps;
	}

	@XmlElement(name = "P605_E_PS")
	public String getP605EPs() {
		return p605EPs;
	}

	public void setP605EPs(String p605EPs) {
		this.p605EPs = p605EPs;
	}

	@XmlElement(name = "P605_QE_PS")
	public Short getP605QePs() {
		return p605QePs;
	}

	public void setP605QePs(Short p605QePs) {
		this.p605QePs = p605QePs;
	}

	@XmlElement(name = "P606_PS")
	public String getP606Ps() {
		return p606Ps;
	}

	public void setP606Ps(String p606Ps) {
		this.p606Ps = p606Ps;
	}

	@XmlElement(name = "P606_HS_PS")
	public Short getP606HsPs() {
		return p606HsPs;
	}

	public void setP606HsPs(Short p606HsPs) {
		this.p606HsPs = p606HsPs;
	}

	@XmlElement(name = "P606_DA_PS")
	public Short getP606DaPs() {
		return p606DaPs;
	}

	public void setP606DaPs(Short p606DaPs) {
		this.p606DaPs = p606DaPs;
	}

	@XmlElement(name = "P607_PS")
	public String getP607Ps() {
		return p607Ps;
	}

	public void setP607Ps(String p607Ps) {
		this.p607Ps = p607Ps;
	}

	@XmlElement(name = "P608_PS")
	public String getP608Ps() {
		return p608Ps;
	}

	public void setP608Ps(String p608Ps) {
		this.p608Ps = p608Ps;
	}

	@XmlElement(name = "P609_PS_1")
	public String getP609Ps1() {
		return p609Ps1;
	}

	public void setP609Ps1(String p609Ps1) {
		this.p609Ps1 = p609Ps1;
	}

	@XmlElement(name = "P609_PS_2")
	public String getP609Ps2() {
		return p609Ps2;
	}

	public void setP609Ps2(String p609Ps2) {
		this.p609Ps2 = p609Ps2;
	}

	@XmlElement(name = "P609_PS_3")
	public String getP609Ps3() {
		return p609Ps3;
	}

	public void setP609Ps3(String p609Ps3) {
		this.p609Ps3 = p609Ps3;
	}

	@XmlElement(name = "P609_PS_4")
	public String getP609Ps4() {
		return p609Ps4;
	}

	public void setP609Ps4(String p609Ps4) {
		this.p609Ps4 = p609Ps4;
	}

	@XmlElement(name = "P609_PS_5")
	public String getP609Ps5() {
		return p609Ps5;
	}

	public void setP609Ps5(String p609Ps5) {
		this.p609Ps5 = p609Ps5;
	}

	@XmlElement(name = "P609_PS_6")
	public String getP609Ps6() {
		return p609Ps6;
	}

	public void setP609Ps6(String p609Ps6) {
		this.p609Ps6 = p609Ps6;
	}

	@XmlElement(name = "P609_PS_7")
	public String getP609Ps7() {
		return p609Ps7;
	}

	public void setP609Ps7(String p609Ps7) {
		this.p609Ps7 = p609Ps7;
	}

	@XmlElement(name = "P609_PS_7E")
	public String getP609Ps7e() {
		return p609Ps7e;
	}

	public void setP609Ps7e(String p609Ps7e) {
		this.p609Ps7e = p609Ps7e;
	}

	@XmlElement(name = "P102_C4")
	public String getP102C4() {
		return p102C4;
	}

	public void setP102C4(String p102C4) {
		this.p102C4 = p102C4;
	}

	@XmlElement(name = "P103_C4")
	public Integer getP103C4() {
		return p103C4;
	}

	public void setP103C4(Integer p103C4) {
		this.p103C4 = p103C4;
	}

	@XmlElement(name = "P104_C4_LU")
	public Integer getP104C4Lu() {
		return p104C4Lu;
	}

	public void setP104C4Lu(Integer p104C4Lu) {
		this.p104C4Lu = p104C4Lu;
	}

	@XmlElement(name = "P104_C4_MA")
	public Integer getP104C4Ma() {
		return p104C4Ma;
	}

	public void setP104C4Ma(Integer p104C4Ma) {
		this.p104C4Ma = p104C4Ma;
	}

	@XmlElement(name = "P104_C4_MI")
	public Integer getP104C4Mi() {
		return p104C4Mi;
	}

	public void setP104C4Mi(Integer p104C4Mi) {
		this.p104C4Mi = p104C4Mi;
	}

	@XmlElement(name = "P104_C4_JU")
	public Integer getP104C4Ju() {
		return p104C4Ju;
	}

	public void setP104C4Ju(Integer p104C4Ju) {
		this.p104C4Ju = p104C4Ju;
	}

	@XmlElement(name = "P104_C4_VI")
	public Integer getP104C4Vi() {
		return p104C4Vi;
	}

	public void setP104C4Vi(Integer p104C4Vi) {
		this.p104C4Vi = p104C4Vi;
	}

	@XmlElement(name = "P104_C4_SA")
	public Integer getP104C4Sa() {
		return p104C4Sa;
	}

	public void setP104C4Sa(Integer p104C4Sa) {
		this.p104C4Sa = p104C4Sa;
	}

	@XmlElement(name = "P104_C4_DO")
	public Integer getP104C4Do() {
		return p104C4Do;
	}

	public void setP104C4Do(Integer p104C4Do) {
		this.p104C4Do = p104C4Do;
	}

	@XmlElement(name = "P105_C4A_1")
	public String getP105C4a1() {
		return p105C4a1;
	}

	public void setP105C4a1(String p105C4a1) {
		this.p105C4a1 = p105C4a1;
	}

	@XmlElement(name = "P105_C4A_2")
	public String getP105C4a2() {
		return p105C4a2;
	}

	public void setP105C4a2(String p105C4a2) {
		this.p105C4a2 = p105C4a2;
	}

	@XmlElement(name = "P105_C4A_3")
	public String getP105C4a3() {
		return p105C4a3;
	}

	public void setP105C4a3(String p105C4a3) {
		this.p105C4a3 = p105C4a3;
	}

	@XmlElement(name = "P105_C4A_4")
	public String getP105C4a4() {
		return p105C4a4;
	}

	public void setP105C4a4(String p105C4a4) {
		this.p105C4a4 = p105C4a4;
	}

	@XmlElement(name = "P105_C4A_5")
	public String getP105C4a5() {
		return p105C4a5;
	}

	public void setP105C4a5(String p105C4a5) {
		this.p105C4a5 = p105C4a5;
	}

	@XmlElement(name = "P105_C4A_5E")
	public String getP105C4a5e() {
		return p105C4a5e;
	}

	public void setP105C4a5e(String p105C4a5e) {
		this.p105C4a5e = p105C4a5e;
	}

	@XmlElement(name = "P105_C4I")
	public String getP105C4i() {
		return p105C4i;
	}

	public void setP105C4i(String p105C4i) {
		this.p105C4i = p105C4i;
	}

	@XmlElement(name = "P106_C4I")
	public Integer getP106C4i() {
		return p106C4i;
	}

	public void setP106C4i(Integer p106C4i) {
		this.p106C4i = p106C4i;
	}

	@XmlElement(name = "P107_C4_LU")
	public Integer getP107C4Lu() {
		return p107C4Lu;
	}

	public void setP107C4Lu(Integer p107C4Lu) {
		this.p107C4Lu = p107C4Lu;
	}

	@XmlElement(name = "P107_C4_MA")
	public Integer getP107C4Ma() {
		return p107C4Ma;
	}

	public void setP107C4Ma(Integer p107C4Ma) {
		this.p107C4Ma = p107C4Ma;
	}

	@XmlElement(name = "P107_C4_MI")
	public Integer getP107C4Mi() {
		return p107C4Mi;
	}

	public void setP107C4Mi(Integer p107C4Mi) {
		this.p107C4Mi = p107C4Mi;
	}

	@XmlElement(name = "P107_C4_JU")
	public Integer getP107C4Ju() {
		return p107C4Ju;
	}

	public void setP107C4Ju(Integer p107C4Ju) {
		this.p107C4Ju = p107C4Ju;
	}

	@XmlElement(name = "P107_C4_VI")
	public Integer getP107C4Vi() {
		return p107C4Vi;
	}

	public void setP107C4Vi(Integer p107C4Vi) {
		this.p107C4Vi = p107C4Vi;
	}

	@XmlElement(name = "P107_C4_SA")
	public Integer getP107C4Sa() {
		return p107C4Sa;
	}

	public void setP107C4Sa(Integer p107C4Sa) {
		this.p107C4Sa = p107C4Sa;
	}

	@XmlElement(name = "P107_C4_DO")
	public Integer getP107C4Do() {
		return p107C4Do;
	}

	public void setP107C4Do(Integer p107C4Do) {
		this.p107C4Do = p107C4Do;
	}

	@XmlElement(name = "PCONV_C4_1")
	public String getPconvC41() {
		return pconvC41;
	}

	public void setPconvC41(String pconvC41) {
		this.pconvC41 = pconvC41;
	}

	@XmlElement(name = "PCONV_C4_2")
	public String getPconvC42() {
		return pconvC42;
	}

	public void setPconvC42(String pconvC42) {
		this.pconvC42 = pconvC42;
	}

	@XmlElement(name = "PCONV_C4_3")
	public String getPconvC43() {
		return pconvC43;
	}

	public void setPconvC43(String pconvC43) {
		this.pconvC43 = pconvC43;
	}

	@XmlElement(name = "PCONV_C4_4")
	public String getPconvC44() {
		return pconvC44;
	}

	public void setPconvC44(String pconvC44) {
		this.pconvC44 = pconvC44;
	}

	@XmlElement(name = "PCONV_C4_5")
	public String getPconvC45() {
		return pconvC45;
	}

	public void setPconvC45(String pconvC45) {
		this.pconvC45 = pconvC45;
	}

	@XmlElement(name = "PCONV_C4_6")
	public String getPconvC46() {
		return pconvC46;
	}

	public void setPconvC46(String pconvC46) {
		this.pconvC46 = pconvC46;
	}

	@XmlElement(name = "PCONV_C4_7")
	public String getPconvC47() {
		return pconvC47;
	}

	public void setPconvC47(String pconvC47) {
		this.pconvC47 = pconvC47;
	}

	@XmlElement(name = "PCONV_C4_8")
	public String getPconvC48() {
		return pconvC48;
	}

	public void setPconvC48(String pconvC48) {
		this.pconvC48 = pconvC48;
	}

	@XmlElement(name = "PCONV_C4_E")
	public String getPconvC4E() {
		return pconvC4E;
	}

	public void setPconvC4E(String pconvC4E) {
		this.pconvC4E = pconvC4E;
	}

	@XmlElement(name = "PCOPAE_C4")
	public String getPcopaeC4() {
		return pcopaeC4;
	}

	public void setPcopaeC4(String pcopaeC4) {
		this.pcopaeC4 = pcopaeC4;
	}

	@XmlElement(name = "PCOPAE_RD")
	public String getPcopaeRd() {
		return pcopaeRd;
	}

	public void setPcopaeRd(String pcopaeRd) {
		this.pcopaeRd = pcopaeRd;
	}

	@XmlElement(name = "PCONEI_C4")
	public String getPconeiC4() {
		return pconeiC4;
	}

	public void setPconeiC4(String pconeiC4) {
		this.pconeiC4 = pconeiC4;
	}

	@XmlElement(name = "PCONEI_RD")
	public String getPconeiRd() {
		return pconeiRd;
	}

	public void setPconeiRd(String pconeiRd) {
		this.pconeiRd = pconeiRd;
	}

	@XmlElement(name = "P111_109_1")
	public String getP1111091() {
		return p1111091;
	}

	public void setP1111091(String p1111091) {
		this.p1111091 = p1111091;
	}

	@XmlElement(name = "P111_109_2")
	public String getP1111092() {
		return p1111092;
	}

	public void setP1111092(String p1111092) {
		this.p1111092 = p1111092;
	}

	@XmlElement(name = "P111_109_3")
	public String getP1111093() {
		return p1111093;
	}

	public void setP1111093(String p1111093) {
		this.p1111093 = p1111093;
	}

	@XmlElement(name = "P111_109_4")
	public String getP1111094() {
		return p1111094;
	}

	public void setP1111094(String p1111094) {
		this.p1111094 = p1111094;
	}

	@XmlElement(name = "P111_109E")
	public String getP111109e() {
		return p111109e;
	}

	public void setP111109e(String p111109e) {
		this.p111109e = p111109e;
	}

	@XmlElement(name = "P112_110_1")
	public String getP1121101() {
		return p1121101;
	}

	public void setP1121101(String p1121101) {
		this.p1121101 = p1121101;
	}

	@XmlElement(name = "P112_110_2")
	public String getP1121102() {
		return p1121102;
	}

	public void setP1121102(String p1121102) {
		this.p1121102 = p1121102;
	}

	@XmlElement(name = "P112_110_3")
	public String getP1121103() {
		return p1121103;
	}

	public void setP1121103(String p1121103) {
		this.p1121103 = p1121103;
	}

	@XmlElement(name = "P112_110_4")
	public String getP1121104() {
		return p1121104;
	}

	public void setP1121104(String p1121104) {
		this.p1121104 = p1121104;
	}

	@XmlElement(name = "P112_110_E")
	public String getP112110E() {
		return p112110E;
	}

	public void setP112110E(String p112110E) {
		this.p112110E = p112110E;
	}

	@XmlElement(name = "P113_111_R")
	public String getP113111R() {
		return p113111R;
	}

	public void setP113111R(String p113111R) {
		this.p113111R = p113111R;
	}

	@XmlElement(name = "P113_111_E")
	public String getP113111E() {
		return p113111E;
	}

	public void setP113111E(String p113111E) {
		this.p113111E = p113111E;
	}

	@XmlElement(name = "P114_112_R")
	public String getP114112R() {
		return p114112R;
	}

	public void setP114112R(String p114112R) {
		this.p114112R = p114112R;
	}

	@XmlElement(name = "P114_112_E")
	public String getP114112E() {
		return p114112E;
	}

	public void setP114112E(String p114112E) {
		this.p114112E = p114112E;
	}

	@XmlElement(name = "P402_C4")
	public String getP402C4() {
		return p402C4;
	}

	public void setP402C4(String p402C4) {
		this.p402C4 = p402C4;
	}

	@XmlElement(name = "P403_C4_1")
	public String getP403C41() {
		return p403C41;
	}

	public void setP403C41(String p403C41) {
		this.p403C41 = p403C41;
	}

	@XmlElement(name = "P403_C4_2")
	public String getP403C42() {
		return p403C42;
	}

	public void setP403C42(String p403C42) {
		this.p403C42 = p403C42;
	}

	@XmlElement(name = "P403_C4_3")
	public String getP403C43() {
		return p403C43;
	}

	public void setP403C43(String p403C43) {
		this.p403C43 = p403C43;
	}

	@XmlElement(name = "P403_C4_4")
	public String getP403C44() {
		return p403C44;
	}

	public void setP403C44(String p403C44) {
		this.p403C44 = p403C44;
	}

	@XmlElement(name = "P403_C4_5")
	public String getP403C45() {
		return p403C45;
	}

	public void setP403C45(String p403C45) {
		this.p403C45 = p403C45;
	}

	@XmlElement(name = "P403_C4_6")
	public String getP403C46() {
		return p403C46;
	}

	public void setP403C46(String p403C46) {
		this.p403C46 = p403C46;
	}

	@XmlElement(name = "P403_C4_7")
	public String getP403C47() {
		return p403C47;
	}

	public void setP403C47(String p403C47) {
		this.p403C47 = p403C47;
	}

	@XmlElement(name = "P403_C4_8")
	public String getP403C48() {
		return p403C48;
	}

	public void setP403C48(String p403C48) {
		this.p403C48 = p403C48;
	}

	@XmlElement(name = "P403_C4_9")
	public String getP403C49() {
		return p403C49;
	}

	public void setP403C49(String p403C49) {
		this.p403C49 = p403C49;
	}

	@XmlElement(name = "P403_C4_9E")
	public String getP403C49e() {
		return p403C49e;
	}

	public void setP403C49e(String p403C49e) {
		this.p403C49e = p403C49e;
	}

	@XmlElement(name = "P403_C4_10")
	public String getP403C410() {
		return p403C410;
	}

	public void setP403C410(String p403C410) {
		this.p403C410 = p403C410;
	}

	@XmlElement(name = "P404_C4_11")
	public String getP404C411() {
		return p404C411;
	}

	public void setP404C411(String p404C411) {
		this.p404C411 = p404C411;
	}

	@XmlElement(name = "P404_C4_12")
	public Integer getP404C412() {
		return p404C412;
	}

	public void setP404C412(Integer p404C412) {
		this.p404C412 = p404C412;
	}

	@XmlElement(name = "P404_C4_21")
	public String getP404C421() {
		return p404C421;
	}

	public void setP404C421(String p404C421) {
		this.p404C421 = p404C421;
	}

	@XmlElement(name = "P404_C4_22")
	public Integer getP404C422() {
		return p404C422;
	}

	public void setP404C422(Integer p404C422) {
		this.p404C422 = p404C422;
	}

	@XmlElement(name = "P404_C4_31")
	public String getP404C431() {
		return p404C431;
	}

	public void setP404C431(String p404C431) {
		this.p404C431 = p404C431;
	}

	@XmlElement(name = "P404_C4_32")
	public Integer getP404C432() {
		return p404C432;
	}

	public void setP404C432(Integer p404C432) {
		this.p404C432 = p404C432;
	}

	@XmlElement(name = "P404_C4_41")
	public String getP404C441() {
		return p404C441;
	}

	public void setP404C441(String p404C441) {
		this.p404C441 = p404C441;
	}

	@XmlElement(name = "P405_C4_1")
	public Integer getP405C41() {
		return p405C41;
	}

	public void setP405C41(Integer p405C41) {
		this.p405C41 = p405C41;
	}

	@XmlElement(name = "P405_C4_2")
	public Integer getP405C42() {
		return p405C42;
	}

	public void setP405C42(Integer p405C42) {
		this.p405C42 = p405C42;
	}

	@XmlElement(name = "P405_C4_3")
	public Integer getP405C43() {
		return p405C43;
	}

	public void setP405C43(Integer p405C43) {
		this.p405C43 = p405C43;
	}

	@XmlElement(name = "TDOCENTES")
	public Integer getTdocentes() {
		return tdocentes;
	}

	public void setTdocentes(Integer tdocentes) {
		this.tdocentes = tdocentes;
	}

	@XmlElement(name = "TDOCAULA")
	public Integer getTdocaula() {
		return tdocaula;
	}

	public void setTdocaula(Integer tdocaula) {
		this.tdocaula = tdocaula;
	}

	@XmlElement(name = "TAUXILIAR")
	public Integer getTauxiliar() {
		return tauxiliar;
	}

	public void setTauxiliar(Integer tauxiliar) {
		this.tauxiliar = tauxiliar;
	}

	@XmlElement(name = "TADMINIST")
	public Integer getTadminist() {
		return tadminist;
	}

	public void setTadminist(Integer tadminist) {
		this.tadminist = tadminist;
	}

	@XmlElement(name = "ANOTACIONES")
	public String getAnotaciones() {
		return anotaciones;
	}

	public void setAnotaciones(String anotaciones) {
		this.anotaciones = anotaciones;
	}

	@XmlElement(name = "FUENTE")
	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	@XmlElement(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	@XmlElement(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name = "TIPO_ENVIO")
	public String getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(String tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	@XmlElement(name = "ULTIMO")
	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	@XmlElement(name = "G_ETNICO_1")
	public String getgEtnico1() {
		return gEtnico1;
	}

	public void setgEtnico1(String gEtnico1) {
		this.gEtnico1 = gEtnico1;
	}

	@XmlElement(name = "G_ETNICO_2")
	public String getgEtnico2() {
		return gEtnico2;
	}

	public void setgEtnico2(String gEtnico2) {
		this.gEtnico2 = gEtnico2;
	}

	@XmlElement(name = "G_ETNICO_3")
	public String getgEtnico3() {
		return gEtnico3;
	}

	public void setgEtnico3(String gEtnico3) {
		this.gEtnico3 = gEtnico3;
	}

	@XmlElement(name = "G_ETNICO_4")
	public String getgEtnico4() {
		return gEtnico4;
	}

	public void setgEtnico4(String gEtnico4) {
		this.gEtnico4 = gEtnico4;
	}

	@XmlElement(name = "G_ETNICO_5")
	public String getgEtnico5() {
		return gEtnico5;
	}

	public void setgEtnico5(String gEtnico5) {
		this.gEtnico5 = gEtnico5;
	}

	@XmlElement(name = "G_ETNICO_6")
	public String getgEtnico6() {
		return gEtnico6;
	}

	public void setgEtnico6(String gEtnico6) {
		this.gEtnico6 = gEtnico6;
	}

	@XmlElement(name = "G_ETNICO_7")
	public String getgEtnico7() {
		return gEtnico7;
	}

	public void setgEtnico7(String gEtnico7) {
		this.gEtnico7 = gEtnico7;
	}

	@XmlElement(name = "G_ETNICO_8")
	public String getgEtnico8() {
		return gEtnico8;
	}

	public void setgEtnico8(String gEtnico8) {
		this.gEtnico8 = gEtnico8;
	}

	@XmlElement(name = "G_ETNICO_9")
	public String getgEtnico9() {
		return gEtnico9;
	}

	public void setgEtnico9(String gEtnico9) {
		this.gEtnico9 = gEtnico9;
	}

	@XmlElement(name = "G_ETNICO_E")
	public String getgEtnicoE() {
		return gEtnicoE;
	}

	public void setgEtnicoE(String gEtnicoE) {
		this.gEtnicoE = gEtnicoE;
	}

	@XmlElement(name = "P408_I")
	public String getP408I() {
		return p408I;
	}

	public void setP408I(String p408I) {
		this.p408I = p408I;
	}

	@XmlElement(name = "P408_II")
	public String getP408Ii() {
		return p408Ii;
	}

	public void setP408Ii(String p408Ii) {
		this.p408Ii = p408Ii;
	}

	@XmlElement(name = "CARRERAS")
	public List<Matricula2018Carreras> getMatricula2018CarrerasList() {
		return matricula2018CarrerasList;
	}

	public void setMatricula2018CarrerasList(List<Matricula2018Carreras> matricula2018CarrerasList) {
		this.matricula2018CarrerasList = matricula2018CarrerasList;
	}

	@XmlElement(name = "RECURSOS")
	// @XmlJavaTypeAdapter(Matricula2018RecursosMapAdapter.class)
	public List<Matricula2018Recursos> getDetalleRecursos() {
		return detalleRecursos;
	}

	public void setDetalleRecursos(List<Matricula2018Recursos> detalleRecursos) {
		this.detalleRecursos = detalleRecursos;
	}

	@XmlElement(name = "PERSONAL")
	public List<Matricula2018Personal> getMatricula2018PersonalList() {
		return matricula2018PersonalList;
	}

	public void setMatricula2018PersonalList(List<Matricula2018Personal> matricula2018PersonalList) {
		this.matricula2018PersonalList = matricula2018PersonalList;
	}

	@XmlElement(name = "LENGUA")
	public List<Matricula2018Dlen> getMatricula2018DlenList() {
		return matricula2018DlenList;
	}

	public void setMatricula2018DlenList(List<Matricula2018Dlen> matricula2018DlenList) {
		this.matricula2018DlenList = matricula2018DlenList;
	}

	@XmlElement(name = "SECCION")
	// @XmlJavaTypeAdapter(Matricula2018SeccionMapAdapter.class)
	public List<Matricula2018Seccion> getDetalleSeccion() {
		return detalleSeccion;
	}

	public void setDetalleSeccion(List<Matricula2018Seccion> detalleSeccion) {
		this.detalleSeccion = detalleSeccion;
	}

	// cambio

	@XmlElement(name = "MATRICULA")
	public List<Matricula2018Matricula> getDetalleMatricula() {
		return detalleMatricula;
	}

	public void setDetalleMatricula(List<Matricula2018Matricula> detalleMatricula) {
		this.detalleMatricula = detalleMatricula;
	}

	@XmlElement(name = "RESPONSABLE")
	public List<Matricula2018Resp> getMatricula2018RespList() {
		return matricula2018RespList;
	}

	public void setMatricula2018RespList(List<Matricula2018Resp> matricula2018RespList) {
		this.matricula2018RespList = matricula2018RespList;
	}

	@XmlElement(name = "EBA")
	public List<Matricula2018Eba> getMatricula2018EbaList() {
		return matricula2018EbaList;
	}

	public void setMatricula2018EbaList(List<Matricula2018Eba> matricula2018EbaList) {
		this.matricula2018EbaList = matricula2018EbaList;
	}

	@XmlElement(name = "PRONOEI")
	public List<Matricula2018Localpronoei> getMatricula2018LocalpronoeiList() {
		return matricula2018LocalpronoeiList;
	}

	public void setMatricula2018LocalpronoeiList(List<Matricula2018Localpronoei> matricula2018LocalpronoeiList) {
		this.matricula2018LocalpronoeiList = matricula2018LocalpronoeiList;
	}

	@XmlElement(name = "SAANEES")
	public List<Matricula2018Saanee> getMatricula2018SaaneeList() {
		return matricula2018SaaneeList;
	}

	public void setMatricula2018SaaneeList(List<Matricula2018Saanee> matricula2018SaaneeList) {
		this.matricula2018SaaneeList = matricula2018SaaneeList;
	}

	@XmlElement(name = "EIB_EST")
	public String getEibEst() {
		return eibEst;
	}

	public void setEibEst(String eibEst) {
		this.eibEst = eibEst;
	}

	@XmlElement(name = "P131_3AS_1")
	public String getP1313as1() {
		return p1313as1;
	}

	public void setP1313as1(String p1313as1) {
		this.p1313as1 = p1313as1;
	}

	@XmlElement(name = "P131_3AS_2")
	public String getP1313as2() {
		return p1313as2;
	}

	public void setP1313as2(String p1313as2) {
		this.p1313as2 = p1313as2;
	}

	@XmlElement(name = "P131_3AS_3")
	public String getP1313as3() {
		return p1313as3;
	}

	public void setP1313as3(String p1313as3) {
		this.p1313as3 = p1313as3;
	}

	@XmlElement(name = "P131_3AS_4")
	public String getP1313as4() {
		return p1313as4;
	}

	public void setP1313as4(String p1313as4) {
		this.p1313as4 = p1313as4;
	}

	@XmlElement(name = "P131_3AS_5")
	public String getP1313as5() {
		return p1313as5;
	}

	public void setP1313as5(String p1313as5) {
		this.p1313as5 = p1313as5;
	}

	@XmlElement(name = "P131_3AS_6")
	public String getP1313as6() {
		return p1313as6;
	}

	public void setP1313as6(String p1313as6) {
		this.p1313as6 = p1313as6;
	}

	@XmlElement(name = "P131_3AS_7")
	public String getP1313as7() {
		return p1313as7;
	}

	public void setP1313as7(String p1313as7) {
		this.p1313as7 = p1313as7;
	}

	@XmlElement(name = "P131_3AS_8")
	public String getP1313as8() {
		return p1313as8;
	}

	public void setP1313as8(String p1313as8) {
		this.p1313as8 = p1313as8;
	}

	@XmlElement(name = "P131_3AS_E")
	public String getP1313asE() {
		return p1313asE;
	}

	public void setP1313asE(String p1313asE) {
		this.p1313asE = p1313asE;
	}

	@XmlElement(name = "P605_D_PS")
	public String getP605DPs() {
		return p605DPs;
	}

	public void setP605DPs(String p605DPs) {
		this.p605DPs = p605DPs;
	}

	@XmlElement(name = "P605_QD_PS")
	public Short getP605QdPs() {
		return p605QdPs;
	}

	public void setP605QdPs(Short p605QdPs) {
		this.p605QdPs = p605QdPs;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2018Cabecera)) {
			return false;
		}
		Matricula2018Cabecera other = (Matricula2018Cabecera) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Cabecera[idEnvio=" + idEnvio + "]";
	}

}
