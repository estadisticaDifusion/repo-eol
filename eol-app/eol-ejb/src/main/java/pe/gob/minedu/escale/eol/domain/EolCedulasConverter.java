package pe.gob.minedu.escale.eol.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author WARODRIGUEZ
 *
 */

@XmlRootElement(name = "cedulas")
public class EolCedulasConverter {

	private List<EolCedulaConverter> items;

	@XmlElement(name = "items")
	public List<EolCedulaConverter> getItems() {
		return items;
	}

	public void setItems(List<EolCedulaConverter> items) {
		this.items = items;
	}

}
