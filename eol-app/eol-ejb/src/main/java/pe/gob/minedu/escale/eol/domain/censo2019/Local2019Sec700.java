package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2019_sec700")
public class Local2019Sec700 implements Serializable {
	private static final long serialVersionUID = 1L;
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Basic(optional = false)
	    @Column(name = "ID_ENVIO")
	    private Long idEnvio;

	    @Column(name = "CUADRO")
	    private String cuadro;
	    @Column(name = "ORDEN")
	    private Integer orden;
	    @Column(name = "P700_ESP")
	    private String p700Esp;
	    @Column(name = "P700_1")
	    private String p7001;
	    @Column(name = "P700_2")
	    private Integer p7002;
	    @Column(name = "P700_3")
	    private Integer p7003;
	    @Column(name = "P700_4")
	    private Integer p7004;
	    @Column(name = "P700_5")
	    private Integer p7005;
	    @Column(name = "P700_6")
	    private Integer p7006;
	    @Column(name = "P700_7")
	    private Integer p7007;
	    @Column(name = "P700_8")
	    private Integer p7008;
	    @Column(name = "P700_9")
	    private Integer p7009;
	    @Column(name = "P700_10")
	    private Integer p70010;
	    @Column(name = "P700_11")
	    private Integer p70011;
	    @Column(name = "P700_12")
	    private Integer p70012;
	    @Column(name = "P700_TOT")
	    private Integer p700Tot;
	    
	    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	    @ManyToOne
	    private Local2019Cabecera local2019Cabecera;

	    public Local2019Sec700() {
	    }

	    public Local2019Sec700(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }

	    public Long getIdEnvio() {
	        return idEnvio;
	    }

	    public void setIdEnvio(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }
	    @XmlElement(name = "CUADRO")
	    public String getCuadro() {
	        return cuadro;
	    }

	    public void setCuadro(String cuadro) {
	        this.cuadro = cuadro;
	    }
	    @XmlElement(name = "ORDEN")
	    public Integer getOrden() {
	        return orden;
	    }

	    public void setOrden(Integer orden) {
	        this.orden = orden;
	    }
	    @XmlElement(name = "P700_ESP")
	    public String getP700Esp() {
	        return p700Esp;
	    }

	    public void setP700Esp(String p700Esp) {
	        this.p700Esp = p700Esp;
	    }
	    @XmlElement(name = "P700_1")
	    public String getP7001() {
	        return p7001;
	    }

	    public void setP7001(String p7001) {
	        this.p7001 = p7001;
	    }
	    @XmlElement(name = "P700_2")
	    public Integer getP7002() {
	        return p7002;
	    }

	    public void setP7002(Integer p7002) {
	        this.p7002 = p7002;
	    }
	    @XmlElement(name = "P700_3")
	    public Integer getP7003() {
	        return p7003;
	    }

	    public void setP7003(Integer p7003) {
	        this.p7003 = p7003;
	    }
	    @XmlElement(name = "P700_4")
	    public Integer getP7004() {
	        return p7004;
	    }

	    public void setP7004(Integer p7004) {
	        this.p7004 = p7004;
	    }
	    @XmlElement(name = "P700_5")
	    public Integer getP7005() {
	        return p7005;
	    }

	    public void setP7005(Integer p7005) {
	        this.p7005 = p7005;
	    }
	    @XmlElement(name = "P700_6")
	    public Integer getP7006() {
	        return p7006;
	    }

	    public void setP7006(Integer p7006) {
	        this.p7006 = p7006;
	    }
	    @XmlElement(name = "P700_7")
	    public Integer getP7007() {
	        return p7007;
	    }

	    public void setP7007(Integer p7007) {
	        this.p7007 = p7007;
	    }
	    @XmlElement(name = "P700_8")
	    public Integer getP7008() {
	        return p7008;
	    }

	    public void setP7008(Integer p7008) {
	        this.p7008 = p7008;
	    }
	    @XmlElement(name = "P700_9")
	    public Integer getP7009() {
	        return p7009;
	    }

	    public void setP7009(Integer p7009) {
	        this.p7009 = p7009;
	    }
	    @XmlElement(name = "P700_10")
	    public Integer getP70010() {
	        return p70010;
	    }

	    public void setP70010(Integer p70010) {
	        this.p70010 = p70010;
	    }
	    @XmlElement(name = "P700_11")
	    public Integer getP70011() {
	        return p70011;
	    }

	    public void setP70011(Integer p70011) {
	        this.p70011 = p70011;
	    }
	    @XmlElement(name = "P700_12")
	    public Integer getP70012() {
	        return p70012;
	    }

	    public void setP70012(Integer p70012) {
	        this.p70012 = p70012;
	    }
	    @XmlElement(name = "P700_TOT")
	    public Integer getP700Tot() {
	        return p700Tot;
	    }

	    public void setP700Tot(Integer p700Tot) {
	        this.p700Tot = p700Tot;
	    }

	    @XmlTransient
	    public Local2019Cabecera getLocal2019Cabecera() {
	        return local2019Cabecera;
	    }

	    public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
	        this.local2019Cabecera = local2019Cabecera;
	    }

	    
	    @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
	        return hash;
	    }

	    @Override
	    public boolean equals(Object object) {
	        // TODO: Warning - this method won't work in the case the id fields are not set
	        if (!(object instanceof Local2019Sec700)) {
	            return false;
	        }
	        Local2019Sec700 other = (Local2019Sec700) object;
	        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
	            return false;
	        }
	        return true;
	    }

	    @Override
	    public String toString() {
	        return "pe.gob.minedu.entidad2019.Local2019Sec700[idEnvio=" + idEnvio + "]";
	    }
	
	
}
