/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.converter.EolActividadConverter;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="actividades")
public class EolActividadesConverter {

    private List<EolActividadConverter> items;

    @XmlElement(name="items")
    public List<EolActividadConverter> getItems() {
        return items;
    }

    public void setItems(List<EolActividadConverter> items) {
        this.items = items;
    }

    }
