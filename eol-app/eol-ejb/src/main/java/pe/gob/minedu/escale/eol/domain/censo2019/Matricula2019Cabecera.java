/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2019_cabecera")
public class Matricula2019Cabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03AP = "c03ap";
	public final static String CEDULA_03AS = "c03as";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_04AI = "c04ai";
	public final static String CEDULA_04AA = "c04aa";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08AI = "c08ai";
	public final static String CEDULA_08AP = "c08ap";
	public final static String CEDULA_09A = "c09a";
	public final static String CEDULA_CIRE = "cire";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;

	@Basic(optional = false)
	@Column(name = "NROCED")
	private String nroced;

	@Basic(optional = false)
	@Column(name = "COD_MOD")
	private String codMod;

	@Basic(optional = false)
	@Column(name = "ANEXO")
	private String anexo;

	@Column(name = "DNI_CORD")
	private String dniCord;

	@Basic(optional = false)
	@Column(name = "CEN_EDU")
	private String cenEdu;

	@Column(name = "CODLOCAL")
	private String codlocal;

	@Column(name = "DISTRITO")
	private String distrito;
	@Column(name = "LOCALIDAD")
	private String localidad;
	@Column(name = "TIPOIESUP")
	private String tipoiesup;
	@Column(name = "EGESTORA")
	private String egestora;
	@Column(name = "TIPOFINA_1")
	private String tipofina1;
	@Column(name = "TIPOFINA_2")
	private String tipofina2;
	@Column(name = "TIPOFINA_3")
	private String tipofina3;
	@Column(name = "TIPOFINA_4")
	private String tipofina4;
	@Column(name = "TIPOPROG")
	private String tipoprog;
	@Column(name = "NRORD_CRE")
	private String nrordCre;
	@Column(name = "FECRES_CD")
	private String fecresCd;
	@Column(name = "FECRES_CM")
	private String fecresCm;
	@Column(name = "FECRES_CA")
	private String fecresCa;
	@Column(name = "NRORD_REN")
	private String nrordRen;
	@Column(name = "FECRES_RD")
	private String fecresRd;
	@Column(name = "FECRES_RM")
	private String fecresRm;
	@Column(name = "FECRES_RA")
	private String fecresRa;
	@Column(name = "FORMATEN")
	private String formaten;
	@Column(name = "GESTION")
	private String gestion;
	@Column(name = "CODUGEL")
	private String codugel;
	@Column(name = "METATEN")
	private String metaten;
	@Column(name = "MSE_EBR")
	private String mseEbr;
	@Column(name = "MSE_SRE")
	private String mseSre;
	@Column(name = "MSE_ST")
	private String mseSt;
	@Basic(optional = false)
	@Column(name = "NIV_MOD")
	private String nivMod;
	@Column(name = "ALUM_35IIC")
	private String alum35iic;
	@Column(name = "ALUM_02IC")
	private String alum02ic;
	@Column(name = "P101_C5A")
	private String p101C5a;
	@Column(name = "P101_C5A_I")
	private String p101C5aI;
	@Column(name = "P101_C5A_P")
	private String p101C5aP;
	@Column(name = "P101_C5A_S")
	private String p101C5aS;

	@Column(name = "P102_C5A")
	private String p102C5a;
	@Column(name = "P102_C5A_1")
	private String p102C5a1;
	@Column(name = "QN_EST_C6A")
	private Integer qnEstC6a;
	@Column(name = "P1011_C6A")
	private String p1011C6a;
	@Column(name = "P1011_C6A1")
	private String p1011C6a1;
	@Column(name = "P1011_C6A2")
	private String p1011C6a2;
	@Column(name = "P1011_C6A3")
	private String p1011C6a3;
	@Column(name = "P1012_C6A1")
	private String p1012C6a1;
	@Column(name = "P1012_C6A2")
	private String p1012C6a2;
	@Column(name = "P1012_C6A3")
	private String p1012C6a3;
	@Column(name = "P1021_C7A")
	private Integer p1021C7a;
	@Column(name = "P1022_C7A")
	private String p1022C7a;
	@Column(name = "P103_C7A_1")
	private String p103C7a1;
	@Column(name = "P103_C7A_2")
	private String p103C7a2;
	@Column(name = "P103_C7A_3")
	private String p103C7a3;
	@Column(name = "P103_C7A_4")
	private String p103C7a4;
	@Column(name = "P103_C7A_5")
	private String p103C7a5;
	@Column(name = "P103_C7A_6")
	private String p103C7a6;
	@Column(name = "P103_C7A_E")
	private String p103C7aE;
	@Column(name = "P101_C1")
	private String p101C1;
	@Column(name = "P101_C1_A")
	private String p101C1A;
	@Column(name = "INI_CLAS_D")
	private String iniClasD;
	@Column(name = "INI_CLAS_M")
	private String iniClasM;
	@Column(name = "P101_C2_1")
	private String p101C21;
	@Column(name = "P101_C2_2")
	private String p101C22;
	@Column(name = "P101_C2_3")
	private String p101C23;
	@Column(name = "P101_C2_4")
	private String p101C24;
	@Column(name = "FIN_CLAS_D")
	private String finClasD;
	@Column(name = "FIN_CLAS_M")
	private String finClasM;
	@Column(name = "P103_C2_1")
	private String p103C21;
	@Column(name = "P103_C2_2")
	private String p103C22;
	@Column(name = "P103_C2_3")
	private String p103C23;
	@Column(name = "P103_C2_4")
	private String p103C24;
	@Column(name = "P103_C2_5")
	private String p103C25;
	@Column(name = "P103_C2_6")
	private String p103C26;
	@Column(name = "PL_TUTORIA")
	private String plTutoria;
	@Column(name = "PAT_2019")
	private String pat2019;
	@Column(name = "P105_C3PS")
	private String p105C3ps;
	@Column(name = "P106_C3PS")
	private String p106C3ps;
	@Column(name = "P107_C3PS")
	private String p107C3ps;
	@Column(name = "P108_C3PS")
	private String p108C3ps;
	@Column(name = "P109_C3_01")
	private String p109C301;
	@Column(name = "P109_C3_02")
	private String p109C302;
	@Column(name = "P109_C3_03")
	private String p109C303;
	@Column(name = "P109_C3_04")
	private String p109C304;
	@Column(name = "P109_C3_05")
	private String p109C305;
	@Column(name = "P109_C3_06")
	private String p109C306;
	@Column(name = "P109_C3_07")
	private String p109C307;
	@Column(name = "P109_C3_08")
	private String p109C308;
	@Column(name = "P109_C3_09")
	private String p109C309;
	@Column(name = "P109_C3_10")
	private String p109C310;
	@Column(name = "P109_C3_11")
	private String p109C311;
	@Column(name = "P109_C3_12")
	private String p109C312;
	@Column(name = "P109_C3_13")
	private String p109C313;
	@Column(name = "P109_C3_14")
	private String p109C314;
	@Column(name = "P109_C314E")
	private String p109C314e;
	@Column(name = "N_CONV_1")
	private String nConv1;
	@Column(name = "N_CONV_2")
	private String nConv2;
	@Column(name = "N_CONV_3")
	private String nConv3;
	@Column(name = "LIBRO_IN_1")
	private String libroIn1;
	@Column(name = "LIBRO_IN_2")
	private String libroIn2;
	@Column(name = "LIBRO_IN_3")
	private String libroIn3;
	@Column(name = "LIBRO_IN_Q")
	private Integer libroInQ;
	@Column(name = "LIBRO_IN_D")
	private String libroInD;
	@Column(name = "LIBRO_IN_M")
	private String libroInM;
	@Column(name = "LIBRO_IN_A")
	private String libroInA;
	@Column(name = "AF_SISEVE")
	private String afSiseve;
	@Column(name = "RPT_SISEVE")
	private String rptSiseve;
	@Column(name = "SISEVE_QN")
	private Integer siseveQn;
	@Column(name = "NO_SISEVE")
	private String noSiseve;
	@Column(name = "SISEVE_ESP")
	private String siseveEsp;
	@Column(name = "LC_ANEMIA")
	private String lcAnemia;
	@Column(name = "SERV_NE_1")
	private String servNe1;
	@Column(name = "SERV_NE_2")
	private String servNe2;
	@Column(name = "SERV_NE_3")
	private String servNe3;
	@Column(name = "SERV_NE_4")
	private String servNe4;
	@Column(name = "SERV_NE_5")
	private String servNe5;
	@Column(name = "SERV_NE_6")
	private String servNe6;
	@Column(name = "SERV_NE_6E")
	private String servNe6e;
	@Column(name = "SERV_NE_7")
	private String servNe7;
	@Column(name = "VACAN_MAT")
	private String vacanMat;
	@Column(name = "VAC_MAT_NO")
	private String vacMatNo;
	@Column(name = "G_ETNICO_1")
	private String gEtnico1;
	@Column(name = "G_ETNICO_2")
	private String gEtnico2;
	@Column(name = "G_ETNICO_3")
	private String gEtnico3;
	@Column(name = "G_ETNICO_4")
	private String gEtnico4;
	@Column(name = "G_ETNICO_5")
	private String gEtnico5;
	@Column(name = "G_ETNICO_6")
	private String gEtnico6;
	@Column(name = "G_ETNICO_7")
	private String gEtnico7;
	@Column(name = "G_ETNICO_8")
	private String gEtnico8;
	@Column(name = "G_ETNICO_9")
	private String gEtnico9;
	@Column(name = "G_ETNICO_E")
	private String gEtnicoE;
	@Column(name = "GRUPO_ETN")
	private String grupoEtn;
	@Column(name = "GRP_ETN_C")
	private Integer grpEtnC;
	@Column(name = "ES_EIB")
	private String esEib;
	@Column(name = "EIB_EST")
	private String eibEst;
	@Column(name = "EIB_RES")
	private String eibRes;
	@Column(name = "CAS_2_LEN")
	private String cas2Len;
	@Column(name = "ORIG_2_LEN")
	private String orig2Len;
	@Column(name = "HLEN_ORIG")
	private String hlenOrig;
	@Column(name = "COD_LORIG")
	private String codLorig;
	@Column(name = "EHLEN_ORI")
	private String ehlenOri;
	@Column(name = "SI_LENORIG")
	private String siLenorig;
	@Column(name = "SI_CODLEN")
	private String siCodlen;
	@Column(name = "P130_C3AS")
	private String p130C3as;
	@Column(name = "P131_C3AS")
	private String p131C3as;
	@Column(name = "P131_3AS_1")
	private String p1313as1;
	@Column(name = "P131_3AS_2")
	private String p1313as2;
	@Column(name = "P131_3AS_3")
	private String p1313as3;
	@Column(name = "P131_3AS_4")
	private String p1313as4;
	@Column(name = "P131_3AS_5")
	private String p1313as5;
	@Column(name = "P131_3AS_6")
	private String p1313as6;
	@Column(name = "P131_3AS_7")
	private String p1313as7;
	@Column(name = "P131_3AS_8")
	private String p1313as8;
	@Column(name = "P131_3AS_E")
	private String p1313asE;
	@Column(name = "P102_C4")
	private String p102C4;
	@Column(name = "P103_C4")
	private Integer p103C4;
	@Column(name = "P104_C4_LU")
	private Integer p104C4Lu;
	@Column(name = "P104_C4_MA")
	private Integer p104C4Ma;
	@Column(name = "P104_C4_MI")
	private Integer p104C4Mi;
	@Column(name = "P104_C4_JU")
	private Integer p104C4Ju;
	@Column(name = "P104_C4_VI")
	private Integer p104C4Vi;
	@Column(name = "P104_C4_SA")
	private Integer p104C4Sa;
	@Column(name = "P104_C4_DO")
	private Integer p104C4Do;
	@Column(name = "P105_C4I")
	private String p105C4i;
	@Column(name = "P106_C4I")
	private Integer p106C4i;
	@Column(name = "P107_C4_LU")
	private Integer p107C4Lu;
	@Column(name = "P107_C4_MA")
	private Integer p107C4Ma;
	@Column(name = "P107_C4_MI")
	private Integer p107C4Mi;
	@Column(name = "P107_C4_JU")
	private Integer p107C4Ju;
	@Column(name = "P107_C4_VI")
	private Integer p107C4Vi;
	@Column(name = "P107_C4_SA")
	private Integer p107C4Sa;
	@Column(name = "P107_C4_DO")
	private Integer p107C4Do;
	@Column(name = "P105_C4A_1")
	private String p105C4a1;
	@Column(name = "P105_C4A_2")
	private String p105C4a2;
	@Column(name = "P105_C4A_3")
	private String p105C4a3;
	@Column(name = "P105_C4A_4")
	private String p105C4a4;
	@Column(name = "P105_C4A_5")
	private String p105C4a5;
	@Column(name = "P105_C4A_5E")
	private String p105C4a5e;
	@Column(name = "PCONV_C4_1")
	private String pconvC41;
	@Column(name = "PCONV_C4_2")
	private String pconvC42;
	@Column(name = "PCONV_C4_3")
	private String pconvC43;
	@Column(name = "PCONV_C4_4")
	private String pconvC44;
	@Column(name = "PCONV_C4_5")
	private String pconvC45;
	@Column(name = "PCONV_C4_6")
	private String pconvC46;
	@Column(name = "PCONV_C4_7")
	private String pconvC47;
	@Column(name = "PCONV_C4_8")
	private String pconvC48;
	@Column(name = "PCONV_C4_E")
	private String pconvC4E;
	@Column(name = "PCOPAE_C4")
	private String pcopaeC4;
	@Column(name = "PCOPAE_RD")
	private String pcopaeRd;
	@Column(name = "PCONEI_C4")
	private String pconeiC4;
	@Column(name = "PCONEI_RD")
	private String pconeiRd;
	@Column(name = "P111_109_1")
	private String p1111091;
	@Column(name = "P111_109_2")
	private String p1111092;
	@Column(name = "P111_109_3")
	private String p1111093;
	@Column(name = "P111_109_4")
	private String p1111094;
	@Column(name = "P111_109E")
	private String p111109e;
	@Column(name = "P112_110_1")
	private String p1121101;
	@Column(name = "P112_110_2")
	private String p1121102;
	@Column(name = "P112_110_3")
	private String p1121103;
	@Column(name = "P112_110_4")
	private String p1121104;
	@Column(name = "P112_110_E")
	private String p112110E;
	@Column(name = "P113_111_R")
	private String p113111R;
	@Column(name = "P113_111_E")
	private String p113111E;
	@Column(name = "P114_112_R")
	private String p114112R;
	@Column(name = "P114_112_E")
	private String p114112E;
	@Column(name = "TDOCENTES")
	private Integer tdocentes;
	@Column(name = "TDOCAULA")
	private Integer tdocaula;
	@Column(name = "TAUXILIAR")
	private Integer tauxiliar;
	@Column(name = "TADMINIST")
	private Integer tadminist;
	@Column(name = "P333")
	private String p333;
	@Column(name = "P333_QN")
	private Integer p333Qn;
	@Column(name = "P402_C8")
	private String p402C8;
	@Column(name = "P402_C8_Q")
	private Integer p402C8Q;
	@Column(name = "P401")
	private String p401;
	@Column(name = "P403_C3AP")
	private String p403C3ap;
	@Column(name = "P405_C3AP")
	private String p405C3ap;
	@Column(name = "P406_C3AP")
	private String p406C3ap;
	@Column(name = "KIT_CAS4")
	private Integer kitCas4;
	@Column(name = "KIT_CAS5")
	private Integer kitCas5;
	@Column(name = "KIT_ORI4")
	private Integer kitOri4;
	@Column(name = "KIT_ORI5")
	private Integer kitOri5;
	@Column(name = "P403")
	private String p403;
	@Column(name = "P404")
	private String p404;
	@Column(name = "P405")
	private Integer p405;
	@Column(name = "P406")
	private String p406;
	@Column(name = "P407")
	private String p407;
	@Column(name = "P407_SI")
	private String p407Si;
	@Column(name = "P401_C4_11")
	private String p401C411;
	@Column(name = "P401_C4_12")
	private Integer p401C412;
	@Column(name = "P401_C4_21")
	private String p401C421;
	@Column(name = "P401_C4_22")
	private Integer p401C422;
	@Column(name = "P401_C4_31")
	private String p401C431;
	@Column(name = "P401_C4_32")
	private Integer p401C432;
	@Column(name = "P401_C4_41")
	private String p401C441;
	@Column(name = "P402_C4_41")
	private String p402C441;
	@Column(name = "P403_C4_1")
	private String p403C41;
	@Column(name = "P403_C4_2")
	private String p403C42;
	@Column(name = "P403_C4_3")
	private String p403C43;
	@Column(name = "P403_C4_4")
	private String p403C44;
	@Column(name = "P403_C4_5")
	private String p403C45;
	@Column(name = "P403_C4_6")
	private String p403C46;
	@Column(name = "P403_C4_7")
	private String p403C47;
	@Column(name = "P403_C4_8")
	private String p403C48;
	@Column(name = "P403_C4_9")
	private String p403C49;
	@Column(name = "P403_C4_9E")
	private String p403C49e;
	@Column(name = "P403_C4_10")
	private String p403C410;
	@Column(name = "P405_C4_1")
	private Integer p405C41;
	@Column(name = "P405_C4_2")
	private Integer p405C42;
	@Column(name = "P405_C4_3")
	private Integer p405C43;
	@Column(name = "P406_C4_1")
	private Integer p406C41;
	@Column(name = "P406_C4_2")
	private Integer p406C42;
	@Column(name = "P406_C4_3")
	private Integer p406C43;
	@Column(name = "P501_C8")
	private String p501C8;
	@Column(name = "P502_C8")
	private String p502C8;
	@Column(name = "P503_C8")
	private Integer p503C8;
	@Column(name = "P504_C8")
	private String p504C8;
	@Column(name = "P505_C8")
	private String p505C8;
	@Column(name = "P502")
	private String p502;
	@Column(name = "P502_D")
	private String p502D;
	@Column(name = "P502_M")
	private String p502M;
	@Column(name = "P502_A")
	private String p502A;
	@Column(name = "P503")
	private String p503;
	@Column(name = "RUTA_SBI")
	private String rutaSbi;
	@Column(name = "RUTA_SBI_1")
	private Integer rutaSbi1;
	@Column(name = "RUTA_SBI_2")
	private String rutaSbi2;
	@Column(name = "RUTA_SLA")
	private String rutaSla;
	@Column(name = "RUTA_SLA_1")
	private Integer rutaSla1;
	@Column(name = "RUTA_SLA_2")
	private String rutaSla2;
	@Column(name = "P601_QH")
	private Integer p601Qh;
	@Column(name = "P601_QM")
	private Integer p601Qm;
	@Column(name = "P601_PS")
	private String p601Ps;
	@Column(name = "P602_PS")
	private String p602Ps;
	@Column(name = "P603_PS")
	private String p603Ps;
	@Column(name = "P604_PS")
	private String p604Ps;
	@Column(name = "P605_E_PS")
	private String p605EPs;
	@Column(name = "P605_QE_PS")
	private Integer p605QePs;
	@Column(name = "P605_D_PS")
	private String p605DPs;
	@Column(name = "P605_QD_PS")
	private Integer p605QdPs;
	@Column(name = "P606_PS")
	private String p606Ps;
	@Column(name = "P606_HS_PS")
	private Integer p606HsPs;
	@Column(name = "P606_DA_PS")
	private Integer p606DaPs;
	@Column(name = "P607_PS")
	private String p607Ps;
	@Column(name = "P608_PS")
	private String p608Ps;
	@Column(name = "P609_PS_1")
	private String p609Ps1;
	@Column(name = "P609_PS_2")
	private String p609Ps2;
	@Column(name = "P609_PS_3")
	private String p609Ps3;
	@Column(name = "P609_PS_4")
	private String p609Ps4;
	@Column(name = "P609_PS_5")
	private String p609Ps5;
	@Column(name = "P609_PS_6")
	private String p609Ps6;
	@Column(name = "P609_PS_7")
	private String p609Ps7;
	@Column(name = "P609_PS_7E")
	private String p609Ps7e;

	@Column(name = "P701_PS")
	private String p701Ps;

	@Column(name = "P702_PS_1")
	private Integer p702Ps1;

	@Column(name = "P702_PS_2")
	private Integer p702Ps2;

	@Column(name = "P702_PS_3")
	private Integer p702Ps3;

	@Column(name = "P702_PS_4")
	private Integer p702Ps4;

	@Column(name = "P702_PS_5")
	private Integer p702Ps5;

	@Column(name = "P702_PS_6")
	private Integer p702Ps6;

	@Column(name = "P702_PS_7")
	private Integer p702Ps7;

	@Column(name = "P702_PS_8")
	private Integer p702Ps8;

	@Column(name = "P702_PS_8E")
	private String p702Ps8e;
	@Column(name = "P801_C3AS")
	private String p801C3as;
	@Column(name = "P801_C3_1")
	private String p801C31;
	@Column(name = "P801_C3_2")
	private String p801C32;
	@Column(name = "P801_C3_3")
	private String p801C33;
	@Column(name = "P801_C3_4")
	private String p801C34;

	@Column(name = "P801_C3_5")
	private String p801C35;

	@Column(name = "P801_C3_5E")
	private String p801C35e;

	@Column(name = "P802_C3AS")
	private String p802C3as;

	@Column(name = "P802_C3SI")
	private String p802C3si;

	@Column(name = "P803_C3AS")
	private String p803C3as;

	@Column(name = "P803_C3SI")
	private String p803C3si;

	@Column(name = "P804_C3AS")
	private String p804C3as;

	@Column(name = "P804_C3ASE")
	private String p804C3ase;

	@Column(name = "ANOTACIONES")
	private String anotaciones;

	@Column(name = "FUENTE")
	private String fuente;

	@Column(name = "FECHA_ENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;

	@Column(name = "VERSION")
	private String version;

	@Column(name = "TIPO_ENVIO")
	private String tipoEnvio;

	@Column(name = "ULTIMO")
	private Boolean ultimo;

//    @MapKeyColumn(name = "cuadro", length = 5)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Seccion> detalleSeccion;

//    @MapKeyColumn(name = "cuadro", length = 5)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Matricula> detalleMatricula;

//    @MapKeyColumn(name = "cuadro", length = 5)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Recursos> matricula2019RecursosList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Personal> matricula2019PersonalList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Carreras> matricula2019CarrerasList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Localpronoei> matricula2019LocalpronoeiList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Sede> matricula2019SedeList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Muledad> matricula2019MuledadList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Resp> matricula2019RespList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Dlen> matricula2019DlenList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Eba> matricula2019EbaList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019S100> matricula2019S100List;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Matricula2019Saanee> matricula2019SaaneeList;

	@Transient
	private long token;

	@Transient
	private String msg;
	@Transient
	private String estadoRpt;

	public Matricula2019Cabecera() {

	}

	public Matricula2019Cabecera(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Matricula2019Cabecera(Long idEnvio, String nroced, String codMod, String anexo, String cenEdu,
			String codlocal, String nivMod) {
		this.idEnvio = idEnvio;
		this.nroced = nroced;
		this.codMod = codMod;
		this.anexo = anexo;
		this.cenEdu = cenEdu;
		this.codlocal = codlocal;
		this.nivMod = nivMod;
	}

	@XmlAttribute
	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	@XmlAttribute(name = "MSG")
	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@XmlAttribute(name = "ESTADO_RPT")
	public String getEstadoRpt() {
		return this.estadoRpt;
	}

	public void setEstadoRpt(String estadoRpt) {
		this.estadoRpt = estadoRpt;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "NROCED")
	public String getNroced() {
		return nroced;
	}

	public void setNroced(String nroced) {
		this.nroced = nroced;
	}

	@XmlElement(name = "COD_MOD")
	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	@XmlElement(name = "ANEXO")
	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	@XmlElement(name = "DNI_CORD")
	public String getDniCord() {
		return dniCord;
	}

	public void setDniCord(String dniCord) {
		this.dniCord = dniCord;
	}

	@XmlElement(name = "CEN_EDU")
	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	@XmlElement(name = "CODLOCAL")
	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	@XmlElement(name = "DISTRITO")
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@XmlElement(name = "LOCALIDAD")
	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	@XmlElement(name = "EGESTORA")
	public String getEgestora() {
		return egestora;
	}

	public void setEgestora(String egestora) {
		this.egestora = egestora;
	}

	@XmlElement(name = "TIPOFINA_1")
	public String getTipofina1() {
		return tipofina1;
	}

	public void setTipofina1(String tipofina1) {
		this.tipofina1 = tipofina1;
	}

	@XmlElement(name = "TIPOFINA_2")
	public String getTipofina2() {
		return tipofina2;
	}

	public void setTipofina2(String tipofina2) {
		this.tipofina2 = tipofina2;
	}

	@XmlElement(name = "TIPOFINA_3")
	public String getTipofina3() {
		return tipofina3;
	}

	public void setTipofina3(String tipofina3) {
		this.tipofina3 = tipofina3;
	}

	@XmlElement(name = "TIPOFINA_4")
	public String getTipofina4() {
		return tipofina4;
	}

	public void setTipofina4(String tipofina4) {
		this.tipofina4 = tipofina4;
	}

	@XmlElement(name = "TIPOPROG")
	public String getTipoprog() {
		return tipoprog;
	}

	public void setTipoprog(String tipoprog) {
		this.tipoprog = tipoprog;
	}

	@XmlElement(name = "NRORD_CRE")
	public String getNrordCre() {
		return nrordCre;
	}

	public void setNrordCre(String nrordCre) {
		this.nrordCre = nrordCre;
	}

	@XmlElement(name = "NRORD_REN")
	public String getNrordRen() {
		return nrordRen;
	}

	public void setNrordRen(String nrordRen) {
		this.nrordRen = nrordRen;
	}

	@XmlElement(name = "NIV_MOD")
	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	@XmlElement(name = "FORMATEN")
	public String getFormaten() {
		return formaten;
	}

	public void setFormaten(String formaten) {
		this.formaten = formaten;
	}

	@XmlElement(name = "ALUM_02IC")
	public String getAlum02ic() {
		return alum02ic;
	}

	public void setAlum02ic(String alum02ic) {
		this.alum02ic = alum02ic;
	}

	@XmlElement(name = "GESTION")
	public String getGestion() {
		return gestion;
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	@XmlElement(name = "CODUGEL")
	public String getCodugel() {
		return codugel;
	}

	public void setCodugel(String codugel) {
		this.codugel = codugel;
	}

	@XmlElement(name = "TIPOIESUP")
	public String getTipoiesup() {
		return tipoiesup;
	}

	public void setTipoiesup(String tipoiesup) {
		this.tipoiesup = tipoiesup;
	}

	@XmlElement(name = "P101_C5A")
	public String getP101C5a() {
		return p101C5a;
	}

	public void setP101C5a(String p101C5a) {
		this.p101C5a = p101C5a;
	}

	@XmlElement(name = "P101_C5A_I")
	public String getP101C5aI() {
		return p101C5aI;
	}

	public void setP101C5aI(String p101C5aI) {
		this.p101C5aI = p101C5aI;
	}

	@XmlElement(name = "P101_C5A_P")
	public String getP101C5aP() {
		return p101C5aP;
	}

	public void setP101C5aP(String p101C5aP) {
		this.p101C5aP = p101C5aP;
	}

	@XmlElement(name = "P101_C5A_S")
	public String getP101C5aS() {
		return p101C5aS;
	}

	public void setP101C5aS(String p101C5aS) {
		this.p101C5aS = p101C5aS;
	}

	@XmlElement(name = "P102_C5A")
	public String getP102C5a() {
		return p102C5a;
	}

	public void setP102C5a(String p102C5a) {
		this.p102C5a = p102C5a;
	}

	@XmlElement(name = "P102_C5A_1")
	public String getP102C5a1() {
		return p102C5a1;
	}

	public void setP102C5a1(String p102C5a1) {
		this.p102C5a1 = p102C5a1;
	}

	@XmlElement(name = "P101_C1")
	public String getP101C1() {
		return p101C1;
	}

	public void setP101C1(String p101C1) {
		this.p101C1 = p101C1;
	}

	@XmlElement(name = "P101_C1_A")
	public String getP101C1A() {
		return p101C1A;
	}

	public void setP101C1A(String p101C1A) {
		this.p101C1A = p101C1A;
	}

	@XmlElement(name = "INI_CLAS_D")
	public String getIniClasD() {
		return iniClasD;
	}

	public void setIniClasD(String iniClasD) {
		this.iniClasD = iniClasD;
	}

	@XmlElement(name = "INI_CLAS_M")
	public String getIniClasM() {
		return iniClasM;
	}

	public void setIniClasM(String iniClasM) {
		this.iniClasM = iniClasM;
	}

	@XmlElement(name = "P101_C2_1")
	public String getP101C21() {
		return p101C21;
	}

	public void setP101C21(String p101C21) {
		this.p101C21 = p101C21;
	}

	@XmlElement(name = "P101_C2_2")
	public String getP101C22() {
		return p101C22;
	}

	public void setP101C22(String p101C22) {
		this.p101C22 = p101C22;
	}

	@XmlElement(name = "P101_C2_3")
	public String getP101C23() {
		return p101C23;
	}

	public void setP101C23(String p101C23) {
		this.p101C23 = p101C23;
	}

	@XmlElement(name = "P101_C2_4")
	public String getP101C24() {
		return p101C24;
	}

	public void setP101C24(String p101C24) {
		this.p101C24 = p101C24;
	}

	@XmlElement(name = "FIN_CLAS_D")
	public String getFinClasD() {
		return finClasD;
	}

	public void setFinClasD(String finClasD) {
		this.finClasD = finClasD;
	}

	@XmlElement(name = "FIN_CLAS_M")
	public String getFinClasM() {
		return finClasM;
	}

	public void setFinClasM(String finClasM) {
		this.finClasM = finClasM;
	}

	@XmlElement(name = "P103_C2_1")
	public String getP103C21() {
		return p103C21;
	}

	public void setP103C21(String p103C21) {
		this.p103C21 = p103C21;
	}

	@XmlElement(name = "P103_C2_2")
	public String getP103C22() {
		return p103C22;
	}

	public void setP103C22(String p103C22) {
		this.p103C22 = p103C22;
	}

	@XmlElement(name = "P103_C2_3")
	public String getP103C23() {
		return p103C23;
	}

	public void setP103C23(String p103C23) {
		this.p103C23 = p103C23;
	}

	@XmlElement(name = "P103_C2_4")
	public String getP103C24() {
		return p103C24;
	}

	public void setP103C24(String p103C24) {
		this.p103C24 = p103C24;
	}

	@XmlElement(name = "P103_C2_5")
	public String getP103C25() {
		return p103C25;
	}

	public void setP103C25(String p103C25) {
		this.p103C25 = p103C25;
	}

	@XmlElement(name = "P103_C2_6")
	public String getP103C26() {
		return p103C26;
	}

	public void setP103C26(String p103C26) {
		this.p103C26 = p103C26;
	}

	@XmlElement(name = "PL_TUTORIA")
	public String getPlTutoria() {
		return plTutoria;
	}

	public void setPlTutoria(String plTutoria) {
		this.plTutoria = plTutoria;
	}

	@XmlElement(name = "N_CONV_1")
	public String getNConv1() {
		return nConv1;
	}

	public void setNConv1(String nConv1) {
		this.nConv1 = nConv1;
	}

	@XmlElement(name = "N_CONV_2")
	public String getNConv2() {
		return nConv2;
	}

	public void setNConv2(String nConv2) {
		this.nConv2 = nConv2;
	}

	@XmlElement(name = "N_CONV_3")
	public String getNConv3() {
		return nConv3;
	}

	public void setNConv3(String nConv3) {
		this.nConv3 = nConv3;
	}

	@XmlElement(name = "LIBRO_IN_1")
	public String getLibroIn1() {
		return libroIn1;
	}

	public void setLibroIn1(String libroIn1) {
		this.libroIn1 = libroIn1;
	}

	@XmlElement(name = "LIBRO_IN_2")
	public String getLibroIn2() {
		return libroIn2;
	}

	public void setLibroIn2(String libroIn2) {
		this.libroIn2 = libroIn2;
	}

	@XmlElement(name = "LIBRO_IN_3")
	public String getLibroIn3() {
		return libroIn3;
	}

	public void setLibroIn3(String libroIn3) {
		this.libroIn3 = libroIn3;
	}

	@XmlElement(name = "LIBRO_IN_D")
	public String getLibroInD() {
		return libroInD;
	}

	public void setLibroInD(String libroInD) {
		this.libroInD = libroInD;
	}

	@XmlElement(name = "LIBRO_IN_M")
	public String getLibroInM() {
		return libroInM;
	}

	public void setLibroInM(String libroInM) {
		this.libroInM = libroInM;
	}

	@XmlElement(name = "LIBRO_IN_A")
	public String getLibroInA() {
		return libroInA;
	}

	public void setLibroInA(String libroInA) {
		this.libroInA = libroInA;
	}

	@XmlElement(name = "SERV_NE_1")
	public String getServNe1() {
		return servNe1;
	}

	public void setServNe1(String servNe1) {
		this.servNe1 = servNe1;
	}

	@XmlElement(name = "SERV_NE_2")
	public String getServNe2() {
		return servNe2;
	}

	public void setServNe2(String servNe2) {
		this.servNe2 = servNe2;
	}

	@XmlElement(name = "SERV_NE_3")
	public String getServNe3() {
		return servNe3;
	}

	public void setServNe3(String servNe3) {
		this.servNe3 = servNe3;
	}

	@XmlElement(name = "SERV_NE_4")
	public String getServNe4() {
		return servNe4;
	}

	public void setServNe4(String servNe4) {
		this.servNe4 = servNe4;
	}

	@XmlElement(name = "SERV_NE_5")
	public String getServNe5() {
		return servNe5;
	}

	public void setServNe5(String servNe5) {
		this.servNe5 = servNe5;
	}

	@XmlElement(name = "SERV_NE_6")
	public String getServNe6() {
		return servNe6;
	}

	public void setServNe6(String servNe6) {
		this.servNe6 = servNe6;
	}

	@XmlElement(name = "SERV_NE_6E")
	public String getServNe6e() {
		return servNe6e;
	}

	public void setServNe6e(String servNe6e) {
		this.servNe6e = servNe6e;
	}

	@XmlElement(name = "SERV_NE_7")
	public String getServNe7() {
		return servNe7;
	}

	public void setServNe7(String servNe7) {
		this.servNe7 = servNe7;
	}

	@XmlElement(name = "LC_ANEMIA")
	public String getLcAnemia() {
		return lcAnemia;
	}

	public void setLcAnemia(String lcAnemia) {
		this.lcAnemia = lcAnemia;
	}

	@XmlElement(name = "AF_SISEVE")
	public String getAfSiseve() {
		return afSiseve;
	}

	public void setAfSiseve(String afSiseve) {
		this.afSiseve = afSiseve;
	}

	@XmlElement(name = "RPT_SISEVE")
	public String getRptSiseve() {
		return rptSiseve;
	}

	public void setRptSiseve(String rptSiseve) {
		this.rptSiseve = rptSiseve;
	}

	@XmlElement(name = "GRUPO_ETN")
	public String getGrupoEtn() {
		return grupoEtn;
	}

	public void setGrupoEtn(String grupoEtn) {
		this.grupoEtn = grupoEtn;
	}

	@XmlElement(name = "ES_EIB")
	public String getEsEib() {
		return esEib;
	}

	public void setEsEib(String esEib) {
		this.esEib = esEib;
	}

	@XmlElement(name = "EIB_RES")
	public String getEibRes() {
		return eibRes;
	}

	public void setEibRes(String eibRes) {
		this.eibRes = eibRes;
	}

	@XmlElement(name = "CAS_2_LEN")
	public String getCas2Len() {
		return cas2Len;
	}

	public void setCas2Len(String cas2Len) {
		this.cas2Len = cas2Len;
	}

	@XmlElement(name = "HLEN_ORIG")
	public String getHlenOrig() {
		return hlenOrig;
	}

	public void setHlenOrig(String hlenOrig) {
		this.hlenOrig = hlenOrig;
	}

	@XmlElement(name = "COD_LORIG")
	public String getCodLorig() {
		return codLorig;
	}

	public void setCodLorig(String codLorig) {
		this.codLorig = codLorig;
	}

	@XmlElement(name = "EHLEN_ORI")
	public String getEhlenOri() {
		return ehlenOri;
	}

	public void setEhlenOri(String ehlenOri) {
		this.ehlenOri = ehlenOri;
	}

	@XmlElement(name = "SI_LENORIG")
	public String getSiLenorig() {
		return siLenorig;
	}

	public void setSiLenorig(String siLenorig) {
		this.siLenorig = siLenorig;
	}

	@XmlElement(name = "SI_CODLEN")
	public String getSiCodlen() {
		return siCodlen;
	}

	public void setSiCodlen(String siCodlen) {
		this.siCodlen = siCodlen;
	}

	@XmlElement(name = "P401")
	public String getP401() {
		return p401;
	}

	public void setP401(String p401) {
		this.p401 = p401;
	}

	@XmlElement(name = "P403_C3AP")
	public String getP403C3ap() {
		return p403C3ap;
	}

	public void setP403C3ap(String p403C3ap) {
		this.p403C3ap = p403C3ap;
	}

	@XmlElement(name = "RUTA_SBI")
	public String getRutaSbi() {
		return rutaSbi;
	}

	public void setRutaSbi(String rutaSbi) {
		this.rutaSbi = rutaSbi;
	}

	@XmlElement(name = "RUTA_SBI_2")
	public String getRutaSbi2() {
		return rutaSbi2;
	}

	public void setRutaSbi2(String rutaSbi2) {
		this.rutaSbi2 = rutaSbi2;
	}

	@XmlElement(name = "RUTA_SLA")
	public String getRutaSla() {
		return rutaSla;
	}

	public void setRutaSla(String rutaSla) {
		this.rutaSla = rutaSla;
	}

	@XmlElement(name = "RUTA_SLA_1")
	public Integer getRutaSla1() {
		return rutaSla1;
	}

	public void setRutaSla1(Integer rutaSla1) {
		this.rutaSla1 = rutaSla1;
	}

	@XmlElement(name = "RUTA_SLA_2")
	public String getRutaSla2() {
		return rutaSla2;
	}

	public void setRutaSla2(String rutaSla2) {
		this.rutaSla2 = rutaSla2;
	}

	@XmlElement(name = "P601_QH")
	public Integer getP601Qh() {
		return p601Qh;
	}

	public void setP601Qh(Integer p601Qh) {
		this.p601Qh = p601Qh;
	}

	@XmlElement(name = "P601_QM")
	public Integer getP601Qm() {
		return p601Qm;
	}

	public void setP601Qm(Integer p601Qm) {
		this.p601Qm = p601Qm;
	}

	@XmlElement(name = "P602_PS")
	public String getP602Ps() {
		return p602Ps;
	}

	public void setP602Ps(String p602Ps) {
		this.p602Ps = p602Ps;
	}

	@XmlElement(name = "P601_PS")
	public String getP601Ps() {
		return p601Ps;
	}

	public void setP601Ps(String p601Ps) {
		this.p601Ps = p601Ps;
	}

	@XmlElement(name = "P603_PS")
	public String getP603Ps() {
		return p603Ps;
	}

	public void setP603Ps(String p603Ps) {
		this.p603Ps = p603Ps;
	}

	@XmlElement(name = "P604_PS")
	public String getP604Ps() {
		return p604Ps;
	}

	public void setP604Ps(String p604Ps) {
		this.p604Ps = p604Ps;
	}

	@XmlElement(name = "P605_E_PS")
	public String getP605EPs() {
		return p605EPs;
	}

	public void setP605EPs(String p605EPs) {
		this.p605EPs = p605EPs;
	}

	@XmlElement(name = "P606_PS")
	public String getP606Ps() {
		return p606Ps;
	}

	public void setP606Ps(String p606Ps) {
		this.p606Ps = p606Ps;
	}

	@XmlElement(name = "P607_PS")
	public String getP607Ps() {
		return p607Ps;
	}

	public void setP607Ps(String p607Ps) {
		this.p607Ps = p607Ps;
	}

	@XmlElement(name = "P608_PS")
	public String getP608Ps() {
		return p608Ps;
	}

	public void setP608Ps(String p608Ps) {
		this.p608Ps = p608Ps;
	}

	@XmlElement(name = "P609_PS_1")
	public String getP609Ps1() {
		return p609Ps1;
	}

	public void setP609Ps1(String p609Ps1) {
		this.p609Ps1 = p609Ps1;
	}

	@XmlElement(name = "P609_PS_2")
	public String getP609Ps2() {
		return p609Ps2;
	}

	public void setP609Ps2(String p609Ps2) {
		this.p609Ps2 = p609Ps2;
	}

	@XmlElement(name = "P609_PS_3")
	public String getP609Ps3() {
		return p609Ps3;
	}

	public void setP609Ps3(String p609Ps3) {
		this.p609Ps3 = p609Ps3;
	}

	@XmlElement(name = "P609_PS_4")
	public String getP609Ps4() {
		return p609Ps4;
	}

	public void setP609Ps4(String p609Ps4) {
		this.p609Ps4 = p609Ps4;
	}

	@XmlElement(name = "P609_PS_5")
	public String getP609Ps5() {
		return p609Ps5;
	}

	public void setP609Ps5(String p609Ps5) {
		this.p609Ps5 = p609Ps5;
	}

	@XmlElement(name = "P609_PS_6")
	public String getP609Ps6() {
		return p609Ps6;
	}

	public void setP609Ps6(String p609Ps6) {
		this.p609Ps6 = p609Ps6;
	}

	@XmlElement(name = "P609_PS_7")
	public String getP609Ps7() {
		return p609Ps7;
	}

	public void setP609Ps7(String p609Ps7) {
		this.p609Ps7 = p609Ps7;
	}

	@XmlElement(name = "P609_PS_7E")
	public String getP609Ps7e() {
		return p609Ps7e;
	}

	public void setP609Ps7e(String p609Ps7e) {
		this.p609Ps7e = p609Ps7e;
	}

	@XmlElement(name = "P102_C4")
	public String getP102C4() {
		return p102C4;
	}

	public void setP102C4(String p102C4) {
		this.p102C4 = p102C4;
	}

	@XmlElement(name = "P103_C4")
	public Integer getP103C4() {
		return p103C4;
	}

	public void setP103C4(Integer p103C4) {
		this.p103C4 = p103C4;
	}

	@XmlElement(name = "P104_C4_LU")
	public Integer getP104C4Lu() {
		return p104C4Lu;
	}

	public void setP104C4Lu(Integer p104C4Lu) {
		this.p104C4Lu = p104C4Lu;
	}

	@XmlElement(name = "P104_C4_MA")
	public Integer getP104C4Ma() {
		return p104C4Ma;
	}

	public void setP104C4Ma(Integer p104C4Ma) {
		this.p104C4Ma = p104C4Ma;
	}

	@XmlElement(name = "P104_C4_MI")
	public Integer getP104C4Mi() {
		return p104C4Mi;
	}

	public void setP104C4Mi(Integer p104C4Mi) {
		this.p104C4Mi = p104C4Mi;
	}

	@XmlElement(name = "P104_C4_JU")
	public Integer getP104C4Ju() {
		return p104C4Ju;
	}

	public void setP104C4Ju(Integer p104C4Ju) {
		this.p104C4Ju = p104C4Ju;
	}

	@XmlElement(name = "P104_C4_VI")
	public Integer getP104C4Vi() {
		return p104C4Vi;
	}

	public void setP104C4Vi(Integer p104C4Vi) {
		this.p104C4Vi = p104C4Vi;
	}

	@XmlElement(name = "P104_C4_SA")
	public Integer getP104C4Sa() {
		return p104C4Sa;
	}

	public void setP104C4Sa(Integer p104C4Sa) {
		this.p104C4Sa = p104C4Sa;
	}

	@XmlElement(name = "P104_C4_DO")
	public Integer getP104C4Do() {
		return p104C4Do;
	}

	public void setP104C4Do(Integer p104C4Do) {
		this.p104C4Do = p104C4Do;
	}

	@XmlElement(name = "P105_C4A_1")
	public String getP105C4a1() {
		return p105C4a1;
	}

	public void setP105C4a1(String p105C4a1) {
		this.p105C4a1 = p105C4a1;
	}

	@XmlElement(name = "P105_C4A_2")
	public String getP105C4a2() {
		return p105C4a2;
	}

	public void setP105C4a2(String p105C4a2) {
		this.p105C4a2 = p105C4a2;
	}

	@XmlElement(name = "P105_C4A_3")
	public String getP105C4a3() {
		return p105C4a3;
	}

	public void setP105C4a3(String p105C4a3) {
		this.p105C4a3 = p105C4a3;
	}

	@XmlElement(name = "P105_C4A_4")
	public String getP105C4a4() {
		return p105C4a4;
	}

	public void setP105C4a4(String p105C4a4) {
		this.p105C4a4 = p105C4a4;
	}

	@XmlElement(name = "P105_C4A_5")
	public String getP105C4a5() {
		return p105C4a5;
	}

	public void setP105C4a5(String p105C4a5) {
		this.p105C4a5 = p105C4a5;
	}

	@XmlElement(name = "P105_C4A_5E")
	public String getP105C4a5e() {
		return p105C4a5e;
	}

	public void setP105C4a5e(String p105C4a5e) {
		this.p105C4a5e = p105C4a5e;
	}

	@XmlElement(name = "P105_C4I")
	public String getP105C4i() {
		return p105C4i;
	}

	public void setP105C4i(String p105C4i) {
		this.p105C4i = p105C4i;
	}

	@XmlElement(name = "P106_C4I")
	public Integer getP106C4i() {
		return p106C4i;
	}

	public void setP106C4i(Integer p106C4i) {
		this.p106C4i = p106C4i;
	}

	@XmlElement(name = "P107_C4_LU")
	public Integer getP107C4Lu() {
		return p107C4Lu;
	}

	public void setP107C4Lu(Integer p107C4Lu) {
		this.p107C4Lu = p107C4Lu;
	}

	@XmlElement(name = "P107_C4_MA")
	public Integer getP107C4Ma() {
		return p107C4Ma;
	}

	public void setP107C4Ma(Integer p107C4Ma) {
		this.p107C4Ma = p107C4Ma;
	}

	@XmlElement(name = "P107_C4_MI")
	public Integer getP107C4Mi() {
		return p107C4Mi;
	}

	public void setP107C4Mi(Integer p107C4Mi) {
		this.p107C4Mi = p107C4Mi;
	}

	@XmlElement(name = "P107_C4_JU")
	public Integer getP107C4Ju() {
		return p107C4Ju;
	}

	public void setP107C4Ju(Integer p107C4Ju) {
		this.p107C4Ju = p107C4Ju;
	}

	@XmlElement(name = "P107_C4_VI")
	public Integer getP107C4Vi() {
		return p107C4Vi;
	}

	public void setP107C4Vi(Integer p107C4Vi) {
		this.p107C4Vi = p107C4Vi;
	}

	@XmlElement(name = "P107_C4_SA")
	public Integer getP107C4Sa() {
		return p107C4Sa;
	}

	public void setP107C4Sa(Integer p107C4Sa) {
		this.p107C4Sa = p107C4Sa;
	}

	@XmlElement(name = "P107_C4_DO")
	public Integer getP107C4Do() {
		return p107C4Do;
	}

	public void setP107C4Do(Integer p107C4Do) {
		this.p107C4Do = p107C4Do;
	}

	@XmlElement(name = "PCONV_C4_1")
	public String getPconvC41() {
		return pconvC41;
	}

	public void setPconvC41(String pconvC41) {
		this.pconvC41 = pconvC41;
	}

	@XmlElement(name = "PCONV_C4_2")
	public String getPconvC42() {
		return pconvC42;
	}

	public void setPconvC42(String pconvC42) {
		this.pconvC42 = pconvC42;
	}

	@XmlElement(name = "PCONV_C4_3")
	public String getPconvC43() {
		return pconvC43;
	}

	public void setPconvC43(String pconvC43) {
		this.pconvC43 = pconvC43;
	}

	@XmlElement(name = "PCONV_C4_4")
	public String getPconvC44() {
		return pconvC44;
	}

	public void setPconvC44(String pconvC44) {
		this.pconvC44 = pconvC44;
	}

	@XmlElement(name = "PCONV_C4_5")
	public String getPconvC45() {
		return pconvC45;
	}

	public void setPconvC45(String pconvC45) {
		this.pconvC45 = pconvC45;
	}

	@XmlElement(name = "PCONV_C4_6")
	public String getPconvC46() {
		return pconvC46;
	}

	public void setPconvC46(String pconvC46) {
		this.pconvC46 = pconvC46;
	}

	@XmlElement(name = "PCONV_C4_7")
	public String getPconvC47() {
		return pconvC47;
	}

	public void setPconvC47(String pconvC47) {
		this.pconvC47 = pconvC47;
	}

	@XmlElement(name = "PCONV_C4_8")
	public String getPconvC48() {
		return pconvC48;
	}

	public void setPconvC48(String pconvC48) {
		this.pconvC48 = pconvC48;
	}

	@XmlElement(name = "PCONV_C4_E")
	public String getPconvC4E() {
		return pconvC4E;
	}

	public void setPconvC4E(String pconvC4E) {
		this.pconvC4E = pconvC4E;
	}

	@XmlElement(name = "PCOPAE_C4")
	public String getPcopaeC4() {
		return pcopaeC4;
	}

	public void setPcopaeC4(String pcopaeC4) {
		this.pcopaeC4 = pcopaeC4;
	}

	@XmlElement(name = "PCOPAE_RD")
	public String getPcopaeRd() {
		return pcopaeRd;
	}

	public void setPcopaeRd(String pcopaeRd) {
		this.pcopaeRd = pcopaeRd;
	}

	@XmlElement(name = "PCONEI_C4")
	public String getPconeiC4() {
		return pconeiC4;
	}

	public void setPconeiC4(String pconeiC4) {
		this.pconeiC4 = pconeiC4;
	}

	@XmlElement(name = "PCONEI_RD")
	public String getPconeiRd() {
		return pconeiRd;
	}

	public void setPconeiRd(String pconeiRd) {
		this.pconeiRd = pconeiRd;
	}

	@XmlElement(name = "P111_109_1")
	public String getP1111091() {
		return p1111091;
	}

	public void setP1111091(String p1111091) {
		this.p1111091 = p1111091;
	}

	@XmlElement(name = "P111_109_2")
	public String getP1111092() {
		return p1111092;
	}

	public void setP1111092(String p1111092) {
		this.p1111092 = p1111092;
	}

	@XmlElement(name = "P111_109_3")
	public String getP1111093() {
		return p1111093;
	}

	public void setP1111093(String p1111093) {
		this.p1111093 = p1111093;
	}

	@XmlElement(name = "P111_109_4")
	public String getP1111094() {
		return p1111094;
	}

	public void setP1111094(String p1111094) {
		this.p1111094 = p1111094;
	}

	@XmlElement(name = "P111_109E")
	public String getP111109e() {
		return p111109e;
	}

	public void setP111109e(String p111109e) {
		this.p111109e = p111109e;
	}

	@XmlElement(name = "P112_110_1")
	public String getP1121101() {
		return p1121101;
	}

	public void setP1121101(String p1121101) {
		this.p1121101 = p1121101;
	}

	@XmlElement(name = "P112_110_2")
	public String getP1121102() {
		return p1121102;
	}

	public void setP1121102(String p1121102) {
		this.p1121102 = p1121102;
	}

	@XmlElement(name = "P112_110_3")
	public String getP1121103() {
		return p1121103;
	}

	public void setP1121103(String p1121103) {
		this.p1121103 = p1121103;
	}

	@XmlElement(name = "P112_110_4")
	public String getP1121104() {
		return p1121104;
	}

	public void setP1121104(String p1121104) {
		this.p1121104 = p1121104;
	}

	@XmlElement(name = "P112_110_E")
	public String getP112110E() {
		return p112110E;
	}

	public void setP112110E(String p112110E) {
		this.p112110E = p112110E;
	}

	@XmlElement(name = "P113_111_R")
	public String getP113111R() {
		return p113111R;
	}

	public void setP113111R(String p113111R) {
		this.p113111R = p113111R;
	}

	@XmlElement(name = "P113_111_E")
	public String getP113111E() {
		return p113111E;
	}

	public void setP113111E(String p113111E) {
		this.p113111E = p113111E;
	}

	@XmlElement(name = "P114_112_R")
	public String getP114112R() {
		return p114112R;
	}

	public void setP114112R(String p114112R) {
		this.p114112R = p114112R;
	}

	@XmlElement(name = "P114_112_E")
	public String getP114112E() {
		return p114112E;
	}

	public void setP114112E(String p114112E) {
		this.p114112E = p114112E;
	}

	@XmlElement(name = "P403_C4_1")
	public String getP403C41() {
		return p403C41;
	}

	public void setP403C41(String p403C41) {
		this.p403C41 = p403C41;
	}

	@XmlElement(name = "P403_C4_2")
	public String getP403C42() {
		return p403C42;
	}

	public void setP403C42(String p403C42) {
		this.p403C42 = p403C42;
	}

	@XmlElement(name = "P403_C4_3")
	public String getP403C43() {
		return p403C43;
	}

	public void setP403C43(String p403C43) {
		this.p403C43 = p403C43;
	}

	@XmlElement(name = "P403_C4_4")
	public String getP403C44() {
		return p403C44;
	}

	public void setP403C44(String p403C44) {
		this.p403C44 = p403C44;
	}

	@XmlElement(name = "P403_C4_5")
	public String getP403C45() {
		return p403C45;
	}

	public void setP403C45(String p403C45) {
		this.p403C45 = p403C45;
	}

	@XmlElement(name = "P403_C4_6")
	public String getP403C46() {
		return p403C46;
	}

	public void setP403C46(String p403C46) {
		this.p403C46 = p403C46;
	}

	@XmlElement(name = "P403_C4_7")
	public String getP403C47() {
		return p403C47;
	}

	public void setP403C47(String p403C47) {
		this.p403C47 = p403C47;
	}

	@XmlElement(name = "P403_C4_8")
	public String getP403C48() {
		return p403C48;
	}

	public void setP403C48(String p403C48) {
		this.p403C48 = p403C48;
	}

	@XmlElement(name = "P403_C4_9")
	public String getP403C49() {
		return p403C49;
	}

	public void setP403C49(String p403C49) {
		this.p403C49 = p403C49;
	}

	@XmlElement(name = "P403_C4_9E")
	public String getP403C49e() {
		return p403C49e;
	}

	public void setP403C49e(String p403C49e) {
		this.p403C49e = p403C49e;
	}

	@XmlElement(name = "P403_C4_10")
	public String getP403C410() {
		return p403C410;
	}

	public void setP403C410(String p403C410) {
		this.p403C410 = p403C410;
	}

	@XmlElement(name = "P405_C4_1")
	public Integer getP405C41() {
		return p405C41;
	}

	public void setP405C41(Integer p405C41) {
		this.p405C41 = p405C41;
	}

	@XmlElement(name = "P405_C4_2")
	public Integer getP405C42() {
		return p405C42;
	}

	public void setP405C42(Integer p405C42) {
		this.p405C42 = p405C42;
	}

	@XmlElement(name = "P405_C4_3")
	public Integer getP405C43() {
		return p405C43;
	}

	public void setP405C43(Integer p405C43) {
		this.p405C43 = p405C43;
	}

	@XmlElement(name = "TDOCENTES")
	public Integer getTdocentes() {
		return tdocentes;
	}

	public void setTdocentes(Integer tdocentes) {
		this.tdocentes = tdocentes;
	}

	@XmlElement(name = "TDOCAULA")
	public Integer getTdocaula() {
		return tdocaula;
	}

	public void setTdocaula(Integer tdocaula) {
		this.tdocaula = tdocaula;
	}

	@XmlElement(name = "TAUXILIAR")
	public Integer getTauxiliar() {
		return tauxiliar;
	}

	public void setTauxiliar(Integer tauxiliar) {
		this.tauxiliar = tauxiliar;
	}

	@XmlElement(name = "TADMINIST")
	public Integer getTadminist() {
		return tadminist;
	}

	public void setTadminist(Integer tadminist) {
		this.tadminist = tadminist;
	}

	@XmlElement(name = "ANOTACIONES")
	public String getAnotaciones() {
		return anotaciones;
	}

	public void setAnotaciones(String anotaciones) {
		this.anotaciones = anotaciones;
	}

	@XmlElement(name = "FUENTE")
	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	@XmlElement(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	@XmlElement(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name = "TIPO_ENVIO")
	public String getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(String tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	@XmlElement(name = "G_ETNICO_1")
	public String getgEtnico1() {
		return gEtnico1;
	}

	public void setgEtnico1(String gEtnico1) {
		this.gEtnico1 = gEtnico1;
	}

	@XmlElement(name = "G_ETNICO_2")
	public String getgEtnico2() {
		return gEtnico2;
	}

	public void setgEtnico2(String gEtnico2) {
		this.gEtnico2 = gEtnico2;
	}

	@XmlElement(name = "G_ETNICO_3")
	public String getgEtnico3() {
		return gEtnico3;
	}

	public void setgEtnico3(String gEtnico3) {
		this.gEtnico3 = gEtnico3;
	}

	@XmlElement(name = "G_ETNICO_4")
	public String getgEtnico4() {
		return gEtnico4;
	}

	public void setgEtnico4(String gEtnico4) {
		this.gEtnico4 = gEtnico4;
	}

	@XmlElement(name = "G_ETNICO_5")
	public String getgEtnico5() {
		return gEtnico5;
	}

	public void setgEtnico5(String gEtnico5) {
		this.gEtnico5 = gEtnico5;
	}

	@XmlElement(name = "G_ETNICO_6")
	public String getgEtnico6() {
		return gEtnico6;
	}

	public void setgEtnico6(String gEtnico6) {
		this.gEtnico6 = gEtnico6;
	}

	@XmlElement(name = "G_ETNICO_7")
	public String getgEtnico7() {
		return gEtnico7;
	}

	public void setgEtnico7(String gEtnico7) {
		this.gEtnico7 = gEtnico7;
	}

	@XmlElement(name = "G_ETNICO_8")
	public String getgEtnico8() {
		return gEtnico8;
	}

	public void setgEtnico8(String gEtnico8) {
		this.gEtnico8 = gEtnico8;
	}

	@XmlElement(name = "G_ETNICO_9")
	public String getgEtnico9() {
		return gEtnico9;
	}

	public void setgEtnico9(String gEtnico9) {
		this.gEtnico9 = gEtnico9;
	}

	@XmlElement(name = "G_ETNICO_E")
	public String getgEtnicoE() {
		return gEtnicoE;
	}

	public void setgEtnicoE(String gEtnicoE) {
		this.gEtnicoE = gEtnicoE;
	}

	@XmlElement(name = "EIB_EST")
	public String getEibEst() {
		return eibEst;
	}

	public void setEibEst(String eibEst) {
		this.eibEst = eibEst;
	}

	@XmlElement(name = "P131_3AS_1")
	public String getP1313as1() {
		return p1313as1;
	}

	public void setP1313as1(String p1313as1) {
		this.p1313as1 = p1313as1;
	}

	@XmlElement(name = "P131_3AS_2")
	public String getP1313as2() {
		return p1313as2;
	}

	public void setP1313as2(String p1313as2) {
		this.p1313as2 = p1313as2;
	}

	@XmlElement(name = "P131_3AS_3")
	public String getP1313as3() {
		return p1313as3;
	}

	public void setP1313as3(String p1313as3) {
		this.p1313as3 = p1313as3;
	}

	@XmlElement(name = "P131_3AS_4")
	public String getP1313as4() {
		return p1313as4;
	}

	public void setP1313as4(String p1313as4) {
		this.p1313as4 = p1313as4;
	}

	@XmlElement(name = "P131_3AS_5")
	public String getP1313as5() {
		return p1313as5;
	}

	public void setP1313as5(String p1313as5) {
		this.p1313as5 = p1313as5;
	}

	@XmlElement(name = "P131_3AS_6")
	public String getP1313as6() {
		return p1313as6;
	}

	public void setP1313as6(String p1313as6) {
		this.p1313as6 = p1313as6;
	}

	@XmlElement(name = "P131_3AS_7")
	public String getP1313as7() {
		return p1313as7;
	}

	public void setP1313as7(String p1313as7) {
		this.p1313as7 = p1313as7;
	}

	@XmlElement(name = "P131_3AS_8")
	public String getP1313as8() {
		return p1313as8;
	}

	public void setP1313as8(String p1313as8) {
		this.p1313as8 = p1313as8;
	}

	@XmlElement(name = "P131_3AS_E")
	public String getP1313asE() {
		return p1313asE;
	}

	public void setP1313asE(String p1313asE) {
		this.p1313asE = p1313asE;
	}

	@XmlElement(name = "P605_D_PS")
	public String getP605DPs() {
		return p605DPs;
	}

	public void setP605DPs(String p605DPs) {
		this.p605DPs = p605DPs;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@XmlElement(name = "FECRES_CD")
	public String getFecresCd() {
		return fecresCd;
	}

	public void setFecresCd(String fecresCd) {
		this.fecresCd = fecresCd;
	}

	@XmlElement(name = "FECRES_CM")
	public String getFecresCm() {
		return fecresCm;
	}

	public void setFecresCm(String fecresCm) {
		this.fecresCm = fecresCm;
	}

	@XmlElement(name = "FECRES_CA")
	public String getFecresCa() {
		return fecresCa;
	}

	public void setFecresCa(String fecresCa) {
		this.fecresCa = fecresCa;
	}

	@XmlElement(name = "FECRES_RD")
	public String getFecresRd() {
		return fecresRd;
	}

	public void setFecresRd(String fecresRd) {
		this.fecresRd = fecresRd;
	}

	@XmlElement(name = "FECRES_RM")
	public String getFecresRm() {
		return fecresRm;
	}

	public void setFecresRm(String fecresRm) {
		this.fecresRm = fecresRm;
	}

	@XmlElement(name = "FECRES_RA")
	public String getFecresRa() {
		return fecresRa;
	}

	public void setFecresRa(String fecresRa) {
		this.fecresRa = fecresRa;
	}

	@XmlElement(name = "METATEN")
	public String getMetaten() {
		return metaten;
	}

	public void setMetaten(String metaten) {
		this.metaten = metaten;
	}

	@XmlElement(name = "MSE_EBR")
	public String getMseEbr() {
		return mseEbr;
	}

	public void setMseEbr(String mseEbr) {
		this.mseEbr = mseEbr;
	}

	@XmlElement(name = "MSE_SRE")
	public String getMseSre() {
		return mseSre;
	}

	public void setMseSre(String mseSre) {
		this.mseSre = mseSre;
	}

	@XmlElement(name = "MSE_ST")
	public String getMseSt() {
		return mseSt;
	}

	public void setMseSt(String mseSt) {
		this.mseSt = mseSt;
	}

	@XmlElement(name = "ALUM_35IIC")
	public String getAlum35iic() {
		return alum35iic;
	}

	public void setAlum35iic(String alum35iic) {
		this.alum35iic = alum35iic;
	}

	@XmlElement(name = "QN_EST_C6A")
	public Integer getQnEstC6a() {
		return qnEstC6a;
	}

	public void setQnEstC6a(Integer qnEstC6a) {
		this.qnEstC6a = qnEstC6a;
	}

	@XmlElement(name = "P1011_C6A")
	public String getP1011C6a() {
		return p1011C6a;
	}

	public void setP1011C6a(String p1011c6a) {
		p1011C6a = p1011c6a;
	}

	@XmlElement(name = "P1011_C6A1")
	public String getP1011C6a1() {
		return p1011C6a1;
	}

	public void setP1011C6a1(String p1011c6a1) {
		p1011C6a1 = p1011c6a1;
	}

	@XmlElement(name = "P1011_C6A2")
	public String getP1011C6a2() {
		return p1011C6a2;
	}

	public void setP1011C6a2(String p1011c6a2) {
		p1011C6a2 = p1011c6a2;
	}

	@XmlElement(name = "P1011_C6A3")
	public String getP1011C6a3() {
		return p1011C6a3;
	}

	public void setP1011C6a3(String p1011c6a3) {
		p1011C6a3 = p1011c6a3;
	}

	@XmlElement(name = "P1012_C6A1")
	public String getP1012C6a1() {
		return p1012C6a1;
	}

	public void setP1012C6a1(String p1012c6a1) {
		p1012C6a1 = p1012c6a1;
	}

	@XmlElement(name = "P1012_C6A2")
	public String getP1012C6a2() {
		return p1012C6a2;
	}

	public void setP1012C6a2(String p1012c6a2) {
		p1012C6a2 = p1012c6a2;
	}

	@XmlElement(name = "P1012_C6A3")
	public String getP1012C6a3() {
		return p1012C6a3;
	}

	public void setP1012C6a3(String p1012c6a3) {
		p1012C6a3 = p1012c6a3;
	}

	@XmlElement(name = "P1021_C7A")
	public Integer getP1021C7a() {
		return p1021C7a;
	}

	public void setP1021C7a(Integer p1021c7a) {
		p1021C7a = p1021c7a;
	}

	@XmlElement(name = "P1022_C7A")
	public String getP1022C7a() {
		return p1022C7a;
	}

	public void setP1022C7a(String p1022c7a) {
		p1022C7a = p1022c7a;
	}

	@XmlElement(name = "P103_C7A_1")
	public String getP103C7a1() {
		return p103C7a1;
	}

	public void setP103C7a1(String p103c7a1) {
		p103C7a1 = p103c7a1;
	}

	@XmlElement(name = "P103_C7A_2")
	public String getP103C7a2() {
		return p103C7a2;
	}

	public void setP103C7a2(String p103c7a2) {
		p103C7a2 = p103c7a2;
	}

	@XmlElement(name = "P103_C7A_3")
	public String getP103C7a3() {
		return p103C7a3;
	}

	public void setP103C7a3(String p103c7a3) {
		p103C7a3 = p103c7a3;
	}

	@XmlElement(name = "P103_C7A_4")
	public String getP103C7a4() {
		return p103C7a4;
	}

	public void setP103C7a4(String p103c7a4) {
		p103C7a4 = p103c7a4;
	}

	@XmlElement(name = "P103_C7A_5")
	public String getP103C7a5() {
		return p103C7a5;
	}

	public void setP103C7a5(String p103c7a5) {
		p103C7a5 = p103c7a5;
	}

	@XmlElement(name = "P103_C7A_6")
	public String getP103C7a6() {
		return p103C7a6;
	}

	public void setP103C7a6(String p103c7a6) {
		p103C7a6 = p103c7a6;
	}

	@XmlElement(name = "P103_C7A_E")
	public String getP103C7aE() {
		return p103C7aE;
	}

	public void setP103C7aE(String p103c7aE) {
		p103C7aE = p103c7aE;
	}

	@XmlElement(name = "PAT_2019")
	public String getPat2019() {
		return pat2019;
	}

	public void setPat2019(String pat2019) {
		this.pat2019 = pat2019;
	}

	@XmlElement(name = "P105_C3PS")
	public String getP105C3ps() {
		return p105C3ps;
	}

	public void setP105C3ps(String p105c3ps) {
		p105C3ps = p105c3ps;
	}

	@XmlElement(name = "P106_C3PS")
	public String getP106C3ps() {
		return p106C3ps;
	}

	public void setP106C3ps(String p106c3ps) {
		p106C3ps = p106c3ps;
	}

	@XmlElement(name = "P107_C3PS")
	public String getP107C3ps() {
		return p107C3ps;
	}

	public void setP107C3ps(String p107c3ps) {
		p107C3ps = p107c3ps;
	}

	@XmlElement(name = "P108_C3PS")
	public String getP108C3ps() {
		return p108C3ps;
	}

	public void setP108C3ps(String p108c3ps) {
		p108C3ps = p108c3ps;
	}

	@XmlElement(name = "P109_C3_01")
	public String getP109C301() {
		return p109C301;
	}

	public void setP109C301(String p109c301) {
		p109C301 = p109c301;
	}

	@XmlElement(name = "P109_C3_02")
	public String getP109C302() {
		return p109C302;
	}

	public void setP109C302(String p109c302) {
		p109C302 = p109c302;
	}

	@XmlElement(name = "P109_C3_03")
	public String getP109C303() {
		return p109C303;
	}

	public void setP109C303(String p109c303) {
		p109C303 = p109c303;
	}

	@XmlElement(name = "P109_C3_04")
	public String getP109C304() {
		return p109C304;
	}

	public void setP109C304(String p109c304) {
		p109C304 = p109c304;
	}

	@XmlElement(name = "P109_C3_05")
	public String getP109C305() {
		return p109C305;
	}

	public void setP109C305(String p109c305) {
		p109C305 = p109c305;
	}

	@XmlElement(name = "P109_C3_06")
	public String getP109C306() {
		return p109C306;
	}

	public void setP109C306(String p109c306) {
		p109C306 = p109c306;
	}

	@XmlElement(name = "P109_C3_07")
	public String getP109C307() {
		return p109C307;
	}

	public void setP109C307(String p109c307) {
		p109C307 = p109c307;
	}

	@XmlElement(name = "P109_C3_08")
	public String getP109C308() {
		return p109C308;
	}

	public void setP109C308(String p109c308) {
		p109C308 = p109c308;
	}

	@XmlElement(name = "P109_C3_09")
	public String getP109C309() {
		return p109C309;
	}

	public void setP109C309(String p109c309) {
		p109C309 = p109c309;
	}

	@XmlElement(name = "P109_C3_10")
	public String getP109C310() {
		return p109C310;
	}

	public void setP109C310(String p109c310) {
		p109C310 = p109c310;
	}

	@XmlElement(name = "P109_C3_11")
	public String getP109C311() {
		return p109C311;
	}

	public void setP109C311(String p109c311) {
		p109C311 = p109c311;
	}

	@XmlElement(name = "P109_C3_12")
	public String getP109C312() {
		return p109C312;
	}

	public void setP109C312(String p109c312) {
		p109C312 = p109c312;
	}

	@XmlElement(name = "P109_C3_13")
	public String getP109C313() {
		return p109C313;
	}

	public void setP109C313(String p109c313) {
		p109C313 = p109c313;
	}

	@XmlElement(name = "P109_C3_14")
	public String getP109C314() {
		return p109C314;
	}

	public void setP109C314(String p109c314) {
		p109C314 = p109c314;
	}

	@XmlElement(name = "P109_C314E")
	public String getP109C314e() {
		return p109C314e;
	}

	public void setP109C314e(String p109c314e) {
		p109C314e = p109c314e;
	}

	@XmlElement(name = "LIBRO_IN_Q")
	public Integer getLibroInQ() {
		return libroInQ;
	}

	public void setLibroInQ(Integer libroInQ) {
		this.libroInQ = libroInQ;
	}

	@XmlElement(name = "SISEVE_QN")
	public Integer getSiseveQn() {
		return siseveQn;
	}

	public void setSiseveQn(Integer siseveQn) {
		this.siseveQn = siseveQn;
	}

	@XmlElement(name = "NO_SISEVE")
	public String getNoSiseve() {
		return noSiseve;
	}

	public void setNoSiseve(String noSiseve) {
		this.noSiseve = noSiseve;
	}

	@XmlElement(name = "SISEVE_ESP")
	public String getSiseveEsp() {
		return siseveEsp;
	}

	public void setSiseveEsp(String siseveEsp) {
		this.siseveEsp = siseveEsp;
	}

	@XmlElement(name = "VACAN_MAT")
	public String getVacanMat() {
		return vacanMat;
	}

	public void setVacanMat(String vacanMat) {
		this.vacanMat = vacanMat;
	}

	@XmlElement(name = "VAC_MAT_NO")
	public String getVacMatNo() {
		return vacMatNo;
	}

	public void setVacMatNo(String vacMatNo) {
		this.vacMatNo = vacMatNo;
	}

	@XmlElement(name = "GRP_ETN_C")
	public Integer getGrpEtnC() {
		return grpEtnC;
	}

	public void setGrpEtnC(Integer grpEtnC) {
		this.grpEtnC = grpEtnC;
	}

	@XmlElement(name = "ORIG_2_LEN")
	public String getOrig2Len() {
		return orig2Len;
	}

	public void setOrig2Len(String orig2Len) {
		this.orig2Len = orig2Len;
	}

	@XmlElement(name = "P130_C3AS")
	public String getP130C3as() {
		return p130C3as;
	}

	public void setP130C3as(String p130c3as) {
		p130C3as = p130c3as;
	}

	@XmlElement(name = "P131_C3AS")
	public String getP131C3as() {
		return p131C3as;
	}

	public void setP131C3as(String p131c3as) {
		p131C3as = p131c3as;
	}

	@XmlElement(name = "P333")
	public String getP333() {
		return p333;
	}

	public void setP333(String p333) {
		this.p333 = p333;
	}

	@XmlElement(name = "P333_QN")
	public Integer getP333Qn() {
		return p333Qn;
	}

	public void setP333Qn(Integer p333Qn) {
		this.p333Qn = p333Qn;
	}

	@XmlElement(name = "P402_C8")
	public String getP402C8() {
		return p402C8;
	}

	public void setP402C8(String p402c8) {
		p402C8 = p402c8;
	}

	@XmlElement(name = "P402_C8_Q")
	public Integer getP402C8Q() {
		return p402C8Q;
	}

	public void setP402C8Q(Integer p402c8q) {
		p402C8Q = p402c8q;
	}

	@XmlElement(name = "P405_C3AP")
	public String getP405C3ap() {
		return p405C3ap;
	}

	public void setP405C3ap(String p405c3ap) {
		p405C3ap = p405c3ap;
	}

	@XmlElement(name = "P406_C3AP")
	public String getP406C3ap() {
		return p406C3ap;
	}

	public void setP406C3ap(String p406c3ap) {
		p406C3ap = p406c3ap;
	}

	@XmlElement(name = "KIT_CAS4")
	public Integer getKitCas4() {
		return kitCas4;
	}

	public void setKitCas4(Integer kitCas4) {
		this.kitCas4 = kitCas4;
	}

	@XmlElement(name = "KIT_CAS5")
	public Integer getKitCas5() {
		return kitCas5;
	}

	public void setKitCas5(Integer kitCas5) {
		this.kitCas5 = kitCas5;
	}

	@XmlElement(name = "KIT_ORI4")
	public Integer getKitOri4() {
		return kitOri4;
	}

	public void setKitOri4(Integer kitOri4) {
		this.kitOri4 = kitOri4;
	}

	@XmlElement(name = "KIT_ORI5")
	public Integer getKitOri5() {
		return kitOri5;
	}

	public void setKitOri5(Integer kitOri5) {
		this.kitOri5 = kitOri5;
	}

	@XmlElement(name = "P403")
	public String getP403() {
		return p403;
	}

	public void setP403(String p403) {
		this.p403 = p403;
	}

	@XmlElement(name = "P404")
	public String getP404() {
		return p404;
	}

	public void setP404(String p404) {
		this.p404 = p404;
	}

	@XmlElement(name = "P405")
	public Integer getP405() {
		return p405;
	}

	public void setP405(Integer p405) {
		this.p405 = p405;
	}

	@XmlElement(name = "P406")
	public String getP406() {
		return p406;
	}

	public void setP406(String p406) {
		this.p406 = p406;
	}

	@XmlElement(name = "P407")
	public String getP407() {
		return p407;
	}

	public void setP407(String p407) {
		this.p407 = p407;
	}

	@XmlElement(name = "P407_SI")
	public String getP407Si() {
		return p407Si;
	}

	public void setP407Si(String p407Si) {
		this.p407Si = p407Si;
	}

	@XmlElement(name = "P401_C4_11")
	public String getP401C411() {
		return p401C411;
	}

	public void setP401C411(String p401c411) {
		p401C411 = p401c411;
	}

	@XmlElement(name = "P401_C4_12")
	public Integer getP401C412() {
		return p401C412;
	}

	public void setP401C412(Integer p401c412) {
		p401C412 = p401c412;
	}

	@XmlElement(name = "P401_C4_21")
	public String getP401C421() {
		return p401C421;
	}

	public void setP401C421(String p401c421) {
		p401C421 = p401c421;
	}

	@XmlElement(name = "P401_C4_22")
	public Integer getP401C422() {
		return p401C422;
	}

	public void setP401C422(Integer p401c422) {
		p401C422 = p401c422;
	}

	@XmlElement(name = "P401_C4_31")
	public String getP401C431() {
		return p401C431;
	}

	public void setP401C431(String p401c431) {
		p401C431 = p401c431;
	}

	@XmlElement(name = "P401_C4_32")
	public Integer getP401C432() {
		return p401C432;
	}

	public void setP401C432(Integer p401c432) {
		p401C432 = p401c432;
	}

	@XmlElement(name = "P401_C4_41")
	public String getP401C441() {
		return p401C441;
	}

	public void setP401C441(String p401c441) {
		p401C441 = p401c441;
	}

	@XmlElement(name = "P402_C4_41")
	public String getP402C441() {
		return p402C441;
	}

	public void setP402C441(String p402c441) {
		p402C441 = p402c441;
	}

	@XmlElement(name = "P406_C4_1")
	public Integer getP406C41() {
		return p406C41;
	}

	public void setP406C41(Integer p406c41) {
		p406C41 = p406c41;
	}

	@XmlElement(name = "P406_C4_2")
	public Integer getP406C42() {
		return p406C42;
	}

	public void setP406C42(Integer p406c42) {
		p406C42 = p406c42;
	}

	@XmlElement(name = "P406_C4_3")
	public Integer getP406C43() {
		return p406C43;
	}

	public void setP406C43(Integer p406c43) {
		p406C43 = p406c43;
	}

	@XmlElement(name = "P501_C8")
	public String getP501C8() {
		return p501C8;
	}

	public void setP501C8(String p501c8) {
		p501C8 = p501c8;
	}

	@XmlElement(name = "P502_C8")
	public String getP502C8() {
		return p502C8;
	}

	public void setP502C8(String p502c8) {
		p502C8 = p502c8;
	}

	@XmlElement(name = "P503_C8")
	public Integer getP503C8() {
		return p503C8;
	}

	public void setP503C8(Integer p503c8) {
		p503C8 = p503c8;
	}

	@XmlElement(name = "P504_C8")
	public String getP504C8() {
		return p504C8;
	}

	public void setP504C8(String p504c8) {
		p504C8 = p504c8;
	}

	@XmlElement(name = "P505_C8")
	public String getP505C8() {
		return p505C8;
	}

	public void setP505C8(String p505c8) {
		p505C8 = p505c8;
	}

	@XmlElement(name = "P502")
	public String getP502() {
		return p502;
	}

	public void setP502(String p502) {
		this.p502 = p502;
	}

	@XmlElement(name = "P502_D")
	public String getP502D() {
		return p502D;
	}

	public void setP502D(String p502d) {
		p502D = p502d;
	}

	@XmlElement(name = "P502_M")
	public String getP502M() {
		return p502M;
	}

	public void setP502M(String p502m) {
		p502M = p502m;
	}

	@XmlElement(name = "P502_A")
	public String getP502A() {
		return p502A;
	}

	public void setP502A(String p502a) {
		p502A = p502a;
	}

	@XmlElement(name = "P503")
	public String getP503() {
		return p503;
	}

	public void setP503(String p503) {
		this.p503 = p503;
	}

	@XmlElement(name = "RUTA_SBI_1")
	public Integer getRutaSbi1() {
		return rutaSbi1;
	}

	public void setRutaSbi1(Integer rutaSbi1) {
		this.rutaSbi1 = rutaSbi1;
	}

	@XmlElement(name = "P605_QE_PS")
	public Integer getP605QePs() {
		return p605QePs;
	}

	public void setP605QePs(Integer p605QePs) {
		this.p605QePs = p605QePs;
	}

	@XmlElement(name = "P605_QD_PS")
	public Integer getP605QdPs() {
		return p605QdPs;
	}

	public void setP605QdPs(Integer p605QdPs) {
		this.p605QdPs = p605QdPs;
	}

	@XmlElement(name = "P606_HS_PS")
	public Integer getP606HsPs() {
		return p606HsPs;
	}

	public void setP606HsPs(Integer p606HsPs) {
		this.p606HsPs = p606HsPs;
	}

	@XmlElement(name = "P606_DA_PS")
	public Integer getP606DaPs() {
		return p606DaPs;
	}

	public void setP606DaPs(Integer p606DaPs) {
		this.p606DaPs = p606DaPs;
	}

	@XmlElement(name = "P701_PS")
	public String getP701Ps() {
		return p701Ps;
	}

	public void setP701Ps(String p701Ps) {
		this.p701Ps = p701Ps;
	}

	@XmlElement(name = "P702_PS_1")
	public Integer getP702Ps1() {
		return p702Ps1;
	}

	public void setP702Ps1(Integer p702Ps1) {
		this.p702Ps1 = p702Ps1;
	}

	@XmlElement(name = "P702_PS_2")
	public Integer getP702Ps2() {
		return p702Ps2;
	}

	public void setP702Ps2(Integer p702Ps2) {
		this.p702Ps2 = p702Ps2;
	}

	@XmlElement(name = "P702_PS_3")
	public Integer getP702Ps3() {
		return p702Ps3;
	}

	public void setP702Ps3(Integer p702Ps3) {
		this.p702Ps3 = p702Ps3;
	}

	@XmlElement(name = "P702_PS_4")
	public Integer getP702Ps4() {
		return p702Ps4;
	}

	public void setP702Ps4(Integer p702Ps4) {
		this.p702Ps4 = p702Ps4;
	}

	@XmlElement(name = "P702_PS_5")
	public Integer getP702Ps5() {
		return p702Ps5;
	}

	public void setP702Ps5(Integer p702Ps5) {
		this.p702Ps5 = p702Ps5;
	}

	@XmlElement(name = "P702_PS_6")
	public Integer getP702Ps6() {
		return p702Ps6;
	}

	public void setP702Ps6(Integer p702Ps6) {
		this.p702Ps6 = p702Ps6;
	}

	@XmlElement(name = "P702_PS_7")
	public Integer getP702Ps7() {
		return p702Ps7;
	}

	public void setP702Ps7(Integer p702Ps7) {
		this.p702Ps7 = p702Ps7;
	}

	@XmlElement(name = "P702_PS_8")
	public Integer getP702Ps8() {
		return p702Ps8;
	}

	public void setP702Ps8(Integer p702Ps8) {
		this.p702Ps8 = p702Ps8;
	}

	@XmlElement(name = "P702_PS_8E")
	public String getP702Ps8e() {
		return p702Ps8e;
	}

	public void setP702Ps8e(String p702Ps8e) {
		this.p702Ps8e = p702Ps8e;
	}

	@XmlElement(name = "P801_C3AS")
	public String getP801C3as() {
		return p801C3as;
	}

	public void setP801C3as(String p801c3as) {
		p801C3as = p801c3as;
	}

	@XmlElement(name = "P801_C3_1")
	public String getP801C31() {
		return p801C31;
	}

	public void setP801C31(String p801c31) {
		p801C31 = p801c31;
	}

	@XmlElement(name = "P801_C3_2")
	public String getP801C32() {
		return p801C32;
	}

	public void setP801C32(String p801c32) {
		p801C32 = p801c32;
	}

	@XmlElement(name = "P801_C3_3")
	public String getP801C33() {
		return p801C33;
	}

	public void setP801C33(String p801c33) {
		p801C33 = p801c33;
	}

	@XmlElement(name = "P801_C3_4")
	public String getP801C34() {
		return p801C34;
	}

	public void setP801C34(String p801c34) {
		p801C34 = p801c34;
	}

	@XmlElement(name = "P801_C3_5")
	public String getP801C35() {
		return p801C35;
	}

	public void setP801C35(String p801c35) {
		p801C35 = p801c35;
	}

	@XmlElement(name = "P801_C3_5E")
	public String getP801C35e() {
		return p801C35e;
	}

	public void setP801C35e(String p801c35e) {
		p801C35e = p801c35e;
	}

	@XmlElement(name = "P802_C3AS")
	public String getP802C3as() {
		return p802C3as;
	}

	public void setP802C3as(String p802c3as) {
		p802C3as = p802c3as;
	}

	@XmlElement(name = "P802_C3SI")
	public String getP802C3si() {
		return p802C3si;
	}

	public void setP802C3si(String p802c3si) {
		p802C3si = p802c3si;
	}

	@XmlElement(name = "P803_C3AS")
	public String getP803C3as() {
		return p803C3as;
	}

	public void setP803C3as(String p803c3as) {
		p803C3as = p803c3as;
	}

	@XmlElement(name = "P803_C3SI")
	public String getP803C3si() {
		return p803C3si;
	}

	public void setP803C3si(String p803c3si) {
		p803C3si = p803c3si;
	}

	@XmlElement(name = "P804_C3AS")
	public String getP804C3as() {
		return p804C3as;
	}

	public void setP804C3as(String p804c3as) {
		p804C3as = p804c3as;
	}

	@XmlElement(name = "P804_C3ASE")
	public String getP804C3ase() {
		return p804C3ase;
	}

	public void setP804C3ase(String p804c3ase) {
		p804C3ase = p804c3ase;
	}

	@XmlElement(name = "ULTIMO")
	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	@XmlElement(name = "SECCION")
	public List<Matricula2019Seccion> getDetalleSeccion() {
		return detalleSeccion;
	}

	public void setDetalleSeccion(List<Matricula2019Seccion> detalleSeccion) {
		this.detalleSeccion = detalleSeccion;
	}

	@XmlElement(name = "PERSONAL")
	public List<Matricula2019Personal> getMatricula2019PersonalList() {
		return matricula2019PersonalList;
	}

	public void setMatricula2019PersonalList(List<Matricula2019Personal> matricula2019PersonalList) {
		this.matricula2019PersonalList = matricula2019PersonalList;
	}

	@XmlElement(name = "MATRICULA")
	public List<Matricula2019Matricula> getDetalleMatricula() {
		return detalleMatricula;
	}

	public void setDetalleMatricula(List<Matricula2019Matricula> detalleMatricula) {
		this.detalleMatricula = detalleMatricula;
	}

	@XmlElement(name = "CARRERAS")
	public List<Matricula2019Carreras> getMatricula2019CarrerasList() {
		return matricula2019CarrerasList;
	}

	public void setMatricula2019CarrerasList(List<Matricula2019Carreras> matricula2019CarrerasList) {
		this.matricula2019CarrerasList = matricula2019CarrerasList;
	}

	@XmlElement(name = "PRONOEI")
	public List<Matricula2019Localpronoei> getMatricula2019LocalpronoeiList() {
		return matricula2019LocalpronoeiList;
	}

	public void setMatricula2019LocalpronoeiList(List<Matricula2019Localpronoei> matricula2019LocalpronoeiList) {
		this.matricula2019LocalpronoeiList = matricula2019LocalpronoeiList;
	}

	@XmlElement(name = "SEDE")
	public List<Matricula2019Sede> getMatricula2019SedeList() {
		return matricula2019SedeList;
	}

	public void setMatricula2019SedeList(List<Matricula2019Sede> matricula2019SedeList) {
		this.matricula2019SedeList = matricula2019SedeList;
	}

	@XmlElement(name = "MULTIEDAD")
	public List<Matricula2019Muledad> getMatricula2019MuledadList() {
		return matricula2019MuledadList;
	}

	public void setMatricula2019MuledadList(List<Matricula2019Muledad> matricula2019MuledadList) {
		this.matricula2019MuledadList = matricula2019MuledadList;
	}

	@XmlElement(name = "RESPONSABLE")
	public List<Matricula2019Resp> getMatricula2019RespList() {
		return matricula2019RespList;
	}

	public void setMatricula2019RespList(List<Matricula2019Resp> matricula2019RespList) {
		this.matricula2019RespList = matricula2019RespList;
	}

	@XmlElement(name = "LENGUA")
	public List<Matricula2019Dlen> getMatricula2019DlenList() {
		return matricula2019DlenList;
	}

	public void setMatricula2019DlenList(List<Matricula2019Dlen> matricula2019DlenList) {
		this.matricula2019DlenList = matricula2019DlenList;
	}

	@XmlElement(name = "EBA")
	public List<Matricula2019Eba> getMatricula2019EbaList() {
		return matricula2019EbaList;
	}

	public void setMatricula2019EbaList(List<Matricula2019Eba> matricula2019EbaList) {
		this.matricula2019EbaList = matricula2019EbaList;
	}

	@XmlElement(name = "RECURSOS")
	public List<Matricula2019Recursos> getMatricula2019RecursosList() {
		return matricula2019RecursosList;
	}

	public void setMatricula2019RecursosList(List<Matricula2019Recursos> matricula2019RecursosList) {
		this.matricula2019RecursosList = matricula2019RecursosList;
	}

	@XmlElement(name = "SEC100")
	public List<Matricula2019S100> getMatricula2019S100List() {
		return matricula2019S100List;
	}

	public void setMatricula2019S100List(List<Matricula2019S100> matricula2019s100List) {
		matricula2019S100List = matricula2019s100List;
	}

	@XmlElement(name = "SAANEES")
	public List<Matricula2019Saanee> getMatricula2019SaaneeList() {
		return matricula2019SaaneeList;
	}

	public void setMatricula2019SaaneeList(List<Matricula2019Saanee> matricula2019SaaneeList) {
		this.matricula2019SaaneeList = matricula2019SaaneeList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2019Cabecera)) {
			return false;
		}
		Matricula2019Cabecera other = (Matricula2019Cabecera) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Cabecera[idEnvio=" + idEnvio + "]";
	}

}
