/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "matricula2019_dlen")
public class Matricula2019Dlen implements Serializable {
    private static final long serialVersionUID = 1L;

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "NRO")
    private Integer nro;
    @Column(name = "PERDNI")
    private String perdni;
    @Column(name = "PEBI01")
    private String pebi01;
    @Column(name = "PEBI02")
    private String pebi02;
    @Column(name = "PEBI03")
    private String pebi03;
    @Column(name = "PEBI04")
    private String pebi04;
    @Column(name = "PEBI05")
    private String pebi05;
    @Column(name = "PEBI06")
    private String pebi06;
    @Column(name = "PEBI07")
    private String pebi07;
    @Column(name = "PEBI08")
    private String pebi08;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2019Cabecera matricula2019Cabecera;

    public Matricula2019Dlen() {
    }

    public Matricula2019Dlen(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NRO")
    public Integer getNro() {
        return nro;
    }

    public void setNro(Integer nro) {
        this.nro = nro;
    }

    @XmlElement(name = "PERDNI")
    public String getPerdni() {
        return perdni;
    }

    public void setPerdni(String perdni) {
        this.perdni = perdni;
    }

    @XmlElement(name = "PEBI01")
    public String getPebi01() {
        return pebi01;
    }

    public void setPebi01(String pebi01) {
        this.pebi01 = pebi01;
    }

    @XmlElement(name = "PEBI02")
    public String getPebi02() {
        return pebi02;
    }

    public void setPebi02(String pebi02) {
        this.pebi02 = pebi02;
    }

    @XmlElement(name = "PEBI03")
    public String getPebi03() {
        return pebi03;
    }

    public void setPebi03(String pebi03) {
        this.pebi03 = pebi03;
    }

    @XmlElement(name = "PEBI04")
    public String getPebi04() {
        return pebi04;
    }

    public void setPebi04(String pebi04) {
        this.pebi04 = pebi04;
    }

    @XmlElement(name = "PEBI05")
    public String getPebi05() {
        return pebi05;
    }

    public void setPebi05(String pebi05) {
        this.pebi05 = pebi05;
    }

    @XmlElement(name = "PEBI06")
    public String getPebi06() {
        return pebi06;
    }

    public void setPebi06(String pebi06) {
        this.pebi06 = pebi06;
    }

    @XmlElement(name = "PEBI07")
    public String getPebi07() {
        return pebi07;
    }

    public void setPebi07(String pebi07) {
        this.pebi07 = pebi07;
    }

    @XmlElement(name = "PEBI08")
    public String getPebi08() {
        return pebi08;
    }

    public void setPebi08(String pebi08) {
        this.pebi08 = pebi08;
    }
    
    @XmlTransient
    public Matricula2019Cabecera getMatricula2019Cabecera() {
        return matricula2019Cabecera;
    }

    public void setMatricula2019Cabecera(Matricula2019Cabecera matricula2019Cabecera) {
        this.matricula2019Cabecera = matricula2019Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2019Dlen)) {
            return false;
        }
        Matricula2019Dlen other = (Matricula2019Dlen) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Dlen[idEnvio=" + idEnvio + "]";
    }

}
