/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.ejb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.enums.estadistica.ConstanteEnum;
//import pe.gob.minedu.escale.eol.domain.Constantes;

import pe.gob.minedu.escale.eol.domain.Constantes;

import pe.gob.minedu.escale.eol.ejb.AbstractFacade;

//import pe.gob.minedu.escale.eol.ejb.ConstanteLocal;

/**
 *
 * @author Ing. Oscar Mateo
 * 
 */

@Stateless
public class ConstanteFacade extends AbstractFacade<Constantes> implements ConstanteLocal {

	private Logger logger = Logger.getLogger(ConstanteFacade.class);

	@PersistenceContext(unitName = "eol-PUAux")
	private EntityManager em;

	private static Map<String, List<Constantes>> mapCons = new HashMap<String, List<Constantes>>();
	private static StringBuffer cadPQL = new StringBuffer();

	public ConstanteFacade() {
		super(Constantes.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public List<Constantes> listarConstXTipo(ConstanteEnum tipConst) {
		logger.info(":: ConstanteFacade.listarConstXTipo :: Starting execution...");
		if (tipConst != null
				&& (mapCons.get(tipConst.getTipo()) == null || mapCons.get(tipConst.getTipo()).isEmpty())) {
			cadPQL.delete(0, cadPQL.length());
			cadPQL.append(" SELECT c FROM  Constantes c");
			cadPQL.append(" WHERE c.tipConst=:tipo");

			Query query = em.createQuery(cadPQL.toString());
			query.setParameter("tipo", tipConst.getTipo());
			List<Constantes> listMap = query.getResultList();
			mapCons.put(tipConst.getTipo(), listMap);
			return listMap;
		}
		logger.info(":: ConstanteFacade.listarConstXTipo :: Execution finish.");
		return tipConst != null ? mapCons.get(tipConst.getTipo()) : null;

	}

	public Constantes devConstante(ConstanteEnum tipConst, String codConst) {
		logger.info(":: ConstanteFacade.devConstante :: Starting execution...");
		List<Constantes> listMap = listarConstXTipo(tipConst);
		Constantes constDev = null;
		for (Constantes constante : listMap) {
			if (constante.getCodConst().equals(codConst)) {
				constDev = constante;
			}
		}
		logger.info(":: ConstanteFacade.devConstante :: Execution finish.");
		return constDev;
	}

}
