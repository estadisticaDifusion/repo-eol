/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "local2019_sec500")
public class Local2019Sec500 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;

    @Column(name = "CUADRO")
    private String cuadro;
    @Column(name = "ORDEN")
    private Integer orden;
    @Column(name = "P500_ESP")
    private String p500Esp;
    @Column(name = "P500_1")
    private String p5001;
    @Column(name = "P500_2")
    private Integer p5002;
    @Column(name = "P500_3")
    private Integer p5003;
    @Column(name = "P500_4")
    private Integer p5004;
    @Column(name = "P500_5")
    private Integer p5005;
    @Column(name = "P500_6")
    private Integer p5006;
    
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2019Cabecera local2019Cabecera;

    public Local2019Sec500() {
    }

    public Local2019Sec500(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

   
    @XmlElement(name="P500_6")
    public Integer getP5006() {
		return p5006;
	}

	public void setP5006(Integer p5006) {
		this.p5006 = p5006;
	}

	@XmlElement(name="P500_ESP")
    public String getP500Esp() {
        return p500Esp;
    }

    public void setP500Esp(String p500Esp) {
        this.p500Esp = p500Esp;
    }


    @XmlElement(name="P500_2")
    public Integer getP5002() {
        return p5002;
    }

    public void setP5002(Integer p5002) {
        this.p5002 = p5002;
    }

    @XmlElement(name="P500_3")
    public Integer getP5003() {
        return p5003;
    }

    public void setP5003(Integer p5003) {
        this.p5003 = p5003;
    }

    @XmlElement(name="P500_4")
    public Integer getP5004() {
        return p5004;
    }

    public void setP5004(Integer p5004) {
        this.p5004 = p5004;
    }

    @XmlElement(name="P500_5")
    public Integer getP5005() {
        return p5005;
    }

    public void setP5005(Integer p5005) {
        this.p5005 = p5005;
    }

    ////////////////////////////////////////////////////
    @XmlElement(name="CUADRO")
    public String getCuadro() {
		return cuadro;
	}

	public void setCuadro(String cuadro) {
		this.cuadro = cuadro;
	}
	@XmlElement(name="ORDEN")
	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	@XmlElement(name="P500_1")
	public String getP5001() {
		return p5001;
	}

	public void setP5001(String p5001) {
		this.p5001 = p5001;
	}
	
	
	@XmlTransient
	public Local2019Cabecera getLocal2019Cabecera() {
		return local2019Cabecera;
	}

	public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
		this.local2019Cabecera = local2019Cabecera;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2019Sec500)) {
            return false;
        }
        Local2019Sec500 other = (Local2019Sec500) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2019[idEnvio=" + idEnvio + "]";
    }

}
