package pe.gob.minedu.escale.eol.domain.censo2018;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
//import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018MatriculaMapAdapter.Matricula2018MatriculaList;

import pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018MatriculaMapAdapter.Matricula2018MatriculaList;

public class Matricula2018MatriculaMapAdapter
		extends XmlAdapter<Matricula2018MatriculaList, Map<String, Matricula2018Matricula>> {
	
	
	private static final Logger LOGGER = Logger.getLogger(Matricula2018MatriculaMapAdapter.class.getName());
	


	 static class Matricula2018MatriculaList {

	        private List<Matricula2018Matricula> detalle;

	        private Matricula2018MatriculaList(ArrayList<Matricula2018Matricula> lista) {
	            detalle = lista;
	        }

	        public Matricula2018MatriculaList() {
	        }

	        @XmlElement(name = "CUADROS")
	        public List<Matricula2018Matricula> getDetalle() {
	            return detalle;
	        }

	        public void setDetalle(List<Matricula2018Matricula> detalle) {
	            this.detalle = detalle;
	        }
	    }
	

	 
	   @Override
	    public Map<String, Matricula2018Matricula> unmarshal(Matricula2018MatriculaList v) throws Exception {

	        Map<String, Matricula2018Matricula> map = new HashMap<String, Matricula2018Matricula>();
	        for (Matricula2018Matricula detalle : v.getDetalle()) {
	            map.put(detalle.getCuadro(), detalle);
	        }
	        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
	        return map;
	    }

	
	
	   @Override
	    public Matricula2018MatriculaList marshal(Map<String, Matricula2018Matricula> v) throws Exception {
	        if(v==null)return null;

	        Set<String> keySet = v.keySet();
	        if (keySet.isEmpty()) {
	            LOGGER.fine("no hay claves");
	            return null;

	        }
	        ArrayList<Matricula2018Matricula> lista = new ArrayList<Matricula2018Matricula>();
	        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
	            String key = it.next();
	            Matricula2018Matricula $detalle = v.get(key);
	            $detalle.setCuadro(key);
	            lista.add($detalle);
	        }
	        Matricula2018MatriculaList list = new Matricula2018MatriculaList(lista);
	        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
	        return list;
	    }
	   
}
