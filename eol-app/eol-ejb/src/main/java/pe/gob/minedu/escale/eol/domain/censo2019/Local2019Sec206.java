package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2019_sec206")
public class Local2019Sec206 implements Serializable {
	private static final long serialVersionUID = 1L;
	
	  @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Basic(optional = false)
	    @Column(name = "ID_ENVIO")
	    private Long idEnvio;
	    @Column(name = "P206_1")
	    private String p2061;
	    @Column(name = "P206_21")
	    private Integer p20621;
	    @Column(name = "P206_22")
	    private Integer p20622;
	    @Column(name = "P206_3")
	    private String p2063;
	    @Column(name = "P206_4")
	    private String p2064;
	    @Column(name = "P206_51")
	    private Integer p20651;
	    @Column(name = "P206_52")
	    private Integer p20652;
	    @Column(name = "P206_6")
	    private String p2066;
	    @Column(name = "P206_7")
	    private String p2067;
	    @Column(name = "P206_8")
	    private String p2068;
	    @Column(name = "P206_9")
	    private String p2069;
	    @Column(name = "P206_10")
	    private String p20610;
	    @Column(name = "P206_11")
	    private String p20611;
	    @Column(name = "P206_12")
	    private String p20612;
	    
	    
	    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	    @ManyToOne
	    private Local2019Cabecera local2019Cabecera;

	    public Local2019Sec206() {
	    }

	    public Local2019Sec206(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }
	  
	    public Long getIdEnvio() {
	        return idEnvio;
	    }

	    public void setIdEnvio(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }
	    @XmlElement(name = "P206_1")
	    public String getP2061() {
	        return p2061;
	    }

	    public void setP2061(String p2061) {
	        this.p2061 = p2061;
	    }
	    @XmlElement(name = "P206_21")
	    public Integer getP20621() {
	        return p20621;
	    }

	    public void setP20621(Integer p20621) {
	        this.p20621 = p20621;
	    }
	    @XmlElement(name = "P206_22")
	    public Integer getP20622() {
	        return p20622;
	    }

	    public void setP20622(Integer p20622) {
	        this.p20622 = p20622;
	    }
	    @XmlElement(name = "P206_3")
	    public String getP2063() {
	        return p2063;
	    }

	    public void setP2063(String p2063) {
	        this.p2063 = p2063;
	    }
	    @XmlElement(name = "P206_4")
	    public String getP2064() {
	        return p2064;
	    }

	    public void setP2064(String p2064) {
	        this.p2064 = p2064;
	    }
	    @XmlElement(name = "P206_51")
	    public Integer getP20651() {
	        return p20651;
	    }

	    public void setP20651(Integer p20651) {
	        this.p20651 = p20651;
	    }
	    @XmlElement(name = "P206_52")
	    public Integer getP20652() {
	        return p20652;
	    }

	    public void setP20652(Integer p20652) {
	        this.p20652 = p20652;
	    }
	    @XmlElement(name = "P206_6")
	    public String getP2066() {
	        return p2066;
	    }

	    public void setP2066(String p2066) {
	        this.p2066 = p2066;
	    }
	    @XmlElement(name = "P206_7")
	    public String getP2067() {
	        return p2067;
	    }

	    public void setP2067(String p2067) {
	        this.p2067 = p2067;
	    }
	    @XmlElement(name = "P206_8")
	    public String getP2068() {
	        return p2068;
	    }

	    public void setP2068(String p2068) {
	        this.p2068 = p2068;
	    }
	    @XmlElement(name = "P206_9")
	    public String getP2069() {
	        return p2069;
	    }

	    public void setP2069(String p2069) {
	        this.p2069 = p2069;
	    }
	    @XmlElement(name = "P206_10")
	    public String getP20610() {
	        return p20610;
	    }

	    public void setP20610(String p20610) {
	        this.p20610 = p20610;
	    }
	    @XmlElement(name = "P206_11")
	    public String getP20611() {
	        return p20611;
	    }

	    public void setP20611(String p20611) {
	        this.p20611 = p20611;
	    }
	    @XmlElement(name = "P206_12")
	    public String getP20612() {
	        return p20612;
	    }

	    public void setP20612(String p20612) {
	        this.p20612 = p20612;
	    }
	    
	    @XmlTransient
	    public Local2019Cabecera getLocal2019Cabecera() {
	        return local2019Cabecera;
	    }

	    public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
	        this.local2019Cabecera = local2019Cabecera;
	    }

	    @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
	        return hash;
	    }

	    @Override
	    public boolean equals(Object object) {
	        // TODO: Warning - this method won't work in the case the id fields are not set
	        if (!(object instanceof Local2019Sec206)) {
	            return false;
	        }
	        Local2019Sec206 other = (Local2019Sec206) object;
	        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
	            return false;
	        }
	        return true;
	    }

	    @Override
	    public String toString() {
	        return "pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec206[idEnvio=" + idEnvio + "]";
	    }
	
	
}
