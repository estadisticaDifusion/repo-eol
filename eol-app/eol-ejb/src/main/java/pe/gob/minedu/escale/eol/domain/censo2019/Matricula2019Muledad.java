package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "matricula2019_muledad")
public class Matricula2019Muledad implements Serializable {
	 private static final long serialVersionUID = 1L;
	 	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Basic(optional = false)
	    @Column(name = "ID_ENVIO")
	    private Long idEnvio;
	    @Basic(optional = false)
	    @Column(name = "NROCED")
	    private String nroced;
	    @Basic(optional = false)
	    @Column(name = "CUADRO")
	    private String cuadro;
	    @Basic(optional = false)
	    @Column(name = "NUMERO")
	    private String numero;
	    @Basic(optional = false)
	    @Column(name = "DESCRIP")
	    private String descrip;
	    @Column(name = "CHK1")
	    private String chk1;
	    @Column(name = "CHK2")
	    private String chk2;
	    @Column(name = "CHK3")
	    private String chk3;
	    @Column(name = "CHK4")
	    private String chk4;
	    @Column(name = "CHK5")
	    private String chk5;
	    @Column(name = "CHK6")
	    private String chk6;
	    @Column(name = "CHK7")
	    private String chk7;
	    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	    @ManyToOne(optional = false)
	    private Matricula2019Cabecera matricula2019Cabecera;

	    public Matricula2019Muledad() {
	    }

	    public Matricula2019Muledad(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }

	    public Matricula2019Muledad(Long idEnvio, String nroced, String cuadro, String numero, String descrip) {
	        this.idEnvio = idEnvio;
	        this.nroced = nroced;
	        this.cuadro = cuadro;
	        this.numero = numero;
	        this.descrip = descrip;
	    }

	    public Long getIdEnvio() {
	        return idEnvio;
	    }

	    public void setIdEnvio(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }
	    @XmlElement(name = "NROCED")
	    public String getNroced() {
	        return nroced;
	    }

	    public void setNroced(String nroced) {
	        this.nroced = nroced;
	    }
	    @XmlElement(name = "CUADRO")
	    public String getCuadro() {
	        return cuadro;
	    }

	    public void setCuadro(String cuadro) {
	        this.cuadro = cuadro;
	    }
	    @XmlElement(name = "NUMERO")
	    public String getNumero() {
	        return numero;
	    }

	    public void setNumero(String numero) {
	        this.numero = numero;
	    }
	    @XmlElement(name = "DESCRIP")
	    public String getDescrip() {
	        return descrip;
	    }

	    public void setDescrip(String descrip) {
	        this.descrip = descrip;
	    }
	    @XmlElement(name = "CHK1")
	    public String getChk1() {
	        return chk1;
	    }

	    public void setChk1(String chk1) {
	        this.chk1 = chk1;
	    }
	    @XmlElement(name = "CHK2")
	    public String getChk2() {
	        return chk2;
	    }

	    public void setChk2(String chk2) {
	        this.chk2 = chk2;
	    }
	    @XmlElement(name = "CHK3")
	    public String getChk3() {
	        return chk3;
	    }

	    public void setChk3(String chk3) {
	        this.chk3 = chk3;
	    }
	    @XmlElement(name = "CHK4")
	    public String getChk4() {
	        return chk4;
	    }

	    public void setChk4(String chk4) {
	        this.chk4 = chk4;
	    }
	    @XmlElement(name = "CHK5")
	    public String getChk5() {
	        return chk5;
	    }

	    public void setChk5(String chk5) {
	        this.chk5 = chk5;
	    }
	    @XmlElement(name = "CHK6")
	    public String getChk6() {
	        return chk6;
	    }

	    public void setChk6(String chk6) {
	        this.chk6 = chk6;
	    }
	    
	    @XmlElement(name = "CHK7")
	    public String getChk7() {
			return chk7;
		}

		public void setChk7(String chk7) {
			this.chk7 = chk7;
		}

		@XmlTransient
	    public Matricula2019Cabecera getMatricula2019Cabecera() {
	        return matricula2019Cabecera;
	    }

	    public void setMatricula2019Cabecera(Matricula2019Cabecera matricula2019Cabecera) {
	        this.matricula2019Cabecera = matricula2019Cabecera;
	    }

	    @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
	        return hash;
	    }

	    @Override
	    public boolean equals(Object object) {
	        // TODO: Warning - this method won't work in the case the id fields are not set
	        if (!(object instanceof Matricula2019Muledad)) {
	            return false;
	        }
	        Matricula2019Muledad other = (Matricula2019Muledad) object;
	        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
	            return false;
	        }
	        return true;
	    }

	    @Override
	    public String toString() {
	        return "pe.gob.minedu.entidad2019.Matricula2019Muledad[idEnvio=" + idEnvio + "]";
	    }
	
}
