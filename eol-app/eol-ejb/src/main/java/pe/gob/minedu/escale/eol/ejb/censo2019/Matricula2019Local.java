package pe.gob.minedu.escale.eol.ejb.censo2019;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Cabecera;

@Local
public interface Matricula2019Local {

	public void create(Matricula2019Cabecera cedula);
	public Matricula2019Cabecera findByCodModAnNiv(String codmod, String anexo, String nivmod);
	public List<Object[]> registrarEnvioIdentificacion(String codmod, String anexo, String nivmod);
}
