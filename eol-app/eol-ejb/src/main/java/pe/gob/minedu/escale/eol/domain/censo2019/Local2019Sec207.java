package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2019_sec207")
public class Local2019Sec207 implements Serializable {
	private static final long serialVersionUID = 1L;
	
	   @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Basic(optional = false)
	    @Column(name = "ID_ENVIO")
	    private Long idEnvio;
	    @Column(name = "P207_1")
	    private Integer p2071;
	    @Column(name = "P206_2")
	    private String p2062;
	    @Column(name = "P207_3")
	    private String p2073;
	    @Column(name = "P207_4")
	    private Integer p2074;
	    @Column(name = "P207_5")
	    private BigDecimal p2075;
	    @Column(name = "P207_6")
	    private Integer p2076;
	    @Column(name = "P207_7")
	    private String p2077;
	    @Column(name = "P207_8")
	    private String p2078;
	    @Column(name = "P207_9")
	    private String p2079;
	    @Column(name = "P207_10")
	    private String p20710;
	    @Column(name = "P207_111")
	    private String p207111;
	    @Column(name = "P207_112")
	    private String p207112;
	    @Column(name = "P207_113")
	    private String p207113;
	    @Column(name = "P207_114")
	    private String p207114;
	    @Column(name = "P207_115")
	    private String p207115;
	    @Column(name = "P207_116")
	    private String p207116;
	    @Column(name = "P207_117")
	    private String p207117;
	    @Column(name = "P207_12")
	    private String p20712;
	    @Column(name = "P207_13")
	    private String p20713;
	    @Column(name = "P207_14")
	    private String p20714;
	    @Column(name = "P207_15")
	    private String p20715;
	    @Column(name = "P207_16")
	    private String p20716;
	    @Column(name = "P207_17")
	    private String p20717;
	    @Column(name = "P207_18")
	    private String p20718;
	    
	    
	    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	    @ManyToOne
	    private Local2019Cabecera local2019Cabecera;

	    
	    
	    public Local2019Sec207() {
	    }

	    public Local2019Sec207(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }

	    public Long getIdEnvio() {
	        return idEnvio;
	    }

	    public void setIdEnvio(Long idEnvio) {
	        this.idEnvio = idEnvio;
	    }
	    @XmlElement(name = "P207_1")
	    public Integer getP2071() {
	        return p2071;
	    }

	    public void setP2071(Integer p2071) {
	        this.p2071 = p2071;
	    }
	    @XmlElement(name = "P206_2")
	    public String getP2062() {
	        return p2062;
	    }

	    public void setP2062(String p2062) {
	        this.p2062 = p2062;
	    }
	    @XmlElement(name = "P207_3")
	    public String getP2073() {
	        return p2073;
	    }

	    public void setP2073(String p2073) {
	        this.p2073 = p2073;
	    }
	    @XmlElement(name = "P207_4")
	    public Integer getP2074() {
	        return p2074;
	    }

	    public void setP2074(Integer p2074) {
	        this.p2074 = p2074;
	    }
	    @XmlElement(name = "P207_5")
	    public BigDecimal getP2075() {
	        return p2075;
	    }

	    public void setP2075(BigDecimal p2075) {
	        this.p2075 = p2075;
	    }
	    @XmlElement(name = "P207_6")
	    public Integer getP2076() {
	        return p2076;
	    }

	    public void setP2076(Integer p2076) {
	        this.p2076 = p2076;
	    }
	    @XmlElement(name = "P207_7")
	    public String getP2077() {
	        return p2077;
	    }

	    public void setP2077(String p2077) {
	        this.p2077 = p2077;
	    }
	    @XmlElement(name = "P207_8")
	    public String getP2078() {
	        return p2078;
	    }

	    public void setP2078(String p2078) {
	        this.p2078 = p2078;
	    }
	    @XmlElement(name = "P207_9")
	    public String getP2079() {
	        return p2079;
	    }

	    public void setP2079(String p2079) {
	        this.p2079 = p2079;
	    }
	    @XmlElement(name = "P207_10")
	    public String getP20710() {
	        return p20710;
	    }

	    public void setP20710(String p20710) {
	        this.p20710 = p20710;
	    }
	    @XmlElement(name = "P207_111")
	    public String getP207111() {
	        return p207111;
	    }

	    public void setP207111(String p207111) {
	        this.p207111 = p207111;
	    }
	    @XmlElement(name = "P207_112")
	    public String getP207112() {
	        return p207112;
	    }

	    public void setP207112(String p207112) {
	        this.p207112 = p207112;
	    }
	    @XmlElement(name = "P207_113")
	    public String getP207113() {
	        return p207113;
	    }

	    public void setP207113(String p207113) {
	        this.p207113 = p207113;
	    }
	    @XmlElement(name = "P207_114")
	    public String getP207114() {
	        return p207114;
	    }

	    public void setP207114(String p207114) {
	        this.p207114 = p207114;
	    }
	    @XmlElement(name = "P207_115")
	    public String getP207115() {
	        return p207115;
	    }

	    public void setP207115(String p207115) {
	        this.p207115 = p207115;
	    }
	    @XmlElement(name = "P207_116")
	    public String getP207116() {
	        return p207116;
	    }

	    public void setP207116(String p207116) {
	        this.p207116 = p207116;
	    }
	    @XmlElement(name = "P207_117")
	    public String getP207117() {
	        return p207117;
	    }

	    public void setP207117(String p207117) {
	        this.p207117 = p207117;
	    }
	    @XmlElement(name = "P207_12")
	    public String getP20712() {
	        return p20712;
	    }

	    public void setP20712(String p20712) {
	        this.p20712 = p20712;
	    }
	    @XmlElement(name = "P207_13")
	    public String getP20713() {
	        return p20713;
	    }

	    public void setP20713(String p20713) {
	        this.p20713 = p20713;
	    }
	    @XmlElement(name = "P207_14")
	    public String getP20714() {
	        return p20714;
	    }

	    public void setP20714(String p20714) {
	        this.p20714 = p20714;
	    }
	    @XmlElement(name = "P207_15")
	    public String getP20715() {
	        return p20715;
	    }

	    public void setP20715(String p20715) {
	        this.p20715 = p20715;
	    }
	    @XmlElement(name = "P207_16")
	    public String getP20716() {
	        return p20716;
	    }

	    public void setP20716(String p20716) {
	        this.p20716 = p20716;
	    }
	    @XmlElement(name = "P207_17")
	    public String getP20717() {
	        return p20717;
	    }

	    public void setP20717(String p20717) {
	        this.p20717 = p20717;
	    }
	    @XmlElement(name = "P207_18")
	    public String getP20718() {
	        return p20718;
	    }

	    public void setP20718(String p20718) {
	        this.p20718 = p20718;
	    }

	    
	    
	    @XmlTransient
	    public Local2019Cabecera getLocal2019Cabecera() {
	        return local2019Cabecera;
	    }

	    public void setLocal2019Cabecera(Local2019Cabecera local2019Cabecera) {
	        this.local2019Cabecera = local2019Cabecera;
	    }

	    @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
	        return hash;
	    }

	    @Override
	    public boolean equals(Object object) {
	        // TODO: Warning - this method won't work in the case the id fields are not set
	        if (!(object instanceof Local2019Sec207)) {
	            return false;
	        }
	        Local2019Sec207 other = (Local2019Sec207) object;
	        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
	            return false;
	        }
	        return true;
	    }

	    @Override
	    public String toString() {
	        return "pe.gob.minedu.entidad2019.Local2019Sec207[idEnvio=" + idEnvio + "]";
	    }
	
	
}
