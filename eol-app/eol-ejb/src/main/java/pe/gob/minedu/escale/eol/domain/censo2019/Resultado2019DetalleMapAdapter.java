package pe.gob.minedu.escale.eol.domain.censo2019;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019DetalleMapAdapter.Resultado2019DetalleList;

/**
 * 
 * @author WARODRIGUEZ
 *
 */

public class Resultado2019DetalleMapAdapter
		extends XmlAdapter<Resultado2019DetalleList, Map<String, Resultado2019Detalle>> {
	private static final Logger LOGGER = Logger.getLogger(Resultado2019DetalleMapAdapter.class.getName());

	
	public static class Resultado2019DetalleList {
		
		private List<Resultado2019Detalle> detalle;

		private Resultado2019DetalleList(ArrayList<Resultado2019Detalle> lista) {
			LOGGER.info("LLEGO RESULTADO DETALLE MAP ADAPTER : "  + lista );
			detalle = lista;
		}

		public Resultado2019DetalleList() {
			LOGGER.info("LLEGO RESULTADO DETALLE MAP ADAPTER 888888888888888: " + detalle   );
		}

		@XmlElement(name = "CUADROS")
		public List<Resultado2019Detalle> getDetalle() {
			LOGGER.info("LLEGO RESULTADO DETALLE MAP ADAPTER : 1111 " );
			return detalle;
		}

		public void setDetalle(List<Resultado2019Detalle> detalle) {
			this.detalle = detalle;
		}
	}

	@Override
	public Map<String, Resultado2019Detalle> unmarshal(Resultado2019DetalleList v) throws Exception {
		LOGGER.info("LLEGO RESULTADO DETALLE MAP ADAPTER 22222 : "  + v );
		Map<String, Resultado2019Detalle> map = new HashMap<String, Resultado2019Detalle>();
		for (Resultado2019Detalle detalle : v.getDetalle()) {
			map.put(detalle.getCuadro(), detalle);
		}
		LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[] { v, map });
		return map;
	}

	@Override
	public Resultado2019DetalleList marshal(Map<String, Resultado2019Detalle> v) throws Exception {
		LOGGER.info("LLEGO RESULTADO DETALLE MAP ADAPTER 33333 : "  + v );
		if (v == null) {
			LOGGER.fine("no hay detalles");
			return null;
		}
		Set<String> keySet = v.keySet();
		if (keySet.isEmpty()) {
			LOGGER.fine("no hay claves");
			return null;

		}
		ArrayList<Resultado2019Detalle> lista = new ArrayList<Resultado2019Detalle>();
		for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
			String key = it.next();
			Resultado2019Detalle $detalle = v.get(key);
			$detalle.setCuadro(key);
			lista.add($detalle);
		}
		Resultado2019DetalleList list = new Resultado2019DetalleList(lista);
		LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[] { v, lista });
		return list;

	}

}
