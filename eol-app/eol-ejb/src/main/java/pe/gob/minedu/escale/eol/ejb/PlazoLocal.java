package pe.gob.minedu.escale.eol.ejb;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.domain.Plazo;



/**
 *
 * @author Ing. Oscar Mateo   
 */

@Local
public interface PlazoLocal {
	Plazo find(String periodo, String proceso);

	String verificarPlazoEntrega(String anio, String tipoDocumento);
}
