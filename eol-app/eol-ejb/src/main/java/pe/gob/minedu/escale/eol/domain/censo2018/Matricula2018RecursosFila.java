/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "matricula2018_recursos_fila")
public class Matricula2018RecursosFila implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "VCTRL_1")
    private String vctrl1;
    @Column(name = "VCTRL_2")
    private String vctrl2;
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL")
    private Integer total;
    @Column(name = "DATO01")
    private Integer dato01;
    @Column(name = "DATO02")
    private Integer dato02;
    @Column(name = "DATO03")
    private Integer dato03;
    @Column(name = "DATO04")
    private Integer dato04;
    @Column(name = "DATO05")
    private Integer dato05;
    @Column(name = "DATO06")
    private Integer dato06;
    @Column(name = "CHK1")
    private String chk1;
    @Column(name = "CHK2")
    private String chk2;
    @Column(name = "CHK3")
    private String chk3;
    @Column(name = "CHK4")
    private String chk4;
    @Column(name = "CHK5")
    private String chk5;
    @Column(name = "DIA_1")
    private String dia1;
    @Column(name = "MES_1")
    private String mes1;
    @Column(name = "ANIO_1")
    private String anio1;
    @Column(name = "DIA_2")
    private String dia2;
    @Column(name = "MES_2")
    private String mes2;
    @Column(name = "ANIO_2")
    private String anio2;
    @JoinColumn(name = "DETALLE_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Matricula2018Recursos matricula2018Recursos;

    public Matricula2018RecursosFila() {
    }

    public Matricula2018RecursosFila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "VCTRL_1")
    public String getVctrl1() {
        return vctrl1;
    }

    public void setVctrl1(String vctrl1) {
        this.vctrl1 = vctrl1;
    }

    @XmlElement(name = "VCTRL_2")
    public String getVctrl2() {
        return vctrl2;
    }

    public void setVctrl2(String vctrl2) {
        this.vctrl2 = vctrl2;
    }

    @XmlElement(name = "TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name = "DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name = "TOTAL")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @XmlElement(name = "DATO01")
    public Integer getDato01() {
        return dato01;
    }

    public void setDato01(Integer dato01) {
        this.dato01 = dato01;
    }

    @XmlElement(name = "DATO02")
    public Integer getDato02() {
        return dato02;
    }

    public void setDato02(Integer dato02) {
        this.dato02 = dato02;
    }

    @XmlElement(name = "DATO03")
    public Integer getDato03() {
        return dato03;
    }

    public void setDato03(Integer dato03) {
        this.dato03 = dato03;
    }

    @XmlElement(name = "DATO04")
    public Integer getDato04() {
        return dato04;
    }

    public void setDato04(Integer dato04) {
        this.dato04 = dato04;
    }

    @XmlElement(name = "DATO05")
    public Integer getDato05() {
        return dato05;
    }

    public void setDato05(Integer dato05) {
        this.dato05 = dato05;
    }

    @XmlElement(name = "DATO06")
    public Integer getDato06() {
        return dato06;
    }

    public void setDato06(Integer dato06) {
        this.dato06 = dato06;
    }

    @XmlElement(name = "CHK1")
    public String getChk1() {
        return chk1;
    }

    public void setChk1(String chk1) {
        this.chk1 = chk1;
    }

    @XmlElement(name = "CHK2")
    public String getChk2() {
        return chk2;
    }

    public void setChk2(String chk2) {
        this.chk2 = chk2;
    }

    @XmlElement(name = "CHK3")
    public String getChk3() {
        return chk3;
    }

    public void setChk3(String chk3) {
        this.chk3 = chk3;
    }

    @XmlElement(name = "CHK4")
    public String getChk4() {
        return chk4;
    }

    public void setChk4(String chk4) {
        this.chk4 = chk4;
    }

    @XmlElement(name = "CHK5")
    public String getChk5() {
        return chk5;
    }

    public void setChk5(String chk5) {
        this.chk5 = chk5;
    }

    @XmlElement(name = "ANIO_1")
    public String getAnio1() {
        return anio1;
    }

    public void setAnio1(String anio1) {
        this.anio1 = anio1;
    }

    @XmlElement(name = "ANIO_2")
    public String getAnio2() {
        return anio2;
    }

    public void setAnio2(String anio2) {
        this.anio2 = anio2;
    }

    @XmlElement(name = "DIA_1")
    public String getDia1() {
        return dia1;
    }

    public void setDia1(String dia1) {
        this.dia1 = dia1;
    }

    @XmlElement(name = "DIA_2")
    public String getDia2() {
        return dia2;
    }

    public void setDia2(String dia2) {
        this.dia2 = dia2;
    }

    @XmlElement(name = "MES_1")
    public String getMes1() {
        return mes1;
    }

    public void setMes1(String mes1) {
        this.mes1 = mes1;
    }

    @XmlElement(name = "MES_2")
    public String getMes2() {
        return mes2;
    }

    public void setMes2(String mes2) {
        this.mes2 = mes2;
    }

    @XmlTransient
    public Matricula2018Recursos getMatricula2018Recursos() {
        return matricula2018Recursos;
    }

    public void setMatricula2018Recursos(Matricula2018Recursos matricula2018Recursos) {
        this.matricula2018Recursos = matricula2018Recursos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2018RecursosFila)) {
            return false;
        }
        Matricula2018RecursosFila other = (Matricula2018RecursosFila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018RecursosFila[idEnvio=" + idEnvio + "]";
    }

}
