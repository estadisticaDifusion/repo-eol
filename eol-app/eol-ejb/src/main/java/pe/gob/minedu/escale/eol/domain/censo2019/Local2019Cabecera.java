/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2019_cabecera")
public class Local2019Cabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;
	
	@Column(name = "CODLOCAL")
	private String codlocal;
	
	@Column(name = "TIPOLOCAL")
	private String tipolocal;
	
	@Column(name = "TELEFONO")
	private String telefono;
	
	@Column(name = "CODGEO")
	private String codgeo;
	
	@Column(name = "DPTO")
	private String dpto;
	
	@Column(name = "PROV")
	private String prov;
	
	@Column(name = "DISTRITO")
	private String distrito;
	
	@Column(name = "CODUGEL")
	private String codugel;
	
	@Column(name = "CEN_POB")
	private String cenPob;
	
	@Column(name = "CATEG_CP")
	private String categCp;
	
	@Column(name = "NOMLUGAR")
	private String nomlugar;
	
	@Column(name = "CATEGORIA")
	private String categoria;
	
	@Column(name = "TIPOVIA")
	private String tipovia;
	
	@Column(name = "NOMBVIA")
	private String nombvia;
	
	@Column(name = "NUMERO")
	private String numero;
	
	@Column(name = "MANZANA")
	private String manzana;
	
	@Column(name = "LOTE")
	private String lote;
	
	@Column(name = "SECTOR")
	private String sector;
	
	@Column(name = "ZONA")
	private String zona;
	
	@Column(name = "ETAPA")
	private String etapa;
	
	@Column(name = "OTROS")
	private String otros;
	
	@Column(name = "REFERENCIA")
	private String referencia;
	@Column(name = "P101")
	private String p101;
	@Column(name = "P101_ESP")
	private String p101Esp;
	@Column(name = "P102")
	private String p102;
	@Column(name = "P102_ESP")
	private String p102Esp;
	@Column(name = "P103")
	private String p103;
	@Column(name = "P103_FREG")
	private String p103Freg;
	@Column(name = "P103_OREG")
	private String p103Oreg;
	@Column(name = "P103_AREAE")
	private Integer p103Areae;
	@Column(name = "P103_AREAD")
	private Integer p103Aread;
	@Column(name = "P104")
	private String p104;
	
	@Column(name = "P104_EST")
	private String p104Est;
	
	@Column(name = "P104_TER")
	private String p104Ter;
	
	@Column(name = "P105")
	private String p105;
	
	@Column(name = "P106")
	private String p106;
	
	@Column(name = "P106_ESP")
	private String p106Esp;
	
	@Column(name = "P107")
	private String p107;
	
	@Column(name = "P107_ITSE")
	private String p107Itse;
	
	@Column(name = "P108")
	private String p108;
	
	@Column(name = "P109")
	private String p109;
	
	@Column(name = "P109_SI")
	private String p109Si;
	
	@Column(name = "P110_1")
	private String p1101;
	
	@Column(name = "P110_2")
	private String p1102;
	
	@Column(name = "P110_3")
	private String p1103;
	
	@Column(name = "P110_4")
	private String p1104;
	@Column(name = "P110_5")
	private String p1105;
	@Column(name = "P110_6")
	private String p1106;
	@Column(name = "P110_ESP")
	private String p110Esp;
	@Column(name = "P111_1")
	private String p1111;
	@Column(name = "P111_2")
	private String p1112;
	@Column(name = "P111_3")
	private String p1113;
	@Column(name = "P111_4")
	private String p1114;
	@Column(name = "P111_5")
	private String p1115;
	@Column(name = "P111_6")
	private String p1116;
	@Column(name = "P111_ESP")
	private String p111Esp;
	@Column(name = "P113_ATAE")
	private Integer p113Atae;
	@Column(name = "P113_ATAD")
	private Integer p113Atad;
	@Column(name = "P113_ACE")
	private Integer p113Ace;
	@Column(name = "P113_ACD")
	private Integer p113Acd;
	@Column(name = "P202")
	private String p202;
	@Column(name = "P203")
	private String p203;
	@Column(name = "P203_ESP")
	private String p203Esp;
	@Column(name = "P204")
	private String p204;
	@Column(name = "P204_SI")
	private String p204Si;
	@Column(name = "P205_TPT")
	private Integer p205Tpt;
	@Column(name = "P207_QEDIF")
	private Integer p207Qedif;
	@Column(name = "P301_QAULA")
	private Integer p301Qaula;
	@Column(name = "P401")
	private Integer p401;
	@Column(name = "P402")
	private Integer p402;
	@Column(name = "P403")
	private Integer p403;
	@Column(name = "P404")
	private Integer p404;
	@Column(name = "P501")
	private Integer p501;
	@Column(name = "P502")
	private Integer p502;
	@Column(name = "P503")
	private Integer p503;
	@Column(name = "P504")
	private Integer p504;
	@Column(name = "P601")
	private String p601;
	@Column(name = "P601_SI")
	private String p601Si;
	@Column(name = "P602_1")
	private Integer p6021;
	@Column(name = "P602_2")
	private Integer p6022;
	@Column(name = "P603")
	private String p603;
	@Column(name = "P604_1")
	private String p6041;
	@Column(name = "P604_2")
	private String p6042;
	@Column(name = "P604_3")
	private String p6043;
	@Column(name = "P604_4")
	private String p6044;
	@Column(name = "P604_5")
	private String p6045;
	@Column(name = "P604_6")
	private String p6046;
	@Column(name = "P604_7")
	private String p6047;
	@Column(name = "P604_8")
	private String p6048;
	@Column(name = "P604_9")
	private String p6049;
	@Column(name = "P604_10")
	private String p60410;
	@Column(name = "P604_11")
	private String p60411;
	@Column(name = "P604_12")
	private String p60412;
	@Column(name = "P604_13")
	private String p60413;
	@Column(name = "P604_14")
	private String p60414;
	@Column(name = "P605")
	private String p605;
	@Column(name = "P606")
	private String p606;
	@Column(name = "P607")
	private String p607;
	@Column(name = "P607_NO")
	private String p607No;
	@Column(name = "P607_ESP")
	private String p607Esp;
	@Column(name = "P608_1")
	private String p6081;
	@Column(name = "P608_2")
	private String p6082;
	@Column(name = "P608_3")
	private String p6083;
	@Column(name = "P608_4")
	private String p6084;
	@Column(name = "P608_5")
	private String p6085;
	@Column(name = "P608_6")
	private String p6086;
	@Column(name = "P608_7")
	private String p6087;
	@Column(name = "P608_8")
	private String p6088;
	@Column(name = "P608_9")
	private String p6089;
	@Column(name = "P609")
	private String p609;
	@Column(name = "P610_1")
	private String p6101;
	@Column(name = "P610_2")
	private String p6102;
	@Column(name = "P610_3")
	private String p6103;
	@Column(name = "P610_4")
	private String p6104;
	@Column(name = "P610_5")
	private String p6105;
	@Column(name = "P610_6")
	private String p6106;
	@Column(name = "P610_7")
	private String p6107;
	@Column(name = "P702")
	private String p702;
	@Column(name = "P703")
	private String p703;
	@Column(name = "P703_ESP")
	private String p703Esp;
	@Column(name = "P704")
	private String p704;
	@Column(name = "P704_ESP")
	private String p704Esp;
	@Column(name = "P706")
	private String p706;
	@Column(name = "P707_1")
	private String p7071;
	@Column(name = "P707_2")
	private String p7072;
	@Column(name = "P707_3")
	private String p7073;
	@Column(name = "P707_4")
	private String p7074;
	@Column(name = "P707_5")
	private String p7075;
	@Column(name = "P707_6")
	private String p7076;
	@Column(name = "P707_7")
	private String p7077;
	@Column(name = "P707_8")
	private String p7078;
	@Column(name = "P707_9")
	private String p7079;
	@Column(name = "P707_10")
	private String p70710;
	@Column(name = "P707_11")
	private String p70711;
	@Column(name = "P707_11NT")
	private String p70711nt;
	@Column(name = "P708")
	private String p708;
	@Column(name = "P708_PROV")
	private String p708Prov;
	@Column(name = "P708_NSUM")
	private String p708Nsum;
	@Column(name = "P709")
	private String p709;
	@Column(name = "P709_NO")
	private Integer p709No;
	@Column(name = "P709_NO1")
	private String p709No1;
	@Column(name = "P709_NO2")
	private String p709No2;
	@Column(name = "P709_NO3")
	private String p709No3;
	@Column(name = "P710")
	private String p710;
	@Column(name = "P710_ESP")
	private String p710Esp;
	@Column(name = "P711")
	private String p711;
	@Column(name = "P711_ESP")
	private String p711Esp;
	@Column(name = "P711_PROV")
	private String p711Prov;
	@Column(name = "P711_NSUM")
	private String p711Nsum;
	@Column(name = "P712")
	private String p712;
	@Column(name = "P712_NO")
	private Integer p712No;
	@Column(name = "P712_NO1")
	private String p712No1;
	@Column(name = "P712_NO2")
	private String p712No2;
	@Column(name = "P712_NO3")
	private String p712No3;
	@Column(name = "P713")
	private String p713;
	@Column(name = "P713_ESP")
	private String p713Esp;
	@Column(name = "P714_11")
	private String p71411;
	@Column(name = "P714_12")
	private String p71412;
	@Column(name = "P714_13")
	private String p71413;
	@Column(name = "P714_21")
	private String p71421;
	@Column(name = "P714_22")
	private String p71422;
	@Column(name = "P714_23")
	private String p71423;
	@Column(name = "P714_31")
	private String p71431;
	@Column(name = "P714_32")
	private String p71432;
	@Column(name = "P714_33")
	private String p71433;
	@Column(name = "P715")
	private String p715;
	@Column(name = "P716")
	private String p716;
	@Column(name = "P717")
	private String p717;
	@Column(name = "P717_SIQN")
	private Integer p717Siqn;
	@Column(name = "P718_QN1")
	private Integer p718Qn1;
	@Column(name = "P718_QN2")
	private Integer p718Qn2;
	@Column(name = "P718_QN3")
	private Integer p718Qn3;
	@Column(name = "P901_1")
	private String p9011;
	@Column(name = "P901_2")
	private String p9012;
	@Column(name = "P901_3")
	private String p9013;
	@Column(name = "P901_4")
	private String p9014;
	@Column(name = "P901_5")
	private String p9015;
	@Column(name = "P901_6")
	private String p9016;
	@Column(name = "P901_7")
	private String p9017;
	@Column(name = "P901_8")
	private String p9018;
	@Column(name = "P901_9")
	private String p9019;
	@Column(name = "P901_10")
	private String p90110;
	@Column(name = "P901_11")
	private String p90111;
	@Column(name = "P901_12")
	private String p90112;
	@Column(name = "P901_13")
	private String p90113;
	@Column(name = "P901_14")
	private String p90114;
	@Column(name = "P901_15")
	private String p90115;
	@Column(name = "P901_16")
	private String p90116;
	@Column(name = "P901_17")
	private String p90117;
	@Column(name = "P901_18")
	private String p90118;
	@Column(name = "P901_19")
	private String p90119;
	@Column(name = "P901_20")
	private String p90120;
	@Column(name = "P901_ESP")
	private String p901Esp;
	@Column(name = "P902")
	private String p902;
	@Column(name = "P903")
	private String p903;
	@Column(name = "P904")
	private String p904;
	@Column(name = "P904_SI1")
	private String p904Si1;
	@Column(name = "P904_SI2")
	private String p904Si2;
	@Column(name = "P904_SI3")
	private String p904Si3;
	@Column(name = "P904_SI4")
	private String p904Si4;
	@Column(name = "P904_SI5")
	private String p904Si5;
	@Column(name = "P904_SI6")
	private String p904Si6;
	@Column(name = "P904_SIE")
	private String p904Sie;
	@Column(name = "P905_1")
	private String p9051;
	@Column(name = "P905_11")
	private String p90511;
	@Column(name = "P905_2")
	private String p9052;
	@Column(name = "P905_21")
	private String p90521;
	@Column(name = "P905_3")
	private String p9053;
	@Column(name = "P905_31")
	private String p90531;
	@Column(name = "P905_4")
	private String p9054;
	@Column(name = "P905_41")
	private String p90541;
	@Column(name = "P905_5")
	private String p9055;
	@Column(name = "P905_51")
	private String p90551;
	@Column(name = "P906")
	private String p906;
	@Column(name = "P906_SI1")
	private String p906Si1;
	@Column(name = "P906_SI2")
	private String p906Si2;
	@Column(name = "P906_SI3")
	private String p906Si3;
	@Column(name = "P906_SI4")
	private String p906Si4;
	@Column(name = "P906_SI5")
	private String p906Si5;
	@Column(name = "P906_SI6")
	private String p906Si6;
	@Column(name = "P906_SIE")
	private String p906Sie;
	@Column(name = "P907_1")
	private String p9071;
	@Column(name = "P907_11")
	private String p90711;
	@Column(name = "P907_2")
	private String p9072;
	@Column(name = "P907_21")
	private String p90721;
	@Column(name = "P907_3")
	private String p9073;
	@Column(name = "P907_31")
	private String p90731;
	@Column(name = "P907_4")
	private String p9074;
	@Column(name = "P907_41")
	private String p90741;
	@Column(name = "P907_5")
	private String p9075;
	@Column(name = "P907_51")
	private String p90751;
	@Column(name = "P908_1")
	private String p9081;
	@Column(name = "P908_2")
	private String p9082;

	@Column(name = "P908_3")
	private String p9083;

	@Column(name = "P908_4")
	private String p9084;

	@Column(name = "P908_5")
	private String p9085;

	@Column(name = "P908_6")
	private String p9086;

	@Column(name = "P908_ESP")
	private String p908Esp;

	@Column(name = "P909")
	private String p909;

	@Column(name = "P910_1")
	private String p9101;
	@Column(name = "P910_2")
	private String p9102;
	@Column(name = "P910_3")
	private String p9103;
	@Column(name = "P910_4")
	private String p9104;
	@Column(name = "P910_5")
	private String p9105;
	@Column(name = "P910_6")
	private String p9106;
	@Column(name = "P910_7")
	private String p9107;
	@Column(name = "P910_8")
	private String p9108;
	@Column(name = "P910_ESP")
	private String p910Esp;
	@Column(name = "ANOTACIONES")
	private String anotaciones;
	@Column(name = "VERSION")
	private String version;
	@Column(name = "TIPO_ENVIO")
	private String tipoEnvio;
	@Column(name = "ULTIMO")
	private Boolean ultimo;
	@Column(name = "FECHA_ENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec800> local2019Sec800List;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "local2019Cabecera", fetch = FetchType.LAZY) // EAGER
	private List<Local2019Resp> local2019RespList;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec206> local2019Sec206List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // LAZY
	private List<Local2019Sec700> local2019Sec700List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec500> local2019Sec500List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec207> local2019Sec207List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec400> local2019Sec400List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec300> local2019Sec300List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec405> local2019Sec405List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec611> local2019Sec611List;

	@OneToMany(mappedBy = "local2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER
	private List<Local2019Sec112> local2019Sec112List;

	// fin
	@Transient
	private long token;
	@Transient
	private String msg;
	@Transient
	private String estadoRpt;

	public Local2019Cabecera() {
	}

	public Local2019Cabecera(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "CODLOCAL")
	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	@XmlElement(name = "TIPOLOCAL")
	public String getTipolocal() {
		return tipolocal;
	}

	@XmlElement(name = "TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	@XmlElement(name = "CODGEO")
	public String getCodgeo() {
		return codgeo;
	}

	public void setCodgeo(String codgeo) {
		this.codgeo = codgeo;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setTipolocal(String tipolocal) {
		this.tipolocal = tipolocal;
	}

	@XmlElement(name = "DPTO")
	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	@XmlElement(name = "PROV")
	public String getProv() {
		return prov;
	}

	public void setProv(String prov) {
		this.prov = prov;
	}

	@XmlElement(name = "DISTRITO")
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@XmlElement(name = "CODUGEL")
	public String getCodugel() {
		return codugel;
	}

	public void setCodugel(String codugel) {
		this.codugel = codugel;
	}

	@XmlElement(name = "CEN_POB")
	public String getCenPob() {
		return cenPob;
	}

	public void setCenPob(String cenPob) {
		this.cenPob = cenPob;
	}

	@XmlElement(name = "CATEG_CP")
	public String getCategCp() {
		return categCp;
	}

	public void setCategCp(String categCp) {
		this.categCp = categCp;
	}

	@XmlElement(name = "NOMLUGAR")
	public String getNomlugar() {
		return nomlugar;
	}

	public void setNomlugar(String nomlugar) {
		this.nomlugar = nomlugar;
	}

	@XmlElement(name = "CATEGORIA")
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@XmlElement(name = "TIPOVIA")
	public String getTipovia() {
		return tipovia;
	}

	public void setTipovia(String tipovia) {
		this.tipovia = tipovia;
	}

	@XmlElement(name = "NOMBVIA")
	public String getNombvia() {
		return nombvia;
	}

	public void setNombvia(String nombvia) {
		this.nombvia = nombvia;
	}

	@XmlElement(name = "NUMERO")
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@XmlElement(name = "MANZANA")
	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	@XmlElement(name = "LOTE")
	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	@XmlElement(name = "SECTOR")
	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	@XmlElement(name = "ZONA")
	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	@XmlElement(name = "ETAPA")
	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	@XmlElement(name = "OTROS")
	public String getOtros() {
		return otros;
	}

	public void setOtros(String otros) {
		this.otros = otros;
	}

	@XmlElement(name = "REFERENCIA")
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	@XmlElement(name = "P101")
	public String getP101() {
		return p101;
	}

	public void setP101(String p101) {
		this.p101 = p101;
	}

	@XmlElement(name = "P101_ESP")
	public String getP101Esp() {
		return p101Esp;
	}

	public void setP101Esp(String p101Esp) {
		this.p101Esp = p101Esp;
	}

	@XmlElement(name = "P102")
	public String getP102() {
		return p102;
	}

	public void setP102(String p102) {
		this.p102 = p102;
	}

	@XmlElement(name = "P102_ESP")
	public String getP102Esp() {
		return p102Esp;
	}

	public void setP102Esp(String p102Esp) {
		this.p102Esp = p102Esp;
	}

	@XmlElement(name = "P103")
	public String getP103() {
		return p103;
	}

	public void setP103(String p103) {
		this.p103 = p103;
	}

	@XmlElement(name = "P103_FREG")
	public String getP103Freg() {
		return p103Freg;
	}

	public void setP103Freg(String p103Freg) {
		this.p103Freg = p103Freg;
	}

	@XmlElement(name = "P103_OREG")
	public String getP103Oreg() {
		return p103Oreg;
	}

	public void setP103Oreg(String p103Oreg) {
		this.p103Oreg = p103Oreg;
	}

	@XmlElement(name = "P104")
	public String getP104() {
		return p104;
	}

	public void setP104(String p104) {
		this.p104 = p104;
	}

	@XmlElement(name = "P104_EST")
	public String getP104Est() {
		return p104Est;
	}

	public void setP104Est(String p104Est) {
		this.p104Est = p104Est;
	}

	@XmlElement(name = "P104_TER")
	public String getP104Ter() {
		return p104Ter;
	}

	public void setP104Ter(String p104Ter) {
		this.p104Ter = p104Ter;
	}

	@XmlElement(name = "P105")
	public String getP105() {
		return p105;
	}

	public void setP105(String p105) {
		this.p105 = p105;
	}

	@XmlElement(name = "P106")
	public String getP106() {
		return p106;
	}

	public void setP106(String p106) {
		this.p106 = p106;
	}

	@XmlElement(name = "P106_ESP")
	public String getP106Esp() {
		return p106Esp;
	}

	public void setP106Esp(String p106Esp) {
		this.p106Esp = p106Esp;
	}

	@XmlElement(name = "P108")
	public String getP108() {
		return p108;
	}

	public void setP108(String p108) {
		this.p108 = p108;
	}

	@XmlElement(name = "P109")
	public String getP109() {
		return p109;
	}

	public void setP109(String p109) {
		this.p109 = p109;
	}

	@XmlElement(name = "P111_1")
	public String getP1111() {
		return p1111;
	}

	public void setP1111(String p1111) {
		this.p1111 = p1111;
	}

	@XmlElement(name = "P202")
	public String getP202() {
		return p202;
	}

	public void setP202(String p202) {
		this.p202 = p202;
	}

	@XmlElement(name = "P702")
	public String getP702() {
		return p702;
	}

	public void setP702(String p702) {
		this.p702 = p702;
	}

	@XmlElement(name = "P703_ESP")
	public String getP703Esp() {
		return p703Esp;
	}

	public void setP703Esp(String p703Esp) {
		this.p703Esp = p703Esp;
	}

	@XmlElement(name = "P704")
	public String getP704() {
		return p704;
	}

	public void setP704(String p704) {
		this.p704 = p704;
	}

	@XmlElement(name = "ANOTACIONES")
	public String getAnotaciones() {
		return anotaciones;
	}

	public void setAnotaciones(String anotaciones) {
		this.anotaciones = anotaciones;
	}

	@XmlElement(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name = "TIPO_ENVIO")
	public String getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(String tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	@XmlElement(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	@XmlElement(name = "RESPONSABLE")
	public List<Local2019Resp> getLocal2019RespList() {
		return local2019RespList;
	}

	public void setLocal2019RespList(List<Local2019Resp> local2019RespList) {
		this.local2019RespList = local2019RespList;
	}

	@XmlElement(name = "SEC112")
	public List<Local2019Sec112> getLocal2019Sec112List() {
		return local2019Sec112List;
	}

	public void setLocal2019Sec112List(List<Local2019Sec112> local2019Sec112List) {
		this.local2019Sec112List = local2019Sec112List;
	}

	@XmlElement(name = "SEC206")
	public List<Local2019Sec206> getLocal2019Sec206List() {
		return local2019Sec206List;
	}

	public void setLocal2019Sec206List(List<Local2019Sec206> local2019Sec206List) {
		this.local2019Sec206List = local2019Sec206List;
	}

	@XmlElement(name = "SEC207")
	public List<Local2019Sec207> getLocal2019Sec207List() {
		return local2019Sec207List;
	}

	public void setLocal2019Sec207List(List<Local2019Sec207> local2019Sec207List) {
		this.local2019Sec207List = local2019Sec207List;
	}

	@XmlElement(name = "SEC400")
	public List<Local2019Sec400> getLocal2019Sec400List() {
		return local2019Sec400List;
	}

	public void setLocal2019Sec400List(List<Local2019Sec400> local2019Sec400List) {
		this.local2019Sec400List = local2019Sec400List;
	}

	@XmlElement(name = "SEC500")
	public List<Local2019Sec500> getLocal2019Sec500List() {
		return local2019Sec500List;
	}

	public void setLocal2019Sec500List(List<Local2019Sec500> local2019Sec500List) {
		this.local2019Sec500List = local2019Sec500List;
	}

	@XmlElement(name = "SEC611")
	public List<Local2019Sec611> getLocal2019Sec611List() {
		return local2019Sec611List;
	}

	public void setLocal2019Sec611List(List<Local2019Sec611> local2019Sec611List) {
		this.local2019Sec611List = local2019Sec611List;
	}

	@XmlElement(name = "SEC800")
	public List<Local2019Sec800> getLocal2019Sec800List() {
		return local2019Sec800List;
	}

	public void setLocal2019Sec800List(List<Local2019Sec800> local2019Sec800List) {
		this.local2019Sec800List = local2019Sec800List;
	}

	@XmlElement(name = "SEC700")
	public List<Local2019Sec700> getLocal2019Sec700List() {
		return local2019Sec700List;
	}

	public void setLocal2019Sec700List(List<Local2019Sec700> local2019Sec700List) {
		this.local2019Sec700List = local2019Sec700List;
	}

	@XmlElement(name = "SEC405")
	public List<Local2019Sec405> getLocal2019Sec405List() {
		return local2019Sec405List;
	}

	public void setLocal2019Sec405List(List<Local2019Sec405> local2019Sec405List) {
		this.local2019Sec405List = local2019Sec405List;
	}

	@XmlElement(name = "SEC300")
	public List<Local2019Sec300> getLocal2019Sec300List() {
		return local2019Sec300List;
	}

	public void setLocal2019Sec300List(List<Local2019Sec300> local2019Sec300List) {
		this.local2019Sec300List = local2019Sec300List;
	}

	@XmlAttribute(name = "ESTADO_RPT")
	public String getEstadoRpt() {
		return estadoRpt;
	}

	public void setEstadoRpt(String estadoRpt) {
		this.estadoRpt = estadoRpt;
	}

	@XmlAttribute(name = "MSG")
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@XmlAttribute
	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	/////////////////////////////////////////////////////////////////////////////////////////

	@XmlElement(name = "P103_AREAE")
	public Integer getP103Areae() {
		return p103Areae;
	}

	public void setP103Areae(Integer p103Areae) {
		this.p103Areae = p103Areae;
	}

	@XmlElement(name = "P103_AREAD")
	public Integer getP103Aread() {
		return p103Aread;
	}

	public void setP103Aread(Integer p103Aread) {
		this.p103Aread = p103Aread;
	}

	@XmlElement(name = "P107")
	public String getP107() {
		return p107;
	}

	public void setP107(String p107) {
		this.p107 = p107;
	}

	@XmlElement(name = "P107_ITSE")
	public String getP107Itse() {
		return p107Itse;
	}

	public void setP107Itse(String p107Itse) {
		this.p107Itse = p107Itse;
	}

	@XmlElement(name = "P109_SI")
	public String getP109Si() {
		return p109Si;
	}

	public void setP109Si(String p109Si) {
		this.p109Si = p109Si;
	}

	@XmlElement(name = "P110_1")
	public String getP1101() {
		return p1101;
	}

	public void setP1101(String p1101) {
		this.p1101 = p1101;
	}

	@XmlElement(name = "P110_2")
	public String getP1102() {
		return p1102;
	}

	public void setP1102(String p1102) {
		this.p1102 = p1102;
	}

	@XmlElement(name = "P110_3")
	public String getP1103() {
		return p1103;
	}

	public void setP1103(String p1103) {
		this.p1103 = p1103;
	}

	@XmlElement(name = "P110_4")
	public String getP1104() {
		return p1104;
	}

	public void setP1104(String p1104) {
		this.p1104 = p1104;
	}

	@XmlElement(name = "P110_5")
	public String getP1105() {
		return p1105;
	}

	public void setP1105(String p1105) {
		this.p1105 = p1105;
	}

	@XmlElement(name = "P110_6")
	public String getP1106() {
		return p1106;
	}

	public void setP1106(String p1106) {
		this.p1106 = p1106;
	}

	@XmlElement(name = "P110_ESP")
	public String getP110Esp() {
		return p110Esp;
	}

	public void setP110Esp(String p110Esp) {
		this.p110Esp = p110Esp;
	}

	@XmlElement(name = "P111_ESP")
	public String getP111Esp() {
		return p111Esp;
	}

	public void setP111Esp(String p111Esp) {
		this.p111Esp = p111Esp;
	}

	@XmlElement(name = "P113_ATAE")
	public Integer getP113Atae() {
		return p113Atae;
	}

	public void setP113Atae(Integer p113Atae) {
		this.p113Atae = p113Atae;
	}

	@XmlElement(name = "P113_ATAD")
	public Integer getP113Atad() {
		return p113Atad;
	}

	public void setP113Atad(Integer p113Atad) {
		this.p113Atad = p113Atad;
	}

	@XmlElement(name = "P113_ACE")
	public Integer getP113Ace() {
		return p113Ace;
	}

	public void setP113Ace(Integer p113Ace) {
		this.p113Ace = p113Ace;
	}

	@XmlElement(name = "P113_ACD")
	public Integer getP113Acd() {
		return p113Acd;
	}

	public void setP113Acd(Integer p113Acd) {
		this.p113Acd = p113Acd;
	}

	@XmlElement(name = "P203")
	public String getP203() {
		return p203;
	}

	public void setP203(String p203) {
		this.p203 = p203;
	}

	@XmlElement(name = "P203_ESP")
	public String getP203Esp() {
		return p203Esp;
	}

	public void setP203Esp(String p203Esp) {
		this.p203Esp = p203Esp;
	}

	@XmlElement(name = "P204")
	public String getP204() {
		return p204;
	}

	public void setP204(String p204) {
		this.p204 = p204;
	}

	@XmlElement(name = "P204_SI")
	public String getP204Si() {
		return p204Si;
	}

	public void setP204Si(String p204Si) {
		this.p204Si = p204Si;
	}

	@XmlElement(name = "P205_TPT")
	public Integer getP205Tpt() {
		return p205Tpt;
	}

	public void setP205Tpt(Integer p205Tpt) {
		this.p205Tpt = p205Tpt;
	}

	@XmlElement(name = "P207_QEDIF")
	public Integer getP207Qedif() {
		return p207Qedif;
	}

	public void setP207Qedif(Integer p207Qedif) {
		this.p207Qedif = p207Qedif;
	}

	@XmlElement(name = "P301_QAULA")
	public Integer getP301Qaula() {
		return p301Qaula;
	}

	public void setP301Qaula(Integer p301Qaula) {
		this.p301Qaula = p301Qaula;
	}

	@XmlElement(name = "P601")
	public String getP601() {
		return p601;
	}

	public void setP601(String p601) {
		this.p601 = p601;
	}

	@XmlElement(name = "P601_SI")
	public String getP601Si() {
		return p601Si;
	}

	public void setP601Si(String p601Si) {
		this.p601Si = p601Si;
	}

	@XmlElement(name = "P602_1")
	public Integer getP6021() {
		return p6021;
	}

	public void setP6021(Integer p6021) {
		this.p6021 = p6021;
	}

	@XmlElement(name = "P602_2")
	public Integer getP6022() {
		return p6022;
	}

	public void setP6022(Integer p6022) {
		this.p6022 = p6022;
	}

	@XmlElement(name = "P603")
	public String getP603() {
		return p603;
	}

	public void setP603(String p603) {
		this.p603 = p603;
	}

	@XmlElement(name = "P604_1")
	public String getP6041() {
		return p6041;
	}

	public void setP6041(String p6041) {
		this.p6041 = p6041;
	}

	@XmlElement(name = "P604_2")
	public String getP6042() {
		return p6042;
	}

	public void setP6042(String p6042) {
		this.p6042 = p6042;
	}

	@XmlElement(name = "P604_3")
	public String getP6043() {
		return p6043;
	}

	public void setP6043(String p6043) {
		this.p6043 = p6043;
	}

	@XmlElement(name = "P604_4")
	public String getP6044() {
		return p6044;
	}

	public void setP6044(String p6044) {
		this.p6044 = p6044;
	}

	@XmlElement(name = "P604_5")
	public String getP6045() {
		return p6045;
	}

	public void setP6045(String p6045) {
		this.p6045 = p6045;
	}

	@XmlElement(name = "P604_6")
	public String getP6046() {
		return p6046;
	}

	public void setP6046(String p6046) {
		this.p6046 = p6046;
	}

	@XmlElement(name = "P604_7")
	public String getP6047() {
		return p6047;
	}

	public void setP6047(String p6047) {
		this.p6047 = p6047;
	}

	@XmlElement(name = "P604_8")
	public String getP6048() {
		return p6048;
	}

	public void setP6048(String p6048) {
		this.p6048 = p6048;
	}

	@XmlElement(name = "P604_9")
	public String getP6049() {
		return p6049;
	}

	public void setP6049(String p6049) {
		this.p6049 = p6049;
	}

	@XmlElement(name = "P604_10")
	public String getP60410() {
		return p60410;
	}

	public void setP60410(String p60410) {
		this.p60410 = p60410;
	}

	@XmlElement(name = "P604_11")
	public String getP60411() {
		return p60411;
	}

	public void setP60411(String p60411) {
		this.p60411 = p60411;
	}

	@XmlElement(name = "P604_12")
	public String getP60412() {
		return p60412;
	}

	public void setP60412(String p60412) {
		this.p60412 = p60412;
	}

	@XmlElement(name = "P604_13")
	public String getP60413() {
		return p60413;
	}

	public void setP60413(String p60413) {
		this.p60413 = p60413;
	}

	@XmlElement(name = "P604_14")
	public String getP60414() {
		return p60414;
	}

	public void setP60414(String p60414) {
		this.p60414 = p60414;
	}

	@XmlElement(name = "P605")
	public String getP605() {
		return p605;
	}

	public void setP605(String p605) {
		this.p605 = p605;
	}

	@XmlElement(name = "P606")
	public String getP606() {
		return p606;
	}

	public void setP606(String p606) {
		this.p606 = p606;
	}

	@XmlElement(name = "P607")
	public String getP607() {
		return p607;
	}

	public void setP607(String p607) {
		this.p607 = p607;
	}

	@XmlElement(name = "P607_NO")
	public String getP607No() {
		return p607No;
	}

	public void setP607No(String p607No) {
		this.p607No = p607No;
	}

	@XmlElement(name = "P607_ESP")
	public String getP607Esp() {
		return p607Esp;
	}

	public void setP607Esp(String p607Esp) {
		this.p607Esp = p607Esp;
	}

	@XmlElement(name = "P608_1")
	public String getP6081() {
		return p6081;
	}

	public void setP6081(String p6081) {
		this.p6081 = p6081;
	}

	@XmlElement(name = "P608_2")
	public String getP6082() {
		return p6082;
	}

	public void setP6082(String p6082) {
		this.p6082 = p6082;
	}

	@XmlElement(name = "P608_3")
	public String getP6083() {
		return p6083;
	}

	public void setP6083(String p6083) {
		this.p6083 = p6083;
	}

	@XmlElement(name = "P608_4")
	public String getP6084() {
		return p6084;
	}

	public void setP6084(String p6084) {
		this.p6084 = p6084;
	}

	@XmlElement(name = "P608_5")
	public String getP6085() {
		return p6085;
	}

	public void setP6085(String p6085) {
		this.p6085 = p6085;
	}

	@XmlElement(name = "P608_6")
	public String getP6086() {
		return p6086;
	}

	public void setP6086(String p6086) {
		this.p6086 = p6086;
	}

	@XmlElement(name = "P608_7")
	public String getP6087() {
		return p6087;
	}

	public void setP6087(String p6087) {
		this.p6087 = p6087;
	}

	@XmlElement(name = "P608_8")
	public String getP6088() {
		return p6088;
	}

	public void setP6088(String p6088) {
		this.p6088 = p6088;
	}

	@XmlElement(name = "P608_9")
	public String getP6089() {
		return p6089;
	}

	public void setP6089(String p6089) {
		this.p6089 = p6089;
	}

	@XmlElement(name = "P609")
	public String getP609() {
		return p609;
	}

	public void setP609(String p609) {
		this.p609 = p609;
	}

	@XmlElement(name = "P610_1")
	public String getP6101() {
		return p6101;
	}

	public void setP6101(String p6101) {
		this.p6101 = p6101;
	}

	@XmlElement(name = "P610_2")
	public String getP6102() {
		return p6102;
	}

	public void setP6102(String p6102) {
		this.p6102 = p6102;
	}

	@XmlElement(name = "P610_3")
	public String getP6103() {
		return p6103;
	}

	public void setP6103(String p6103) {
		this.p6103 = p6103;
	}

	@XmlElement(name = "P610_4")
	public String getP6104() {
		return p6104;
	}

	public void setP6104(String p6104) {
		this.p6104 = p6104;
	}

	@XmlElement(name = "P610_5")
	public String getP6105() {
		return p6105;
	}

	public void setP6105(String p6105) {
		this.p6105 = p6105;
	}

	@XmlElement(name = "P610_6")
	public String getP6106() {
		return p6106;
	}

	public void setP6106(String p6106) {
		this.p6106 = p6106;
	}

	@XmlElement(name = "P610_7")
	public String getP6107() {
		return p6107;
	}

	public void setP6107(String p6107) {
		this.p6107 = p6107;
	}

	@XmlElement(name = "P703")
	public String getP703() {
		return p703;
	}

	public void setP703(String p703) {
		this.p703 = p703;
	}

	@XmlElement(name = "P704_ESP")
	public String getP704Esp() {
		return p704Esp;
	}

	public void setP704Esp(String p704Esp) {
		this.p704Esp = p704Esp;
	}

	@XmlElement(name = "P706")
	public String getP706() {
		return p706;
	}

	public void setP706(String p706) {
		this.p706 = p706;
	}

	@XmlElement(name = "P707_1")
	public String getP7071() {
		return p7071;
	}

	public void setP7071(String p7071) {
		this.p7071 = p7071;
	}

	@XmlElement(name = "P707_2")
	public String getP7072() {
		return p7072;
	}

	public void setP7072(String p7072) {
		this.p7072 = p7072;
	}

	@XmlElement(name = "P707_3")
	public String getP7073() {
		return p7073;
	}

	public void setP7073(String p7073) {
		this.p7073 = p7073;
	}

	@XmlElement(name = "P707_4")
	public String getP7074() {
		return p7074;
	}

	public void setP7074(String p7074) {
		this.p7074 = p7074;
	}

	@XmlElement(name = "P707_5")
	public String getP7075() {
		return p7075;
	}

	public void setP7075(String p7075) {
		this.p7075 = p7075;
	}

	@XmlElement(name = "P707_6")
	public String getP7076() {
		return p7076;
	}

	public void setP7076(String p7076) {
		this.p7076 = p7076;
	}

	@XmlElement(name = "P707_7")
	public String getP7077() {
		return p7077;
	}

	public void setP7077(String p7077) {
		this.p7077 = p7077;
	}

	@XmlElement(name = "P707_8")
	public String getP7078() {
		return p7078;
	}

	public void setP7078(String p7078) {
		this.p7078 = p7078;
	}

	@XmlElement(name = "P707_9")
	public String getP7079() {
		return p7079;
	}

	public void setP7079(String p7079) {
		this.p7079 = p7079;
	}

	@XmlElement(name = "P707_10")
	public String getP70710() {
		return p70710;
	}

	public void setP70710(String p70710) {
		this.p70710 = p70710;
	}

	@XmlElement(name = "P707_11")
	public String getP70711() {
		return p70711;
	}

	public void setP70711(String p70711) {
		this.p70711 = p70711;
	}

	@XmlElement(name = "P707_11NT")
	public String getP70711nt() {
		return p70711nt;
	}

	public void setP70711nt(String p70711nt) {
		this.p70711nt = p70711nt;
	}

	@XmlElement(name = "P708")
	public String getP708() {
		return p708;
	}

	public void setP708(String p708) {
		this.p708 = p708;
	}

	@XmlElement(name = "P708_PROV")
	public String getP708Prov() {
		return p708Prov;
	}

	public void setP708Prov(String p708Prov) {
		this.p708Prov = p708Prov;
	}

	@XmlElement(name = "P708_NSUM")
	public String getP708Nsum() {
		return p708Nsum;
	}

	public void setP708Nsum(String p708Nsum) {
		this.p708Nsum = p708Nsum;
	}

	@XmlElement(name = "P709")
	public String getP709() {
		return p709;
	}

	public void setP709(String p709) {
		this.p709 = p709;
	}

	@XmlElement(name = "P709_NO")
	public Integer getP709No() {
		return p709No;
	}

	public void setP709No(Integer p709No) {
		this.p709No = p709No;
	}

	@XmlElement(name = "P709_NO1")
	public String getP709No1() {
		return p709No1;
	}

	public void setP709No1(String p709No1) {
		this.p709No1 = p709No1;
	}

	@XmlElement(name = "P709_NO2")
	public String getP709No2() {
		return p709No2;
	}

	public void setP709No2(String p709No2) {
		this.p709No2 = p709No2;
	}

	@XmlElement(name = "P709_NO3")
	public String getP709No3() {
		return p709No3;
	}

	public void setP709No3(String p709No3) {
		this.p709No3 = p709No3;
	}

	@XmlElement(name = "P710")
	public String getP710() {
		return p710;
	}

	public void setP710(String p710) {
		this.p710 = p710;
	}

	@XmlElement(name = "P710_ESP")
	public String getP710Esp() {
		return p710Esp;
	}

	public void setP710Esp(String p710Esp) {
		this.p710Esp = p710Esp;
	}

	@XmlElement(name = "P711")
	public String getP711() {
		return p711;
	}

	public void setP711(String p711) {
		this.p711 = p711;
	}

	@XmlElement(name = "P711_ESP")
	public String getP711Esp() {
		return p711Esp;
	}

	public void setP711Esp(String p711Esp) {
		this.p711Esp = p711Esp;
	}

	@XmlElement(name = "P711_PROV")
	public String getP711Prov() {
		return p711Prov;
	}

	public void setP711Prov(String p711Prov) {
		this.p711Prov = p711Prov;
	}

	@XmlElement(name = "P711_NSUM")
	public String getP711Nsum() {
		return p711Nsum;
	}

	public void setP711Nsum(String p711Nsum) {
		this.p711Nsum = p711Nsum;
	}

	@XmlElement(name = "P712")
	public String getP712() {
		return p712;
	}

	public void setP712(String p712) {
		this.p712 = p712;
	}

	@XmlElement(name = "P712_NO")
	public Integer getP712No() {
		return p712No;
	}

	public void setP712No(Integer p712No) {
		this.p712No = p712No;
	}

	@XmlElement(name = "P712_NO1")
	public String getP712No1() {
		return p712No1;
	}

	public void setP712No1(String p712No1) {
		this.p712No1 = p712No1;
	}

	@XmlElement(name = "P712_NO2")
	public String getP712No2() {
		return p712No2;
	}

	public void setP712No2(String p712No2) {
		this.p712No2 = p712No2;
	}

	@XmlElement(name = "P712_NO3")
	public String getP712No3() {
		return p712No3;
	}

	public void setP712No3(String p712No3) {
		this.p712No3 = p712No3;
	}

	@XmlElement(name = "P713")
	public String getP713() {
		return p713;
	}

	public void setP713(String p713) {
		this.p713 = p713;
	}

	@XmlElement(name = "P713_ESP")
	public String getP713Esp() {
		return p713Esp;
	}

	public void setP713Esp(String p713Esp) {
		this.p713Esp = p713Esp;
	}

	@XmlElement(name = "P714_11")
	public String getP71411() {
		return p71411;
	}

	public void setP71411(String p71411) {
		this.p71411 = p71411;
	}

	@XmlElement(name = "P714_12")
	public String getP71412() {
		return p71412;
	}

	public void setP71412(String p71412) {
		this.p71412 = p71412;
	}

	@XmlElement(name = "P714_13")
	public String getP71413() {
		return p71413;
	}

	public void setP71413(String p71413) {
		this.p71413 = p71413;
	}

	@XmlElement(name = "P714_21")
	public String getP71421() {
		return p71421;
	}

	public void setP71421(String p71421) {
		this.p71421 = p71421;
	}

	@XmlElement(name = "P714_22")
	public String getP71422() {
		return p71422;
	}

	public void setP71422(String p71422) {
		this.p71422 = p71422;
	}

	@XmlElement(name = "P714_23")
	public String getP71423() {
		return p71423;
	}

	public void setP71423(String p71423) {
		this.p71423 = p71423;
	}

	@XmlElement(name = "P714_31")
	public String getP71431() {
		return p71431;
	}

	public void setP71431(String p71431) {
		this.p71431 = p71431;
	}

	@XmlElement(name = "P714_32")
	public String getP71432() {
		return p71432;
	}

	public void setP71432(String p71432) {
		this.p71432 = p71432;
	}

	@XmlElement(name = "P714_33")
	public String getP71433() {
		return p71433;
	}

	public void setP71433(String p71433) {
		this.p71433 = p71433;
	}

	@XmlElement(name = "P715")
	public String getP715() {
		return p715;
	}

	public void setP715(String p715) {
		this.p715 = p715;
	}

	@XmlElement(name = "P716")
	public String getP716() {
		return p716;
	}

	public void setP716(String p716) {
		this.p716 = p716;
	}

	@XmlElement(name = "P717")
	public String getP717() {
		return p717;
	}

	public void setP717(String p717) {
		this.p717 = p717;
	}

	@XmlElement(name = "P717_SIQN")
	public Integer getP717Siqn() {
		return p717Siqn;
	}

	public void setP717Siqn(Integer p717Siqn) {
		this.p717Siqn = p717Siqn;
	}

	@XmlElement(name = "P718_QN1")
	public Integer getP718Qn1() {
		return p718Qn1;
	}

	public void setP718Qn1(Integer p718Qn1) {
		this.p718Qn1 = p718Qn1;
	}

	@XmlElement(name = "P718_QN2")
	public Integer getP718Qn2() {
		return p718Qn2;
	}

	public void setP718Qn2(Integer p718Qn2) {
		this.p718Qn2 = p718Qn2;
	}

	@XmlElement(name = "P718_QN3")
	public Integer getP718Qn3() {
		return p718Qn3;
	}

	public void setP718Qn3(Integer p718Qn3) {
		this.p718Qn3 = p718Qn3;
	}

	@XmlElement(name = "P901_1")
	public String getP9011() {
		return p9011;
	}

	public void setP9011(String p9011) {
		this.p9011 = p9011;
	}

	@XmlElement(name = "P901_2")
	public String getP9012() {
		return p9012;
	}

	public void setP9012(String p9012) {
		this.p9012 = p9012;
	}

	@XmlElement(name = "P901_3")
	public String getP9013() {
		return p9013;
	}

	public void setP9013(String p9013) {
		this.p9013 = p9013;
	}

	@XmlElement(name = "P901_4")
	public String getP9014() {
		return p9014;
	}

	public void setP9014(String p9014) {
		this.p9014 = p9014;
	}

	@XmlElement(name = "P901_5")
	public String getP9015() {
		return p9015;
	}

	public void setP9015(String p9015) {
		this.p9015 = p9015;
	}

	@XmlElement(name = "P901_6")
	public String getP9016() {
		return p9016;
	}

	public void setP9016(String p9016) {
		this.p9016 = p9016;
	}

	@XmlElement(name = "P901_7")
	public String getP9017() {
		return p9017;
	}

	public void setP9017(String p9017) {
		this.p9017 = p9017;
	}

	@XmlElement(name = "P901_8")
	public String getP9018() {
		return p9018;
	}

	public void setP9018(String p9018) {
		this.p9018 = p9018;
	}

	@XmlElement(name = "P901_9")
	public String getP9019() {
		return p9019;
	}

	public void setP9019(String p9019) {
		this.p9019 = p9019;
	}

	@XmlElement(name = "P901_10")
	public String getP90110() {
		return p90110;
	}

	public void setP90110(String p90110) {
		this.p90110 = p90110;
	}

	@XmlElement(name = "P901_11")
	public String getP90111() {
		return p90111;
	}

	public void setP90111(String p90111) {
		this.p90111 = p90111;
	}

	@XmlElement(name = "P901_12")
	public String getP90112() {
		return p90112;
	}

	public void setP90112(String p90112) {
		this.p90112 = p90112;
	}

	@XmlElement(name = "P901_13")
	public String getP90113() {
		return p90113;
	}

	public void setP90113(String p90113) {
		this.p90113 = p90113;
	}

	@XmlElement(name = "P901_14")
	public String getP90114() {
		return p90114;
	}

	public void setP90114(String p90114) {
		this.p90114 = p90114;
	}

	@XmlElement(name = "P901_15")
	public String getP90115() {
		return p90115;
	}

	public void setP90115(String p90115) {
		this.p90115 = p90115;
	}

	@XmlElement(name = "P901_16")
	public String getP90116() {
		return p90116;
	}

	public void setP90116(String p90116) {
		this.p90116 = p90116;
	}

	@XmlElement(name = "P901_17")
	public String getP90117() {
		return p90117;
	}

	public void setP90117(String p90117) {
		this.p90117 = p90117;
	}

	@XmlElement(name = "P901_18")
	public String getP90118() {
		return p90118;
	}

	public void setP90118(String p90118) {
		this.p90118 = p90118;
	}

	@XmlElement(name = "P901_19")
	public String getP90119() {
		return p90119;
	}

	public void setP90119(String p90119) {
		this.p90119 = p90119;
	}

	@XmlElement(name = "P901_20")
	public String getP90120() {
		return p90120;
	}

	public void setP90120(String p90120) {
		this.p90120 = p90120;
	}

	@XmlElement(name = "P901_ESP")
	public String getP901Esp() {
		return p901Esp;
	}

	public void setP901Esp(String p901Esp) {
		this.p901Esp = p901Esp;
	}

	@XmlElement(name = "P902")
	public String getP902() {
		return p902;
	}

	public void setP902(String p902) {
		this.p902 = p902;
	}

	@XmlElement(name = "P903")
	public String getP903() {
		return p903;
	}

	public void setP903(String p903) {
		this.p903 = p903;
	}

	@XmlElement(name = "P904")
	public String getP904() {
		return p904;
	}

	public void setP904(String p904) {
		this.p904 = p904;
	}

	@XmlElement(name = "P904_SI1")
	public String getP904Si1() {
		return p904Si1;
	}

	public void setP904Si1(String p904Si1) {
		this.p904Si1 = p904Si1;
	}

	@XmlElement(name = "P904_SI2")
	public String getP904Si2() {
		return p904Si2;
	}

	public void setP904Si2(String p904Si2) {
		this.p904Si2 = p904Si2;
	}

	@XmlElement(name = "P904_SI3")
	public String getP904Si3() {
		return p904Si3;
	}

	public void setP904Si3(String p904Si3) {
		this.p904Si3 = p904Si3;
	}

	@XmlElement(name = "P904_SI4")
	public String getP904Si4() {
		return p904Si4;
	}

	public void setP904Si4(String p904Si4) {
		this.p904Si4 = p904Si4;
	}

	@XmlElement(name = "P904_SI5")
	public String getP904Si5() {
		return p904Si5;
	}

	public void setP904Si5(String p904Si5) {
		this.p904Si5 = p904Si5;
	}

	@XmlElement(name = "P904_SI6")
	public String getP904Si6() {
		return p904Si6;
	}

	public void setP904Si6(String p904Si6) {
		this.p904Si6 = p904Si6;
	}

	@XmlElement(name = "P904_SIE")
	public String getP904Sie() {
		return p904Sie;
	}

	public void setP904Sie(String p904Sie) {
		this.p904Sie = p904Sie;
	}

	@XmlElement(name = "P905_1")
	public String getP9051() {
		return p9051;
	}

	public void setP9051(String p9051) {
		this.p9051 = p9051;
	}

	@XmlElement(name = "P905_11")
	public String getP90511() {
		return p90511;
	}

	public void setP90511(String p90511) {
		this.p90511 = p90511;
	}

	@XmlElement(name = "P905_2")
	public String getP9052() {
		return p9052;
	}

	public void setP9052(String p9052) {
		this.p9052 = p9052;
	}

	@XmlElement(name = "P905_21")
	public String getP90521() {
		return p90521;
	}

	public void setP90521(String p90521) {
		this.p90521 = p90521;
	}

	@XmlElement(name = "P905_3")
	public String getP9053() {
		return p9053;
	}

	public void setP9053(String p9053) {
		this.p9053 = p9053;
	}

	@XmlElement(name = "P905_31")
	public String getP90531() {
		return p90531;
	}

	public void setP90531(String p90531) {
		this.p90531 = p90531;
	}

	@XmlElement(name = "P905_4")
	public String getP9054() {
		return p9054;
	}

	public void setP9054(String p9054) {
		this.p9054 = p9054;
	}

	@XmlElement(name = "P905_41")
	public String getP90541() {
		return p90541;
	}

	public void setP90541(String p90541) {
		this.p90541 = p90541;
	}

	@XmlElement(name = "P905_5")
	public String getP9055() {
		return p9055;
	}

	public void setP9055(String p9055) {
		this.p9055 = p9055;
	}

	@XmlElement(name = "P905_51")
	public String getP90551() {
		return p90551;
	}

	public void setP90551(String p90551) {
		this.p90551 = p90551;
	}

	@XmlElement(name = "P906")
	public String getP906() {
		return p906;
	}

	public void setP906(String p906) {
		this.p906 = p906;
	}

	@XmlElement(name = "P906_SI1")
	public String getP906Si1() {
		return p906Si1;
	}

	public void setP906Si1(String p906Si1) {
		this.p906Si1 = p906Si1;
	}

	@XmlElement(name = "P906_SI2")
	public String getP906Si2() {
		return p906Si2;
	}

	public void setP906Si2(String p906Si2) {
		this.p906Si2 = p906Si2;
	}

	@XmlElement(name = "P906_SI3")
	public String getP906Si3() {
		return p906Si3;
	}

	public void setP906Si3(String p906Si3) {
		this.p906Si3 = p906Si3;
	}

	@XmlElement(name = "P906_SI4")
	public String getP906Si4() {
		return p906Si4;
	}

	public void setP906Si4(String p906Si4) {
		this.p906Si4 = p906Si4;
	}

	@XmlElement(name = "P906_SI5")
	public String getP906Si5() {
		return p906Si5;
	}

	public void setP906Si5(String p906Si5) {
		this.p906Si5 = p906Si5;
	}

	@XmlElement(name = "P906_SI6")
	public String getP906Si6() {
		return p906Si6;
	}

	public void setP906Si6(String p906Si6) {
		this.p906Si6 = p906Si6;
	}

	@XmlElement(name = "P906_SIE")
	public String getP906Sie() {
		return p906Sie;
	}

	public void setP906Sie(String p906Sie) {
		this.p906Sie = p906Sie;
	}

	@XmlElement(name = "P907_1")
	public String getP9071() {
		return p9071;
	}

	public void setP9071(String p9071) {
		this.p9071 = p9071;
	}

	@XmlElement(name = "P907_11")
	public String getP90711() {
		return p90711;
	}

	public void setP90711(String p90711) {
		this.p90711 = p90711;
	}

	@XmlElement(name = "P907_2")
	public String getP9072() {
		return p9072;
	}

	public void setP9072(String p9072) {
		this.p9072 = p9072;
	}

	@XmlElement(name = "P907_21")
	public String getP90721() {
		return p90721;
	}

	public void setP90721(String p90721) {
		this.p90721 = p90721;
	}

	@XmlElement(name = "P907_3")
	public String getP9073() {
		return p9073;
	}

	public void setP9073(String p9073) {
		this.p9073 = p9073;
	}

	@XmlElement(name = "P907_31")
	public String getP90731() {
		return p90731;
	}

	public void setP90731(String p90731) {
		this.p90731 = p90731;
	}

	@XmlElement(name = "P907_4")
	public String getP9074() {
		return p9074;
	}

	public void setP9074(String p9074) {
		this.p9074 = p9074;
	}

	@XmlElement(name = "P907_41")
	public String getP90741() {
		return p90741;
	}

	public void setP90741(String p90741) {
		this.p90741 = p90741;
	}

	@XmlElement(name = "P907_5")
	public String getP9075() {
		return p9075;
	}

	public void setP9075(String p9075) {
		this.p9075 = p9075;
	}

	@XmlElement(name = "P907_51")
	public String getP90751() {
		return p90751;
	}

	public void setP90751(String p90751) {
		this.p90751 = p90751;
	}

	@XmlElement(name = "P908_1")
	public String getP9081() {
		return p9081;
	}

	public void setP9081(String p9081) {
		this.p9081 = p9081;
	}

	@XmlElement(name = "P908_2")
	public String getP9082() {
		return p9082;
	}

	public void setP9082(String p9082) {
		this.p9082 = p9082;
	}

	@XmlElement(name = "P908_3")
	public String getP9083() {
		return p9083;
	}

	public void setP9083(String p9083) {
		this.p9083 = p9083;
	}

	@XmlElement(name = "P908_4")
	public String getP9084() {
		return p9084;
	}

	public void setP9084(String p9084) {
		this.p9084 = p9084;
	}

	@XmlElement(name = "P908_5")
	public String getP9085() {
		return p9085;
	}

	public void setP9085(String p9085) {
		this.p9085 = p9085;
	}

	@XmlElement(name = "P908_6")
	public String getP9086() {
		return p9086;
	}

	public void setP9086(String p9086) {
		this.p9086 = p9086;
	}

	@XmlElement(name = "P908_ESP")
	public String getP908Esp() {
		return p908Esp;
	}

	public void setP908Esp(String p908Esp) {
		this.p908Esp = p908Esp;
	}

	@XmlElement(name = "P909")
	public String getP909() {
		return p909;
	}

	public void setP909(String p909) {
		this.p909 = p909;
	}

	@XmlElement(name = "P910_1")
	public String getP9101() {
		return p9101;
	}

	public void setP9101(String p9101) {
		this.p9101 = p9101;
	}

	@XmlElement(name = "P910_2")
	public String getP9102() {
		return p9102;
	}

	public void setP9102(String p9102) {
		this.p9102 = p9102;
	}

	@XmlElement(name = "P910_3")
	public String getP9103() {
		return p9103;
	}

	public void setP9103(String p9103) {
		this.p9103 = p9103;
	}

	@XmlElement(name = "P910_4")
	public String getP9104() {
		return p9104;
	}

	public void setP9104(String p9104) {
		this.p9104 = p9104;
	}

	@XmlElement(name = "P910_5")
	public String getP9105() {
		return p9105;
	}

	public void setP9105(String p9105) {
		this.p9105 = p9105;
	}

	@XmlElement(name = "P910_6")
	public String getP9106() {
		return p9106;
	}

	public void setP9106(String p9106) {
		this.p9106 = p9106;
	}

	@XmlElement(name = "P910_7")
	public String getP9107() {
		return p9107;
	}

	public void setP9107(String p9107) {
		this.p9107 = p9107;
	}

	@XmlElement(name = "P910_8")
	public String getP9108() {
		return p9108;
	}

	public void setP9108(String p9108) {
		this.p9108 = p9108;
	}

	@XmlElement(name = "P910_ESP")
	public String getP910Esp() {
		return p910Esp;
	}

	public void setP910Esp(String p910Esp) {
		this.p910Esp = p910Esp;
	}

	@XmlElement(name = "ULTIMO")
	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	@XmlElement(name = "P111_2")
	public String getP1112() {
		return p1112;
	}

	public void setP1112(String p1112) {
		this.p1112 = p1112;
	}

	@XmlElement(name = "P111_3")
	public String getP1113() {
		return p1113;
	}

	public void setP1113(String p1113) {
		this.p1113 = p1113;
	}

	@XmlElement(name = "P111_4")
	public String getP1114() {
		return p1114;
	}

	public void setP1114(String p1114) {
		this.p1114 = p1114;
	}

	@XmlElement(name = "P111_5")
	public String getP1115() {
		return p1115;
	}

	public void setP1115(String p1115) {
		this.p1115 = p1115;
	}

	@XmlElement(name = "P111_6")
	public String getP1116() {
		return p1116;
	}

	public void setP1116(String p1116) {
		this.p1116 = p1116;
	}

	@XmlElement(name = "P401")
	public Integer getP401() {
		return p401;
	}

	public void setP401(Integer p401) {
		this.p401 = p401;
	}

	@XmlElement(name = "P402")
	public Integer getP402() {
		return p402;
	}

	public void setP402(Integer p402) {
		this.p402 = p402;
	}

	@XmlElement(name = "P403")
	public Integer getP403() {
		return p403;
	}

	public void setP403(Integer p403) {
		this.p403 = p403;
	}

	@XmlElement(name = "P404")
	public Integer getP404() {
		return p404;
	}

	public void setP404(Integer p404) {
		this.p404 = p404;
	}

	@XmlElement(name = "P501")
	public Integer getP501() {
		return p501;
	}

	public void setP501(Integer p501) {
		this.p501 = p501;
	}

	@XmlElement(name = "P502")
	public Integer getP502() {
		return p502;
	}

	public void setP502(Integer p502) {
		this.p502 = p502;
	}

	@XmlElement(name = "P503")
	public Integer getP503() {
		return p503;
	}

	public void setP503(Integer p503) {
		this.p503 = p503;
	}

	@XmlElement(name = "P504")
	public Integer getP504() {
		return p504;
	}

	public void setP504(Integer p504) {
		this.p504 = p504;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Local2019Cabecera)) {
			return false;
		}
		Local2019Cabecera other = (Local2019Cabecera) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.censo2019.Local2019Cabecera[idEnvio=" + idEnvio + "]";
	}

}
