/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.domain;

import javax.ws.rs.client.Client;
import pe.gob.minedu.escale.eol.domain.EolActividadesConverter;
import pe.gob.minedu.escale.eol.client.constantes.Constantes;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;

/**
 * Jersey REST client generated for REST resource:codigos-rest
 * [distritos/{idDistrito}/]<br>
 * USAGE:
 * <pre>
 *        DistritoClient client = new DistritoClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author DSILVA
 */
public class EolActividadesClient {

    private Logger logger = Logger.getLogger(EolActividadesClient.class);

    private Client client;

    public EolActividadesClient() {
        client = ClientBuilder.newClient();
    }

    public EolActividadesConverter getActividadesConverter(String nombre, String periodo) {
        WebTarget target = client.target(Constantes.BASE_URI_EOL_ADMIN).path("/get/actividades");
        target = target.queryParam("nombre", nombre);
        target = target.queryParam("periodo", periodo);
        logger.info(" URL: " + target.getUri().toString());
        return target.request(MediaType.APPLICATION_XML).get(new javax.ws.rs.core.GenericType<EolActividadesConverter>() {
        });
    }
    /*
    private MultivaluedMap getQueryOrFormParams(String[] paramNames, String[] paramValues) {
        MultivaluedMap<String, String> qParams = new com.sun.jersey.api.representation.Form();
        for (int i = 0; i < paramNames.length; i++) {
            if (paramValues[i] != null) {
                qParams.add(paramNames[i], paramValues[i]);
            }
        }
        return qParams;
    }

    public void close() {
        client.destroy();
    }
     */
}
