package pe.gob.minedu.escale.eol.ejb.censo2019;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Resp;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec112;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec206;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec500;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec700;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec800;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec207;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec300;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec400;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec405;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec611;
import pe.gob.minedu.escale.eol.ejb.AbstractFacade;

@Singleton
public class Local2019Facade extends AbstractFacade<Local2019Cabecera> implements Local2019Local {

	@PersistenceContext(unitName = "eol-PUAux")
	private EntityManager em;

	static final Logger LOGGER = Logger.getLogger(Local2019Facade.class.getName());

	public Local2019Facade() {
		super(Local2019Cabecera.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return em;
	}

	public void create(Local2019Cabecera entity) {
		String sql = "UPDATE Local2019Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();

		if (entity.getLocal2019Sec206List() != null)
			for (Local2019Sec206 sec206 : entity.getLocal2019Sec206List()) {
				sec206.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019RespList() != null)
			for (Local2019Resp resp : entity.getLocal2019RespList()) {
				resp.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec800List() != null)
			for (Local2019Sec800 sec800 : entity.getLocal2019Sec800List()) {
				sec800.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec700List() != null)
			for (Local2019Sec700 sec700 : entity.getLocal2019Sec700List()) {
				sec700.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec500List() != null)
			for (Local2019Sec500 sec500 : entity.getLocal2019Sec500List()) {
				sec500.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec207List() != null)
			for (Local2019Sec207 sec207 : entity.getLocal2019Sec207List()) {
				sec207.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec400List() != null)
			for (Local2019Sec400 sec400 : entity.getLocal2019Sec400List()) {
				sec400.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec300List() != null)
			for (Local2019Sec300 sec300 : entity.getLocal2019Sec300List()) {
				sec300.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec405List() != null)
			for (Local2019Sec405 sec405 : entity.getLocal2019Sec405List()) {
				sec405.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec611List() != null)
			for (Local2019Sec611 sec611 : entity.getLocal2019Sec611List()) {
				sec611.setLocal2019Cabecera(entity);
			}
		if (entity.getLocal2019Sec112List() != null)
			for (Local2019Sec112 sec112 : entity.getLocal2019Sec112List()) {
				sec112.setLocal2019Cabecera(entity);
			}

		entity.setUltimo(true);
		if (entity.getFechaEnvio() == null) {

			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			// c.add(Calendar.HOUR, -5); UTILIZADO SOLO PARA EL SERVIDOR EXTERNO

			entity.setFechaEnvio(c.getTime());
		}

		em.persist(entity);
		em.flush();
	}

	public Local2019Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2019Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2019Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void createCabecera(Local2019Cabecera entity) {
		String sql = "UPDATE Local2019Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();

		em.persist(entity);
		// em.flush();
	}

}
