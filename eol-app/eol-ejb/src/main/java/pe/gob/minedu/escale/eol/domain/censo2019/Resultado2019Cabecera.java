package pe.gob.minedu.escale.eol.domain.censo2019;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/***
 * 
 * @author WARODRIGUEZ
 * 
 */

@XmlRootElement(name = "cedulaResultado2019")
@Entity
@Table(name = "resultado2019_cabecera")
public class Resultado2019Cabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	public final static String CEDULA_01B = "c01b";
	public final static String CEDULA_02B = "c02b";
	public final static String CEDULA_03BP = "c03bp";
	public final static String CEDULA_03BS = "c03bs";
	public final static String CEDULA_04BI = "c04bi";
	public final static String CEDULA_04BA = "c04ba";
	public final static String CEDULA_05B = "c05b";
	public final static String CEDULA_06B = "c06b";
	public final static String CEDULA_07B = "c07b";
	public final static String CEDULA_08BI = "c08bi";
	public final static String CEDULA_08BP = "c08bp";
	public final static String CEDULA_09B = "c09b";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;

	@Basic(optional = false)
	@Column(name = "NROCED")
	private String nroced;

	@Basic(optional = false)
	@Column(name = "COD_MOD")
	private String codMod;

	@Basic(optional = false)
	@Column(name = "ANEXO")
	private String anexo;

	@Column(name = "TIPOREG")
	private String tiporeg;

	@Column(name = "CODLOCAL")
	private String codlocal;

	@Column(name = "TIPPROG")
	private String tipprog;

	@Column(name = "CEN_EDU")
	private String cenEdu;

	@Column(name = "NIV_MOD")
	private String nivMod;

	@Column(name = "CODOOII")
	private String codooii;

	@Column(name = "CODGEO")
	private String codgeo;

	@Column(name = "DISTRITO")
	private String distrito;

	@Column(name = "FORMATEN")
	private String formaten;

	@Column(name = "TIPONEE")
	private String tiponee;

	@Column(name = "TIPOISE")
	private String tipoise;

	@Column(name = "DNI_CORD")
	private String dniCord;

	@Column(name = "CEBA_INI")
	private String cebaIni;

	@Column(name = "CEBA_INT")
	private String cebaInt;

	@Column(name = "PERF_INI")
	private String perfIni;

	@Column(name = "PERF_INT")
	private String perfInt;

	@Column(name = "CEBA_PR")
	private String cebaPr;

	@Column(name = "CEBA_SP")
	private String cebaSp;

	@Column(name = "PERF_PR")
	private String perfPr;

	@Column(name = "PERF_SP")
	private String perfSp;

	@Column(name = "ULTIMO")
	private Boolean ultimo;

	@Column(name = "VERSION")
	private String version;

	@Column(name = "TIPO_ENVIO")
	private Boolean tipoEnvio;

	@Basic(optional = false)
	@Column(name = "FECHA_ENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;

	@Column(name = "CONTROL")
	private String control;

	@Column(name = "VALIDO")
	private Boolean valido;

	@Column(name = "CONFIRMADO")
	private String confirmado;

	@Column(name = "SITUACION")
	private String situacion;

	@Column(name = "VARREC")
	private String varrec;

	// @MapKeyColumn(name = "CUADRO", length = 5)
	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "resultado2019Cabecera",
	// fetch = FetchType.EAGER)
	
	// @OneToMany(mappedBy = "cedula", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OneToMany(mappedBy = "cedula", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Resultado2019Detalle> resultado2019ResultadoList;
	
	//@OneToMany(mappedBy = "resultado2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OneToMany(mappedBy = "resultado2019Cabecera", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Resultado2019Resp> resultado2019RespList;

	@Transient
	private String msg;

	@Transient
	private String estadoRpt;

	@Transient
	private long token;

	@Transient
	private String metEnse;

	@Transient
	private String modServ;
	
	public Resultado2019Cabecera() {
	}

	public Resultado2019Cabecera(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Resultado2019Cabecera(Long idEnvio, String nroced, String codMod, String anexo, Date fechaEnvio) {
		this.idEnvio = idEnvio;
		this.nroced = nroced;
		this.codMod = codMod;
		this.anexo = anexo;
		this.fechaEnvio = fechaEnvio;
	}

	// @XmlAttribute
	public Long getIdEnvio() {

		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {

		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "NROCED")
	public String getNroced() {

		return nroced;
	}

	public void setNroced(String nroced) {

		this.nroced = nroced;
	}

	@XmlElement(name = "COD_MOD")
	public String getCodMod() {

		return codMod;
	}

	public void setCodMod(String codMod) {

		this.codMod = codMod;
	}

	@XmlElement(name = "ANEXO")
	public String getAnexo() {

		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	@XmlElement(name = "TIPOREG")
	public String getTiporeg() {
		return tiporeg;
	}

	public void setTiporeg(String tiporeg) {
		this.tiporeg = tiporeg;
	}

	@XmlElement(name = "CODLOCAL")
	public String getCodlocal() {

		return codlocal;
	}

	public void setCodlocal(String codlocal) {

		this.codlocal = codlocal;
	}

	@XmlElement(name = "TIPPROG")
	public String getTipprog() {

		return tipprog;
	}

	public void setTipprog(String tipprog) {

		this.tipprog = tipprog;
	}

	@XmlElement(name = "CEN_EDU")
	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	@XmlElement(name = "NIV_MOD")
	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	@XmlElement(name = "CODOOII")
	public String getCodooii() {

		return codooii;
	}

	public void setCodooii(String codooii) {

		this.codooii = codooii;
	}

	@XmlElement(name = "CODGEO")
	public String getCodgeo() {

		return codgeo;
	}

	public void setCodgeo(String codgeo) {

		this.codgeo = codgeo;
	}

	@XmlElement(name = "DISTRITO")
	public String getDistrito() {

		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@XmlElement(name = "FORMATEN")
	public String getFormaten() {
		return formaten;
	}

	public void setFormaten(String formaten) {
		this.formaten = formaten;
	}

	@XmlElement(name = "TIPONEE")
	public String getTiponee() {
		return tiponee;
	}

	public void setTiponee(String tiponee) {
		this.tiponee = tiponee;
	}

	@XmlElement(name = "TIPOISE")
	public String getTipoise() {
		return tipoise;
	}

	public void setTipoise(String tipoise) {
		this.tipoise = tipoise;
	}

	@XmlElement(name = "DNI_CORD")
	public String getDniCord() {
		return dniCord;
	}

	public void setDniCord(String dniCord) {

		this.dniCord = dniCord;
	}

	@XmlElement(name = "CEBA_INI")
	public String getCebaIni() {

		return cebaIni;
	}

	public void setCebaIni(String cebaIni) {

		this.cebaIni = cebaIni;
	}

	@XmlElement(name = "CEBA_INT")
	public String getCebaInt() {

		return cebaInt;
	}

	public void setCebaInt(String cebaInt) {

		this.cebaInt = cebaInt;
	}

	@XmlElement(name = "PERF_INI")
	public String getPerfIni() {

		return perfIni;
	}

	public void setPerfIni(String perfIni) {

		this.perfIni = perfIni;
	}

	@XmlElement(name = "PERF_INT")
	public String getPerfInt() {

		return perfInt;
	}

	public void setPerfInt(String perfInt) {

		this.perfInt = perfInt;
	}

	@XmlElement(name = "CEBA_PR")
	public String getCebaPr() {

		return cebaPr;
	}

	public void setCebaPr(String cebaPr) {
		this.cebaPr = cebaPr;
	}

	@XmlElement(name = "CEBA_SP")
	public String getCebaSp() {
		return cebaSp;
	}

	public void setCebaSp(String cebaSp) {
		this.cebaSp = cebaSp;
	}

	@XmlElement(name = "PERF_PR")
	public String getPerfPr() {
		return perfPr;
	}

	public void setPerfPr(String perfPr) {
		this.perfPr = perfPr;
	}

	@XmlElement(name = "PERF_SP")
	public String getPerfSp() {
		return perfSp;
	}

	public void setPerfSp(String perfSp) {
		this.perfSp = perfSp;
	}

	@XmlElement(name = "ULTIMO")
	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	@XmlElement(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name = "TIPO_ENVIO")
	public Boolean getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(Boolean tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	@XmlElement(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	@XmlElement(name = "CONTROL")
	public String getControl() {
		return control;
	}

	public void setControl(String control) {
		this.control = control;
	}

	@XmlElement(name = "VALIDO")
	public Boolean getValido() {
		return valido;
	}

	public void setValido(Boolean valido) {
		this.valido = valido;
	}

	@XmlElement(name = "CONFIRMADO")
	public String getConfirmado() {
		return confirmado;
	}

	public void setConfirmado(String confirmado) {
		this.confirmado = confirmado;
	}

	@XmlElement(name = "SITUACION")
	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	@XmlElement(name = "VARREC")
	public String getVarrec() {
		return varrec;
	}

	public void setVarrec(String varrec) {
		this.varrec = varrec;
	}

	@XmlElement(name = "RESPONSABLE")
	public List<Resultado2019Resp> getResultado2019RespList() {
		return resultado2019RespList;
	}

	public void setResultado2019RespList(List<Resultado2019Resp> resultado2019RespList) {

		this.resultado2019RespList = resultado2019RespList;
	}

	// @XmlJavaTypeAdapter(Resultado2019DetalleMapAdapter.class)
	// @XmlElement(name = "DETALLE")
	// public Map<String, Resultado2019Detalle> getDetalle() {
	// LOGGER.info("LLEGO CEDULA RESULTADO 2019
	// jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj..............");
	// return detalle;
	// }
	//
	// public void setDetalle(Map<String, Resultado2019Detalle> detalle) {
	// LOGGER.info("LLEGO CEDULA RESULTADO 2019
	// kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk..............");
	// this.detalle = detalle;
	// }


	@XmlElement(name = "DETALLE")
	public List<Resultado2019Detalle> getResultado2019ResultadoList() {
		return resultado2019ResultadoList;
	}

	public void setResultado2019ResultadoList(List<Resultado2019Detalle> resultado2019ResultadoList) {

		this.resultado2019ResultadoList = resultado2019ResultadoList;
	}

	@XmlAttribute
	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	@XmlAttribute(name = "ESTADO_RPT")
	public String getEstadoRpt() {
		return estadoRpt;
	}

	public void setEstadoRpt(String estadoRpt) {
		this.estadoRpt = estadoRpt;
	}

	@XmlAttribute(name = "MET_ENSE")
	public String getMetEnse() {
		return metEnse;
	}

	public void setMetEnse(String metEnse) {
		this.metEnse = metEnse;
	}
	
	@XmlAttribute(name = "MOD_SERV")
	public String getModServ() {
		return modServ;
	}
	
	public void setModServ(String modServ) {
		this.modServ = modServ;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Resultado2019Cabecera)) {
			return false;
		}
		Resultado2019Cabecera other = (Resultado2019Cabecera) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera[idEnvio=" + idEnvio + "]";

	}
}
