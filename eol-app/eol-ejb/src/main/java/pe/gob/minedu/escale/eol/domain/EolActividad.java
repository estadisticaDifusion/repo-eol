/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "eol_actividad")
public class EolActividad implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;

    @Column(name = "FECHA_TERMINO")
    @Temporal(TemporalType.DATE)
    private Date fechaTermino;

    @Column(name = "FECHA_LIMITE")
    @Temporal(TemporalType.DATE)
    private Date fechaLimite;

    @Column(name = "FECHA_TERMINOSIGIED")
    @Temporal(TemporalType.DATE)
    private Date fechaTerminosigied;

    @Column(name = "FECHA_LIMITESIGIED")
    @Temporal(TemporalType.DATE)
    private Date fechaLimitesigied;

    @Column(name = "FECHA_TERMINOHUELGA")
    @Temporal(TemporalType.DATE)
    private Date fechaTerminohuelga;

    @Column(name = "FECHA_LIMITEHUELGA")
    @Temporal(TemporalType.DATE)
    private Date fechaLimitehuelga;

    @Column(name = "URL_FORMATO")
    private String urlFormato;

    @Column(name = "URL_CONSTANCIA")
    private String urlConstancia;

    @Column(name = "URL_COBERTURA")
    private String urlCobertura;

    @Column(name = "URL_SITUACION")
    private String urlSituacion;

    @Column(name = "URL_OMISOS")
    private String urlOmisos;

    @Lob
    @Column(name = "SQL_CENSO")
    private String sqlCenso;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "SITUACION")
    private String situacion;

    @Column(name = "NOMBRE_CEDULA")
    private String nombreCedula;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "ESTADO_SIE")
    private boolean estadoSie = false;

    @Column(name = "ESTADO_CONSTANCIA")
    private boolean estadoConstancia = false;

    @Column(name = "ESTADO_FORMATO")
    private boolean estadoFormato = false;

    @Column(name = "ESTADO_COBERTURA")
    private boolean estadoCobertura = false;

    @Column(name = "ESTADO_SITUACION")
    private boolean estadoSituacion = false;

    @Column(name = "ESTADO_OMISOS")
    private boolean estadoOmisos = false;

    @Column(name = "ORDEN")
    private String orden;

    @JoinColumn(name = "ID_PERIODO", referencedColumnName = "ID")
    @ManyToOne
    private EolPeriodo eolPeriodo;

    public EolActividad() {
    }

    public EolActividad(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public Date getFechaTerminosigied() {
        return fechaTerminosigied;
    }

    public void setFechaTerminosigied(Date fechaTerminosigied) {
        this.fechaTerminosigied = fechaTerminosigied;
    }

    public Date getFechaTerminohuelga() {
        return fechaTerminohuelga;
    }

    public void setFechaTerminohuelga(Date fechaTerminohuelga) {
        this.fechaTerminohuelga = fechaTerminohuelga;
    }

    public Date getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getUrlFormato() {
        return urlFormato;
    }

    public void setUrlFormato(String urlFormato) {
        this.urlFormato = urlFormato;
    }

    public String getUrlConstancia() {
        return urlConstancia;
    }

    public void setUrlConstancia(String urlConstancia) {
        this.urlConstancia = urlConstancia;
    }

    public String getUrlCobertura() {
        return urlCobertura;
    }

    public void setUrlCobertura(String urlCobertura) {
        this.urlCobertura = urlCobertura;
    }

    public String getUrlSituacion() {
        return urlSituacion;
    }

    public void setUrlSituacion(String urlSituacion) {
        this.urlSituacion = urlSituacion;
    }

    public String getUrlOmisos() {
        return urlOmisos;
    }

    public void setUrlOmisos(String urlOmisos) {
        this.urlOmisos = urlOmisos;
    }

    public String getSqlCenso() {
        return sqlCenso;
    }

    public void setSqlCenso(String sqlCenso) {
        this.sqlCenso = sqlCenso;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getNombreCedula() {
        return nombreCedula;
    }

    public void setNombreCedula(String nombreCedula) {
        this.nombreCedula = nombreCedula;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean getEstadoSie() {
        return estadoSie;
    }

    public void setEstadoSie(boolean estadoSie) {
        this.estadoSie = estadoSie;
    }

    public boolean getEstadoConstancia() {
        return estadoConstancia;
    }

    public void setEstadoConstancia(boolean estadoConstancia) {
        this.estadoConstancia = estadoConstancia;
    }

    public boolean getEstadoFormato() {
        return estadoFormato;
    }

    public void setEstadoFormato(boolean estadoFormato) {
        this.estadoFormato = estadoFormato;
    }

    public boolean getEstadoCobertura() {
        return estadoCobertura;
    }

    public void setEstadoCobertura(boolean estadoCobertura) {
        this.estadoCobertura = estadoCobertura;
    }

    public boolean getEstadoSituacion() {
        return estadoSituacion;
    }

    public void setEstadoSituacion(boolean estadoSituacion) {
        this.estadoSituacion = estadoSituacion;
    }

    public boolean getEstadoOmisos() {
        return estadoOmisos;
    }

    public void setEstadoOmisos(boolean estadoOmisos) {
        this.estadoOmisos = estadoOmisos;
    }

    public EolPeriodo getEolPeriodo() {
        return eolPeriodo;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public void setEolPeriodo(EolPeriodo eolPeriodo) {
        this.eolPeriodo = eolPeriodo;
    }

    public Date getFechaLimitehuelga() {
        return fechaLimitehuelga;
    }

    public void setFechaLimitehuelga(Date fechaLimitehuelga) {
        this.fechaLimitehuelga = fechaLimitehuelga;
    }

    public Date getFechaLimitesigied() {
        return fechaLimitesigied;
    }

    public void setFechaLimitesigied(Date fechaLimitesigied) {
        this.fechaLimitesigied = fechaLimitesigied;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EolActividad)) {
            return false;
        }
        EolActividad other = (EolActividad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EolActividad{" + "id=" + id + '}';
    }

}
