package pe.gob.minedu.escale.eol.ejb.censo2019;



import javax.ejb.Local;

import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Cabecera;


@Local
public interface Local2019Local {

	public void create(Local2019Cabecera entity);
	public Local2019Cabecera findByCodLocal(String codLocal);
	public void createCabecera(Local2019Cabecera entity);

}
