/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.ejb.censo2019;


import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Carreras;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Dlen;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Eba;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Localpronoei;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Matricula;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019MatriculaFila;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Muledad;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Personal;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Recursos;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019RecursosFila;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Resp;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019S100;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Saanee;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Seccion;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019SeccionFila;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Sede;
import pe.gob.minedu.escale.eol.ejb.AbstractFacade;

@Singleton
public class Matricula2019Facade extends AbstractFacade<Matricula2019Cabecera> implements Matricula2019Local {

	static final Logger LOGGER = Logger.getLogger(Matricula2019Facade.class.getName());

	@PersistenceContext(unitName = "eol-PUAux")
	private EntityManager em;
	
	public static final String CEDULA_MATRICULA = "CENSO-MATRICULA";
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03AP = "c03ap";
	public final static String CEDULA_03AS = "c03as";
	public final static String CEDULA_04AI = "c04ai";
	public final static String CEDULA_04AA = "c04aa";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08AI = "c08ai";
	public final static String CEDULA_08AP = "c08ap";
	public final static String CEDULA_09A = "c09a";


	public Matricula2019Facade() {
		super(Matricula2019Cabecera.class);
	}

	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public void create(Matricula2019Cabecera cedula) {
		
		String sql = "UPDATE Matricula2019Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroced=:nroCed ";
		//cedula.setIdEnvio(1l);
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroced());
		query.executeUpdate();
		

		
		if (cedula.getDetalleMatricula() != null) {

			for (Matricula2019Matricula mat : cedula.getDetalleMatricula()) {
				
				mat.setMatricula2019Cabecera(cedula);
				for (Matricula2019MatriculaFila matf : mat.getMatricula2019MatriculaFilaList() ){
					matf.setMatricula2019Matricula(mat);
				}
			}
			
			/*
            Set<String> keys = cedula.getDetalleMatricula().keySet();
            for (Iterator<String> it = keys.iterator(); it.hasNext();) {
                String key = it.next();
                Matricula2018Matricula detalle = cedula.getDetalleMatricula().get(key);
                detalle.setMatricula2018Cabecera(cedula);
                List<Matricula2018MatriculaFila> filas = detalle.getMatricula2018MatriculaFilaList();
                for (Matricula2018MatriculaFila fila : filas) {
                    fila.setMatricula2018Matricula(detalle);
                }
            }*/
        }

        if (cedula.getDetalleSeccion() != null) {
        	for(Matricula2019Seccion sec : cedula.getDetalleSeccion()){
        		
        		sec.setMatricula2019Cabecera(cedula);
        		for(Matricula2019SeccionFila secf: sec.getMatricula2019SeccionFilaList()){
        			secf.setMatricula2019Seccion(sec);;
        		}
        	}
        	
            /*Set<String> keys = cedula.getDetalleSeccion().keySet();
            for (Iterator<String> it = keys.iterator(); it.hasNext();) {
                String key = it.next();
                Matricula2018Seccion detalle = cedula.getDetalleSeccion().get(key);
                detalle.setMatricula2018Cabecera(cedula);
                List<Matricula2018SeccionFila> filas = detalle.getMatricula2018SeccionFilaList();
                for (Matricula2018SeccionFila fila : filas) {
                    fila.setMatricula2018Seccion(detalle);
                }
            }*/
        }

        if(cedula.getMatricula2019RecursosList() != null){
        	for(Matricula2019Recursos rec : cedula.getMatricula2019RecursosList()){
        		
        		rec.setMatricula2019Cabecera(cedula);
        		for(Matricula2019RecursosFila recf : rec.getMatricula2019RecursosFilaList()){
        			recf.setMatricula2019Recursos(rec);
        		}
        	}
        	
        	/*
            Set<String> keys = cedula.getDetalleRecursos().keySet();
            for (Iterator<String> it = keys.iterator(); it.hasNext();) {
                String key = it.next();
                Matricula2018Recursos detalle = cedula.getDetalleRecursos().get(key);
                detalle.setMatricula2018Cabecera(cedula);
                List<Matricula2018RecursosFila> filas = detalle.getMatricula2018RecursosFilaList();
                for (Matricula2018RecursosFila fila : filas) {
                    fila.setMatricula2018Recursos(detalle);
                }
            }*/
        }

        
        if(cedula.getMatricula2019PersonalList()!=null){
        	System.out.println("CEDUL 222 :" +  cedula.getIdEnvio() );
        	for(Matricula2019Personal per:cedula.getMatricula2019PersonalList())
            {   per.setMatricula2019Cabecera(cedula);
            }
        }
        if(cedula.getMatricula2019LocalpronoeiList() != null){
        	for(Matricula2019Localpronoei loc : cedula.getMatricula2019LocalpronoeiList()){
                loc.setMatricula2019Cabecera(cedula);
            }
        }
        if(cedula.getMatricula2019CarrerasList() != null){
        	for (Matricula2019Carreras carr : cedula.getMatricula2019CarrerasList()) {
                carr.setMatricula2019Cabecera(cedula);
            }
        }
        if(cedula.getMatricula2019SaaneeList() != null){
           
        	for (Matricula2019Saanee saan : cedula.getMatricula2019SaaneeList()) {
                saan.setMatricula2019Cabecera(cedula);
            }
        }
//        if(cedula.getMatricula2018EbaList() != null){
//            for (Matricula2018Eba eba : cedula.getMatricula2018EbaList()) {
//                eba.setMatricula2018Cabecera(cedula);
//            }
//        }
        if(cedula.getMatricula2019DlenList() != null){
        	for (Matricula2019Dlen dlen : cedula.getMatricula2019DlenList()) {
                dlen.setMatricula2019Cabecera(cedula);
            }
        }
        
        if(cedula.getMatricula2019EbaList() != null) {
        	for(Matricula2019Eba eba : cedula.getMatricula2019EbaList()) {
        		eba.setMatricula2019Cabecera(cedula);
        	}
        }
        
        if(cedula.getMatricula2019RespList() != null){
        	for (Matricula2019Resp resp : cedula.getMatricula2019RespList()) {
                resp.setMatricula2019Cabecera(cedula);
            }
        }
        if(cedula.getMatricula2019S100List() != null){
        	for(Matricula2019S100 s100 : cedula.getMatricula2019S100List())
        	{
        		s100.setMatricula2019Cabecera(cedula);
        	}
        }
        if(cedula.getMatricula2019MuledadList() != null){
        	for(Matricula2019Muledad medad: cedula.getMatricula2019MuledadList()){
        		medad.setMatricula2019Cabecera(cedula);
        	}
        }
        if(cedula.getMatricula2019SedeList() != null){
        	for(Matricula2019Sede sede: cedula.getMatricula2019SedeList()){
        		sede.setMatricula2019Cabecera(cedula);
        	}	
        }
		
		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null){
			
			Calendar c = Calendar.getInstance();
	        c.setTime(new Date()); 
	        //c.add(Calendar.HOUR, -5); UTILIZADO SOLO PARA EL SERVIDOR EXTERNO
	        
			cedula.setFechaEnvio(c.getTime());
		}	

		LOGGER.info(" ANTES DEL PERSIST ");
		em.persist(cedula);
		em.flush();
	}

	public Matricula2019Cabecera findByCodModAnNiv(String codmod, String anexo, String nivmod) {
		Query q = em.createQuery(
				"SELECT M FROM Matricula2019Cabecera M WHERE M.codMod=:codmod AND M.anexo=:anexo AND M.nivMod=:nivmod AND M.ultimo=true");
		q.setParameter("codmod", codmod);
		q.setParameter("anexo", anexo);
		q.setParameter("nivmod", nivmod);
		try {
			return (Matricula2019Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> registrarEnvioIdentificacion(String codmod, String anexo, String nivmod) {
		Query q = null;

		try {
			String query = "CALL registrar_envio_2019(?,?,?)";
			q = (Query) em.createNativeQuery(query);

			q.setParameter("1", codmod);
			q.setParameter("2", anexo);
			q.setParameter("3", nivmod);

			List<Object[]> listObj = q.getResultList();

			return listObj;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
