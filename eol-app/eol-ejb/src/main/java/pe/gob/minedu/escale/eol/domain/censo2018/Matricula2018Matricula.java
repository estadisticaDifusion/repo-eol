/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.domain.censo2018;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "matricula2018_matricula")
public class Matricula2018Matricula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;

	@Column(name = "CUADRO")
	private String cuadro;

	@JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	//@ManyToOne(optional = false)
	@ManyToOne
	private Matricula2018Cabecera matricula2018Cabecera;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2018Matricula", fetch = FetchType.EAGER)
	private List<Matricula2018MatriculaFila> matricula2018MatriculaFilaList;
	public Matricula2018Matricula() {
	}

	public Matricula2018Matricula(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "CUADRO")
	public String getCuadro() {
		return cuadro;
	}

	public void setCuadro(String cuadro) {
		this.cuadro = cuadro;
	}

	@XmlElement(name = "MATRICULA_FILAS")
	public List<Matricula2018MatriculaFila> getMatricula2018MatriculaFilaList() {
		return matricula2018MatriculaFilaList;
	}

	public void setMatricula2018MatriculaFilaList(List<Matricula2018MatriculaFila> matricula2018MatriculaFilaList) {
		this.matricula2018MatriculaFilaList = matricula2018MatriculaFilaList;
	}

	@XmlTransient
	public Matricula2018Cabecera getMatricula2018Cabecera() {
		return matricula2018Cabecera;
	}

	public void setMatricula2018Cabecera(Matricula2018Cabecera matricula2018Cabecera) {
		this.matricula2018Cabecera = matricula2018Cabecera;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2018Matricula)) {
			return false;
		}
		Matricula2018Matricula other = (Matricula2018Matricula) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Matricula[idEnvio=" + idEnvio + "]";
	}

}
