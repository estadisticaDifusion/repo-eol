package pe.gob.minedu.escale.eol.padron.test;

import static junit.framework.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.Test;

import pe.gob.minedu.escale.eol.dto.entities.CodigoDTO;
import pe.gob.minedu.escale.eol.dto.entities.FormaDTO;
import pe.gob.minedu.escale.eol.padron.domain.Codigo;
import pe.gob.minedu.escale.eol.padron.domain.Forma;

public class UtilTest {

    private Logger logger = Logger.getLogger(UtilTest.class);

    @Test
    public void testUtils() {
    	logger.info(":: UtilTest.testUtils :: Starting execution...");
    	Forma forma = new Forma();
    	forma.setIdCodigo("001");
    	forma.setOrden(1);
    	forma.setValor("Escolarizado");
    	
    	FormaDTO formaDTO = (FormaDTO)setCodigoDomainToDto(forma, FormaDTO.class);
    	assertTrue(!(formaDTO == null));
    	logger.info("formaDTO="+formaDTO);
    	
    	logger.info(":: UtilTest.testUtils :: Execution finish.");
    }

	private <T extends CodigoDTO> CodigoDTO setCodigoDomainToDto(Codigo codigo, Class<T> clase) {
		
		CodigoDTO codigoDTO = null;
		if (codigo instanceof Forma) {
			codigoDTO = new FormaDTO();
		}
		
		if (codigo != null) {
			codigoDTO.setIdCodigo(codigo.getIdCodigo());
			codigoDTO.setValor(codigo.getValor());
			codigoDTO.setOrden(codigo.getOrden());
		}
		return codigoDTO;
	}
}
