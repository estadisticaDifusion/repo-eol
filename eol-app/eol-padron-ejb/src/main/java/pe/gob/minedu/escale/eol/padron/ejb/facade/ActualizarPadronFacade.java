///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package pe.gob.minedu.escale.eol.padron.ejb.facade;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import javax.ejb.Singleton;
//
//import pe.gob.minedu.escale.eol.dbf.DbfFile;
//import pe.gob.minedu.escale.eol.dbf.DbfFile.Field;
//import pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal;
//import pe.gob.minedu.escale.eol.padron.importar.dao.ActualizarPadronDao;
//import pe.gob.minedu.escale.eol.padron.importar.dao.DAOFactory;
//import pe.gob.minedu.escale.eol.padron.importar.dao.domain.TableField;
//
///**
// *
// *
// * @author MHUAMANI
// * @version 1.0
// */
//@Singleton
//public class ActualizarPadronFacade implements ActualizarPadronLocal {
//
//	private static final Logger LOGGER = Logger.getLogger(ActualizarPadronFacade.class.getName());
//
//	@Override
//	public synchronized void actualizar(File file, List<Map<String, String>> mapSIG) throws IOException {
//		LOGGER.info("iniciando actualizador");
//
//        DbfFile dbf = new DbfFile(file);        
//        dbf.open();
//        ActualizarPadronDao dao = DAOFactory.getInstance().getActualizarPadronDAO("padron");
//        dao.nuevaTabla();
//        LOGGER.info("transfiriendo registros");
//        
//        for (dbf.go(1); !dbf.eof(); dbf.skip(1)) {
//            Map<String, String> fila = dbf.scatter();
//            dao.agregar(fila);            
//            if(fila.get("OPC")!=null && !fila.get("OPC").isEmpty())
//                mapSIG.add(fila);
//        }
//        
//        LOGGER.info("iniciando actualizador");
//        dao.actualizar();
//        LOGGER.info("Fin de carga de registros");
//        //dao.borrarTabla();
//                
//        dbf.close();
//        file.delete();
//	}
//
//	@Override
//	public Field[] obtenerCampos(File uploadedFile) {
//        DbfFile dbfFile = null;
//        try {
//            dbfFile = new DbfFile(uploadedFile);
//            dbfFile.open();
//            return dbfFile.getFields();
//        } catch (FileNotFoundException e) {
//        	LOGGER.log(Level.WARNING, null, e);
//        } catch (IOException e) {
//        	LOGGER.log(Level.WARNING, null, e);
//        } finally {
//            if (dbfFile != null) {
//                try {
//                    dbfFile.close();
//                } catch (IOException e) {
//                	LOGGER.log(Level.WARNING, null, e);
//                }
//            }
//        }
//        
//        return null;
//	}
//
//	@Override
//	public List<TableField> obtenerCamposTabla(String tabla) {
//	      ActualizarPadronDao dao = DAOFactory.getInstance().getActualizarPadronDAO(tabla);
//	      return dao.getFields();
//	}
//
//}
