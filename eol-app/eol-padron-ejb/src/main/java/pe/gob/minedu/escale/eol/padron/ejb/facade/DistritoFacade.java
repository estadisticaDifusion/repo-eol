package pe.gob.minedu.escale.eol.padron.ejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.dto.entities.DistritoDTO;
import pe.gob.minedu.escale.eol.padron.domain.Distrito;
import pe.gob.minedu.escale.eol.padron.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.padron.ejb.DistritoLocal;
import pe.gob.minedu.escale.eol.padron.mappers.MapperUbicacion;

@Singleton
public class DistritoFacade extends AbstractFacade<Distrito> implements DistritoLocal {

	private static final Logger logger = Logger.getLogger(DistritoFacade.class);
	
	@PersistenceContext(unitName = "padron-ejbPU")
    private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public DistritoFacade() {
		super(Distrito.class);
	}

	@Override
	public Distrito findById(String codigoDistrito) {
		logger.info(":: ProvinciaFacade.findById :: Starting execution...");
		Distrito distrito = em.find(Distrito.class, codigoDistrito);
		logger.info(" Distrito = "+distrito );
        logger.info(":: Distrito.findById :: Execution finish.");
		
		return distrito ;
	}

	@Override
	public DistritoDTO getDistrito(String codigoDistrito) {
		logger.info(":: DistritoFacade.findById :: Starting execution...");
		Distrito distrito = em.find(Distrito.class, codigoDistrito);
		logger.info(" Distrito = "+distrito );
		DistritoDTO distritoDTO = MapperUbicacion.setDistritoDomainDto(distrito);
        logger.info(":: Distrito.findById :: Execution finish.");
		
		return distritoDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DistritoDTO> getListDistritoDTO(String codigoProvincia) {
		List<DistritoDTO> listprovinciaDTO = new ArrayList<DistritoDTO>();
		
		String sql= "select e from Distrito e where e.provincia.idProvincia= '"+codigoProvincia+"' order by e.idDistrito ";
		
		List<Distrito> lista = em.createQuery(sql).getResultList(); 
		
		for(Distrito rg : lista) {
			listprovinciaDTO.add(MapperUbicacion.setDistritoDomainDto(rg));
		}
		
		return listprovinciaDTO;
	}



	
}
