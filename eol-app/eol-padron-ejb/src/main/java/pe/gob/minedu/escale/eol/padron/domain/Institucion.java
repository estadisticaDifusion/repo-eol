/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "padron")
public class Institucion implements Serializable {

	private static final long serialVersionUID = 1L;
	/*
	 * @EmbeddedId private InstitucionId institucionId;
	 */
	@Id
	@Column(name = "cod_mod")
	private String codMod;

	// @Id
	@Column(name = "anexo")
	private String anexo;

	@Column(name = "codlocal")
	private String codlocal;

	@Column(name = "cen_edu")
	private String cenEdu;
	@Column(name = "dir_cen")
	private String dirCen;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "email")
	private String email;
	
	@Column(name = "pagweb")
	private String pagweb;
	
	@Column(name = "director")
	private String director;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "codgeo", referencedColumnName = "id_distrito")
	private Distrito distrito;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "codooii", referencedColumnName = "id_ugel")
	private Ugel ugel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "codigel", referencedColumnName = "codigel")
	private Igel igel;

	@Column(name = "tipoigel")
	private String tipoigel;

	@Column(name = "cen_pob")
	private String cenPob;

	@Column(name = "codccpp")
	private String codccpp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "formas", referencedColumnName = "id")
	private Forma forma;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_area", referencedColumnName = "id")
	private Area area;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "situadir", referencedColumnName = "id")
	private SituacionDirector situacionDirector;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ges_dep", referencedColumnName = "id")
	private GestionDependencia gestionDependencia;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estado", referencedColumnName = "id")
	private Estado estado;

	@Column(name = "mcenso")
	private String mcenso;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "niv_mod", referencedColumnName = "id")
	private NivelModalidad nivelModalidad;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_tur", referencedColumnName = "id")
	private Turno turno;
/*
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "tipoprog", referencedColumnName = "id", nullable = true)
	private Tipoprog tipoprog;*/

	@Column(name = "tipoprog")
	private String tipoprog;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipssexo", referencedColumnName = "id")
	private Genero genero;

	@Column(name = "progdist")
	private String progdist;

	@Column(name = "progarti")
	private String progarti;

	@Lob
	@Column(name = "comenta")
	private String comenta;

	@Column(name = "fechareg")
	private String fechareg;

	@Column(name = "fecharet")
	private String fecharet;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "area_sig", referencedColumnName = "id")
	private Area areaSig;

	@Column(name = "registro")
	private String registro;

	// INICIO MODIF JC 13-04-2011
	@Column(name = "metodo")
	private String metodo;

	@Column(name = "contjesc")
	private String contjesc;

	@Column(name = "fecharea")
	private String fecharea;

	@Column(name = "codcp_inei")
	private String codcp_inei;

	@Column(name = "alt_cp")
	private Integer alt_cp;

	@Column(name = "fte_cp")
	private String fte_cp;

	// FIN MODIF JC 13-04-2012
	@Column(name = "tipoice")
	private String tipoIce;

	@Column(name = "cod_cord")
	private String codCord;

	@Column(name = "clong_ie")
	private String clongIE;

	@Column(name = "clat_ie")
	private String clatIE;

	@Column(name = "referencia")
	private String referencia;

	@Column(name = "fte_area")
	private String fteArea;

	@Column(name = "clong_cp")
	private String clongCp;

	@Column(name = "clat_cp")
	private String clatCp;

	@Column(name = "cp_etnico")
	private String cpEtnico;

	@Column(name = "len_etnica")
	private String lenEtnica;

	@Column(name = "nlat_cp")
	private Float nlatCp;

	@Column(name = "nlong_cp")
	private Float nlongCp;

	@Column(name = "nlat_ie")
	private Float nlatIE;

	@Column(name = "nlong_ie")
	private Float nlongIE;

	@Column(name = "nzoom")
	private Integer nzoom;

	@Column(name = "localidad")
	private String localidad;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_car", referencedColumnName = "id")
	private Caracteristica caracteristica;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "gestion", referencedColumnName = "id")
	private Gestion gestion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dis_vrae", referencedColumnName = "id")
	private DisVrae disVrae;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dis_crecer", referencedColumnName = "id")
	private DisCrecer disCrecer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dis_juntos", referencedColumnName = "id")
	private DisJuntos disJuntos;

	@Column(name = "dre")
	private String dre;

	@Column(name = "progise")
	private String progise;

	@Column(name = "codinst")
	private String codinst;

	// imendoza 20170306 inicio
	@Column(name = "sienvio")
	private String sienvio;
	
	/*
	@OneToMany(orphanRemoval = true)
	@JoinTable(catalog = "info2018", name = "estadbas18", joinColumns = {
		@JoinColumn(name = "cod_mod",referencedColumnName = "cod_mod"),
		@JoinColumn(name = "anexo",referencedColumnName = "anexo")
	})*/
	
	@OneToMany
	@JoinColumns({
		@JoinColumn(name = "cod_mod",referencedColumnName = "cod_mod"),
		@JoinColumn(name = "anexo",referencedColumnName = "anexo")
	})
	private List<Estadistica> censoestadistica;

	public Institucion() {
	}

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getSienvio() {
		return sienvio;
	}

	public void setSienvio(String sienvio) {
		this.sienvio = sienvio;
	}
	// imendoza 20170306 fin

	/*
	 * public Institucion(String codMod, String anexo) { this.codMod = codMod;
	 * this.anexo = anexo; }
	 * 
	 * 
	 */
	public Forma getForma() {
		return forma;
	}

	public void setForma(Forma forma) {
		this.forma = forma;
	}

	public String getMcenso() {
		return mcenso;
	}

	public void setMcenso(String mcenso) {
		this.mcenso = mcenso;
	}

	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	public String getDirCen() {
		return dirCen;
	}

	public void setDirCen(String dirCen) {
		this.dirCen = dirCen;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPagweb() {
		return pagweb;
	}

	public void setPagweb(String pagweb) {
		this.pagweb = pagweb;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public SituacionDirector getSituacionDirector() {
		return situacionDirector;
	}

	public void setSituacionDirector(SituacionDirector situacionDirector) {
		this.situacionDirector = situacionDirector;
	}

	public Distrito getDistrito() {
		return distrito;
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}

	public Ugel getUgel() {
		return ugel;
	}

	public void setUgel(Ugel ugel) {
		this.ugel = ugel;
	}

	public Igel getIgel() {
		return igel;
	}

	public void setIgel(Igel igel) {
		this.igel = igel;
	}

	public String getTipoigel() {
		return tipoigel;
	}

	public void setTipoigel(String tipoigel) {
		this.tipoigel = tipoigel;
	}

	public String getCenPob() {
		return cenPob;
	}

	public void setCenPob(String cenPob) {
		this.cenPob = cenPob;
	}

	public String getCodccpp() {
		return codccpp;
	}

	public void setCodccpp(String codccpp) {
		this.codccpp = codccpp;
	}

	public GestionDependencia getGestionDependencia() {
		return gestionDependencia;
	}

	public void setGestionDependencia(GestionDependencia gestionDependencia) {
		this.gestionDependencia = gestionDependencia;
	}

	public NivelModalidad getNivelModalidad() {
		return nivelModalidad;
	}

	public void setNivelModalidad(NivelModalidad nivelModalidad) {
		this.nivelModalidad = nivelModalidad;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public String getProgdist() {
		return progdist;
	}

	public void setProgdist(String progdist) {
		this.progdist = progdist;
	}

	public String getProgarti() {
		return progarti;
	}

	public void setProgarti(String progarti) {
		this.progarti = progarti;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getComenta() {
		return comenta;
	}

	public void setComenta(String comenta) {
		this.comenta = comenta;
	}

	public String getFechareg() {
		return fechareg;
	}

	public void setFechareg(String fechareg) {
		this.fechareg = fechareg;
	}

	public String getFecharet() {
		return fecharet;
	}

	public void setFecharet(String fecharet) {
		this.fecharet = fecharet;
	}

	public Area getAreaSig() {
		return areaSig;
	}

	public void setAreaSig(Area areaSig) {
		this.areaSig = areaSig;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	/*
	 * public List<Esbas2009> getEsbas2009() { return esbas2009; }
	 * 
	 * public void setEsbas2009(List<Esbas2009> esbas2009) { this.esbas2009 =
	 * esbas2009; }
	 */
	/*
	 * @Override public boolean equals(Object obj) { if (obj == null) { return
	 * false; } if (getClass() != obj.getClass()) { return false; } final
	 * Institucion other = (Institucion) obj; if ((this.codMod == null) ?
	 * (other.codMod != null) : !this.codMod.equals(other.codMod)) { return false; }
	 * if ((this.anexo == null) ? (other.anexo != null) :
	 * !this.anexo.equals(other.anexo)) { return false; } return true; }
	 * 
	 * @Override public int hashCode() { int hash = 7; hash = 97 * hash +
	 * (this.codMod != null ? this.codMod.hashCode() : 0); hash = 97 * hash +
	 * (this.anexo != null ? this.anexo.hashCode() : 0); return hash; }
	 */
	/*
	 * public List<Estadistica> getEstadistica() { return estadistica; }
	 * 
	 * public void setEstadistica(List<Estadistica> estadistica) { this.estadistica
	 * = estadistica; }
	 */
	/**
	 * @return the metodo
	 */
	public String getMetodo() {
		return metodo;
	}

	/**
	 * @param metodo the metodo to set
	 */
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	/**
	 * @return the contjesc
	 */
	public String getContjesc() {
		return contjesc;
	}

	/**
	 * @param contjesc the contjesc to set
	 */
	public void setContjesc(String contjesc) {
		this.contjesc = contjesc;
	}

	/**
	 * @return the fecharea
	 */
	public String getFecharea() {
		return fecharea;
	}

	/**
	 * @param fecharea the fecharea to set
	 */
	public void setFecharea(String fecharea) {
		this.fecharea = fecharea;
	}

	/**
	 * @return the codcp_inei
	 */
	public String getCodcp_inei() {
		return codcp_inei;
	}

	/**
	 * @param codcp_inei the codcp_inei to set
	 */
	public void setCodcp_inei(String codcp_inei) {
		this.codcp_inei = codcp_inei;
	}

	/**
	 * @return the tipoprog
	 */
/*
	public Tipoprog getTipoprog() {
		return tipoprog;
	}

	public void setTipoprog(Tipoprog tipoprog) {
		this.tipoprog = tipoprog;
	}
*/
	public String getTipoprog() {
		return tipoprog;
	}

	public void setTipoprog(String tipoprog) {
		this.tipoprog = tipoprog;
	}

	/**
	 * @return the alt_cp
	 */
	public Integer getAlt_cp() {
		return alt_cp;
	}


	/**
	 * @param alt_cp the alt_cp to set
	 */
	public void setAlt_cp(Integer alt_cp) {
		this.alt_cp = alt_cp;
	}

	/**
	 * @return the fte_cp
	 */
	public String getFte_cp() {
		return fte_cp;
	}

	/**
	 * @param fte_cp the fte_cp to set
	 */
	public void setFte_cp(String fte_cp) {
		this.fte_cp = fte_cp;
	}

	/**
	 * @return the tipoIce
	 */
	public String getTipoIce() {
		return tipoIce;
	}

	/**
	 * @param tipoIce the tipoIce to set
	 */
	public void setTipoIce(String tipoIce) {
		this.tipoIce = tipoIce;
	}

	/**
	 * @return the codCord
	 */
	public String getCodCord() {
		return codCord;
	}

	/**
	 * @param codCord the codCord to set
	 */
	public void setCodCord(String codCord) {
		this.codCord = codCord;
	}

	/**
	 * @return the clongIE
	 */
	public String getClongIE() {
		return clongIE;
	}

	/**
	 * @param clongIE the clongIE to set
	 */
	public void setClongIE(String clongIE) {
		this.clongIE = clongIE;
	}

	/**
	 * @return the clatIE
	 */
	public String getClatIE() {
		return clatIE;
	}

	/**
	 * @param clatIE the clatIE to set
	 */
	public void setClatIE(String clatIE) {
		this.clatIE = clatIE;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the fteArea
	 */
	public String getFteArea() {
		return fteArea;
	}

	/**
	 * @param fteArea the fteArea to set
	 */
	public void setFteArea(String fteArea) {
		this.fteArea = fteArea;
	}

	/**
	 * @return the clongCp
	 */
	public String getClongCp() {
		return clongCp;
	}

	/**
	 * @param clongCp the clongCp to set
	 */
	public void setClongCp(String clongCp) {
		this.clongCp = clongCp;
	}

	/**
	 * @return the clatCp
	 */
	public String getClatCp() {
		return clatCp;
	}

	/**
	 * @param clatCp the clatCp to set
	 */
	public void setClatCp(String clatCp) {
		this.clatCp = clatCp;
	}

	/**
	 * @return the cpEtnico
	 */
	public String getCpEtnico() {
		return cpEtnico;
	}

	/**
	 * @param cpEtnico the cpEtnico to set
	 */
	public void setCpEtnico(String cpEtnico) {
		this.cpEtnico = cpEtnico;
	}

	/**
	 * @return the lenEtnica
	 */
	public String getLenEtnica() {
		return lenEtnica;
	}

	/**
	 * @param lenEtnica the lenEtnica to set
	 */
	public void setLenEtnica(String lenEtnica) {
		this.lenEtnica = lenEtnica;
	}

	/**
	 * @return the nlatCp
	 */
	public Float getNlatCp() {
		return nlatCp;
	}

	/**
	 * @param nlatCp the nlatCp to set
	 */
	public void setNlatCp(Float nlatCp) {
		this.nlatCp = nlatCp;
	}

	/**
	 * @return the nlongCp
	 */
	public Float getNlongCp() {
		return nlongCp;
	}

	/**
	 * @param nlongCp the nlongCp to set
	 */
	public void setNlongCp(Float nlongCp) {
		this.nlongCp = nlongCp;
	}

	/**
	 * @return the nlatIE
	 */
	public Float getNlatIE() {
		return nlatIE;
	}

	/**
	 * @param nlatIE the nlatIE to set
	 */
	public void setNlatIE(Float nlatIE) {
		this.nlatIE = nlatIE;
	}

	/**
	 * @return the nlongIE
	 */
	public Float getNlongIE() {
		return nlongIE;
	}

	/**
	 * @param nlongIE the nlongIE to set
	 */
	public void setNlongIE(Float nlongIE) {
		this.nlongIE = nlongIE;
	}

	/**
	 * @return the nzoom
	 */
	public Integer getNzoom() {
		return nzoom;
	}

	/**
	 * @param nzoom the nzoom to set
	 */
	public void setNzoom(Integer nzoom) {
		this.nzoom = nzoom;
	}

	/**
	 * @return the localidad
	 */
	public String getLocalidad() {
		return localidad;
	}

	/**
	 * @param localidad the localidad to set
	 */
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	/**
	 * @return the caracteristica
	 */
	public Caracteristica getCaracteristica() {
		return caracteristica;
	}

	/**
	 * @param caracteristica the caracteristica to set
	 */
	public void setCaracteristica(Caracteristica caracteristica) {
		this.caracteristica = caracteristica;
	}

	/**
	 * @return the gestion
	 */
	public Gestion getGestion() {
		return gestion;
	}

	/**
	 * @param gestion the gestion to set
	 */
	public void setGestion(Gestion gestion) {
		this.gestion = gestion;
	}

	/**
	 * @return the disVrae
	 */
	public DisVrae getDisVrae() {
		return disVrae;
	}

	/**
	 * @param disVrae the disVrae to set
	 */
	public void setDisVrae(DisVrae disVrae) {
		this.disVrae = disVrae;
	}

	/**
	 * @return the disCrecer
	 */
	public DisCrecer getDisCrecer() {
		return disCrecer;
	}

	/**
	 * @param disCrecer the disCrecer to set
	 */
	public void setDisCrecer(DisCrecer disCrecer) {
		this.disCrecer = disCrecer;
	}

	/**
	 * @return the disJuntos
	 */
	public DisJuntos getDisJuntos() {
		return disJuntos;
	}

	/**
	 * @param disJuntos the disJuntos to set
	 */
	public void setDisJuntos(DisJuntos disJuntos) {
		this.disJuntos = disJuntos;
	}

	/**
	 * @return the dre
	 */
	public String getDre() {
		return dre;
	}

	/**
	 * @param dre the dre to set
	 */
	public void setDre(String dre) {
		this.dre = dre;
	}

	/**
	 * @return the progise
	 */
	public String getProgise() {
		return progise;
	}

	/**
	 * @param progise the progise to set
	 */
	public void setProgise(String progise) {
		this.progise = progise;
	}

	public String getCodinst() {
		return codinst;
	}

	public void setCodinst(String codinst) {
		this.codinst = codinst;
	}

	public List<Estadistica> getCensoestadistica() {
		return censoestadistica;
	}

	public void setCensoestadistica(List<Estadistica> censoestadistica) {
		this.censoestadistica = censoestadistica;
	}


	

}
