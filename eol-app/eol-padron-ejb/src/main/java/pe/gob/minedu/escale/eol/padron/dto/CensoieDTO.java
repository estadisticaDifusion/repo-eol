package pe.gob.minedu.escale.eol.padron.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.InfoA;
import pe.gob.minedu.escale.eol.padron.domain.InfoB;
import pe.gob.minedu.escale.eol.padron.domain.InfoF;
import pe.gob.minedu.escale.eol.padron.domain.InfoC;
import pe.gob.minedu.escale.eol.padron.domain.InfoD;
import pe.gob.minedu.escale.eol.padron.domain.InfoG;
import pe.gob.minedu.escale.eol.padron.domain.InfoK;
import pe.gob.minedu.escale.eol.padron.domain.InfoT;
import pe.gob.minedu.escale.eol.padron.domain.InfoM;
import pe.gob.minedu.escale.eol.padron.domain.InfoE;
import pe.gob.minedu.escale.eol.padron.domain.InfoL;

//@XmlRootElement
public class CensoieDTO {
	
	private List<InfoA> infoInicial;
	private List<InfoB> infoPrimaria;
	private List<InfoF> infoSecundaria;
	private List<InfoC> infoPrimariaAdultos;
	private List<InfoD> infoBasicaAlternativa;
	private List<InfoG> infoSecundariaAdultos;
	private List<InfoK> infoISP;
	private List<InfoT> infoIST;
	private List<InfoM> infoSuperiorArtistica;
	private List<InfoE> infoEducacionEspecial;
	private List<InfoL> infoCETPRO;
	
	public CensoieDTO() {		
	}

	public List<InfoA> getInfoInicial() {
		return infoInicial;
	}

	public void setInfoInicial(List<InfoA> infoInicial) {
		this.infoInicial = infoInicial;
	}

	public List<InfoB> getInfoPrimaria() {
		return infoPrimaria;
	}

	public void setInfoPrimaria(List<InfoB> infoPrimaria) {
		this.infoPrimaria = infoPrimaria;
	}

	public List<InfoF> getInfoSecundaria() {
		return infoSecundaria;
	}

	public void setInfoSecundaria(List<InfoF> infoSecundaria) {
		this.infoSecundaria = infoSecundaria;
	}

	public List<InfoC> getInfoPrimariaAdultos() {
		return infoPrimariaAdultos;
	}

	public void setInfoPrimariaAdultos(List<InfoC> infoPrimariaAdultos) {
		this.infoPrimariaAdultos = infoPrimariaAdultos;
	}

	public List<InfoD> getInfoBasicaAlternativa() {
		return infoBasicaAlternativa;
	}

	public void setInfoBasicaAlternativa(List<InfoD> infoBasicaAlternativa) {
		this.infoBasicaAlternativa = infoBasicaAlternativa;
	}

	public List<InfoG> getInfoSecundariaAdultos() {
		return infoSecundariaAdultos;
	}

	public void setInfoSecundariaAdultos(List<InfoG> infoSecundariaAdultos) {
		this.infoSecundariaAdultos = infoSecundariaAdultos;
	}

	public List<InfoK> getInfoISP() {
		return infoISP;
	}

	public void setInfoISP(List<InfoK> infoISP) {
		this.infoISP = infoISP;
	}

	public List<InfoT> getInfoIST() {
		return infoIST;
	}

	public void setInfoIST(List<InfoT> infoIST) {
		this.infoIST = infoIST;
	}

	public List<InfoM> getInfoSuperiorArtistica() {
		return infoSuperiorArtistica;
	}

	public void setInfoSuperiorArtistica(List<InfoM> infoSuperiorArtistica) {
		this.infoSuperiorArtistica = infoSuperiorArtistica;
	}

	public List<InfoE> getInfoEducacionEspecial() {
		return infoEducacionEspecial;
	}

	public void setInfoEducacionEspecial(List<InfoE> infoEducacionEspecial) {
		this.infoEducacionEspecial = infoEducacionEspecial;
	}

	public List<InfoL> getInfoCETPRO() {
		return infoCETPRO;
	}

	public void setInfoCETPRO(List<InfoL> infoCETPRO) {
		this.infoCETPRO = infoCETPRO;
	}
	
}
