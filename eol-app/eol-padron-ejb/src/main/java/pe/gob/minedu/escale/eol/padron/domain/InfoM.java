/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Diego Silva
 * 
 * Update Author: Ing. Oscar Mateo Date : 09/08/2019
 * 
 */
@Entity
public class InfoM implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cod_mod")
	private String codMod;

	// @Id
	@Column(name = "anexo")
	private String anexo;

	// @Id
	@Column(name = "niv_mod")
	private String nivMod;

	@Column(name = "matr1")
	private int matr01;
	
	@Column(name = "matr2")
	private int matr02;
	
	@Column(name = "matr3")
	private int matr03;
	
	@Column(name = "matr4")
	private int matr04;
	
	@Column(name = "matr5")
	private int matr05;
	
	@Column(name = "matr6")
	private int matr06;
	
	@Column(name = "matr7")
	private int matr07;
	
	@Column(name = "matr8")
	private int matr08;
	
	@Column(name = "matr9")
	private int matr09;
	
	@Column(name = "matr10")
	private int matr10;
	
	@Column(name = "secc01")
	private int secc01;
	
	@Column(name = "secc02")
	private int secc02;
	
	@Column(name = "secc03")
	private int secc03;
	
	@Column(name = "secc04")
	private int secc04;
	
	@Column(name = "secc05")
	private int secc05;
	
	@Column(name = "secc06")
	private int secc06;
	
	@Column(name = "secc07")
	private int secc07;
	
	@Column(name = "secc08")
	private int secc08;
	
	@Column(name = "secc09")
	private int secc09;
	
	@Column(name = "secc10")
	private int secc10;
	
	@Column(name = "talumno")
	private Integer talumno;
	
	@Column(name = "tseccion")
	private int tseccion;
	
	@Column(name = "tdocente")
	private int tdocente;

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	public int getMatr01() {
		return matr01;
	}

	public void setMatr01(int matr01) {
		this.matr01 = matr01;
	}

	public int getMatr02() {
		return matr02;
	}

	public void setMatr02(int matr02) {
		this.matr02 = matr02;
	}

	public int getMatr03() {
		return matr03;
	}

	public void setMatr03(int matr03) {
		this.matr03 = matr03;
	}

	public int getMatr04() {
		return matr04;
	}

	public void setMatr04(int matr04) {
		this.matr04 = matr04;
	}

	public int getMatr05() {
		return matr05;
	}

	public void setMatr05(int matr05) {
		this.matr05 = matr05;
	}

	public int getMatr06() {
		return matr06;
	}

	public void setMatr06(int matr06) {
		this.matr06 = matr06;
	}

	public int getMatr07() {
		return matr07;
	}

	public void setMatr07(int matr07) {
		this.matr07 = matr07;
	}

	public int getMatr08() {
		return matr08;
	}

	public void setMatr08(int matr08) {
		this.matr08 = matr08;
	}

	public int getMatr09() {
		return matr09;
	}

	public void setMatr09(int matr09) {
		this.matr09 = matr09;
	}

	public int getMatr10() {
		return matr10;
	}

	public void setMatr10(int matr10) {
		this.matr10 = matr10;
	}

	public int getSecc01() {
		return secc01;
	}

	public void setSecc01(int secc01) {
		this.secc01 = secc01;
	}

	public int getSecc02() {
		return secc02;
	}

	public void setSecc02(int secc02) {
		this.secc02 = secc02;
	}

	public int getSecc03() {
		return secc03;
	}

	public void setSecc03(int secc03) {
		this.secc03 = secc03;
	}

	public int getSecc04() {
		return secc04;
	}

	public void setSecc04(int secc04) {
		this.secc04 = secc04;
	}

	public int getSecc05() {
		return secc05;
	}

	public void setSecc05(int secc05) {
		this.secc05 = secc05;
	}

	public int getSecc06() {
		return secc06;
	}

	public void setSecc06(int secc06) {
		this.secc06 = secc06;
	}

	public int getSecc07() {
		return secc07;
	}

	public void setSecc07(int secc07) {
		this.secc07 = secc07;
	}

	public int getSecc08() {
		return secc08;
	}

	public void setSecc08(int secc08) {
		this.secc08 = secc08;
	}

	public int getSecc09() {
		return secc09;
	}

	public void setSecc09(int secc09) {
		this.secc09 = secc09;
	}

	public int getSecc10() {
		return secc10;
	}

	public void setSecc10(int secc10) {
		this.secc10 = secc10;
	}

	public Integer getTalumno() {
		return talumno;
	}

	public void setTalumno(Integer talumno) {
		this.talumno = talumno;
	}

	public int getTseccion() {
		return tseccion;
	}

	public void setTseccion(int tseccion) {
		this.tseccion = tseccion;
	}

	public int getTdocente() {
		return tdocente;
	}

	public void setTdocente(int tdocente) {
		this.tdocente = tdocente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((anexo == null) ? 0 : anexo.hashCode());
		result = prime * result + ((codMod == null) ? 0 : codMod.hashCode());
		result = prime * result + matr01;
		result = prime * result + matr02;
		result = prime * result + matr03;
		result = prime * result + matr04;
		result = prime * result + matr05;
		result = prime * result + matr06;
		result = prime * result + matr07;
		result = prime * result + matr08;
		result = prime * result + matr09;
		result = prime * result + matr10;
		result = prime * result + ((nivMod == null) ? 0 : nivMod.hashCode());
		result = prime * result + secc01;
		result = prime * result + secc02;
		result = prime * result + secc03;
		result = prime * result + secc04;
		result = prime * result + secc05;
		result = prime * result + secc06;
		result = prime * result + secc07;
		result = prime * result + secc08;
		result = prime * result + secc09;
		result = prime * result + secc10;
		result = prime * result + ((talumno == null) ? 0 : talumno.hashCode());
		result = prime * result + tdocente;
		result = prime * result + tseccion;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoM other = (InfoM) obj;
		if (anexo == null) {
			if (other.anexo != null)
				return false;
		} else if (!anexo.equals(other.anexo))
			return false;
		if (codMod == null) {
			if (other.codMod != null)
				return false;
		} else if (!codMod.equals(other.codMod))
			return false;
		if (matr01 != other.matr01)
			return false;
		if (matr02 != other.matr02)
			return false;
		if (matr03 != other.matr03)
			return false;
		if (matr04 != other.matr04)
			return false;
		if (matr05 != other.matr05)
			return false;
		if (matr06 != other.matr06)
			return false;
		if (matr07 != other.matr07)
			return false;
		if (matr08 != other.matr08)
			return false;
		if (matr09 != other.matr09)
			return false;
		if (matr10 != other.matr10)
			return false;
		if (nivMod == null) {
			if (other.nivMod != null)
				return false;
		} else if (!nivMod.equals(other.nivMod))
			return false;
		if (secc01 != other.secc01)
			return false;
		if (secc02 != other.secc02)
			return false;
		if (secc03 != other.secc03)
			return false;
		if (secc04 != other.secc04)
			return false;
		if (secc05 != other.secc05)
			return false;
		if (secc06 != other.secc06)
			return false;
		if (secc07 != other.secc07)
			return false;
		if (secc08 != other.secc08)
			return false;
		if (secc09 != other.secc09)
			return false;
		if (secc10 != other.secc10)
			return false;
		if (talumno == null) {
			if (other.talumno != null)
				return false;
		} else if (!talumno.equals(other.talumno))
			return false;
		if (tdocente != other.tdocente)
			return false;
		if (tseccion != other.tseccion)
			return false;
		return true;
	}

}
