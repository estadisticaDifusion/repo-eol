/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.dto.entities.UgelDTO;
import pe.gob.minedu.escale.eol.padron.domain.Ugel;

/**
 *
 * @author Ing. Oscar Mateo
 */
@Local
public interface UgelLocal {
	public Ugel findById(String codigoUgel);

	UgelDTO getUgel(String codigoUgel);
	
	List<UgelDTO> gestListUgel(String codigodre);
}
