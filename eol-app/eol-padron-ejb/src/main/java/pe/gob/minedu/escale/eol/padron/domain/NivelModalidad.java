/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name="v_niv_mod")
@XmlRootElement(name = "nivelModalidad")

//@DiscriminatorValue(value = "niv_mod")
public class NivelModalidad extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1652214742784977463L;
}
