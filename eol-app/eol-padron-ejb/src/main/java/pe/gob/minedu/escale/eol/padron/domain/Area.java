/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@Entity
@XmlRootElement(name = "area")
@Table(name = "v_cod_area")
// @DiscriminatorValue(value = "cod_area")
@NamedQueries({ @NamedQuery(name = "Area.findAll", query = "SELECT c FROM Area c ORDER BY c.orden"),
		@NamedQuery(name = "Area.findById", query = "SELECT c FROM Area c WHERE c.idCodigo=:id") })
public class Area extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4980412869375442273L;
}
