/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "disVrae")

@Entity
@Table(name="v_dis_vrae")
//@DiscriminatorValue(value = "v_gestion")
@NamedQueries({
    @NamedQuery(name = "DisVrae.findAll", query = "SELECT c FROM DisVrae c ORDER BY c.orden"),
    @NamedQuery(name = "DisVrae.findById", query = "SELECT c FROM DisVrae c WHERE c.idCodigo=:id")})
public class DisVrae extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8375142806474395120L;
}
