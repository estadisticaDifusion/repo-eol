package pe.gob.minedu.escale.eol.padron.ejb;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.entities.DistritoDTO;
import pe.gob.minedu.escale.eol.padron.domain.Distrito;

public interface DistritoLocal {

	public Distrito findById(String codigoDistrito);
	
	DistritoDTO getDistrito(String codigoDistrito);
	
	List<DistritoDTO> getListDistritoDTO(String codigoProvincia);
}
