package pe.gob.minedu.escale.eol.padron.mappers;

import pe.gob.minedu.escale.eol.dto.entities.DistritoDTO;
import pe.gob.minedu.escale.eol.dto.entities.ProvinciaDTO;
import pe.gob.minedu.escale.eol.dto.entities.RegionDTO;
import pe.gob.minedu.escale.eol.padron.domain.Distrito;
import pe.gob.minedu.escale.eol.padron.domain.Provincia;
import pe.gob.minedu.escale.eol.padron.domain.Region;

public class MapperUbicacion {

	public static RegionDTO setRegionDomainDto(Region region) {
		
		RegionDTO regionDTO = new RegionDTO();
		
		if(region != null) {
			regionDTO.setIdRegion(region.getIdRegion());
			regionDTO.setNombreRegion(region.getNombreRegion());
			regionDTO.setPointX(region.getPointX());
			regionDTO.setPointY(region.getPointY());
			regionDTO.setZoom(region.getZoom());
		}
		
		return regionDTO;
	} 
	
	public static ProvinciaDTO setProvinciaDomainDto(Provincia provincia) {
		
		ProvinciaDTO provinciaDTO = new ProvinciaDTO();
		
		if(provincia != null) {
			provinciaDTO.setIdProvincia(provincia.getIdProvincia());
			provinciaDTO.setNombreProvincia(provincia.getNombreProvincia());
			provinciaDTO.setRegion(setRegionDomainDto(provincia.getRegion()));
			provinciaDTO.setPointX(provincia.getPointX());
			provinciaDTO.setPointY(provincia.getPointY());
			provinciaDTO.setZoom(provincia.getZoom());
		}
		
		return provinciaDTO;
	}
	
	public static DistritoDTO setDistritoDomainDto(Distrito distrito) {
		DistritoDTO distritoDTO = new DistritoDTO();
		
		if(distrito != null) {
			distritoDTO.setIdDistrito(distrito.getIdDistrito());
			distritoDTO.setNombreDistrito(distrito.getNombreDistrito());
			distritoDTO.setProvincia(setProvinciaDomainDto(distrito.getProvincia()));
			distritoDTO.setPointX(distrito.getPointX());
			distritoDTO.setPointY(distrito.getPointY());
			distritoDTO.setZoom(distrito.getZoom());
		}
		
		return distritoDTO;
	}
	
}
