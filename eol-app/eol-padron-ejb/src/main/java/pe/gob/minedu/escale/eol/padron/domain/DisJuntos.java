/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "disJuntos")

@Entity
@Table(name="v_dis_juntos")
//@DiscriminatorValue(value = "v_gestion")
@NamedQueries({
    @NamedQuery(name = "DisJuntos.findAll", query = "SELECT c FROM DisJuntos c ORDER BY c.orden"),
    @NamedQuery(name = "DisJuntos.findById", query = "SELECT c FROM DisJuntos c WHERE c.idCodigo=:id")})
public class DisJuntos extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3772788428394756299L;
}
