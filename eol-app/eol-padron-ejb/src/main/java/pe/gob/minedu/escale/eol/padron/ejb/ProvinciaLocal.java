package pe.gob.minedu.escale.eol.padron.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.dto.entities.ProvinciaDTO;
import pe.gob.minedu.escale.eol.padron.domain.Provincia;

@Local
public interface ProvinciaLocal {

public Provincia findById(String codigoProvincia);
	
ProvinciaDTO getProvincia(String codigoProvincia);

List<ProvinciaDTO> getListProvinciaDTO(String codigoRegion);
	
}
