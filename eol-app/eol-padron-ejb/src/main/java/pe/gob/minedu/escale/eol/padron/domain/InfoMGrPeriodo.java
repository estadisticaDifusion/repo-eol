package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InfoMGrPeriodo implements Serializable {

	private static final long serialVersionUID = 1L;
	/*
	 * @EmbeddedId private InfoMGrPeriodoId infoMGrPeriodoId;
	 */

	@Id
	@Column(name = "cod_mod")
	private String codMod;

	// @Id
	@Column(name = "anexo")
	private String anexo;

	// @Id
	@Column(name = "niv_mod")
	private String nivMod;

	@Column(name = "colum1")
	private String colum1;
	@Column(name = "colum2")
	private String colum2;
	@Column(name = "colum3")
	private String colum3;
	@Column(name = "colum4")
	private String colum4;
	@Column(name = "colum5")
	private String colum5;
	@Column(name = "colum6")
	private String colum6;
	@Column(name = "colum7")
	private String colum7;
	@Column(name = "colum8")
	private String colum8;
	@Column(name = "colum9")
	private String colum9;
	@Column(name = "colum10")
	private String colum10;
	/*
	 * public InfoMGrPeriodoId getInfoMGrPeriodoId() { return infoMGrPeriodoId; }
	 * 
	 * public void setInfoMGrPeriodoId(InfoMGrPeriodoId infoMGrPeriodoId) {
	 * this.infoMGrPeriodoId = infoMGrPeriodoId; }
	 */

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	public String getColum1() {
		return colum1;
	}

	public void setColum1(String colum1) {
		this.colum1 = colum1;
	}

	public String getColum10() {
		return colum10;
	}

	public void setColum10(String colum10) {
		this.colum10 = colum10;
	}

	public String getColum2() {
		return colum2;
	}

	public void setColum2(String colum2) {
		this.colum2 = colum2;
	}

	public String getColum3() {
		return colum3;
	}

	public void setColum3(String colum3) {
		this.colum3 = colum3;
	}

	public String getColum4() {
		return colum4;
	}

	public void setColum4(String colum4) {
		this.colum4 = colum4;
	}

	public String getColum5() {
		return colum5;
	}

	public void setColum5(String colum5) {
		this.colum5 = colum5;
	}

	public String getColum6() {
		return colum6;
	}

	public void setColum6(String colum6) {
		this.colum6 = colum6;
	}

	public String getColum7() {
		return colum7;
	}

	public void setColum7(String colum7) {
		this.colum7 = colum7;
	}

	public String getColum8() {
		return colum8;
	}

	public void setColum8(String colum8) {
		this.colum8 = colum8;
	}

	public String getColum9() {
		return colum9;
	}

	public void setColum9(String colum9) {
		this.colum9 = colum9;
	}

}
