///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package pe.gob.minedu.escale.eol.padron.ejb;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//import java.util.Map;
//
//import javax.ejb.Local;
//import pe.gob.minedu.escale.eol.dbf.DbfFile.Field;
//import pe.gob.minedu.escale.eol.padron.importar.dao.domain.TableField;
//
///**
// *
// *
// * @author MHUAMANI
// * @version 1.0
// */
//@Local
//public interface ActualizarPadronLocal {
//	
//	void actualizar(File file,List<Map<String,String>> mapSIG) throws IOException;
//
//	Field[] obtenerCampos(File uploadedFile);
//	
//	List<TableField> obtenerCamposTabla(String tabla);
//	
//}
