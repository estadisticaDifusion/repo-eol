/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.ejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.dto.entities.DistritoDTO;
import pe.gob.minedu.escale.eol.dto.entities.UgelDTO;
import pe.gob.minedu.escale.eol.padron.domain.Distrito;
import pe.gob.minedu.escale.eol.padron.domain.Ugel;
import pe.gob.minedu.escale.eol.padron.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.padron.ejb.UgelLocal;
import pe.gob.minedu.escale.eol.padron.mappers.MapperUbicacion;
import pe.gob.minedu.escale.eol.padron.mappers.MapperUgel;

/**
 *
 * @author Ing. Oscar Mateo
 */
//@Stateless
@Singleton
public class UgelFacade extends AbstractFacade<Ugel> implements UgelLocal {

    private static final Logger logger = Logger.getLogger(InstitucionFacade.class);

    @PersistenceContext(unitName = "padron-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UgelFacade() {
        super(Ugel.class);
    }

    public Ugel findById(String codigoUgel) {
        logger.info(":: UgelFacade.findById :: Starting execution...");
        Ugel ugel = em.find(Ugel.class, codigoUgel);
        logger.info(" ugel = "+ugel);
        logger.info(":: UgelFacade.findById :: Execution finish.");
        return ugel;
    }
    
    public UgelDTO getUgel(String codigoUgel) {
        logger.info(":: UgelFacade.getUgel :: Starting execution...");
        Ugel ugel = em.find(Ugel.class, codigoUgel);
        logger.info(" ugel = "+ugel);
    	UgelDTO ugelDTO = MapperUgel.setUgelDomainToDto(ugel);
    	logger.info(":: UgelFacade.getUgel :: Execution finish.");
    	return ugelDTO;
    }

	@Override
	public List<UgelDTO> gestListUgel(String codigodre) {
		List<UgelDTO> listugelDTO = new ArrayList<UgelDTO>();
		
		String sql= "select e from Ugel e where e.direccionRegional.id= '"+codigodre+"' order by e.idUgel ";
		
		List<Ugel> lista = em.createQuery(sql).getResultList(); 
		
		for(Ugel rg : lista) {
			listugelDTO.add(MapperUgel.setUgelDomainToDto(rg));
		}
		
		return listugelDTO;
	}
    
    
    
}
