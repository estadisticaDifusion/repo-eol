package pe.gob.minedu.escale.eol.padron.ejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.dto.entities.ProvinciaDTO;
import pe.gob.minedu.escale.eol.padron.domain.Provincia;
import pe.gob.minedu.escale.eol.padron.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.padron.ejb.ProvinciaLocal;
import pe.gob.minedu.escale.eol.padron.mappers.MapperUbicacion;

@Singleton
public class ProvinciaFacade extends AbstractFacade<Provincia> implements ProvinciaLocal {

	private static final Logger logger = Logger.getLogger(ProvinciaFacade.class);
	
	@PersistenceContext(unitName = "padron-ejbPU")
    private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public ProvinciaFacade() {
		super(Provincia.class);
	}

	@Override
	public Provincia findById(String codigoProvincia) {
		logger.info(":: ProvinciaFacade.findById :: Starting execution...");
		Provincia provincia = em.find(Provincia.class, codigoProvincia);
		logger.info(" provincia = "+provincia);
        logger.info(":: Provincia.findById :: Execution finish.");
		
		return provincia;
	}

	@Override
	public ProvinciaDTO getProvincia(String codigoProvincia) {
		logger.info(":: ProvinciaFacade.findById :: Starting execution...");
		Provincia provincia = em.find(Provincia.class, codigoProvincia);
		logger.info(" provincia = "+provincia);
		ProvinciaDTO provinciaDTO = MapperUbicacion.setProvinciaDomainDto(provincia);
        logger.info(":: Provincia.findById :: Execution finish.");
		
		return provinciaDTO;
	}

	@Override
	public List<ProvinciaDTO> getListProvinciaDTO(String codigoRegion) {
		List<ProvinciaDTO> listprovinciaDTO = new ArrayList<ProvinciaDTO>();
		
		String sql= "select e from Provincia e where e.region.idRegion= '"+codigoRegion+"' order by e.idProvincia ";
		
		List<Provincia> lista = em.createQuery(sql).getResultList(); 
		
		for(Provincia rg : lista) {
			listprovinciaDTO.add(MapperUbicacion.setProvinciaDomainDto(rg));
		}
		
		return listprovinciaDTO;
	}

	

}
