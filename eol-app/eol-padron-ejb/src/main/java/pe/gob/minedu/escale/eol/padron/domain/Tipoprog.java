/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */

@Entity
@Table(name="v_tipoprog")
@XmlRootElement(name = "tipoprog")
public class Tipoprog extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8778801746761636155L;
}
