/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "igels")
public class Igel implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8918272103304358351L;
	@Id
    @Column(name = "codigel")
    private String idIgel;
    @Column(name = "nomigel")
    private String nombreUgel;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id_dre", name = "id_dre")
    private DireccionRegional direccionRegional;

    public Igel() {
    }

    public DireccionRegional getDireccionRegional() {
        return direccionRegional;
    }

    public void setDireccionRegional(DireccionRegional direccionRegional) {
        this.direccionRegional = direccionRegional;
    }

    public String getIdIgel() {
        return idIgel;
    }

    public void setIdIgel(String idIgel) {
        this.idIgel = idIgel;
    }

    public String getNombreUgel() {
        return nombreUgel;
    }

    public void setNombreUgel(String nombreUgel) {
        this.nombreUgel = nombreUgel;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Igel other = (Igel) obj;
        if ((this.idIgel == null) ? (other.idIgel != null) : !this.idIgel.equals(other.idIgel)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.idIgel != null ? this.idIgel.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return direccionRegional == null ? null : (direccionRegional.getNombreDre() + " / " + nombreUgel);
    }
}
