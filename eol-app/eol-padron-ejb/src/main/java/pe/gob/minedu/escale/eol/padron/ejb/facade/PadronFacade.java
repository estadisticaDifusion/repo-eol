/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.ejb.facade;

import static pe.gob.minedu.escale.eol.padron.constant.Constantes.USUARIO_DRE;
import static pe.gob.minedu.escale.eol.padron.constant.Constantes.USUARIO_DRE_CALLAO;
import static pe.gob.minedu.escale.eol.padron.constant.Constantes.USUARIO_DRE_LIMA_METRO;
import static pe.gob.minedu.escale.eol.padron.constant.Constantes.USUARIO_DRE_LIMA_PROV;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;

/**
 *
 * @author JMATAMOROS
 */

@Singleton
public class PadronFacade extends AbstractFacade<CentroEducativo> implements PadronLocal {

	private final Logger logger = Logger.getLogger(PadronFacade.class);

	private String UNION_SQL = "UNION";
	private String UNION_ALL_SQL = "UNION ALL";
	private String SQL_CUENTA_CENTROS = " select a.codigel, count(distinct a.cod_mod,a.anexo) as cuenta "
			+ " from padron a "
			+ " where !(SUBSTRING(niv_mod,1,1)='A' AND anexo !='0') and SUBSTRING(tipoice,1,1)='1' and a.codigel like '%1$s%%' "
			+ " %2$s  and  a.mcenso='1' and a.estado='1' group by 1 " + UNION_SQL;
	private String SQL_CUENTA_CENTROS_EXP = " select a.codigel, count(distinct a.cod_mod,a.anexo) as cuenta "
			+ " from padron a "
			+ " where !(SUBSTRING(niv_mod,1,1)='A' AND anexo !='0') and a.mcenso='1' and a.estado='1' and SUBSTRING(a.tipoice,1,1)='1' "
			+ " %1$s %2$s    group by 1 " + UNION_SQL;

	private String SQL_CUENTA_CENTROS_EXP_BY_LOCAL = " select a.codooii, count(distinct a.codlocal) as cuenta "
			+ " from padron a "
			+ " where a.niv_mod!='A5'  and a.mcenso='1' and a.estado='1' and SUBSTRING(a.tipoice,1,1)='1' "
			+ " %1$s %2$s    group by 1 ";

	@PersistenceContext(unitName = "padron-ejbPU")
	private EntityManager em;

	public PadronFacade() {
		super(CentroEducativo.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public CentroEducativo obtainByCodMod(String codMod, String anexo) {
		String cadSQL = "SELECT p FROM CentroEducativo p WHERE p.codMod=:codMod AND p.anexo=:anexo";
		Query query = em.createQuery(cadSQL);
		query.setParameter("codMod", codMod);
		query.setParameter("anexo", anexo);
		try {
			return (CentroEducativo) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public List<CentroEducativo> getCodigosModularesByCodLocal(String codLocal) {
		Query q = em.createQuery(
				"SELECT p FROM CentroEducativo p WHERE p.codlocal=:codlocal AND p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1'")
				.setParameter("codlocal", codLocal);

		List<CentroEducativo> ces = q.getResultList();
		return ces;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentros(String idUgel, String act) {

		Query query = null;
		String filtroUgel = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String filtAct = act.equals("CENSO-RESULTADO")
				? " AND a.tipoprog  NOT IN ('1','2','3','5','6','7','8','9') AND a.niv_mod!='A1' "
				: "";
		StringBuilder sb = new StringBuilder();

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codigel like '1501%' or a.codigel like '1510%')" + filtAct, ""));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codigel like '1501%' or a.codigel like '1510%')" + filtAct, " and a.progarti='1' "));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codigel like '1502%' or a.codigel like '1520%')" + filtAct, ""));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codigel like '1502%' or a.codigel like '1520%')" + filtAct, " and a.progarti='1' "));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codigel like '0701%' or a.codigel like '0700%')" + filtAct, ""));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codigel like '0701%' or a.codigel like '0700%')" + filtAct, " and a.progarti='1' "));
		} else {
			sb.append(String.format(SQL_CUENTA_CENTROS, filtroUgel, filtAct));
			sb.append(String.format(SQL_CUENTA_CENTROS, filtroUgel, " and a.progarti='1' " + filtAct));
		}

		sb.setLength(sb.length() - UNION_SQL.length());
		String sql = String.format("select codigel,sum(cuenta) as cuenta from (%1s) a group by 1 order by 1 ",
				sb.toString());

		query = em.createNativeQuery(sql);

		List<Object[]> res = query.getResultList();
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentros2014(String idUgel, String act) {

		Query query = null;
		String filtroUgel = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String filtAct = act.equals("CENSO-RESULTADO")
				? " AND ( a.niv_mod NOT IN ('A1','E0') AND a.tipoprog NOT IN ('11','12','13') ) "
				: "";
		StringBuilder sb = new StringBuilder();
		String filtroPadron = "";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			filtroPadron = " and (a.codooii like '1501%' or a.codooii like '1510%')";
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '1501%' or a.codooii like '1510%')" + filtAct, ""));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '1501%' or a.codooii like '1510%')" + filtAct, " and a.progarti='1' "));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '1501%' or a.codooii like '1510%')" + filtAct, " and a.progise='1' "));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			filtroPadron = " and (a.codooii like '1502%' or a.codooii like '1520%')";
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '1502%' or a.codooii like '1520%')" + filtAct, ""));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '1502%' or a.codooii like '1520%')" + filtAct, " and a.progarti='1' "));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '1502%' or a.codooii like '1520%')" + filtAct, " and a.progise='1' "));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			filtroPadron = " and (a.codooii like '0701%' or a.codooii like '0700%')";
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '0701%' or a.codooii like '0700%')" + filtAct, ""));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '0701%' or a.codooii like '0700%')" + filtAct, " and a.progarti='1' "));
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP,
					" and (a.codooii like '0701%' or a.codooii like '0700%')" + filtAct, " and a.progise='1' "));
		} else {
			sb.append(String.format(SQL_CUENTA_CENTROS, filtroUgel, filtAct));
			sb.append(String.format(SQL_CUENTA_CENTROS, filtroUgel, " and a.progarti='1' " + filtAct));
			sb.append(String.format(SQL_CUENTA_CENTROS, filtroUgel, " and a.progise='1' " + filtAct));
			filtroPadron = String.format("and a.codigel like '%1$s%%' ", filtroUgel.toString());
		}

		sb.setLength(sb.length() - UNION_SQL.length());
		String sql = String.format("select codigel,sum(cuenta) as cuenta from (%1s) a group by 1 ", sb.toString());
		String sqlAgrupado = String.format(
				"select a.codigel,IFNULL(b.cuenta,0) as cuenta from ( select distinct codigel FROM padron a WHERE a.estado = 1 %1$s ) a left join ( %2$s ) b on a.codigel = b.codigel order by 1",
				filtroPadron, sql);
		logger.info("Cant centros :" + sqlAgrupado.toString());
		query = em.createNativeQuery(sqlAgrupado);
		List<Object[]> res = query.getResultList();
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosByLocal(String idUgel) {
		logger.info(":: PadronFacade.getCuentaCentrosByLocal :: Starting execution...");
		Query query = null;
		String filtroUgel = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String filtroPadron = "";

		StringBuilder sb = new StringBuilder();

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			filtroPadron = " and (a.codooii like '1501%' or a.codooii like '1510%') ";
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP_BY_LOCAL,
					" and (a.codooii like '1501%' or a.codooii like '1510%')", ""));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			filtroPadron = " and (a.codooii like '1502%' or a.codooii like '1520%') ";
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP_BY_LOCAL,
					" and (a.codooii like '1502%' or a.codooii like '1520%')", ""));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			filtroPadron = " and (a.codooii like '0701%' or a.codooii like '0700%') ";
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP_BY_LOCAL,
					" and (a.codooii like '0701%' or a.codooii like '0700%')", ""));
		} else {
			filtroPadron = " and (a.codooii like '".concat(filtroUgel).concat("%' )");
			sb.append(String.format(SQL_CUENTA_CENTROS_EXP_BY_LOCAL,
					" and (a.codooii like '".concat(filtroUgel).concat("%' )"), ""));
		}

		String sql = String.format("select codooii,sum(cuenta) as cuenta from (%1s) a group by 1 order by 1 ",
				sb.toString());
		String sqlAgrupado = String.format(
				"select a.codooii,IFNULL(b.cuenta,0) as cuenta from ( select distinct codooii FROM padron a WHERE a.estado = 1 %1$s ) a left join ( %2$s ) b on a.codooii = b.codooii order by 1",
				filtroPadron, sql);

		logger.info(" sentence sqlAgrupado: " + sqlAgrupado.toString());

		query = em.createNativeQuery(sqlAgrupado);

		List<Object[]> res = query.getResultList();
		logger.info(":: PadronFacade.getCuentaCentrosByLocal :: Execution finish.");
		return res;

	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosGroupNivMod(String idUgel, String tipo) {
		Query query = null;
		String filtroUgel = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlCnt = " select a.codigel, %1$s as niv_mod, count(distinct a.cod_mod,a.anexo) as cuenta "
				+ " from padron a " + " where a.mcenso='1' " + " %2$s %3$s group by 2 " + UNION_SQL;
		StringBuilder sb = new StringBuilder();
		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '1501%' or a.codigel like '1510%')",
					""));
			sb.append(String.format(sqlCnt, " 'A4' ", " and (a.codigel like '1501%' or a.codigel like '1510%')",
					" and a.progarti='1' "));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '1502%' or a.codigel like '1520%')",
					""));
			sb.append(String.format(sqlCnt, " 'A4' ", " and (a.codigel like '1502%' or a.codigel like '1520%')",
					" and a.progarti='1' "));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '0701%' or a.codigel like '0700%')",
					""));
			sb.append(String.format(sqlCnt, " 'A4' ", " and (a.codigel like '0701%' or a.codigel like '0700%')",
					" and a.progarti='1' "));
		} else if (tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '".concat(filtroUgel).concat("%')"),
					""));
			sb.append(String.format(sqlCnt, " 'A4' ", " and (a.codigel like '".concat(filtroUgel).concat("%')"),
					" and a.progarti='1' "));
		} else {
			sb.append(String.format(sqlCnt, " a.niv_mod ", String.format(" and (a.codigel = %1$s )", idUgel), ""));
			sb.append(String.format(sqlCnt, " 'A4' ", String.format(" and (a.codigel = %1$s )", idUgel),
					" and a.progarti='1' "));
		}
		sb.setLength(sb.length() - UNION_SQL.length());
		String sql = String.format("select codigel,niv_mod,sum(cuenta) as cuenta from (%1s) a group by 2 order by 2 ",
				sb.toString());
		query = em.createNativeQuery(sql);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosGroupNivModMCenso(String idUgel, String tipo) {
		Query query = null;
		String filtroUgel = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlCnt = " select a.codigel, %1$s as niv_mod, count(distinct a.cod_mod,a.anexo) as cuenta "
				+ " from padron a "
				+ " where !(SUBSTRING(niv_mod,1,1)='A' AND anexo !='0') AND SUBSTRING(tipoice,1,1)='1' AND  a.mcenso='1' AND a.estado='1' "
				+ " %2$s %3$s group by 2 " + UNION_ALL_SQL;
		StringBuilder sb = new StringBuilder();
		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '1501%' or a.codigel like '1510%')",
					""));
			sb.append(String.format(sqlCnt, " 'T0' ", " and (a.codigel like '1501%' or a.codigel like '1510%')",
					" and a.progise='1' "));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '1502%' or a.codigel like '1520%')",
					""));
			sb.append(String.format(sqlCnt, " 'T0' ", " and (a.codigel like '1502%' or a.codigel like '1520%')",
					" and a.progise='1' "));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '0701%' or a.codigel like '0700%')",
					""));
			sb.append(String.format(sqlCnt, " 'T0' ", " and (a.codigel like '0701%' or a.codigel like '0700%')",
					" and a.progise='1' "));
		} else if (tipo.equals(USUARIO_DRE)) {
			sb.append(String.format(sqlCnt, " a.niv_mod ", " and (a.codigel like '".concat(filtroUgel).concat("%')"),
					""));
			sb.append(String.format(sqlCnt, " 'T0' ", " and (a.codigel like '".concat(filtroUgel).concat("%')"),
					" and a.progise='1' "));
		} else {
			sb.append(String.format(sqlCnt, " a.niv_mod ", String.format(" and (a.codigel = %1$s )", idUgel), ""));
			sb.append(String.format(sqlCnt, " 'T0' ", String.format(" and (a.codigel = %1$s )", idUgel),
					" and a.progise='1' "));
		}
		sb.setLength(sb.length() - UNION_ALL_SQL.length());
		String sql = String.format("select codigel,niv_mod,sum(cuenta) as cuenta from (%1s) a group by 2 order by 2 ",
				sb.toString());
		query = em.createNativeQuery(sql);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosByUGEL(String idUgel, String tipo, String act) {
		logger.info("::  PadronFacade.getCuentaCentrosByUGEL :: Starting execution...");
		Query query = null;
		StringBuilder sb = new StringBuilder();
		String filtAct = "";
		if (act.equals("CENSO-RESULTADO")) {
			filtAct = " AND p.tipoprog  NOT IN ('1','2','3','5','6','7','8','9') AND p.niv_mod!='A1' ";
		} else if (act.equals("CENSO-ID")) {
			filtAct = " AND p.niv_mod!='A5' AND p.niv_mod!='E0'";
		} else if (act.equals("EVALUACION-SIAGIE")) {
			filtAct = " AND p.niv_mod IN ('A1','A2','A3','A5','B0','F0','E1','E2') ";
		} else {
			filtAct = "";
		}

		String WHERE1 = "";
		String WHERE2 = "";
		String WHERE3 = "";
		String sqlLISTA = " SELECT  p.cod_mod, p.anexo, %1$s ,p.tipoice "
				+ " ,p.codlocal, p.cen_edu,p.formas,p.ges_dep,p.codgeo,u.DISTRITO"
				+ " ,p.cen_pob, p.dir_cen, p.telefono " + " FROM padron p LEFT JOIN  "
				+ " ubigeo u  ON p.codgeo= u.UBIGEO"
				+ " WHERE p.mcenso='1' AND p.estado='1' AND !(SUBSTRING(p.niv_mod,1,1)='A' AND p.anexo !='0')  AND SUBSTRING(p.tipoice,1,1)='1' "
				+ filtAct + " %2$s  UNION";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%')";
			WHERE2 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%')";
			WHERE2 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') ";
			WHERE2 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (tipo.equals(USUARIO_DRE)) {
			WHERE1 = String.format(" AND p.codooii LIKE %1$s ", idUgel.substring(0, 4) + "%");
			WHERE2 = String.format(" AND p.codooii LIKE %1$s AND p.progarti='1'", idUgel.substring(0, 4) + "%");
			WHERE3 = String.format(" AND p.codooii LIKE %1$s AND p.progise='1'", idUgel.substring(0, 4) + "%");
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else {
			WHERE1 = String.format(" AND p.codooii = %1$s ", idUgel);
			WHERE2 = String.format(" AND p.codooii = %1$s AND p.progarti='1'", idUgel);
			WHERE3 = String.format(" AND p.codooii = %1$s AND p.progise='1'", idUgel);
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		}

		sb.setLength(sb.length() - UNION_SQL.length());
		sb.append(" ORDER BY 1 , 2, 3");

		logger.info(" getCuentaCentrosByUGEL SB: " + sb.toString());
		logger.info(" parameter idUgel: " + idUgel);

		query = em.createNativeQuery(sb.toString());

		logger.info(" getCuentaCentrosByUGEL QUERY:  " + query.toString());

		List<Object[]> res = query.getResultList();
		logger.info(":: PadronFacade.getCuentaCentrosByUGEL :: Execution finish.");
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosByUGELMarcoRecuperacion(String idUgel, String tipo, String act, String anio) {
		logger.info(":: PadronFacade.getCuentaCentrosByUGELMarcoRecuperacion :: Starting execution...");
		Query query = null;
		StringBuilder sb = new StringBuilder();
		String filtAct = "";
		if (act.equals("CENSO-RESULTADO")) {
			filtAct = " AND p.tipoprog  NOT IN ('1','2','3','5','6','7','8','9') AND p.niv_mod!='A1' ";
		} else if (act.equals("CENSO-RESULTADO-RECUPERACION")) {
			filtAct = " AND p.tipoprog  NOT IN ('1','2','3','5','6','7','8','9') AND p.niv_mod!='A1' AND p.niv_mod in ('B0','F0','D0','D1','D2') ";
		} else if (act.equals("CENSO-ID")) {
			filtAct = " AND p.niv_mod!='A5' AND p.niv_mod!='E0'";
		} else if (act.equals("EVALUACION-SIAGIE")) {
			filtAct = " AND p.niv_mod IN ('A1','A2','A3','A5','B0','F0','E1','E2') ";
		} else {
			filtAct = "";
		}

		String WHERE1 = "";
		String WHERE2 = "";
		String WHERE3 = "";
		String sqlLISTA = " SELECT  p.cod_mod, p.anexo,%1$s ,p.tipoice "
				+ " ,p.codlocal, p.cen_edu,p.formas,p.ges_dep,p.codgeo,u.DISTRITO"
				+ " ,p.cen_pob, p.dir_cen, p.telefono " + " FROM padron p LEFT JOIN  "
				+ " ubigeo u  ON p.codgeo= u.UBIGEO" + " INNER JOIN estadistica.resultado" + anio
				+ "_cabecera r ON p.COD_MOD=r.COD_MOD AND p.ANEXO=r.ANEXO "
				+ " WHERE p.mcenso='1' AND p.estado='1' AND r.varrec in ('1','2') AND r.ultimo AND !(SUBSTRING(p.niv_mod,1,1)='A' AND p.anexo !='0')  AND SUBSTRING(p.tipoice,1,1)='1' "
				+ filtAct + " %2$s  UNION";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%')";
			WHERE2 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%')";
			WHERE2 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') ";
			WHERE2 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (tipo.equals(USUARIO_DRE)) {
			WHERE1 = String.format(" AND p.codooii LIKE %1$s ", idUgel.substring(0, 4) + "%");
			WHERE2 = String.format(" AND p.codooii LIKE %1$s AND p.progarti='1'", idUgel.substring(0, 4) + "%");
			WHERE3 = String.format(" AND p.codooii LIKE %1$s AND p.progise='1'", idUgel.substring(0, 4) + "%");
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else {
			WHERE1 = String.format(" AND p.codooii = %1$s ", idUgel);
			WHERE2 = String.format(" AND p.codooii = %1$s AND p.progarti='1'", idUgel);
			WHERE3 = String.format(" AND p.codooii = %1$s AND p.progise='1'", idUgel);
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		}

		sb.setLength(sb.length() - UNION_SQL.length());
		sb.append(" ORDER BY 1 , 2, 3");

		logger.info("Sentence SQL Marco Recuperacion:" + sb.toString());

		query = em.createNativeQuery(sb.toString());
		List<Object[]> res = query.getResultList();

		logger.info(":: PadronFacade.getCuentaCentrosByUGELMarcoRecuperacion :: Execution finish.");
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosByUGEL2012(String idUgel, String tipo, String act) {
		Query query = null;
		StringBuilder sb = new StringBuilder();
		String filtAct = act.equals("CENSO-RESULTADO")
				? " AND p.tipoprog  NOT IN ('1','2','3','5','6','7','8','9') AND p.niv_mod!='A1' "
				: "";
		String WHERE1 = "";
		String WHERE2 = "";
		String WHERE3 = "";
		String sqlLISTA = " SELECT  p.cod_mod, p.anexo,%1$s ,p.tipoice "
				+ " ,p.codlocal, p.cen_edu,p.formas,p.ges_dep,p.codgeo,u.DISTRITO"
				+ " ,p.cen_pob, p.dir_cen, p.telefono " + " FROM padron p LEFT JOIN  "
				+ " ubigeo u  ON p.codgeo= u.UBIGEO"
				+ " WHERE p.mcenso='1' AND (p.fechareg not LIKE '%2013') AND !(SUBSTRING(p.niv_mod,1,1)='A' AND p.anexo !='0')  AND SUBSTRING(p.tipoice,1,1)='1' "
				+ filtAct + " %2$s  UNION";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%')";
			WHERE2 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%')";
			WHERE2 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') ";
			WHERE2 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (tipo.equals(USUARIO_DRE)) {
			WHERE1 = String.format(" AND p.codooii LIKE %1$s ", idUgel.substring(0, 4) + "%");
			WHERE2 = String.format(" AND p.codooii LIKE %1$s AND p.progarti='1'", idUgel.substring(0, 4) + "%");
			WHERE3 = String.format(" AND p.codooii LIKE %1$s AND p.progise='1'", idUgel.substring(0, 4) + "%");
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else {
			WHERE1 = String.format(" AND p.codooii = %1$s ", idUgel);
			WHERE2 = String.format(" AND p.codooii = %1$s AND p.progarti='1'", idUgel);
			WHERE3 = String.format(" AND p.codooii = %1$s AND p.progise='1'", idUgel);
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		}

		sb.setLength(sb.length() - UNION_SQL.length());
		sb.append(" ORDER BY 1 , 2, 3");
		query = em.createNativeQuery(sb.toString());
		query.setParameter(1, idUgel);
		List<Object[]> res = query.getResultList();
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosByUGEL2013(String idUgel, String tipo, String act) {
		Query query = null;
		StringBuilder sb = new StringBuilder();
		String filtAct = act.equals("CENSO-RESULTADO")
				? " AND p.tipoprog  NOT IN ('1','2','3','5','6','7','8','9') AND p.niv_mod!='A1' "
				: "";
		String WHERE1 = "";
		String WHERE2 = "";
		String WHERE3 = "";
		String sqlLISTA = " SELECT  p.cod_mod, p.anexo,%1$s ,p.tipoice "
				+ " ,p.codlocal, p.cen_edu,p.formas,p.ges_dep,p.codgeo,u.DISTRITO"
				+ " ,p.cen_pob, p.dir_cen, p.telefono " + " FROM padron p LEFT JOIN  "
				+ " ubigeo u  ON p.codgeo= u.UBIGEO"
				+ " WHERE p.mcenso='1' AND !(SUBSTRING(p.niv_mod,1,1)='A' AND p.anexo !='0')  AND SUBSTRING(p.tipoice,1,1)='1' "
				+ filtAct + " %2$s  UNION";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%')";
			WHERE2 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%')";
			WHERE2 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') ";
			WHERE2 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progarti='1'";
			WHERE3 = " AND (p.codooii LIKE '0701%' OR p.codooii LIKE '0700%') AND p.progise='1'";
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else if (tipo.equals(USUARIO_DRE)) {
			WHERE1 = String.format(" AND p.codooii LIKE %1$s ", idUgel.substring(0, 4) + "%");
			WHERE2 = String.format(" AND p.codooii LIKE %1$s AND p.progarti='1'", idUgel.substring(0, 4) + "%");
			WHERE3 = String.format(" AND p.codooii LIKE %1$s AND p.progise='1'", idUgel.substring(0, 4) + "%");
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		} else {
			WHERE1 = String.format(" AND p.codooii = %1$s ", idUgel);
			WHERE2 = String.format(" AND p.codooii = %1$s AND p.progarti='1'", idUgel);
			WHERE3 = String.format(" AND p.codooii = %1$s AND p.progise='1'", idUgel);
			sb.append(String.format(sqlLISTA, " p.niv_mod", WHERE1));
			sb.append(String.format(sqlLISTA, " 'A4' as niv_mod", WHERE2));
			sb.append(String.format(sqlLISTA, " 'T0' as niv_mod", WHERE3));
		}

		sb.setLength(sb.length() - UNION_SQL.length());
		sb.append(" ORDER BY 1 , 2, 3");
		query = em.createNativeQuery(sb.toString());
		query.setParameter(1, idUgel);
		List<Object[]> res = query.getResultList();
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosByLocal(String idUgel, String tipo) {
		logger.info(":: PadronFacade.getCuentaCentrosByLocal(two parameters) :: Starting execution...");
		Query query = null;
		StringBuilder sb = new StringBuilder();
		String WHERE1 = "";

		String sqlLISTA = " SELECT distinct " + " p.codlocal, p.cen_edu,p.formas" + " ,p.ges_dep,p.codgeo,u.DISTRITO"
				+ " ,p.cen_pob, p.dir_cen, p.telefono " + " FROM padron p LEFT JOIN  "
				+ " ubigeo u  ON p.codgeo= u.UBIGEO"
				+ " WHERE niv_mod!='A5' AND SUBSTRING(p.tipoice,1,1)='1' AND p.mcenso='1' AND p.estado='1' %1$s GROUP BY 1 ORDER BY 1";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codigel LIKE '1501%' OR p.codigel LIKE '1510%')";
			sb.append(String.format(sqlLISTA, WHERE1));

		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codigel LIKE '1502%' OR p.codigel LIKE '1520%')";
			sb.append(String.format(sqlLISTA, WHERE1));

		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codigel LIKE '0701%' OR p.codigel LIKE '0700%') ";
			sb.append(String.format(sqlLISTA, WHERE1));

		} else if (tipo.equals(USUARIO_DRE)) {
			WHERE1 = String.format(" AND p.codigel LIKE '%1$s' ", idUgel.substring(0, 4) + "%");
			sb.append(String.format(sqlLISTA, WHERE1));
		} else {
			WHERE1 = String.format(" AND p.codigel = '%1$s' ", idUgel);
			sb.append(String.format(sqlLISTA, WHERE1));
		}

		logger.info(" sb.toString()=" + sb.toString());

		query = em.createNativeQuery(sb.toString());

		List<Object[]> res = query.getResultList();
		logger.info(":: PadronFacade.getCuentaCentrosByLocal(two parameters) :: Execution finish.");
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaCentrosByLocal(String idUgel, String tipo, String act) {
		Query query = null;
		StringBuilder sb = new StringBuilder();
		String WHERE1 = "";

		String filtAct = "";
		if (act.equals("FEN-COSTERO")) {
			filtAct = " AND p.niv_mod IN ('A1','A2','A3','B0','F0','E1','E2') ";
		} else {
			filtAct = " AND niv_mod!='A5'";
		}

		String sqlLISTA = " SELECT  " + " p.codlocal, p.cen_edu,p.formas" + " ,p.ges_dep,p.codgeo,u.DISTRITO"
				+ " ,p.cen_pob, p.dir_cen, p.telefono " + " FROM padron p LEFT JOIN  "
				+ " ubigeo u  ON p.codgeo= u.UBIGEO" + " WHERE SUBSTRING(p.tipoice,1,1)='1' AND p.mcenso='1' " + filtAct
				+ " %1$s GROUP BY 1 ORDER BY 1";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codigel LIKE '1501%' OR p.codigel LIKE '1510%')";
			sb.append(String.format(sqlLISTA, WHERE1));

		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codigel LIKE '1502%' OR p.codigel LIKE '1520%')";
			sb.append(String.format(sqlLISTA, WHERE1));

		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (p.codigel LIKE '0701%' OR p.codigel LIKE '0700%') ";
			sb.append(String.format(sqlLISTA, WHERE1));

		} else if (tipo.equals(USUARIO_DRE)) {
			WHERE1 = String.format(" AND p.codigel LIKE '%1$s' ", idUgel.substring(0, 4) + "%");
			sb.append(String.format(sqlLISTA, WHERE1));
		} else {
			WHERE1 = String.format(" AND p.codigel = '%1$s' ", idUgel);
			sb.append(String.format(sqlLISTA, WHERE1));
		}
		query = em.createNativeQuery(sb.toString());
		query.setParameter(1, idUgel);
		List<Object[]> res = query.getResultList();
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<CentroEducativo> getCentrosByUgel(String ugel) {
		Query q = em.createQuery(
				"SELECT IE FROM CentroEducativo IE WHERE IE.codooii LIKE  :ugel AND SUBSTRING(IE.tipoice,1,1)='1' ")
				.setParameter("ugel", ugel);
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CentroEducativo> getCentrosByCodInst(String codInst) {
		Query q = em
				.createQuery("SELECT IE FROM CentroEducativo IE WHERE IE.codinst = :codInst AND IE.estado = :estado")
				.setParameter("codInst", codInst).setParameter("estado", "1");
		return q.getResultList();
	}

}
