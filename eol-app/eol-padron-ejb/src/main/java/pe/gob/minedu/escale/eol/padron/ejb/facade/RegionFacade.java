package pe.gob.minedu.escale.eol.padron.ejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.dto.entities.RegionDTO;
import pe.gob.minedu.escale.eol.padron.domain.Region;
import pe.gob.minedu.escale.eol.padron.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.padron.ejb.RegionLocal;
import pe.gob.minedu.escale.eol.padron.mappers.MapperUbicacion;

@Singleton
public class RegionFacade extends AbstractFacade<Region> implements RegionLocal {
	
	private static final Logger logger = Logger.getLogger(RegionFacade.class);
	
	@PersistenceContext(unitName = "padron-ejbPU")
    private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public RegionFacade() {
		super(Region.class);
	}

	@Override
	public Region findById(String codigoRegion) {
		logger.info(":: RegionFacade.findById :: Starting execution...");
		Region region = em.find(Region.class, codigoRegion);
		logger.info(" region = "+region);
        logger.info(":: RegionFacade.findById :: Execution finish.");
        return region;
	}

	@Override
	public RegionDTO getRegion(String codigoRegion) {
		logger.info(":: RegionFacade.findById :: Starting execution...");
		Region region = em.find(Region.class, codigoRegion);
		logger.info(" region = "+region);
		RegionDTO regionDTO = MapperUbicacion.setRegionDomainDto(region);
        logger.info(":: RegionFacade.findById :: Execution finish.");
        return regionDTO;
	}

	@Override
	public List<RegionDTO> getListRegionDTO() {

		List<RegionDTO> listregionDTO = new ArrayList<RegionDTO>();
		
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Region> criteriaQuery = builder.createQuery(Region.class);
		Root<Region> region = criteriaQuery.from(Region.class);
		criteriaQuery.select(region);
		criteriaQuery.orderBy(builder.asc(region.get("idRegion")));
		
		List<Region> lista = em.createQuery(criteriaQuery).getResultList();
		
		for(Region rg : lista) {
			listregionDTO.add(MapperUbicacion.setRegionDomainDto(rg));
		}
		
		return listregionDTO;
	}

	

}
