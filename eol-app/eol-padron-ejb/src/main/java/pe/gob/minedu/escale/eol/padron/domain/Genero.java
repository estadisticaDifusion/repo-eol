/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@Entity
@XmlRootElement(name = "genero")

@Table(name="v_tipssexo")
//@DiscriminatorValue(value = "tipssexo")
@NamedQueries({
    @NamedQuery(name = "Genero.findAll", query = "SELECT c FROM Genero c ORDER BY c.orden"),
    @NamedQuery(name = "Genero.findById", query = "SELECT c FROM Genero c WHERE c.idCodigo=:id")})
public class Genero extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6080915399542821201L;
}
