package pe.gob.minedu.escale.eol.padron.ejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.dto.entities.DireccionRegionalDTO;
import pe.gob.minedu.escale.eol.padron.domain.DireccionRegional;
import pe.gob.minedu.escale.eol.padron.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.padron.ejb.DreLocal;
import pe.gob.minedu.escale.eol.padron.mappers.MapperUgel;

@Singleton
public class DreFacade extends AbstractFacade<DireccionRegional> implements DreLocal {

    private static final Logger logger = Logger.getLogger(DreFacade.class);

    @PersistenceContext(unitName = "padron-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DreFacade() {
        super(DireccionRegional.class);
    }

    public DireccionRegional findById(String codigoDrel) {
        logger.info(":: DreFacade.findById :: Starting execution...");
        DireccionRegional dre = em.find(DireccionRegional.class, codigoDrel);
        logger.info(" dre = "+dre);
        logger.info(":: DreFacade.findById :: Execution finish.");
        return dre;
    }

	@Override
	public List<DireccionRegionalDTO> gestListDre() {
		

		List<DireccionRegionalDTO> listdreDTO = new ArrayList<DireccionRegionalDTO>();
		
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<DireccionRegional> criteriaQuery = builder.createQuery(DireccionRegional.class);
		Root<DireccionRegional> dre = criteriaQuery.from(DireccionRegional.class);
		criteriaQuery.select(dre);
		criteriaQuery.orderBy(builder.asc(dre.get("id")));
		
		List<DireccionRegional> lista = em.createQuery(criteriaQuery).getResultList();
		
		for(DireccionRegional rg : lista) {
			listdreDTO.add(MapperUgel.setDreDomainToDto(rg));
		}
		
		return listdreDTO;
		
	}
}
