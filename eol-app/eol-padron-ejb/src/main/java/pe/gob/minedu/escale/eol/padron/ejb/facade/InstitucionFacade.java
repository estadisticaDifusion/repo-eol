/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.ejb.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.dto.entities.AreaDTO;
import pe.gob.minedu.escale.eol.dto.entities.CaracteristicaDTO;
import pe.gob.minedu.escale.eol.dto.entities.CodigoDTO;
import pe.gob.minedu.escale.eol.dto.entities.DisCrecerDTO;
import pe.gob.minedu.escale.eol.dto.entities.DisJuntosDTO;
import pe.gob.minedu.escale.eol.dto.entities.DisVraeDTO;
import pe.gob.minedu.escale.eol.dto.entities.DistritoDTO;
import pe.gob.minedu.escale.eol.dto.entities.EstadisticaDTO;
import pe.gob.minedu.escale.eol.dto.entities.EstadoDTO;
import pe.gob.minedu.escale.eol.dto.entities.FormaDTO;
import pe.gob.minedu.escale.eol.dto.entities.GeneroDTO;
import pe.gob.minedu.escale.eol.dto.entities.GestionDTO;
import pe.gob.minedu.escale.eol.dto.entities.GestionDependenciaDTO;
import pe.gob.minedu.escale.eol.dto.entities.IgelDTO;
import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.dto.entities.NivelModalidadDTO;
import pe.gob.minedu.escale.eol.dto.entities.ProvinciaDTO;
import pe.gob.minedu.escale.eol.dto.entities.RegionDTO;
import pe.gob.minedu.escale.eol.dto.entities.SituacionDirectorDTO;
import pe.gob.minedu.escale.eol.dto.entities.TotalEstadisticaDTO;
import pe.gob.minedu.escale.eol.dto.entities.TurnoDTO;
import pe.gob.minedu.escale.eol.padron.domain.Area;
import pe.gob.minedu.escale.eol.padron.domain.Caracteristica;
import pe.gob.minedu.escale.eol.padron.domain.Codigo;
import pe.gob.minedu.escale.eol.padron.domain.DireccionRegional;
import pe.gob.minedu.escale.eol.padron.domain.DisCrecer;
import pe.gob.minedu.escale.eol.padron.domain.DisJuntos;
import pe.gob.minedu.escale.eol.padron.domain.DisVrae;
import pe.gob.minedu.escale.eol.padron.domain.Distrito;
import pe.gob.minedu.escale.eol.padron.domain.Estadistica;
import pe.gob.minedu.escale.eol.padron.domain.Estado;
import pe.gob.minedu.escale.eol.padron.domain.Forma;
import pe.gob.minedu.escale.eol.padron.domain.Genero;
import pe.gob.minedu.escale.eol.padron.domain.Gestion;
import pe.gob.minedu.escale.eol.padron.domain.GestionDependencia;
import pe.gob.minedu.escale.eol.padron.domain.Igel;
import pe.gob.minedu.escale.eol.padron.domain.InfoA;
import pe.gob.minedu.escale.eol.padron.domain.InfoB;
import pe.gob.minedu.escale.eol.padron.domain.InfoC;
import pe.gob.minedu.escale.eol.padron.domain.InfoD;
import pe.gob.minedu.escale.eol.padron.domain.InfoE;
import pe.gob.minedu.escale.eol.padron.domain.InfoF;
import pe.gob.minedu.escale.eol.padron.domain.InfoG;
import pe.gob.minedu.escale.eol.padron.domain.InfoK;
import pe.gob.minedu.escale.eol.padron.domain.InfoL;
import pe.gob.minedu.escale.eol.padron.domain.InfoM;
import pe.gob.minedu.escale.eol.padron.domain.InfoT;
import pe.gob.minedu.escale.eol.padron.domain.Institucion;
import pe.gob.minedu.escale.eol.padron.domain.NivelModalidad;
import pe.gob.minedu.escale.eol.padron.domain.Provincia;
import pe.gob.minedu.escale.eol.padron.domain.Region;
import pe.gob.minedu.escale.eol.padron.domain.SituacionDirector;
import pe.gob.minedu.escale.eol.padron.domain.Turno;
import pe.gob.minedu.escale.eol.padron.domain.Ugel;
import pe.gob.minedu.escale.eol.padron.dto.CensoieDTO;
import pe.gob.minedu.escale.eol.padron.dto.InstitucionEducativaDTO;
import pe.gob.minedu.escale.eol.padron.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.padron.ejb.InstitucionLocal;
import pe.gob.minedu.escale.eol.padron.mappers.MapperUgel;

/**
 * Recuerso que obtiene instituciones educativas utilizando JPA
 *
 * @author DSILVA
 */
@Stateless
//@Singleton
public class InstitucionFacade extends AbstractFacade<Institucion> implements InstitucionLocal {

	private final Logger logger = Logger.getLogger(InstitucionFacade.class);

	@PersistenceContext(unitName = "padron-ejbPU")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public InstitucionFacade() {
		super(Institucion.class);
	}
	
	@Override
	public List<InstitucionDTO> getInstitucionList(String codmod, String codlocal, String ubigeo, String codooii,
			String nombreCP, String nombreIE, String disVrae, String disJuntos, String disCrecer, String disNinguno,
			String matIndigena, List<String> niveles, List<String> gestiones, List<String> areas, String estado,
			List<String> tipoices, List<String> formas, List<String> estados, String progarti, String progise,
			int start, int max, String orderBy) {
		logger.info(":: InstitucionFacade.getInstitucionList :: Starting execution...");
		List<InstitucionDTO> entitiesDTO = new ArrayList<InstitucionDTO>();
		List<Institucion> entities = searchInstitucion(codmod, codlocal, ubigeo, codooii, nombreCP, nombreIE, disVrae,
				disJuntos, disCrecer, disNinguno, matIndigena, niveles, gestiones, areas, estado, tipoices, formas,
				estados, progarti, progise, start, max, orderBy);
		logger.info("INSTITUCIONES OBTENIDADAS DESDE EL PADRON : " + entities.isEmpty()  );
		if (entities != null) {
			for (Institucion institucion : entities) {
				entitiesDTO.add(mapInstitucionDomainToDto(institucion));
			}
		}
		logger.info(":: InstitucionFacade.getInstitucionList :: Execution finish.");
		return entitiesDTO;

	}

	@Override
	public Long getRecordCount(String codmod, String codlocal, String ubigeo, String codooii, String nombreCP,
			String nombreIE, String disVrae, String disJuntos, String disCrecer, String disNinguno, String matIndigena,
			List<String> niveles, List<String> gestiones, List<String> areas, String estado, List<String> tipoices,
			List<String> formas, List<String> estados, String progarti, String progise) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<Institucion> institucion = criteriaQuery.from(Institucion.class);

		criteriaQuery.select(criteriaBuilder.count(institucion));

		Set<Predicate> restrictions = crearCondicionesCount(criteriaBuilder, institucion, codmod, codlocal, ubigeo,
				codooii, nombreCP, nombreIE, disVrae, disJuntos, disCrecer, disNinguno, matIndigena, niveles, gestiones,
				areas, estado, tipoices, formas, estados, progarti, progise);
		Long result = 0L;
		if (restrictions != null && !restrictions.isEmpty()) {
			criteriaQuery.where(criteriaBuilder.and(restrictions.toArray(new Predicate[0])));
			result = em.createQuery(criteriaQuery).getSingleResult();
			if (result == null) {
				logger.info("Se obtuvo registros null");
			} else {
				logger.info("Se encontraron " + result + " registros");
			}
		} else {
			result = em.createQuery(criteriaQuery).getSingleResult();
			if (result == null) {
				logger.info("Se obtuvo registros null");
			} else {
				logger.info("Se encontraron " + result + " registros");
			}

		}
		return result;
	}

	@Override
	public List<InstitucionEducativaDTO> getInstitucionEducativaList(String ubigeo, String dreugel,
			String idCentroPoblado, String nomCentroPoblado, String codigoModular, String nombreIE, String codigoLocal,
			String direccionIE, String progarti, String mcenso, String progise, List<String> gestiones,
			List<String> niveles, List<String> areas) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			if (ubigeo != null && !ubigeo.isEmpty()) {
				params.put("ubigeo", ubigeo);
			}
			if (dreugel != null && !dreugel.isEmpty()) {

				dreugel = (dreugel.endsWith("00") || dreugel.endsWith("150101") || dreugel.endsWith("070101"))
						? dreugel.substring(0, 4)
						: dreugel;
				params.put("dreugel", dreugel);
			}
			if (idCentroPoblado != null && !idCentroPoblado.isEmpty()) {
				params.put("idCentroPoblado", idCentroPoblado);
			}
			if (nomCentroPoblado != null && !nomCentroPoblado.isEmpty()) {
				params.put("nomCentroPoblado", nomCentroPoblado.replaceAll(" ", "%"));
			}
			if (nombreIE != null && !nombreIE.isEmpty()) {
				params.put("nombreIE", nombreIE.replaceAll(" ", "%"));
			}
			if (direccionIE != null && !direccionIE.isEmpty()) {
				params.put("direccionIE", direccionIE.replaceAll(" ", "%"));
			}
			if (codigoModular != null && !codigoModular.isEmpty()) {
				params.put("codigoModular", codigoModular);
			}
			if (codigoLocal != null && !codigoLocal.isEmpty()) {
				params.put("codigoLocal", codigoLocal);
			}
			if (progarti != null && !progarti.isEmpty()) {
				params.put("progarti", progarti);
			}

			if (progise != null && !progise.isEmpty()) {
				params.put("progise", progise);
			}
			if (gestiones != null && !gestiones.isEmpty()) {
				params.put("gestiones", gestiones);
			}
			if (niveles != null && !niveles.isEmpty()) {
				params.put("niveles", niveles);
			}
			if (areas != null && !areas.isEmpty()) {
				params.put("areas", areas);
			}
			if (params.isEmpty()) {
				return null;
			}

			return setDataInstitucionDTOList(params);
		} catch (Exception e) {
			logger.error("problem getInstitucionEducativaList exception caught: " + e);
		}
		return null;
	}

	private <T> Set<Predicate> crearCondiciones(CriteriaBuilder criteriaBuilder, Root<T> institucion, String codmod,
			String codlocal, String ubigeo, String codooii, String nombreCP, String nombreIE, String disVrae,
			String disJuntos, String disCrecer, String disNinguno, String matIndigena, List<String> niveles,
			List<String> gestiones, List<String> areas, String estado, List<String> tipoices, List<String> formas,
			List<String> estados, String progarti, String progise) {
		logger.info(":: InstitucionFacade.crearCondiciones :: Starting execution...");
		logger.info("value codooii: " + codooii);
		if (niveles != null) {
			niveles.remove("");
		}
		if (areas != null) {
			areas.remove("");
		}
		if (gestiones != null) {
			gestiones.remove("");
		}
		if (estados != null) {
			estados.remove("");
		}
		if (formas != null) {
			formas.remove("");
		}
		if (tipoices != null) {
			tipoices.remove("");
		}
		Set<Predicate> restrictions = new LinkedHashSet<Predicate>();

		if (codmod != null && !codmod.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("codMod").as(String.class),
					"%" + codmod.replaceAll(" ", "%") + "%"));
		}
		if (codlocal != null && !codlocal.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("codlocal").as(String.class), codlocal));
		}
		if (nombreIE != null && !nombreIE.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("cenEdu").as(String.class),
					"%" + nombreIE.replaceAll(" ", "%") + "%"));
		}

		if (nombreCP != null && !nombreCP.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("cenPob").as(String.class),
					"%" + nombreCP.replaceAll(" ", "%") + "%"));
		}

		if (niveles != null && !niveles.isEmpty()) {
			restrictions.add(institucion.get("nivelModalidad").get("idCodigo").in(niveles));
		}

		if (niveles != null && !niveles.isEmpty()) {
			restrictions.add(institucion.get("nivelModalidad").get("idCodigo").in(niveles));
		}

		if (estados != null && !estados.isEmpty()) {
			restrictions.add(institucion.get("estado").get("idCodigo").in(estados));
		}

		if (matIndigena != null && !matIndigena.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("cpEtnico"), matIndigena));
		}

		if (progarti != null && !progarti.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("progarti"), progarti));
		}
		
		if (progise != null && !progise.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("progise"), progise));
		}

		if (gestiones != null && !gestiones.isEmpty()) {
			restrictions.add(institucion.get("gestionDependencia").get("idCodigo").in(gestiones));
		}

		/**
		 * por Intervencion *
		 */
		Predicate $predInter = null;
		if (disNinguno == null) {
			if (disVrae != null && disJuntos == null && disCrecer == null) {
				$predInter = criteriaBuilder
						.and(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"));

			} else if (disVrae == null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder
						.and(criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"));
			} else if (disVrae == null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder
						.and(criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"));
			} else if (disVrae == null && disJuntos != null && disCrecer != null) {
				$predInter = criteriaBuilder.or(
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"));
			} else if (disVrae != null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"));
			} else if (disVrae != null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"));
			} else if (disVrae != null && disJuntos != null && disCrecer != null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"),
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"));
			}
		} else {
			Predicate $predInter1 = criteriaBuilder.and(
					criteriaBuilder.notEqual(institucion.get("disVrae").get("idCodigo"), "1"),
					criteriaBuilder.equal(institucion.get("disJuntos").get("idCodigo"), "a"),
					criteriaBuilder.equal(institucion.get("disCrecer").get("idCodigo"), "a"));
			;
			if (disVrae != null && disJuntos == null && disCrecer == null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						$predInter1);
			} else if (disVrae == null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder
						.or(criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae == null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder
						.or(criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae == null && disJuntos != null && disCrecer != null) {
				$predInter = criteriaBuilder.or(
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae != null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae != null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae == null && disJuntos == null && disCrecer == null) {
				$predInter = $predInter1;
			}

		}
		if ($predInter != null) {
			restrictions.add($predInter);
		}

		if (areas != null && !areas.isEmpty()) {
			restrictions.add(institucion.get("areaSig").get("idCodigo").in(areas));
		}

		if (formas != null && !formas.isEmpty()) {
			restrictions.add(institucion.get("forma").get("idCodigo").in(formas));
		}

		if (codooii != null && !codooii.isEmpty()) {
			Predicate $codooii = null;
			switch (codooii.length()) {
			case 4:
				DireccionRegional dre = em.find(DireccionRegional.class, codooii);
				if (dre != null) {
					$codooii = criteriaBuilder.equal(institucion.get("dre"), codooii.substring(0, 4));
				}
				break;
			case 6:
				Ugel ugel = em.find(Ugel.class, codooii);
				if (ugel != null) {
					$codooii = criteriaBuilder.equal(institucion.get("ugel"), ugel);
				}
				break;
			}
			if ($codooii != null) {
				restrictions.add($codooii);
			}
		}

		logger.info("ubigeo = " + ubigeo);
		if (ubigeo != null && !ubigeo.isEmpty()) {
			Predicate $ubigeo = null;
			switch (ubigeo.length()) {
			case 2:
				Region region = em.find(Region.class, ubigeo);// new Region(ubigeo);
				if (region != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito").get("provincia").get("region"), region);
				}
				break;
			case 4:
				Provincia provincia = em.find(Provincia.class, ubigeo);
				if (provincia != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito").get("provincia"), provincia);
				}
				break;
			default: //case 6:
				logger.info("search ubigeo = " + ubigeo);
				Distrito distrito = new Distrito(ubigeo);
				$ubigeo = criteriaBuilder.equal(institucion.get("distrito"), distrito);
				
/*				logger.info("search ubigeo = " + ubigeo);
				Distrito distrito = em.find(Distrito.class, ubigeo);
				if (distrito != null) {
					logger.info("found distrito = " + distrito);
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito"), distrito); //distrito.getIdDistrito()
				}*/
				break;
			}
			if ($ubigeo != null) {
				restrictions.add($ubigeo);
			}
		}
		
		logger.info("restrictions = " + restrictions);
		
		logger.info(":: InstitucionFacade.crearCondiciones :: Execution finish.");
		return restrictions;
	}

	private <T> Set<Predicate> crearCondicionesCount(CriteriaBuilder criteriaBuilder, Root<T> institucion,
			String codmod, String codlocal, String ubigeo, String codooii, String nombreCP, String nombreIE,
			String disVrae, String disJuntos, String disCrecer, String disNinguno, String matIndigena,
			List<String> niveles, List<String> gestiones, List<String> areas, String estado, List<String> tipoices,
			List<String> formas, List<String> estados, String progarti, String progise) {

		logger.info("values niveles: " + niveles + "; areas=" + areas + "; gestiones=" + gestiones + "; estados="
				+ estados + "; formas=" + formas + "; tipoices=" + tipoices);
		if (niveles != null) {
			niveles.remove("");
		}
		if (areas != null) {
			areas.remove("");
		}
		if (gestiones != null) {
			gestiones.remove("");
		}
		if (estados != null) {
			estados.remove("");
		}

		if (formas != null) {
			formas.remove("");
		}
		if (tipoices != null) {
			tipoices.remove("");
		}

		Set<Predicate> restrictions = new LinkedHashSet<Predicate>();

		if (codmod != null && !codmod.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("codMod").as(String.class),
					"%" + codmod.replaceAll(" ", "%") + "%"));
		}
		if (codlocal != null && !codlocal.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("codlocal").as(String.class), codlocal));
		}
		if (nombreIE != null && !nombreIE.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("cenEdu").as(String.class),
					"%" + nombreIE.replaceAll(" ", "%") + "%"));
		}

		if (nombreCP != null && !nombreCP.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("cenPob").as(String.class),
					"%" + nombreCP.replaceAll(" ", "%") + "%"));
		}

		if (niveles != null && !niveles.isEmpty()) {
			restrictions.add(institucion.get("nivelModalidad").get("idCodigo").in(niveles));
		}

		if (estados != null && !estados.isEmpty()) {
			restrictions.add(institucion.get("estado").get("idCodigo").in(estados));
		}
		/**
		 * matricula indigena > 25% *
		 */
		if (matIndigena != null && !matIndigena.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("cpEtnico"), matIndigena));
		}

		if (gestiones != null && !gestiones.isEmpty()) {
			restrictions.add(institucion.get("gestionDependencia").get("idCodigo").in(gestiones));
		}

		if (progarti != null && !progarti.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("progarti"), progarti));
		}
		
		if (progise != null && !progise.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("progise"), progise));
		}

		Predicate $predInter = null;
		if (disNinguno == null) {
			if (disVrae != null && disJuntos == null && disCrecer == null) {
				$predInter = criteriaBuilder
						.and(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"));

			} else if (disVrae == null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder
						.and(criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"));
			} else if (disVrae == null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder
						.and(criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"));
			} else if (disVrae == null && disJuntos != null && disCrecer != null) {
				$predInter = criteriaBuilder.or(
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"));
			} else if (disVrae != null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"));
			} else if (disVrae != null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"));
			} else if (disVrae != null && disJuntos != null && disCrecer != null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"),
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"));
			}
		} else {
			Predicate $predInter1 = criteriaBuilder.and(
					criteriaBuilder.notEqual(institucion.get("disVrae").get("idCodigo"), "1"),
					criteriaBuilder.equal(institucion.get("disJuntos").get("idCodigo"), "a"),
					criteriaBuilder.equal(institucion.get("disCrecer").get("idCodigo"), "a"));
			;
			if (disVrae != null && disJuntos == null && disCrecer == null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						$predInter1);
			} else if (disVrae == null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder
						.or(criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae == null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder
						.or(criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae == null && disJuntos != null && disCrecer != null) {
				$predInter = criteriaBuilder.or(
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae != null && disJuntos == null && disCrecer != null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disCrecer").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae != null && disJuntos != null && disCrecer == null) {
				$predInter = criteriaBuilder.or(criteriaBuilder.equal(institucion.get("disVrae").get("idCodigo"), "1"),
						criteriaBuilder.notEqual(institucion.get("disJuntos").get("idCodigo"), "a"), $predInter1);
			} else if (disVrae == null && disJuntos == null && disCrecer == null) {
				$predInter = $predInter1;
			}

		}
		if ($predInter != null) {
			restrictions.add($predInter);
		}

		/**
		 * por area *
		 */
		if (areas != null && !areas.isEmpty()) {
			restrictions.add(institucion.get("areaSig").get("idCodigo").in(areas));
		}
		/**
		 * por formas*
		 */
		if (formas != null && !formas.isEmpty()) {
			restrictions.add(institucion.get("forma").get("idCodigo").in(formas));
		}

		/**
		 * por codooii *
		 */
		if (codooii != null && !codooii.isEmpty()) {
			Predicate $codooii = null;
			switch (codooii.length()) {
			case 4:
				DireccionRegional dre = em.find(DireccionRegional.class, codooii);
				if (dre != null) {
					$codooii = criteriaBuilder.equal(institucion.get("dre"), codooii.substring(0, 4));
				}
				break;
			case 6:
				Ugel ugel = em.find(Ugel.class, codooii);
				if (ugel != null) {
					$codooii = criteriaBuilder.equal(institucion.get("ugel"), ugel);
				}
				break;
			}
			if ($codooii != null) {
				restrictions.add($codooii);
			}
		}

		if (ubigeo != null && !ubigeo.isEmpty()) {
			Predicate $ubigeo = null;
			switch (ubigeo.length()) {
			case 2:
				Region region = em.find(Region.class, ubigeo);
				if (region != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito").get("provincia").get("region"), region);
				}
				break;
			case 4:
				Provincia provincia = em.find(Provincia.class, ubigeo);
				if (provincia != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito").get("provincia"), provincia);
				}
				break;
			default: //case 6:
				Distrito distrito = new Distrito(ubigeo);
				$ubigeo = criteriaBuilder.equal(institucion.get("distrito"), distrito);
				
/*				Distrito distrito = em.find(Distrito.class, ubigeo);
				if (distrito != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito"), distrito);
				}*/
				break;
			}
			if ($ubigeo != null) {
				restrictions.add($ubigeo);

			}
		}
		return restrictions;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CensoieDTO getDataCensoIE(String codmod,String anexo,String anio,String nivel) {
		
		CensoieDTO censoiedto = new CensoieDTO();
		String sql = "";
		Query query = null;
		
		sql = "select * from info"+anio+".esbas_nivel_"+nivel+ " where cod_mod=:codmod and anexo=:anexo ";
		
		if(anio!=null && nivel!=null && !nivel.isEmpty() && !anio.isEmpty()) {
			
			try {
				switch (nivel.charAt(0)) {
				case 'A':
				case 'a':
					query = em.createNativeQuery(sql, InfoA.class);
					censoiedto.setInfoInicial(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				case 'B':
				case 'b':
					query = em.createNativeQuery(sql, InfoB.class);
					censoiedto.setInfoPrimaria(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				case 'C':
				case 'c':
					if(new Integer(anio)<2010) {
						query = em.createNativeQuery(sql, InfoC.class);
						censoiedto.setInfoPrimariaAdultos(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					}
					break;
				case 'D':
				case 'd':
					if(new Integer(anio)>2005) {
						query = em.createNativeQuery(sql, InfoD.class);
						censoiedto.setInfoBasicaAlternativa(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					}
					break;
				case 'E':
				case 'e':
					query = em.createNativeQuery(sql, InfoE.class);
					censoiedto.setInfoSecundaria(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				case 'F':
				case 'f':
					query = em.createNativeQuery(sql, InfoF.class);
					censoiedto.setInfoSecundaria(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				case 'G':
				case 'g':
					if(new Integer(anio)<2010) {
						query = em.createNativeQuery(sql, InfoG.class);
						censoiedto.setInfoSecundariaAdultos(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					}
					break;
				case 'K':
				case 'k':
					query = em.createNativeQuery(sql, InfoK.class);
					censoiedto.setInfoISP(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				case 'L':
				case 'l':
					query = em.createNativeQuery(sql, InfoL.class);
					censoiedto.setInfoCETPRO(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				case 'M':
				case 'm':
					query = em.createNativeQuery(sql, InfoM.class);
					censoiedto.setInfoSuperiorArtistica(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				case 'T':
				case 't':
					query = em.createNativeQuery(sql, InfoT.class);
					censoiedto.setInfoIST(query.setParameter("codmod", codmod).setParameter("anexo", anexo).getResultList());
					break;
				default:
					break;
				}
			} catch (Exception e) {
				logger.info(":: Excepcion en setDataCensoIE :: "+e.getMessage());
			}
			
		}		
	
		return censoiedto;
	}

	@SuppressWarnings("unchecked")
	private List<InstitucionEducativaDTO> setDataInstitucionDTOList(Map<String, Object> params) {
		logger.info(":: InstitucionFacade.setDataInstitucionList :: Starting execution...");
		List<InstitucionEducativaDTO> institucionDTOList = new ArrayList<InstitucionEducativaDTO>();
		Query query = null;
		StringBuilder sqlSb = new StringBuilder();
		sqlSb.append(
				"SELECT p.cod_mod, p.anexo, p.codlocal, p.cen_edu, p.codooii, i.nomigel, p.codccpp, p.cen_pob, p.niv_mod, p.codgeo, ");
		sqlSb.append(
				" p.gestion, g.valor as nom_gestion, p.ges_dep, gd.valor as nom_gd, p.cod_car, cc.valor as nom_cc, p.formas, ");
		sqlSb.append(
				" f.valor as nom_form, p.cod_tur, t.valor as nom_t, p.area_sig, a.valor as nom_are, p.estado, e.valor as nom_esta, p.dir_cen, ");
		sqlSb.append(
				" p.telefono, p.pagweb, p.email, p.director, p.tipssexo, s.valor as nom_sex, u.departamento, u.provincia, u.distrito, ");
		sqlSb.append(
				" n.valor as nom_niv, p.dis_crecer, p.dis_juntos, p.dis_vrae, p.alt_cp, IF(p.nlong_ie!=0,p.nlong_ie,p.nlong_cp) as coordx, ");
		sqlSb.append(" IF(p.nlat_ie!=0,p.nlat_ie,p.nlat_cp) as coordy ");
		sqlSb.append("FROM padron p ");
		sqlSb.append(" left join ubigeo u on (p.codgeo=u.ubigeo) ");
		sqlSb.append(" left join v_niv_mod n on (p.niv_mod=n.id) ");
		sqlSb.append(" left join v_estado e on (p.estado=e.id) ");
		sqlSb.append(" left join v_tipssexo s on (p.tipssexo=s.id) ");
		sqlSb.append(" left join v_cod_tur t on (p.cod_tur=t.id) ");
		sqlSb.append(" left join v_ges_dep gd on (p.ges_dep=gd.id) ");
		sqlSb.append(" left join v_gestion g on (p.gestion=g.id) ");
		sqlSb.append(" left join v_cod_area a on (p.area_sig=a.id) ");
		sqlSb.append(" left join v_cod_car cc on (p.cod_car=cc.id) ");
		sqlSb.append(" left join v_formas f on (p.formas=f.id) ");
		sqlSb.append(" left join igels i on (p.codigel=i.codigel) ");
		sqlSb.append("WHERE p.estado='1' ");
		if (params.get("ubigeo") != null) {
			sqlSb.append(" and p.codgeo like '" + params.get("ubigeo") + "%' ");
		}
		if (params.get("dreugel") != null) {
			sqlSb.append(" and p.codooii like '" + params.get("dreugel") + "%' ");
		}
		if (params.get("idCentroPoblado") != null) {
			sqlSb.append(" and p.codccpp = '" + params.get("idCentroPoblado") + "' ");
		}
		if (params.get("codigoLocal") != null) {
			sqlSb.append(" and p.codlocal = '" + params.get("codigoLocal") + "' ");
		}
		if (params.get("nomCentroPoblado") != null) {
			sqlSb.append(" and p.cen_pob like '%" + params.get("nomCentroPoblado") + "%' ");
		}
		if (params.get("direccionIE") != null) {
			sqlSb.append(" and p.dir_cen like '%" + params.get("direccionIE") + "%' ");
		}
		if (params.get("codigoModular") != null) {
			sqlSb.append(" and p.cod_mod= '" + params.get("codigoModular") + "' ");
		}
		if (params.get("nombreIE") != null) {
			sqlSb.append(" and p.cen_edu like '%" + params.get("nombreIE") + "%' ");
		}
		if (params.get("cod_mod") != null) {
			sqlSb.append(" and p.cod_mod = '" + params.get("cod_mod") + "' ");
		}
		if (params.get("anexo") != null) {
			sqlSb.append(" and p.anexo = '" + params.get("anexo") + "' ");
		}
		if (params.get("progarti") != null) {
			sqlSb.append(" and p.progarti='" + params.get("progarti") + "' ");
		}
		if (params.get("progise") != null) {
			sqlSb.append(" and p.progise='" + params.get("progise") + "' ");
		}
		if (params.get("niveles") != null) {
			sqlSb.append(" and ( ");
			List<String> niveles = (List<String>) params.get("niveles");
			int index = 1;
			for (String nivel : niveles) {
				sqlSb.append(" p.niv_mod='" + nivel + "' " + (index < niveles.size() ? " or " : ""));
				index++;
			}
			sqlSb.append(") ");
		}
		sqlSb.append("GROUP BY p.cod_mod,p.anexo");
		logger.info(" setDataInstitucionList sentence SQL: " + sqlSb.toString());

		query = em.createNativeQuery(sqlSb.toString());

		List<Object[]> list = query.getResultList();
		InstitucionEducativaDTO institucionDTO = null;

		for (Object[] o : list) {
			institucionDTO = new InstitucionEducativaDTO();
			institucionDTO.setCodigoModular(o[0] != null ? o[0].toString() : "");
			institucionDTO.setAnexo(o[1] != null ? o[1].toString() : "");
			institucionDTO.setCodigoLocal(o[2] != null ? o[2].toString() : "");
			institucionDTO.setNombreIE(o[3] != null ? o[3].toString() : "");
			institucionDTO.setCodDreUgel(o[4] != null ? o[4].toString() : "");
			institucionDTO.setNomDreUgel(o[5] != null ? o[5].toString() : "");
			institucionDTO.setCodCentroPoblado(o[6] != null ? o[6].toString() : "");
			institucionDTO.setCentroPoblado(o[7] != null ? o[7].toString() : "");
			institucionDTO.setCodNivelModalidad(o[8] != null ? o[8].toString() : "");
			institucionDTO.setUbigeo(o[9] != null ? o[9].toString() : "");
			institucionDTO.setCodGestion(o[10] != null ? o[10].toString() : "");
			institucionDTO.setDescGestion(o[11] != null ? o[11].toString() : "");
			institucionDTO.setCodDependencia(o[12] != null ? o[12].toString() : "");
			institucionDTO.setDescDependencia(o[13] != null ? o[13].toString() : "");
			institucionDTO.setCodCaracteristica(o[14] != null ? o[14].toString() : "");
			institucionDTO.setDescCaracteristica(o[15] != null ? o[15].toString() : "");
			institucionDTO.setForma(o[16] != null ? o[16].toString() : "");
			institucionDTO.setDescForma(o[17] != null ? o[17].toString() : "");
			institucionDTO.setCodTurno(o[18] != null ? o[18].toString() : "");
			institucionDTO.setDescTurno(o[19] != null ? o[19].toString() : "");
			institucionDTO.setCodArea(o[20] != null ? o[20].toString() : "");
			institucionDTO.setDescArea(o[21] != null ? o[21].toString() : "");
			institucionDTO.setEstado(o[22] != null ? o[22].toString() : "");
			institucionDTO.setDescEstado(o[23] != null ? o[23].toString() : "");
			institucionDTO.setDireccion(o[24] != null ? o[24].toString() : "");
			institucionDTO.setTelefono(o[25] != null ? o[25].toString() : "");
			institucionDTO.setPaginaWeb(o[26] != null ? o[26].toString() : "");
			institucionDTO.setCorreoElectronico(o[27] != null ? o[27].toString() : "");
			institucionDTO.setDirector(o[28] != null ? o[28].toString() : "");
			institucionDTO.setTipoSexo(o[29] != null ? o[29].toString() : "");
			institucionDTO.setDescTipoSexo(o[30] != null ? o[30].toString() : "");
			institucionDTO.setDepartamento(o[31] != null ? o[31].toString() : "");
			institucionDTO.setProvincia(o[32] != null ? o[32].toString() : "");
			institucionDTO.setDistrito(o[33] != null ? o[33].toString() : "");
			institucionDTO.setNivelModalidad(o[34] != null ? o[34].toString() : "");
			institucionDTO.setDisCrecer(o[35] != null ? o[35].toString() : "");
			institucionDTO.setDisJuntos(o[36] != null ? o[36].toString() : "");
			institucionDTO.setDisVrae(o[37] != null ? o[37].toString() : "");
			institucionDTO.setAltCP(o[38] != null ? o[38].toString() : "");
			institucionDTO.setCoordX(o[39] != null ? new BigDecimal(o[39].toString()) : new BigDecimal(0));
			institucionDTO.setCoordY(o[40] != null ? new BigDecimal(o[40].toString()) : new BigDecimal(0));
			institucionDTO.setAlumnos(0);
			institucionDTO.setDocentes(0);
			institucionDTOList.add(institucionDTO);
		}

		logger.info(":: InstitucionFacade.setDataInstitucionDTOList :: Execution finish.");
		return institucionDTOList;

	}

	private List<Institucion> searchInstitucion(String codmod, String codlocal, String ubigeo, String codooii,
			String nombreCP, String nombreIE, String disVrae, String disJuntos, String disCrecer, String disNinguno,
			String matIndigena, List<String> niveles, List<String> gestiones, List<String> areas, String estado,
			List<String> tipoices, List<String> formas, List<String> estados, String progarti, String progise,
			int start, int max, String orderBy) {
		List<Institucion> entities = null;
		logger.info(" em = " + em);
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Institucion> criteria = criteriaBuilder.createQuery(Institucion.class);
		Root<Institucion> institucion = criteria.from(Institucion.class);
		
		criteria.select(institucion);
		Set<Predicate> restrictions = crearCondiciones(criteriaBuilder, institucion, codmod, codlocal, ubigeo, codooii,
				nombreCP, nombreIE, disVrae, disJuntos, disCrecer, disNinguno, matIndigena, niveles, gestiones, areas,
				estado, tipoices, formas, estados, progarti, progise);
		
		
		if (orderBy != null && !orderBy.isEmpty()) {
			criteria.orderBy(criteriaBuilder.asc(institucion.get(orderBy).as(String.class)));
		}
		if (restrictions != null && !restrictions.isEmpty()) {
			criteria.where(criteriaBuilder.and(restrictions.toArray(new Predicate[0])));
			if (max != -1) {
				entities = em.createQuery(criteria).setFirstResult(start).setMaxResults(max).getResultList();
			} else {
				entities = em.createQuery(criteria).getResultList();
			}

			if (entities == null) {
				logger.info("Se obtuvo registros null");
			} else {
				logger.info("Se encontraron " + entities.size() + " registros: ");
			}
		} else {
			if (max != -1) {
				entities = em.createQuery(criteria).setFirstResult(start).setMaxResults(max).getResultList();
			} else {
				entities = em.createQuery(criteria).getResultList();
			}

			if (entities == null) {
				logger.info("Se obtuvo registros null");
			} else {
				logger.info("Se encontraron " + entities.size() + " registros: ");
			}
		}
		return entities;
	}

	private InstitucionDTO mapInstitucionDomainToDto(Institucion institucion) {
		InstitucionDTO institucionDTO = new InstitucionDTO();
		institucionDTO.setCodMod(institucion.getCodMod());
		institucionDTO.setAnexo(institucion.getAnexo());
		institucionDTO.setCodlocal(institucion.getCodlocal());
		institucionDTO.setCenEdu(institucion.getCenEdu());
		institucionDTO.setDirCen(institucion.getDirCen());
		institucionDTO.setTelefono(institucion.getTelefono());
		institucionDTO.setEmail(institucion.getEmail());
		institucionDTO.setPagweb(institucion.getPagweb());
		institucionDTO.setDirector(institucion.getDirector());
		institucionDTO.setTipoigel(institucion.getTipoigel());
		institucionDTO.setCenPob(institucion.getCenPob());
		institucionDTO.setCodccpp(institucion.getCodccpp());
		institucionDTO.setMcenso(institucion.getMcenso());
		institucionDTO.setProgdist(institucion.getProgdist());
		institucionDTO.setProgarti(institucion.getProgarti());
		institucionDTO.setComenta(institucion.getComenta());
		institucionDTO.setFechareg(institucion.getFechareg());
		institucionDTO.setFecharet(institucion.getFecharet());
		institucionDTO.setRegistro(institucion.getRegistro());
		institucionDTO.setMetodo(institucion.getMetodo());
		institucionDTO.setContjesc(institucion.getContjesc());
		institucionDTO.setFecharea(institucion.getFecharea());
		institucionDTO.setCodcp_inei(institucion.getCodcp_inei());
		institucionDTO.setAlt_cp(institucion.getAlt_cp());
		institucionDTO.setFte_cp(institucion.getFte_cp());
		institucionDTO.setTipoIce(institucion.getTipoIce());
		institucionDTO.setCodCord(institucion.getCodCord());
		institucionDTO.setClongIE(institucion.getClongIE());
		institucionDTO.setClatIE(institucion.getClatIE());
		institucionDTO.setReferencia(institucion.getReferencia());
		institucionDTO.setFteArea(institucion.getFteArea());
		institucionDTO.setClongCp(institucion.getClongCp());
		institucionDTO.setClatCp(institucion.getClatCp());
		institucionDTO.setCpEtnico(institucion.getCpEtnico());
		institucionDTO.setLenEtnica(institucion.getLenEtnica());
		institucionDTO.setNlatCp(institucion.getNlatCp());
		institucionDTO.setNlongCp(institucion.getNlongCp());
		institucionDTO.setNlatIE(institucion.getNlatIE());
		institucionDTO.setNlongIE(institucion.getNlongIE());
		institucionDTO.setNzoom(institucion.getNzoom());
		institucionDTO.setLocalidad(institucion.getLocalidad());
		institucionDTO.setDre(institucion.getDre());
		institucionDTO.setProgise(institucion.getProgise());
		institucionDTO.setCodinst(institucion.getCodinst());
		institucionDTO.setSienvio(institucion.getSienvio());
		institucionDTO.setTipoprog(institucion.getTipoprog());
		institucionDTO.setDistrito(setDistritoDomainToDto(institucion.getDistrito()));
		institucionDTO.setUgel(MapperUgel.setUgelDomainToDto(institucion.getUgel()));
		institucionDTO.setIgel(setIgelDomainToDto(institucion.getIgel()));
		institucionDTO.setForma((FormaDTO) setCodigoDomainToDto(institucion.getForma()));
		institucionDTO.setArea((AreaDTO) setCodigoDomainToDto(institucion.getArea()));
		institucionDTO
				.setSituacionDirector((SituacionDirectorDTO) setCodigoDomainToDto(institucion.getSituacionDirector()));
		institucionDTO.setGestionDependencia(
				(GestionDependenciaDTO) setCodigoDomainToDto(institucion.getGestionDependencia()));
		institucionDTO.setEstado((EstadoDTO) setCodigoDomainToDto(institucion.getEstado()));
		institucionDTO.setNivelModalidad((NivelModalidadDTO) setCodigoDomainToDto(institucion.getNivelModalidad()));
		institucionDTO.setTurno((TurnoDTO) setCodigoDomainToDto(institucion.getTurno()));
		institucionDTO.setGenero((GeneroDTO) setCodigoDomainToDto(institucion.getGenero()));
		institucionDTO.setAreaSig((AreaDTO) setCodigoDomainToDto(institucion.getAreaSig()));
		institucionDTO.setCaracteristica((CaracteristicaDTO) setCodigoDomainToDto(institucion.getCaracteristica()));
		institucionDTO.setGestion((GestionDTO) setCodigoDomainToDto(institucion.getGestion()));
		institucionDTO.setDisVrae((DisVraeDTO) setCodigoDomainToDto(institucion.getDisVrae()));
		institucionDTO.setDisCrecer((DisCrecerDTO) setCodigoDomainToDto(institucion.getDisCrecer()));
		institucionDTO.setDisJuntos((DisJuntosDTO) setCodigoDomainToDto(institucion.getDisJuntos()));

		TotalEstadisticaDTO totalEstadistica = new TotalEstadisticaDTO();
		totalEstadistica.setTotAlumno(0);
		totalEstadistica.setTotDocente(0);
		totalEstadistica.setTotSeccion(0);
		
		institucionDTO.setEstadistica(setEstadisticaToDto(institucion.getCensoestadistica(), totalEstadistica));
		
		if (institucionDTO.getEstadistica() != null && institucionDTO.getEstadistica().size()>0) {
			institucionDTO.setImputado(institucionDTO.getEstadistica().get(0).getImputado());
		}
		
		institucionDTO.setTotalEstadistica(totalEstadistica);
		
		return institucionDTO;
	}
	
	private List<EstadisticaDTO> setEstadisticaToDto(List<Estadistica> liEstadistica, TotalEstadisticaDTO totalEstadistica){
		
		List<EstadisticaDTO> listaddto = new ArrayList<EstadisticaDTO>();
		EstadisticaDTO staddto;
		
		for(Estadistica estad : liEstadistica) {
			staddto = new EstadisticaDTO();
			
			staddto.setCodMod(estad.getCodMod());
			staddto.setAnexo(estad.getAnexo());
			staddto.setTipoReg(estad.getTipoReg());
			/*staddto.setCodMod(estad.getId().getCodMod());
			staddto.setAnexo(estad.getId().getAnexo());
			staddto.setTipoReg(estad.getId().getTipoReg());*/
			staddto.setTalumno(estad.getTalumno());
			staddto.setTseccion(estad.getTseccion());
			staddto.setTdocente(estad.getTdocente());
			staddto.setNivelModalidad((NivelModalidadDTO) setCodigoDomainToDto(estad.getNivelModalidad()));
			staddto.setImputado(estad.getImputado());
			staddto.setTotalh(estad.getTotalh());
			staddto.setTotalm(estad.getTotalm());
			staddto.setMatr1h(estad.getMatr1h());
			staddto.setMatr2h(estad.getMatr2h());
			staddto.setMatr3h(estad.getMatr3h());
			staddto.setMatr4h(estad.getMatr4h());
			staddto.setMatr5h(estad.getMatr5h());
			staddto.setMatr6h(estad.getMatr6h());
			staddto.setMatr7h(estad.getMatr7h());
			staddto.setMatr8h(estad.getMatr8h());
			staddto.setMatr9h(estad.getMatr9h());
			staddto.setMatr10h(estad.getMatr10h());
			
			staddto.setMatr1m(estad.getMatr1m());
			staddto.setMatr2m(estad.getMatr2m());
			staddto.setMatr3m(estad.getMatr3m());
			staddto.setMatr4m(estad.getMatr4m());
			staddto.setMatr5m(estad.getMatr5m());
			staddto.setMatr6m(estad.getMatr6m());
			staddto.setMatr7m(estad.getMatr7m());
			staddto.setMatr8m(estad.getMatr8m());
			staddto.setMatr9m(estad.getMatr9m());
			staddto.setMatr10m(estad.getMatr10m());
			
			totalEstadistica.setTotAlumno(totalEstadistica.getTotAlumno() + estad.getTalumno());
			totalEstadistica.setTotDocente(totalEstadistica.getTotDocente() + estad.getTdocente());
			totalEstadistica.setTotSeccion(totalEstadistica.getTotSeccion() + estad.getTseccion());
			
			listaddto.add(staddto);			
		}

		return listaddto;
	}

	private DistritoDTO setDistritoDomainToDto(Distrito distrito) {
		DistritoDTO distritoDTO = new DistritoDTO();
		if (distrito != null) {
			distritoDTO.setIdDistrito(distrito.getIdDistrito());
			distritoDTO.setNombreDistrito(distrito.getNombreDistrito());
			distritoDTO.setPointX(distrito.getPointX());
			distritoDTO.setPointY(distrito.getPointY());
			distritoDTO.setZoom(distrito.getZoom());
			distritoDTO.setProvincia(setProvinciaDomainToDto(distrito.getProvincia()));
		}
		return distritoDTO;
	}

	private ProvinciaDTO setProvinciaDomainToDto(Provincia provincia) {
		ProvinciaDTO provinciaDTO = new ProvinciaDTO();
		if (provincia != null) {
			provinciaDTO.setIdProvincia(provincia.getIdProvincia());
			provinciaDTO.setNombreProvincia(provincia.getNombreProvincia());
			provinciaDTO.setPointX(provincia.getPointX());
			provinciaDTO.setPointY(provincia.getPointY());
			provinciaDTO.setZoom(provincia.getZoom());
			provinciaDTO.setRegion(setRegionDomainToDto(provincia.getRegion()));
		}
		return provinciaDTO;
	}

	private RegionDTO setRegionDomainToDto(Region region) {
		RegionDTO regionDTO = new RegionDTO();
		if (region != null) {
			regionDTO.setIdRegion(region.getIdRegion());
			regionDTO.setNombreRegion(region.getNombreRegion());
			regionDTO.setPointX(region.getPointX());
			regionDTO.setPointY(region.getPointY());
			regionDTO.setZoom(region.getZoom());
		}
		return regionDTO;
	}
	/*
	 * private UgelDTO setUgelDomainToDto(Ugel ugel) { UgelDTO ugelDTO = new
	 * UgelDTO(); if (ugel != null) { ugelDTO.setIdUgel(ugel.getIdUgel());
	 * ugelDTO.setNombreUgel(ugel.getNombreUgel());
	 * ugelDTO.setPointX(ugel.getPointX()); ugelDTO.setPointY(ugel.getPointY());
	 * ugelDTO.setZoom(ugel.getZoom());
	 * ugelDTO.setDireccionRegional(setDreDomainToDto(ugel.getDireccionRegional()));
	 * } return ugelDTO; }
	 * 
	 * private DireccionRegionalDTO setDreDomainToDto(DireccionRegional dre) {
	 * DireccionRegionalDTO dreDTO = new DireccionRegionalDTO(); if (dre != null) {
	 * dreDTO.setId(dre.getId()); dreDTO.setNombreDre(dre.getNombreDre());
	 * dreDTO.setPointX(dre.getPointX()); dreDTO.setPointY(dre.getPointY());
	 * dreDTO.setZoom(dre.getZoom()); } return dreDTO; }
	 */

	private IgelDTO setIgelDomainToDto(Igel igel) {
		IgelDTO igelDTO = new IgelDTO();
		if (igel != null) {
			igelDTO.setIdIgel(igel.getIdIgel());
			igelDTO.setNombreUgel(igel.getNombreUgel());
			igelDTO.setDireccionRegional(MapperUgel.setDreDomainToDto(igel.getDireccionRegional()));
		}
		return igelDTO;
	}

	private CodigoDTO setCodigoDomainToDto(Codigo codigo) {
		CodigoDTO codigoDTO = null;
		if (codigo instanceof Forma) {
			codigoDTO = new FormaDTO();
		} else if (codigo instanceof Area) {
			codigoDTO = new AreaDTO();
		} else if (codigo instanceof SituacionDirector) {
			codigoDTO = new SituacionDirectorDTO();
		} else if (codigo instanceof GestionDependencia) {
			codigoDTO = new GestionDependenciaDTO();
		} else if (codigo instanceof Estado) {
			codigoDTO = new EstadoDTO();
		} else if (codigo instanceof NivelModalidad) {
			codigoDTO = new NivelModalidadDTO();
		} else if (codigo instanceof Turno) {
			codigoDTO = new TurnoDTO();
		} else if (codigo instanceof Genero) {
			codigoDTO = new GeneroDTO();
		} else if (codigo instanceof Area) {
			codigoDTO = new AreaDTO();
		} else if (codigo instanceof Caracteristica) {
			codigoDTO = new CaracteristicaDTO();
		} else if (codigo instanceof Gestion) {
			codigoDTO = new GestionDTO();
		} else if (codigo instanceof DisVrae) {
			codigoDTO = new DisVraeDTO();
		} else if (codigo instanceof DisCrecer) {
			codigoDTO = new DisCrecerDTO();
		} else if (codigo instanceof DisJuntos) {
			codigoDTO = new DisJuntosDTO();
		}

		if (codigoDTO != null && codigo != null) {
			codigoDTO.setIdCodigo(codigo.getIdCodigo());
			codigoDTO.setValor(codigo.getValor());
			codigoDTO.setOrden(codigo.getOrden());
		}
		return codigoDTO;
	}
}
