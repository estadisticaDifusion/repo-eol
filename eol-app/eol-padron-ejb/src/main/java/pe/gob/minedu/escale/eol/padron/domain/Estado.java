/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@Entity
@XmlRootElement(name = "estado")

@Table(name="v_estado")
//@DiscriminatorValue(value = "estado")
@NamedQueries({
    @NamedQuery(name = "Estado.findAll", query = "SELECT c FROM Estado c ORDER BY c.orden"),
    @NamedQuery(name = "Estado.findById", query = "SELECT c FROM Estado c WHERE c.idCodigo=:id")})
public class Estado extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6037868212271693975L;
}
