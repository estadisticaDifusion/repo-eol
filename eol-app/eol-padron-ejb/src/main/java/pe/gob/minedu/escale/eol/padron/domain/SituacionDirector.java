/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@Entity
@XmlRootElement (name = "situacionDirector")

@Table(name="v_situadir")
//@DiscriminatorValue(value = "situadir")
@NamedQueries({
    @NamedQuery(name = "SituacionDirector.findAll", query = "SELECT c FROM SituacionDirector c ORDER BY c.orden"),
    @NamedQuery(name = "SituacionDirector.findById", query = "SELECT c FROM SituacionDirector c WHERE c.idCodigo=:id")})
public class SituacionDirector extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2253309689686437133L;
}
