/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Diego Silva
 * 
 * Update Author: Ing. Oscar Mateo Date : 09/08/2019 .
 * 
 */
//@XmlRootElement(name = "estadistica")
@Entity
@Table(catalog = "info2019", name = "estadbas19")
public class Estadistica implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static String NO_ES_IMPUTADO = "2";

	@Id
	@Column(name = "cod_mod")
	private String codMod;

	// @Id
	@Column(name = "anexo")
	private String anexo;

	// @Id
	@Column(name = "tiporeg")
	private String tipoReg;

	@Column(name = "talumno")
	private int talumno;
	
	@Column(name = "tseccion")
	private int tseccion;
	
	@Column(name = "tdocente")
	private int tdocente;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id", name = "niv_mod")
	private NivelModalidad nivelModalidad;
	
	@Column(name = "imputado")
	private String imputado;

	@Column(name = "total_h")
	private int totalh;

	@Column(name = "total_m")
	private int totalm;

	@Column(name = "alu01h")
	private int matr1h;

	@Column(name = "alu02h")
	private int matr2h;
	
	@Column(name = "alu03h")
	private int matr3h;
	
	@Column(name = "alu04h")
	private int matr4h;
	
	@Column(name = "alu05h")
	private int matr5h;
	
	@Column(name = "alu06h")
	private int matr6h;
	
	@Column(name = "alu07h")
	private int matr7h;
	
	@Column(name = "alu08h")
	private int matr8h;
	
	@Column(name = "alu09h")
	private int matr9h;
	
	@Column(name = "alu10h")
	private int matr10h;

	@Column(name = "alu01m")
	private int matr1m;
	
	@Column(name = "alu02m")
	private int matr2m;
	
	@Column(name = "alu03m")
	private int matr3m;
	
	@Column(name = "alu04m")
	private int matr4m;
	
	@Column(name = "alu05m")
	private int matr5m;
	
	@Column(name = "alu06m")
	private int matr6m;
	
	@Column(name = "alu07m")
	private int matr7m;
	
	@Column(name = "alu08m")
	private int matr8m;
	
	@Column(name = "alu09m")
	private int matr9m;
	
	@Column(name = "alu10m")
	private int matr10m;

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public int getTalumno() {
		return imputado.equals(NO_ES_IMPUTADO) ? talumno : 0;
	}

	public void setTalumno(int talumno) {
		this.talumno = talumno;
	}

	public int getTdocente() {
		return imputado.equals(NO_ES_IMPUTADO) ? tdocente : 0;
	}

	public void setTdocente(int tdocente) {
		this.tdocente = tdocente;
	}

	public String getTipoReg() {
		return tipoReg;
	}

	public void setTipoReg(String tipoReg) {
		this.tipoReg = tipoReg;
	}

	public int getTseccion() {
		return imputado.equals(NO_ES_IMPUTADO) ? tseccion : 0;
	}

	public void setTseccion(int tseccion) {
		this.tseccion = tseccion;
	}

	public NivelModalidad getNivelModalidad() {
		return nivelModalidad;
	}

	public void setNivelModalidad(NivelModalidad nivelModalidad) {
		this.nivelModalidad = nivelModalidad;
	}

	public String getImputado() {
		return imputado;
	}

	public void setImputado(String imputado) {
		this.imputado = imputado;
	}

	public int getMatr1h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr1h : 0;

	}

	public void setMatr1h(int matr1h) {
		this.matr1h = matr1h;
	}

	public int getMatr1m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr1m : 0;

	}

	public void setMatr1m(int matr1m) {
		this.matr1m = matr1m;
	}

	public int getMatr2h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr2h : 0;
	}

	public void setMatr2h(int matr2h) {
		this.matr2h = matr2h;
	}

	public int getMatr2m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr2m : 0;
	}

	public void setMatr2m(int matr2m) {
		this.matr2m = matr2m;
	}

	public int getMatr3h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr3h : 0;
	}

	public void setMatr3h(int matr3h) {
		this.matr3h = matr3h;
	}

	public int getMatr3m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr3m : 0;
	}

	public void setMatr3m(int matr3m) {
		this.matr3m = matr3m;
	}

	public int getMatr4h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr4h : 0;
	}

	public void setMatr4h(int matr4h) {
		this.matr4h = matr4h;
	}

	public int getMatr4m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr4m : 0;
	}

	public void setMatr4m(int matr4m) {
		this.matr4m = matr4m;
	}

	public int getMatr5h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr5h : 0;
	}

	public void setMatr5h(int matr5h) {
		this.matr5h = matr5h;
	}

	public int getMatr5m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr5m : 0;
	}

	public void setMatr5m(int matr5m) {
		this.matr5m = matr5m;
	}

	public int getMatr6h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr6h : 0;
	}

	public void setMatr6h(int matr6h) {
		this.matr6h = matr6h;
	}

	public int getMatr6m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr6m : 0;
	}

	public void setMatr6m(int matr6m) {
		this.matr6m = matr6m;
	}

	public int getMatr7h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr7h : 0;
	}

	public void setMatr7h(int matr7h) {
		this.matr7h = matr7h;
	}

	public int getMatr8h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr8h : 0;
	}

	public void setMatr8h(int matr8h) {
		this.matr8h = matr8h;
	}

	public int getMatr9h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr9h : 0;
	}

	public void setMatr9h(int matr9h) {
		this.matr9h = matr9h;
	}

	public int getMatr10h() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr10h : 0;
	}

	public void setMatr10h(int matr10h) {
		this.matr10h = matr10h;
	}

	public int getMatr7m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr7m : 0;
	}

	public void setMatr7m(int matr7m) {
		this.matr7m = matr7m;
	}

	public int getMatr8m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr8m : 0;
	}

	public void setMatr8m(int matr8m) {
		this.matr8m = matr8m;
	}

	public int getMatr9m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr9m : 0;

	}

	public void setMatr9m(int matr9m) {
		this.matr9m = matr9m;
	}

	public int getMatr10m() {
		return imputado.equals(NO_ES_IMPUTADO) ? matr10m : 0;
	}

	public void setMatr10m(int matr10m) {
		this.matr10m = matr10m;
	}

	public int getTotalh() {
		return imputado.equals(NO_ES_IMPUTADO) ? totalh : 0;
	}

	public void setTotalh(int totalh) {
		this.totalh = totalh;
	}

	public int getTotalm() {
		return imputado.equals(NO_ES_IMPUTADO) ? totalm : 0;

	}

	public void setTotalm(int totalm) {
		this.totalm = totalm;
	}

}
