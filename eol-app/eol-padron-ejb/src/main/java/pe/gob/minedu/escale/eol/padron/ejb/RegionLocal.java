package pe.gob.minedu.escale.eol.padron.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.dto.entities.RegionDTO;
import pe.gob.minedu.escale.eol.padron.domain.Region;

@Local
public interface RegionLocal {

	public Region findById(String codigoRegion);
	
	RegionDTO getRegion(String codigoRegion);
	
	List<RegionDTO> getListRegionDTO();
}
