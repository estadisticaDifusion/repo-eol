package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;

@Entity
public class InfoMEdad implements Serializable {

	private static final long serialVersionUID = 1L;
	/*
	 * @EmbeddedId private InfoMEdadId infoMEdadId;
	 */

	@Id
	@Column(name = "cod_mod")
	private String codMod;

	// @Id
	@Column(name = "anexo")
	private String anexo;

	// @Id
	@Column(name = "niv_mod")
	private String nivMod;

	// @Id
	@Column(name = "tipdato")
	private int tipdato;

	@Column(name = "th")
	private int th;
	@Column(name = "tm")
	private int tm;
	/*
	 * public InfoMEdadId getInfoMEdadId() { return infoMEdadId; }
	 * 
	 * public void setInfoMEdadId(InfoMEdadId infoMEdadId) { this.infoMEdadId =
	 * infoMEdadId; }
	 */

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	//@XmlElement(name = "tipdato")
	public int getTipdato() {
		return tipdato;
	}

	public void setTipdato(int tipdato) {
		this.tipdato = tipdato;
	}

	@XmlElement(name = "th")
	public int getTh() {
		return th;
	}

	public void setTh(int th) {
		this.th = th;
	}

	@XmlElement(name = "tm")
	public int getTm() {
		return tm;
	}

	public void setTm(int tm) {
		this.tm = tm;
	}

}
