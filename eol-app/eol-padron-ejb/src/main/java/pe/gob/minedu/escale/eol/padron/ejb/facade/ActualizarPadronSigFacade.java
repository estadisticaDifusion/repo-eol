///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package pe.gob.minedu.escale.eol.padron.ejb.facade;
//
//import java.io.IOException;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Logger;
//
//import javax.ejb.Singleton;
//
//import pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronSigLocal;
//import pe.gob.minedu.escale.eol.padron.importar.dao.ActualizarPadronSigDao;
//import pe.gob.minedu.escale.eol.padron.importar.dao.DAOFactory;
//
///**
// *
// *
// * @author MHUAMANI
// * @version 1.0
// */
//@Singleton
//public class ActualizarPadronSigFacade implements ActualizarPadronSigLocal {
//
//	private static final Logger LOGGER = Logger.getLogger(ActualizarPadronSigFacade.class.getName());
//
//	@Override
//	public synchronized void actualizar(List<Map<String, String>> mapSIG) throws IOException {
//	    LOGGER.info("iniciando actualizador");
//
//	    ActualizarPadronSigDao daoSIG = DAOFactory.getInstance().getActualizarPadronSigDAO();
//	    LOGGER.info("Inicio carga de padron SIG");
//	    for(Map<String,String> fila:mapSIG) {
//	        LOGGER.info("Add padron SIG");
//	        daoSIG.agregarSIG(fila);
//	    }
//	    LOGGER.info("Fin de carga de padron SIG");
//	}
//
//}
