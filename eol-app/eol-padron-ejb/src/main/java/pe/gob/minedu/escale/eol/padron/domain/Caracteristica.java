/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "caracteristica")

@Entity
@Table(name = "v_cod_car")
//@DiscriminatorValue(value = "v_cod_car")
@NamedQueries({
    @NamedQuery(name = "Caracteristica.findAll", query = "SELECT c FROM Caracteristica c ORDER BY c.orden")
    ,
    @NamedQuery(name = "Caracteristica.findById", query = "SELECT c FROM Caracteristica c WHERE c.idCodigo=:id")})
public class Caracteristica extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3126390921075896785L;
}
