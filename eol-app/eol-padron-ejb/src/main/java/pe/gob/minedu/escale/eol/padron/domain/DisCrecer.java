/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "disCrecer")

@Entity
//@Immutable
@Table(name = "v_dis_crecer")
//@DiscriminatorValue(value = "v_gestion")
@NamedQueries({
    @NamedQuery(name = "DisCrecer.findAll", query = "SELECT c FROM DisCrecer c ORDER BY c.orden"),
    @NamedQuery(name = "DisCrecer.findById", query = "SELECT c FROM DisCrecer c WHERE c.idCodigo=:id")})
public class DisCrecer extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5881573269710835601L;
    
}