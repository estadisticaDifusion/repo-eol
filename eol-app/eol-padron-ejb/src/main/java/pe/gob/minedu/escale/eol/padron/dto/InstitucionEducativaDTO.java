package pe.gob.minedu.escale.eol.padron.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class InstitucionEducativaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4589280719846030846L;
	private String codigoModular;
	private String anexo;
	private String codigoLocal;
	private String nombreIE;
	private String codDreUgel;
	private String nomDreUgel;
	private String ubigeo;
	private String codCentroPoblado;
	private String centroPoblado;
	private String codNivelModalidad;
	private String codGestion;
	private String descGestion;
	private String forma;
	private String descForma;
	private String codTurno;
	private String codArea;
	private String estado;
	private String descEstado;
	private String direccion;
	private String telefono;
	private String paginaWeb;
	private String correoElectronico;
	private String director;
	private String tipoSexo;
	private BigDecimal coordX;
	private BigDecimal coordY;
	private String departamento;
	private String provincia;
	private String distrito;
	private String nivelModalidad;
	private Integer alumnos;
	private Integer docentes;
	private String descTipoSexo;
	private String descTurno;
	private String descArea;
	private String altCP;
	private String disCrecer;
	private String disJuntos;
	private String disVrae;
	private String codDependencia;
	private String descDependencia;
	private String codCaracteristica;
	private String descCaracteristica;

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getCodigoLocal() {
		return codigoLocal;
	}

	public void setCodigoLocal(String codigoLocal) {
		this.codigoLocal = codigoLocal;
	}

	public String getNombreIE() {
		return nombreIE;
	}

	public void setNombreIE(String nombreIE) {
		this.nombreIE = nombreIE;
	}

	public String getCodDreUgel() {
		return codDreUgel;
	}

	public void setCodDreUgel(String codDreUgel) {
		this.codDreUgel = codDreUgel;
	}

	public String getNomDreUgel() {
		return nomDreUgel;
	}

	public void setNomDreUgel(String nomDreUgel) {
		this.nomDreUgel = nomDreUgel;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getCodCentroPoblado() {
		return codCentroPoblado;
	}

	public void setCodCentroPoblado(String codCentroPoblado) {
		this.codCentroPoblado = codCentroPoblado;
	}

	public String getCentroPoblado() {
		return centroPoblado;
	}

	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}

	public String getCodNivelModalidad() {
		return codNivelModalidad;
	}

	public void setCodNivelModalidad(String codNivelModalidad) {
		this.codNivelModalidad = codNivelModalidad;
	}

	public String getCodGestion() {
		return codGestion;
	}

	public void setCodGestion(String codGestion) {
		this.codGestion = codGestion;
	}

	public String getDescGestion() {
		return descGestion;
	}

	public void setDescGestion(String descGestion) {
		this.descGestion = descGestion;
	}

	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

	public String getDescForma() {
		return descForma;
	}

	public void setDescForma(String descForma) {
		this.descForma = descForma;
	}

	public String getCodTurno() {
		return codTurno;
	}

	public void setCodTurno(String codTurno) {
		this.codTurno = codTurno;
	}

	public String getCodArea() {
		return codArea;
	}

	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDescEstado() {
		return descEstado;
	}

	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPaginaWeb() {
		return paginaWeb;
	}

	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getTipoSexo() {
		return tipoSexo;
	}

	public void setTipoSexo(String tipoSexo) {
		this.tipoSexo = tipoSexo;
	}

	public BigDecimal getCoordX() {
		return coordX;
	}

	public void setCoordX(BigDecimal coordX) {
		this.coordX = coordX;
	}

	public BigDecimal getCoordY() {
		return coordY;
	}

	public void setCoordY(BigDecimal coordY) {
		this.coordY = coordY;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getNivelModalidad() {
		return nivelModalidad;
	}

	public void setNivelModalidad(String nivelModalidad) {
		this.nivelModalidad = nivelModalidad;
	}

	public Integer getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(Integer alumnos) {
		this.alumnos = alumnos;
	}

	public Integer getDocentes() {
		return docentes;
	}

	public void setDocentes(Integer docentes) {
		this.docentes = docentes;
	}

	public String getDescTipoSexo() {
		return descTipoSexo;
	}

	public void setDescTipoSexo(String descTipoSexo) {
		this.descTipoSexo = descTipoSexo;
	}

	public String getDescTurno() {
		return descTurno;
	}

	public void setDescTurno(String descTurno) {
		this.descTurno = descTurno;
	}

	public String getDescArea() {
		return descArea;
	}

	public void setDescArea(String descArea) {
		this.descArea = descArea;
	}

	public String getAltCP() {
		return altCP;
	}

	public void setAltCP(String altCP) {
		this.altCP = altCP;
	}

	public String getDisCrecer() {
		return disCrecer;
	}

	public void setDisCrecer(String disCrecer) {
		this.disCrecer = disCrecer;
	}

	public String getDisJuntos() {
		return disJuntos;
	}

	public void setDisJuntos(String disJuntos) {
		this.disJuntos = disJuntos;
	}

	public String getDisVrae() {
		return disVrae;
	}

	public void setDisVrae(String disVrae) {
		this.disVrae = disVrae;
	}

	public String getCodDependencia() {
		return codDependencia;
	}

	public void setCodDependencia(String codDependencia) {
		this.codDependencia = codDependencia;
	}

	public String getDescDependencia() {
		return descDependencia;
	}

	public void setDescDependencia(String descDependencia) {
		this.descDependencia = descDependencia;
	}

	public String getCodCaracteristica() {
		return codCaracteristica;
	}

	public void setCodCaracteristica(String codCaracteristica) {
		this.codCaracteristica = codCaracteristica;
	}

	public String getDescCaracteristica() {
		return descCaracteristica;
	}

	public void setDescCaracteristica(String descCaracteristica) {
		this.descCaracteristica = descCaracteristica;
	}

}
