/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "distritos")
public class Distrito implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8517343602088309115L;
	@Id
	@Column(name = "id_distrito")
	private String idDistrito;
	
	@Column(name = "distrito")
	private String nombreDistrito;
	
	@ManyToOne
	@JoinColumn(name = "id_provincia", referencedColumnName = "id_provincia")
	private Provincia provincia;
	
	@Column(name = "point_x")
	private Double pointX;
	
	@Column(name = "point_y")
	private Double pointY;
	
	@Column(name = "zoom")
	private Integer zoom;

	public Distrito() {
	}

	public Distrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getNombreDistrito() {
		return nombreDistrito;
	}

	public void setNombreDistrito(String nombreDistrito) {
		this.nombreDistrito = nombreDistrito;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public Double getPointX() {
		return pointX;
	}

	public void setPointX(Double pointX) {
		this.pointX = pointX;
	}

	public Double getPointY() {
		return pointY;
	}

	public void setPointY(Double pointY) {
		this.pointY = pointY;
	}

	public Integer getZoom() {
		return zoom;
	}

	public void setZoom(Integer zoom) {
		this.zoom = zoom;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Distrito other = (Distrito) obj;
		if ((this.idDistrito == null) ? (other.idDistrito != null) : !this.idDistrito.equals(other.idDistrito)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 29 * hash + (this.idDistrito != null ? this.idDistrito.hashCode() : 0);
		return hash;
	}

	@Override
	public String toString() {
		return getProvincia().getRegion().getNombreRegion() + " / " + getProvincia().getNombreProvincia() + " / "
				+ nombreDistrito;
	}
}
