/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.padron.dto.CensoieDTO;
import pe.gob.minedu.escale.eol.padron.dto.InstitucionEducativaDTO;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Local
public interface InstitucionLocal {

	List<InstitucionDTO> getInstitucionList(String codmod, String codlocal, String ubigeo, String codooii,
			String nombreCP, String nombreIE, String disVrae, String disJuntos, String disCrecer, String disNinguno,
			String matIndigena, List<String> niveles, List<String> gestiones, List<String> areas, String estado,
			List<String> tipoices, List<String> formas, List<String> estados, String progarti, String progise,
			int start, int max, String orderBy);

	Long getRecordCount(String codmod, String codlocal, String ubigeo, String codooii, String nombreCP, String nombreIE,
			String disVrae, String disJuntos, String disCrecer, String disNinguno, String matIndigena,
			List<String> niveles, List<String> gestiones, List<String> areas, String estado, List<String> tipoices,
			List<String> formas, List<String> estados, String progarti, String progise);

	List<InstitucionEducativaDTO> getInstitucionEducativaList(String ubigeo, String dreugel, String idCentroPoblado,
			String nomCentroPoblado, String codigoModular, String nombreIE, String codigoLocal, String direccionIE,
			String progarti, String mcenso, String progise, List<String> gestiones, List<String> niveles,
			List<String> areas);
	
	CensoieDTO getDataCensoIE(String codmod,String anexo,String anio,String nivel);
}
