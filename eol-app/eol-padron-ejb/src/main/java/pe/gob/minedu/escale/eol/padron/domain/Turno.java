/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "turno")
@Entity
@Table(name="v_cod_tur")
//@DiscriminatorValue(value = "cod_tur")
@NamedQueries({
    @NamedQuery(name = "Turno.findAll", query = "SELECT c FROM Turno c ORDER BY c.orden"),
    @NamedQuery(name = "Turno.findById", query = "SELECT c FROM Turno c WHERE c.idCodigo=:id")})
public class Turno extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4936800752902895870L;
}
