package pe.gob.minedu.escale.eol.padron.mappers;

import pe.gob.minedu.escale.eol.dto.entities.DireccionRegionalDTO;
import pe.gob.minedu.escale.eol.dto.entities.UgelDTO;
import pe.gob.minedu.escale.eol.padron.domain.DireccionRegional;
import pe.gob.minedu.escale.eol.padron.domain.Ugel;

public class MapperUgel {

	public static UgelDTO setUgelDomainToDto(Ugel ugel) {
		UgelDTO ugelDTO = new UgelDTO();
		if (ugel != null) {
			ugelDTO.setIdUgel(ugel.getIdUgel());
			ugelDTO.setNombreUgel(ugel.getNombreUgel());
			ugelDTO.setPointX(ugel.getPointX());
			ugelDTO.setPointY(ugel.getPointY());
			ugelDTO.setZoom(ugel.getZoom());
			ugelDTO.setDireccionRegional(setDreDomainToDto(ugel.getDireccionRegional()));
		}
		return ugelDTO;
	}

	public static DireccionRegionalDTO setDreDomainToDto(DireccionRegional dre) {
		DireccionRegionalDTO dreDTO = new DireccionRegionalDTO();
		if (dre != null) {
			dreDTO.setId(dre.getId());
			dreDTO.setNombreDre(dre.getNombreDre());
			dreDTO.setPointX(dre.getPointX());
			dreDTO.setPointY(dre.getPointY());
			dreDTO.setZoom(dre.getZoom());
		}
		return dreDTO;
	}
}
