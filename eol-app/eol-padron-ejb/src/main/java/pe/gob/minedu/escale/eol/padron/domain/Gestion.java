/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "gestion")

@Entity
@Table(name="v_gestion")
//@DiscriminatorValue(value = "v_gestion")
@NamedQueries({
    @NamedQuery(name = "Gestion.findAll", query = "SELECT c FROM Gestion c ORDER BY c.orden"),
    @NamedQuery(name = "Gestion.findById", query = "SELECT c FROM Gestion c WHERE c.idCodigo=:id")})
public class Gestion extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7837196632873484931L;
}
