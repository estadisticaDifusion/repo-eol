/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "ugels")
public class Ugel implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3116180821442586447L;
	@Id
    @Column(name = "id_ugel")
    private String idUgel;
	
    @Column(name = "ugel")
    private String nombreUgel;
    
    @Column(name = "point_x")
    private Double pointX;
    
    @Column(name = "point_y")
    private Double pointY;
    
    @Column(name = "zoom")
    private Integer zoom;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id_dre", name = "id_dre")
    private DireccionRegional direccionRegional;

    public Ugel() {
    }

    public String getIdUgel() {
        return idUgel;
    }

    public void setIdUgel(String idUgel) {
        this.idUgel = idUgel;
    }

    public String getNombreUgel() {
        return nombreUgel;
    }

    public void setNombreUgel(String nombreUgel) {
        this.nombreUgel = nombreUgel;
    }

    public DireccionRegional getDireccionRegional() {
        return direccionRegional;
    }

    public void setDireccionRegional(DireccionRegional direccionRegional) {
        this.direccionRegional = direccionRegional;
    }

    public double getPointX() {
        return pointX;
    }

    public void setPointX(double pointX) {
        this.pointX = pointX;
    }

    public double getPointY() {
        return pointY;
    }

    public void setPointY(double pointY) {
        this.pointY = pointY;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }
}
