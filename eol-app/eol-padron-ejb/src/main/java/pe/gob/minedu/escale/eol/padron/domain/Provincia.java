/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "provincias")
public class Provincia implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4269107507519820482L;

	@Id
	@Column(name = "id_provincia")
	private String idProvincia;

	@Column(name = "provincia")
	private String nombreProvincia;

	@ManyToOne
	@JoinColumn(referencedColumnName = "id_region", name = "id_region")
	private Region region;

	@Column(name = "point_x")
	private Double pointX;

	@Column(name = "point_y")
	private Double pointY;

	@Column(name = "zoom")
	private Integer zoom;

	public Provincia() {
	}

	public Provincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getNombreProvincia() {
		return nombreProvincia;
	}

	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Double getPointX() {
		return pointX;
	}

	public void setPointX(Double pointX) {
		this.pointX = pointX;
	}

	public Double getPointY() {
		return pointY;
	}

	public void setPointY(Double pointY) {
		this.pointY = pointY;
	}

	public Integer getZoom() {
		return zoom;
	}

	public void setZoom(Integer zoom) {
		this.zoom = zoom;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Provincia other = (Provincia) obj;
		if ((this.idProvincia == null) ? (other.idProvincia != null) : !this.idProvincia.equals(other.idProvincia)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 47 * hash + (this.idProvincia != null ? this.idProvincia.hashCode() : 0);
		return hash;
	}

}
