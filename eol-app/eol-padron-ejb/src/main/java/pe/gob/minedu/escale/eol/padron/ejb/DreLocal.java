package pe.gob.minedu.escale.eol.padron.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.dto.entities.DireccionRegionalDTO;
import pe.gob.minedu.escale.eol.padron.domain.DireccionRegional;

/**
 *
 * @author Ing. Oscar Mateo
 */
@Local
public interface DreLocal {
	DireccionRegional findById(String codigoDrel);
	
	List<DireccionRegionalDTO> gestListDre();
}
