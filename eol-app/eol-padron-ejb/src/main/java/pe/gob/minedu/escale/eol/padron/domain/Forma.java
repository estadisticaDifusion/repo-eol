/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.padron.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "v_formas")
//@DiscriminatorValue(value = "formas")
@NamedQueries({
    @NamedQuery(name = "Forma.findAll", query = "SELECT c FROM Forma c ORDER BY c.orden"),
    @NamedQuery(name = "Forma.findById", query = "SELECT c FROM Forma c WHERE c.idCodigo=:id")})
@XmlRootElement(name="forma")
public class Forma extends Codigo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 811586013938702454L;
}
