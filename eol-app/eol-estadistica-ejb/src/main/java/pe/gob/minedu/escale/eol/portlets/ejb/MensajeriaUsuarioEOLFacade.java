/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.EolMensajeriaUsuario;

/**
 *
 * @author JMATAMOROS
 */
@Stateless
public class MensajeriaUsuarioEOLFacade extends AbstractFacade<EolMensajeriaUsuario> {
	
	@PersistenceContext(unitName = "eol-PU") // eol-portletPU
	private EntityManager em;

	public MensajeriaUsuarioEOLFacade() {
		super(EolMensajeriaUsuario.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public List<EolMensajeriaUsuario> findMsjsLeidosByUsuario(String usuario) {
		Query q = em.createQuery("SELECT MU FROM EolMensajeriaUsuario MU WHERE MU.usuario=:usuario ");
		q.setParameter("usuario", usuario);
		// q.setParameter("estado", new Integer(1));
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<EolMensajeriaUsuario> findMsjsLeidosByUsuarioAndMsj(String usuario, Integer idMsj) {
		Query q = em.createQuery(
				"SELECT MU FROM EolMensajeriaUsuario MU WHERE MU.usuario=:usuario AND MU.mensaje.id=:idMsj ");
		// EolMensajeriaUsuario D;
		// D.getMensaje().getId()
		q.setParameter("usuario", usuario);
		q.setParameter("idMsj", idMsj);
		return q.getResultList();
	}

	public Long findMsjsDeleteByBusuario(String usuario) {
		Query q = em.createNativeQuery(
				"SELECT COUNT(*)  FROM eol_mensajeria M INNER JOIN  eol_mensajeria_usuario U ON M.ID= U.ID_MENSAJE WHERE U.USUARIO =? AND U.ESTADO =2");
		q.setParameter(1, usuario);
		return (Long) q.getSingleResult();
	}

}
