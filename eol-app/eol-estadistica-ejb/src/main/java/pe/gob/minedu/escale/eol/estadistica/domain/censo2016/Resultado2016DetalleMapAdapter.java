/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2016.Resultado2016DetalleMapAdapter.Resultado2016DetalleList;

/**
 *
 * @author Administrador
 */
public class Resultado2016DetalleMapAdapter extends XmlAdapter<Resultado2016DetalleList, Map<String, Resultado2016Detalle>> {

    private static final Logger LOGGER = Logger.getLogger(Resultado2016DetalleMapAdapter.class.getName());

    static class Resultado2016DetalleList {

        private List<Resultado2016Detalle> detalle;

        private Resultado2016DetalleList(ArrayList<Resultado2016Detalle> lista) {
            detalle = lista;
        }

        public Resultado2016DetalleList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Resultado2016Detalle> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Resultado2016Detalle> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Resultado2016Detalle> unmarshal(Resultado2016DetalleList v) throws Exception {
        Map<String, Resultado2016Detalle> map = new HashMap<String, Resultado2016Detalle>();
        for (Resultado2016Detalle detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Resultado2016DetalleList marshal(Map<String, Resultado2016Detalle> v) throws Exception {
        if(v==null)
        {   LOGGER.fine("no hay detalles");
            return null;
        }
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Resultado2016Detalle> lista = new ArrayList<Resultado2016Detalle>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Resultado2016Detalle $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Resultado2016DetalleList list = new Resultado2016DetalleList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }

}
