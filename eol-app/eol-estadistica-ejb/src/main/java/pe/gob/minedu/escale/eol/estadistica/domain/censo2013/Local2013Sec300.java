/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "local2013_sec300")
public class Local2013Sec300 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P302_1")
    private Integer p3021;
    @Column(name = "P302_2")
    private String p3022;
    @Column(name = "P302_3")
    private String p3023;
    @Column(name = "P302_4M1")
    private String p3024m1;
    @Column(name = "P302_4M2")
    private String p3024m2;
    @Column(name = "P302_4M3")
    private String p3024m3;
    @Column(name = "P302_4M4")
    private Integer p3024m4;
    @Column(name = "P302_4T1")
    private String p3024t1;
    @Column(name = "P302_4T2")
    private String p3024t2;
    @Column(name = "P302_4T3")
    private String p3024t3;
    @Column(name = "P302_4T4")
    private Integer p3024t4;
    @Column(name = "P302_4N1")
    private String p3024n1;
    @Column(name = "P302_4N2")
    private String p3024n2;
    @Column(name = "P302_4N3")
    private String p3024n3;
    @Column(name = "P302_4N4")
    private Integer p3024n4;
    @Column(name = "P302_5")
    private Integer p3025;
    @Column(name = "P302_6")
    private String p3026;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2013Cabecera local2013Cabecera;

    public Local2013Sec300() {
    }

    public Local2013Sec300(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P302_1")
    public Integer getP3021() {
        return p3021;
    }

    public void setP3021(Integer p3021) {
        this.p3021 = p3021;
    }

    @XmlElement(name="P302_2")
    public String getP3022() {
        return p3022;
    }

    public void setP3022(String p3022) {
        this.p3022 = p3022;
    }

    @XmlElement(name="P302_3")
    public String getP3023() {
        return p3023;
    }

    public void setP3023(String p3023) {
        this.p3023 = p3023;
    }

    @XmlElement(name="P302_4M1")
    public String getP3024m1() {
        return p3024m1;
    }

    public void setP3024m1(String p3024m1) {
        this.p3024m1 = p3024m1;
    }

    @XmlElement(name="P302_4M2")
    public String getP3024m2() {
        return p3024m2;
    }

    public void setP3024m2(String p3024m2) {
        this.p3024m2 = p3024m2;
    }

    @XmlElement(name="P302_4M3")
    public String getP3024m3() {
        return p3024m3;
    }

    public void setP3024m3(String p3024m3) {
        this.p3024m3 = p3024m3;
    }

    @XmlElement(name="P302_4M4")
    public Integer getP3024m4() {
        return p3024m4;
    }

    public void setP3024m4(Integer p3024m4) {
        this.p3024m4 = p3024m4;
    }

    @XmlElement(name="P302_4T1")
    public String getP3024t1() {
        return p3024t1;
    }

    public void setP3024t1(String p3024t1) {
        this.p3024t1 = p3024t1;
    }

    @XmlElement(name="P302_4T2")
    public String getP3024t2() {
        return p3024t2;
    }

    public void setP3024t2(String p3024t2) {
        this.p3024t2 = p3024t2;
    }

    @XmlElement(name="P302_4T3")
    public String getP3024t3() {
        return p3024t3;
    }

    public void setP3024t3(String p3024t3) {
        this.p3024t3 = p3024t3;
    }

    @XmlElement(name="P302_4T4")
    public Integer getP3024t4() {
        return p3024t4;
    }

    public void setP3024t4(Integer p3024t4) {
        this.p3024t4 = p3024t4;
    }

    @XmlElement(name="P302_4N1")
    public String getP3024n1() {
        return p3024n1;
    }

    public void setP3024n1(String p3024n1) {
        this.p3024n1 = p3024n1;
    }

    @XmlElement(name="P302_4N2")
    public String getP3024n2() {
        return p3024n2;
    }

    public void setP3024n2(String p3024n2) {
        this.p3024n2 = p3024n2;
    }

    @XmlElement(name="P302_4N3")
    public String getP3024n3() {
        return p3024n3;
    }

    public void setP3024n3(String p3024n3) {
        this.p3024n3 = p3024n3;
    }

    @XmlElement(name="P302_4N4")
    public Integer getP3024n4() {
        return p3024n4;
    }

    public void setP3024n4(Integer p3024n4) {
        this.p3024n4 = p3024n4;
    }

    @XmlElement(name="P302_5")
    public Integer getP3025() {
        return p3025;
    }

    public void setP3025(Integer p3025) {
        this.p3025 = p3025;
    }

    @XmlElement(name="P302_6")
    public String getP3026() {
        return p3026;
    }

    public void setP3026(String p3026) {
        this.p3026 = p3026;
    }

    @XmlTransient
    public Local2013Cabecera getLocal2013Cabecera() {
        return local2013Cabecera;
    }

    public void setLocal2013Cabecera(Local2013Cabecera local2013Cabecera) {
        this.local2013Cabecera = local2013Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2013Sec300)) {
            return false;
        }
        Local2013Sec300 other = (Local2013Sec300) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

   
}
