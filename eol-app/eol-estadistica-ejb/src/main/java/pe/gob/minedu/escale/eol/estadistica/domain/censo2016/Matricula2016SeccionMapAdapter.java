package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2016.Matricula2016SeccionMapAdapter.Matricula2016SeccionList;

public class Matricula2016SeccionMapAdapter extends XmlAdapter<Matricula2016SeccionList, Map<String, Matricula2016Seccion>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2016SeccionMapAdapter.class.getName());

    static class Matricula2016SeccionList {

        private List<Matricula2016Seccion> detalle;

        private Matricula2016SeccionList(ArrayList<Matricula2016Seccion> lista) {
            detalle = lista;
        }

        public Matricula2016SeccionList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2016Seccion> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2016Seccion> detalle) {
            this.detalle = detalle;
        }
    }


    @Override
    public Map<String, Matricula2016Seccion> unmarshal(Matricula2016SeccionList v) throws Exception {

        Map<String, Matricula2016Seccion> map = new HashMap<String, Matricula2016Seccion>();
        for (Matricula2016Seccion detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2016SeccionList marshal(Map<String, Matricula2016Seccion> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2016Seccion> lista = new ArrayList<Matricula2016Seccion>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2016Seccion $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2016SeccionList list = new Matricula2016SeccionList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }

}
