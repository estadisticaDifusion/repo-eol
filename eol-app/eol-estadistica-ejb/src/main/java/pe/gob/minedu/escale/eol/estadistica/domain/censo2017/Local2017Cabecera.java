package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2017_cabecera")
public class Local2017Cabecera implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Column(name = "NOMBVIA")
    private String nombvia;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "MANZANA")
    private String manzana;
    @Column(name = "LOTE")
    private String lote;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Column(name = "ETAPA")
    private String etapa;
    @Column(name = "SECTOR")
    private String sector;
    @Column(name = "ZONA")
    private String zona;
    @Column(name = "OTROS")
    private String otros;
    @Column(name = "CATEG_CP")
    private String categCp;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "CODCCPP")
    private String codccpp;
    @Column(name = "REFERENCIA")
    private String referencia;
    @Column(name = "TELEFONO")
    private String telefono;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "DPTO")
    private String dpto;
    @Column(name = "PROV")
    private String prov;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "COD_AREA")
    private String codArea;
    @Column(name = "COND_TEN")
    private String condTen;
    @Column(name = "PROPLOCAL")
    private String proplocal;
    @Column(name = "ENTIDAD")
    private String entidad;
    @Column(name = "FICHA_REG")
    private String fichaReg;
    @Column(name = "OFICINA_REG")
    private String oficinaReg;
    @Column(name = "GESTION")
    private String gestion;
    //inicio
    @Column(name = "P201_AT")
    private Integer p201At;
    @Column(name = "P201_AC")
    private Integer p201Ac;
    @Column(name = "P202")
    private String p202;
    @Column(name = "P203_MAT")
    private String p203Mat;
    @Column(name = "P203_EST")
    private String p203Est;
    @Column(name = "P204")
    private String p204;
    @Column(name = "P205")
    private String p205;
    @Column(name = "P205_TP")
    private String p205Tp;
    @Column(name = "P206")
    private Integer p206;
    @Column(name = "P301_ADM")
    private Integer p301Adm;
    @Column(name = "P301_EDU")
    private Integer p301Edu;
    @Column(name = "P303_EOB")
    private String p303Eob;
    @Column(name = "P303_INI")
    private String p303Ini;
    @Column(name = "P303_PRI")
    private String p303Pri;
    @Column(name = "P303_SEC")
    private String p303Sec;
    @Column(name = "P303_EBA")
    private String p303Eba;
    @Column(name = "P303_EBE")
    private String p303Ebe;
    @Column(name = "P303_SUP")
    private String p303Sup;
    @Column(name = "P303_ETP")
    private String p303Etp;
    @Column(name = "P304")
    private Integer p304;
    @Column(name = "P401")
    private String p401;
    @Column(name = "P402_1")
    private String p4021;
    @Column(name = "P402_2")
    private String p4022;
    @Column(name = "P402_3")
    private String p4023;
    @Column(name = "P402_4")
    private String p4024;
    @Column(name = "P402_5")
    private String p4025;
    @Column(name = "P402_6")
    private String p4026;
    @Column(name = "P402_7")
    private String p4027;
    @Column(name = "P402_8")
    private String p4028;
    @Column(name = "P402_9")
    private String p4029;
    @Column(name = "P402_10")
    private String p40210;
    @Column(name = "P402_11")
    private String p40211;
    @Column(name = "P402_12")
    private String p40212;
    @Column(name = "P402_13")
    private String p40213;
    @Column(name = "P402_14")
    private String p40214;
    @Column(name = "P403")
    private String p403;
    @Column(name = "P404")
    private String p404;
    @Column(name = "P406_1")
    private String p4061;
    @Column(name = "P406_2")
    private String p4062;
    @Column(name = "P406_3")
    private String p4063;
    @Column(name = "P406_4")
    private String p4064;
    @Column(name = "P406_5")
    private String p4065;
    @Column(name = "P406_6")
    private String p4066;
    @Column(name = "P406_7")
    private String p4067;
    @Column(name = "P407")
    private String p407;
    @Column(name = "P407_Qn")
    private Integer p407Qn;
    @Column(name = "P409")
    private String p409;
    @Column(name = "P411_1")
    private Integer p4111;
    @Column(name = "P411_2")
    private Integer p4112;
    @Column(name = "P411_3")
    private Integer p4113;
    @Column(name = "P411_4")
    private Integer p4114;
    @Column(name = "P412")
    private Integer p412;
    @Column(name = "P413_11")
    private String p41311;
    @Column(name = "P413_12")
    private String p41312;
    @Column(name = "P413_13")
    private Short p41313;
    @Column(name = "P413_14")
    private Short p41314;
    @Column(name = "P413_21")
    private String p41321;
    @Column(name = "P413_22")
    private String p41322;
    @Column(name = "P413_23")
    private Short p41323;
    @Column(name = "P413_24")
    private Short p41324;
    @Column(name = "P413_31")
    private String p41331;
    @Column(name = "P413_32")
    private String p41332;
    @Column(name = "P413_33")
    private Short p41333;
    @Column(name = "P413_34")
    private Short p41334;
    @Column(name = "P414")
    private String p414;
    @Column(name = "P414_S")
    private String p414S;
    @Column(name = "P415")
    private String p415;
    @Column(name = "P416")
    private String p416;
    @Column(name = "P416_E")
    private String p416E;
    @Column(name = "P416_S")
    private String p416S;
    @Column(name = "P417")
    private String p417;
    @Column(name = "P417_USO")
    private String p417Uso;
    @Column(name = "P417_EST")
    private String p417Est;
    @Column(name = "P418")
    private String p418;
    @Column(name = "P418_EST")
    private String p418Est;
    @Column(name = "P419")
    private String p419;
    @Column(name = "P421")
    private String p421;
    @Column(name = "P421_Qn")
    private Integer p421Qn;
    @Column(name = "P601_1")
    private String p6011;
    @Column(name = "P601_2")
    private String p6012;
    @Column(name = "P601_3")
    private String p6013;
    @Column(name = "P601_4")
    private String p6014;
    @Column(name = "P601_5")
    private String p6015;
    @Column(name = "P601_6")
    private String p6016;
    @Column(name = "P601_7")
    private String p6017;
    @Column(name = "P601_7T")
    private String p6017t;
    @Column(name = "P601_8")
    private String p6018;
    @Column(name = "P601_9")
    private String p6019;
    @Column(name = "P601_10")
    private String p60110;
    @Column(name = "P601_11")
    private String p60111;
    @Column(name = "P701")
    private String p701;
    @Column(name = "P701_1")
    private String p7011;
    @Column(name = "P701_2")
    private String p7012;
    @Column(name = "P701_3")
    private String p7013;
    @Column(name = "P701_4")
    private String p7014;
    @Column(name = "P701_5")
    private String p7015;
    @Column(name = "P701_6")
    private String p7016;
    @Column(name = "P701_7")
    private String p7017;
    @Column(name = "P701_8")
    private String p7018;
    @Column(name = "P701_9")
    private String p7019;
    @Column(name = "P701_10")
    private String p70110;
    @Column(name = "P701_11")
    private String p70111;
    @Column(name = "P701_12")
    private String p70112;
    @Column(name = "P701_13")
    private String p70113;
    @Column(name = "P701_14")
    private String p70114;
    @Column(name = "P701_15")
    private String p70115;
    @Column(name = "P701_16")
    private String p70116;
    @Column(name = "P701_17")
    private String p70117;
    @Column(name = "P701_18")
    private String p70118;
    @Column(name = "P701_18E")
    private String p70118e;
    @Column(name = "P702")
    private String p702;
    @Column(name = "P702_ESP")
    private String p702Esp;
    @Column(name = "P7031")
    private String p7031;
    @Column(name = "P7032_11")
    private String p703211;
    @Column(name = "P7032_12")
    private String p703212;
    @Column(name = "P7032_21")
    private String p703221;
    @Column(name = "P7032_22")
    private String p703222;
    @Column(name = "P7032_31")
    private String p703231;
    @Column(name = "P7032_32")
    private String p703232;
    @Column(name = "P7032_41")
    private String p703241;
    @Column(name = "P7032_42")
    private String p703242;
    @Column(name = "P7032_51")
    private String p703251;
    @Column(name = "P7032_52")
    private String p703252;
    @Column(name = "P7032_53")
    private String p703253;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "APATERNO")
    private String apaterno;
    @Column(name = "AMATERNO")
    private String amaterno;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    
    /*
    @OneToMany(mappedBy = "local2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2017Sec104> local2017Sec104List;
    @OneToMany(mappedBy = "local2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2017Sec207> local2017Sec207List;
    @OneToMany(mappedBy = "local2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2017Sec300> local2017Sec300List;
    @OneToMany(mappedBy = "local2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2017Sec304> local2017Sec304List;
    @OneToMany(mappedBy = "local2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2017Sec400> local2017Sec400List;
    @OneToMany(mappedBy = "local2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2017Sec500> local2017Sec500List;
    */
    
    //fin
    @Transient
    private long token;
    @Transient
    private String msg;
    @Transient
    private String estadoRpt;
    @Transient
    private String fechasigied;

    public Local2017Cabecera() {
    }

    public Local2017Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name = "NOMBVIA")
    public String getNombvia() {
        return nombvia;
    }

    public void setNombvia(String nombvia) {
        this.nombvia = nombvia;
    }

    @XmlElement(name = "NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name = "MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name = "LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name = "CATEGORIA")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlElement(name = "NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name = "ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name = "SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name = "ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name = "OTROS")
    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    @XmlElement(name = "CATEG_CP")
    public String getCategCp() {
        return categCp;
    }

    public void setCategCp(String categCp) {
        this.categCp = categCp;
    }

    @XmlElement(name = "CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name = "CODCCPP")
    public String getCodccpp() {
        return codccpp;
    }

    public void setCodccpp(String codccpp) {
        this.codccpp = codccpp;
    }

    @XmlElement(name = "REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlElement(name = "DPTO")
    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }

    @XmlElement(name = "PROV")
    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "COD_AREA")
    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    @XmlElement(name = "COND_TEN")
    public String getCondTen() {
        return condTen;
    }

    public void setCondTen(String condTen) {
        this.condTen = condTen;
    }

    @XmlElement(name = "PROPLOCAL")
    public String getProplocal() {
        return proplocal;
    }

    public void setProplocal(String proplocal) {
        this.proplocal = proplocal;
    }

    @XmlElement(name = "GESTION")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name = "FICHA_REG")
    public String getFichaReg() {
        return fichaReg;
    }

    public void setFichaReg(String fichaReg) {
        this.fichaReg = fichaReg;
    }

    @XmlElement(name = "OFICINA_REG")
    public String getOficinaReg() {
        return oficinaReg;
    }

    public void setOficinaReg(String oficinaReg) {
        this.oficinaReg = oficinaReg;
    }

    @XmlElement(name = "ENTIDAD")
    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    @XmlElement(name = "AMATERNO")
    public String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }

    @XmlElement(name = "APATERNO")
    public String getApaterno() {
        return apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    @XmlElement(name = "NOMBRES")
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @XmlElement(name = "TELEFONO")
    public String getTelefono() {
        return telefono;
    }
//inicio

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @XmlElement(name = "P201_AC")
    public Integer getP201Ac() {
        return p201Ac;
    }

    @XmlElement(name = "P201_AT")
    public Integer getP201At() {
        return p201At;
    }

    @XmlElement(name = "P202")
    public String getP202() {
        return p202;
    }

    @XmlElement(name = "P203_EST")
    public String getP203Est() {
        return p203Est;
    }

    @XmlElement(name = "P203_MAT")
    public String getP203Mat() {
        return p203Mat;
    }

    @XmlElement(name = "P204")
    public String getP204() {
        return p204;
    }

    @XmlElement(name = "P205")
    public String getP205() {
        return p205;
    }

    @XmlElement(name = "P205_TP")
    public String getP205Tp() {
        return p205Tp;
    }

    @XmlElement(name = "P206")
    public Integer getP206() {
        return p206;
    }

    @XmlElement(name = "P301_ADM")
    public Integer getP301Adm() {
        return p301Adm;
    }

    @XmlElement(name = "P301_EDU")
    public Integer getP301Edu() {
        return p301Edu;
    }

    @XmlElement(name = "P303_EBA")
    public String getP303Eba() {
        return p303Eba;
    }

    @XmlElement(name = "P303_EBE")
    public String getP303Ebe() {
        return p303Ebe;
    }

    @XmlElement(name = "P303_EOB")
    public String getP303Eob() {
        return p303Eob;
    }

    @XmlElement(name = "P303_ETP")
    public String getP303Etp() {
        return p303Etp;
    }

    @XmlElement(name = "P303_INI")
    public String getP303Ini() {
        return p303Ini;
    }

    @XmlElement(name = "P303_PRI")
    public String getP303Pri() {
        return p303Pri;
    }

    @XmlElement(name = "P303_SEC")
    public String getP303Sec() {
        return p303Sec;
    }

    @XmlElement(name = "P303_SUP")
    public String getP303Sup() {
        return p303Sup;
    }

    @XmlElement(name = "P304")
    public Integer getP304() {
        return p304;
    }

    @XmlElement(name = "P401")
    public String getP401() {
        return p401;
    }

    @XmlElement(name = "P402_1")
    public String getP4021() {
        return p4021;
    }

    @XmlElement(name = "P402_10")
    public String getP40210() {
        return p40210;
    }

    @XmlElement(name = "P402_11")
    public String getP40211() {
        return p40211;
    }

    @XmlElement(name = "P402_12")
    public String getP40212() {
        return p40212;
    }

    @XmlElement(name = "P402_13")
    public String getP40213() {
        return p40213;
    }

    @XmlElement(name = "P402_14")
    public String getP40214() {
        return p40214;
    }

    @XmlElement(name = "P402_2")
    public String getP4022() {
        return p4022;
    }

    @XmlElement(name = "P402_3")
    public String getP4023() {
        return p4023;
    }

    @XmlElement(name = "P402_4")
    public String getP4024() {
        return p4024;
    }

    @XmlElement(name = "P402_5")
    public String getP4025() {
        return p4025;
    }

    @XmlElement(name = "P402_6")
    public String getP4026() {
        return p4026;
    }

    @XmlElement(name = "P402_7")
    public String getP4027() {
        return p4027;
    }

    @XmlElement(name = "P402_8")
    public String getP4028() {
        return p4028;
    }

    @XmlElement(name = "P402_9")
    public String getP4029() {
        return p4029;
    }

    @XmlElement(name = "P403")
    public String getP403() {
        return p403;
    }

    @XmlElement(name = "P404")
    public String getP404() {
        return p404;
    }

    @XmlElement(name = "P406_1")
    public String getP4061() {
        return p4061;
    }

    @XmlElement(name = "P406_2")
    public String getP4062() {
        return p4062;
    }

    @XmlElement(name = "P406_3")
    public String getP4063() {
        return p4063;
    }

    @XmlElement(name = "P406_4")
    public String getP4064() {
        return p4064;
    }

    @XmlElement(name = "P406_5")
    public String getP4065() {
        return p4065;
    }

    @XmlElement(name = "P406_6")
    public String getP4066() {
        return p4066;
    }

    @XmlElement(name = "P406_7")
    public String getP4067() {
        return p4067;
    }

    @XmlElement(name = "P407")
    public String getP407() {
        return p407;
    }

    @XmlElement(name = "P407_Qn")
    public Integer getP407Qn() {
        return p407Qn;
    }

    @XmlElement(name = "P409")
    public String getP409() {
        return p409;
    }

    @XmlElement(name = "P411_1")
    public Integer getP4111() {
        return p4111;
    }

    @XmlElement(name = "P411_2")
    public Integer getP4112() {
        return p4112;
    }

    @XmlElement(name = "P411_3")
    public Integer getP4113() {
        return p4113;
    }

    @XmlElement(name = "P411_4")
    public Integer getP4114() {
        return p4114;
    }

    @XmlElement(name = "P412")
    public Integer getP412() {
        return p412;
    }

    @XmlElement(name = "P413_11")
    public String getP41311() {
        return p41311;
    }

    @XmlElement(name = "P413_12")
    public String getP41312() {
        return p41312;
    }

    @XmlElement(name = "P413_13")
    public Short getP41313() {
        return p41313;
    }

    @XmlElement(name = "P413_14")
    public Short getP41314() {
        return p41314;
    }

    @XmlElement(name = "P413_21")
    public String getP41321() {
        return p41321;
    }

    @XmlElement(name = "P413_22")
    public String getP41322() {
        return p41322;
    }

    @XmlElement(name = "P413_23")
    public Short getP41323() {
        return p41323;
    }

    @XmlElement(name = "P413_24")
    public Short getP41324() {
        return p41324;
    }

    @XmlElement(name = "P413_31")
    public String getP41331() {
        return p41331;
    }

    @XmlElement(name = "P413_32")
    public String getP41332() {
        return p41332;
    }

    @XmlElement(name = "P413_33")
    public Short getP41333() {
        return p41333;
    }

    @XmlElement(name = "P413_34")
    public Short getP41334() {
        return p41334;
    }

    @XmlElement(name = "P414")
    public String getP414() {
        return p414;
    }

    @XmlElement(name = "P414_S")
    public String getP414S() {
        return p414S;
    }

    @XmlElement(name = "P415")
    public String getP415() {
        return p415;
    }

    @XmlElement(name = "P416")
    public String getP416() {
        return p416;
    }

    @XmlElement(name = "P416_E")
    public String getP416E() {
        return p416E;
    }

    @XmlElement(name = "P416_S")
    public String getP416S() {
        return p416S;
    }

    @XmlElement(name = "P417")
    public String getP417() {
        return p417;
    }

    @XmlElement(name = "P417_EST")
    public String getP417Est() {
        return p417Est;
    }

    @XmlElement(name = "P417_USO")
    public String getP417Uso() {
        return p417Uso;
    }

    @XmlElement(name = "P418")
    public String getP418() {
        return p418;
    }

    @XmlElement(name = "P418_EST")
    public String getP418Est() {
        return p418Est;
    }

    @XmlElement(name = "P419")
    public String getP419() {
        return p419;
    }

    @XmlElement(name = "P421")
    public String getP421() {
        return p421;
    }

    @XmlElement(name = "P421_Qn")
    public Integer getP421Qn() {
        return p421Qn;
    }

    @XmlElement(name = "P601_1")
    public String getP6011() {
        return p6011;
    }

    @XmlElement(name = "P601_10")
    public String getP60110() {
        return p60110;
    }

    @XmlElement(name = "P601_11")
    public String getP60111() {
        return p60111;
    }

    @XmlElement(name = "P601_2")
    public String getP6012() {
        return p6012;
    }

    @XmlElement(name = "P601_3")
    public String getP6013() {
        return p6013;
    }

    @XmlElement(name = "P601_4")
    public String getP6014() {
        return p6014;
    }

    @XmlElement(name = "P601_5")
    public String getP6015() {
        return p6015;
    }

    @XmlElement(name = "P601_6")
    public String getP6016() {
        return p6016;
    }

    @XmlElement(name = "P601_7")
    public String getP6017() {
        return p6017;
    }

    @XmlElement(name = "P601_7T")
    public String getP6017t() {
        return p6017t;
    }

    @XmlElement(name = "P601_8")
    public String getP6018() {
        return p6018;
    }

    @XmlElement(name = "P601_9")
    public String getP6019() {
        return p6019;
    }

    @XmlElement(name = "P701")
    public String getP701() {
        return p701;
    }

    @XmlElement(name = "P701_1")
    public String getP7011() {
        return p7011;
    }

    @XmlElement(name = "P701_10")
    public String getP70110() {
        return p70110;
    }

    @XmlElement(name = "P701_11")
    public String getP70111() {
        return p70111;
    }

    @XmlElement(name = "P701_12")
    public String getP70112() {
        return p70112;
    }

    @XmlElement(name = "P701_13")
    public String getP70113() {
        return p70113;
    }

    @XmlElement(name = "P701_14")
    public String getP70114() {
        return p70114;
    }

    @XmlElement(name = "P701_15")
    public String getP70115() {
        return p70115;
    }

    @XmlElement(name = "P701_16")
    public String getP70116() {
        return p70116;
    }

    @XmlElement(name = "P701_17")
    public String getP70117() {
        return p70117;
    }

    @XmlElement(name = "P701_18")
    public String getP70118() {
        return p70118;
    }

    @XmlElement(name = "P701_18E")
    public String getP70118e() {
        return p70118e;
    }

    @XmlElement(name = "P701_2")
    public String getP7012() {
        return p7012;
    }

    @XmlElement(name = "P701_3")
    public String getP7013() {
        return p7013;
    }

    @XmlElement(name = "P701_4")
    public String getP7014() {
        return p7014;
    }

    @XmlElement(name = "P701_5")
    public String getP7015() {
        return p7015;
    }

    @XmlElement(name = "P701_6")
    public String getP7016() {
        return p7016;
    }

    @XmlElement(name = "P701_7")
    public String getP7017() {
        return p7017;
    }

    @XmlElement(name = "P701_8")
    public String getP7018() {
        return p7018;
    }

    @XmlElement(name = "P701_9")
    public String getP7019() {
        return p7019;
    }

    @XmlElement(name = "P702")
    public String getP702() {
        return p702;
    }

    @XmlElement(name = "P702_ESP")
    public String getP702Esp() {
        return p702Esp;
    }

    @XmlElement(name = "P7031")
    public String getP7031() {
        return p7031;
    }

    @XmlElement(name = "P7032_11")
    public String getP703211() {
        return p703211;
    }

    @XmlElement(name = "P7032_12")
    public String getP703212() {
        return p703212;
    }

    @XmlElement(name = "P7032_21")
    public String getP703221() {
        return p703221;
    }

    @XmlElement(name = "P7032_22")
    public String getP703222() {
        return p703222;
    }

    @XmlElement(name = "P7032_31")
    public String getP703231() {
        return p703231;
    }

    @XmlElement(name = "P7032_32")
    public String getP703232() {
        return p703232;
    }

    @XmlElement(name = "P7032_41")
    public String getP703241() {
        return p703241;
    }

    @XmlElement(name = "P7032_42")
    public String getP703242() {
        return p703242;
    }

    @XmlElement(name = "P7032_51")
    public String getP703251() {
        return p703251;
    }

    @XmlElement(name = "P7032_52")
    public String getP703252() {
        return p703252;
    }

    @XmlElement(name = "P7032_53")
    public String getP703253() {
        return p703253;
    }
//inicio
    //inicio

//falta mapear los listados y los setters
    //fin
    //fin
    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    //@XmlElement(name = "ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    //@XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
/*
    @XmlElement(name = "SEC300")
    public List<Local2017Sec300> getLocal2017Sec300List() {
        return local2017Sec300List;
    }

    public void setLocal2017Sec300List(List<Local2017Sec300> local2017Sec300List) {
        this.local2017Sec300List = local2017Sec300List;
    }

    @XmlElement(name = "SEC304")
    public List<Local2017Sec304> getLocal2017Sec304List() {
        return local2017Sec304List;
    }

    public void setLocal2017Sec304List(List<Local2017Sec304> local2017Sec304List) {
        this.local2017Sec304List = local2017Sec304List;
    }

    @XmlElement(name = "SEC400")
    public List<Local2017Sec400> getLocal2017Sec400List() {
        return local2017Sec400List;
    }

    public void setLocal2017Sec400List(List<Local2017Sec400> local2017Sec400List) {
        this.local2017Sec400List = local2017Sec400List;
    }

    @XmlElement(name = "SEC500")
    public List<Local2017Sec500> getLocal2017Sec500List() {
        return local2017Sec500List;
    }

    public void setLocal2017Sec500List(List<Local2017Sec500> local2017Sec500List) {
        this.local2017Sec500List = local2017Sec500List;
    }

    @XmlElement(name = "SEC207")
    public List<Local2017Sec207> getLocal2017Sec207List() {
        return local2017Sec207List;
    }

    public void setLocal2017Sec207List(List<Local2017Sec207> local2017Sec207List) {
        this.local2017Sec207List = local2017Sec207List;
    }

    @XmlElement(name = "SEC104")
    public List<Local2017Sec104> getLocal2017Sec104List() {
        return local2017Sec104List;
    }

    public void setLocal2017Sec104List(List<Local2017Sec104> local2017Sec104List) {
        this.local2017Sec104List = local2017Sec104List;

    }
*/
    @XmlAttribute(name = "ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlAttribute(name = "MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlElement
    public String getFechasigied() {
        return fechasigied;
    }

    public void setFechasigied(String fechasigied) {
        this.fechasigied = fechasigied;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    public void setP201Ac(Integer p201Ac) {
        this.p201Ac = p201Ac;
    }

    public void setP201At(Integer p201At) {
        this.p201At = p201At;
    }

    public void setP202(String p202) {
        this.p202 = p202;
    }

    public void setP203Est(String p203Est) {
        this.p203Est = p203Est;
    }

    public void setP203Mat(String p203Mat) {
        this.p203Mat = p203Mat;
    }

    public void setP204(String p204) {
        this.p204 = p204;
    }

    public void setP205(String p205) {
        this.p205 = p205;
    }

    public void setP205Tp(String p205Tp) {
        this.p205Tp = p205Tp;
    }

    public void setP206(Integer p206) {
        this.p206 = p206;
    }

    public void setP301Adm(Integer p301Adm) {
        this.p301Adm = p301Adm;
    }

    public void setP301Edu(Integer p301Edu) {
        this.p301Edu = p301Edu;
    }

    public void setP303Eba(String p303Eba) {
        this.p303Eba = p303Eba;
    }

    public void setP303Ebe(String p303Ebe) {
        this.p303Ebe = p303Ebe;
    }

    public void setP303Eob(String p303Eob) {
        this.p303Eob = p303Eob;
    }

    public void setP303Etp(String p303Etp) {
        this.p303Etp = p303Etp;
    }

    public void setP303Ini(String p303Ini) {
        this.p303Ini = p303Ini;
    }

    public void setP303Pri(String p303Pri) {
        this.p303Pri = p303Pri;
    }

    public void setP303Sec(String p303Sec) {
        this.p303Sec = p303Sec;
    }

    public void setP303Sup(String p303Sup) {
        this.p303Sup = p303Sup;
    }

    public void setP304(Integer p304) {
        this.p304 = p304;
    }

    public void setP401(String p401) {
        this.p401 = p401;
    }

    public void setP4021(String p4021) {
        this.p4021 = p4021;
    }

    public void setP40210(String p40210) {
        this.p40210 = p40210;
    }

    public void setP40211(String p40211) {
        this.p40211 = p40211;
    }

    public void setP40212(String p40212) {
        this.p40212 = p40212;
    }

    public void setP40213(String p40213) {
        this.p40213 = p40213;
    }

    public void setP40214(String p40214) {
        this.p40214 = p40214;
    }

    public void setP4022(String p4022) {
        this.p4022 = p4022;
    }

    public void setP4023(String p4023) {
        this.p4023 = p4023;
    }

    public void setP4024(String p4024) {
        this.p4024 = p4024;
    }

    public void setP4025(String p4025) {
        this.p4025 = p4025;
    }

    public void setP4026(String p4026) {
        this.p4026 = p4026;
    }

    public void setP4027(String p4027) {
        this.p4027 = p4027;
    }

    public void setP4028(String p4028) {
        this.p4028 = p4028;
    }

    public void setP4029(String p4029) {
        this.p4029 = p4029;
    }

    public void setP403(String p403) {
        this.p403 = p403;
    }

    public void setP404(String p404) {
        this.p404 = p404;
    }

    public void setP4061(String p4061) {
        this.p4061 = p4061;
    }

    public void setP4062(String p4062) {
        this.p4062 = p4062;
    }

    public void setP4063(String p4063) {
        this.p4063 = p4063;
    }

    public void setP4064(String p4064) {
        this.p4064 = p4064;
    }

    public void setP4065(String p4065) {
        this.p4065 = p4065;
    }

    public void setP4066(String p4066) {
        this.p4066 = p4066;
    }

    public void setP4067(String p4067) {
        this.p4067 = p4067;
    }

    public void setP407(String p407) {
        this.p407 = p407;
    }

    public void setP407Qn(Integer p407Qn) {
        this.p407Qn = p407Qn;
    }

    public void setP409(String p409) {
        this.p409 = p409;
    }

    public void setP4111(Integer p4111) {
        this.p4111 = p4111;
    }

    public void setP4112(Integer p4112) {
        this.p4112 = p4112;
    }

    public void setP4113(Integer p4113) {
        this.p4113 = p4113;
    }

    public void setP4114(Integer p4114) {
        this.p4114 = p4114;
    }

    public void setP412(Integer p412) {
        this.p412 = p412;
    }

    public void setP41311(String p41311) {
        this.p41311 = p41311;
    }

    public void setP41312(String p41312) {
        this.p41312 = p41312;
    }

    public void setP41313(Short p41313) {
        this.p41313 = p41313;
    }

    public void setP41314(Short p41314) {
        this.p41314 = p41314;
    }

    public void setP41321(String p41321) {
        this.p41321 = p41321;
    }

    public void setP41322(String p41322) {
        this.p41322 = p41322;
    }

    public void setP41323(Short p41323) {
        this.p41323 = p41323;
    }

    public void setP41324(Short p41324) {
        this.p41324 = p41324;
    }

    public void setP41331(String p41331) {
        this.p41331 = p41331;
    }

    public void setP41332(String p41332) {
        this.p41332 = p41332;
    }

    public void setP41333(Short p41333) {
        this.p41333 = p41333;
    }

    public void setP41334(Short p41334) {
        this.p41334 = p41334;
    }

    public void setP414(String p414) {
        this.p414 = p414;
    }

    public void setP414S(String p414S) {
        this.p414S = p414S;
    }

    public void setP415(String p415) {
        this.p415 = p415;
    }

    public void setP416(String p416) {
        this.p416 = p416;
    }

    public void setP416E(String p416E) {
        this.p416E = p416E;
    }

    public void setP416S(String p416S) {
        this.p416S = p416S;
    }

    public void setP417(String p417) {
        this.p417 = p417;
    }

    public void setP417Est(String p417Est) {
        this.p417Est = p417Est;
    }

    public void setP417Uso(String p417Uso) {
        this.p417Uso = p417Uso;
    }

    public void setP418(String p418) {
        this.p418 = p418;
    }

    public void setP418Est(String p418Est) {
        this.p418Est = p418Est;
    }

    public void setP419(String p419) {
        this.p419 = p419;
    }

    public void setP421(String p421) {
        this.p421 = p421;
    }

    public void setP421Qn(Integer p421Qn) {
        this.p421Qn = p421Qn;
    }

    public void setP6011(String p6011) {
        this.p6011 = p6011;
    }

    public void setP60110(String p60110) {
        this.p60110 = p60110;
    }

    public void setP60111(String p60111) {
        this.p60111 = p60111;
    }

    public void setP6012(String p6012) {
        this.p6012 = p6012;
    }

    public void setP6013(String p6013) {
        this.p6013 = p6013;
    }

    public void setP6014(String p6014) {
        this.p6014 = p6014;
    }

    public void setP6015(String p6015) {
        this.p6015 = p6015;
    }

    public void setP6016(String p6016) {
        this.p6016 = p6016;
    }

    public void setP6017(String p6017) {
        this.p6017 = p6017;
    }

    public void setP6017t(String p6017t) {
        this.p6017t = p6017t;
    }

    public void setP6018(String p6018) {
        this.p6018 = p6018;
    }

    public void setP6019(String p6019) {
        this.p6019 = p6019;
    }

    public void setP701(String p701) {
        this.p701 = p701;
    }

    public void setP7011(String p7011) {
        this.p7011 = p7011;
    }

    public void setP70110(String p70110) {
        this.p70110 = p70110;
    }

    public void setP70111(String p70111) {
        this.p70111 = p70111;
    }

    public void setP70112(String p70112) {
        this.p70112 = p70112;
    }

    public void setP70113(String p70113) {
        this.p70113 = p70113;
    }

    public void setP70114(String p70114) {
        this.p70114 = p70114;
    }

    public void setP70115(String p70115) {
        this.p70115 = p70115;
    }

    public void setP70116(String p70116) {
        this.p70116 = p70116;
    }

    public void setP70117(String p70117) {
        this.p70117 = p70117;
    }

    public void setP70118(String p70118) {
        this.p70118 = p70118;
    }

    public void setP70118e(String p70118e) {
        this.p70118e = p70118e;
    }

    public void setP7012(String p7012) {
        this.p7012 = p7012;
    }

    public void setP7013(String p7013) {
        this.p7013 = p7013;
    }

    public void setP7014(String p7014) {
        this.p7014 = p7014;
    }

    public void setP7015(String p7015) {
        this.p7015 = p7015;
    }

    public void setP7016(String p7016) {
        this.p7016 = p7016;
    }

    public void setP7017(String p7017) {
        this.p7017 = p7017;
    }

    public void setP7018(String p7018) {
        this.p7018 = p7018;
    }

    public void setP7019(String p7019) {
        this.p7019 = p7019;
    }

    public void setP702(String p702) {
        this.p702 = p702;
    }

    public void setP702Esp(String p702Esp) {
        this.p702Esp = p702Esp;
    }

    public void setP7031(String p7031) {
        this.p7031 = p7031;
    }

    public void setP703211(String p703211) {
        this.p703211 = p703211;
    }

    public void setP703212(String p703212) {
        this.p703212 = p703212;
    }

    public void setP703221(String p703221) {
        this.p703221 = p703221;
    }

    public void setP703222(String p703222) {
        this.p703222 = p703222;
    }

    public void setP703231(String p703231) {
        this.p703231 = p703231;
    }

    public void setP703232(String p703232) {
        this.p703232 = p703232;
    }

    public void setP703241(String p703241) {
        this.p703241 = p703241;
    }

    public void setP703242(String p703242) {
        this.p703242 = p703242;
    }

    public void setP703251(String p703251) {
        this.p703251 = p703251;
    }

    public void setP703252(String p703252) {
        this.p703252 = p703252;
    }

    public void setP703253(String p703253) {
        this.p703253 = p703253;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2017Cabecera)) {
            return false;
        }
        Local2017Cabecera other = (Local2017Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Local2017Cabecera[idEnvio=" + idEnvio + "]";
    }
}
