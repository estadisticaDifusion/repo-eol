package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2016_cabecera")
public class Local2016Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Column(name = "NOMBVIA")
    private String nombvia;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "MANZANA")
    private String manzana;
    @Column(name = "LOTE")
    private String lote;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Column(name = "ETAPA")
    private String etapa;
    @Column(name = "SECTOR")
    private String sector;
    @Column(name = "ZONA")
    private String zona;
    @Column(name = "OTROS")
    private String otros;
    @Column(name = "CATEG_CP")
    private String categCp;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "CODCCPP")
    private String codccpp;
    @Column(name = "REFERENCIA")
    private String referencia;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "DPTO")
    private String dpto;
    @Column(name = "PROV")
    private String prov;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "COD_AREA")
    private String codArea;
    @Column(name = "COND_TEN")
    private String condTen;
    @Column(name = "PROPLOCAL")
    private String proplocal;
    @Column(name = "FICHA_REG")
    private String fichaReg;
    @Column(name = "OFICINA_REG")
    private String oficinaReg;
    @Column(name = "GESTION")
    private String gestion;
    @Column(name = "P201")
    private String p201;
    @Column(name = "P202_1")
    private String p2021;
    @Column(name = "P202_2")
    private String p2022;
    @Column(name = "P202_3")
    private String p2023;
    @Column(name = "P202_4")
    private String p2024;
    @Column(name = "P202_5")
    private String p2025;
    @Column(name = "P202_6")
    private String p2026;
    @Column(name = "P202_7")
    private String p2027;
    @Column(name = "P202_8")
    private String p2028;
    @Column(name = "P202_9")
    private String p2029;
    @Column(name = "P202_10")
    private String p20210;
    @Column(name = "P203")
    private String p203;
    @Column(name = "P205")
    private String p205;
    @Column(name = "P209")
    private String p209;
    @Column(name = "P211_1")
    private String p2111;
    @Column(name = "P211_2")
    private String p2112;
    @Column(name = "P211_3")
    private String p2113;
    @Column(name = "P211_4")
    private String p2114;
    @Column(name = "P211_5")
    private String p2115;
    @Column(name = "P211_6")
    private String p2116;
    @Column(name = "P211_7")
    private String p2117;
    @Column(name = "P213")
    private String p213;
    @Column(name = "P214")
    private String p214;
    @Column(name = "P215")
    private String p215;
    @Column(name = "P217")
    private Integer p217;
    @Column(name = "P220_1")
    private Integer p2201;
    @Column(name = "P220_2")
    private Integer p2202;
    @Column(name = "P220_3")
    private Integer p2203;
    @Column(name = "P220_4")
    private Integer p2204;
    @Column(name = "P221")
    private Integer p221;
    @Column(name = "P222")
    private String p222;
    @Column(name = "P222_S")
    private String p222S;
    @Column(name = "P223")
    private String p223;
    @Column(name = "P223_E")
    private String p223E;
    @Column(name = "P223_S")
    private String p223S;
    @Column(name = "P224")
    private String p224;
    @Column(name = "P225")
    private String p225;
    @Column(name = "P227")
    private Integer p227;
    @Column(name = "P228")
    private String p228;
    @Column(name = "P229")
    private String p229;
    @Column(name = "P301_ADM")
    private Integer p301Adm;
    @Column(name = "P301_EDU")
    private Integer p301Edu;
    @Column(name = "P401")
    private Integer p401;
    @Column(name = "P601_1")
    private String p6011;
    @Column(name = "P601_2")
    private String p6012;
    @Column(name = "P601_3")
    private String p6013;
    @Column(name = "P601_4")
    private String p6014;
    @Column(name = "P601_5")
    private String p6015;
    @Column(name = "P601_61")
    private String p60161;
    @Column(name = "P601_62")
    private String p60162;
    @Column(name = "P601_7")
    private String p6017;
    @Column(name = "P601_8")
    private String p6018;
    @Column(name = "P601_9")
    private String p6019;
    @Column(name = "P601_10")
    private String p60110;
    @Column(name = "P7011")
    private String p7011;
    @Column(name = "P7012")
    private String p7012;
    @Column(name = "P7013_1")
    private String p70131;
    @Column(name = "P7013_2")
    private String p70132;
    @Column(name = "P7014")
    private String p7014;
    @Column(name = "P7015")
    private String p7015;
    @Column(name = "P7016_1")
    private String p70161;
    @Column(name = "P7016_2")
    private String p70162;
    @Column(name = "P702")
    private String p702;
    @Column(name = "P702_1")
    private String p7021;
    @Column(name = "P702_2")
    private String p7022;
    @Column(name = "P702_3")
    private String p7023;
    @Column(name = "P702_4")
    private String p7024;
    @Column(name = "P702_5")
    private String p7025;
    @Column(name = "P702_6")
    private String p7026;
    @Column(name = "P702_7")
    private String p7027;
    @Column(name = "P702_8")
    private String p7028;
    @Column(name = "P702_9")
    private String p7029;
    @Column(name = "P702_10")
    private String p70210;
    @Column(name = "P702_11")
    private String p70211;
    @Column(name = "P702_12")
    private String p70212;
    @Column(name = "P702_13")
    private String p70213;
    @Column(name = "P702_14")
    private String p70214;
    @Column(name = "P702_15")
    private String p70215;
    @Column(name = "P702_16")
    private String p70216;
    @Column(name = "P702_17")
    private String p70217;
    @Column(name = "P702_18")
    private String p70218;
    @Column(name = "P702_18E")
    private String p70218e;
    @Column(name = "P703")
    private String p703;
    @Column(name = "P703_ESP")
    private String p703Esp;
    @Column(name = "P7041")
    private String p7041;
    @Column(name = "P7042_11")
    private String p704211;
    @Column(name = "P7042_12")
    private String p704212;
    @Column(name = "P7042_13")
    private String p704213;
    @Column(name = "P7042_21")
    private String p704221;
    @Column(name = "P7042_22")
    private String p704222;
    @Column(name = "P7042_23")
    private String p704223;
    @Column(name = "P7042_31")
    private String p704231;
    @Column(name = "P7042_32")
    private String p704232;
    @Column(name = "P7042_33")
    private String p704233;
    @Column(name = "P7042_41")
    private String p704241;
    @Column(name = "P7042_42")
    private String p704242;
    @Column(name = "P7042_43")
    private String p704243;
    @Column(name = "P7042_51")
    private String p704251;
    @Column(name = "P7042_52")
    private String p704252;
    @Column(name = "P7042_53")
    private String p704253;
    @Column(name = "P7042_54")
    private String p704254;
    @Column(name = "P7051")
    private String p7051;
    @Column(name = "P7052")
    private String p7052;
    @Column(name = "P7052_1")
    private String p70521;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    
    /*
    @OneToMany(mappedBy = "local2016Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2016Sec300> local2016Sec300List;
    @OneToMany(mappedBy = "local2016Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2016Sec304> local2016Sec304List;
    @OneToMany(mappedBy = "local2016Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2016Sec402> local2016Sec402List;
    @OneToMany(mappedBy = "local2016Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2016Sec500> local2016Sec500List;
    @OneToMany(mappedBy = "local2016Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2016Sec200> local2016Sec200List;
    @OneToMany(mappedBy = "local2016Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2016Sec104> local2016Sec104List;
*/
    @Transient
    private long token;
    @Transient
    private String msg;
    @Transient
    private String estadoRpt;

    public Local2016Cabecera() {
    }

    public Local2016Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name = "NOMBVIA")
    public String getNombvia() {
        return nombvia;
    }

    public void setNombvia(String nombvia) {
        this.nombvia = nombvia;
    }

    @XmlElement(name = "NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name = "MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name = "LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name = "CATEGORIA")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlElement(name = "NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name = "ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name = "SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name = "ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name = "OTROS")
    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    @XmlElement(name = "CATEG_CP")
    public String getCategCp() {
        return categCp;
    }

    public void setCategCp(String categCp) {
        this.categCp = categCp;
    }

    @XmlElement(name = "CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name = "CODCCPP")
    public String getCodccpp() {
        return codccpp;
    }

    public void setCodccpp(String codccpp) {
        this.codccpp = codccpp;
    }

    @XmlElement(name = "REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    
    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlElement(name = "DPTO")
    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }

    @XmlElement(name = "PROV")
    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "COD_AREA")
    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    @XmlElement(name = "COND_TEN")
    public String getCondTen() {
        return condTen;
    }

    public void setCondTen(String condTen) {
        this.condTen = condTen;
    }

    @XmlElement(name = "PROPLOCAL")
    public String getProplocal() {
        return proplocal;
    }

    public void setProplocal(String proplocal) {
        this.proplocal = proplocal;
    }

    @XmlElement(name = "GESTION")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }
    @XmlElement(name = "FICHA_REG")
    public String getFichaReg() {
        return fichaReg;
    }

    public void setFichaReg(String fichaReg) {
        this.fichaReg = fichaReg;
    }
    @XmlElement(name = "OFICINA_REG")
    public String getOficinaReg() {
        return oficinaReg;
    }

    public void setOficinaReg(String oficinaReg) {
        this.oficinaReg = oficinaReg;
    }


    

    @XmlElement(name = "P201")
    public String getP201() {
        return p201;
    }

    public void setP201(String p201) {
        this.p201 = p201;
    }

    @XmlElement(name = "P202_1")
    public String getP2021() {
        return p2021;
    }

    public void setP2021(String p2021) {
        this.p2021 = p2021;
    }

    @XmlElement(name = "P202_2")
    public String getP2022() {
        return p2022;
    }

    public void setP2022(String p2022) {
        this.p2022 = p2022;
    }

    @XmlElement(name = "P202_3")
    public String getP2023() {
        return p2023;
    }

    public void setP2023(String p2023) {
        this.p2023 = p2023;
    }

    @XmlElement(name = "P202_4")
    public String getP2024() {
        return p2024;
    }

    public void setP2024(String p2024) {
        this.p2024 = p2024;
    }

    @XmlElement(name = "P202_5")
    public String getP2025() {
        return p2025;
    }

    public void setP2025(String p2025) {
        this.p2025 = p2025;
    }

    @XmlElement(name = "P202_6")
    public String getP2026() {
        return p2026;
    }

    public void setP2026(String p2026) {
        this.p2026 = p2026;
    }

    @XmlElement(name = "P202_7")
    public String getP2027() {
        return p2027;
    }

    public void setP2027(String p2027) {
        this.p2027 = p2027;
    }

    @XmlElement(name = "P202_8")
    public String getP2028() {
        return p2028;
    }

    public void setP2028(String p2028) {
        this.p2028 = p2028;
    }

    @XmlElement(name = "P202_9")
    public String getP2029() {
        return p2029;
    }

    public void setP2029(String p2029) {
        this.p2029 = p2029;
    }

    @XmlElement(name = "P202_10")
    public String getP20210() {
        return p20210;
    }

    public void setP20210(String p20210) {
        this.p20210 = p20210;
    }

    @XmlElement(name = "P203")
    public String getP203() {
        return p203;
    }

    public void setP203(String p203) {
        this.p203 = p203;
    }
    @XmlElement(name = "P205")
    public String getP205() {
        return p205;
    }

    public void setP205(String p205) {
        this.p205 = p205;
    }
    @XmlElement(name = "P209")
    public String getP209() {
        return p209;
    }

    public void setP209(String p209) {
        this.p209 = p209;
    }
    @XmlElement(name = "P211_1")
    public String getP2111() {
        return p2111;
    }

    public void setP2111(String p2111) {
        this.p2111 = p2111;
    }
    @XmlElement(name = "P211_2")
    public String getP2112() {
        return p2112;
    }

    public void setP2112(String p2112) {
        this.p2112 = p2112;
    }
    @XmlElement(name = "P211_3")
    public String getP2113() {
        return p2113;
    }

    public void setP2113(String p2113) {
        this.p2113 = p2113;
    }
    @XmlElement(name = "P211_4")
    public String getP2114() {
        return p2114;
    }

    public void setP2114(String p2114) {
        this.p2114 = p2114;
    }
    @XmlElement(name = "P211_5")
    public String getP2115() {
        return p2115;
    }

    public void setP2115(String p2115) {
        this.p2115 = p2115;
    }
    @XmlElement(name = "P211_6")
    public String getP2116() {
        return p2116;
    }

    public void setP2116(String p2116) {
        this.p2116 = p2116;
    }
    @XmlElement(name = "P211_7")
    public String getP2117() {
        return p2117;
    }

    public void setP2117(String p2117) {
        this.p2117 = p2117;
    }
    @XmlElement(name = "P213")
    public String getP213() {
        return p213;
    }

    public void setP213(String p213) {
        this.p213 = p213;
    }

    @XmlElement(name = "P214")
    public String getP214() {
        return p214;
    }

    public void setP214(String p214) {
        this.p214 = p214;
    }
    @XmlElement(name = "P215")
    public String getP215() {
        return p215;
    }

    public void setP215(String p215) {
        this.p215 = p215;
    }
    @XmlElement(name = "P217")
    public Integer getP217() {
        return p217;
    }

    public void setP217(Integer p217) {
        this.p217 = p217;
    }
    @XmlElement(name = "P220_1")
    public Integer getP2201() {
        return p2201;
    }

    public void setP2201(Integer p2201) {
        this.p2201 = p2201;
    }
    @XmlElement(name = "P220_2")
    public Integer getP2202() {
        return p2202;
    }

    public void setP2202(Integer p2202) {
        this.p2202 = p2202;
    }
    @XmlElement(name = "P220_3")
    public Integer getP2203() {
        return p2203;
    }

    public void setP2203(Integer p2203) {
        this.p2203 = p2203;
    }
    @XmlElement(name = "P220_4")
    public Integer getP2204() {
        return p2204;
    }

    public void setP2204(Integer p2204) {
        this.p2204 = p2204;
    }
    @XmlElement(name = "P221")
    public Integer getP221() {
        return p221;
    }

    public void setP221(Integer p221) {
        this.p221 = p221;
    }
    @XmlElement(name = "P222")
    public String getP222() {
        return p222;
    }

    public void setP222(String p222) {
        this.p222 = p222;
    }
    @XmlElement(name = "P222_S")
    public String getP222S() {
        return p222S;
    }

    public void setP222S(String p222S) {
        this.p222S = p222S;
    }
    @XmlElement(name = "P223")
    public String getP223() {
        return p223;
    }

    public void setP223(String p223) {
        this.p223 = p223;
    }
    @XmlElement(name = "P223_E")
    public String getP223E() {
        return p223E;
    }

    public void setP223E(String p223E) {
        this.p223E = p223E;
    }
    @XmlElement(name = "P223_S")
    public String getP223S() {
        return p223S;
    }

    public void setP223S(String p223S) {
        this.p223S = p223S;
    }
    @XmlElement(name = "P224")
    public String getP224() {
        return p224;
    }

    public void setP224(String p224) {
        this.p224 = p224;
    }
    @XmlElement(name = "P225")
    public String getP225() {
        return p225;
    }

    public void setP225(String p225) {
        this.p225 = p225;
    }
    @XmlElement(name = "P227")
    public Integer getP227() {
        return p227;
    }

    public void setP227(Integer p227) {
        this.p227 = p227;
    }
    @XmlElement(name = "P228")
    public String getP228() {
        return p228;
    }

    public void setP228(String p228) {
        this.p228 = p228;
    }
    @XmlElement(name = "P229")
    public String getP229() {
        return p229;
    }

    public void setP229(String p229) {
        this.p229 = p229;
    }
    @XmlElement(name = "P301_ADM")
    public Integer getP301Adm() {
        return p301Adm;
    }

    public void setP301Adm(Integer p301Adm) {
        this.p301Adm = p301Adm;
    }

    @XmlElement(name = "P301_EDU")
    public Integer getP301Edu() {
        return p301Edu;
    }

    public void setP301Edu(Integer p301Edu) {
        this.p301Edu = p301Edu;
    }

    @XmlElement(name = "P401")
    public Integer getP401() {
        return p401;
    }

    public void setP401(Integer p401) {
        this.p401 = p401;
    }

    @XmlElement(name = "P601_1")
    public String getP6011() {
        return p6011;
    }

    public void setP6011(String p6011) {
        this.p6011 = p6011;
    }

    @XmlElement(name = "P601_2")
    public String getP6012() {
        return p6012;
    }

    public void setP6012(String p6012) {
        this.p6012 = p6012;
    }

    @XmlElement(name = "P601_3")
    public String getP6013() {
        return p6013;
    }

    public void setP6013(String p6013) {
        this.p6013 = p6013;
    }

    @XmlElement(name = "P601_4")
    public String getP6014() {
        return p6014;
    }

    public void setP6014(String p6014) {
        this.p6014 = p6014;
    }

    @XmlElement(name = "P601_5")
    public String getP6015() {
        return p6015;
    }

    public void setP6015(String p6015) {
        this.p6015 = p6015;
    }

    @XmlElement(name = "P601_61")
    public String getP60161() {
        return p60161;
    }

    public void setP60161(String p60161) {
        this.p60161 = p60161;
    }

    @XmlElement(name = "P601_62")
    public String getP60162() {
        return p60162;
    }

    public void setP60162(String p60162) {
        this.p60162 = p60162;
    }

    @XmlElement(name = "P601_7")
    public String getP6017() {
        return p6017;
    }

    public void setP6017(String p6017) {
        this.p6017 = p6017;
    }

    @XmlElement(name = "P601_8")
    public String getP6018() {
        return p6018;
    }

    public void setP6018(String p6018) {
        this.p6018 = p6018;
    }

    @XmlElement(name = "P601_9")
    public String getP6019() {
        return p6019;
    }

    public void setP6019(String p6019) {
        this.p6019 = p6019;
    }

    @XmlElement(name = "P601_10")
    public String getP60110() {
        return p60110;
    }

    public void setP60110(String p60110) {
        this.p60110 = p60110;
    }

    @XmlElement(name = "P7011")
    public String getP7011() {
        return p7011;
    }

    public void setP7011(String p7011) {
        this.p7011 = p7011;
    }

    @XmlElement(name = "P7012")
    public String getP7012() {
        return p7012;
    }

    public void setP7012(String p7012) {
        this.p7012 = p7012;
    }

    @XmlElement(name = "P7013_1")
    public String getP70131() {
        return p70131;
    }

    public void setP70131(String p70131) {
        this.p70131 = p70131;
    }

    @XmlElement(name = "P7013_2")
    public String getP70132() {
        return p70132;
    }

    public void setP70132(String p70132) {
        this.p70132 = p70132;
    }

    @XmlElement(name = "P7014")
    public String getP7014() {
        return p7014;
    }

    public void setP7014(String p7014) {
        this.p7014 = p7014;
    }

    @XmlElement(name = "P7015")
    public String getP7015() {
        return p7015;
    }

    public void setP7015(String p7015) {
        this.p7015 = p7015;
    }

    @XmlElement(name = "P7016_1")
    public String getP70161() {
        return p70161;
    }

    public void setP70161(String p70161) {
        this.p70161 = p70161;
    }

    @XmlElement(name = "P7016_2")
    public String getP70162() {
        return p70162;
    }

    public void setP70162(String p70162) {
        this.p70162 = p70162;
    }

    @XmlElement(name = "P702")
    public String getP702() {
        return p702;
    }

    public void setP702(String p702) {
        this.p702 = p702;
    }

    @XmlElement(name = "P702_1")
    public String getP7021() {
        return p7021;
    }

    public void setP7021(String p7021) {
        this.p7021 = p7021;
    }

    @XmlElement(name = "P702_2")
    public String getP7022() {
        return p7022;
    }

    public void setP7022(String p7022) {
        this.p7022 = p7022;
    }

    @XmlElement(name = "P702_3")
    public String getP7023() {
        return p7023;
    }

    public void setP7023(String p7023) {
        this.p7023 = p7023;
    }

    @XmlElement(name = "P702_4")
    public String getP7024() {
        return p7024;
    }

    public void setP7024(String p7024) {
        this.p7024 = p7024;
    }

    @XmlElement(name = "P702_5")
    public String getP7025() {
        return p7025;
    }

    public void setP7025(String p7025) {
        this.p7025 = p7025;
    }

    @XmlElement(name = "P702_6")
    public String getP7026() {
        return p7026;
    }

    public void setP7026(String p7026) {
        this.p7026 = p7026;
    }

    @XmlElement(name = "P702_7")
    public String getP7027() {
        return p7027;
    }

    public void setP7027(String p7027) {
        this.p7027 = p7027;
    }

    @XmlElement(name = "P702_8")
    public String getP7028() {
        return p7028;
    }

    public void setP7028(String p7028) {
        this.p7028 = p7028;
    }

    @XmlElement(name = "P702_9")
    public String getP7029() {
        return p7029;
    }

    public void setP7029(String p7029) {
        this.p7029 = p7029;
    }

    @XmlElement(name = "P702_10")
    public String getP70210() {
        return p70210;
    }

    public void setP70210(String p70210) {
        this.p70210 = p70210;
    }

    @XmlElement(name = "P702_11")
    public String getP70211() {
        return p70211;
    }

    public void setP70211(String p70211) {
        this.p70211 = p70211;
    }

    @XmlElement(name = "P702_12")
    public String getP70212() {
        return p70212;
    }

    public void setP70212(String p70212) {
        this.p70212 = p70212;
    }

    @XmlElement(name = "P702_13")
    public String getP70213() {
        return p70213;
    }

    public void setP70213(String p70213) {
        this.p70213 = p70213;
    }

    @XmlElement(name = "P702_14")
    public String getP70214() {
        return p70214;
    }

    public void setP70214(String p70214) {
        this.p70214 = p70214;
    }

    @XmlElement(name = "P702_15")
    public String getP70215() {
        return p70215;
    }

    public void setP70215(String p70215) {
        this.p70215 = p70215;
    }

    @XmlElement(name = "P702_16")
    public String getP70216() {
        return p70216;
    }

    public void setP70216(String p70216) {
        this.p70216 = p70216;
    }

    @XmlElement(name = "P702_17")
    public String getP70217() {
        return p70217;
    }

    public void setP70217(String p70217) {
        this.p70217 = p70217;
    }

    @XmlElement(name = "P702_18")
    public String getP70218() {
        return p70218;
    }

    public void setP70218(String p70218) {
        this.p70218 = p70218;
    }

    @XmlElement(name = "P702_18E")
    public String getP70218e() {
        return p70218e;
    }

    public void setP70218e(String p70218e) {
        this.p70218e = p70218e;
    }

    @XmlElement(name = "P703")
    public String getP703() {
        return p703;
    }

    public void setP703(String p703) {
        this.p703 = p703;
    }

    @XmlElement(name = "P703_ESP")
    public String getP703Esp() {
        return p703Esp;
    }

    public void setP703Esp(String p703Esp) {
        this.p703Esp = p703Esp;
    }

    @XmlElement(name = "P7041")
    public String getP7041() {
        return p7041;
    }

    public void setP7041(String p7041) {
        this.p7041 = p7041;
    }

    @XmlElement(name = "P7042_11")
    public String getP704211() {
        return p704211;
    }

    public void setP704211(String p704211) {
        this.p704211 = p704211;
    }

    @XmlElement(name = "P7042_12")
    public String getP704212() {
        return p704212;
    }

    public void setP704212(String p704212) {
        this.p704212 = p704212;
    }

    @XmlElement(name = "P7042_13")
    public String getP704213() {
        return p704213;
    }

    public void setP704213(String p704213) {
        this.p704213 = p704213;
    }


    @XmlElement(name = "P7042_21")
    public String getP704221() {
        return p704221;
    }

    public void setP704221(String p704221) {
        this.p704221 = p704221;
    }

    @XmlElement(name = "P7042_22")
    public String getP704222() {
        return p704222;
    }

    public void setP704222(String p704222) {
        this.p704222 = p704222;
    }

    @XmlElement(name = "P7042_23")
    public String getP704223() {
        return p704223;
    }

    public void setP704223(String p704223) {
        this.p704223 = p704223;
    }
    @XmlElement(name = "P7042_31")
    public String getP704231() {
        return p704231;
    }

    public void setP704231(String p704231) {
        this.p704231 = p704231;
    }

    @XmlElement(name = "P7042_32")
    public String getP704232() {
        return p704232;
    }

    public void setP704232(String p704232) {
        this.p704232 = p704232;
    }

    @XmlElement(name = "P7042_33")
    public String getP704233() {
        return p704233;
    }

    public void setP704233(String p704233) {
        this.p704233 = p704233;
    }
    @XmlElement(name = "P7042_41")
    public String getP704241() {
        return p704241;
    }

    public void setP704241(String p704241) {
        this.p704241 = p704241;
    }

    @XmlElement(name = "P7042_42")
    public String getP704242() {
        return p704242;
    }

    public void setP704242(String p704242) {
        this.p704242 = p704242;
    }

    @XmlElement(name = "P7042_43")
    public String getP704243() {
        return p704243;
    }

    public void setP704243(String p704243) {
        this.p704243 = p704243;
    }
    @XmlElement(name = "P7042_51")
    public String getP704251() {
        return p704251;
    }

    public void setP704251(String p704251) {
        this.p704251 = p704251;
    }

    @XmlElement(name = "P7042_52")
    public String getP704252() {
        return p704252;
    }

    public void setP704252(String p704252) {
        this.p704252 = p704252;
    }

    @XmlElement(name = "P7042_53")
    public String getP704253() {
        return p704253;
    }

    public void setP704253(String p704253) {
        this.p704253 = p704253;
    }

    @XmlElement(name = "P7042_54")
    public String getP704254() {
        return p704254;
    }

    public void setP704254(String p704254) {
        this.p704254 = p704254;
    }

    @XmlElement(name = "P7051")
    public String getP7051() {
        return p7051;
    }

    public void setP7051(String p7051) {
        this.p7051 = p7051;
    }

    @XmlElement(name = "P7052")
    public String getP7052() {
        return p7052;
    }

    public void setP7052(String p7052) {
        this.p7052 = p7052;
    }

    @XmlElement(name = "P7052_1")
    public String getP70521() {
        return p70521;
    }

    public void setP70521(String p70521) {
        this.p70521 = p70521;
    }

    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    //@XmlElement(name = "ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    //@XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
    
    /*
    @XmlElement(name = "SEC300")
    public List<Local2016Sec300> getLocal2016Sec300List() {
        return local2016Sec300List;
    }

    public void setLocal2016Sec300List(List<Local2016Sec300> local2016Sec300List) {
        this.local2016Sec300List = local2016Sec300List;
    }
    @XmlElement(name = "SEC304")
    public List<Local2016Sec304> getLocal2016Sec304List() {
        return local2016Sec304List;
    }

    public void setLocal2016Sec304List(List<Local2016Sec304> local2016Sec304List) {
        this.local2016Sec304List = local2016Sec304List;
    }
    @XmlElement(name = "SEC402")
    public List<Local2016Sec402> getLocal2016Sec402List() {
        return local2016Sec402List;
    }

    public void setLocal2016Sec402List(List<Local2016Sec402> local2016Sec402List) {
        this.local2016Sec402List = local2016Sec402List;
    }

    @XmlElement(name = "SEC500")
    public List<Local2016Sec500> getLocal2016Sec500List() {
        return local2016Sec500List;
    }

    public void setLocal2016Sec500List(List<Local2016Sec500> local2016Sec500List) {
        this.local2016Sec500List = local2016Sec500List;
    }
    @XmlElement(name = "SEC200")
    public List<Local2016Sec200> getLocal2016Sec200List() {
        return local2016Sec200List;
    }

    public void setLocal2016Sec200List(List<Local2016Sec200> local2016Sec200List) {
        this.local2016Sec200List = local2016Sec200List;
    }
    @XmlElement(name = "SEC104")
    public List<Local2016Sec104> getLocal2016Sec104List() {
        return local2016Sec104List;
    }

    public void setLocal2016Sec104List(List<Local2016Sec104> local2016Sec104List) {
        this.local2016Sec104List = local2016Sec104List;

    }*/

    @XmlAttribute(name = "ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlAttribute(name = "MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2016Cabecera)) {
            return false;
        }
        Local2016Cabecera other = (Local2016Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Local2016Cabecera[idEnvio=" + idEnvio + "]";
    }

}
