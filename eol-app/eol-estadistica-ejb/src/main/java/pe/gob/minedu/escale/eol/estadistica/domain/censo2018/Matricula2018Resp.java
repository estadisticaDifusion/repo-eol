/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "matricula2018_resp")
public class Matricula2018Resp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "NROCED")
    private String nroced;
    @Column(name = "ORDEN")
    private String orden;
    @Column(name = "NOM_APE")
    private String nomApe;
    @Column(name = "CARGO")
    private String cargo;
    @Column(name = "CORREO")
    private String correo;
    @Column(name = "TELEFONO")
    private String telefono;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2018Cabecera matricula2018Cabecera;

    public Matricula2018Resp() {
    }

    public Matricula2018Resp(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2018Resp(Long idEnvio, String nroced, String orden) {
        this.idEnvio = idEnvio;
        this.nroced = nroced;
        this.orden = orden;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "ORDEN")
    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    @XmlElement(name = "NOM_APE")
    public String getNomApe() {
        return nomApe;
    }

    public void setNomApe(String nomApe) {
        this.nomApe = nomApe;
    }

    @XmlElement(name = "CARGO")
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @XmlElement(name = "CORREO")
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @XmlElement(name = "TELEFONO")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @XmlTransient
    public Matricula2018Cabecera getMatricula2018Cabecera() {
        return matricula2018Cabecera;
    }

    public void setMatricula2018Cabecera(Matricula2018Cabecera matricula2018Cabecera) {
        this.matricula2018Cabecera = matricula2018Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2018Resp)) {
            return false;
        }
        Matricula2018Resp other = (Matricula2018Resp) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Resp[idEnvio=" + idEnvio + "]";
    }

}
