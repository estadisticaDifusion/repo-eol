/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "resultado2016_detalle")
public class Resultado2016Detalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Basic(optional = false)
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
    
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Resultado2016Cabecera cedula;
  /*  
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detalle", fetch = FetchType.EAGER)
    private List<Resultado2016Fila> filas;
*/
    public Resultado2016Detalle() {
    }

    public Resultado2016Detalle(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Resultado2016Detalle(Long idEnvio, String cuadro) {
        this.idEnvio = idEnvio;
        this.cuadro = cuadro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Resultado2016Cabecera getCedula() {
        return cedula;
    }

    public void setCedula(Resultado2016Cabecera cedula) {
        this.cedula = cedula;
    }
/*
    @XmlElement(name="FILAS")
    public List<Resultado2016Fila> getFilas() {
        return filas;
    }

    public void setFilas(List<Resultado2016Fila> filas) {
        this.filas = filas;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado2016Detalle)) {
            return false;
        }
        Resultado2016Detalle other = (Resultado2016Detalle) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Resultado2016Detalle[idEnvio=" + idEnvio + "]";
    }

}
