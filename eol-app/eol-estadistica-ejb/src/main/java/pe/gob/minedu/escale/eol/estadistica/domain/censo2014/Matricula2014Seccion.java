/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2014_seccion")
public class Matricula2014Seccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
/*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2014Seccion")
    private List<Matricula2014SeccionFila> matricula2014SeccionFilaList;
*/

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2014Cabecera matricula2014Cabecera;

    public Matricula2014Seccion() {
    }

    public Matricula2014Seccion(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }
/*

    @XmlElement(name="SECCION_FILAS")
    public List<Matricula2014SeccionFila> getMatricula2014SeccionFilaList() {
        return matricula2014SeccionFilaList;
    }

    public void setMatricula2014SeccionFilaList(List<Matricula2014SeccionFila> matricula2014SeccionFilaList) {
        this.matricula2014SeccionFilaList = matricula2014SeccionFilaList;
    }*/

    @XmlTransient
    public Matricula2014Cabecera getMatricula2014Cabecera() {
        return matricula2014Cabecera;
    }

    public void setMatricula2014Cabecera(Matricula2014Cabecera matricula2014Cabecera) {
        this.matricula2014Cabecera = matricula2014Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2014Seccion)) {
            return false;
        }
        Matricula2014Seccion other = (Matricula2014Seccion) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    

}
