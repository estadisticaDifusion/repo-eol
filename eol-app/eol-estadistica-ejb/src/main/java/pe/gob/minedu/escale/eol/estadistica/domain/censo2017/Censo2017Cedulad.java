/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author warodriguez
 */

@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2017_cedulad")
public class Censo2017Cedulad implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;
	
	@Column(name = "NROCED")
	private String nroced;
	
	@Column(name = "COD_MOD")
	private String codMod;
	
	@Column(name = "CODLOCAL")
	private String codlocal;
	
	@Column(name = "CODUGEL")
	private String codugel;
	@Column(name = "P_01SW")
	private String p01sw;
	@Column(name = "P_02AINI")
	private String p02aini;
	@Column(name = "P_02BPRI")
	private String p02bpri;
	@Column(name = "P_02CSEC")
	private String p02csec;
	@Column(name = "P_03AINI_D")
	private String p03ainiD;
	@Column(name = "P_03AINI_M")
	private String p03ainiM;
	@Column(name = "P_03BPRI_D")
	private String p03bpriD;
	@Column(name = "P_03BPRI_M")
	private String p03bpriM;
	@Column(name = "P_03CSEC_D")
	private String p03csecD;
	@Column(name = "P_03CSEC_M")
	private String p03csecM;
	@Column(name = "P_04AINI")
	private String p04aini;
	@Column(name = "P_04BPRI")
	private String p04bpri;
	@Column(name = "P_04CSEC")
	private String p04csec;
	@Column(name = "P_05AUGEL")
	private String p05augel;
	@Column(name = "P_05BDRE")
	private String p05bdre;
	@Column(name = "P_05CMUN")
	private String p05cmun;
	@Column(name = "P_05DOTR")
	private String p05dotr;
	@Column(name = "P_05DOTR_E")
	private String p05dotrE;
	@Column(name = "P_05ENO")
	private String p05eno;
	@Column(name = "P_06")
	private String p06;
	@Column(name = "P_07AINI")
	private String p07aini;
	@Column(name = "P_07BPRI")
	private String p07bpri;
	@Column(name = "P_07CSEC")
	private String p07csec;
	@Column(name = "P_08A11")
	private String p08a11;
	@Column(name = "P_08A12")
	private String p08a12;
	@Column(name = "P_08A13")
	private String p08a13;
	@Column(name = "P_08B21")
	private String p08b21;
	@Column(name = "P_08B22")
	private String p08b22;
	@Column(name = "P_08B23")
	private String p08b23;
	@Column(name = "P_08C31")
	private String p08c31;
	@Column(name = "P_08C32")
	private String p08c32;
	@Column(name = "P_08C33")
	private String p08c33;
	@Column(name = "P_08D41")
	private String p08d41;
	@Column(name = "P_08D42")
	private String p08d42;
	@Column(name = "P_08D43")
	private String p08d43;
	@Column(name = "P_08E51")
	private String p08e51;
	@Column(name = "P_08E52")
	private String p08e52;
	@Column(name = "P_08E53")
	private String p08e53;
	@Column(name = "P_09A11")
	private String p09a11;
	@Column(name = "P_09A12")
	private String p09a12;
	@Column(name = "P_09A13")
	private String p09a13;
	@Column(name = "P_09B21")
	private String p09b21;
	@Column(name = "P_09B22")
	private String p09b22;
	@Column(name = "P_09B23")
	private String p09b23;
	@Column(name = "P_09C31")
	private String p09c31;
	@Column(name = "P_09C32")
	private String p09c32;
	@Column(name = "P_09C33")
	private String p09c33;
	@Column(name = "P_09D41")
	private String p09d41;
	@Column(name = "P_09D42")
	private String p09d42;
	@Column(name = "P_09D43")
	private String p09d43;
	@Column(name = "P_09E51")
	private String p09e51;
	@Column(name = "P_09E52")
	private String p09e52;
	@Column(name = "P_09E53")
	private String p09e53;
	@Column(name = "P_09F61")
	private String p09f61;
	@Column(name = "P_09F62")
	private String p09f62;
	@Column(name = "P_09F63")
	private String p09f63;
	@Column(name = "P_10AINI")
	private Integer p10aini;
	@Column(name = "P_10BPRI")
	private Integer p10bpri;
	@Column(name = "P_10CSEC")
	private Integer p10csec;
	@Column(name = "P_11AINI")
	private Integer p11aini;
	@Column(name = "P_11BPRI")
	private Integer p11bpri;
	@Column(name = "P_11CSEC")
	private Integer p11csec;
	@Column(name = "P_12AINI")
	private Integer p12aini;
	@Column(name = "P_12BPRI")
	private Integer p12bpri;
	@Column(name = "P_12CSEC")
	private Integer p12csec;
	@Column(name = "P_13")
	private String p13;
	@Column(name = "P_13_ESP")
	private String p13Esp;
	@Column(name = "P_14")
	private String p14;
	@Column(name = "P_15AINI")
	private String p15aini;
	@Column(name = "P_15BPRI")
	private String p15bpri;
	@Column(name = "P_15CSEC")
	private String p15csec;
	@Column(name = "P_16AIE")
	private String p16aie;
	@Column(name = "P_16BUGEL")
	private String p16bugel;
	@Column(name = "P_16CDRE")
	private String p16cdre;
	@Column(name = "P_16DMUN")
	private String p16dmun;
	@Column(name = "APATERNO")
	private String apaterno;
	@Column(name = "AMATERNO")
	private String amaterno;
	@Column(name = "NOMBRES")
	private String nombres;
	@Column(name = "FUENTE")
	private String fuente;
	@Column(name = "FECHA_ENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;
	
	@Column(name = "ULTIMO")
	private Boolean ultimo;
	
	@Column(name = "VERSION")
	private String version;
	
	@Column(name = "SITUACION")
	private String situacion;

	@Transient
	private String ESTADO_RPT;

	@Transient
	private String msg;

	@Transient
	private long token;

	@Transient
	private String anexo;

	public Censo2017Cedulad() {
	}

	public Censo2017Cedulad(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Censo2017Cedulad(Long idEnvio, String nroced, String codlocal, Date fechaEnvio) {
		this.idEnvio = idEnvio;
		this.nroced = nroced;
		this.codlocal = codlocal;
		this.fechaEnvio = fechaEnvio;
	}

	@XmlAttribute
	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "NROCED")
	public String getNroced() {
		return nroced;
	}

	public void setNroced(String nroced) {
		this.nroced = nroced;
	}

	@XmlElement(name = "CODLOCAL")
	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	@XmlElement(name = "COD_MOD")
	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	@XmlElement(name = "CODUGEL")
	public String getCodugel() {
		return codugel;
	}

	public void setCodugel(String codugel) {
		this.codugel = codugel;
	}

	@XmlElement(name = "P_01SW")
	public String getP01sw() {
		return p01sw;
	}

	public void setP01sw(String p01sw) {
		this.p01sw = p01sw;
	}

	@XmlElement(name = "P_02AINI")
	public String getP02aini() {
		return p02aini;
	}

	public void setP02aini(String p02aini) {
		this.p02aini = p02aini;
	}

	@XmlElement(name = "P_02BPRI")
	public String getP02bpri() {
		return p02bpri;
	}

	public void setP02bpri(String p02bpri) {
		this.p02bpri = p02bpri;
	}

	@XmlElement(name = "P_02CSEC")
	public String getP02csec() {
		return p02csec;
	}

	public void setP02csec(String p02csec) {
		this.p02csec = p02csec;
	}

	@XmlElement(name = "P_03AINI_D")
	public String getP03ainiD() {
		return p03ainiD;
	}

	public void setP03ainiD(String p03ainiD) {
		this.p03ainiD = p03ainiD;
	}

	@XmlElement(name = "P_03AINI_M")
	public String getP03ainiM() {
		return p03ainiM;
	}

	public void setP03ainiM(String p03ainiM) {
		this.p03ainiM = p03ainiM;
	}

	@XmlElement(name = "P_03BPRI_D")
	public String getP03bpriD() {
		return p03bpriD;
	}

	public void setP03bpriD(String p03bpriD) {
		this.p03bpriD = p03bpriD;
	}

	@XmlElement(name = "P_03BPRI_M")
	public String getP03bpriM() {
		return p03bpriM;
	}

	public void setP03bpriM(String p03bpriM) {
		this.p03bpriM = p03bpriM;
	}

	@XmlElement(name = "P_03CSEC_D")
	public String getP03csecD() {
		return p03csecD;
	}

	public void setP03csecD(String p03csecD) {
		this.p03csecD = p03csecD;
	}

	@XmlElement(name = "P_03CSEC_M")
	public String getP03csecM() {
		return p03csecM;
	}

	public void setP03csecM(String p03csecM) {
		this.p03csecM = p03csecM;
	}

	@XmlElement(name = "P_04AINI")
	public String getP04aini() {
		return p04aini;
	}

	public void setP04aini(String p04aini) {
		this.p04aini = p04aini;
	}

	@XmlElement(name = "P_04BPRI")
	public String getP04bpri() {
		return p04bpri;
	}

	public void setP04bpri(String p04bpri) {
		this.p04bpri = p04bpri;
	}

	@XmlElement(name = "P_04CSEC")
	public String getP04csec() {
		return p04csec;
	}

	public void setP04csec(String p04csec) {
		this.p04csec = p04csec;
	}

	@XmlElement(name = "P_05AUGEL")
	public String getP05augel() {
		return p05augel;
	}

	public void setP05augel(String p05augel) {
		this.p05augel = p05augel;
	}

	@XmlElement(name = "P_05BDRE")
	public String getP05bdre() {
		return p05bdre;
	}

	public void setP05bdre(String p05bdre) {
		this.p05bdre = p05bdre;
	}

	@XmlElement(name = "P_05CMUN")
	public String getP05cmun() {
		return p05cmun;
	}

	public void setP05cmun(String p05cmun) {
		this.p05cmun = p05cmun;
	}

	@XmlElement(name = "P_05DOTR")
	public String getP05dotr() {
		return p05dotr;
	}

	public void setP05dotr(String p05dotr) {
		this.p05dotr = p05dotr;
	}

	@XmlElement(name = "P_05DOTR_E")
	public String getP05dotrE() {
		return p05dotrE;
	}

	public void setP05dotrE(String p05dotrE) {
		this.p05dotrE = p05dotrE;
	}

	@XmlElement(name = "P_05ENO")
	public String getP05eno() {
		return p05eno;
	}

	public void setP05eno(String p05eno) {
		this.p05eno = p05eno;
	}

	@XmlElement(name = "P_06")
	public String getP06() {
		return p06;
	}

	public void setP06(String p06) {
		this.p06 = p06;
	}

	@XmlElement(name = "P_07AINI")
	public String getP07aini() {
		return p07aini;
	}

	public void setP07aini(String p07aini) {
		this.p07aini = p07aini;
	}

	@XmlElement(name = "P_07BPRI")
	public String getP07bpri() {
		return p07bpri;
	}

	public void setP07bpri(String p07bpri) {
		this.p07bpri = p07bpri;
	}

	@XmlElement(name = "P_07CSEC")
	public String getP07csec() {
		return p07csec;
	}

	public void setP07csec(String p07csec) {
		this.p07csec = p07csec;
	}

	@XmlElement(name = "P_08A11")
	public String getP08a11() {
		return p08a11;
	}

	public void setP08a11(String p08a11) {
		this.p08a11 = p08a11;
	}

	@XmlElement(name = "P_08A12")
	public String getP08a12() {
		return p08a12;
	}

	public void setP08a12(String p08a12) {
		this.p08a12 = p08a12;
	}

	@XmlElement(name = "P_08A13")
	public String getP08a13() {
		return p08a13;
	}

	public void setP08a13(String p08a13) {
		this.p08a13 = p08a13;
	}

	@XmlElement(name = "P_08B21")
	public String getP08b21() {
		return p08b21;
	}

	public void setP08b21(String p08b21) {
		this.p08b21 = p08b21;
	}

	@XmlElement(name = "P_08B22")
	public String getP08b22() {
		return p08b22;
	}

	public void setP08b22(String p08b22) {
		this.p08b22 = p08b22;
	}

	@XmlElement(name = "P_08B23")
	public String getP08b23() {
		return p08b23;
	}

	public void setP08b23(String p08b23) {
		this.p08b23 = p08b23;
	}

	@XmlElement(name = "P_08C31")
	public String getP08c31() {
		return p08c31;
	}

	public void setP08c31(String p08c31) {
		this.p08c31 = p08c31;
	}

	@XmlElement(name = "P_08C32")
	public String getP08c32() {
		return p08c32;
	}

	public void setP08c32(String p08c32) {
		this.p08c32 = p08c32;
	}

	@XmlElement(name = "P_08C33")
	public String getP08c33() {
		return p08c33;
	}

	public void setP08c33(String p08c33) {
		this.p08c33 = p08c33;
	}

	@XmlElement(name = "P_08D41")
	public String getP08d41() {
		return p08d41;
	}

	public void setP08d41(String p08d41) {
		this.p08d41 = p08d41;
	}

	@XmlElement(name = "P_08D42")
	public String getP08d42() {
		return p08d42;
	}

	public void setP08d42(String p08d42) {
		this.p08d42 = p08d42;
	}

	@XmlElement(name = "P_08D43")
	public String getP08d43() {
		return p08d43;
	}

	public void setP08d43(String p08d43) {
		this.p08d43 = p08d43;
	}

	@XmlElement(name = "P_08E51")
	public String getP08e51() {
		return p08e51;
	}

	public void setP08e51(String p08e51) {
		this.p08e51 = p08e51;
	}

	@XmlElement(name = "P_08E52")
	public String getP08e52() {
		return p08e52;
	}

	public void setP08e52(String p08e52) {
		this.p08e52 = p08e52;
	}

	@XmlElement(name = "P_08E53")
	public String getP08e53() {
		return p08e53;
	}

	public void setP08e53(String p08e53) {
		this.p08e53 = p08e53;
	}

	@XmlElement(name = "P_09A11")
	public String getP09a11() {
		return p09a11;
	}

	public void setP09a11(String p09a11) {
		this.p09a11 = p09a11;
	}

	@XmlElement(name = "P_09A12")
	public String getP09a12() {
		return p09a12;
	}

	public void setP09a12(String p09a12) {
		this.p09a12 = p09a12;
	}

	@XmlElement(name = "P_09A13")
	public String getP09a13() {
		return p09a13;
	}

	public void setP09a13(String p09a13) {
		this.p09a13 = p09a13;
	}

	@XmlElement(name = "P_09B21")
	public String getP09b21() {
		return p09b21;
	}

	public void setP09b21(String p09b21) {
		this.p09b21 = p09b21;
	}

	@XmlElement(name = "P_09B22")
	public String getP09b22() {
		return p09b22;
	}

	public void setP09b22(String p09b22) {
		this.p09b22 = p09b22;
	}

	@XmlElement(name = "P_09B23")
	public String getP09b23() {
		return p09b23;
	}

	public void setP09b23(String p09b23) {
		this.p09b23 = p09b23;
	}

	@XmlElement(name = "P_09C31")
	public String getP09c31() {
		return p09c31;
	}

	public void setP09c31(String p09c31) {
		this.p09c31 = p09c31;
	}

	@XmlElement(name = "P_09C32")
	public String getP09c32() {
		return p09c32;
	}

	public void setP09c32(String p09c32) {
		this.p09c32 = p09c32;
	}

	@XmlElement(name = "P_09C33")
	public String getP09c33() {
		return p09c33;
	}

	public void setP09c33(String p09c33) {
		this.p09c33 = p09c33;
	}

	@XmlElement(name = "P_09D41")
	public String getP09d41() {
		return p09d41;
	}

	public void setP09d41(String p09d41) {
		this.p09d41 = p09d41;
	}

	@XmlElement(name = "P_09D42")
	public String getP09d42() {
		return p09d42;
	}

	public void setP09d42(String p09d42) {
		this.p09d42 = p09d42;
	}

	@XmlElement(name = "P_09D43")
	public String getP09d43() {
		return p09d43;
	}

	public void setP09d43(String p09d43) {
		this.p09d43 = p09d43;
	}

	@XmlElement(name = "P_09E51")
	public String getP09e51() {
		return p09e51;
	}

	public void setP09e51(String p09e51) {
		this.p09e51 = p09e51;
	}

	@XmlElement(name = "P_09E52")
	public String getP09e52() {
		return p09e52;
	}

	public void setP09e52(String p09e52) {
		this.p09e52 = p09e52;
	}

	@XmlElement(name = "P_09E53")
	public String getP09e53() {
		return p09e53;
	}

	public void setP09e53(String p09e53) {
		this.p09e53 = p09e53;
	}

	@XmlElement(name = "P_09F61")
	public String getP09f61() {
		return p09f61;
	}

	public void setP09f61(String p09f61) {
		this.p09f61 = p09f61;
	}

	@XmlElement(name = "P_09F62")
	public String getP09f62() {
		return p09f62;
	}

	public void setP09f62(String p09f62) {
		this.p09f62 = p09f62;
	}

	@XmlElement(name = "P_09F63")
	public String getP09f63() {
		return p09f63;
	}

	public void setP09f63(String p09f63) {
		this.p09f63 = p09f63;
	}

	@XmlElement(name = "P_10AINI")
	public Integer getP10aini() {
		return p10aini;
	}

	public void setP10aini(Integer p10aini) {
		this.p10aini = p10aini;
	}

	@XmlElement(name = "P_10BPRI")
	public Integer getP10bpri() {
		return p10bpri;
	}

	public void setP10bpri(Integer p10bpri) {
		this.p10bpri = p10bpri;
	}

	@XmlElement(name = "P_10CSEC")
	public Integer getP10csec() {
		return p10csec;
	}

	public void setP10csec(Integer p10csec) {
		this.p10csec = p10csec;
	}

	@XmlElement(name = "P_11AINI")
	public Integer getP11aini() {
		return p11aini;
	}

	public void setP11aini(Integer p11aini) {
		this.p11aini = p11aini;
	}

	@XmlElement(name = "P_11BPRI")
	public Integer getP11bpri() {
		return p11bpri;
	}

	public void setP11bpri(Integer p11bpri) {
		this.p11bpri = p11bpri;
	}

	@XmlElement(name = "P_11CSEC")
	public Integer getP11csec() {
		return p11csec;
	}

	public void setP11csec(Integer p11csec) {
		this.p11csec = p11csec;
	}

	@XmlElement(name = "P_12AINI")
	public Integer getP12aini() {
		return p12aini;
	}

	public void setP12aini(Integer p12aini) {
		this.p12aini = p12aini;
	}

	@XmlElement(name = "P_12BPRI")
	public Integer getP12bpri() {
		return p12bpri;
	}

	public void setP12bpri(Integer p12bpri) {
		this.p12bpri = p12bpri;
	}

	@XmlElement(name = "P_12CSEC")
	public Integer getP12csec() {
		return p12csec;
	}

	public void setP12csec(Integer p12csec) {
		this.p12csec = p12csec;
	}

	@XmlElement(name = "P_13")
	public String getP13() {
		return p13;
	}

	public void setP13(String p13) {
		this.p13 = p13;
	}

	@XmlElement(name = "P_13_ESP")
	public String getP13Esp() {
		return p13Esp;
	}

	public void setP13Esp(String p13Esp) {
		this.p13Esp = p13Esp;
	}

	@XmlElement(name = "P_14")
	public String getP14() {
		return p14;
	}

	public void setP14(String p14) {
		this.p14 = p14;
	}

	@XmlElement(name = "P_15AINI")
	public String getP15aini() {
		return p15aini;
	}

	public void setP15aini(String p15aini) {
		this.p15aini = p15aini;
	}

	@XmlElement(name = "P_15BPRI")
	public String getP15bpri() {
		return p15bpri;
	}

	public void setP15bpri(String p15bpri) {
		this.p15bpri = p15bpri;
	}

	@XmlElement(name = "P_15CSEC")
	public String getP15csec() {
		return p15csec;
	}

	public void setP15csec(String p15csec) {
		this.p15csec = p15csec;
	}

	@XmlElement(name = "P_16AIE")
	public String getP16aie() {
		return p16aie;
	}

	public void setP16aie(String p16aie) {
		this.p16aie = p16aie;
	}

	@XmlElement(name = "P_16BUGEL")
	public String getP16bugel() {
		return p16bugel;
	}

	public void setP16bugel(String p16bugel) {
		this.p16bugel = p16bugel;
	}

	@XmlElement(name = "P_16CDRE")
	public String getP16cdre() {
		return p16cdre;
	}

	public void setP16cdre(String p16cdre) {
		this.p16cdre = p16cdre;
	}

	@XmlElement(name = "P_16DMUN")
	public String getP16dmun() {
		return p16dmun;
	}

	public void setP16dmun(String p16dmun) {
		this.p16dmun = p16dmun;
	}

	@XmlElement(name = "APATERNO")
	public String getApaterno() {
		return apaterno;
	}

	public void setApaterno(String apaterno) {
		this.apaterno = apaterno;
	}

	@XmlElement(name = "AMATERNO")
	public String getAmaterno() {
		return amaterno;
	}

	public void setAmaterno(String amaterno) {
		this.amaterno = amaterno;
	}

	@XmlElement(name = "NOMBRES")
	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	@XmlElement(name = "FUENTE")
	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	@XmlElement(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	@XmlElement(name = "ULTIMO")
	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	@XmlElement(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name = "SITUACION")
	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	@XmlAttribute(name = "ESTADO_RPT")
	public String getESTADO_RPT() {
		return ESTADO_RPT;
	}

	public void setESTADO_RPT(String ESTADO_RPT) {
		this.ESTADO_RPT = ESTADO_RPT;
	}

	@XmlAttribute(name = "MSG")
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@XmlAttribute
	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	@XmlAttribute
	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Censo2017Cedulad)) {
			return false;
		}
		Censo2017Cedulad other = (Censo2017Cedulad) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Censo2017Cedulad{" + "idEnvio=" + idEnvio + "nroced=" + nroced + "codMod=" + codMod + "codlocal="
				+ codlocal + "codugel=" + codugel + "p01sw=" + p01sw + "p02aini=" + p02aini + "p02bpri=" + p02bpri
				+ "p02csec=" + p02csec + "p03ainiD=" + p03ainiD + "p03ainiM=" + p03ainiM + "p03bpriD=" + p03bpriD
				+ "p03bpriM=" + p03bpriM + "p03csecD=" + p03csecD + "p03csecM=" + p03csecM + "p04aini=" + p04aini
				+ "p04bpri=" + p04bpri + "p04csec=" + p04csec + "p05augel=" + p05augel + "p05bdre=" + p05bdre
				+ "p05cmun=" + p05cmun + "p05dotr=" + p05dotr + "p05dotrE=" + p05dotrE + "p05eno=" + p05eno + "p06="
				+ p06 + "p07aini=" + p07aini + "p07bpri=" + p07bpri + "p07csec=" + p07csec + "p08a11=" + p08a11
				+ "p08a12=" + p08a12 + "p08a13=" + p08a13 + "p08b21=" + p08b21 + "p08b22=" + p08b22 + "p08b23=" + p08b23
				+ "p08c31=" + p08c31 + "p08c32=" + p08c32 + "p08c33=" + p08c33 + "p08d41=" + p08d41 + "p08d42=" + p08d42
				+ "p08d43=" + p08d43 + "p08e51=" + p08e51 + "p08e52=" + p08e52 + "p08e53=" + p08e53 + "p09a11=" + p09a11
				+ "p09a12=" + p09a12 + "p09a13=" + p09a13 + "p09b21=" + p09b21 + "p09b22=" + p09b22 + "p09b23=" + p09b23
				+ "p09c31=" + p09c31 + "p09c32=" + p09c32 + "p09c33=" + p09c33 + "p09d41=" + p09d41 + "p09d42=" + p09d42
				+ "p09d43=" + p09d43 + "p09e51=" + p09e51 + "p09e52=" + p09e52 + "p09e53=" + p09e53 + "p09f61=" + p09f61
				+ "p09f62=" + p09f62 + "p09f63=" + p09f63 + "p10aini=" + p10aini + "p10bpri=" + p10bpri + "p10csec="
				+ p10csec + "p11aini=" + p11aini + "p11bpri=" + p11bpri + "p11csec=" + p11csec + "p12aini=" + p12aini
				+ "p12bpri=" + p12bpri + "p12csec=" + p12csec + "p13=" + p13 + "p13Esp=" + p13Esp + "p14=" + p14
				+ "p15aini=" + p15aini + "p15bpri=" + p15bpri + "p15csec=" + p15csec + "p16aie=" + p16aie + "p16bugel="
				+ p16bugel + "p16cdre=" + p16cdre + "p16dmun=" + p16dmun + "apaterno=" + apaterno + "amaterno="
				+ amaterno + "nombres=" + nombres + "fuente=" + fuente + "fechaEnvio=" + fechaEnvio + "ultimo=" + ultimo
				+ "version=" + version + "situacion=" + situacion + "ESTADO_RPT=" + ESTADO_RPT + "msg=" + msg + "token="
				+ token + "anexo=" + anexo + '}';
	}

}
