/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JBEDRILLANA
 */
@XmlRootElement(name = "cedulaResultado2016")
@Entity
@Table(name = "resultado2016_cabecera")
public class Resultado2016Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public final static String CEDULA_01B = "c01b";
    public final static String CEDULA_02B = "c02b";
    public final static String CEDULA_03B = "c03b";
    public final static String CEDULA_04B = "c04b";
    public final static String CEDULA_05B = "c05b";
    public final static String CEDULA_06B = "c06b";
    public final static String CEDULA_07B = "c07b";
    public final static String CEDULA_08B = "c08b";
    public final static String CEDULA_09B = "c09b";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Transient
    private long token;
    @Basic(optional = false)
    @Column(name = "NROCED")
    private String nroced;
    @Basic(optional = false)
    @Column(name = "COD_MOD")
    private String codMod;
    @Basic(optional = false)
    @Column(name = "ANEXO")
    private String anexo;
    @Column(name = "TIPOREG")
    private String tiporeg;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPPROG")
    private String tipprog;
    @Column(name = "CEN_EDU")
    private String cenEdu;
    @Column(name = "NIV_MOD")
    private String nivMod;
    @Column(name = "CODOOII")
    private String codooii;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "FORMATEN")
    private String formaten;
    @Column(name = "TIPONEE")
    private String tiponee;
    @Column(name = "TIPOISE")
    private String tipoise;
    @Column(name = "DNI_CORD")
    private String dniCord;
    @Column(name = "MET_ENSE")
    private String metEnse;
    @Column(name = "MET_ESPE")
    private String metEspe;
    @Column(name = "CEBA_INI")
    private String cebaIni;
    @Column(name = "CEBA_INT")
    private String cebaInt;
    @Column(name = "PERF_INI")
    private String perfIni;
    @Column(name = "PERF_INT")
    private String perfInt;
    @Column(name = "CEBA_PR")
    private String cebaPr;
    @Column(name = "CEBA_SP")
    private String cebaSp;
    @Column(name = "PERF_PR")
    private String perfPr;
    @Column(name = "PERF_SP")
    private String perfSp;
    @Column(name = "APE_DIR")
    private String apeDir;
    @Column(name = "NOM_DIR")
    private String nomDir;
    @Column(name = "ENZ_LNEX")
    private String enzLnex;
    @Column(name = "LENG_EXT")
    private String lengExt;
    @Basic(optional = false)
    @Column(name = "SPA_N1")
    private int spaN1;
    @Basic(optional = false)
    @Column(name = "SPA_A1")
    private float spaA1;
    @Basic(optional = false)
    @Column(name = "SPA_N2")
    private int spaN2;
    @Basic(optional = false)
    @Column(name = "SPA_A2")
    private float spaA2;
    @Basic(optional = false)
    @Column(name = "SPA_N3")
    private int spaN3;
    @Basic(optional = false)
    @Column(name = "SPA_A3")
    private float spaA3;
    @Column(name = "FTE_AGUA")
    private String fteAgua;
    @Column(name = "SUM_AGUA")
    private String sumAgua;
    @Column(name = "ESP_AGUA")
    private String espAgua;
    @Column(name = "GRADO_0")
    private String grado0;
    @Column(name = "GRADO_1")
    private String grado1;
    @Column(name = "GRADO_2")
    private String grado2;
    @Column(name = "GRADO_3")
    private String grado3;
    @Column(name = "GRADO_4")
    private String grado4;
    @Column(name = "GRADO_5")
    private String grado5;
    @Column(name = "GRADO_6")
    private String grado6;
    @Column(name = "PERF_XLU")
    private String perfXlu;
    @Column(name = "PERF_NLU")
    private Integer perfNlu;
    @Column(name = "PERF_XMA")
    private String perfXma;
    @Column(name = "PERF_NMA")
    private Integer perfNma;
    @Column(name = "PERF_XMI")
    private String perfXmi;
    @Column(name = "PERF_NMI")
    private Integer perfNmi;
    @Column(name = "PERF_XJU")
    private String perfXju;
    @Column(name = "PERF_NJU")
    private Integer perfNju;
    @Column(name = "PERF_XVI")
    private String perfXvi;
    @Column(name = "PERF_NVI")
    private Integer perfNvi;
    @Column(name = "PERF_XSA")
    private String perfXsa;
    @Column(name = "PERF_NSA")
    private Integer perfNsa;
    @Column(name = "PERF_XDO")
    private String perfXdo;
    @Column(name = "PERF_NDO")
    private Integer perfNdo;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Basic(optional = false)
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "SITUACION")
    private String situacion;
    @Column(name = "CONFIRMADO")
    private String confirmado;
    @Column(name = "CONTROL")
    private String control;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "VALIDO")
    private Boolean valido;
/*
    @MapKeyColumn(name = "CUADRO", length = 5)
    @OneToMany(mappedBy = "cedula", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String,Resultado2016Detalle> detalle;
*/
    @Transient
    private String msg;

    @Transient
    private String estadoRpt;

    public Resultado2016Cabecera() {
    }

    public Resultado2016Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Resultado2016Cabecera(Long idEnvio, String nroced, String codMod, String anexo, Date fechaEnvio) {
        this.idEnvio = idEnvio;
        this.nroced = nroced;
        this.codMod = codMod;
        this.anexo = anexo;
        this.fechaEnvio = fechaEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name = "ANEXO")
    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    @XmlElement(name = "TIPOREG")
    public String getTiporeg() {
        return tiporeg;
    }

    public void setTiporeg(String tiporeg) {
        this.tiporeg = tiporeg;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPPROG")
    public String getTipprog() {
        return tipprog;
    }

    public void setTipprog(String tipprog) {
        this.tipprog = tipprog;
    }

    @XmlElement(name = "CEN_EDU")
    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    @XmlElement(name = "NIV_MOD")
    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    @XmlElement(name = "CODOOII")
    public String getCodooii() {
        return codooii;
    }

    public void setCodooii(String codooii) {
        this.codooii = codooii;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "FORMATEN")
    public String getFormaten() {
        return formaten;
    }

    public void setFormaten(String formaten) {
        this.formaten = formaten;
    }

    @XmlElement(name = "TIPONEE")
    public String getTiponee() {
        return tiponee;
    }

    public void setTiponee(String tiponee) {
        this.tiponee = tiponee;
    }

    @XmlElement(name = "TIPOISE")
    public String getTipoise() {
        return tipoise;
    }

    public void setTipoise(String tipoise) {
        this.tipoise = tipoise;
    }

    @XmlElement(name = "DNI_CORD")
    public String getDniCord() {
        return dniCord;
    }

    public void setDniCord(String dniCord) {
        this.dniCord = dniCord;
    }

    @XmlElement(name = "MET_ENSE")
    public String getMetEnse() {
        return metEnse;
    }

    public void setMetEnse(String metEnse) {
        this.metEnse = metEnse;
    }

    @XmlElement(name = "MET_ESPE")
    public String getMetEspe() {
        return metEspe;
    }

    public void setMetEspe(String metEspe) {
        this.metEspe = metEspe;
    }

    @XmlElement(name = "CEBA_INI")
    public String getCebaIni() {
        return cebaIni;
    }

    public void setCebaIni(String cebaIni) {
        this.cebaIni = cebaIni;
    }

    @XmlElement(name = "CEBA_INT")
    public String getCebaInt() {
        return cebaInt;
    }

    public void setCebaInt(String cebaInt) {
        this.cebaInt = cebaInt;
    }

    @XmlElement(name = "PERF_INI")
    public String getPerfIni() {
        return perfIni;
    }

    public void setPerfIni(String perfIni) {
        this.perfIni = perfIni;
    }

    @XmlElement(name = "PERF_INT")
    public String getPerfInt() {
        return perfInt;
    }

    public void setPerfInt(String perfInt) {
        this.perfInt = perfInt;
    }

    @XmlElement(name = "CEBA_PR")
    public String getCebaPr() {
        return cebaPr;
    }

    public void setCebaPr(String cebaPr) {
        this.cebaPr = cebaPr;
    }

    @XmlElement(name = "CEBA_SP")
    public String getCebaSp() {
        return cebaSp;
    }

    public void setCebaSp(String cebaSp) {
        this.cebaSp = cebaSp;
    }

    @XmlElement(name = "PERF_PR")
    public String getPerfPr() {
        return perfPr;
    }

    public void setPerfPr(String perfPr) {
        this.perfPr = perfPr;
    }

    @XmlElement(name = "PERF_SP")
    public String getPerfSp() {
        return perfSp;
    }

    public void setPerfSp(String perfSp) {
        this.perfSp = perfSp;
    }

    @XmlElement(name = "APE_DIR")
    public String getApeDir() {
        return apeDir;
    }

    public void setApeDir(String apeDir) {
        this.apeDir = apeDir;
    }

    @XmlElement(name = "NOM_DIR")
    public String getNomDir() {
        return nomDir;
    }

    public void setNomDir(String nomDir) {
        this.nomDir = nomDir;
    }

    @XmlElement(name = "ENZ_LNEX")
    public String getEnzLnex() {
        return enzLnex;
    }

    public void setEnzLnex(String enzLnex) {
        this.enzLnex = enzLnex;
    }

    @XmlElement(name = "LENG_EXT")
    public String getLengExt() {
        return lengExt;
    }

    public void setLengExt(String lengExt) {
        this.lengExt = lengExt;
    }

    @XmlElement(name = "SPA_N1")
    public int getSpaN1() {
        return spaN1;
    }

    public void setSpaN1(int spaN1) {
        this.spaN1 = spaN1;
    }

    @XmlElement(name = "SPA_A1")
    public float getSpaA1() {
        return spaA1;
    }

    public void setSpaA1(float spaA1) {
        this.spaA1 = spaA1;
    }

    @XmlElement(name = "SPA_N2")
    public int getSpaN2() {
        return spaN2;
    }

    public void setSpaN2(int spaN2) {
        this.spaN2 = spaN2;
    }

    @XmlElement(name = "SPA_A2")
    public float getSpaA2() {
        return spaA2;
    }

    public void setSpaA2(float spaA2) {
        this.spaA2 = spaA2;
    }

    @XmlElement(name = "SPA_N3")
    public int getSpaN3() {
        return spaN3;
    }

    public void setSpaN3(int spaN3) {
        this.spaN3 = spaN3;
    }

    @XmlElement(name = "SPA_A3")
    public float getSpaA3() {
        return spaA3;
    }

    public void setSpaA3(float spaA3) {
        this.spaA3 = spaA3;
    }

    @XmlElement(name = "FTE_AGUA")
    public String getFteAgua() {
        return fteAgua;
    }

    public void setFteAgua(String fteAgua) {
        this.fteAgua = fteAgua;
    }

    @XmlElement(name = "SUM_AGUA")
    public String getSumAgua() {
        return sumAgua;
    }

    public void setSumAgua(String sumAgua) {
        this.sumAgua = sumAgua;
    }

    @XmlElement(name = "ESP_AGUA")
    public String getEspAgua() {
        return espAgua;
    }

    public void setEspAgua(String espAgua) {
        this.espAgua = espAgua;
    }

    @XmlElement(name = "GRADO_0")
    public String getGrado0() {
        return grado0;
    }

    public void setGrado0(String grado0) {
        this.grado0 = grado0;
    }

    @XmlElement(name = "GRADO_1")
    public String getGrado1() {
        return grado1;
    }

    public void setGrado1(String grado1) {
        this.grado1 = grado1;
    }

    @XmlElement(name = "GRADO_2")
    public String getGrado2() {
        return grado2;
    }

    public void setGrado2(String grado2) {
        this.grado2 = grado2;
    }

    @XmlElement(name = "GRADO_3")
    public String getGrado3() {
        return grado3;
    }

    public void setGrado3(String grado3) {
        this.grado3 = grado3;
    }

    @XmlElement(name = "GRADO_4")
    public String getGrado4() {
        return grado4;
    }

    public void setGrado4(String grado4) {
        this.grado4 = grado4;
    }

    @XmlElement(name = "GRADO_5")
    public String getGrado5() {
        return grado5;
    }

    public void setGrado5(String grado5) {
        this.grado5 = grado5;
    }

    @XmlElement(name = "GRADO_6")
    public String getGrado6() {
        return grado6;
    }

    public void setGrado6(String grado6) {
        this.grado6 = grado6;
    }

    @XmlElement(name = "PERF_XLU")
    public String getPerfXlu() {
        return perfXlu;
    }

    public void setPerfXlu(String perfXlu) {
        this.perfXlu = perfXlu;
    }

    @XmlElement(name = "PERF_NLU")
    public Integer getPerfNlu() {
        return perfNlu;
    }

    public void setPerfNlu(Integer perfNlu) {
        this.perfNlu = perfNlu;
    }

    @XmlElement(name = "PERF_XMA")
    public String getPerfXma() {
        return perfXma;
    }

    public void setPerfXma(String perfXma) {
        this.perfXma = perfXma;
    }

    @XmlElement(name = "PERF_NMA")
    public Integer getPerfNma() {
        return perfNma;
    }

    public void setPerfNma(Integer perfNma) {
        this.perfNma = perfNma;
    }

    @XmlElement(name = "PERF_XMI")
    public String getPerfXmi() {
        return perfXmi;
    }

    public void setPerfXmi(String perfXmi) {
        this.perfXmi = perfXmi;
    }

    @XmlElement(name = "PERF_NMI")
    public Integer getPerfNmi() {
        return perfNmi;
    }

    public void setPerfNmi(Integer perfNmi) {
        this.perfNmi = perfNmi;
    }

    @XmlElement(name = "PERF_XJU")
    public String getPerfXju() {
        return perfXju;
    }

    public void setPerfXju(String perfXju) {
        this.perfXju = perfXju;
    }

    @XmlElement(name = "PERF_NJU")
    public Integer getPerfNju() {
        return perfNju;
    }

    public void setPerfNju(Integer perfNju) {
        this.perfNju = perfNju;
    }

    @XmlElement(name = "PERF_XVI")
    public String getPerfXvi() {
        return perfXvi;
    }

    public void setPerfXvi(String perfXvi) {
        this.perfXvi = perfXvi;
    }

    @XmlElement(name = "PERF_NVI")
    public Integer getPerfNvi() {
        return perfNvi;
    }

    public void setPerfNvi(Integer perfNvi) {
        this.perfNvi = perfNvi;
    }

    @XmlElement(name = "PERF_XSA")
    public String getPerfXsa() {
        return perfXsa;
    }

    public void setPerfXsa(String perfXsa) {
        this.perfXsa = perfXsa;
    }

    @XmlElement(name = "PERF_NSA")
    public Integer getPerfNsa() {
        return perfNsa;
    }

    public void setPerfNsa(Integer perfNsa) {
        this.perfNsa = perfNsa;
    }

    @XmlElement(name = "PERF_XDO")
    public String getPerfXdo() {
        return perfXdo;
    }

    public void setPerfXdo(String perfXdo) {
        this.perfXdo = perfXdo;
    }

    @XmlElement(name = "PERF_NDO")
    public Integer getPerfNdo() {
        return perfNdo;
    }

    public void setPerfNdo(Integer perfNdo) {
        this.perfNdo = perfNdo;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    @XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @XmlElement(name = "SITUACION")
    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    @XmlElement(name = "CONFIRMADO")
    public String getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(String confirmado) {
        this.confirmado = confirmado;
    }

    @XmlElement(name = "CONTROL")
    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    @XmlElement(name = "ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    @XmlElement(name = "VALIDO")
    public Boolean getValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }

/*
    @XmlJavaTypeAdapter(Resultado2016DetalleMapAdapter.class)
    @XmlElement(name = "DETALLE")
    public Map<String, Resultado2016Detalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(Map<String, Resultado2016Detalle> detalle) {
        this.detalle = detalle;
    }*/

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado2016Cabecera)) {
            return false;
        }
        Resultado2016Cabecera other = (Resultado2016Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Resultado2016Cabecera[idEnvio=" + idEnvio + "]";
    }

}
