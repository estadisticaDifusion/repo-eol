/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.ejb.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(schema = "padron", name = "ugels")
public class Ugel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_ugel")
	private String idUgel;

	@Column(name = "ugel")
	private String nombreUgel;

	@Column(name = "point_x")
	private double pointX;

	@Column(name = "point_y")
	private double pointY;

	@Column(name = "zoom")
	private int zoom;

	@ManyToOne
	@JoinColumn(referencedColumnName = "id_dre", name = "id_dre")
	private DireccionRegional direccionRegional;

	public Ugel() {
	}

	public String getIdUgel() {
		return idUgel;
	}

	public void setIdUgel(String idUgel) {
		this.idUgel = idUgel;
	}

	public String getNombreUgel() {
		return nombreUgel;
	}

	public void setNombreUgel(String nombreUgel) {
		this.nombreUgel = nombreUgel;
	}

	public DireccionRegional getDireccionRegional() {
		return direccionRegional;
	}

	public void setDireccionRegional(DireccionRegional direccionRegional) {
		this.direccionRegional = direccionRegional;
	}

	public double getPointX() {
		return pointX;
	}

	public void setPointX(double pointX) {
		this.pointX = pointX;
	}

	public double getPointY() {
		return pointY;
	}

	public void setPointY(double pointY) {
		this.pointY = pointY;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = zoom;
	}
}
