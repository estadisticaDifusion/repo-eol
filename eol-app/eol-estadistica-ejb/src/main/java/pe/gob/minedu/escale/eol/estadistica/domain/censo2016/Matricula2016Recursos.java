package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "matricula2016_recursos")
public class Matricula2016Recursos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2016Cabecera matricula2016Cabecera;
    
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2016Recursos")
    private List<Matricula2016RecursosFila> matricula2016RecursosFilaList;
*/
    public Matricula2016Recursos() {
    }

    public Matricula2016Recursos(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Matricula2016Cabecera getMatricula2016Cabecera() {
        return matricula2016Cabecera;
    }

    public void setMatricula2016Cabecera(Matricula2016Cabecera matricula2016Cabecera) {
        this.matricula2016Cabecera = matricula2016Cabecera;
    }
/*
    @XmlElement(name="RECURSOS_FILAS")
    public List<Matricula2016RecursosFila> getMatricula2016RecursosFilaList() {
        return matricula2016RecursosFilaList;
    }

    public void setMatricula2016RecursosFilaList(List<Matricula2016RecursosFila> matricula2016RecursosFilaList) {
        this.matricula2016RecursosFilaList = matricula2016RecursosFilaList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2016Recursos)) {
            return false;
        }
        Matricula2016Recursos other = (Matricula2016Recursos) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
}
