/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_seccion")
public class Matricula2017Seccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2017Cabecera matricula2017Cabecera;
    
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Seccion")
    private List<Matricula2017SeccionFila> matricula2017SeccionFilaList;
*/
    public Matricula2017Seccion() {
    }

    public Matricula2017Seccion(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }
/*
    @XmlElement(name = "SECCION_FILAS")
    public List<Matricula2017SeccionFila> getMatricula2017SeccionFilaList() {
        return matricula2017SeccionFilaList;
    }

    public void setMatricula2017SeccionFilaList(List<Matricula2017SeccionFila> matricula2017SeccionFilaList) {
        this.matricula2017SeccionFilaList = matricula2017SeccionFilaList;
    }
*/
    @XmlTransient
    public Matricula2017Cabecera getMatricula2017Cabecera() {
        return matricula2017Cabecera;
    }

    public void setMatricula2017Cabecera(Matricula2017Cabecera matricula2017Cabecera) {
        this.matricula2017Cabecera = matricula2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2017Seccion)) {
            return false;
        }
        Matricula2017Seccion other = (Matricula2017Seccion) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo20172.Matricula2017Seccion[idEnvio=" + idEnvio + "]";
    }
}
