/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.ejb.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author JMATAMOROS
 */

@Embeddable
public class EolPadronPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "COD_MOD")
	private String codMod;

	@Column(name = "ANEXO")
	private String anexo;

	/**
	 * @return the codMod
	 */
	public String getCodMod() {
		return codMod;
	}

	/**
	 * @param codMod the codMod to set
	 */
	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	/**
	 * @return the anexo
	 */
	public String getAnexo() {
		return anexo;
	}

	/**
	 * @param anexo the anexo to set
	 */
	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

}
