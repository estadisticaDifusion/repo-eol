/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb;

import static pe.gob.minedu.escale.eol.estadistica.util.Constantes.USUARIO_DRE;
import static pe.gob.minedu.escale.eol.estadistica.util.Constantes.USUARIO_DRE_CALLAO;
import static pe.gob.minedu.escale.eol.estadistica.util.Constantes.USUARIO_DRE_LIMA_METRO;
import static pe.gob.minedu.escale.eol.estadistica.util.Constantes.USUARIO_DRE_LIMA_PROV;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Resultado2011Cabecera;
import pe.gob.minedu.escale.eol.estadistica.util.Constantes;

/**
 *
 * @author DSILVA
 */
@Stateless
public class EnvioDocumentosFacade {

	private final Logger logger = Logger.getLogger(EnvioDocumentosFacade.class);

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	private StringBuffer cadSQL = new StringBuffer();

	static String BD_ESTADISTICA = "estadistica";
	static String BD_PADRON = "padron";
	private int rangoRepMat[][] = { { 16, 6, 3 }, { 16, 6, 2 }, { 12, 6, 3 }, { 12, 6, 3 }, { 20, 10, 2 },
			{ 32, 16, 2 }, { 40, 0, 2 }, { 70, 31, 3 }, { 4, 2, 2 } };
	private int rangoRepMat2013[][] = { { 16, 6, 3 }, { 16, 6, 2 }, { 12, 6, 3 }, { 18, 9, 3 }, { 20, 10, 2 },
			{ 32, 16, 2 }, { 40, 0, 2 }, { 50, 20, 3 }, { 4, 2, 2 } };
	private int rangoRepMat2014[][] = { { 16, 6, 3, 3 }, { 16, 6, 2, 0 }, { 12, 6, 3, 3 }, { 18, 9, 3, 3 },
			{ 20, 10, 2, 2 }, { 32, 16, 2, 2 }, { 40, 0, 2, 2 }, { 52, 16, 3, 3 }, { 4, 2, 2, 2 } };
	private int rangoRepMat2015[][] = { { 14, 7, 3, 3 }, { 14, 7, 2, 0 }, { 12, 6, 3, 3 }, { 18, 9, 2, 3 },
			{ 20, 10, 2, 2 }, { 24, 12, 2, 2 }, { 40, 0, 2, 2 }, { 44, 14, 3, 3 }, { 4, 2, 2, 2 } };
	private int rangoRepMat2016[][] = { { 14, 7, 4, 3 }, { 14, 7, 2, 0 }, { 12, 6, 4, 3 }, { 30, 15, 2, 3 },
			{ 20, 10, 2, 2 }, { 24, 12, 2, 2 }, { 40, 0, 2, 2 }, { 50, 14, 3, 3 }, { 4, 2, 2, 2 }, { 32, 16, 2, 3 } };
	private int rangoRepMat2017[][] = { /* 1a */{ 14, 7, 4, 3 }, /* 2a */ { 14, 7, 2, 0 }, /* 3a */ { 12, 6, 4, 3 },
			/* 4ai */ { 30, 15, 2, 3 }, /* 5a */ { 20, 10, 2, 2 }, /* 6a */ { 24, 12, 2, 2 }, /* 7a */ { 40, 0, 2, 2 },
			/* 8a */ { 50, 14, 3, 3 }, /* 9a */ { 4, 2, 2, 2 }, /* 4aa */ { 32, 16, 2, 3 }, /* 3ap */ { 12, 6, 4, 3 },
			/* 3as */ { 10, 5, 4, 3 }, /* 8ai */ { 24, 8, 3, 3 }, /* 8ap */ { 20, 6, 3, 3 } };
	private int rangoRepMat2018[][] = { /* 1a */{ 14, 7, 4, 3 }, /* 2a */ { 14, 7, 2, 0 }, /* 3a */ { 12, 6, 4, 3 },
			/* 4ai */ { 30, 15, 2, 3 }, /* 5a */ { 20, 10, 2, 2 }, /* 6a */ { 24, 12, 2, 2 }, /* 7a */ { 40, 0, 2, 2 },
			/* 8a */ { 50, 14, 3, 3 }, /* 9a */ { 4, 2, 2, 2 }, /* 4aa */ { 32, 16, 2, 3 }, /* 3ap */ { 12, 6, 4, 3 },
			/* 3as */ { 10, 5, 4, 3 }, /* 8ai */ { 24, 8, 3, 3 }, /* 8ap */ { 20, 6, 3, 3 } };
	private int rangoRepMat2019[][] = { /* 1a */{ 14, 7, 4, 3 }, /* 2a */ { 14, 7, 2, 0 }, /* 3a */ { 12, 6, 4, 3 },
			/* 4ai */ { 30, 15, 2, 3 }, /* 5a */ { 20, 10, 2, 2 }, /* 6a */ { 24, 12, 2, 2 }, /* 7a */ { 40, 0, 2, 2 },
			/* 8a */ { 50, 14, 3, 3 }, /* 9a */ { 4, 2, 2, 2 }, /* 4aa */ { 32, 16, 2, 3 }, /* 3ap */ { 12, 6, 4, 3 },
			/* 3as */ { 10, 5, 4, 3 }, /* 8ai */ { 24, 8, 3, 3 }, /* 8ap */ { 20, 6, 3, 3 } };
	// private int rangoRepLoc2013[][] = {{125, 18}};
	private int rangoRepRes[] = { 22, 22, 36, 18, 8, 32, 16, 44, 8 };
	private int rangoRepRes2012[] = { 22, 22, 36, 36, 8, 32, 16, 44, 8 };
	private int rangoRepRes2013[] = { 28, 22, 36, 34, 8, 56, 16, 36, 8 };
	private int rangoRepRes2014[] = { 28, 22, 114, 34, 8, 56, 16, 36, 8 };
	private int rangoRepRes2015[] = { 20, 20, 108, 34, 16, 76, 32, 40, 32 };
	private int rangoRepRes2016[] = { 20, 20, 114, 34, 16, 76, 32, 40, 36, 68 };
	private int rangoRepRes2017[] = { 20, 20, 114, 34, 16, 76, 32, 40, 36, 114, 68, 40 };
	private int rangoRepRes2018[] = { 20, 20, 114, 34, 16, 76, 32, 22, 44, 95, 68, 24 };
	private int rangoRepRes2019[] = { 20, 20, 114, 34, 16, 76, 32, 22, 44, 95, 68, 24 };
	// static String BD_ESTADISTICA="estadistica_capa";
	// static String BD_PADRON="padron_capa";
	// imendoza
	static String nroCedula1A = "c01a";
	static String nroCedula2A = "c02a";
	static String nroCedula3A = "c03a";
	static String nroCedula3Ap = "c03ap";
	static String nroCedula3As = "c03as";
	static String nroCedula4AI = "c04ai";
	static String nroCedula4AA = "c04aa";
	static String nroCedula5A = "c05a";
	static String nroCedula6A = "c06a";
	static String nroCedula7A = "c07a";
	static String nroCedula8A = "c08a";
	static String nroCedula8Ai = "c08ai";
	static String nroCedula8Ap = "c08ap";
	static String nroCedula9A = "c09a";

	static String cedula3BP = "3bp";
	static String cedula3BS = "3bs";
	static String cedula4AA = "4aa";
	static String cedula4BA = "4ba";
	static String cedula8BI = "3bp";
	static String cedula8BP = "8bp";

	static String cedula3AP = "3ap";
	static String cedula3AS = "3as";
	static String cedula8AI = "8ai";
	static String cedula8AP = "8ap";
	static String cuadro201 = "C201";
	static String cuadro202 = "C202";
	static String cuadro203 = "C203";
	static String cuadro204 = "C204";
	static String cuadro206 = "C206";
	static String cuadro207 = "C207";
	static String cuadro208 = "C208";
	static String cuadro209 = "C209";
	// imendoza 20170330 inicio
	final static String NROCEDULA_1A = "c01a";
	final static String NROCEDULA_2A = "c02a";
	final static String NROCEDULA_3AP = "c03ap";
	final static String NROCEDULA_3AS = "c03as";
	final static String NROCEDULA_4AI = "c04ai";
	final static String NROCEDULA_4AA = "c04aa";
	final static String NROCEDULA_5A = "c05a";
	final static String NROCEDULA_6A = "c06a";
	final static String NROCEDULA_7A = "c07a";
	final static String NROCEDULA_8AI = "c08ai";
	final static String NROCEDULA_8AP = "c08ap";
	final static String NROCEDULA_9A = "c09a";
	// imendoza 20170330 fin

	public <T> T getDocumentoEnviado(Class<T> clazz, Map<String, String> params) {
		String sql = "SELECT a FROM " + clazz.getSimpleName()
				+ " a WHERE a.anioEscolar=:anioEscolar AND a.codigoModular=:codigoModular AND a.grado=:grado AND a.seccion=:seccion AND a.ultimo=true ";
		String tipo = params.get("tipo");
		String anioEscolar = params.get("anioEscolar");
		String codigoModular = params.get("codigoModular");
		String grado = params.get("grado");
		String seccion = params.get("seccion");
		if (tipo != null && !tipo.isEmpty()) {
			sql += " AND a.tipo=:tipo";
		}
		sql += " ORDER BY a.grado,a.seccion" + ((tipo != null && !tipo.isEmpty()) ? ",a.tipo" : "");
		Query query = em.createQuery(sql);
		query.setParameter("anioEscolar", anioEscolar).setParameter("codigoModular", codigoModular)
				.setParameter("grado", grado).setParameter("seccion", seccion);
		if (tipo != null && !tipo.isEmpty()) {
			query.setParameter("tipo", tipo);
		}
		List<T> lista = query.getResultList();
		if (lista.isEmpty()) {
			return null;
		}
		return lista.get(0);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> getEnvioTablero(Map<String, String> mapQuery, String codMod, String codLocal, String anexo,
			String nivMod) {
		StringBuffer strQ = new StringBuffer();
		Iterator it = mapQuery.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry en = (Map.Entry) it.next();
			strQ.append(mapQuery.get(en.getKey()));
			strQ.append(" UNION ");
		}

		strQ.delete(strQ.length() - 7, strQ.length());

		Query q = em.createNativeQuery(strQ.toString());
		q.setParameter("COD_MOD", codMod);
		q.setParameter("ANEXO", anexo);
		q.setParameter("COD_LOCAL", codLocal);
		q.setParameter("NIV_MOD", nivMod.substring(0, 1).concat("%"));

		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getPreLlenadoMatricula(String codMod, String cuadro, String nivel, String nroced,
			String periodo) {
		Query query = null;
		cadSQL.delete(0, cadSQL.length());
		cadSQL.append(" SELECT   IDEENVIO ,  FECHAENVIO_NOMINA ,  COD_MOD ,");
		cadSQL.append(" CUADRO, TIPDATO ,  T_H ,  T_M ,  DATO01H , ");
		cadSQL.append(" DATO01M ,  DATO02H ,  DATO02M ,  DATO03H ,  DATO03M ,");
		cadSQL.append(" DATO04H ,  DATO04M ,  DATO05H ,  DATO05M ,  DATO06H ,");
		cadSQL.append(" DATO06M ,  DATO07H ,  DATO07M ,  DATO08H ,  DATO08M ,");
		cadSQL.append(" DATO09H ,  DATO09M ,  DATO10H ,  DATO10M ,  APROBADO_IE ,");
		cadSQL.append(" APROBADO_UGEL  ");
		cadSQL.append(" FROM mat_300_prellenado_siagie ");
		cadSQL.append(" WHERE COD_MOD=? AND CUADRO=? AND NIVEL=? ORDER BY TIPDATO");

		query = em.createNativeQuery(cadSQL.toString());
		query.setParameter(1, codMod);
		query.setParameter(2, cuadro);
		query.setParameter(3, nivel);

		List<Object[]> listResult = query.getResultList();

		/*
		 * if (listResult != null && !listResult.isEmpty()) { String sqlUpdate =
		 * "UPDATE mat_300_prellenado_siagie SET FLAG_DESCARGA='1' WHERE COD_MOD=? AND CUADRO=? AND NIVEL=? AND NROCED=? "
		 * ; em.createNativeQuery(sqlUpdate).setParameter(1, codMod).setParameter(2,
		 * cuadro).setParameter(3, nivel) .setParameter(4, nroced).setParameter(5,
		 * periodo).executeUpdate(); }
		 */

		return listResult;

	}
	// INICIO - LISTA DE ENVIOS POR UGEL/DRE
	// -- INICIO 2011

	public List<Object[]> getResumenEnvioMatDocRec2011(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODIGEL %1$s ?   ";

		String sql1 = "SELECT p.CODIGEL AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta"
				+ " FROM resultado2012_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.ESTADO='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		String sql = "SELECT p.CODIGEL AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIVMOD) AS Cuenta"
				+ " FROM matricula2011_cabecera e" + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.ESTADO='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODIGEL LIKE '1501%' OR p.CODIGEL LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODIGEL LIKE '1502%' OR p.CODIGEL LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODIGEL LIKE '0701%' OR p.CODIGEL like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioLocal2011(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.codigel %1$s ?   ";
		String sql = "SELECT p.codigel as CODIGEL_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2011_cabecera e " + " INNER JOIN local2011_sec104 d ON e.ID_ENVIO= d.CABECERA_IDENVIO "
				+ " INNER JOIN " + BD_PADRON
				+ ".padron p ON d.P104_CM=p.COD_MOD AND d.P104_ANX=p.ANEXO AND p.codlocal=e.CODLOCAL "
				+ " WHERE  e.ULTIMO  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codigel LIKE '1501%' OR p.codigel LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codigel LIKE '1502%' OR p.codigel LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codigel LIKE '0701%' OR p.codigel like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioLocal2012(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2012_cabecera e " + " INNER JOIN local2012_sec104 d ON e.ID_ENVIO= d.CABECERA_IDENVIO "
				+ " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  e.ULTIMO  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioResultado2011(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODIGEL %1$s ?   ";
		String sql = " SELECT p.CODIGEL AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".resultado2011_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO AND SUBSTRING(e.NIV_MOD,1,1)= SUBSTRING(p.NIV_MOD,1,1) "
				+ " WHERE p.ESTADO='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODIGEL LIKE '1501%' OR p.CODIGEL LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODIGEL LIKE '1502%' OR p.CODIGEL LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODIGEL LIKE '0701%' OR p.CODIGEL like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();
	}
	// -- FINAL 2011
	// -- INICIO 2012

	public List<Object[]> getPreLlenadoResultado2012(String codMod, String cuadro, String nivel) {
		Query query = null;
		cadSQL.delete(0, cadSQL.length());
		cadSQL.append(" SELECT  COD_MOD ,");
		cadSQL.append(" CUADRO, TIPDATO ,  TOTAL_1 ,  TOTAL_2,  D01 , ");
		cadSQL.append(" D02 ,  D03,  D04,  D05,  D06 ,");
		cadSQL.append(" D07 ,  D08,  D09,  D10,  D11 , D12");
		cadSQL.append(" FROM eval_100_prellenado_siagie_2012 ");
		cadSQL.append(" WHERE COD_MOD=? AND CUADRO=? AND NIV_MOD=? ORDER BY TIPDATO");
		// cadSQL.append(" WHERE COD_MOD=? AND CUADRO=? AND NIVEL=? AND NROCED=? AND
		// PERIODO=? ORDER BY TIPDATO");

		query = em.createNativeQuery(cadSQL.toString());
		query.setParameter(1, codMod);
		query.setParameter(2, cuadro);
		query.setParameter(3, nivel);

		// query.setParameter(5, periodo);
		List<Object[]> listResult = query.getResultList();
		return listResult;

	}

	public List<Object[]> getResumenEnvioMatDocRec2012(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2012_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.ESTADO='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioResultado2012(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String sql = " SELECT p.CODOOII AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".resultado2012_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO AND SUBSTRING(e.NIV_MOD,1,1)= SUBSTRING(p.NIV_MOD,1,1) "
				+ " WHERE p.ESTADO='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();
	}

	// -- FINAL 2012
	// -- INICIO 2013
	public List<Object[]> getResumenEnvioMatDocRec2013(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2013_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.ESTADO='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioMatDocRec2015(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2015_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.mcenso='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY : " + sql);
		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioMatDocRec2016(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2016_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.mcenso='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY : " + sql);
		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioMatDocRec2017(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2017_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO "
				+ " WHERE p.mcenso='1' AND p.estado='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY : " + sql);
		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	/*
	 * public List<Object[]> getResumenEnvioMatDocRec2017AntesFecha(String idUgel,
	 * String fechacorte) { Query query = null; String WHERE1 =
	 * " AND p.CODOOII %1$s ?   "; String wheredate = (!fechacorte.equals("") &&
	 * fechacorte != null ? " AND e.FECHA_ENVIO < '" + fechacorte + "'" : "");
	 * 
	 * String sql =
	 * "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta"
	 * + " FROM " + BD_ESTADISTICA + ".matricula2017_cabecera e " + " INNER JOIN " +
	 * BD_PADRON + ".padron p " + " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " +
	 * " WHERE p.mcenso='1' AND p.estado='1' " + wheredate + "  %1$s GROUP BY 1 ";
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) { WHERE1 =
	 * " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_LIMA_PROV))
	 * { WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
	 * WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') "; sql =
	 * String.format(sql, WHERE1); } else { sql = String.format(sql, WHERE1); if
	 * (idUgel.endsWith("00")) { idUgel = idUgel.substring(0, 4) + "%"; sql =
	 * String.format(sql, "LIKE"); } else { sql = String.format(sql, "="); } } sql =
	 * "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";
	 * logger.info("QUERY ENVIO: " + sql); query =
	 * em.createNativeQuery(sql).setParameter(1, idUgel); return
	 * query.getResultList();
	 * 
	 * }
	 */

	public List<Object[]> getResumenEnvioMatDocRec2018AntesFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String wheredate = (!fechacorte.equals("") && fechacorte != null ? " AND e.FECHA_ENVIO < '" + fechacorte + "'"
				: "");

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2018_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD = p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.mcenso='1' AND p.estado='1' " + wheredate
				+ "  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY ENVIO: " + sql);
		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioMatDocRec2019AntesFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String wheredate = (!fechacorte.equals("") && fechacorte != null ? " AND e.FECHA_ENVIO < '" + fechacorte + "'"
				: "");

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2019_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD = p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.mcenso='1' AND p.estado='1' " + wheredate
				+ "  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY ENVIO: " + sql);
		if (!(idUgel.equals(USUARIO_DRE_LIMA_METRO) || idUgel.equals(USUARIO_DRE_LIMA_PROV)
				|| idUgel.equals(USUARIO_DRE_CALLAO))) {
			query = em.createNativeQuery(sql).setParameter(1, idUgel);
		} else {
			query = em.createNativeQuery(sql);
		}

		return query.getResultList();

	}

	/*
	 * public List<Object[]> getResumenEnvioMatDocRec2017PostFecha(String idUgel,
	 * String fechacorte) { Query query = null; String WHERE1 =
	 * " AND p.CODOOII %1$s ?   ";
	 * 
	 * String sql =
	 * "SELECT p.CODOOII,e.COD_MOD,e.ANEXO,e.NIV_MOD,SUM(IF(e.FECHA_ENVIO < '" +
	 * fechacorte + "',1,0)) AS ENVIOANTES" + " FROM " + BD_ESTADISTICA +
	 * ".matricula2017_cabecera e  " + " INNER JOIN " + BD_PADRON +
	 * ".padron p  ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO  " +
	 * " WHERE p.mcenso='1' AND p.estado='1' %1$s  GROUP BY 1,2,3,4 " +
	 * " HAVING SUM(IF(e.FECHA_ENVIO < '" + fechacorte + "',1,0)) = 0 ";
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) { WHERE1 =
	 * " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_LIMA_PROV))
	 * { WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
	 * WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') "; sql =
	 * String.format(sql, WHERE1); } else { sql = String.format(sql, WHERE1); if
	 * (idUgel.endsWith("00")) { idUgel = idUgel.substring(0, 4) + "%"; sql =
	 * String.format(sql, "LIKE"); } else { sql = String.format(sql, "="); } } sql =
	 * "SELECT CODOOII, COUNT(DISTINCT COD_MOD,ANEXO,NIV_MOD) AS CONTEO FROM (" +
	 * sql + ") b GROUP BY 1 ORDER BY 1"; logger.info("QUERY mat post: " + sql);
	 * query = em.createNativeQuery(sql).setParameter(1, idUgel); return
	 * query.getResultList();
	 * 
	 * }
	 */

	public List<Object[]> getResumenEnvioMatDocRec2018PostFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII,e.COD_MOD,e.ANEXO,e.NIV_MOD,SUM(IF(e.FECHA_ENVIO < '" + fechacorte
				+ "',1,0)) AS ENVIOANTES" + " FROM " + BD_ESTADISTICA + ".matricula2018_cabecera e  " + " INNER JOIN "
				+ BD_PADRON + ".padron p  ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO  "
				+ " WHERE p.mcenso='1' AND p.estado='1' %1$s  GROUP BY 1,2,3,4 " + " HAVING SUM(IF(e.FECHA_ENVIO < '"
				+ fechacorte + "',1,0)) = 0 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "SELECT CODOOII, COUNT(DISTINCT COD_MOD,ANEXO,NIV_MOD) AS CONTEO FROM (" + sql
				+ ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY mat post: " + sql);
		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioMatDocRec2019PostFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII,e.COD_MOD,e.ANEXO,e.NIV_MOD,SUM(IF(e.FECHA_ENVIO < '" + fechacorte
				+ "',1,0)) AS ENVIOANTES" + " FROM " + BD_ESTADISTICA + ".matricula2019_cabecera e  " + " INNER JOIN "
				+ BD_PADRON + ".padron p  ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO  "
				+ " WHERE p.mcenso='1' AND p.estado='1' %1$s  GROUP BY 1,2,3,4 " + " HAVING SUM(IF(e.FECHA_ENVIO < '"
				+ fechacorte + "',1,0)) = 0 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "SELECT CODOOII, COUNT(DISTINCT COD_MOD,ANEXO,NIV_MOD) AS CONTEO FROM (" + sql
				+ ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY mat post: " + sql);

		if (!(idUgel.equals(USUARIO_DRE_LIMA_METRO) || idUgel.equals(USUARIO_DRE_LIMA_PROV)
				|| idUgel.equals(USUARIO_DRE_CALLAO))) {
			query = em.createNativeQuery(sql).setParameter(1, idUgel);
		} else {
			query = em.createNativeQuery(sql);
		}
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioMatDocRec2014(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";

		String sql = "SELECT p.CODOOII AS CODOOII,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".matricula2014_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO " + " WHERE p.mcenso='1' AND e.ULTIMO  %1$s GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";
		logger.info("QUERY : " + sql);
		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioResultado2013(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String sql = " SELECT p.CODOOII AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".resultado2013_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO AND SUBSTRING(e.NIV_MOD,1,1)= SUBSTRING(p.NIV_MOD,1,1) "
				+ " WHERE p.MCENSO='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioResultado2014(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String sql = " SELECT p.CODOOII AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".resultado2014_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO AND SUBSTRING(e.NIV_MOD,1,1)= SUBSTRING(p.NIV_MOD,1,1) "
				+ " WHERE p.MCENSO='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioResultado2015(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String sql = " SELECT p.CODOOII AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".resultado2015_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO AND SUBSTRING(e.NIV_MOD,1,1)= SUBSTRING(p.NIV_MOD,1,1) "
				+ " WHERE p.MCENSO='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioResultado2016(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String sql = " SELECT p.CODOOII AS CODIGEL,COUNT(DISTINCT e.COD_MOD,e.ANEXO,e.NIV_MOD) AS Cuenta" + " FROM "
				+ BD_ESTADISTICA + ".resultado2016_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO=p.ANEXO AND SUBSTRING(e.NIV_MOD,1,1)= SUBSTRING(p.NIV_MOD,1,1) "
				+ " WHERE p.MCENSO='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);
		logger.info("QUERY envio : " + sql);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioResultado2017(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String WHERE_ISE = " AND p.progise='1' AND e.NIV_MOD = 'T0' ";
		String sqlise = "";
		String sql = " SELECT p.CODOOII AS CODIGEL,COUNT(DISTINCT e.COD_MOD) AS Cuenta " + " FROM " + BD_ESTADISTICA
				+ ".resultado2017_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO = p.anexo "
				+ " WHERE p.MCENSO='1' AND p.estado='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s %2$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sqlise = String.format(sql, WHERE1, WHERE_ISE);
			sql = String.format(sql, WHERE1, "");
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sqlise = String.format(sql, WHERE1, WHERE_ISE);
			sql = String.format(sql, WHERE1, "");
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sqlise = String.format(sql, WHERE1, WHERE_ISE);
			sql = String.format(sql, WHERE1, "");
		} else {
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				WHERE1 = " AND p.CODOOII LIKE ? ";
				sqlise = String.format(sql, WHERE1, WHERE_ISE);
				sql = String.format(sql, WHERE1, "");
			} else {
				WHERE1 = " AND p.CODOOII = ? ";
				sqlise = String.format(sql, WHERE1, WHERE_ISE);
				sql = String.format(sql, WHERE1, "");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + " UNION ALL " + sqlise + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel).setParameter(2, idUgel);

		System.out.println("QUERY envio resultado : " + sql);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioResultado2019(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.CODOOII %1$s ?   ";
		String WHERE_ISE = " AND p.progise='1' AND e.NIV_MOD = 'T0' ";
		String sqlise = "";
		String sql = " SELECT p.CODOOII AS CODIGEL,COUNT(DISTINCT e.COD_MOD) AS Cuenta " + " FROM " + BD_ESTADISTICA
				+ ".resultado2019_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p "
				+ " ON e.COD_MOD=p.COD_MOD AND e.ANEXO = p.anexo "
				+ " WHERE p.MCENSO='1' AND p.estado='1' AND p.TIPOPROG NOT IN ('1','2','3','5','6','7','8','9') AND SUBSTRING(p.tipoice,1,1)='1' AND e.ULTIMO %1$s %2$s GROUP BY 1  ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
			sqlise = String.format(sql, WHERE1, WHERE_ISE);
			sql = String.format(sql, WHERE1, "");
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
			sqlise = String.format(sql, WHERE1, WHERE_ISE);
			sql = String.format(sql, WHERE1, "");
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII like '0700%') ";
			sqlise = String.format(sql, WHERE1, WHERE_ISE);
			sql = String.format(sql, WHERE1, "");
		} else {
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				WHERE1 = " AND p.CODOOII LIKE ? ";
				sqlise = String.format(sql, WHERE1, WHERE_ISE);
				sql = String.format(sql, WHERE1, "");
			} else {
				WHERE1 = " AND p.CODOOII = ? ";
				sqlise = String.format(sql, WHERE1, WHERE_ISE);
				sql = String.format(sql, WHERE1, "");
			}
		}
		sql = "select CODIGEL, SUM(Cuenta) FROM (" + sql + " UNION ALL " + sqlise + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel).setParameter(2, idUgel);

		System.out.println("QUERY envio resultado : " + sql);
		return query.getResultList();
	}

	public List<Object[]> getPreLlenadoResultado2013(String codMod, String cuadro, String nivel) {
		Query query = null;
		cadSQL.delete(0, cadSQL.length());
		cadSQL.append(" SELECT  COD_MOD ,");
		cadSQL.append(" CUADRO, TIPDATO ,  TOTAL_1 ,  TOTAL_2,  D01 , ");
		cadSQL.append(" D02 ,  D03,  D04,  D05,  D06 ,");
		cadSQL.append(" D07 ,  D08,  D09,  D10,  D11 , D12");
		cadSQL.append(" FROM eval_100_prellenado_siagie_2013 ");
		cadSQL.append(" WHERE COD_MOD=? AND CUADRO=? AND NIV_MOD=? ORDER BY TIPDATO");

		query = em.createNativeQuery(cadSQL.toString());
		query.setParameter(1, codMod);
		query.setParameter(2, cuadro);
		query.setParameter(3, nivel);

		List<Object[]> listResult = query.getResultList();
		return listResult;
	}

	public List<Object[]> getPreLlenadoResultado2014(String codMod, String cuadro, String nivel) {
		Query query = null;
		cadSQL.delete(0, cadSQL.length());
		cadSQL.append(" SELECT  COD_MOD ,");
		cadSQL.append(" CUADRO, TIPDATO ,  TOTAL_1 ,  TOTAL_2,  D01 , ");
		cadSQL.append(" D02 ,  D03,  D04,  D05,  D06 ,");
		cadSQL.append(" D07 ,  D08,  D09,  D10,  D11 , D12");
		cadSQL.append(" FROM eval_100_prellenado_siagie_2014 ");
		cadSQL.append(" WHERE COD_MOD=? AND CUADRO=? AND NIV_MOD=? ORDER BY TIPDATO");

		query = em.createNativeQuery(cadSQL.toString());
		query.setParameter(1, codMod);
		query.setParameter(2, cuadro);
		query.setParameter(3, nivel);

		List<Object[]> listResult = query.getResultList();
		return listResult;
	}

	public List<Object[]> getResumenEnvioLocal2013(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2013_cabecera e " + " INNER JOIN local2013_sec104 d ON e.ID_ENVIO= d.CABECERA_IDENVIO "
				+ " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  e.ULTIMO  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2, idUgel);
		return query.getResultList();

	}

	// FINAL -- 2013
	// INICIO -2014
	public List<Object[]> getResumenEnvioLocal2014(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2014_cabecera e " + " INNER JOIN local2014_sec104 d ON e.ID_ENVIO= d.CABECERA_IDENVIO "
				+ " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  e.ULTIMO  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2, idUgel);
		return query.getResultList();

	}
	// FINAL - LISTA DE ENVIOS POR UGEL/DRE

	// INICIOS DEL 2015
	public List<Object[]> getResumenEnvioLocal2015(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		// String WHERE2 = " AND COD_IGEL %1$s ? AND NIVMOD ='A4' ";
		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2015_cabecera e " + " INNER JOIN local2015_sec104 d ON e.ID_ENVIO= d.CABECERA_IDENVIO "
				+ " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  e.ULTIMO  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		// d.P104_CM=p.COD_MOD AND d.P104_ANX=p.ANEXO
		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2, idUgel);
		return query.getResultList();

	}
	// INICIOS DEL 2015

	public List<Object[]> getResumenEnvioLocal2016(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		// String WHERE2 = " AND COD_IGEL %1$s ? AND NIVMOD ='A4' ";
		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2016_cabecera e "
				// + " INNER JOIN local2016_sec104 d ON e.ID_ENVIO= d.CABECERA_IDENVIO "
				+ " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  e.ULTIMO  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		// d.P104_CM=p.COD_MOD AND d.P104_ANX=p.ANEXO
		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2, idUgel);

		logger.info("QUERY LOCAL: " + sql);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioLocal2017(String idUgel) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2017_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  e.ULTIMO  %1$s AND p.estado='1' AND p.mcenso = '1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);

		logger.info("QUERY LOCAL: " + sql);
		return query.getResultList();

	}

	/*
	 * public List<Object[]> getResumenEnvioLocal2017AntesFecha(String idUgel,
	 * String fechacorte) { Query query = null; String WHERE1 =
	 * " AND p.codooii %1$s ?   "; String wheredate = (!fechacorte.equals("") &&
	 * fechacorte != null ? " e.FECHA_ENVIO < '" + fechacorte + "'" : "");
	 * 
	 * String sql =
	 * "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta" +
	 * " FROM local2017_cabecera e " + " INNER JOIN " + BD_PADRON +
	 * ".padron p ON  p.codlocal=e.CODLOCAL " + " WHERE  " + wheredate +
	 * " AND p.mcenso = '1'  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 "
	 * ; if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) { WHERE1 =
	 * " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_LIMA_PROV))
	 * { WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
	 * WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') "; sql =
	 * String.format(sql, WHERE1); } else { sql = String.format(sql, WHERE1); if
	 * (idUgel.endsWith("00")) { idUgel = idUgel.substring(0, 4) + "%"; sql =
	 * String.format(sql, "LIKE"); } else { sql = String.format(sql, "="); } } sql =
	 * "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql +
	 * ") b GROUP BY 1 ORDER BY 1";
	 * 
	 * query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2,
	 * idUgel);
	 * 
	 * logger.info("QUERY LOCAL: " + sql); return query.getResultList();
	 * 
	 * }
	 */

	public List<Object[]> getResumenEnvioLocal2018AntesFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String wheredate = (!fechacorte.equals("") && fechacorte != null ? " e.FECHA_ENVIO < '" + fechacorte + "'"
				: "");

		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2018_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  " + wheredate
				+ " AND p.mcenso = '1'  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);

		logger.info("QUERY LOCAL: " + sql);
		return query.getResultList();
	}

	public List<Object[]> getResumenEnvioLocal2019AntesFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String wheredate = (!fechacorte.equals("") && fechacorte != null ? " e.FECHA_ENVIO < '" + fechacorte + "'"
				: "");

		String sql = "SELECT p.codooii as CODOOII_PADRON,COUNT(DISTINCT e.CODLOCAL) AS Cuenta"
				+ " FROM local2019_cabecera e " + " INNER JOIN " + BD_PADRON + ".padron p ON  p.codlocal=e.CODLOCAL "
				+ " WHERE  " + wheredate
				+ " AND p.mcenso = '1'  %1$s AND p.estado='1' AND p.NIV_MOD !='A5' GROUP BY 1 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, SUM(Cuenta) FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		if (!(idUgel.equals(USUARIO_DRE_LIMA_METRO) || idUgel.equals(USUARIO_DRE_LIMA_PROV)
				|| idUgel.equals(USUARIO_DRE_CALLAO))) {
			query = em.createNativeQuery(sql).setParameter(1, idUgel);
		} else {
			query = em.createNativeQuery(sql);
		}

		logger.info("QUERY LOCAL: " + sql);
		return query.getResultList();
	}

	/*
	 * public List<Object[]> getResumenEnvioLocal2017PostFecha(String idUgel, String
	 * fechacorte) { Query query = null; String WHERE1 = " AND p.codooii %1$s ?   ";
	 * String sql =
	 * "SELECT p.codooii AS CODOOII_PADRON,e.CODLOCAL AS CODLOCAL,SUM(IF(e.FECHA_ENVIO < '"
	 * + fechacorte + "',1,0)) AS ENVIOANTES" +
	 * " FROM local2017_cabecera e  INNER JOIN " + BD_PADRON +
	 * ".padron p ON  p.codlocal=e.CODLOCAL  " + " WHERE  " +
	 * " p.estado='1' AND p.mcenso = '1' AND p.NIV_MOD !='A5' %1$s GROUP BY 1,2 " +
	 * " HAVING SUM(IF(e.FECHA_ENVIO < '" + fechacorte + "',1,0)) = 0 ";
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) { WHERE1 =
	 * " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_LIMA_PROV))
	 * { WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') "; sql =
	 * String.format(sql, WHERE1); } else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
	 * WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') "; sql =
	 * String.format(sql, WHERE1); } else { sql = String.format(sql, WHERE1); if
	 * (idUgel.endsWith("00")) { idUgel = idUgel.substring(0, 4) + "%"; sql =
	 * String.format(sql, "LIKE"); } else { sql = String.format(sql, "="); } } sql =
	 * "select CODOOII_PADRON, COUNT(DISTINCT CODLOCAL) AS CONTEO FROM (" + sql +
	 * ") b GROUP BY 1 ORDER BY 1";
	 * 
	 * query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2,
	 * idUgel);
	 * 
	 * logger.info("QUERY LOCAL: " + sql); return query.getResultList();
	 * 
	 * }
	 */

	public List<Object[]> getResumenEnvioLocal2018PostFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String sql = "SELECT p.codooii AS CODOOII_PADRON,e.CODLOCAL AS CODLOCAL,SUM(IF(e.FECHA_ENVIO < '" + fechacorte
				+ "',1,0)) AS ENVIOANTES" + " FROM local2018_cabecera e  INNER JOIN " + BD_PADRON
				+ ".padron p ON  p.codlocal=e.CODLOCAL  " + " WHERE  "
				+ " p.estado='1' AND p.mcenso = '1' AND p.NIV_MOD !='A5' %1$s GROUP BY 1,2 "
				+ " HAVING SUM(IF(e.FECHA_ENVIO < '" + fechacorte + "',1,0)) = 0 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, COUNT(DISTINCT CODLOCAL) AS CONTEO FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		query = em.createNativeQuery(sql).setParameter(1, idUgel);// .setParameter(2, idUgel);

		logger.info("QUERY LOCAL: " + sql);
		return query.getResultList();

	}

	public List<Object[]> getResumenEnvioLocal2019PostFecha(String idUgel, String fechacorte) {
		Query query = null;
		String WHERE1 = " AND p.codooii %1$s ?   ";
		String sql = "SELECT p.codooii AS CODOOII_PADRON,e.CODLOCAL AS CODLOCAL,SUM(IF(e.FECHA_ENVIO < '" + fechacorte
				+ "',1,0)) AS ENVIOANTES" + " FROM local2019_cabecera e  INNER JOIN " + BD_PADRON
				+ ".padron p ON  p.codlocal=e.CODLOCAL  " + " WHERE  "
				+ " p.estado='1' AND p.mcenso = '1' AND p.NIV_MOD !='A5' %1$s GROUP BY 1,2 "
				+ " HAVING SUM(IF(e.FECHA_ENVIO < '" + fechacorte + "',1,0)) = 0 ";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE1 = " AND (p.codooii LIKE '1501%' OR p.codooii LIKE '1510%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE1 = " AND (p.codooii LIKE '1502%' OR p.codooii LIKE '1520%') ";
			sql = String.format(sql, WHERE1);
		} else if (idUgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE1 = " AND (p.codooii LIKE '0701%' OR p.codooii like '0700%') ";
			sql = String.format(sql, WHERE1);
		} else {
			sql = String.format(sql, WHERE1);
			if (idUgel.endsWith("00")) {
				idUgel = idUgel.substring(0, 4) + "%";
				sql = String.format(sql, "LIKE");
			} else {
				sql = String.format(sql, "=");
			}
		}
		sql = "select CODOOII_PADRON, COUNT(DISTINCT CODLOCAL) AS CONTEO FROM (" + sql + ") b GROUP BY 1 ORDER BY 1";

		if (!(idUgel.equals(USUARIO_DRE_LIMA_METRO) || idUgel.equals(USUARIO_DRE_LIMA_PROV)
				|| idUgel.equals(USUARIO_DRE_CALLAO))) {
			query = em.createNativeQuery(sql).setParameter(1, idUgel);
		} else {
			query = em.createNativeQuery(sql);
		}

		logger.info("QUERY LOCAL: " + sql);
		return query.getResultList();

	}

	public List<Object[]> getCuentaMatricula(String idUgel, String tipo) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = " SELECT " + " A.COD_IGEL AS COD_IGEL,B.COD_CONST AS NIV_MOD,"
				+ " COUNT(DISTINCT A.COD_MOD,A.ANEXO, A.NIVMOD) AS CUENTA,A.FECHAENVIO FECHA "
				+ " FROM matricula2011_cabecera A LEFT JOIN constantes B "
				+ " ON ( A.NIVMOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE A.ESTADO_PADRON=1 AND A.ULTIMO %1$s GROUP BY 2 ";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.COD_IGEL LIKE '1501%' OR A.COD_IGEL LIKE '1510%')";
			// WHERE2=" AND (A.COD_IGEL LIKE '1501%' OR A.COD_IGEL LIKE '1510%') AND
			// A.NIVMOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.COD_IGEL LIKE '1502%' OR A.COD_IGEL LIKE '1520%')";
			// WHERE2=" AND (A.COD_IGEL LIKE '1502%' OR A.COD_IGEL LIKE '1520%') AND
			// A.NIVMOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.COD_IGEL LIKE '0701%' OR A.COD_IGEL LIKE '0700%')";
			WHERE2 = " AND (A.COD_IGEL LIKE '0701%' OR A.COD_IGEL LIKE '0700%') AND A.NIVMOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.COD_IGEL LIKE '".concat(idDRE).concat("%'");
			// WHERE2=" AND A.COD_IGEL LIKE '".concat(idDRE).concat("%'").concat(" AND
			// A.NIVMOD='A4'");

			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else {
			WHERE1 = String.format(" AND A.COD_IGEL = %1$s ", idUgel);
			// WHERE2=String.format(" AND A.COD_IGEL = %1$s AND A.NIVMOD='A4'",idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		}

		// cadSQL.setLength(cadSQL.length()-UNION.length());
		// System.out.println("SQL ENVIO:"+cadSQL.toString());
		String sql = "SELECT COD_IGEL ,NIV_MOD , SUM(CUENTA) FROM (" + cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		query = em.createNativeQuery(sql);
		return query.getResultList();
	}

	public List<Object[]> getCuentaMatriculaNewYear(String idUgel, String tipo) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = " SELECT " + " A.CODUGEL AS CODUGEL,B.COD_CONST AS NIV_MOD,"
				+ " COUNT(DISTINCT A.COD_MOD,A.ANEXO, A.NIV_MOD) AS CUENTA,A.FECHA_ENVIO FECHA "
				+ " FROM matricula2014_cabecera A INNER JOIN padron.padron p ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
				+ "LEFT JOIN constantes B "
				+ " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1' AND A.ULTIMO = 1  %1$s GROUP BY 2 ";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
			// WHERE2=" AND (A.COD_IGEL LIKE '1501%' OR A.COD_IGEL LIKE '1510%') AND
			// A.NIVMOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
			// WHERE2=" AND (A.COD_IGEL LIKE '1502%' OR A.COD_IGEL LIKE '1520%') AND
			// A.NIVMOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')";
			WHERE2 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
			// WHERE2=" AND A.COD_IGEL LIKE '".concat(idDRE).concat("%'").concat(" AND
			// A.NIVMOD='A4'");

			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else {
			WHERE1 = String.format(" AND A.CODUGEL = %1$s ", idUgel);
			// WHERE2=String.format(" AND A.COD_IGEL = %1$s AND A.NIVMOD='A4'",idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		}

		// cadSQL.setLength(cadSQL.length()-UNION.length());
		// System.out.println("SQL ENVIO:"+cadSQL.toString());
		String sql = "SELECT CODUGEL ,NIV_MOD , SUM(CUENTA) FROM (" + cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		// System.out.println("N1: " + sql);
		query = em.createNativeQuery(sql);
		// System.out.println("N2: " + sql);
		return query.getResultList();
	}

	public List<Object[]> getCuentaMatriculaMcenso2015(String idUgel, String tipo) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = " SELECT " + " A.CODUGEL AS CODUGEL,B.COD_CONST AS NIV_MOD,"
				+ " COUNT(DISTINCT A.COD_MOD,A.ANEXO, A.NIV_MOD) AS CUENTA,A.FECHA_ENVIO FECHA "
				+ " FROM matricula2015_cabecera A INNER JOIN padron.padron p ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
				+ "LEFT JOIN constantes B "
				// + " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1'
				// AND p.sienvio = '2' AND A.FECHA_ENVIO < '2017-07-16' %1$s GROUP BY 2 ";
				+ " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1' AND A.FECHA_ENVIO < '2017-07-16'  %1$s GROUP BY 2 ";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
			// WHERE2=" AND (A.COD_IGEL LIKE '1501%' OR A.COD_IGEL LIKE '1510%') AND
			// A.NIVMOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
			// WHERE2=" AND (A.COD_IGEL LIKE '1502%' OR A.COD_IGEL LIKE '1520%') AND
			// A.NIVMOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')";
			WHERE2 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
			// WHERE2=" AND A.COD_IGEL LIKE '".concat(idDRE).concat("%'").concat(" AND
			// A.NIVMOD='A4'");

			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		} else {
			WHERE1 = String.format(" AND A.CODUGEL = %1$s ", idUgel);
			// WHERE2=String.format(" AND A.COD_IGEL = %1$s AND A.NIVMOD='A4'",idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
			// cadSQL.append(String.format(sqlMatDocRec,WHERE2));
		}

		// cadSQL.setLength(cadSQL.length()-UNION.length());
		// System.out.println("SQL ENVIO:"+cadSQL.toString());
		String sql = "SELECT CODUGEL ,NIV_MOD , SUM(CUENTA) FROM (" + cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		// System.out.println("N1: " + sql);
		query = em.createNativeQuery(sql);
		// System.out.println("N2: " + sql);
		return query.getResultList();
	}

	public List<Object[]> getCuentaMatriculaMcenso2016(String idUgel, String tipo) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = " SELECT " + " A.CODUGEL AS CODUGEL,B.COD_CONST AS NIV_MOD,"
				+ " COUNT(DISTINCT A.COD_MOD,A.ANEXO, A.NIV_MOD) AS CUENTA,A.FECHA_ENVIO FECHA "
				+ " FROM matricula2016_cabecera A INNER JOIN padron.padron p ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
				+ "LEFT JOIN constantes B "
				+ " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1' AND A.ULTIMO = 1  %1$s GROUP BY 2 ";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')";
			WHERE2 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else {
			WHERE1 = String.format(" AND A.CODUGEL = %1$s ", idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		}

		String sql = "SELECT CODUGEL ,NIV_MOD , SUM(CUENTA) FROM (" + cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		// System.out.println("N1: " + sql);
		query = em.createNativeQuery(sql);
		// System.out.println("N2: " + sql);
		return query.getResultList();
	}

	public List<Object[]> getCuentaMatriculaMcenso2017(String idUgel, String tipo, String fechacorte) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String wheredate = (!fechacorte.equals("") && fechacorte != null ? " AND A.FECHA_ENVIO < '" + fechacorte + "'"
				: "");

		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = " SELECT " + " A.CODUGEL AS CODUGEL,B.COD_CONST AS NIV_MOD,"
				+ " COUNT(DISTINCT A.COD_MOD,A.ANEXO, A.NIV_MOD) AS CUENTA,A.FECHA_ENVIO FECHA "
				+ " FROM matricula2017_cabecera A INNER JOIN padron.padron p ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
				+ "LEFT JOIN constantes B "
				// + " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1'
				// AND p.estado='1' AND p.sienvio = '2' "+ wheredate +" %1$s GROUP BY 2 ";
				+ " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1' AND p.estado='1' "
				+ wheredate + "  %1$s GROUP BY 2 ";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')";
			WHERE2 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else {
			WHERE1 = String.format(" AND A.CODUGEL = %1$s ", idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		}

		String sql = "SELECT CODUGEL ,NIV_MOD , SUM(CUENTA) FROM (" + cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		// System.out.println("N1: " + sql);
		query = em.createNativeQuery(sql);
		// System.out.println("MARCO 2: " + sql);
		return query.getResultList();
	}

	/*
	 * public List<Object[]> getCuentaMatriculaMcenso2018(String idUgel, String
	 * tipo, String fechacorte) { Query query = null; String WHERE1 = ""; String
	 * WHERE2 = ""; String wheredate = (!fechacorte.equals("") && fechacorte != null
	 * ? " AND A.FECHA_ENVIO < '" + fechacorte + "'" : "");
	 * 
	 * String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
	 * String sqlMatDocRec = " SELECT " +
	 * " A.CODUGEL AS CODUGEL,B.COD_CONST AS NIV_MOD," +
	 * " COUNT(DISTINCT A.COD_MOD,A.ANEXO, A.NIV_MOD) AS CUENTA,A.FECHA_ENVIO FECHA "
	 * +
	 * " FROM matricula2018_cabecera A INNER JOIN padron.padron p ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
	 * + "LEFT JOIN constantes B " // + " ON ( A.NIV_MOD = B.COD_CONST AND
	 * B.TIP_CONST='NIVMOD') WHERE p.mcenso='1' // AND p.estado='1' AND p.sienvio =
	 * '2' "+ wheredate +" %1$s GROUP BY 2 "; +
	 * " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1' AND p.estado='1' "
	 * + wheredate + "  %1$s GROUP BY 2 "; // + " UNION";
	 * 
	 * cadSQL.delete(0, cadSQL.length());
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
	 * WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else if
	 * (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) { WHERE1 =
	 * " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else if
	 * (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) { WHERE1 =
	 * " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')"; WHERE2 =
	 * " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' "
	 * ; cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else if
	 * (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) { WHERE1 =
	 * " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else { WHERE1 =
	 * String.format(" AND A.CODUGEL = %1$s ", idUgel);
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); }
	 * 
	 * String sql = "SELECT CODUGEL ,NIV_MOD , SUM(CUENTA) FROM (" +
	 * cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2"; // System.out.println("N1: "
	 * + sql); query = em.createNativeQuery(sql); // System.out.println("MARCO 2: "
	 * + sql); return query.getResultList(); }
	 */

	public List<Object[]> getCuentaMatriculaMcenso2019(String idUgel, String tipo, String fechacorte) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String wheredate = (!fechacorte.equals("") && fechacorte != null ? " AND A.FECHA_ENVIO < '" + fechacorte + "'"
				: "");

		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = " SELECT " + " A.CODUGEL AS CODUGEL,B.COD_CONST AS NIV_MOD,"
				+ " COUNT(DISTINCT A.COD_MOD,A.ANEXO, A.NIV_MOD) AS CUENTA,A.FECHA_ENVIO FECHA "
				+ " FROM matricula2019_cabecera A INNER JOIN padron.padron p ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
				+ "LEFT JOIN constantes B "
				// + " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1'
				// AND p.estado='1' AND p.sienvio = '2' "+ wheredate +" %1$s GROUP BY 2 ";
				+ " ON ( A.NIV_MOD = B.COD_CONST AND B.TIP_CONST='NIVMOD') WHERE p.mcenso='1' AND p.estado='1' "
				+ wheredate + "  %1$s GROUP BY 2 ";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')";
			WHERE2 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else {
			WHERE1 = String.format(" AND A.CODUGEL = %1$s ", idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		}

		String sql = "SELECT CODUGEL ,NIV_MOD , SUM(CUENTA) FROM (" + cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		// System.out.println("N1: " + sql);
		query = em.createNativeQuery(sql);
		// System.out.println("MARCO 2: " + sql);
		return query.getResultList();
	}

	public List<Object[]> getCuentaMatriculaMcensoPost2017(String idUgel, String tipo, String fechacorte) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = "SELECT A.CODUGEL,A.COD_MOD,A.ANEXO,A.NIV_MOD,SUM(IF(A.FECHA_ENVIO < '" + fechacorte
				+ "',1,0)) AS ENVIOANTES "
				+ "FROM estadistica.matricula2017_cabecera A  INNER JOIN padron.padron p  ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
				+ "WHERE "
				// + "p.mcenso='1' AND p.estado='1' AND p.sienvio = '2' %1$s GROUP BY 1,2,3,4 "
				+ "p.mcenso='1' AND p.estado='1' %1$s GROUP BY 1,2,3,4 " + "HAVING SUM(IF(A.FECHA_ENVIO < '"
				+ fechacorte + "',1,0)) = 0";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')";
			WHERE2 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else {
			WHERE1 = String.format(" AND A.CODUGEL = %1$s ", idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		}

		String sql = "SELECT CODUGEL,NIV_MOD, COUNT(DISTINCT COD_MOD,ANEXO,NIV_MOD) AS CONTEO FROM ("
				+ cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		// System.out.println("N2: " + sql);
		query = em.createNativeQuery(sql);
		// System.out.println("MARCO 3: " + sql);
		return query.getResultList();
	}

	/*
	 * e public List<Object[]> getCuentaMatriculaMcensoPost2018(String idUgel,
	 * String tipo, String fechacorte) { Query query = null; String WHERE1 = "";
	 * String WHERE2 = ""; String idDRE = idUgel.endsWith("00") ?
	 * idUgel.substring(0, 4) : idUgel; String sqlMatDocRec =
	 * "SELECT A.CODUGEL,A.COD_MOD,A.ANEXO,A.NIV_MOD,SUM(IF(A.FECHA_ENVIO < '" +
	 * fechacorte + "',1,0)) AS ENVIOANTES " +
	 * "FROM estadistica.matricula2018_cabecera A  INNER JOIN padron.padron p  ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
	 * + "WHERE " // +
	 * "p.mcenso='1' AND p.estado='1' AND p.sienvio = '2' %1$s GROUP BY 1,2,3,4 " +
	 * "p.mcenso='1' AND p.estado='1' %1$s GROUP BY 1,2,3,4 " +
	 * "HAVING SUM(IF(A.FECHA_ENVIO < '" + fechacorte + "',1,0)) = 0"; // +
	 * " UNION";
	 * 
	 * cadSQL.delete(0, cadSQL.length());
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
	 * WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else if
	 * (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) { WHERE1 =
	 * " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else if
	 * (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) { WHERE1 =
	 * " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')"; WHERE2 =
	 * " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' "
	 * ; cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else if
	 * (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) { WHERE1 =
	 * " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); } else { WHERE1 =
	 * String.format(" AND A.CODUGEL = %1$s ", idUgel);
	 * cadSQL.append(String.format(sqlMatDocRec, WHERE1)); }
	 * 
	 * String sql =
	 * "SELECT CODUGEL,NIV_MOD, COUNT(DISTINCT COD_MOD,ANEXO,NIV_MOD) AS CONTEO FROM ("
	 * + cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2"; //
	 * System.out.println("N2: " + sql); query = em.createNativeQuery(sql); //
	 * System.out.println("MARCO 3: " + sql); return query.getResultList(); }
	 */

	public List<Object[]> getCuentaMatriculaMcensoPost2019(String idUgel, String tipo, String fechacorte) {
		Query query = null;
		String WHERE1 = "";
		String WHERE2 = "";
		String idDRE = idUgel.endsWith("00") ? idUgel.substring(0, 4) : idUgel;
		String sqlMatDocRec = "SELECT A.CODUGEL,A.COD_MOD,A.ANEXO,A.NIV_MOD,SUM(IF(A.FECHA_ENVIO < '" + fechacorte
				+ "',1,0)) AS ENVIOANTES "
				+ "FROM estadistica.matricula2019_cabecera A  INNER JOIN padron.padron p  ON A.COD_MOD=p.COD_MOD AND A.ANEXO=p.ANEXO "
				+ "WHERE "
				// + "p.mcenso='1' AND p.estado='1' AND p.sienvio = '2' %1$s GROUP BY 1,2,3,4 "
				+ "p.mcenso='1' AND p.estado='1' %1$s GROUP BY 1,2,3,4 " + "HAVING SUM(IF(A.FECHA_ENVIO < '"
				+ fechacorte + "',1,0)) = 0";
		// + " UNION";

		cadSQL.delete(0, cadSQL.length());

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1501%' OR A.CODUGEL LIKE '1510%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '1502%' OR A.CODUGEL LIKE '1520%')";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE1 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%')";
			WHERE2 = " AND (A.CODUGEL LIKE '0701%' OR A.CODUGEL LIKE '0700%') AND A.NIV_MOD='A4' ";
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			WHERE1 = " AND A.CODUGEL LIKE '".concat(idDRE).concat("%'");
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		} else {
			WHERE1 = String.format(" AND A.CODUGEL = %1$s ", idUgel);
			cadSQL.append(String.format(sqlMatDocRec, WHERE1));
		}

		String sql = "SELECT CODUGEL,NIV_MOD, COUNT(DISTINCT COD_MOD,ANEXO,NIV_MOD) AS CONTEO FROM ("
				+ cadSQL.toString() + ") b GROUP BY 2 ORDER BY 2";
		// System.out.println("N2: " + sql);
		query = em.createNativeQuery(sql);
		// System.out.println("MARCO 3: " + sql);
		return query.getResultList();
	}

	// INICIO - LISTA DE ENVIOS
	public List<Object[]> getCuentaMatricula2011Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODIGEL %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIVMOD, m.FECHAENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2011_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODIGEL LIKE '1501%' OR p.CODIGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODIGEL LIKE '1502%' OR p.CODIGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODIGEL LIKE '0701%' OR p.CODIGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getCuentaLocal2011Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODIGEL_PADRON %1$s ? ";
		cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2011_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODIGEL_PADRON LIKE '1501%' OR l.CODIGEL_PADRON LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODIGEL_PADRON LIKE '1502%' OR l.CODIGEL_PADRON LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODIGEL_PADRON LIKE '0701%' OR l.CODIGEL_PADRON LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append("GROUP BY 1 ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaLocal2012Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2012_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append("GROUP BY 1 ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaLocal2013Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2013_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append("GROUP BY 1 ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaLocal2014Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2014_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append("GROUP BY 1 ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaLocal2015Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2015_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append("GROUP BY 1 ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	/*
	 * public List<Object[]> getCuentaLocal2016Envios(String idUgel, String tipo) {
	 * cadSQL.delete(0, cadSQL.length()); Query query = null; String WHERE =
	 * "AND l.CODUGEL %1$s ? "; cadSQL.append(" SELECT l.CODLOCAL, ");
	 * cadSQL.append(" '' AS CEN_EDU,  "); cadSQL.
	 * append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN "
	 * ); cadSQL.append(" FROM local2016_cabecera l ");
	 * cadSQL.append(" WHERE l.ULTIMO  ");
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
	 * WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')"; } else if
	 * (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) { WHERE =
	 * " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')"; } else if
	 * (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) { WHERE =
	 * " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')"; } else if
	 * (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) { idUgel =
	 * idUgel.substring(0, 4) + "%"; WHERE = String.format(WHERE, "like"); } else {
	 * WHERE = String.format(WHERE, "="); } cadSQL.append(WHERE);
	 * cadSQL.append("GROUP BY 1 ORDER BY 1"); query =
	 * em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel); return
	 * query.getResultList(); }
	 */

	/* imendoza 20170324 inicio */

	public List<Object[]> getCuentaLocal2017Envios(String idUgel, String tipo) {
		logger.info(":: EnvioDocumentosFacade.getCuentaLocal2017Envios(idUgel, tipo) :: Starting execution...");
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		// cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" SELECT distinct l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2017_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		// cadSQL.append("GROUP BY 1 ORDER BY 1");
		cadSQL.append("ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("ENVIOS LOCAL : "+cadSQL);
		logger.info(":: EnvioDocumentosFacade.getCuentaLocal2017Envios(idUgel, tipo) :: Execution finish.");
		return query.getResultList();
	}

	public List<Object[]> getCuentaLocal2018Envios(String idUgel, String tipo) {
		logger.info(":: EnvioDocumentosFacade.getCuentaLocal2018Envios(idUgel, tipo) :: Starting execution...");
		logger.info("before cadSQL:" + cadSQL);
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		// cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" SELECT distinct l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2018_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		// cadSQL.append("GROUP BY 1 ORDER BY 1");
		cadSQL.append("ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		logger.info("  cadSQL.toString()=" + cadSQL.toString());
		// System.out.println("ENVIOS LOCAL : "+cadSQL);
		logger.info(":: EnvioDocumentosFacade.getCuentaLocal2018Envios(idUgel, tipo) :: Execution finish.");
		return query.getResultList();
	}

	public List<Object[]> getCuentaLocal2019Envios(String idUgel, String tipo) {
		logger.info(":: EnvioDocumentosFacade.getCuentaLocal2019Envios(idUgel, tipo) :: Starting execution...");
		logger.info("before cadSQL:" + cadSQL);
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		// cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" SELECT distinct l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM local2019_cabecera l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
//		} else {
//			WHERE = String.format(WHERE, "=");
//		}
		} else if (tipo.equals(Constantes.USUARIO_UGEL)) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		// cadSQL.append("GROUP BY 1 ORDER BY 1");
		cadSQL.append("ORDER BY 1");
//		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);

		if (!(idUgel.equals(USUARIO_DRE_LIMA_METRO) || idUgel.equals(USUARIO_DRE_LIMA_PROV)
				|| idUgel.equals(USUARIO_DRE_CALLAO))) {
			query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		} else {
			query = em.createNativeQuery(cadSQL.toString());
		}

		logger.info("  cadSQL.toString()=" + cadSQL.toString());
		// System.out.println("ENVIOS LOCAL : "+cadSQL);
//		logger.info(":: EnvioDocumentosFacade.getCuentaLocal2018Envios(idUgel, tipo) :: Execution finish.");
		return query.getResultList();
	}

	public List<Object[]> getCuentaFenNino2017Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND l.CODUGEL %1$s ? ";
		cadSQL.append(" SELECT l.CODLOCAL, ");
		cadSQL.append(" '' AS CEN_EDU,  ");
		cadSQL.append(" '' AS CODGEO, l.FECHA_ENVIO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM matricula2017_cedulad l ");
		cadSQL.append(" WHERE l.ULTIMO  ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1501%' OR l.CODUGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '1502%' OR l.CODUGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (l.CODUGEL LIKE '0701%' OR l.CODUGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) || idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append("GROUP BY 1 ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("ENVIOS LOCAL : "+cadSQL);
		return query.getResultList();
	}

	/* imendoza 20170324 fin */

	public List<Object[]> getCuentaResultado2011Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODIGEL %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2011_cabecera r ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(
				".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO AND SUBSTRING(r.NIV_MOD,1,1)=SUBSTRING(p.NIV_MOD,1,1) ");
		cadSQL.append(" WHERE p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODIGEL LIKE '1501%' OR r.CODIGEL LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODIGEL LIKE '1502%' OR r.CODIGEL LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODIGEL LIKE '0701%' OR r.CODIGEL LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaResultado2012Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2012_cabecera r ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(
				".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO AND SUBSTRING(r.NIV_MOD,1,1)=SUBSTRING(p.NIV_MOD,1,1) ");
		cadSQL.append(" WHERE p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaResultado2013Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2013_cabecera r ");
		// modificación para el listado de Omisos T0
		// cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON
		// r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO AND
		// SUBSTRING(r.NIV_MOD,1,1)=SUBSTRING(p.NIV_MOD,1,1) ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaResultado2014Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2014_cabecera r ");
		// modificación para el listado de Omisos T0
		// cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON
		// r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO AND
		// SUBSTRING(r.NIV_MOD,1,1)=SUBSTRING(p.NIV_MOD,1,1) ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();
	}

	public List<Object[]> getCuentaResultado2015Envios(String idUgel, String tipo, boolean observado) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2015_cabecera r ");
		// modificación para el listado de Omisos T0
		// cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON
		// r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO AND
		// SUBSTRING(r.NIV_MOD,1,1)=SUBSTRING(p.NIV_MOD,1,1) ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");
		if (observado) {
			cadSQL.append(" AND r.VALIDO = 1 ");
		}

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY ENVIO RESULTADOS: " + cadSQL);
		return query.getResultList();
	}

	/*
	 * public List<Object[]> getCuentaResultado2016Envios(String idUgel, String
	 * tipo, boolean observado) { cadSQL.delete(0, cadSQL.length()); Query query =
	 * null; String WHERE = "AND r.CODOOII %1$s ? ";
	 * cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
	 * cadSQL.
	 * append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, "
	 * ); cadSQL.
	 * append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  "
	 * ); cadSQL.append(" FROM  ").append(BD_ESTADISTICA).
	 * append(".resultado2016_cabecera r "); // modificación para el listado de
	 * Omisos T0 // cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron
	 * p ON // r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO AND //
	 * SUBSTRING(r.NIV_MOD,1,1)=SUBSTRING(p.NIV_MOD,1,1) ");
	 * cadSQL.append(" INNER JOIN ").append(BD_PADRON).
	 * append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO "); cadSQL.
	 * append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");
	 * if (observado) { cadSQL.append(" AND r.VALIDO = 1 "); }
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
	 * WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')"; } else if
	 * (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) { WHERE =
	 * " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')"; } else if
	 * (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) { WHERE =
	 * " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')"; } else if
	 * (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) { idUgel =
	 * idUgel.substring(0, 4) + "%"; WHERE = String.format(WHERE, "like"); } else {
	 * WHERE = String.format(WHERE, "="); } cadSQL.append(WHERE);
	 * cadSQL.append(" ORDER BY 1,2,3"); query =
	 * em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel); //
	 * System.out.println("QUERY ENVIO RESULTADOS: " + cadSQL); return
	 * query.getResultList(); }
	 */

	public List<Object[]> getCuentaResultado2017Envios(String idUgel, String tipo, boolean observado) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2017_cabecera r ");
		// modificación para el listado de Omisos T0
		// cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON
		// r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO AND
		// SUBSTRING(r.NIV_MOD,1,1)=SUBSTRING(p.NIV_MOD,1,1) ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");
		if (observado) {
			cadSQL.append(" AND r.VALIDO = 1 ");
		}

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY ENVIO RESULTADOS: " + cadSQL);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaResultado2018Envios(String idUgel, String tipo, boolean observado) {
		logger.info(
				":: EnvioDocumentosFacade.getCuentaResultado2018Envios :: Starting execution... (three parameters)");
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2018_cabecera r ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");
		if (observado) {
			cadSQL.append(" AND r.VALIDO = 1 ");
		}

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");

		logger.info("sentence SQL: " + cadSQL.toString());

		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);

		logger.info(":: EnvioDocumentosFacade.getCuentaResultado2018Envios :: Execution finish. (three parameters)");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaResultado2019Envios(String idUgel, String tipo, boolean observado) {
		logger.info(
				":: EnvioDocumentosFacade.getCuentaResultado2018Envios :: Starting execution... (three parameters)");
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2019_cabecera r ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO ");
		if (observado) {
			cadSQL.append(" AND r.VALIDO = 1 ");
		}

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");

		logger.info("sentence SQL: " + cadSQL.toString());

		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);

		logger.info(":: EnvioDocumentosFacade.getCuentaResultado2018Envios :: Execution finish. (three parameters)");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaResultado2017EnviosReqRecuperacion(String idUgel, String tipo, boolean observado) {
		logger.info(":: EnvioDocumentosFacade.getCuentaResultado2017EnviosReqRecuperacion :: Starting execution...");
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2017_cabecera r ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO AND r.varrec = '2' ");
		if (observado) {
			cadSQL.append(" AND r.VALIDO = 1 ");
		}

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");

		logger.info("Sentence SQL: " + cadSQL.toString());

		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);

		logger.info(":: EnvioDocumentosFacade.getCuentaResultado2017EnviosReqRecuperacion :: Execution finish.");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaResultado2018EnviosReqRecuperacion(String idUgel, String tipo, boolean observado) {
		logger.info(
				":: EnvioDocumentosFacade.getCuentaResultado2018EnviosReqRecuperacion :: Starting execution... (three parameters)");
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2018_cabecera r ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO AND r.varrec = '2' ");
		if (observado) {
			cadSQL.append(" AND r.VALIDO = 1 ");
		}

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");

		logger.info("sentence SQL: " + cadSQL.toString());

		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY ENVIO RESULTADOS: " + cadSQL);
		logger.info(
				":: EnvioDocumentosFacade.getCuentaResultado2018EnviosReqRecuperacion :: Execution finish. (three parameters)");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaResultado2019EnviosReqRecuperacion(String idUgel, String tipo, boolean observado) {
		logger.info(
				":: EnvioDocumentosFacade.getCuentaResultado2018EnviosReqRecuperacion :: Starting execution... (three parameters)");
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND r.CODOOII %1$s ? ";
		cadSQL.append(" SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD, r.FECHA_ENVIO,   ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN  ");
		cadSQL.append(" FROM  ").append(BD_ESTADISTICA).append(".resultado2019_cabecera r ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON r.COD_MOD=p.COD_MOD AND r.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND r.ULTIMO AND r.varrec = '2' ");
		if (observado) {
			cadSQL.append(" AND r.VALIDO = 1 ");
		}

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1501%' OR r.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '1502%' OR r.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (r.CODOOII LIKE '0701%' OR r.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");

		logger.info("sentence SQL: " + cadSQL.toString());

		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY ENVIO RESULTADOS: " + cadSQL);
		logger.info(
				":: EnvioDocumentosFacade.getCuentaResultado2018EnviosReqRecuperacion :: Execution finish. (three parameters)");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCuentaMatricula2012Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2012_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getCuentaMatricula2013Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2013_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getCuentaMatricula2014Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2014_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		return query.getResultList();

	}

	public List<Object[]> getCuentaMatricula2015Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2015_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY :: " + cadSQL);
		return query.getResultList();

	}
	/*
	 * public List<Object[]> getCuentaMatricula2016Envios(String idUgel, String
	 * tipo) {
	 * 
	 * cadSQL.delete(0, cadSQL.length()); Query query = null; String WHERE =
	 * "AND p.CODOOII %1$s ? ";
	 * cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
	 * cadSQL.
	 * append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, "
	 * ); cadSQL.
	 * append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN "
	 * ); cadSQL.append(" FROM ").append(BD_ESTADISTICA).
	 * append(".matricula2016_cabecera m ");
	 * cadSQL.append(" INNER JOIN ").append(BD_PADRON).
	 * append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO "); cadSQL.
	 * append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");
	 * 
	 * if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
	 * WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')"; } else if
	 * (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) { WHERE =
	 * " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')"; } else if
	 * (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) { WHERE =
	 * " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')"; } else if
	 * (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) { idUgel =
	 * idUgel.substring(0, 4) + "%"; WHERE = String.format(WHERE, "like"); } else {
	 * WHERE = String.format(WHERE, "="); } cadSQL.append(WHERE);
	 * cadSQL.append(" ORDER BY 1,2,3"); query =
	 * em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel); //
	 * System.out.println("QUERY :: " + cadSQL); return query.getResultList();
	 * 
	 * }
	 */

	public List<Object[]> getCuentaMatricula2017Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2017_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		// cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND p.sienvio = '2' AND
		// SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");
		cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY MATRICULA :: " + cadSQL);
		return query.getResultList();

	}

	public List<Object[]> getCuentaMatricula2018Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, "); // '' AS NIV_MOD,
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2018_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		// cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND p.sienvio = '2' AND
		// SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");
		cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY MATRICULA :: " + cadSQL);
		return query.getResultList();

	}

	public List<Object[]> getCuentaMatricula2019Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, "); // '' AS NIV_MOD,
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2019_cabecera m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		// cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND p.sienvio = '2' AND
		// SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");
		cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
//			WHERE = String.format(WHERE, "=");   
			WHERE = String.format(WHERE, "like"); // cambio werr
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY MATRICULA :: " + cadSQL);
		return query.getResultList();

	}

	public List<Object[]> getCuentaEvalSiagie2017Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(" SELECT DISTINCT m.COD_MOD,m.ANEXO,'' AS NIV_MOD, m.FECHA_ENVIO, ");
		cadSQL.append(" '' AS TIPOIIEE, '' AS CODLOCAL, '' AS CEN_EDU, '' AS FORMAS, '' AS NIV_MOD, ");
		cadSQL.append(" '' AS GES_DEP, '' AS CODGEO, '' AS DISTRITO, '' AS CEN_POB, '' AS DIR_CEN ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".matricula2017_cedulas m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO ");
		// cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND p.sienvio = '2' AND
		// SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");
		cadSQL.append(" WHERE p.mcenso='1' AND p.estado='1' AND SUBSTRING(p.tipoice,1,1)='1' AND m.ULTIMO ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1,2,3");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY EVAL SIAGIE: " + cadSQL);
		return query.getResultList();

	}

	public List<Object[]> getCuentaID2015Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "AND p.CODOOII %1$s ? ";
		cadSQL.append(
				" SELECT m.COD_IED,m.TIPO_IED AS TIPOIED,p.CEN_EDU AS NOMBIED,m.rzsocial AS RZSOCIAL,m.nroruc AS NRORUC,m.FECHA_ENVIO,p.dir_cen AS DIRECCION  ");
		cadSQL.append(" FROM ( ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_INI AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_INI IS NOT NULL AND ied.CMOD_INI <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_PRI AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_PRI IS NOT NULL AND ied.CMOD_PRI <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_EBA AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_EBA IS NOT NULL AND ied.CMOD_EBA <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_SEC AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_SEC IS NOT NULL AND ied.CMOD_SEC <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_ISP AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_ISP IS NOT NULL AND ied.CMOD_ISP <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_IST AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_IST IS NOT NULL AND ied.CMOD_IST <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_ESFA AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_ESFA IS NOT NULL AND ied.CMOD_ESFA <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_EBE AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_EBE IS NOT NULL AND ied.CMOD_EBE <> '') ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" (SELECT DISTINCT ied.CMOD_EPT AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO  FROM ")
				.append(BD_ESTADISTICA).append(".ie2015_establecimientos ied ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2015_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND ie.ULTIMO = 1 AND  ied.CMOD_EPT IS NOT NULL AND ied.CMOD_EPT <> '')) AS m ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON).append(".padron p ON m.COD_IED=p.COD_MOD  ");
		cadSQL.append(" WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ");

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%')";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%')";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%')";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = String.format(WHERE, "like");
		} else {
			WHERE = String.format(WHERE, "=");
		}
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1");
		query = em.createNativeQuery(cadSQL.toString()).setParameter(1, idUgel);
		// System.out.println("QUERY :: " + cadSQL);
		return query.getResultList();

	}

	public List<Object[]> getCuentaID2016Envios(String idUgel, String tipo) {

		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%') ";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = "AND p.CODOOII LIKE '" + idUgel + "'  ";
		} else {
			WHERE = "AND p.CODOOII = '" + idUgel + "'  ";
		}

		cadSQL.append(
				" SELECT m.COD_IED,m.TIPO_IED AS TIPOIED,m.CEN_EDU AS NOMBIED,m.rzsocial AS RZSOCIAL,m.nroruc AS NRORUC,m.FECHA_ENVIO,m.dir_cen AS DIRECCION   ");
		cadSQL.append(" FROM (  ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_INI AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_INI IS NOT NULL AND ied.CMOD_INI <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_INI=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_PRI AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_PRI IS NOT NULL AND ied.CMOD_PRI <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_PRI=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_EBAI AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen   ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_EBAI IS NOT NULL AND ied.CMOD_EBAI <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_EBAI=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_EBAA AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen   ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_EBAA IS NOT NULL AND ied.CMOD_EBAA <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_EBAA=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_SEC AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen   ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_SEC IS NOT NULL AND ied.CMOD_SEC <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_SEC=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_ISP AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen   ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_ISP IS NOT NULL AND ied.CMOD_ISP <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_ISP=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_IST AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen  ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_IST IS NOT NULL AND ied.CMOD_IST <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_IST=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION  ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_ESFA AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen  ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_ESFA IS NOT NULL AND ied.CMOD_ESFA <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_ESFA=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_EBE AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen   ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_EBE IS NOT NULL AND ied.CMOD_EBE <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_EBE=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" )  ");
		cadSQL.append(" UNION ");
		cadSQL.append(
				" ( SELECT DISTINCT ied.CMOD_ETP AS COD_IED,ie.TIPO_IED,ie.rzsocial,ie.nroruc,ie.FECHA_ENVIO,p.CEN_EDU,p.dir_cen   ");
		cadSQL.append(" FROM ").append(BD_ESTADISTICA).append(".ie2016_establecimientos ied  ");
		cadSQL.append(" INNER JOIN ").append(BD_ESTADISTICA).append(
				".ie2016_cabecera ie ON ied.CABECERA_IDENVIO = ie.ID_ENVIO AND  ied.CMOD_ETP IS NOT NULL AND ied.CMOD_ETP <> '' ");
		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron p ON ied.CMOD_ETP=p.COD_MOD   WHERE p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ")
				.append(WHERE).append(" GROUP BY 1 ");
		cadSQL.append(" ) ");
		cadSQL.append(" ) AS m  ");
		cadSQL.append(" ORDER BY 1 ");

		query = em.createNativeQuery(cadSQL.toString());
		// System.out.println("QUERY :: "+cadSQL);
		return query.getResultList();

	}

	public List<Object[]> getCuentaID2017Envios(String idUgel, String tipo) {
		cadSQL.delete(0, cadSQL.length());
		Query query = null;
		String WHERE = "";

		if (idUgel.equals(USUARIO_DRE_LIMA_METRO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1501%' OR p.CODOOII LIKE '1510%') ";
		} else if (idUgel.equals(USUARIO_DRE_LIMA_PROV) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '1502%' OR p.CODOOII LIKE '1520%') ";
		} else if (idUgel.equals(USUARIO_DRE_CALLAO) && tipo.equals(USUARIO_DRE)) {
			WHERE = " AND (p.CODOOII LIKE '0701%' OR p.CODOOII LIKE '0700%') ";
		} else if (tipo.equals(USUARIO_DRE) && idUgel.endsWith("00")) {
			idUgel = idUgel.substring(0, 4) + "%";
			WHERE = "AND p.CODOOII LIKE '" + idUgel + "'  ";
		} else {
			WHERE = "AND p.CODOOII = '" + idUgel + "'  ";
		}

		cadSQL.append(" SELECT DISTINCT p.cod_mod, ");
		cadSQL.append(" ic.NOMB_IED AS NOMBIED, ");
		cadSQL.append(" ic.RZSOCIAL AS RZSOCIAL,");
		cadSQL.append(" ic.NRORUC AS NRORUC,");
		cadSQL.append(" ic.FECHA_ENVIO AS FECHA_ENVIO,");
		cadSQL.append(" p.dir_cen AS DIRECCION ");
		cadSQL.append(" FROM estadistica.ie2017_cabecera ic ");
		cadSQL.append(" LEFT JOIN padron.padron p ON ic.COD_IED = p.codinst ");
		// cadSQL.append(" WHERE ic.ULTIMO ='1' AND p.mcenso='1' AND p.sienvio = '2' AND
		// SUBSTRING(p.tipoice,1,1)='1' ");
		cadSQL.append(" WHERE ic.ULTIMO ='1' AND p.mcenso='1' AND SUBSTRING(p.tipoice,1,1)='1' ");
		cadSQL.append(WHERE);
		cadSQL.append(" ORDER BY 1");

		query = em.createNativeQuery(cadSQL.toString());

		return query.getResultList();
	}
	// FINAL - LISTA DE ENVIOS

	public Resultado2011Cabecera getResultado2011(Long idEnvio) {
		return em.find(Resultado2011Cabecera.class, idEnvio);
	}

	public Resultado2011Cabecera getResultado2011Envio(String codMod, String anexo, String nivMod) {
		Query q = em.createQuery(
				"SELECT r FROM Resultado2011Cabecera r WHERE r.codMod = :codMod AND r.anexo = :anexo AND r.nivMod = :nivMod AND r.ultimo = :ultimo");
		q.setParameter("codMod", codMod);
		q.setParameter("anexo", anexo);
		q.setParameter("nivMod", nivMod);
		q.setParameter("ultimo", true);
		try {
			return (Resultado2011Cabecera) q.getSingleResult();
		} catch (javax.persistence.NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Object[]> getResumenMatriculaByUgel(String codIgel, String ced) {
		Query query = null;
		String WHERE = "";
		cadSQL.delete(0, cadSQL.length());
		cadSQL.append(
				" SELECT M.COD_IGEL,M.ID_UBIGEO,M.PROV,M.DIST,M.CEN_EDU,M.COD_MOD,M.ANEXO,M.NIVMOD,P.ges_dep,P.estado,");
		if (ced.equals("c01a") || ced.equals("c02a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,M.3H,M.3M,M.4H,M.4M,M.5H,M.5M,");
			cadSQL.append(" M.6H,M.6M,M.7H,M.7M,M.8H,M.8M,");
			cadSQL.append(" S.1,S.2,S.3,S.4,S.5,S.6,");
		} else if (ced.equals("c03a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,M.3H,M.3M,M.4H,M.4M,M.5H,M.5M,");
			cadSQL.append(" M.6H,M.6M,");
			cadSQL.append(" S.1,S.2,S.3,S.4,S.5,S.6,");
		} else if (ced.equals("c04a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,M.3H,M.3M,M.4H,M.4M,M.5H,M.5M,");
			cadSQL.append(" M.6H,M.6M,M.7H,M.7M,M.8H,M.8M,M.9H,M.9M,M.10H,M.10M,M.11H,M.11M,");
			cadSQL.append(" S.1,S.2,S.3,S.4,S.5,S.6,S.7,S.8,S.9,S.10,S.11,");
		} else if (ced.equals("c05a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,M.3H,M.3M,M.4H,M.4M,M.5H,M.5M,");
			cadSQL.append(" M.6H,M.6M,M.7H,M.7M,M.8H,M.8M,M.9H,M.9M,M.10H,M.10M,");
			cadSQL.append(" S.1,S.2,S.3,S.4,S.5,S.6,S.7,S.8,S.9,S.10,");
		} else if (ced.equals("c06a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,M.3H,M.3M,M.4H,M.4M,M.5H,M.5M,M.6H,M.6M,");
			cadSQL.append(" M.7H,M.7M,M.8H,M.8M,M.9H,M.9M,M.10H,M.10M,M.11H,M.11M,M.12H,M.12M,");
			cadSQL.append(" M.13H,M.13M,M.14H,M.14M,M.15H,M.15M,M.16H,M.16M,");
			cadSQL.append(" S.1,S.2,S.3,S.4,S.5,S.6,S.7,S.8,S.9,S.10,S.11,S.12,S.13,S.14,S.15,S.16,");
		} else if (ced.equals("c07a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,M.3H,M.3M,M.4H,M.4M,M.5H,M.5M,M.6H,M.6M,");
			cadSQL.append(" M.7H,M.7M,M.8H,M.8M,M.9H,M.9M,M.10H,M.10M,M.11H,M.11M,M.12H,M.12M,");
			cadSQL.append(
					" M.13H,M.13M,M.14H,M.14M,M.15H,M.15M,M.16H,M.16M,M.17H,M.17M,M.18H,M.18M,M.19H,M.19M,M.20H,M.20M,");
			cadSQL.append(" S.1,S.2,S.3,S.4,S.5,S.6,S.7,S.8,S.9,S.10,S.11,S.12,S.13,S.14,S.15,S.16,");
		} else if (ced.equals("c08a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,M.3H,M.3M,M.4H,M.4M,M.5H,M.5M,M.6H,M.6M,");
			cadSQL.append(" M.7H,M.7M,M.8H,M.8M,M.9H,M.9M,M.10H,M.10M,M.11H,M.11M,M.12H,M.12M,");
			cadSQL.append(" M.13H,M.13M,M.14H,M.14M,M.15H,M.15M,M.16H,M.16M,M.17H,M.17M,M.18H,M.18M,");
			cadSQL.append(" M.19H,M.19M,M.20H,M.20M,M.21H,M.21M,M.22H,M.22M,M.23H,M.23M,M.24H,M.24M,");
			cadSQL.append(" M.25H,M.25M,M.26H,M.26M,M.27H,M.27M,M.28H,M.28M,M.29H,M.29M,M.30H,M.30M,");
			cadSQL.append(" M.31H,M.31M,M.32H,M.32M,M.33H,M.33M,M.34H,M.34M,M.35H,M.35M,");
			cadSQL.append(" S.1,S.2,S.3,S.4,S.5,S.6,S.7,S.8,S.9,S.10,S.11,S.12,S.13,S.14,S.15,S.16,S.17,S.18,");
			cadSQL.append(" S.19,S.20,S.21,S.22,S.23,S.24,S.25,S.26,S.27,S.28,S.29,S.30,S.31,");
		} else if (ced.equals("c09a")) {
			cadSQL.append(" M.1H,M.1M,M.2H,M.2M,");
			cadSQL.append(" S.1,S.2,");
		}
		cadSQL.append(" (D.PERSONAL_01 - PERSONAL_03),PERSONAL_02,PERSONAL_03");
		cadSQL.append(" FROM estadistica.VISTA_REP_MATRICULA2011 M  ");
		cadSQL.append(" INNER JOIN  ").append(BD_ESTADISTICA).append(
				".VISTA_REP_DOCENTES2011 D ON (D.COD_MOD = M.COD_MOD AND D.ANEXO=M.ANEXO AND D.NIVMOD=M.NIVMOD) ");
		cadSQL.append(" LEFT JOIN  ").append(BD_ESTADISTICA).append(
				".VISTA_REP_SECCIONES2011 S ON(S.COD_MOD = M.COD_MOD AND S.ANEXO=M.ANEXO AND S.NIVMOD=M.NIVMOD) ");

		cadSQL.append(" INNER JOIN ").append(BD_PADRON)
				.append(".padron P ON(P.COD_MOD = M.COD_MOD AND P.ANEXO=M.ANEXO) ");

		if (codIgel.equals(USUARIO_DRE_LIMA_METRO)) {
			WHERE = " (M.COD_IGEL LIKE '1501%' OR M.COD_IGEL LIKE '1510%')";
		} else if (codIgel.equals(USUARIO_DRE_LIMA_PROV)) {
			WHERE = " (M.COD_IGEL LIKE '1502%' OR M.COD_IGEL  LIKE '1520%')";
		} else if (codIgel.equals(USUARIO_DRE_CALLAO)) {
			WHERE = " (M.COD_IGEL LIKE '0701%' OR M.COD_IGEL LIKE '0700%')";
		} else if (codIgel.endsWith("00")) {

			WHERE = String.format("M.COD_IGEL LIKE '%s'", codIgel.substring(0, 4) + "%");
		} else {
			WHERE = String.format("M.COD_IGEL= '%s' ", codIgel);
			;
		}

		cadSQL.append(" WHERE ");
		cadSQL.append(WHERE);
		cadSQL.append(" AND M.NROCED=?numCed ORDER BY M.COD_IGEL");

		query = em.createNativeQuery(cadSQL.toString());
		// query.setParameter("codIgel", codIgel);
		query.setParameter("numCed", ced);

		List<Object[]> listResult = query.getResultList();

		return listResult;

	}

	public List<Object[]> getResumeMatricula2012ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.CEN_POB,m.NOMDIR,m.TELEFONO,m.EMAIL");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iM = rangoRepMat[numCed - 1][0];
		int iS = rangoRepMat[numCed - 1][1];
		int iP = rangoRepMat[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}
		cadSQL.append(String.format(" FROM v_matricula_%1$s m ", ced.toUpperCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toUpperCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toUpperCase()));
		}
		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));

		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeMatricula2013ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.NOMDIR,m.TELEFONO,m.EMAIL,m.TIPOPROG");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iM = rangoRepMat2013[numCed - 1][0];
		int iS = rangoRepMat2013[numCed - 1][1];
		int iP = rangoRepMat2013[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}
		cadSQL.append(String.format(" FROM v_matricula13_%1$s m ", ced.toUpperCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion13_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toUpperCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal13_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toUpperCase()));
		}
		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));

		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getResumeMatricula2014ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.NOMDIR,m.TELEFONO,m.EMAIL,m.TIPOPROG");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iM = rangoRepMat2014[numCed - 1][0];
		int iS = rangoRepMat2014[numCed - 1][1];
		int iP = rangoRepMat2014[numCed - 1][2];
		int iPR = rangoRepMat2014[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}
		for (int i = 1; i <= iPR; i++) {
			cadSQL.append(String.format(",r.PER%1$d", i));
		}
		cadSQL.append(String.format(" FROM v_matricula14_%1$s m ", ced.toUpperCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion14_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toUpperCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal14_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toUpperCase()));
		}
		if (iPR > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal14p_%1$s r ON (m.COD_MOD=r.COD_MOD AND m.ANEXO=r.ANEXO) ",
					ced.toUpperCase()));
		}

		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));

		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getResumeMatricula2015ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.TIPOPROG");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iM = rangoRepMat2015[numCed - 1][0];
		int iS = rangoRepMat2015[numCed - 1][1];
		int iP = rangoRepMat2015[numCed - 1][2];
		// int iPR = rangoRepMat2015[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}
		/*
		 * for (int i = 1; i <= iPR; i++) { cadSQL.append(String.format(",r.PER%1$d",
		 * i)); }
		 */
		cadSQL.append(String.format(" FROM v_matricula15_%1$s m ", ced.toUpperCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion15_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toUpperCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal15_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toUpperCase()));
		}
		/*
		 * if (iPR > 0) { cadSQL.append(String.
		 * format("LEFT JOIN v_personal14p_%1$s r ON (m.COD_MOD=r.COD_MOD AND m.ANEXO=r.ANEXO) "
		 * , ced.toUpperCase())); }
		 */

		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));

		// System.out.println("QUERY : " + cadSQL.toString());
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeMatricula2017ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.TIPOPROG");
		int numCed = 0;
		if (ced.equalsIgnoreCase(cedula4AA)) {
			numCed = 10;
		} else if (ced.equalsIgnoreCase(cedula3AP)) {
			numCed = 11;
		} else if (ced.equalsIgnoreCase(cedula3AS)) {
			numCed = 12;
		} else if (ced.equalsIgnoreCase(cedula8AI)) {
			numCed = 13;
		} else if (ced.equalsIgnoreCase(cedula8AP)) {
			numCed = 14;
		} else {
			numCed = Integer.parseInt(ced.substring(0, 1));
		}

		int iM = rangoRepMat2017[numCed - 1][0];
		int iS = rangoRepMat2017[numCed - 1][1];
		int iP = rangoRepMat2017[numCed - 1][2];
		// int iPR = rangoRepMat2015[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}
		/*
		 * for (int i = 1; i <= iPR; i++) { cadSQL.append(String.format(",r.PER%1$d",
		 * i)); }
		 */
		cadSQL.append(String.format(" FROM v_matricula17_%1$s m ", ced.toLowerCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion17_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toLowerCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal17_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toLowerCase()));
		}
		/*
		 * if (iPR > 0) { cadSQL.append(String.
		 * format("LEFT JOIN v_personal14p_%1$s r ON (m.COD_MOD=r.COD_MOD AND m.ANEXO=r.ANEXO) "
		 * , ced.toUpperCase())); }
		 */

		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));

		// System.out.println("QUERY : " + cadSQL.toString());
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeMatricula2018ByUgel(String codUgel, String ced) {
		logger.info(":: EnvioDocumentosFacade.getResumeMatricula2018ByUgel :: Starting execution...");
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.TIPOPROG");
		int numCed = 0;
		if (ced.equalsIgnoreCase(cedula4AA)) {
			numCed = 10;
		} else if (ced.equalsIgnoreCase(cedula3AP)) {
			numCed = 11;
		} else if (ced.equalsIgnoreCase(cedula3AS)) {
			numCed = 12;
		} else if (ced.equalsIgnoreCase(cedula8AI)) {
			numCed = 13;
		} else if (ced.equalsIgnoreCase(cedula8AP)) {
			numCed = 14;
		} else {
			numCed = Integer.parseInt(ced.substring(0, 1));
		}

		int iM = rangoRepMat2018[numCed - 1][0];
		int iS = rangoRepMat2018[numCed - 1][1];
		int iP = rangoRepMat2018[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}
		cadSQL.append(String.format(" FROM v_matricula18_%1$s m ", ced.toLowerCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion18_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toLowerCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal18_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toLowerCase()));
		}
		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));

		logger.info(" getResumeMatricula2018ByUgel SQL: " + cadSQL.toString());

		query = em.createNativeQuery(cadSQL.toString());

		logger.info(":: EnvioDocumentosFacade.getResumeMatricula2018ByUgel :: Execution finish.");
		return query.getResultList();
	}

	// update
	public List<Object[]> getResumeMatricula2019ByUgel(String codUgel, String ced) {
		logger.info(":: EnvioDocumentosFacade.getResumeMatricula2019ByUgel :: Starting execution...");
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.TIPOPROG");
		int numCed = 0;
		if (ced.equalsIgnoreCase(cedula4AA)) {
			numCed = 10;
		} else if (ced.equalsIgnoreCase(cedula3AP)) {
			numCed = 11;
		} else if (ced.equalsIgnoreCase(cedula3AS)) {
			numCed = 12;
		} else if (ced.equalsIgnoreCase(cedula8AI)) {
			numCed = 13;
		} else if (ced.equalsIgnoreCase(cedula8AP)) {
			numCed = 14;
		} else {
			numCed = Integer.parseInt(ced.substring(0, 1));
		}

		int iM = rangoRepMat2019[numCed - 1][0];
		int iS = rangoRepMat2019[numCed - 1][1];
		int iP = rangoRepMat2019[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}
		cadSQL.append(String.format(" FROM v_matricula19_%1$s m ", ced.toLowerCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion19_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toLowerCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal19_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toLowerCase()));
		}
		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));

		logger.info(" getResumeMatricula2019ByUgel SQL: " + cadSQL.toString());

		query = em.createNativeQuery(cadSQL.toString());

		logger.info(":: EnvioDocumentosFacade.getResumeMatricula2019ByUgel :: Execution finish.");
		return query.getResultList();
	}

	public List<Object[]> getResumeMatricula2016ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT m.COD_MOD,m.ANEXO,m.NIV_MOD,m.TIPOPROG");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		if (ced.equalsIgnoreCase(cedula4AA)) {
			numCed = 10;
		}

		int iM = rangoRepMat2016[numCed - 1][0];
		int iS = rangoRepMat2016[numCed - 1][1];
		int iP = rangoRepMat2016[numCed - 1][2];

		for (int i = 1; i <= iM; i++) {
			cadSQL.append(String.format(",m.MATR%1$d", i));
		}
		for (int i = 1; i <= iS; i++) {
			cadSQL.append(String.format(",s.SECC%1$d", i));
		}
		for (int i = 1; i <= iP; i++) {
			cadSQL.append(String.format(",p.PER%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_matricula16_%1$s m ", ced.toLowerCase()));
		if (iS > 0) {
			cadSQL.append(String.format("LEFT JOIN v_seccion16_%1$s s ON (m.COD_MOD=s.COD_MOD AND m.ANEXO=s.ANEXO) ",
					ced.toLowerCase()));
		}
		if (iP > 0) {
			cadSQL.append(String.format("LEFT JOIN v_personal16_%1$s p ON (m.COD_MOD=p.COD_MOD AND m.ANEXO=p.ANEXO) ",
					ced.toLowerCase()));
		}

		cadSQL.append(String.format("WHERE m.CODUGEL LIKE '%1$s'", codUgel));
		logger.info(":: getResumeMatricula2018ByUgel sentence SQL: " + cadSQL.toString());
		query = em.createNativeQuery(cadSQL.toString());
		logger.info(":: EnvioDocumentosFacade.getResumeMatricula2018ByUgel :: Execution finish.");
		return query.getResultList();
	}

	public List<Object[]> getResumeLocal2013ByUgel(String codUgel) {

		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append(
				"SELECT a.CODUGEL, a.CODUGEL, a.CODLOCAL, a.LOC1, a.LOC2, a.LOC3, a.LOC4, a.LOC5, a.LOC6, a.LOC7, a.LOC8, a.LOC9, a.LOC10");
		cadSQL.append(",a.LOC11, a.LOC12, a.LOC13, a.LOC15, a.LOC16, a.LOC17, a.LOC19, a.LOC20");
		cadSQL.append(",a.LOC21, a.LOC22, a.LOC23, a.LOC24, a.LOC25, a.LOC26, a.LOC27, a.LOC28, a.LOC29, a.LOC30");
		cadSQL.append(",a.LOC31, a.LOC32, a.LOC33, a.LOC34, a.LOC35, a.LOC36, a.LOC37, a.LOC38, a.LOC39, a.LOC40");
		cadSQL.append(",a.LOC41, a.LOC42, a.LOC43, a.LOC44, a.LOC45, a.LOC46, a.LOC47, a.LOC48, a.LOC49, a.LOC50");
		cadSQL.append(",a.LOC51, a.LOC52, a.LOC53, a.LOC54, a.LOC55, a.LOC56, a.LOC57, a.LOC58, a.LOC59, a.LOC60");
		cadSQL.append(",a.LOC61, a.LOC62, a.LOC63, a.LOC64, a.LOC65, a.LOC66, a.LOC67, a.LOC68, a.LOC69, a.LOC70");
		cadSQL.append(",a.LOC71, a.LOC72, a.LOC73, a.LOC74, a.LOC75, a.LOC76, a.LOC77, a.LOC78, a.LOC79, a.LOC80");
		cadSQL.append(",a.LOC81, a.LOC82, a.LOC83, a.LOC84, a.LOC85, a.LOC86, a.LOC87, a.LOC88, a.LOC89, a.LOC90");
		cadSQL.append(",a.LOC91, a.LOC92, a.LOC93, a.LOC94, a.LOC95, a.LOC96, a.LOC97, a.LOC98, a.LOC99, a.LOC100");
		cadSQL.append(
				",a.LOC101, a.LOC102, a.LOC103, a.LOC104, a.LOC105, a.LOC106, a.LOC107, a.LOC108, a.LOC109, a.LOC110");
		cadSQL.append(
				",a.LOC111, a.LOC112, a.LOC113, a.LOC114, a.LOC115, a.LOC116, a.LOC117, a.LOC118, a.LOC119, a.LOC120");
		cadSQL.append(
				",a.LOC121, a.LOC122, b.AULAT,b.AULA1,b.AULA2,b.AULA3, b.ESP2, b.ESP3, b.ESP4, b.ESP5, b.ESP6, b.ESP7");
		cadSQL.append(
				",b.ESP8, b.ESP9, b.ESP10, b.ESP11, b.ESP12, b.ESP13, b.ESP14, b.ESP15, b.ESP16, b.ESP17, b.ESP18, b.ESP19");
		cadSQL.append(" FROM v_local2013_c11 a");
		cadSQL.append(" INNER JOIN v_local2013_aulas_c11 b");
		cadSQL.append(" ON a.CODLOCAL=b.CODLOCAL");
		cadSQL.append(String.format(" WHERE a.CODUGEL LIKE '%1$s'", codUgel));

		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();

	}

	public List<Object[]> getResumeLocal2014ByUgel(String codUgel) {

		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append(
				"SELECT a.CODUGEL, a.CODUGEL, a.CODLOCAL, a.LOC1, a.LOC2, a.LOC3, a.LOC4, a.LOC5, a.LOC6, a.LOC7, a.LOC8, a.LOC9, a.LOC10");
		cadSQL.append(",a.LOC11, a.LOC12, a.LOC13, a.LOC15, a.LOC16, a.LOC17, a.LOC19, a.LOC20");
		cadSQL.append(",a.LOC21, a.LOC22, a.LOC23, a.LOC24, a.LOC25, a.LOC26, a.LOC27, a.LOC28, a.LOC29, a.LOC30");
		cadSQL.append(",a.LOC31, a.LOC32, a.LOC33, a.LOC34, a.LOC35, a.LOC36, a.LOC37, a.LOC38, a.LOC39, a.LOC40");
		cadSQL.append(",a.LOC41, a.LOC42, a.LOC43, a.LOC44, a.LOC45, a.LOC46, a.LOC47, a.LOC48, a.LOC49, a.LOC50");
		cadSQL.append(",a.LOC51, a.LOC52, a.LOC53, a.LOC54, a.LOC55, a.LOC56, a.LOC57, a.LOC58, a.LOC59, a.LOC60");
		cadSQL.append(",a.LOC61, a.LOC62, a.LOC63, a.LOC64, a.LOC65, a.LOC66, a.LOC67, a.LOC68, a.LOC69, a.LOC70");
		cadSQL.append(",a.LOC71, a.LOC72, a.LOC73, a.LOC74, a.LOC75, a.LOC76, a.LOC77, a.LOC78, a.LOC79, a.LOC80");
		cadSQL.append(",a.LOC81, a.LOC82, a.LOC83, a.LOC84, a.LOC85, a.LOC86, a.LOC87, a.LOC88, a.LOC89, a.LOC90");
		cadSQL.append(",a.LOC91, a.LOC92, a.LOC93, a.LOC94, a.LOC95, a.LOC96, a.LOC97, a.LOC98, a.LOC99, a.LOC100");
		cadSQL.append(
				",a.LOC101, a.LOC102, a.LOC103, a.LOC104, a.LOC105, a.LOC106, a.LOC107, a.LOC108, a.LOC109, a.LOC110");
		cadSQL.append(
				",a.LOC111, a.LOC112, a.LOC113, a.LOC114, a.LOC115, a.LOC116, a.LOC117, a.LOC118, a.LOC119, a.LOC120");
		cadSQL.append(
				",a.LOC121, a.LOC122, a.LOC123, a.LOC124, a.LOC125, b.ESP1, b.ESP2, b.ESP3, b.ESP4, b.ESP5, b.ESP6, b.ESP7");
		cadSQL.append(
				",b.ESP8, b.ESP9, b.ESP10, b.ESP11, b.ESP12, b.ESP13, b.ESP14, b.ESP15, c.AULA1, c.AULA2, c.AULA3");
		cadSQL.append(" FROM v_local2014_c11 a");
		cadSQL.append(" LEFT JOIN v_local2014_aulas_c11 b");
		cadSQL.append(" ON a.CODLOCAL=b.CODLOCAL");
		cadSQL.append(" LEFT JOIN v_local2014_aulasp_c11 c");
		cadSQL.append(" ON a.CODLOCAL=c.CODLOCAL");
		cadSQL.append(String.format(" WHERE a.CODUGEL LIKE '%1$s'", codUgel));

		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();

	}

	public List<Object[]> getResumeLocal2015ByUgel(String codUgel) {

		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("select a.*");
		cadSQL.append(",b.ESP1,b.ESP2,b.ESP3,b.ESP4,b.ESP5,b.ESP6,b.ESP7,b.ESP8");
		cadSQL.append(",b.ESP9,b.ESP10,b.ESP11,b.ESP12,b.ESP13,b.ESP14");
		cadSQL.append(",c.AULA1,c.AULA2,c.AULA3");
		cadSQL.append(" FROM v_local2015_c11 a");
		cadSQL.append(" LEFT JOIN v_local2015_aulas_c11 b");
		cadSQL.append(" ON a.CODLOCAL=b.CODLOCAL");
		cadSQL.append(" LEFT JOIN v_local2015_aulasp_c11 c");
		cadSQL.append(" ON a.CODLOCAL=c.CODLOCAL");
		cadSQL.append(String.format(" WHERE a.CODUGEL LIKE '%1$s'", codUgel));

		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();

	}

	public List<Object[]> getResumeLocal2016ByUgel(String codUgel) {

		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("select a.*");
		cadSQL.append(
				",b.LOC100,b.LOC101,b.LOC102,b.LOC103,b.LOC104,b.LOC105,b.LOC106,b.LOC107,b.LOC108,b.LOC109,b.LOC110,b.LOC111,b.LOC112,b.LOC113,b.LOC114,b.LOC115,b.LOC116,b.LOC117,b.LOC118,b.LOC119,b.LOC120,b.LOC121,b.LOC122,b.LOC123");
		cadSQL.append(",c.LOC124,c.LOC125,c.LOC126");
		cadSQL.append(" FROM v_local2016_c11 a");
		cadSQL.append(" LEFT JOIN v_local2016_aulas_c11 b");
		cadSQL.append(" ON a.CODLOCAL=b.CODLOCAL");
		cadSQL.append(" LEFT JOIN v_local2016_aulasp_c11 c");
		cadSQL.append(" ON a.CODLOCAL=c.CODLOCAL");
		cadSQL.append(String.format(" WHERE a.CODUGEL LIKE '%1$s'", codUgel));

		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();

	}

	public List<Object[]> getResumeLocal2017ByUgel(String codUgel) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);
		cadSQL.append("select a.* ");
		cadSQL.append(",b.LOC96,b.LOC97,b.LOC98,b.LOC99,b.LOC100 ");
		cadSQL.append(",b.LOC101,b.LOC102,b.LOC103,b.LOC104,b.LOC105 ");
		cadSQL.append(",b.LOC106,b.LOC107 ");
		cadSQL.append(",c.LOC108,c.LOC109,c.LOC110,c.LOC111,c.LOC112 ");
		cadSQL.append(",c.LOC113,c.LOC114,c.LOC115,c.LOC116,c.LOC117 ");
		cadSQL.append(",c.LOC118,c.LOC119,c.LOC120,c.LOC121,c.LOC122 ");
		cadSQL.append(",c.LOC123,c.LOC124,c.LOC125,c.LOC126,c.LOC127 ");
		cadSQL.append(",c.LOC128,c.LOC129,c.LOC130,c.LOC131,c.LOC132 ");
		cadSQL.append(",c.LOC133,c.LOC134,c.LOC135,c.LOC136,c.LOC137 ");
		cadSQL.append(",c.LOC138,c.LOC139,c.LOC140,c.LOC141 ");
		cadSQL.append("FROM v_local2017_c11 a ");
		cadSQL.append("LEFT JOIN v_local2017_aulasp_c11 b ");
		cadSQL.append("ON a.CODLOCAL=b.CODLOCAL ");
		cadSQL.append("LEFT JOIN v_local2017_aulas_c11 c ");
		cadSQL.append("ON a.CODLOCAL=c.CODLOCAL ");
		cadSQL.append(String.format(" WHERE a.CODUGEL LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeLocal2018ByUgel(String codUgel) {
		logger.info(":: EnvioDocumentosFacade.getResumeLocal2018ByUgel :: Starting execution...");
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);
		// cadSQL.append("select a.* ");
		cadSQL.append("select a.codugel, a.codlocal, ");
		cadSQL.append("b.LOC96,b.LOC97,b.LOC98,b.LOC99,b.LOC100 ");
		cadSQL.append(",b.LOC101,b.LOC102,b.LOC103,b.LOC104,b.LOC105 ");
		cadSQL.append(",b.LOC106,b.LOC107 ");
		cadSQL.append(",c.LOC108,c.LOC109,c.LOC110,c.LOC111,c.LOC112 ");
		cadSQL.append(",c.LOC113,c.LOC114,c.LOC115,c.LOC116,c.LOC117 ");
		cadSQL.append(",c.LOC118,c.LOC119,c.LOC120,c.LOC121,c.LOC122 ");
		cadSQL.append(",c.LOC123,c.LOC124,c.LOC125,c.LOC126,c.LOC127 ");
		cadSQL.append(",c.LOC128,c.LOC129,c.LOC130,c.LOC131,c.LOC132 ");
		cadSQL.append(",c.LOC133,c.LOC134,c.LOC135,c.LOC136,c.LOC137 ");
		cadSQL.append(",c.LOC138,c.LOC139 ");
		cadSQL.append("FROM v_local2018_c11 a ");
		cadSQL.append("LEFT JOIN v_local2018_aulasp_c11 b ");
		cadSQL.append("ON a.CODLOCAL=b.CODLOCAL ");
		cadSQL.append("LEFT JOIN v_local2018_aulas_c11 c ");
		cadSQL.append("ON a.CODLOCAL=c.CODLOCAL ");
		cadSQL.append(String.format(" WHERE a.CODUGEL LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		logger.info(":: EnvioDocumentosFacade.getResumeLocal2018ByUgel :: Execution finish.");
		return query.getResultList();
	}

	public List<Object[]> getResumeLocal2019ByUgel(String codUgel) {
		logger.info(":: EnvioDocumentosFacade.getResumeLocal2019ByUgel :: Starting execution...");
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);
		cadSQL.append("select a.* ,");
		// cadSQL.append("select a.codugel, a.codlocal, ");
		cadSQL.append("b.LOC96,b.LOC97,b.LOC98,b.LOC99,b.LOC100 ");
		cadSQL.append(",b.LOC101,b.LOC102,b.LOC103,b.LOC104,b.LOC105 ");
		cadSQL.append(",b.LOC106,b.LOC107 ");
		/*
		 * cadSQL.append(",c.LOC108,c.LOC109,c.LOC110,c.LOC111,c.LOC112 ");
		 * cadSQL.append(",c.LOC113,c.LOC114,c.LOC115,c.LOC116,c.LOC117 ");
		 * cadSQL.append(",c.LOC118,c.LOC119,c.LOC120,c.LOC121,c.LOC122 ");
		 * cadSQL.append(",c.LOC123,c.LOC124,c.LOC125,c.LOC126,c.LOC127 ");
		 * cadSQL.append(",c.LOC128,c.LOC129,c.LOC130,c.LOC131,c.LOC132 ");
		 * cadSQL.append(",c.LOC133,c.LOC134,c.LOC135,c.LOC136,c.LOC137 ");
		 * cadSQL.append(",c.LOC138,c.LOC139 ");
		 */
		cadSQL.append("FROM v_local2019_c11 a ");
		cadSQL.append("LEFT JOIN v_local2019_aulasp_c11 b ");
		cadSQL.append("ON a.CODLOCAL=b.CODLOCAL ");
		// cadSQL.append("LEFT JOIN v_local2019_aulas_c11 c ");
		// cadSQL.append("ON a.CODLOCAL=c.CODLOCAL ");
		cadSQL.append(String.format(" WHERE a.CODUGEL LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		logger.info(":: EnvioDocumentosFacade.getResumeLocal2019ByUgel :: Execution finish.");
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2011ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iR = rangoRepRes[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODIGEL LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2012ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iR = rangoRepRes2012[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2012_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2013ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iR = rangoRepRes2013[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2013_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2014ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iR = rangoRepRes2014[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2014_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2015ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));
		int iR = rangoRepRes2015[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2015_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2016ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));

		if (ced.equalsIgnoreCase(cedula4BA)) {
			numCed = 10;
		}

		int iR = rangoRepRes2016[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2016_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2017ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));

		if (ced.equalsIgnoreCase(cedula3BS)) {
			numCed = 10;
		} else if (ced.equalsIgnoreCase(cedula4BA)) {
			numCed = 11;
		} else if (ced.equalsIgnoreCase(cedula8BP)) {
			numCed = 12;
		}

		int iR = rangoRepRes2017[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2017_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2018ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));

		if (ced.equalsIgnoreCase(cedula3BS)) {
			numCed = 10;
		} else if (ced.equalsIgnoreCase(cedula4BA)) {
			numCed = 11;
		} else if (ced.equalsIgnoreCase(cedula8BP)) {
			numCed = 12;
		}

		int iR = rangoRepRes2018[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2018_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	public List<Object[]> getResumeResultado2019ByUgel(String codUgel, String ced) {
		Query query;
		cadSQL.delete(0, cadSQL.length());
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		cadSQL.append("SELECT r.COD_MOD,r.ANEXO,r.NIV_MOD");
		int numCed = Integer.parseInt(ced.substring(0, 1));

		if (ced.equalsIgnoreCase(cedula3BS)) {
			numCed = 10;
		} else if (ced.equalsIgnoreCase(cedula4BA)) {
			numCed = 11;
		} else if (ced.equalsIgnoreCase(cedula8BP)) {
			numCed = 12;
		}

		int iR = rangoRepRes2019[numCed - 1];

		for (int i = 1; i <= iR; i++) {
			cadSQL.append(String.format(",r.DATO%1$d", i));
		}

		cadSQL.append(String.format(" FROM v_resultado2019_%1$s r ", ced.toUpperCase()));

		cadSQL.append(String.format("WHERE r.CODOOII LIKE '%1$s'", codUgel));
		query = em.createNativeQuery(cadSQL.toString());
		return query.getResultList();
	}

	// imendoza
	public List<Object[]> reporteDetallado2016ByUgel(String codUgel, String cuadro, String nroCed) {
		StringBuilder sbQuery = new StringBuilder();

		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);

		sbQuery.append("select ");
//        if (cuadro != null &&
//                ((cuadro.equals(cuadro202) && !(nroCed.equals(nroCedula4AI))) ||
//                  (cuadro != null &&
//                    (cuadro.equals(cuadro207) ||
//                        cuadro.equals(cuadro208))
//                  )
//                 )
//            ) {
//            sbQuery.append("c.TIPDATO,");
//            sbQuery.append("c.DESCRIP,");
//        }
		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
				|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208) || cuadro.equals(cuadro209)
				|| cuadro.equals(cuadro206) || cuadro.equals(cuadro207)))
				&& (nroCed != null && (nroCed.equals(nroCedula1A) || nroCed.equals(nroCedula2A)
						|| nroCed.equals(nroCedula3A) || nroCed.equals(nroCedula4AI) || nroCed.equals(nroCedula4AA)
						|| nroCed.equals(nroCedula5A) || nroCed.equals(nroCedula6A) || nroCed.equals(nroCedula7A)
						|| nroCed.equals(nroCedula8A) || nroCed.equals(nroCedula9A)))
				&& !(nroCed.equals(nroCedula2A) && cuadro.equals(cuadro201))) {
			if ((cuadro.equals(cuadro203) || cuadro.equals(cuadro204)) && nroCed.equals(nroCedula8A)) {
				sbQuery.append("c.DESCRIP,");
				sbQuery.append("c.TIPDATO,");
			} else if ((cuadro.equals(cuadro207) && nroCed.equals(nroCedula3A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(nroCedula2A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(nroCedula1A))) {
				sbQuery.append("c.TIPDATO,");
				sbQuery.append("c.DESCRIP,");
			} else if (!(cuadro.equals(cuadro201) && nroCed.equals(nroCedula1A))) {
				sbQuery.append("c.DESCRIP,");
			}
		}
//        sbQuery.append("sum(c.TOTAL_1) as HT,");
//        sbQuery.append("sum(c.TOTAL_2) as MT,");
		sbQuery.append(
				"SUM(IFNULL(c.DATO01,0)+IFNULL(c.DATO03,0)+IFNULL(c.DATO05,0)+IFNULL(c.DATO07,0)+IFNULL(c.DATO09,0)+IFNULL(c.DATO11,0)+IFNULL(c.DATO13,0)+IFNULL(c.DATO15,0)+IFNULL(c.DATO17,0)+IFNULL(c.DATO19,0)) as HT,");
		sbQuery.append(
				"SUM(IFNULL(c.DATO02,0)+IFNULL(c.DATO04,0)+IFNULL(c.DATO06,0)+IFNULL(c.DATO08,0)+IFNULL(c.DATO10,0)+IFNULL(c.DATO12,0)+IFNULL(c.DATO14,0)+IFNULL(c.DATO16,0)+IFNULL(c.DATO18,0)+IFNULL(c.DATO20,0)) as MT,");
		if ((cuadro.equals(cuadro206) && nroCed.equals(nroCedula5A))
				|| (cuadro.equals(cuadro209) && (nroCed.equals(nroCedula6A) || nroCed.equals(nroCedula7A)))
				|| ((cuadro.equals(cuadro201) || cuadro.equals(cuadro204)) && nroCed.equals(nroCedula9A))) {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3");
			if (nroCed.equals(nroCedula7A) || nroCed.equals(nroCedula9A)) {
				sbQuery.append(",sum(c.DATO03) as H4,");
				sbQuery.append("sum(c.DATO04) as M4");
			}
		} else {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3,");
			sbQuery.append("sum(c.DATO03) as H4,");
			sbQuery.append("sum(c.DATO04) as M4,");
			sbQuery.append("sum(c.DATO05) as H5,");
			sbQuery.append("sum(c.DATO06) as M5,");
			sbQuery.append("sum(c.DATO07) as H6,");
			sbQuery.append("sum(c.DATO08) as M6 ");
		}

		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202))
				&& (nroCed != null && (nroCed.equals(nroCedula3A) || nroCed.equals(nroCedula4AI))))) {
			if (cuadro.equals(cuadro202) && nroCed.equals(nroCedula4AI)) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7 ");
			} else if ((cuadro.equals(cuadro201) && (nroCed.equals(nroCedula3A)))
					|| (cuadro.equals(cuadro207) && (nroCed.equals(nroCedula3A)))) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7,");
				sbQuery.append("sum(c.DATO11) as H8,");
				sbQuery.append("sum(c.DATO12) as M8 ");
			}

		}
		if (cuadro != null && ((cuadro.equals(cuadro201) && nroCed != null && nroCed.equals(nroCedula2A))
				|| (cuadro.equals(cuadro207) && nroCed != null && nroCed.equals(nroCedula3A))
				|| ((cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
						|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208))
						&& nroCed != null
						&& (nroCed.equals(nroCedula1A) || nroCed.equals(nroCedula4AI) || nroCed.equals(nroCedula4AA)
								|| nroCed.equals(nroCedula5A) || nroCed.equals(nroCedula6A)
								|| nroCed.equals(nroCedula7A) || nroCed.equals(nroCedula8A))))) {
			sbQuery.append(", sum(c.DATO09) as H7,");
			sbQuery.append("sum(c.DATO10) as M7,");
			sbQuery.append("sum(c.DATO11) as H8,");
			sbQuery.append("sum(c.DATO12) as M8,");
			if (!cuadro.equals(cuadro208)) {
				sbQuery.append("sum(c.DATO13) as H9,");
				sbQuery.append("sum(c.DATO14) as M9 ");
			} else if (cuadro.equals(cuadro208)) {
				int total = sbQuery.length();
				sbQuery.deleteCharAt(total - 1);

			}

		}
		if ((cuadro != null
				&& (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
						|| cuadro.equals(cuadro204))
				&& (nroCed != null && (nroCed.equals(nroCedula4AI) || nroCed.equals(nroCedula4AA)
						|| nroCed.equals(nroCedula5A) || nroCed.equals(nroCedula6A) || nroCed.equals(nroCedula7A)
						|| nroCed.equals(nroCedula8A))))) {
			if (!(cuadro.equals(cuadro201) && nroCed.equals(nroCedula8A))) {
				sbQuery.append(", sum(c.DATO15) as H10,");
				sbQuery.append("sum(c.DATO16) as M10,");
			}

			if (nroCed.equals(nroCedula4AA)) {
				sbQuery.deleteCharAt(sbQuery.length() - 1);
			} else {
				if (!(cuadro.equals(cuadro201) && nroCed.equals(nroCedula8A))) {
					sbQuery.append("sum(c.DATO17) as H11,");
					sbQuery.append("sum(c.DATO18) as M12,");
				}
				if (cuadro.equals(cuadro201) && nroCed.equals(nroCedula6A)) {
					int total = sbQuery.length();
					sbQuery.deleteCharAt(total - 1);
				} else {
					if (!((cuadro.equals(cuadro201) || cuadro.equals(cuadro203) || cuadro.equals(cuadro204))
							&& nroCed.equals(nroCedula8A))) {
						sbQuery.append("sum(c.DATO19) as H13,");
						sbQuery.append("sum(c.DATO20) as M14 ");
					} else {
						int total = sbQuery.length();
						sbQuery.deleteCharAt(total - 1);
					}

				}
			}

		}
		sbQuery.append(" from matricula2016_cabecera a ");
		sbQuery.append(" left join matricula2016_matricula b ");
		sbQuery.append(" ON a.ID_ENVIO=b.CABECERA_IDENVIO ");
		sbQuery.append(" left join  matricula2016_matricula_fila c ");
		sbQuery.append(" ON b.ID_ENVIO=c.DETALLE_IDENVIO ");
		// sbQuery.append("where b.CUADRO=?1 and a.CODUGEL= ?2 and a.NROCED= ?3 ");
		sbQuery.append(" where b.CUADRO=?1 and a.CODUGEL LIKE ?2 and a.NROCED= ?3 and a.ULTIMO=1 ");
		sbQuery.append(" group by c.DESCRIP ");
		sbQuery.append(" order by c.TIPDATO ASC;");

		Query query = em.createNativeQuery(sbQuery.toString());
		// System.out.println(nroCed + ": " + cuadro + ": QUERY: " +
		// sbQuery.toString());
		query.setParameter(1, cuadro);
		query.setParameter(2, "%".concat(codUgel));
		query.setParameter(3, nroCed);

		return query.getResultList();

	}
	// imendoza
	// imendoza 20170330 inicio

	public List<Object[]> reporteDetallado2017ByUgel(String codUgel, String cuadro, String nroCed) {
		StringBuilder sbQuery = new StringBuilder();
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);
		sbQuery.append("select ");
		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
				|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208) || cuadro.equals(cuadro209)
				|| cuadro.equals(cuadro206) || cuadro.equals(cuadro207)))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_1A) || nroCed.equals(NROCEDULA_2A)
						|| nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS) || nroCed.equals(NROCEDULA_4AI)
						|| nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_6A)
						|| nroCed.equals(NROCEDULA_7A) || nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)
						|| nroCed.equals(NROCEDULA_9A)))
				&& !(nroCed.equals(NROCEDULA_2A) && cuadro.equals(cuadro201))) {
			if ((cuadro.equals(cuadro207) && nroCed.equals(NROCEDULA_3AP))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_3AS))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_2A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_1A))) {
				sbQuery.append("c.TIPDATO,");
				sbQuery.append("c.DESCRIP,");
			} else if (!(cuadro.equals(cuadro201) && nroCed.equals(NROCEDULA_1A))) {
				sbQuery.append("c.DESCRIP,");
			}
		}
		if ((cuadro.equals(cuadro204) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))) {
			sbQuery.append(
					"SUM(IFNULL(c.DATO01,0)+IFNULL(c.DATO02,0)+IFNULL(c.DATO03,0)+IFNULL(c.DATO04,0)+IFNULL(c.DATO05,0)+IFNULL(c.DATO06,0)+IFNULL(c.DATO07,0)+IFNULL(c.DATO08,0)) as T,");
		} else {
			sbQuery.append(
					"SUM(IFNULL(c.DATO01,0)+IFNULL(c.DATO03,0)+IFNULL(c.DATO05,0)+IFNULL(c.DATO07,0)+IFNULL(c.DATO09,0)+IFNULL(c.DATO11,0)+IFNULL(c.DATO13,0)+IFNULL(c.DATO15,0)+IFNULL(c.DATO17,0)+IFNULL(c.DATO19,0)) as HT,");
			sbQuery.append(
					"SUM(IFNULL(c.DATO02,0)+IFNULL(c.DATO04,0)+IFNULL(c.DATO06,0)+IFNULL(c.DATO08,0)+IFNULL(c.DATO10,0)+IFNULL(c.DATO12,0)+IFNULL(c.DATO14,0)+IFNULL(c.DATO16,0)+IFNULL(c.DATO18,0)+IFNULL(c.DATO20,0)) as MT,");
		}
		if (((cuadro.equals(cuadro201) || cuadro.equals(cuadro204)) && nroCed.equals(NROCEDULA_9A))
				|| (cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3,");
			sbQuery.append("sum(c.DATO03) as H4,");
			sbQuery.append("sum(c.DATO04) as M4");

		} else if (!(cuadro.equals(cuadro209)
				&& (nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)))
				&& !(cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))) {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3,");
			sbQuery.append("sum(c.DATO03) as H4,");
			sbQuery.append("sum(c.DATO04) as M4,");
			sbQuery.append("sum(c.DATO05) as H5,");
			sbQuery.append("sum(c.DATO06) as M5,");
			if (!(cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_8AP))
					&& !(cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_3AP))) {
				sbQuery.append("sum(c.DATO07) as H6,");
				sbQuery.append("sum(c.DATO08) as M6 ");
			} else if (cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_3AP)) {
				sbQuery.append("sum(c.DATO07) as H6,");
				sbQuery.append("sum(c.DATO08) as M6, ");
				sbQuery.append("sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7, ");
				sbQuery.append("sum(c.DATO11) as H8,");
				sbQuery.append("sum(c.DATO12) as M8 ");
			}
		}
		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro206))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS)
						|| nroCed.equals(NROCEDULA_4AI))))) {
			if ((cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_3AS))) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7 ");
			} else if (((cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_3AP)))
					|| (cuadro.equals(cuadro207) && (nroCed.equals(NROCEDULA_3AP))))
					|| (cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_3AS)))) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7,");
				sbQuery.append("sum(c.DATO11) as H8,");
				sbQuery.append("sum(c.DATO12) as M8 ");
			}
		}
		if (cuadro != null
				&& ((cuadro.equals(cuadro201) && nroCed != null && nroCed.equals(NROCEDULA_2A))
						|| (cuadro.equals(cuadro207) && nroCed != null
								&& (nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS)))
						|| ((cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
								|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208))
								&& nroCed != null
								&& (nroCed.equals(NROCEDULA_1A) || nroCed.equals(NROCEDULA_4AI)
										|| nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_5A)
										|| nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)
										|| nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP))))
				&& !(cuadro.equals(cuadro202) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
				&& !(cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))) {
			sbQuery.append(", sum(c.DATO09) as H7,");
			sbQuery.append("sum(c.DATO10) as M7,");
			sbQuery.append("sum(c.DATO11) as H8,");
			sbQuery.append("sum(c.DATO12) as M8,");
			if (!(cuadro.equals(cuadro201) && nroCed.equals(NROCEDULA_8AP))) {
				if (!cuadro.equals(cuadro208)) {
					sbQuery.append("sum(c.DATO13) as H9,");
					sbQuery.append("sum(c.DATO14) as M9 ");
				} else if (cuadro.equals(cuadro208)) {
					int total = sbQuery.length();
					sbQuery.deleteCharAt(total - 1);
				}
			}
		}
		if ((cuadro != null
				&& (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
						|| cuadro.equals(cuadro204) || cuadro.equals(cuadro209) || cuadro.equals(cuadro206))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_4AI) || nroCed.equals(NROCEDULA_4AA)
						|| nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)
						|| nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP))))) {
			if (((cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_4AI)
					|| nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_7A)))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_7A)))
					&& !(cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					&& !(cuadro.equals(cuadro209) && (nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_6A)
							|| nroCed.equals(NROCEDULA_7A)))
					&& !(cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))) {
				sbQuery.append(", sum(c.DATO15) as H10,");
				sbQuery.append("sum(c.DATO16) as M10,");
			}
			if (nroCed.equals(NROCEDULA_4AA) || (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_8AI))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_8AP))) {
				sbQuery.deleteCharAt(sbQuery.length() - 1);
			} else {
				if (!((cuadro.equals(cuadro201) || cuadro.equals(cuadro204))
						&& (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
						&& !((cuadro.equals(cuadro201) || cuadro.equals(cuadro209)) && (nroCed.equals(NROCEDULA_6A)))
						&& !(cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
					sbQuery.append("sum(c.DATO17) as H11,");
					sbQuery.append("sum(c.DATO18) as M12,");
				}
				if ((cuadro.equals(cuadro201) || cuadro.equals(cuadro209)) && nroCed.equals(NROCEDULA_6A)) {
					int total = sbQuery.length();
					sbQuery.deleteCharAt(total - 1);
				} else {
					if (!((cuadro.equals(cuadro201) || cuadro.equals(cuadro203) || cuadro.equals(cuadro204))
							&& (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
							&& !(cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
						sbQuery.append("sum(c.DATO19) as H13,");
						sbQuery.append("sum(c.DATO20) as M14 ");
					} else {
						int total = sbQuery.length();
						sbQuery.deleteCharAt(total - 1);
					}
				}
			}
		}
		sbQuery.append(" from matricula2017_cabecera a ");
		sbQuery.append(" left join matricula2017_matricula b ");
		sbQuery.append(" ON a.ID_ENVIO=b.CABECERA_IDENVIO ");
		sbQuery.append(" left join  matricula2017_matricula_fila c ");
		sbQuery.append(" ON b.ID_ENVIO=c.DETALLE_IDENVIO ");
		sbQuery.append(" where b.CUADRO=?1 and a.CODUGEL LIKE ?2 and a.NROCED= ?3 and a.ULTIMO=1 ");
		sbQuery.append(" group by c.DESCRIP ");
		sbQuery.append(" order by c.TIPDATO ASC;");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter(1, cuadro);
		query.setParameter(2, "%".concat(codUgel));
		query.setParameter(3, nroCed);
		// System.out.println(nroCed + ": " + cuadro + ": QUERY: " +
		// sbQuery.toString());

		return query.getResultList();
	}

	public List<Object[]> reporteDetallado2018ByUgel(String codUgel, String cuadro, String nroCed) {
		logger.info(":: EnvioDocumentosFacade.reporteDetallado2018ByUgel :: Starting execution...");
		StringBuilder sbQuery = new StringBuilder();
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);
		String groupOrderBy = "";
		sbQuery.append("select ");
		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
				|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208) || cuadro.equals(cuadro209)
				|| cuadro.equals(cuadro206) || cuadro.equals(cuadro207)))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_1A) || nroCed.equals(NROCEDULA_2A)
						|| nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS) || nroCed.equals(NROCEDULA_4AI)
						|| nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_6A)
						|| nroCed.equals(NROCEDULA_7A) || nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)
						|| nroCed.equals(NROCEDULA_9A)))
				&& !(nroCed.equals(NROCEDULA_2A) && cuadro.equals(cuadro201))) {
			if ((cuadro.equals(cuadro207) && nroCed.equals(NROCEDULA_3AP))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_3AS))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_2A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_1A))) {
				sbQuery.append("c.TIPDATO,");
				sbQuery.append("c.DESCRIP,");
				groupOrderBy = "group by c.TIPDATO, c.DESCRIP order by c.TIPDATO ASC";
			} else if (!(cuadro.equals(cuadro201) && nroCed.equals(NROCEDULA_1A))) {
				sbQuery.append("c.DESCRIP,");
				groupOrderBy = "group by c.DESCRIP ";
			}
		}
		if ((cuadro.equals(cuadro204) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))) {
			sbQuery.append(
					"SUM(IFNULL(c.DATO01,0)+IFNULL(c.DATO02,0)+IFNULL(c.DATO03,0)+IFNULL(c.DATO04,0)+IFNULL(c.DATO05,0)+IFNULL(c.DATO06,0)+IFNULL(c.DATO07,0)+IFNULL(c.DATO08,0)) as T,");
		} else {
			sbQuery.append(
					"SUM(IFNULL(c.DATO01,0)+IFNULL(c.DATO03,0)+IFNULL(c.DATO05,0)+IFNULL(c.DATO07,0)+IFNULL(c.DATO09,0)+IFNULL(c.DATO11,0)+IFNULL(c.DATO13,0)+IFNULL(c.DATO15,0)+IFNULL(c.DATO17,0)+IFNULL(c.DATO19,0)) as HT,");
			sbQuery.append(
					"SUM(IFNULL(c.DATO02,0)+IFNULL(c.DATO04,0)+IFNULL(c.DATO06,0)+IFNULL(c.DATO08,0)+IFNULL(c.DATO10,0)+IFNULL(c.DATO12,0)+IFNULL(c.DATO14,0)+IFNULL(c.DATO16,0)+IFNULL(c.DATO18,0)+IFNULL(c.DATO20,0)) as MT,");
		}
		if (((cuadro.equals(cuadro201) || cuadro.equals(cuadro204)) && nroCed.equals(NROCEDULA_9A))
				|| (cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3,");
			sbQuery.append("sum(c.DATO03) as H4,");
			sbQuery.append("sum(c.DATO04) as M4");

		} else if (!(cuadro.equals(cuadro209)
				&& (nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)))
				&& !(cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))) {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3,");
			sbQuery.append("sum(c.DATO03) as H4,");
			sbQuery.append("sum(c.DATO04) as M4,");
			sbQuery.append("sum(c.DATO05) as H5,");
			sbQuery.append("sum(c.DATO06) as M5,");
			if (!(cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_8AP))
					&& !(cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_3AP))) {
				sbQuery.append("sum(c.DATO07) as H6,");
				sbQuery.append("sum(c.DATO08) as M6 ");
			} else if (cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_3AP)) {
				sbQuery.append("sum(c.DATO07) as H6,");
				sbQuery.append("sum(c.DATO08) as M6, ");
				sbQuery.append("sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7, ");
				sbQuery.append("sum(c.DATO11) as H8,");
				sbQuery.append("sum(c.DATO12) as M8 ");
			}
		}
		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro206))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS)
						|| nroCed.equals(NROCEDULA_4AI))))) {
			if ((cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_3AS))) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7 ");
			} else if (((cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_3AP)))
					|| (cuadro.equals(cuadro207) && (nroCed.equals(NROCEDULA_3AP))))
					|| (cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_3AS)))) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7,");
				sbQuery.append("sum(c.DATO11) as H8,");
				sbQuery.append("sum(c.DATO12) as M8 ");
			}
		}
		if (cuadro != null
				&& ((cuadro.equals(cuadro201) && nroCed != null && nroCed.equals(NROCEDULA_2A))
						|| (cuadro.equals(cuadro207) && nroCed != null
								&& (nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS)))
						|| ((cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
								|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208))
								&& nroCed != null
								&& (nroCed.equals(NROCEDULA_1A) || nroCed.equals(NROCEDULA_4AI)
										|| nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_5A)
										|| nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)
										|| nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP))))
				&& !(cuadro.equals(cuadro202) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
				&& !(cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))) {
			sbQuery.append(", sum(c.DATO09) as H7,");
			sbQuery.append("sum(c.DATO10) as M7,");
			sbQuery.append("sum(c.DATO11) as H8,");
			sbQuery.append("sum(c.DATO12) as M8,");
			if (!(cuadro.equals(cuadro201) && nroCed.equals(NROCEDULA_8AP))) {
				if (!cuadro.equals(cuadro208)) {
					sbQuery.append("sum(c.DATO13) as H9,");
					sbQuery.append("sum(c.DATO14) as M9 ");
				} else if (cuadro.equals(cuadro208)) {
					int total = sbQuery.length();
					sbQuery.deleteCharAt(total - 1);
				}
			}
		}
		if ((cuadro != null
				&& (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
						|| cuadro.equals(cuadro204) || cuadro.equals(cuadro209) || cuadro.equals(cuadro206))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_4AI) || nroCed.equals(NROCEDULA_4AA)
						|| nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)
						|| nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP))))) {
			if (((cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_4AI)
					|| nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_7A)))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_7A)))
					&& !(cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					&& !(cuadro.equals(cuadro209) && (nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_6A)
							|| nroCed.equals(NROCEDULA_7A)))
					&& !(cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))) {
				sbQuery.append(", sum(c.DATO15) as H10,");
				sbQuery.append("sum(c.DATO16) as M10,");
			}
			if (nroCed.equals(NROCEDULA_4AA) || (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_8AI))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_8AP))) {
				sbQuery.deleteCharAt(sbQuery.length() - 1);
			} else {
				if (!((cuadro.equals(cuadro201) || cuadro.equals(cuadro204))
						&& (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
						&& !((cuadro.equals(cuadro201) || cuadro.equals(cuadro209)) && (nroCed.equals(NROCEDULA_6A)))
						&& !(cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
					sbQuery.append("sum(c.DATO17) as H11,");
					sbQuery.append("sum(c.DATO18) as M12,");
				}
				if ((cuadro.equals(cuadro201) || cuadro.equals(cuadro209)) && nroCed.equals(NROCEDULA_6A)) {
					int total = sbQuery.length();
					sbQuery.deleteCharAt(total - 1);
				} else {
					if (!((cuadro.equals(cuadro201) || cuadro.equals(cuadro203) || cuadro.equals(cuadro204))
							&& (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
							&& !(cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
						sbQuery.append("sum(c.DATO19) as H13,");
						sbQuery.append("sum(c.DATO20) as M14 ");
					} else {
						int total = sbQuery.length();
						sbQuery.deleteCharAt(total - 1);
					}
				}
			}
		}
		sbQuery.append(" from matricula2018_cabecera a ");
		sbQuery.append(" left join matricula2018_matricula b ");
		sbQuery.append(" ON a.ID_ENVIO=b.CABECERA_IDENVIO ");
		sbQuery.append(" left join  matricula2018_matricula_fila c ");
		sbQuery.append(" ON b.ID_ENVIO=c.DETALLE_IDENVIO ");
		sbQuery.append(" where b.CUADRO=?1 and a.CODUGEL LIKE ?2 and a.NROCED= ?3 and a.ULTIMO=1 ");
		sbQuery.append(groupOrderBy);
		// sbQuery.append(" group by c.DESCRIP ");
		// sbQuery.append(" order by c.TIPDATO ASC");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter(1, cuadro);
		query.setParameter(2, "%".concat(codUgel));
		query.setParameter(3, nroCed);

		logger.info("params: 1 (" + cuadro + "2 (" + "%".concat(codUgel) + ")" + " 3(" + nroCed + ")");
		logger.info("Cedula: " + nroCed + ": " + cuadro + ": QUERY: " + sbQuery.toString());
		logger.info(":: EnvioDocumentosFacade.reporteDetallado2018ByUgel :: Execution finish.");
		return query.getResultList();

	}

	public List<Object[]> reporteDetallado2019ByUgel(String codUgel, String cuadro, String nroCed) {
		logger.info(":: EnvioDocumentosFacade.reporteDetallado2019ByUgel :: Starting execution...");
		StringBuilder sbQuery = new StringBuilder();
		codUgel = (codUgel.endsWith("00") || codUgel.endsWith("150101") || codUgel.endsWith("070101"))
				? (codUgel.substring(0, 4).concat("%"))
				: (codUgel);
		String groupOrderBy = "";
		sbQuery.append("select ");
		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
				|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208) || cuadro.equals(cuadro209)
				|| cuadro.equals(cuadro206) || cuadro.equals(cuadro207)))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_1A) || nroCed.equals(NROCEDULA_2A)
						|| nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS) || nroCed.equals(NROCEDULA_4AI)
						|| nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_6A)
						|| nroCed.equals(NROCEDULA_7A) || nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)
						|| nroCed.equals(NROCEDULA_9A)))
				&& !(nroCed.equals(NROCEDULA_2A) && cuadro.equals(cuadro201))) {
			if ((cuadro.equals(cuadro207) && nroCed.equals(NROCEDULA_3AP))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_3AS))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_2A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_1A))) {
				sbQuery.append("c.TIPDATO,");
				sbQuery.append("c.DESCRIP,");
				groupOrderBy = "group by c.TIPDATO, c.DESCRIP order by c.TIPDATO ASC";
			} else if (!(cuadro.equals(cuadro201) && nroCed.equals(NROCEDULA_1A))) {
				sbQuery.append("c.DESCRIP,");
				groupOrderBy = "group by c.DESCRIP ";
			}
		}
		if ((cuadro.equals(cuadro204) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))) {
			sbQuery.append(
					"SUM(IFNULL(c.DATO01,0)+IFNULL(c.DATO02,0)+IFNULL(c.DATO03,0)+IFNULL(c.DATO04,0)+IFNULL(c.DATO05,0)+IFNULL(c.DATO06,0)+IFNULL(c.DATO07,0)+IFNULL(c.DATO08,0)) as T,");
		} else {
			sbQuery.append(
					"SUM(IFNULL(c.DATO01,0)+IFNULL(c.DATO03,0)+IFNULL(c.DATO05,0)+IFNULL(c.DATO07,0)+IFNULL(c.DATO09,0)+IFNULL(c.DATO11,0)+IFNULL(c.DATO13,0)+IFNULL(c.DATO15,0)+IFNULL(c.DATO17,0)+IFNULL(c.DATO19,0)) as HT,");
			sbQuery.append(
					"SUM(IFNULL(c.DATO02,0)+IFNULL(c.DATO04,0)+IFNULL(c.DATO06,0)+IFNULL(c.DATO08,0)+IFNULL(c.DATO10,0)+IFNULL(c.DATO12,0)+IFNULL(c.DATO14,0)+IFNULL(c.DATO16,0)+IFNULL(c.DATO18,0)+IFNULL(c.DATO20,0)) as MT,");
		}
		if (((cuadro.equals(cuadro201) || cuadro.equals(cuadro204)) && nroCed.equals(NROCEDULA_9A))
				|| (cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3,");
			sbQuery.append("sum(c.DATO03) as H4,");
			sbQuery.append("sum(c.DATO04) as M4");

		} else if (!(cuadro.equals(cuadro209)
				&& (nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)))
				&& !(cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))) {
			sbQuery.append("sum(c.DATO01) as H3,");
			sbQuery.append("sum(c.DATO02) as M3,");
			sbQuery.append("sum(c.DATO03) as H4,");
			sbQuery.append("sum(c.DATO04) as M4,");
			sbQuery.append("sum(c.DATO05) as H5,");
			sbQuery.append("sum(c.DATO06) as M5,");
			if (!(cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_8AP))
					&& !(cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_3AP))) {
				sbQuery.append("sum(c.DATO07) as H6,");
				sbQuery.append("sum(c.DATO08) as M6 ");
			} else if (cuadro.equals(cuadro204) && nroCed.equals(NROCEDULA_3AP)) {
				sbQuery.append("sum(c.DATO07) as H6,");
				sbQuery.append("sum(c.DATO08) as M6, ");
				sbQuery.append("sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7, ");
				sbQuery.append("sum(c.DATO11) as H8,");
				sbQuery.append("sum(c.DATO12) as M8 ");
			}
		}
		if ((cuadro != null && (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro206))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS)
						|| nroCed.equals(NROCEDULA_4AI))))) {
			if ((cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_3AS))) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7 ");
			} else if (((cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_3AP)))
					|| (cuadro.equals(cuadro207) && (nroCed.equals(NROCEDULA_3AP))))
					|| (cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_3AS)))) {
				sbQuery.append(", sum(c.DATO09) as H7,");
				sbQuery.append("sum(c.DATO10) as M7,");
				sbQuery.append("sum(c.DATO11) as H8,");
				sbQuery.append("sum(c.DATO12) as M8 ");
			}
		}
		if (cuadro != null
				&& ((cuadro.equals(cuadro201) && nroCed != null && nroCed.equals(NROCEDULA_2A))
						|| (cuadro.equals(cuadro207) && nroCed != null
								&& (nroCed.equals(NROCEDULA_3AP) || nroCed.equals(NROCEDULA_3AS)))
						|| ((cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
								|| cuadro.equals(cuadro204) || cuadro.equals(cuadro208))
								&& nroCed != null
								&& (nroCed.equals(NROCEDULA_1A) || nroCed.equals(NROCEDULA_4AI)
										|| nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_5A)
										|| nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)
										|| nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP))))
				&& !(cuadro.equals(cuadro202) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
				&& !(cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))) {
			sbQuery.append(", sum(c.DATO09) as H7,");
			sbQuery.append("sum(c.DATO10) as M7,");
			sbQuery.append("sum(c.DATO11) as H8,");
			sbQuery.append("sum(c.DATO12) as M8,");
			if (!(cuadro.equals(cuadro201) && nroCed.equals(NROCEDULA_8AP))) {
				if (!cuadro.equals(cuadro208)) {
					sbQuery.append("sum(c.DATO13) as H9,");
					sbQuery.append("sum(c.DATO14) as M9 ");
				} else if (cuadro.equals(cuadro208)) {
					int total = sbQuery.length();
					sbQuery.deleteCharAt(total - 1);
				}
			}
		}
		if ((cuadro != null
				&& (cuadro.equals(cuadro201) || cuadro.equals(cuadro202) || cuadro.equals(cuadro203)
						|| cuadro.equals(cuadro204) || cuadro.equals(cuadro209) || cuadro.equals(cuadro206))
				&& (nroCed != null && (nroCed.equals(NROCEDULA_4AI) || nroCed.equals(NROCEDULA_4AA)
						|| nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_6A) || nroCed.equals(NROCEDULA_7A)
						|| nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP))))) {
			if (((cuadro.equals(cuadro201) && (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_4AI)
					|| nroCed.equals(NROCEDULA_5A) || nroCed.equals(NROCEDULA_7A)))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_7A)))
					&& !(cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					&& !(cuadro.equals(cuadro209) && (nroCed.equals(NROCEDULA_4AA) || nroCed.equals(NROCEDULA_6A)
							|| nroCed.equals(NROCEDULA_7A)))
					&& !(cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))) {
				sbQuery.append(", sum(c.DATO15) as H10,");
				sbQuery.append("sum(c.DATO16) as M10,");
			}
			if (nroCed.equals(NROCEDULA_4AA) || (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_4AI))
					|| (cuadro.equals(cuadro206) && nroCed.equals(NROCEDULA_5A))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_8AI))
					|| (cuadro.equals(cuadro202) && nroCed.equals(NROCEDULA_8AP))) {
				sbQuery.deleteCharAt(sbQuery.length() - 1);
			} else {
				if (!((cuadro.equals(cuadro201) || cuadro.equals(cuadro204))
						&& (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
						&& !((cuadro.equals(cuadro201) || cuadro.equals(cuadro209)) && (nroCed.equals(NROCEDULA_6A)))
						&& !(cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
					sbQuery.append("sum(c.DATO17) as H11,");
					sbQuery.append("sum(c.DATO18) as M12,");
				}
				if ((cuadro.equals(cuadro201) || cuadro.equals(cuadro209)) && nroCed.equals(NROCEDULA_6A)) {
					int total = sbQuery.length();
					sbQuery.deleteCharAt(total - 1);
				} else {
					if (!((cuadro.equals(cuadro201) || cuadro.equals(cuadro203) || cuadro.equals(cuadro204))
							&& (nroCed.equals(NROCEDULA_8AI) || nroCed.equals(NROCEDULA_8AP)))
							&& !(cuadro.equals(cuadro209) && nroCed.equals(NROCEDULA_7A))) {
						sbQuery.append("sum(c.DATO19) as H13,");
						sbQuery.append("sum(c.DATO20) as M14 ");
					} else {
						int total = sbQuery.length();
						sbQuery.deleteCharAt(total - 1);
					}
				}
			}
		}
		sbQuery.append(" from matricula2019_cabecera a ");
		sbQuery.append(" left join matricula2019_matricula b ");
		sbQuery.append(" ON a.ID_ENVIO=b.CABECERA_IDENVIO ");
		sbQuery.append(" left join  matricula2019_matricula_fila c ");
		sbQuery.append(" ON b.ID_ENVIO=c.DETALLE_IDENVIO ");
		sbQuery.append(" where b.CUADRO=?1 and a.CODUGEL LIKE ?2 and a.NROCED= ?3 and a.ULTIMO=1 ");
		sbQuery.append(groupOrderBy);
		// sbQuery.append(" group by c.DESCRIP ");
		// sbQuery.append(" order by c.TIPDATO ASC");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter(1, cuadro);
		query.setParameter(2, "%".concat(codUgel));
		query.setParameter(3, nroCed);

		logger.info("params: 1 (" + cuadro + "2 (" + "%".concat(codUgel) + ")" + " 3(" + nroCed + ")");
		logger.info("Cedula: " + nroCed + ": " + cuadro + ": QUERY: " + sbQuery.toString());
		logger.info(":: EnvioDocumentosFacade.reporteDetallado2018ByUgel :: Execution finish.");
		return query.getResultList();

	}

	public List<Object[]> verificaIndicadorxCodModxVariable(String codMod, String variable) {

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT COD_MOD, ANEXO, VARIABLE ");
		sbQuery.append(" FROM estadistica.eol_indicador a ");
		sbQuery.append(" LEFT JOIN  estadistica.eol_indicador_periodo b ");
		sbQuery.append(" ON a.PERIODO=b.ID_PERIODO ");
		sbQuery.append(" LEFT JOIN  estadistica.eol_indicador_variable c ");
		sbQuery.append(" ON a.VARIABLE=c.ID_VARIABLE ");
		sbQuery.append(" where cod_mod=?1 and c.ID_VARIABLE=?2 and b.ESTADO=1 ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter(1, codMod);
		query.setParameter(2, variable);

		logger.info(" verificacion query : " + query);

		return query.getResultList();

	}

	public List<Object[]> verificaCifrasxCodModxVariablexAnexo(String codMod, String anexo, String variable) {

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT COD_MOD, ANEXO, CIFRA ");
		sbQuery.append(" FROM estadistica.eol_cifras a ");
		sbQuery.append(" LEFT JOIN  estadistica.eol_indicador_periodo b ");
		sbQuery.append(" ON a.periodo=b.ID_PERIODO ");
		sbQuery.append(" LEFT JOIN  estadistica.eol_indicador_cifras c ");
		sbQuery.append(" ON a.CIFRA=c.ID_VARIABLE ");
		sbQuery.append(" where cod_mod=?1 and anexo=?2 and c.ID_VARIABLE=?3 and b.ESTADO=1 ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter(1, codMod);
		query.setParameter(2, anexo);
		query.setParameter(3, variable);

		logger.info(" verificacion query : " + query);

		return query.getResultList();

	}

}
