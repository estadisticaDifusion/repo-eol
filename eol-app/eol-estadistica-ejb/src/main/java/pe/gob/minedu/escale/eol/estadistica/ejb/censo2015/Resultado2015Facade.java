/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.ejb.censo2015;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Resultado2015Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author Administrador
 */
@Singleton
public class Resultado2015Facade extends AbstractFacade<Resultado2015Cabecera> {

	public static final String CEDULA_RESULTADO = "CEDULA-RESULTADO";
	static final Logger LOGGER = Logger.getLogger(Resultado2015Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Resultado2015Facade() {
		super(Resultado2015Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Resultado2015Cabecera cedula) {
		String sql = "UPDATE Resultado2015Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo and a.nroced=:nroced ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroced", cedula.getNroced());
		query.executeUpdate();
		/*
		 * Set<String> keys = cedula.getDetalle().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Resultado2011Detalle detalle = cedula.getDetalle().get(key);
		 * detalle.setCedula(cedula); List<Resultado2011Fila> filas =
		 * detalle.getFilas(); for (Resultado2011Fila fila : filas) {
		 * fila.setDetalle(detalle); } }
		 */
		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

	public List<Object[]> verificarObservacionCedula(String codmod, String anexo, String nroced, Integer totalH,
			Integer totalM, Integer dato01h, Integer dato01m, Integer dato02h, Integer dato02m, Integer dato03h,
			Integer dato03m, Integer dato04h, Integer dato04m, Integer dato05h, Integer dato05m, Integer dato06h,
			Integer dato06m, Integer dato07h, Integer dato07m, Integer dato08h, Integer dato08m, Integer dato09h,
			Integer dato09m, Integer dato10h, Integer dato10m) {
		Query q = null;

		try {
			String query = "CALL verificar_consistencia(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			q = (Query) em.createNativeQuery(query);

			q.setParameter("1", codmod);
			q.setParameter("2", anexo);
			q.setParameter("3", nroced);
			q.setParameter("4", totalH);
			q.setParameter("5", totalM);
			q.setParameter("6", dato01h);
			q.setParameter("7", dato01m);
			q.setParameter("8", dato02h);
			q.setParameter("9", dato02m);
			q.setParameter("10", dato03h);
			q.setParameter("11", dato03m);
			q.setParameter("12", dato04h);
			q.setParameter("13", dato04m);
			q.setParameter("14", dato05h);
			q.setParameter("15", dato05m);
			q.setParameter("16", dato06h);
			q.setParameter("17", dato06m);
			q.setParameter("18", dato07h);
			q.setParameter("19", dato07m);
			q.setParameter("20", dato08h);
			q.setParameter("21", dato08m);
			q.setParameter("22", dato09h);
			q.setParameter("23", dato09m);
			q.setParameter("24", dato10h);
			q.setParameter("25", dato10m);

			List<Object[]> listObj = q.getResultList();

			return listObj;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
