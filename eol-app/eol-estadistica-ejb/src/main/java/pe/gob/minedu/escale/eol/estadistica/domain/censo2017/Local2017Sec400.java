/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "local2017_sec400")
public class Local2017Sec400 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "PREGUNTA")
    private String pregunta;
    @Column(name = "ORDEN")
    private Short orden;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL")
    private String total;
    @Column(name = "DAT01")
    private Integer dat01;
    @Column(name = "DAT02")
    private Integer dat02;
    @Column(name = "DAT03")
    private Integer dat03;
    @Column(name = "DAT04")
    private Integer dat04;
    @Column(name = "DAT05")
    private Integer dat05;  
    @Column(name = "CHK01")
    private String chk01;
    @Column(name = "CHK02")
    private String chk02;
    @Column(name = "CHK03")
    private String chk03;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2017Cabecera local2017Cabecera;

    public Local2017Sec400() {
    }

    public Local2017Sec400(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "PREGUNTA")
    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @XmlElement(name = "ORDEN")
    public Short getOrden() {
        return orden;
    }

    public void setOrden(Short orden) {
        this.orden = orden;
    }

    @XmlElement(name = "DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name="DAT01")
    public Integer getDat01() {
        return dat01;
    }

    public void setDat01(Integer dat01) {
        this.dat01 = dat01;
    }
    @XmlElement(name="DAT02")
    public Integer getDat02() {
        return dat02;
    }

    public void setDat02(Integer dat02) {
        this.dat02 = dat02;
    }
    @XmlElement(name="DAT03")
    public Integer getDat03() {
        return dat03;
    }

    public void setDat03(Integer dat03) {
        this.dat03 = dat03;
    }
    @XmlElement(name="DAT04")
    public Integer getDat04() {
        return dat04;
    }

    public void setDat04(Integer dat04) {
        this.dat04 = dat04;
    }
    @XmlElement(name="DAT05")
    public Integer getDat05() {
        return dat05;
    }

    public void setDat05(Integer dat05) {
        this.dat05 = dat05;
    }

    @XmlElement(name="CHK01")
    public String getChk01() {
        return chk01;
    }

    public void setChk01(String chk01) {
        this.chk01 = chk01;
    }
    @XmlElement(name="CHK02")
    public String getChk02() {
        return chk02;
    }

    public void setChk02(String chk02) {
        this.chk02 = chk02;
    }
    @XmlElement(name="CHK03")
    public String getChk03() {
        return chk03;
    }

    public void setChk03(String chk03) {
        this.chk03 = chk03;
    }
    @XmlElement(name="TOTAL")
    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    
    @XmlTransient
    public Local2017Cabecera getLocal2017Cabecera() {
        return local2017Cabecera;
    }

    public void setLocal2017Cabecera(Local2017Cabecera local2017Cabecera) {
        this.local2017Cabecera = local2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2017Sec400)) {
            return false;
        }
        Local2017Sec400 other = (Local2017Sec400) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Local2017Sec400[idEnvio=" + idEnvio + "]";
    }

}
