/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;


import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2014_localpronoei")
public class Matricula2014Localpronoei implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P501_A")
    private String p501A;
    @Column(name = "P501_M")
    private String p501M;
    @Column(name = "P502")
    private String p502;
    @Column(name = "P502_ESP")
    private String p502esp;
    
    @Column(name = "P503")
    private String p503;    
    @Column(name = "P503_7_CM")
    private String p5037cm;
    @Column(name = "P503_7_NM")
    private String p5037nm;
    @Column(name = "P503_ESP")
    private String p503esp;   
    @Column(name = "P504")
    private String p504;
    @Column(name = "P505")
    private String p505;
    @Column(name = "P506")
    private String p506;
    @Column(name = "P507")
    private String p507;
    @Column(name = "P508")
    private String p508;
    @Column(name = "P508_ESP")
    private String p508Esp;    
    @Column(name = "P509")
    private String p509;
    @Column(name = "P510")
    private String p510;
    @Column(name = "P511")
    private String p511;    
    @Column(name = "P512_00")
    private Integer p51200;    
    @Column(name = "P512_01")
    private Integer p51201;    
    @Column(name = "P512_02")
    private Integer p51202;    
    @Column(name = "P512_03")
    private Integer p51203;    
    @Column(name = "P512_10")
    private Integer p51210;    
    @Column(name = "P512_11")
    private Integer p51211;    
    @Column(name = "P512_12")
    private Integer p51212;    
    @Column(name = "P512_13")
    private Integer p51213; 
    @Column(name = "P512_20")
    private Integer p51220;    
    @Column(name = "P512_21")
    private Integer p51221;    
    @Column(name = "P512_22")
    private Integer p51222;    
    @Column(name = "P512_23")
    private Integer p51223; 
    @Column(name = "P512_30")
    private Integer p51230;    
    @Column(name = "P512_31")
    private Integer p51231;    
    @Column(name = "P512_32")
    private Integer p51232;    
    @Column(name = "P512_33")
    private Integer p51233;    
    
    @Column(name = "P513_1_PIE")
    private String p5131Pie;
    @Column(name = "P513_1_ACE")
    private String p5131Ace;
    @Column(name = "P513_1_OMN")
    private String p5131Omn;
    @Column(name = "P513_1_CAN")
    private String p5131Can;
    @Column(name = "P513_1_LAN")
    private String p5131Lan;
    @Column(name = "P513_1_AVI")
    private String p5131Avi;
    @Column(name = "P513_2_PIE")
    private String p5132Pie;
    @Column(name = "P513_2_ACE")
    private String p5132Ace;
    @Column(name = "P513_2_OMN")
    private String p5132Omn;
    @Column(name = "P513_2_CAN")
    private String p5132Can;
    @Column(name = "P513_2_LAN")
    private String p5132Lan;
    @Column(name = "P513_2_AVI")
    private String p5132Avi;
    @Column(name = "P513_3_PIE")
    private String p5133Pie;
    @Column(name = "P513_3_ACE")
    private String p5133Ace;
    @Column(name = "P513_3_OMN")
    private String p5133Omn;
    @Column(name = "P513_3_CAN")
    private String p5133Can;
    @Column(name = "P513_3_LAN")
    private String p5133Lan;
    @Column(name = "P513_3_AVI")
    private String p5133Avi;
    @Column(name = "P514_1_DIA")
    private String p5141Dia;
    @Column(name = "P514_1_HOR")
    private String p5141Hor;
    @Column(name = "P514_1_MIN")
    private String p5141Min;
    @Column(name = "P514_2_DIA")
    private String p5142Dia;
    @Column(name = "P514_2_HOR")
    private String p5142Hor;
    @Column(name = "P514_2_MIN")
    private String p5142Min;
    @Column(name = "P514_3_DIA")
    private String p5143Dia;
    @Column(name = "P514_3_HOR")
    private String p5143Hor;
    @Column(name = "P514_3_MIN")
    private String p5143Min;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2014Cabecera matricula2014Cabecera;

    public Matricula2014Localpronoei() {
    }

    public Matricula2014Localpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P501_A")
    public String getP501A() {
        return p501A;
    }

    public void setP501A(String p501A) {
        this.p501A = p501A;
    }

    @XmlElement(name="P501_M")
    public String getP501M() {
        return p501M;
    }

    public void setP501M(String p501M) {
        this.p501M = p501M;
    }

    @XmlElement(name="P502")
    public String getP502() {
        return p502;
    }

    public void setP502(String p502) {
        this.p502 = p502;
    }  

    @XmlElement(name="P502_ESP")
    public String getP502esp() {
        return p502esp;
    }

    public void setP502esp(String p502esp) {
        this.p502esp = p502esp;
    }    
    
    @XmlElement(name="P503")
    public String getP503() {
        return p503;
    }

    public void setP503(String p503) {
        this.p503 = p503;
    }
    
    @XmlElement(name="P503_ESP")
    public String getP503esp() {
        return p503esp;
    }

    public void setP503b(String p503esp) {
        this.p503esp = p503esp;
    }
    
    @XmlElement(name="P503_7_CM")
    public String getP5037cm() {
        return p5037cm;
    }

    public void setP5037cm(String p5037cm) {
        this.p5037cm = p5037cm;
    }
    
    @XmlElement(name="P503_7_NM")
    public String getP5037nm() {
        return p5037nm;
    }

    public void setP5037nm(String p5037nm) {
        this.p5037nm = p5037nm;
    } 
       

    @XmlElement(name="P504")
    public String getP504() {
        return p504;
    }

    public void setP504(String p504) {
        this.p504 = p504;
    }

    @XmlElement(name="P505")
    public String getP505() {
        return p505;
    }

    public void setP505(String p505) {
        this.p505 = p505;
    }

    @XmlElement(name="P506")
    public String getP506() {
        return p506;
    }

    public void setP506(String p506) {
        this.p506 = p506;
    }

    @XmlElement(name="P507")
    public String getP507() {
        return p507;
    }

    public void setP507(String p507) {
        this.p507 = p507;
    }

    @XmlElement(name="P508_ESP")
    public String getP508Esp() {
        return p508Esp;
    }

    public void setP508Esp(String p508Esp) {
        this.p508Esp = p508Esp;
    }

    @XmlElement(name="P508")
    public String getP508() {
        return p508;
    }

    public void setP508(String p508) {
        this.p508 = p508;
    }

    @XmlElement(name="P509")
    public String getP509() {
        return p509;
    }

    public void setP509(String p509) {
        this.p509 = p509;
    }

    @XmlElement(name="P510")
    public String getP510() {
        return p510;
    }

    public void setP510(String p510) {
        this.p510 = p510;
    }

    @XmlElement(name="P511")
    public String getP511() {
        return p511;
    }

    public void setP511(String p511) {
        this.p511 = p511;
    }

    @XmlElement(name="P512_00")
    public Integer getP51200() {
        return p51200;
    }

    public void setP51200(Integer p51200) {
        this.p51200 = p51200;
    }
    
    @XmlElement(name="P512_01")
    public Integer getP51201() {
        return p51201;
    }

    public void setP51201(Integer p51201) {
        this.p51201 = p51201;
    }
    
    @XmlElement(name="P512_02")
    public Integer getP51202() {
        return p51202;
    }

    public void setP51202(Integer p51202) {
        this.p51202 = p51202;
    }
    
    @XmlElement(name="P512_03")
    public Integer getP51203() {
        return p51203;
    }

    public void setP51203(Integer p51203) {
        this.p51203 = p51203;
    }
    
    @XmlElement(name="P512_10")
    public Integer getP51210() {
        return p51210;
    }

    public void setP51210(Integer p51210) {
        this.p51210 = p51210;
    }

    @XmlElement(name="P512_11")
    public Integer getP51211() {
        return p51211;
    }

    public void setP51211(Integer p51211) {
        this.p51211 = p51211;
    }
    
    @XmlElement(name="P511_12")
    public Integer getP51212() {
        return p51212;
    }

    public void setP51212(Integer p51212) {
        this.p51212 = p51212;
    }
    
    @XmlElement(name="P512_13")
    public Integer getP51213() {
        return p51213;
    }

    public void setP51213(Integer p51213) {
        this.p51213 = p51213;
    }
    
    @XmlElement(name="P512_20")
    public Integer getP51220() {
        return p51220;
    }

    public void setP51220(Integer p51220) {
        this.p51220 = p51220;
    }

    @XmlElement(name="P512_21")
    public Integer getP51221() {
        return p51221;
    }

    public void setP51221(Integer p51221) {
        this.p51221 = p51221;
    }
    
    @XmlElement(name="P512_22")
    public Integer getP51222() {
        return p51222;
    }

    public void setP51222(Integer p51222) {
        this.p51222 = p51222;
    }
    
    @XmlElement(name="P512_23")
    public Integer getP51223() {
        return p51223;
    }

    public void setP51123(Integer p51223) {
        this.p51223 = p51223;
    }    
    
    @XmlElement(name="P512_30")
    public Integer getP51230() {
        return p51230;
    }

    public void setP51230(Integer p51230) {
        this.p51230 = p51230;
    }

    @XmlElement(name="P512_31")
    public Integer getP51231() {
        return p51231;
    }

    public void setP51231(Integer p51231) {
        this.p51231 = p51231;
    }
    
    @XmlElement(name="P512_32")
    public Integer getP51232() {
        return p51232;
    }

    public void setP51232(Integer p51232) {
        this.p51232 = p51232;
    }
    
    @XmlElement(name="P512_33")
    public Integer getP51233() {
        return p51233;
    }

    public void setP51233(Integer p51233) {
        this.p51233 = p51233;
    }    

    @XmlElement(name="P513_1_PIE")
    public String getP5131Pie() {
        return p5131Pie;
    }

    public void setP5131Pie(String p5131Pie) {
        this.p5131Pie = p5131Pie;
    }

    @XmlElement(name="P513_1_ACE")
    public String getP5131Ace() {
        return p5131Ace;
    }

    public void setP5131Ace(String p5131Ace) {
        this.p5131Ace = p5131Ace;
    }

    @XmlElement(name="P513_1_OMN")
    public String getP5131Omn() {
        return p5131Omn;
    }

    public void setP5131Omn(String p5131Omn) {
        this.p5131Omn = p5131Omn;
    }

    @XmlElement(name="P513_1_CAN")
    public String getP5131Can() {
        return p5131Can;
    }

    public void setP5131Can(String p5131Can) {
        this.p5131Can = p5131Can;
    }

    @XmlElement(name="P513_1_LAN")
    public String getP5131Lan() {
        return p5131Lan;
    }

    public void setP5131Lan(String p5131Lan) {
        this.p5131Lan = p5131Lan;
    }

    @XmlElement(name="P513_1_AVI")
    public String getP5131Avi() {
        return p5131Avi;
    }

    public void setP5131Avi(String p5131Avi) {
        this.p5131Avi = p5131Avi;
    }

    @XmlElement(name="P513_2_PIE")
    public String getP5132Pie() {
        return p5132Pie;
    }

    public void setP5132Pie(String p5132Pie) {
        this.p5132Pie = p5132Pie;
    }

    @XmlElement(name="P513_2_ACE")
    public String getP5132Ace() {
        return p5132Ace;
    }

    public void setP5132Ace(String p5132Ace) {
        this.p5132Ace = p5132Ace;
    }

    @XmlElement(name="P513_2_OMN")
    public String getP5132Omn() {
        return p5132Omn;
    }

    public void setP5132Omn(String p5132Omn) {
        this.p5132Omn = p5132Omn;
    }

    @XmlElement(name="P513_2_CAN")
    public String getP5132Can() {
        return p5132Can;
    }

    public void setP5132Can(String p5132Can) {
        this.p5132Can = p5132Can;
    }

    @XmlElement(name="P513_2_LAN")
    public String getP5132Lan() {
        return p5132Lan;
    }

    public void setP5132Lan(String p5132Lan) {
        this.p5132Lan = p5132Lan;
    }

    @XmlElement(name="P513_2_AVI")
    public String getP5132Avi() {
        return p5132Avi;
    }

    public void setP5132Avi(String p5132Avi) {
        this.p5132Avi = p5132Avi;
    }

    @XmlElement(name="P513_3_PIE")
    public String getP5133Pie() {
        return p5133Pie;
    }

    public void setP5133Pie(String p5133Pie) {
        this.p5133Pie = p5133Pie;
    }

    @XmlElement(name="P513_3_ACE")
    public String getP5133Ace() {
        return p5133Ace;
    }

    public void setP5133Ace(String p5133Ace) {
        this.p5133Ace = p5133Ace;
    }

    @XmlElement(name="P513_3_OMN")
    public String getP5133Omn() {
        return p5133Omn;
    }

    public void setP5133Omn(String p5133Omn) {
        this.p5133Omn = p5133Omn;
    }

    @XmlElement(name="P513_3_CAN")
    public String getP5133Can() {
        return p5133Can;
    }

    public void setP5133Can(String p5133Can) {
        this.p5133Can = p5133Can;
    }

    @XmlElement(name="P513_3_LAN")
    public String getP5133Lan() {
        return p5133Lan;
    }

    public void setP5133Lan(String p5133Lan) {
        this.p5133Lan = p5133Lan;
    }

    @XmlElement(name="P513_3_AVI")
    public String getP5133Avi() {
        return p5133Avi;
    }

    public void setP5133Avi(String p5133Avi) {
        this.p5133Avi = p5133Avi;
    }

    @XmlElement(name="P514_1_DIA")
    public String getP5141Dia() {
        return p5141Dia;
    }

    public void setP5141Dia(String p5141Dia) {
        this.p5141Dia = p5141Dia;
    }

    @XmlElement(name="P514_1_HOR")
    public String getP5141Hor() {
        return p5141Hor;
    }

    public void setP5141Hor(String p5141Hor) {
        this.p5141Hor = p5141Hor;
    }

    @XmlElement(name="P514_1_MIN")
    public String getP5141Min() {
        return p5141Min;
    }

    public void setP5141Min(String p5141Min) {
        this.p5141Min = p5141Min;
    }

    @XmlElement(name="P514_2_DIA")
    public String getP5142Dia() {
        return p5142Dia;
    }

    public void setP5142Dia(String p5142Dia) {
        this.p5142Dia = p5142Dia;
    }

    @XmlElement(name="P514_2_HOR")
    public String getP5142Hor() {
        return p5142Hor;
    }

    public void setP5142Hor(String p5142Hor) {
        this.p5142Hor = p5142Hor;
    }

    @XmlElement(name="P514_2_MIN")
    public String getP5142Min() {
        return p5142Min;
    }

    public void setP5142Min(String p5142Min) {
        this.p5142Min = p5142Min;
    }

    @XmlElement(name="P514_3_DIA")
    public String getP5143Dia() {
        return p5143Dia;
    }

    public void setP5143Dia(String p5143Dia) {
        this.p5143Dia = p5143Dia;
    }

    @XmlElement(name="P514_3_HOR")
    public String getP5143Hor() {
        return p5143Hor;
    }

    public void setP5143Hor(String p5143Hor) {
        this.p5143Hor = p5143Hor;
    }

    @XmlElement(name="P514_3_MIN")
    public String getP5143Min() {
        return p5143Min;
    }

    public void setP5143Min(String p5143Min) {
        this.p5143Min = p5143Min;
    }

    @XmlTransient
    public Matricula2014Cabecera getMatricula2014Cabecera() {
        return matricula2014Cabecera;
    }

    public void setMatricula2014Cabecera(Matricula2014Cabecera matricula2014Cabecera) {
        this.matricula2014Cabecera = matricula2014Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2014Localpronoei)) {
            return false;
        }
        Matricula2014Localpronoei other = (Matricula2014Localpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
