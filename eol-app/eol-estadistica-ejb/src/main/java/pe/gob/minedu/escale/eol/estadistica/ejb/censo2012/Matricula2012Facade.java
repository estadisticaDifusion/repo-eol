/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb.censo2012;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Matricula2012Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author DSILVA
 */
@Singleton
public class Matricula2012Facade extends AbstractFacade<Matricula2012Cabecera> {

	public static final String CEDULA_MATRICULA = "MATRICULA";
	static final Logger LOGGER = Logger.getLogger(Matricula2012Facade.class.getName());
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03A = "c03a";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08A = "c08a";
	public final static String CEDULA_09A = "c09a";
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Matricula2012Facade() {
		super(Matricula2012Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Matricula2012Cabecera cedula) {
		String sql = "UPDATE Matricula2012Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroCed=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroCed());
		query.executeUpdate();

		/*
		 * if (cedula.getDetalleMatricula() != null) { Set<String> keys =
		 * cedula.getDetalleMatricula().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2012Matricula detalle = cedula.getDetalleMatricula().get(key);
		 * detalle.setMatricula2012Cabecera(cedula); List<Matricula2012MatriculaFila>
		 * filas = detalle.getMatricula2012MatriculaFilaList(); for
		 * (Matricula2012MatriculaFila fila : filas) {
		 * fila.setMatricula2012Matricula(detalle); } } }
		 * 
		 * if (cedula.getDetalleRecursos() != null) { Set<String> keys =
		 * cedula.getDetalleRecursos().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2012Recursos detalle = cedula.getDetalleRecursos().get(key);
		 * detalle.setMatricula2012Cabecera(cedula); List<Matricula2012RecursosFila>
		 * filas = detalle.getMatricula2012RecursosFilaList(); if(filas!=null) for
		 * (Matricula2012RecursosFila fila : filas) {
		 * fila.setMatricula2012Recursos(detalle); } } }
		 * 
		 * if (cedula.getDetalleSeccion() != null) { Set<String> keys =
		 * cedula.getDetalleSeccion().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2012Seccion detalle = cedula.getDetalleSeccion().get(key);
		 * detalle.setMatricula2012Cabecera(cedula); List<Matricula2012SeccionFila>
		 * filas = detalle.getMatricula2012SeccionFilaList(); for
		 * (Matricula2012SeccionFila fila : filas) {
		 * fila.setMatricula2012Seccion(detalle); } } }
		 * 
		 * if (cedula.getDetalleSaanee() != null) { Set<String> keys =
		 * cedula.getDetalleSaanee().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next(); Matricula2012Saanee
		 * detalle = cedula.getDetalleSaanee().get(key);
		 * detalle.setMatricula2012Cabecera(cedula); List<Matricula2012SaaneeFila> filas
		 * = detalle.getMatricula2012SaaneeFilaList(); for (Matricula2012SaaneeFila fila
		 * : filas) { fila.setMatricula2012Saanee(detalle); } } }
		 * 
		 * if(cedula.getMatricula2012CoordpronoeiList()!=null){
		 * for(Matricula2012Coordpronoei
		 * coord:cedula.getMatricula2012CoordpronoeiList()){
		 * coord.setMatricula2012Cabecera(cedula); } }
		 * if(cedula.getMatricula2012LocalpronoeiList()!=null){
		 * for(Matricula2012Localpronoei
		 * loc_pro:cedula.getMatricula2012LocalpronoeiList()){
		 * loc_pro.setMatricula2012Cabecera(cedula); } }
		 * 
		 * if(cedula.getMatricula2012PersonalList()!=null){ for(Matricula2012Personal
		 * per:cedula.getMatricula2012PersonalList()) {
		 * per.setMatricula2012Cabecera(cedula); } }
		 * 
		 * if(cedula.getMatricula2012CetproCursoList()!=null){
		 * for(Matricula2012CetproCurso cc:cedula.getMatricula2012CetproCursoList()){
		 * cc.setMatricula2012Cabecera(cedula); } }
		 */

		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

}
