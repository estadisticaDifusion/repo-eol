/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.ejb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author JMATAMOROS
 */
@Entity(name = "EolMensajeria")
@Table(name = "eol_mensajeria")
public class EolMensajeria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "USUARIO_D")
	private String usuarioDestino;

	@Column(name = "ASUNTO")
	private String asunto;

	@Lob
	@Column(name = "MENSAJE")
	private String mensaje;

	@Column(name = "ESTADO")
	private Integer estado;

	@Basic(optional = false)
	@Column(name = "FECHA_CREA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCrea;

	@Column(name = "USUARIO_O")
	private String usuarioOrigen;

	@Column(name = "IMPORTANCIA")
	private Integer importancia;

	@Column(name = "ARCHIVO_ADJUNTO")
	private String archivoAdjunto;

	@Transient
	private boolean leido = false;

	public EolMensajeria() {
	}

	public EolMensajeria(Integer id) {
		this.id = id;
	}

	public EolMensajeria(Integer id, Date fechaCrea) {
		this.id = id;
		this.fechaCrea = fechaCrea;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public Integer getImportancia() {
		return importancia;
	}

	public void setImportancia(Integer importancia) {
		this.importancia = importancia;
	}

	public String getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(String archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EolMensajeria)) {
			return false;
		}
		EolMensajeria other = (EolMensajeria) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.portlets.ejb.domain.EolMensajeria[id=" + id + "]";
	}

	/**
	 * @return the leido
	 */
	public boolean isLeido() {
		return leido;
	}

	/**
	 * @param leido the leido to set
	 */
	public void setLeido(boolean leido) {
		this.leido = leido;
	}

	/**
	 * @return the usuarioDestino
	 */
	public String getUsuarioDestino() {
		return usuarioDestino;
	}

	/**
	 * @param usuarioDestino the usuarioDestino to set
	 */
	public void setUsuarioDestino(String usuarioDestino) {
		this.usuarioDestino = usuarioDestino;
	}

	/**
	 * @return the usuarioOrigen
	 */
	public String getUsuarioOrigen() {
		return usuarioOrigen;
	}

	/**
	 * @param usuarioOrigen the usuarioOrigen to set
	 */
	public void setUsuarioOrigen(String usuarioOrigen) {
		this.usuarioOrigen = usuarioOrigen;
	}

}
