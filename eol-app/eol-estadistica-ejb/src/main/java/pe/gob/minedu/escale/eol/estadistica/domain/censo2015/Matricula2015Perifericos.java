package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "matricula2015_perifericos")
public class Matricula2015Perifericos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "NRO")
    private Integer nro;
    @Column(name = "DESCRI")
    private String descri;
    @Column(name = "DATO01")
    private String dato01;
    @Column(name = "DATO02")
    private String dato02;
    @Column(name = "DATO03")
    private String dato03;
    @Column(name = "DATO04")
    private String dato04;
    @Column(name = "DATO05")
    private String dato05;
    @Column(name = "DATO06")
    private String dato06;
    @Column(name = "DATO07")
    private String dato07;
    @Column(name = "DATO08")
    private String dato08;
    @Column(name = "DATO09")
    private String dato09;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2015Cabecera matricula2015Cabecera;

    public Matricula2015Perifericos() {
    }

    public Matricula2015Perifericos(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="NRO")
    public Integer getNro() {
        return nro;
    }

    public void setNro(Integer nro) {
        this.nro = nro;
    }

    @XmlElement(name="DESCRI")
    public String getDescri() {
        return descri;
    }

    public void setDescri(String descri) {
        this.descri = descri;
    }

    @XmlElement(name="DATO01")
    public String getDato01() {
        return dato01;
    }

    public void setDato01(String dato01) {
        this.dato01 = dato01;
    }

    @XmlElement(name="DATO02")
    public String getDato02() {
        return dato02;
    }

    public void setDato02(String dato02) {
        this.dato02 = dato02;
    }

    @XmlElement(name="DATO03")
    public String getDato03() {
        return dato03;
    }

    public void setDato03(String dato03) {
        this.dato03 = dato03;
    }

    @XmlElement(name="DATO04")
    public String getDato04() {
        return dato04;
    }

    public void setDato04(String dato04) {
        this.dato04 = dato04;
    }

    @XmlElement(name="DATO05")
    public String getDato05() {
        return dato05;
    }

    public void setDato05(String dato05) {
        this.dato05 = dato05;
    }

    @XmlElement(name="DATO06")
    public String getDato06() {
        return dato06;
    }

    public void setDato06(String dato06) {
        this.dato06 = dato06;
    }

    @XmlElement(name="DATO07")
    public String getDato07() {
        return dato07;
    }

    public void setDato07(String dato07) {
        this.dato07 = dato07;
    }

    @XmlElement(name="DATO08")
    public String getDato08() {
        return dato08;
    }

    public void setDato08(String dato08) {
        this.dato08 = dato08;
    }

    @XmlElement(name="DATO09")
    public String getDato09() {
        return dato09;
    }

    public void setDato09(String dato09) {
        this.dato09 = dato09;
    }

    @XmlTransient
    public Matricula2015Cabecera getMatricula2015Cabecera() {
        return matricula2015Cabecera;
    }

    public void setMatricula2015Cabecera(Matricula2015Cabecera matricula2015Cabecera) {
        this.matricula2015Cabecera = matricula2015Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2015Perifericos)) {
            return false;
        }
        Matricula2015Perifericos other = (Matricula2015Perifericos) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
