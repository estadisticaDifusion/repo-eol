/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "matricula2018_localpronoei")
public class Matricula2018Localpronoei implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P601_01")
    private String p60101;
    @Column(name = "P602")
    private String p602;
    @Column(name = "P603")
    private String p603;
    @Column(name = "P604")
    private String p604;
    @Column(name = "P605")
    private String p605;
    @Column(name = "P605_7_CM")
    private String p6057Cm;
    @Column(name = "P605_7_NM")
    private String p6057Nm;
    @Column(name = "P605_ESP")
    private String p605Esp;
    @Column(name = "P606")
    private String p606;
    @Column(name = "P606_SUM")
    private String p606Sum;
    @Column(name = "P607")
    private String p607;
    @Column(name = "P607_SUM")
    private String p607Sum;
    @Column(name = "P607_ESP")
    private String p607Esp;
    @Column(name = "P608")
    private String p608;
    @Column(name = "P609")
    private String p609;
    @Column(name = "P610")
    private String p610;
    @Column(name = "P611")
    private String p611;
    @Column(name = "P611_ESP")
    private String p611Esp;
    @Column(name = "P612")
    private String p612;
    @Column(name = "P612_ESP")
    private String p612Esp;
    @Column(name = "P613")
    private String p613;
    @Column(name = "P613_ESP")
    private String p613Esp;
    @Column(name = "P614")
    private String p614;
    @Column(name = "P615")
    private String p615;
    @Column(name = "P616")
    private String p616;
    @Column(name = "P617")
    private String p617;
    @Column(name = "P618")
    private String p618;
    @Column(name = "P620")
    private Integer p620;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2018Cabecera matricula2018Cabecera;

    public Matricula2018Localpronoei() {
    }

    public Matricula2018Localpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "P601_01")
    public String getP60101() {
        return p60101;
    }

    public void setP60101(String p60101) {
        this.p60101 = p60101;
    }

    @XmlElement(name = "P602")
    public String getP602() {
        return p602;
    }

    public void setP602(String p602) {
        this.p602 = p602;
    }

    @XmlElement(name = "P603")
    public String getP603() {
        return p603;
    }

    public void setP603(String p603) {
        this.p603 = p603;
    }

    @XmlElement(name = "P604")
    public String getP604() {
        return p604;
    }

    public void setP604(String p604) {
        this.p604 = p604;
    }

    @XmlElement(name = "P605")
    public String getP605() {
        return p605;
    }

    public void setP605(String p605) {
        this.p605 = p605;
    }

    @XmlElement(name = "P605_7_CM")
    public String getP6057Cm() {
        return p6057Cm;
    }

    public void setP6057Cm(String p6057Cm) {
        this.p6057Cm = p6057Cm;
    }

    @XmlElement(name = "P605_7_NM")
    public String getP6057Nm() {
        return p6057Nm;
    }

    public void setP6057Nm(String p6057Nm) {
        this.p6057Nm = p6057Nm;
    }

    @XmlElement(name = "P605_ESP")
    public String getP605Esp() {
        return p605Esp;
    }

    public void setP605Esp(String p605Esp) {
        this.p605Esp = p605Esp;
    }

    @XmlElement(name = "P606")
    public String getP606() {
        return p606;
    }

    public void setP606(String p606) {
        this.p606 = p606;
    }

    @XmlElement(name = "P606_SUM")
    public String getP606Sum() {
        return p606Sum;
    }

    public void setP606Sum(String p606Sum) {
        this.p606Sum = p606Sum;
    }

    @XmlElement(name = "P607")
    public String getP607() {
        return p607;
    }

    public void setP607(String p607) {
        this.p607 = p607;
    }

    @XmlElement(name = "P607_SUM")
    public String getP607Sum() {
        return p607Sum;
    }

    public void setP607Sum(String p607Sum) {
        this.p607Sum = p607Sum;
    }

    @XmlElement(name = "P607_ESP")
    public String getP607Esp() {
        return p607Esp;
    }

    public void setP607Esp(String p607Esp) {
        this.p607Esp = p607Esp;
    }

    @XmlElement(name = "P608")
    public String getP608() {
        return p608;
    }

    public void setP608(String p608) {
        this.p608 = p608;
    }

    @XmlElement(name = "P609")
    public String getP609() {
        return p609;
    }

    public void setP609(String p609) {
        this.p609 = p609;
    }

    @XmlElement(name = "P610")
    public String getP610() {
        return p610;
    }

    public void setP610(String p610) {
        this.p610 = p610;
    }

    @XmlElement(name = "P611")
    public String getP611() {
        return p611;
    }

    public void setP611(String p611) {
        this.p611 = p611;
    }

    @XmlElement(name = "P611_ESP")
    public String getP611Esp() {
        return p611Esp;
    }

    public void setP611Esp(String p611Esp) {
        this.p611Esp = p611Esp;
    }

    @XmlElement(name = "P612")
    public String getP612() {
        return p612;
    }

    public void setP612(String p612) {
        this.p612 = p612;
    }

    @XmlElement(name = "P612_ESP")
    public String getP612Esp() {
        return p612Esp;
    }

    public void setP612Esp(String p612Esp) {
        this.p612Esp = p612Esp;
    }

    @XmlElement(name = "P613")
    public String getP613() {
        return p613;
    }

    public void setP613(String p613) {
        this.p613 = p613;
    }

    @XmlElement(name = "P613_ESP")
    public String getP613Esp() {
        return p613Esp;
    }

    public void setP613Esp(String p613Esp) {
        this.p613Esp = p613Esp;
    }

    @XmlElement(name = "P614")
    public String getP614() {
        return p614;
    }

    public void setP614(String p614) {
        this.p614 = p614;
    }

    @XmlElement(name = "P615")
    public String getP615() {
        return p615;
    }

    public void setP615(String p615) {
        this.p615 = p615;
    }

    @XmlElement(name = "P616")
    public String getP616() {
        return p616;
    }

    public void setP616(String p616) {
        this.p616 = p616;
    }

    @XmlElement(name = "P617")
    public String getP617() {
        return p617;
    }

    public void setP617(String p617) {
        this.p617 = p617;
    }

    @XmlElement(name = "P618")
    public String getP618() {
        return p618;
    }

    public void setP618(String p618) {
        this.p618 = p618;
    }

    @XmlElement(name = "P620")
    public Integer getP620() {
        return p620;
    }

    public void setP620(Integer p620) {
        this.p620 = p620;
    }

    @XmlTransient
    public Matricula2018Cabecera getMatricula2018Cabecera() {
        return matricula2018Cabecera;
    }

    public void setMatricula2018Cabecera(Matricula2018Cabecera matricula2018Cabecera) {
        this.matricula2018Cabecera = matricula2018Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2018Localpronoei)) {
            return false;
        }
        Matricula2018Localpronoei other = (Matricula2018Localpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Localpronoei[idEnvio=" + idEnvio + "]";
    }

}
