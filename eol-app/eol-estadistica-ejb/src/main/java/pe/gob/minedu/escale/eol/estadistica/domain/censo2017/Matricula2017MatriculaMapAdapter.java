package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Matricula2017MatriculaMapAdapter.Matricula2017MatriculaList;

public class Matricula2017MatriculaMapAdapter
		extends XmlAdapter<Matricula2017MatriculaList, Map<String, Matricula2017Matricula>> {

	private static final Logger LOGGER = Logger.getLogger(Matricula2017MatriculaMapAdapter.class.getName());

	static class Matricula2017MatriculaList {

		private List<Matricula2017Matricula> detalle;

		private Matricula2017MatriculaList(ArrayList<Matricula2017Matricula> lista) {
			detalle = lista;
		}

		public Matricula2017MatriculaList() {
		}

		@XmlElement(name = "CUADROS")
		public List<Matricula2017Matricula> getDetalle() {
			return detalle;
		}

		public void setDetalle(List<Matricula2017Matricula> detalle) {
			this.detalle = detalle;
		}
	}

	@Override
	public Map<String, Matricula2017Matricula> unmarshal(Matricula2017MatriculaList v) throws Exception {

		Map<String, Matricula2017Matricula> map = new HashMap<String, Matricula2017Matricula>();
		for (Matricula2017Matricula detalle : v.getDetalle()) {
			map.put(detalle.getCuadro(), detalle);
		}
		LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[] { v, map });
		return map;
	}

	@Override
	public Matricula2017MatriculaList marshal(Map<String, Matricula2017Matricula> v) throws Exception {
		if (v == null)
			return null;

		Set<String> keySet = v.keySet();
		if (keySet.isEmpty()) {
			LOGGER.fine("no hay claves");
			return null;

		}
		ArrayList<Matricula2017Matricula> lista = new ArrayList<Matricula2017Matricula>();
		for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
			String key = it.next();
			Matricula2017Matricula $detalle = v.get(key);
			$detalle.setCuadro(key);
			lista.add($detalle);
		}
		Matricula2017MatriculaList list = new Matricula2017MatriculaList(lista);
		LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[] { v, lista });
		return list;
	}
}
