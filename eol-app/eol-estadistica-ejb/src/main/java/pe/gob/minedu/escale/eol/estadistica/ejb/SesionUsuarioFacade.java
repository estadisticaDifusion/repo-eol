/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.domain.SesionUsuario;

/**
 *
 * @author DSILVA
 */
@Singleton
public class SesionUsuarioFacade extends AbstractFacade<SesionUsuario> {

	private Logger logger = Logger.getLogger(SesionUsuarioFacade.class);

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	@PersistenceContext(unitName = "eol-PU-Auth")
	private EntityManager emAuth;

	public SesionUsuarioFacade() {
		super(SesionUsuario.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/**
	 * Verifica que la sesion iniciada por el mismo codigo modular esté activa
	 *
	 * @param codigoModular El código modular enviado en el documento
	 * @param idSesion      la sesión (token) tomada al momento del logeo
	 * @return true si la sesión está activa y dentro de la hora de la sesión.
	 */
	public boolean sesionActiva(String codigoModular, long idSesion) {
		SesionUsuario sesion = find(Long.valueOf(idSesion));
		if (sesion == null) {
			return false;
		}
		// if (!sesion.getUsuario().equals("i" + codigoModular))
		if (!sesion.getUsuario().equals(codigoModular)) {
			return false;
		}
		Date now = new Date();
		return now.before(sesion.getCaduca()) && now.after(sesion.getInicio());
	}

	public boolean sesionActivaAuth(String codigoModular, long idSesion) {
		SesionUsuario sesion = find(Long.valueOf(idSesion));
		if (sesion == null) {
			return false;
		}
		if (!sesion.getUsuario().equals(codigoModular)) {
			return false;
		}
		Date now = new Date();
		return now.before(sesion.getCaduca()) && now.after(sesion.getInicio());
	}
 
	@Lock(LockType.READ)
	public AuthUsuario userAuthValid(String usuario, String contrasenia) {
		logger.info(":: SesionUsuarioFacade.userAuthValid :: Starting execution...");
		AuthUsuario user = this.emAuth.find(AuthUsuario.class, usuario);
	
		
		
		
		
		
		
		if (user != null && contrasenia != null && user.getContrasenia().trim().equals(contrasenia.trim())) {
			logger.info("  value user: " + user);
			logger.info(":: SesionUsuarioFacade.userAuthValid :: Execution finish");
			return user;
		}
		
		
		logger.info(":: SesionUsuarioFacade.userAuthValid :: Execution finish. null");
		return null;
	}

	public AuthUsuario userAuthValid(String usuario) {
		AuthUsuario user = this.emAuth.find(AuthUsuario.class, usuario);
		if (user != null) {
			return user;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<AuthUsuario> getListaCentrosRegistrados(String idRegistrador) {
		return emAuth
				.createQuery("SELECT u FROM AuthUsuario u WHERE u.idRegistrador like :idRegistrador AND u.tipo='CE'")
				.setParameter("idRegistrador", idRegistrador).getResultList();

	}

	// imendoza
	public int update(AuthUsuario entity) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" UPDATE auth.auth_usuario SET ");
		sbQuery.append(" contrasenia= ?1 ");
		sbQuery.append(" WHERE usuario = ?2 ");

		Query query = emAuth.createNativeQuery(sbQuery.toString());

		query = (entity.getContrasenia() != null) ? query.setParameter(1, entity.getContrasenia()) : query;
		query = (entity.getUsuario() != null) ? query.setParameter(2, entity.getUsuario()) : query;
		int exito = 0;
		exito = query.executeUpdate();

		return exito;
	}
	// imendoza

	@Lock(LockType.READ)
	public SesionUsuario obtenerSesionUsuario(String usuario) {
		logger.info(":: SesionUsuarioFacade.obtenerSesionUsuario :: Starting execution...");

		StringBuilder sbQuery = new StringBuilder();
		Long idSesion = 0L;

		sbQuery.append(" SELECT IFNULL(MAX(id_sesion), 0) AS idsession FROM est_sesiones ");
		sbQuery.append(" WHERE usuario = ?1 ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query = (usuario != null) ? query.setParameter(1, usuario) : query;

		Object obj = query.getSingleResult();
		logger.info("value obj return: " + obj + " class: " + obj.getClass());
		if (obj instanceof BigInteger) {
			idSesion = ((BigInteger) obj).longValue();
		} else {
			if (obj instanceof Long) {
				idSesion = (Long) obj;
			}
		}
		logger.info("value idSession: " + idSesion);

		// Obtenemos la ultima sesion
		SesionUsuario sesion = find(Long.valueOf(idSesion));

		// Creamos la nueva sesion
		SesionUsuario newSesion = new SesionUsuario();
		newSesion.setUsuario(usuario);
		newSesion.setInicio(new Date());
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, 1); // caduca en una hora
		newSesion.setCaduca(cal.getTime());

		create(newSesion);

		logger.info(":: SesionUsuarioFacade.obtenerSesionUsuario :: Execution finish.");
		return sesion;
	}

}
