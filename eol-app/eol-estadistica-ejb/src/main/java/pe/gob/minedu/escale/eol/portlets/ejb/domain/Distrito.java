/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.ejb.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(schema = "padron", name = "distritos")
public class Distrito implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2712648905250477855L;

	@Id
	@Column(name = "id_distrito")
	private String idDistrito;

	@Column(name = "distrito")
	private String nombreDistrito;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_provincia", referencedColumnName = "id_provincia")
	private Provincia provincia;

	@Column(name = "point_x")
	private double pointX;

	@Column(name = "point_y")
	private double pointY;

	@Column(name = "zoom")
	private int zoom;

	public Distrito() {
	}

	public Distrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getNombreDistrito() {
		return nombreDistrito;
	}

	public void setNombreDistrito(String nombreDistrito) {
		this.nombreDistrito = nombreDistrito;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public double getPointX() {
		return pointX;
	}

	public void setPointX(double pointX) {
		this.pointX = pointX;
	}

	public double getPointY() {
		return pointY;
	}

	public void setPointY(double pointY) {
		this.pointY = pointY;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = zoom;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Distrito other = (Distrito) obj;
		if ((this.idDistrito == null) ? (other.idDistrito != null) : !this.idDistrito.equals(other.idDistrito)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 29 * hash + (this.idDistrito != null ? this.idDistrito.hashCode() : 0);
		return hash;
	}

	@Override
	public String toString() {
		return getProvincia().getRegion().getNombreRegion() + " / " + getProvincia().getNombreProvincia() + " / "
				+ nombreDistrito;
	}
}
