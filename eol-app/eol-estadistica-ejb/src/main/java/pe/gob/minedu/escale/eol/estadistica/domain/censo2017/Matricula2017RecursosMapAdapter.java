package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Matricula2017RecursosMapAdapter.Matricula2017RecursosList;

public class Matricula2017RecursosMapAdapter  extends XmlAdapter<Matricula2017RecursosList, Map<String, Matricula2017Recursos>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2017RecursosMapAdapter.class.getName());

    static class Matricula2017RecursosList {

        private List<Matricula2017Recursos> detalle;

        private Matricula2017RecursosList(ArrayList<Matricula2017Recursos> lista) {
            detalle = lista;
        }

        public Matricula2017RecursosList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2017Recursos> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2017Recursos> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2017Recursos> unmarshal(Matricula2017RecursosList v) throws Exception {

        Map<String, Matricula2017Recursos> map = new HashMap<String, Matricula2017Recursos>();
        for (Matricula2017Recursos detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2017RecursosList marshal(Map<String, Matricula2017Recursos> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2017Recursos> lista = new ArrayList<Matricula2017Recursos>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2017Recursos $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2017RecursosList list = new Matricula2017RecursosList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
