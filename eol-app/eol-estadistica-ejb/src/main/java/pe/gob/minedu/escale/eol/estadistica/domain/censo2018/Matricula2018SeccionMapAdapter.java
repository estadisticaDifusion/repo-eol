package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2018.Matricula2018SeccionMapAdapter.Matricula2018SeccionList;

public class Matricula2018SeccionMapAdapter
		extends XmlAdapter<Matricula2018SeccionList, Map<String, Matricula2018Seccion>> {

	private static final Logger LOGGER = Logger.getLogger(Matricula2018SeccionMapAdapter.class.getName());

	static class Matricula2018SeccionList {

		private List<Matricula2018Seccion> detalle;

		private Matricula2018SeccionList(ArrayList<Matricula2018Seccion> lista) {
			detalle = lista;
		}

		public Matricula2018SeccionList() {
		}

		@XmlElement(name = "CUADROS")
		public List<Matricula2018Seccion> getDetalle() {
			return detalle;
		}

		public void setDetalle(List<Matricula2018Seccion> detalle) {
			this.detalle = detalle;
		}
	}

	@Override
	public Map<String, Matricula2018Seccion> unmarshal(Matricula2018SeccionList v) throws Exception {

		Map<String, Matricula2018Seccion> map = new HashMap<String, Matricula2018Seccion>();
		for (Matricula2018Seccion detalle : v.getDetalle()) {
			map.put(detalle.getCuadro(), detalle);
		}
		LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[] { v, map });
		return map;
	}

	@Override
	public Matricula2018SeccionList marshal(Map<String, Matricula2018Seccion> v) throws Exception {
		if (v == null)
			return null;
		Set<String> keySet = v.keySet();

		if (keySet.isEmpty()) {
			LOGGER.fine("no hay claves");
			return null;

		}
		ArrayList<Matricula2018Seccion> lista = new ArrayList<Matricula2018Seccion>();
		for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
			String key = it.next();
			Matricula2018Seccion $detalle = v.get(key);
			$detalle.setCuadro(key);
			lista.add($detalle);
		}
		Matricula2018SeccionList list = new Matricula2018SeccionList(lista);
		LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[] { v, lista });
		return list;
	}

}
