package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "local2016_sec402")
public class Local2016Sec402 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P402_1")
    private Integer p4021;
    @Column(name = "P402_2")
    private Integer p4022;
    @Column(name = "P402_3")
    private Integer p4023;
    @Column(name = "P402_4")
    private String p4024;
    @Column(name = "P402_5")
    private String p4025;
    @Column(name = "P402_6")
    private String p4026;
    @Column(name = "P402_7")
    private String p4027;
    @Column(name = "P402_8")
    private String p4028;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2016Cabecera local2016Cabecera;

    public Local2016Sec402() {
    }

    public Local2016Sec402(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    @XmlElement(name = "P402_1")
    public Integer getP4021() {
        return p4021;
    }

    public void setP4021(Integer p4021) {
        this.p4021 = p4021;
    }
    @XmlElement(name = "P402_2")
    public Integer getP4022() {
        return p4022;
    }

    public void setP4022(Integer p4022) {
        this.p4022 = p4022;
    }
    @XmlElement(name = "P402_3")
    public Integer getP4023() {
        return p4023;
    }

    public void setP4023(Integer p4023) {
        this.p4023 = p4023;
    }
    @XmlElement(name = "P402_4")
    public String getP4024() {
        return p4024;
    }

    public void setP4024(String p4024) {
        this.p4024 = p4024;
    }
    @XmlElement(name = "P402_5")
    public String getP4025() {
        return p4025;
    }

    public void setP4025(String p4025) {
        this.p4025 = p4025;
    }
    @XmlElement(name = "P402_6")
    public String getP4026() {
        return p4026;
    }

    public void setP4026(String p4026) {
        this.p4026 = p4026;
    }
    @XmlElement(name = "P402_7")
    public String getP4027() {
        return p4027;
    }

    public void setP4027(String p4027) {
        this.p4027 = p4027;
    }
    @XmlElement(name = "P402_8")
    public String getP4028() {
        return p4028;
    }

    public void setP4028(String p4028) {
        this.p4028 = p4028;
    }
    @XmlTransient
    public Local2016Cabecera getLocal2016Cabecera() {
        return local2016Cabecera;
    }

    public void setLocal2016Cabecera(Local2016Cabecera local2016Cabecera) {
        this.local2016Cabecera = local2016Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2016Sec402)) {
            return false;
        }
        Local2016Sec402 other = (Local2016Sec402) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Local2016Sec402[idEnvio=" + idEnvio + "]";
    }

}
