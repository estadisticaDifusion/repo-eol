/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Matricula2014RecursosMapAdapter.Matricula2014RecursosList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2014RecursosMapAdapter extends XmlAdapter<Matricula2014RecursosList, Map<String, Matricula2014Recursos>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2014RecursosMapAdapter.class.getName());

    static class Matricula2014RecursosList {

        private List<Matricula2014Recursos> detalle;

        private Matricula2014RecursosList(ArrayList<Matricula2014Recursos> lista) {
            detalle = lista;
        }

        public Matricula2014RecursosList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2014Recursos> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2014Recursos> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2014Recursos> unmarshal(Matricula2014RecursosList v) throws Exception {

        Map<String, Matricula2014Recursos> map = new HashMap<String, Matricula2014Recursos>();
        for (Matricula2014Recursos detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2014RecursosList marshal(Map<String, Matricula2014Recursos> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2014Recursos> lista = new ArrayList<Matricula2014Recursos>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2014Recursos $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2014RecursosList list = new Matricula2014RecursosList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
