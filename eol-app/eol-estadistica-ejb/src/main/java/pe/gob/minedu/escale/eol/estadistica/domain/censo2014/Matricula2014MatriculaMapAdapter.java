/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Matricula2014MatriculaMapAdapter.Matricula2014MatriculaList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2014MatriculaMapAdapter extends XmlAdapter<Matricula2014MatriculaList, Map<String, Matricula2014Matricula>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2014MatriculaMapAdapter.class.getName());

    static class Matricula2014MatriculaList {

        private List<Matricula2014Matricula> detalle;

        private Matricula2014MatriculaList(ArrayList<Matricula2014Matricula> lista) {
            detalle = lista;
        }

        public Matricula2014MatriculaList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2014Matricula> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2014Matricula> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2014Matricula> unmarshal(Matricula2014MatriculaList v) throws Exception {

        Map<String, Matricula2014Matricula> map = new HashMap<String, Matricula2014Matricula>();
        for (Matricula2014Matricula detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2014MatriculaList marshal(Map<String, Matricula2014Matricula> v) throws Exception {
        if(v==null)return null;

        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2014Matricula> lista = new ArrayList<Matricula2014Matricula>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2014Matricula $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2014MatriculaList list = new Matricula2014MatriculaList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
