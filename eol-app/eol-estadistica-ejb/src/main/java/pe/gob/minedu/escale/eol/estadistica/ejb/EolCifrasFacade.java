/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.EolIndicadorCifras;

/**
 *
 * @author CMOLINA
 */
@Stateless
public class EolCifrasFacade {

	static final Logger logger = Logger.getLogger(EolIndicadorCifras.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public List<EolIndicadorCifras> listaCifras() {

		Query q = null;

		try {
			q = (Query) em.createQuery("SELECT e FROM EolIndicadorCifras e");

			return q.getResultList();
		} catch (Exception as) {
			as.printStackTrace();
			return null;
		}
	}
}
