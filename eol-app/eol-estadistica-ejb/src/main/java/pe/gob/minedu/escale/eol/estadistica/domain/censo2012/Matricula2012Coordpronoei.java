/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2012_coordpronoei")
public class Matricula2012Coordpronoei implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "COD_MOD")
    private String codMod;
    @Column(name = "COOR_APE")
    private String coorApe;
    @Column(name = "COOR_NOM")
    private String coorNom;
    @Column(name = "COOR_SEXO")
    private String coorSexo;
    @Column(name = "COOR_DNI")
    private String coorDni;
    @Column(name = "COOR_EDA")
    private Integer coorEda;
    @Column(name = "CONDLAB")
    private String condlab;
    @Column(name = "NIV_MAG_LP")
    private String nivMagLp;
    @Column(name = "NIV_MAG_CPM")
    private String nivMagCpm;
    @Column(name = "SITUACARGO")
    private String situacargo;
    @Column(name = "TMPO_SERV")
    private Integer tmpoServ;
    @Column(name = "TMPO_SERV_C")
    private Integer tmpoServC;
    @Column(name = "TITULO_PED")
    private String tituloPed;
    @Column(name = "NROHORAS")
    private Integer nrohoras;
    @Column(name = "NROPROG")
    private Integer nroprog;
    @Column(name = "NHCAPACITA")
    private Integer nhcapacita;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2012Cabecera matricula2012Cabecera;

    public Matricula2012Coordpronoei() {
    }

    public Matricula2012Coordpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name="COOR_APE")
    public String getCoorApe() {
        return coorApe;
    }

    public void setCoorApe(String coorApe) {
        this.coorApe = coorApe;
    }

    @XmlElement(name="COOR_NOM")
    public String getCoorNom() {
        return coorNom;
    }

    public void setCoorNom(String coorNom) {
        this.coorNom = coorNom;
    }

    @XmlElement(name="COOR_SEXO")
    public String getCoorSexo() {
        return coorSexo;
    }

    public void setCoorSexo(String coorSexo) {
        this.coorSexo = coorSexo;
    }

    @XmlElement(name="COOR_DNI")
    public String getCoorDni() {
        return coorDni;
    }

    public void setCoorDni(String coorDni) {
        this.coorDni = coorDni;
    }

    @XmlElement(name="COOR_EDA")
    public Integer getCoorEda() {
        return coorEda;
    }

    public void setCoorEda(Integer coorEda) {
        this.coorEda = coorEda;
    }

    @XmlElement(name="CONDLAB")
    public String getCondlab() {
        return condlab;
    }

    public void setCondlab(String condlab) {
        this.condlab = condlab;
    }

    @XmlElement(name="NIV_MAG_LP")
    public String getNivMagLp() {
        return nivMagLp;
    }

    public void setNivMagLp(String nivMagLp) {
        this.nivMagLp = nivMagLp;
    }

    @XmlElement(name="NIV_MAG_CPM")
    public String getNivMagCpm() {
        return nivMagCpm;
    }

    public void setNivMagCpm(String nivMagCpm) {
        this.nivMagCpm = nivMagCpm;
    }

    @XmlElement(name="SITUACARGO")
    public String getSituacargo() {
        return situacargo;
    }

    public void setSituacargo(String situacargo) {
        this.situacargo = situacargo;
    }

    @XmlElement(name="TMPO_SERV")
    public Integer getTmpoServ() {
        return tmpoServ;
    }

    public void setTmpoServ(Integer tmpoServ) {
        this.tmpoServ = tmpoServ;
    }

    @XmlElement(name="TMPO_SERV_C")
    public Integer getTmpoServC() {
        return tmpoServC;
    }

    public void setTmpoServC(Integer tmpoServC) {
        this.tmpoServC = tmpoServC;
    }

    @XmlElement(name="TITULO_PED")
    public String getTituloPed() {
        return tituloPed;
    }

    public void setTituloPed(String tituloPed) {
        this.tituloPed = tituloPed;
    }

    @XmlElement(name="NROHORAS")
    public Integer getNrohoras() {
        return nrohoras;
    }

    public void setNrohoras(Integer nrohoras) {
        this.nrohoras = nrohoras;
    }

    @XmlElement(name="NROPROG")
    public Integer getNroprog() {
        return nroprog;
    }

    public void setNroprog(Integer nroprog) {
        this.nroprog = nroprog;
    }

    @XmlElement(name="NHCAPACITA")
    public Integer getNhcapacita() {
        return nhcapacita;
    }

    public void setNhcapacita(Integer nhcapacita) {
        this.nhcapacita = nhcapacita;
    }

    @XmlTransient
    public Matricula2012Cabecera getMatricula2012Cabecera() {
        return matricula2012Cabecera;
    }

    public void setMatricula2012Cabecera(Matricula2012Cabecera matricula2012Cabecera) {
        this.matricula2012Cabecera = matricula2012Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2012Coordpronoei)) {
            return false;
        }
        Matricula2012Coordpronoei other = (Matricula2012Coordpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

   
}
