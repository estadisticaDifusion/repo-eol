/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2013_saanee")
public class Matricula2013Saanee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2013Cabecera matricula2013Cabecera;
/*
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "matricula2013Saanee")
    private List<Matricula2013SaaneeFila> matricula2013SaaneeFilaList;
*/
    public Matricula2013Saanee() {
    }

    public Matricula2013Saanee(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Matricula2013Cabecera getMatricula2013Cabecera() {
        return matricula2013Cabecera;
    }

    public void setMatricula2013Cabecera(Matricula2013Cabecera matricula2013Cabecera) {
        this.matricula2013Cabecera = matricula2013Cabecera;
    }
/*
    @XmlElement(name="SAANEE_FILAS")
    public List<Matricula2013SaaneeFila> getMatricula2013SaaneeFilaList() {
        return matricula2013SaaneeFilaList;
    }

    public void setMatricula2013SaaneeFilaList(List<Matricula2013SaaneeFila> matricula2013SaaneeFilaList) {
        this.matricula2013SaaneeFilaList = matricula2013SaaneeFilaList;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2013Saanee)) {
            return false;
        }
        Matricula2013Saanee other = (Matricula2013Saanee) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2013.tmp.Matricula2013Saanee[idEnvio=" + idEnvio + "]";
    }

}
