package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2016.Matricula2016RecursosMapAdapter.Matricula2016RecursosList;

public class Matricula2016RecursosMapAdapter  extends XmlAdapter<Matricula2016RecursosList, Map<String, Matricula2016Recursos>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2016RecursosMapAdapter.class.getName());

    static class Matricula2016RecursosList {

        private List<Matricula2016Recursos> detalle;

        private Matricula2016RecursosList(ArrayList<Matricula2016Recursos> lista) {
            detalle = lista;
        }

        public Matricula2016RecursosList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2016Recursos> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2016Recursos> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2016Recursos> unmarshal(Matricula2016RecursosList v) throws Exception {

        Map<String, Matricula2016Recursos> map = new HashMap<String, Matricula2016Recursos>();
        for (Matricula2016Recursos detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2016RecursosList marshal(Map<String, Matricula2016Recursos> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2016Recursos> lista = new ArrayList<Matricula2016Recursos>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2016Recursos $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2016RecursosList list = new Matricula2016RecursosList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
