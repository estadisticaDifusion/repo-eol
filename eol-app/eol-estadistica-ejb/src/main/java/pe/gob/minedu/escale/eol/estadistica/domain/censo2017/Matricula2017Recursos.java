/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_recursos")
public class Matricula2017Recursos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    
    @Column(name = "CUADRO")
    private String cuadro;
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Recursos")
    private List<Matricula2017RecursosFila> matricula2017RecursosFilaList;
    */
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2017Cabecera matricula2017Cabecera;

    public Matricula2017Recursos() {
    }

    public Matricula2017Recursos(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    //@XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }
    
    /*
    //@XmlElement(name="RECURSOS_FILAS")
    public List<Matricula2017RecursosFila> getMatricula2017RecursosFilaList() {
        return matricula2017RecursosFilaList;
    }

    public void setMatricula2017RecursosFilaList(List<Matricula2017RecursosFila> matricula2017RecursosFilaList) {
        this.matricula2017RecursosFilaList = matricula2017RecursosFilaList;
    }
    */
    
    //@XmlTransient
    public Matricula2017Cabecera getMatricula2017Cabecera() {
        return matricula2017Cabecera;
    }

    public void setMatricula2017Cabecera(Matricula2017Cabecera matricula2017Cabecera) {
        this.matricula2017Cabecera = matricula2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2017Recursos)) {
            return false;
        }
        Matricula2017Recursos other = (Matricula2017Recursos) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Matricula2017Recursos[idEnvio=" + idEnvio + "]";
    }

}
