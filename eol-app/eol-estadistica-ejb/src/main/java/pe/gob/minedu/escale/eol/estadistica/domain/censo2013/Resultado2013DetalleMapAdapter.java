/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2013.Resultado2013DetalleMapAdapter.Resultado2013DetalleList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Resultado2013DetalleMapAdapter extends XmlAdapter<Resultado2013DetalleList, Map<String, Resultado2013Detalle>> {

    private static final Logger LOGGER = Logger.getLogger(Resultado2013DetalleMapAdapter.class.getName());

    static class Resultado2013DetalleList {

        private List<Resultado2013Detalle> detalle;

        private Resultado2013DetalleList(ArrayList<Resultado2013Detalle> lista) {
            detalle = lista;
        }

        public Resultado2013DetalleList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Resultado2013Detalle> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Resultado2013Detalle> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Resultado2013Detalle> unmarshal(Resultado2013DetalleList v) throws Exception {

        Map<String, Resultado2013Detalle> map = new HashMap<String, Resultado2013Detalle>();
        for (Resultado2013Detalle detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Resultado2013DetalleList marshal(Map<String, Resultado2013Detalle> v) throws Exception {
        if(v==null)
        {   LOGGER.fine("no hay detalles");
            return null;            
        }
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Resultado2013Detalle> lista = new ArrayList<Resultado2013Detalle>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Resultado2013Detalle $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Resultado2013DetalleList list = new Resultado2013DetalleList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
