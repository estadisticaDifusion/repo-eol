/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb.censo2014;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Matricula2014Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author DSILVA
 */
@Singleton
public class Matricula2014Facade extends AbstractFacade<Matricula2014Cabecera> {

	public static final String CEDULA_MATRICULA = "MATRICULA";
	static final Logger LOGGER = Logger.getLogger(Matricula2014Facade.class.getName());
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03A = "c03a";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08A = "c08a";
	public final static String CEDULA_09A = "c09a";
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Matricula2014Facade() {
		super(Matricula2014Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Matricula2014Cabecera cedula) {
		String sql = "UPDATE Matricula2014Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroCed=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroCed());
		query.executeUpdate();

		/*
		 * OM if (cedula.getDetalleMatricula() != null) { Set<String> keys =
		 * cedula.getDetalleMatricula().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2014Matricula detalle = cedula.getDetalleMatricula().get(key);
		 * detalle.setMatricula2014Cabecera(cedula); List<Matricula2014MatriculaFila>
		 * filas = detalle.getMatricula2014MatriculaFilaList(); for
		 * (Matricula2014MatriculaFila fila : filas) {
		 * fila.setMatricula2014Matricula(detalle); } } }
		 * 
		 * if (cedula.getDetalleRecursos() != null) { Set<String> keys =
		 * cedula.getDetalleRecursos().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2014Recursos detalle = cedula.getDetalleRecursos().get(key);
		 * detalle.setMatricula2014Cabecera(cedula); List<Matricula2014RecursosFila>
		 * filas = detalle.getMatricula2014RecursosFilaList(); if (filas != null) for
		 * (Matricula2014RecursosFila fila : filas) {
		 * fila.setMatricula2014Recursos(detalle); } } }
		 * 
		 * if (cedula.getDetalleSeccion() != null) { Set<String> keys =
		 * cedula.getDetalleSeccion().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2014Seccion detalle = cedula.getDetalleSeccion().get(key);
		 * detalle.setMatricula2014Cabecera(cedula); List<Matricula2014SeccionFila>
		 * filas = detalle.getMatricula2014SeccionFilaList(); for
		 * (Matricula2014SeccionFila fila : filas) {
		 * fila.setMatricula2014Seccion(detalle); } } }
		 * 
		 * if (cedula.getDetalleDocente() != null) { Set<String> keys =
		 * cedula.getDetalleDocente().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2014Docente detalle = cedula.getDetalleDocente().get(key);
		 * detalle.setMatricula2014Cabecera(cedula); List<Matricula2014DocenteFila>
		 * filas = detalle.getMatricula2014DocenteFilaList(); for
		 * (Matricula2014DocenteFila fila : filas) {
		 * fila.setMatricula2014Docente(detalle); } } }
		 * 
		 * if (cedula.getMatricula2014CoordpronoeiList() != null) { for
		 * (Matricula2014Coordpronoei coord : cedula.getMatricula2014CoordpronoeiList())
		 * { coord.setMatricula2014Cabecera(cedula); } } if
		 * (cedula.getMatricula2014LocalpronoeiList() != null) { for
		 * (Matricula2014Localpronoei loc_pro :
		 * cedula.getMatricula2014LocalpronoeiList()) {
		 * loc_pro.setMatricula2014Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2014SaaneeList() != null) { for (Matricula2014Saanee
		 * per : cedula.getMatricula2014SaaneeList()) {
		 * per.setMatricula2014Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2014PersonalList() != null) { for
		 * (Matricula2014Personal per : cedula.getMatricula2014PersonalList()) {
		 * per.setMatricula2014Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2014PerifericosList() != null) { for
		 * (Matricula2014Perifericos per : cedula.getMatricula2014PerifericosList()) {
		 * per.setMatricula2014Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2014CetproCursoList() != null) { for
		 * (Matricula2014CetproCurso cc : cedula.getMatricula2014CetproCursoList()) {
		 * cc.setMatricula2014Cabecera(cedula); } }
		 */

		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

}
