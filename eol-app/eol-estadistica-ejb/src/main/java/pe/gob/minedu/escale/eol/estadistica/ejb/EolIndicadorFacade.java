/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.domain.EolIndicadorVariable;

/**
 *
 * @author CMOLINA
 */
@Stateless
public class EolIndicadorFacade {

	private Logger logger = Logger.getLogger(EolIndicadorFacade.class);

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public List<EolIndicadorVariable> listaIndicador() {

		Query q = null;

		try {
			q = (Query) em.createQuery("SELECT e FROM EolIndicadorVariable e");

			return q.getResultList();
		} catch (Exception as) {
			logger.error("error en EolIndicadorFacade.listaIndicador", as);
			as.printStackTrace();
			return null;
		}
	}
}
