package pe.gob.minedu.escale.eol.estadistica.ejb.censo2017;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Censo2017Cedulad;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Local
public interface CedulaDLocal {
	void create(Censo2017Cedulad cedulad);
}
