/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.util.enumerations;

/**
 *
 * @author JMATAMOROS
 */
public enum RoleEnum {
    IE(1),
    UGEL(2),
    DRE(3);

    private int cod;

    RoleEnum(int cod)
    {   this.cod=cod;                
    }

    /**
     * @return the cod
     */
    public int getCod() {
        return cod;
    }

    /**
     * @param cod the cod to set
     */
    public void setCod(int cod) {
        this.cod = cod;
    }

}
