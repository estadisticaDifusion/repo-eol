/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "resultado2012_fila")
public class Resultado2012Fila implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "DESCRI")
    private String descri;
    @Column(name = "T_M")
    private Integer tM;
    @Column(name = "T_H")
    private Integer tH;
    @Column(name = "DATO01H")
    private Integer dato01h;
    @Column(name = "DATO01M")
    private Integer dato01m;
    @Column(name = "DATO02H")
    private Integer dato02h;
    @Column(name = "DATO02M")
    private Integer dato02m;
    @Column(name = "DATO03H")
    private Integer dato03h;
    @Column(name = "DATO03M")
    private Integer dato03m;
    @Column(name = "DATO04H")
    private Integer dato04h;
    @Column(name = "DATO04M")
    private Integer dato04m;
    @Column(name = "DATO05H")
    private Integer dato05h;
    @Column(name = "DATO05M")
    private Integer dato05m;
    @Column(name = "DATO06H")
    private Integer dato06h;
    @Column(name = "DATO06M")
    private Integer dato06m;
    @Column(name = "DATO07H")
    private Integer dato07h;
    @Column(name = "DATO07M")
    private Integer dato07m;
    @Column(name = "DATO08H")
    private Integer dato08h;
    @Column(name = "DATO08M")
    private Integer dato08m;
    @Column(name = "DATO09H")
    private Integer dato09h;
    @Column(name = "DATO09M")
    private Integer dato09m;
    @Column(name = "DATO10H")
    private Integer dato10h;
    @Column(name = "DATO10M")
    private Integer dato10m;
    @JoinColumn(name = "DETALLE_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Resultado2012Detalle detalle;

    public Resultado2012Fila() {
    }

    public Resultado2012Fila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }
    
    @XmlElement(name="T_M")
    public Integer getTM() {
        return tM;
    }

    public void setTM(Integer tM) {
        this.tM = tM;
    }

    @XmlElement(name="T_H")
    public Integer getTH() {
        return tH;
    }

    public void setTH(Integer tH) {
        this.tH = tH;
    }

    @XmlElement(name="DATO01H")
    public Integer getDato01h() {
        return dato01h;
    }

    public void setDato01h(Integer dato01h) {
        this.dato01h = dato01h;
    }

    @XmlElement(name="DATO01M")
    public Integer getDato01m() {
        return dato01m;
    }

    public void setDato01m(Integer dato01m) {
        this.dato01m = dato01m;
    }

    @XmlElement(name="DATO02H")
    public Integer getDato02h() {
        return dato02h;
    }

    public void setDato02h(Integer dato02h) {
        this.dato02h = dato02h;
    }

    @XmlElement(name="DATO02M")
    public Integer getDato02m() {
        return dato02m;
    }

    public void setDato02m(Integer dato02m) {
        this.dato02m = dato02m;
    }

    @XmlElement(name="DATO03H")
    public Integer getDato03h() {
        return dato03h;
    }

    public void setDato03h(Integer dato03h) {
        this.dato03h = dato03h;
    }

    @XmlElement(name="DATO03M")
    public Integer getDato03m() {
        return dato03m;
    }

    public void setDato03m(Integer dato03m) {
        this.dato03m = dato03m;
    }

    @XmlElement(name="DATO04H")
    public Integer getDato04h() {
        return dato04h;
    }

    public void setDato04h(Integer dato04h) {
        this.dato04h = dato04h;
    }

    @XmlElement(name="DATO04M")
    public Integer getDato04m() {
        return dato04m;
    }

    public void setDato04m(Integer dato04m) {
        this.dato04m = dato04m;
    }

    @XmlElement(name="DATO05H")
    public Integer getDato05h() {
        return dato05h;
    }

    public void setDato05h(Integer dato05h) {
        this.dato05h = dato05h;
    }

    @XmlElement(name="DATO05M")
    public Integer getDato05m() {
        return dato05m;
    }

    public void setDato05m(Integer dato05m) {
        this.dato05m = dato05m;
    }

    @XmlElement(name="DATO06H")
    public Integer getDato06h() {
        return dato06h;
    }

    public void setDato06h(Integer dato06h) {
        this.dato06h = dato06h;
    }

    @XmlElement(name="DATO06M")
    public Integer getDato06m() {
        return dato06m;
    }

    public void setDato06m(Integer dato06m) {
        this.dato06m = dato06m;
    }

    @XmlElement(name="DATO07H")
    public Integer getDato07h() {
        return dato07h;
    }

    public void setDato07h(Integer dato07h) {
        this.dato07h = dato07h;
    }

    @XmlElement(name="DATO07M")
    public Integer getDato07m() {
        return dato07m;
    }

    public void setDato07m(Integer dato07m) {
        this.dato07m = dato07m;
    }

    @XmlElement(name="DATO08H")
    public Integer getDato08h() {
        return dato08h;
    }

    public void setDato08h(Integer dato08h) {
        this.dato08h = dato08h;
    }

    @XmlElement(name="DATO08M")
    public Integer getDato08m() {
        return dato08m;
    }

    public void setDato08m(Integer dato08m) {
        this.dato08m = dato08m;
    }

    @XmlElement(name="DATO09H")
    public Integer getDato09h() {
        return dato09h;
    }

    public void setDato09h(Integer dato09h) {
        this.dato09h = dato09h;
    }

    @XmlElement(name="DATO09M")
    public Integer getDato09m() {
        return dato09m;
    }

    public void setDato09m(Integer dato09m) {
        this.dato09m = dato09m;
    }

    @XmlElement(name="DATO10H")
    public Integer getDato10h() {
        return dato10h;
    }

    public void setDato10h(Integer dato10h) {
        this.dato10h = dato10h;
    }

    @XmlElement(name="DATO10M")
    public Integer getDato10m() {
        return dato10m;
    }

    public void setDato10m(Integer dato10m) {
        this.dato10m = dato10m;
    }

    @XmlTransient
    public Resultado2012Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Resultado2012Detalle detalle) {
        this.detalle = detalle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado2012Fila)) {
            return false;
        }
        Resultado2012Fila other = (Resultado2012Fila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @XmlElement(name="DESCRI")
    public String getDescri() {
        return descri;
    }
    
    public void setDescri(String descri) {
        this.descri = descri;
    }

   

   

}
