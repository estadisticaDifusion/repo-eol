/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Matricula2014SeccionMapAdapter.Matricula2014SeccionList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2014SeccionMapAdapter extends XmlAdapter<Matricula2014SeccionList, Map<String, Matricula2014Seccion>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2014SeccionMapAdapter.class.getName());

    static class Matricula2014SeccionList {

        private List<Matricula2014Seccion> detalle;

        private Matricula2014SeccionList(ArrayList<Matricula2014Seccion> lista) {
            detalle = lista;
        }

        public Matricula2014SeccionList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2014Seccion> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2014Seccion> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2014Seccion> unmarshal(Matricula2014SeccionList v) throws Exception {

        Map<String, Matricula2014Seccion> map = new HashMap<String, Matricula2014Seccion>();
        for (Matricula2014Seccion detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2014SeccionList marshal(Map<String, Matricula2014Seccion> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2014Seccion> lista = new ArrayList<Matricula2014Seccion>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2014Seccion $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2014SeccionList list = new Matricula2014SeccionList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
