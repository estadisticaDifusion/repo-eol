package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "matricula2016_matricula")
public class Matricula2016Matricula implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2016Cabecera matricula2016Cabecera;
    
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2016Matricula")
    private List<Matricula2016MatriculaFila> matricula2016MatriculaFilaList;
*/
    public Matricula2016Matricula() {
    }

    public Matricula2016Matricula(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Matricula2016Cabecera getMatricula2016Cabecera() {
        return matricula2016Cabecera;
    }

    public void setMatricula2016Cabecera(Matricula2016Cabecera matricula2016Cabecera) {
        this.matricula2016Cabecera = matricula2016Cabecera;
    }
/*
    @XmlElement(name="MATRICULA_FILAS")
    public List<Matricula2016MatriculaFila> getMatricula2016MatriculaFilaList() {
        return matricula2016MatriculaFilaList;
    }

    public void setMatricula2016MatriculaFilaList(List<Matricula2016MatriculaFila> matricula2016MatriculaFilaList) {
        this.matricula2016MatriculaFilaList = matricula2016MatriculaFilaList;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2016Matricula)) {
            return false;
        }
        Matricula2016Matricula other = (Matricula2016Matricula) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
