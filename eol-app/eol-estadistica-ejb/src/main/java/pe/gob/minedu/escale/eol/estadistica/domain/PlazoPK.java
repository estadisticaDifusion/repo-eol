/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author DSILVA
 */
@Embeddable
public class PlazoPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9000456695335148999L;
	@Basic(optional = false)
	@Column(name = "periodo", nullable = false, length = 4)
	private String periodo;
	@Basic(optional = false)
	@Column(name = "proceso", nullable = false, length = 105)
	private String proceso;

	public PlazoPK() {
	}

	public PlazoPK(String periodo, String proceso) {
		this.periodo = periodo;
		this.proceso = proceso;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (periodo != null ? periodo.hashCode() : 0);
		hash += (proceso != null ? proceso.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof PlazoPK)) {
			return false;
		}
		PlazoPK other = (PlazoPK) object;
		if ((this.periodo == null && other.periodo != null)
				|| (this.periodo != null && !this.periodo.equals(other.periodo))) {
			return false;
		}
		if ((this.proceso == null && other.proceso != null)
				|| (this.proceso != null && !this.proceso.equals(other.proceso))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.PlazoPK[periodo=" + periodo + ", proceso=" + proceso + "]";
	}

}
