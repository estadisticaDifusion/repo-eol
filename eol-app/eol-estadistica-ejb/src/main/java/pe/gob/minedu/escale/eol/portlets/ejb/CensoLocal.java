package pe.gob.minedu.escale.eol.portlets.ejb;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.converter.EolCedulaConverter;
import pe.gob.minedu.escale.eol.converter.EolIECensoConverter;
import pe.gob.minedu.escale.eol.dto.estadistica.EolEnvioIECensoDTO;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.EolCenso;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Local
public interface CensoLocal {
	List<EolCenso> findPadron(String codmod, String anexo, String codlocal, String ubigeo, String codooii,
			List<String> niveles, List<String> gestiones, List<String> areas, String estado, List<String> tipoices,
			List<String> formas, List<String> estados, String idActividad);

	Long cantidadPadron(String codmod, String anexo, String codlocal, String ubigeo, String codooii,
			List<String> niveles, List<String> gestiones, List<String> areas, String estado, List<String> tipoices,
			List<String> formas, List<String> estados, String idActividad);

	List<EolIECensoConverter> getConverterCenso(String codmod, String nivel, String anexo, String codlocal);

	List<EolIECensoConverter> cadCensoByCodmod(String nivel, String codmod, String anexo);

	List<EolIECensoConverter> cadCensoCedulaSByCodmod(String nivel, String codmod, String anexo);

	List<EolIECensoConverter> cadCensoByCodLocal(String codlocal);

	List<EolIECensoConverter> cadCedulaDCodLocal(String codlocal);

	List<EolIECensoConverter> cadCensoByCodID(String codId);

	List<EolIECensoConverter> cadCensoByCodIDNivMod(String codId, String codmod, String nivmod);

	List<EolIECensoConverter> cadCensoTotalporCodigos(String nivmod, String codmod, String anexo, String codlocal,
			String codinst);

	List<EolCedulaConverter> cadNivAndAnio(String nivel, String anio);
	
	List<EolEnvioIECensoDTO> censoTotalListByCodigo(String nivmod, String codmod, String anexo, String codlocal,
			String codinst);

}
