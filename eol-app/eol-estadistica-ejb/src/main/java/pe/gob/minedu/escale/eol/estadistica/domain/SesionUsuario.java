/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author DSILVA
 */
@Entity

@Table(name = "est_sesiones")
public class SesionUsuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2306653364454028192L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sesion")
	private Long idSesion;
	@Column(name = "usuario")
	private String usuario;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date inicio;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date caduca;

	public Date getCaduca() {
		return caduca;
	}

	public void setCaduca(Date caduca) {
		this.caduca = caduca;
	}

	public Long getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(Long idSesion) {
		this.idSesion = idSesion;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public String getUsuario() {

		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
