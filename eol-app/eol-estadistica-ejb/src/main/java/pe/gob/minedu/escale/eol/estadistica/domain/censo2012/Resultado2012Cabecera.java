/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulaResultado2012")
@Entity
@Table(name = "resultado2012_cabecera")
public class Resultado2012Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    public final static String CEDULA_01B = "c01b";
    public final static String CEDULA_02B = "c02b";
    public final static String CEDULA_03B = "c03b";
    public final static String CEDULA_04B = "c04b";    
    public final static String CEDULA_05B = "c05b";
    public final static String CEDULA_06B = "c06b";
    public final static String CEDULA_07B = "c07b";
    public final static String CEDULA_08B = "c08b";
    public final static String CEDULA_09B = "c09b";
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Transient
    private long token;
    @Column(name = "NIV_MOD")
    private String nivMod;
    @Column(name = "CEN_EDU")
    private String cenEdu;
    @Column(name = "COD_MOD")
    private String codMod;
    @Column(name = "ANEXO")
    private String anexo;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "NROCED")
    private String nroced;
    @Column(name = "FORMATEN")
    private String formaten;
    @Column(name = "SEANEXO")
    private Boolean seanexo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "TIPPROG")
    private String tipprog;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "SEARTI")
    private Boolean searti;
    @Column(name = "CODOOII")
    private String codooii;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "VERSION")
    private String version;

    @Column(name = "APE_DIR")
    private String apeDir;

    @Column(name = "NOM_DIR")
    private String nomDir;

    @Column(name = "SITUACION")
    private String situacion;
    /*
    @MapKeyColumn(name = "CUADRO", length = 5)
    @OneToMany(mappedBy = "cedula", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String,Resultado2012Detalle> detalle;
*/
    @Transient
    private String msg;

    @Transient
    private String estadoRpt;

    public Resultado2012Cabecera() {
    }

    public Resultado2012Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NIV_MOD")
    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

   

    @XmlElement(name = "CEN_EDU")
    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    @XmlElement(name = "COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name = "ANEXO")
    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "FORMATEN")
    public String getFormaten() {
        return formaten;
    }

    public void setFormaten(String formaten) {
        this.formaten = formaten;
    }

    @XmlElement(name = "SEANEXO")
    public Boolean getSeanexo() {
        return seanexo;
    }

    public void setSeanexo(Boolean seanexo) {
        this.seanexo = seanexo;
    }

    @XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

   
    @XmlElement(name = "TIPPROG")
    public String getTipprog() {
        return tipprog;
    }

    public void setTipprog(String tipprog) {
        this.tipprog = tipprog;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "SEARTI")
    public Boolean getSearti() {
        return searti;
    }

    public void setSearti(Boolean searti) {
        this.searti = searti;
    }

    @XmlElement(name = "CODOOII")
    public String getCodooii() {
        return codooii;
    }

    public void setCodooii(String codooii) {
        this.codooii = codooii;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlTransient
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }
   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {        
        if (!(object instanceof Resultado2012Cabecera)) {
            return false;
        }
        Resultado2012Cabecera other = (Resultado2012Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

/*
    @XmlJavaTypeAdapter(Resultado2012DetalleMapAdapter.class)
    @XmlElement(name = "DETALLE")
    public Map<String, Resultado2012Detalle> getDetalle() {
        return detalle;
    }


    public void setDetalle(Map<String, Resultado2012Detalle> detalle) {
        this.detalle = detalle;
    }
*/

    @XmlAttribute
    public long getToken() {
        return token;
    }


    public void setToken(long token) {
        this.token = token;
    }


    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }


    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

   
    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name = "APE_DIR")
    public String getApeDir() {
        return apeDir;
    }

    public void setApeDir(String apeDir) {
        this.apeDir = apeDir;
    }

    @XmlElement(name = "NOM_DIR")
    public String getNomDir() {
        return nomDir;
    }


    public void setNomDir(String nomDir) {
        this.nomDir = nomDir;
    }

    @XmlElement(name = "SITUACION")
    public String getSituacion() {
        return situacion;
    }

    /**
     * @param situacion the situacion to set
     */
    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }
}
