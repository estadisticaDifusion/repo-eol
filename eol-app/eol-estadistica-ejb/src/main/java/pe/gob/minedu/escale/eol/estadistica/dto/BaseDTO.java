/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author omateo
 */
public class BaseDTO implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long usuarioRegistro;
    private Date fechaRegistro;
    private Long usuarioActualiza;
    private Date fechaActualiza;

    public Long getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Long usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Long getUsuarioActualiza() {
        return usuarioActualiza;
    }

    public void setUsuarioActualiza(Long usuarioActualiza) {
        this.usuarioActualiza = usuarioActualiza;
    }

    public Date getFechaActualiza() {
        return fechaActualiza;
    }

    public void setFechaActualiza(Date fechaActualiza) {
        this.fechaActualiza = fechaActualiza;
    }

}
