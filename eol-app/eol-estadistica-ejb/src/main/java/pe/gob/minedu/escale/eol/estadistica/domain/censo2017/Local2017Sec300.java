/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "local2017_sec300")
public class Local2017Sec300 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "CUADRO")
    private String cuadro;
    @Basic(optional = false)
    @Column(name = "NRO")
    private int nro;
    @Column(name = "P300_ESP")
    private String p300Esp;
    @Column(name = "P300_1")
    private Integer p3001;
    @Column(name = "P300_2")
    private Integer p3002;
    @Column(name = "P300_3")
    private Integer p3003;
    @Column(name = "P300_4")
    private Integer p3004;
  
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2017Cabecera local2017Cabecera;

    public Local2017Sec300() {
    }

    public Local2017Sec300(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Local2017Sec300(Long idEnvio, String cuadro, int nro) {
        this.idEnvio = idEnvio;
        this.cuadro = cuadro;
        this.nro = nro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }
    @XmlElement(name = "NRO")
    public int getNro() {
        return nro;
    }

    public void setNro(int nro) {
        this.nro = nro;
    }
    @XmlElement(name = "P300_ESP")
    public String getP300Esp() {
        return p300Esp;
    }

    public void setP300Esp(String p300Esp) {
        this.p300Esp = p300Esp;
    }
    @XmlElement(name = "P300_1")
    public Integer getP3001() {
        return p3001;
    }

    public void setP3001(Integer p3001) {
        this.p3001 = p3001;
    }
    @XmlElement(name = "P300_2")
    public Integer getP3002() {
        return p3002;
    }

    public void setP3002(Integer p3002) {
        this.p3002 = p3002;
    }
    @XmlElement(name = "P300_3")
    public Integer getP3003() {
        return p3003;
    }

    public void setP3003(Integer p3003) {
        this.p3003 = p3003;
    }
    @XmlElement(name = "P300_4")
    public Integer getP3004() {
        return p3004;
    }

    public void setP3004(Integer p3004) {
        this.p3004 = p3004;
    }
   
    @XmlTransient
    public Local2017Cabecera getLocal2017Cabecera() {
        return local2017Cabecera;
    }

    public void setLocal2017Cabecera(Local2017Cabecera local2017Cabecera) {
        this.local2017Cabecera = local2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2017Sec300)) {
            return false;
        }
        Local2017Sec300 other = (Local2017Sec300) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Local2017Sec300[idEnvio=" + idEnvio + "]";
    }

}
