/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "local2017_sec207")
public class Local2017Sec207 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P207_1")
    private Integer p2071;
    @Column(name = "P207_2")
    private Integer p2072;
    @Column(name = "P207_3")
    private Integer p2073;
    @Column(name = "P207_4")
    private String p2074;
    @Column(name = "P207_5")
    private String p2075;
    @Column(name = "P207_6")
    private String p2076;
    @Column(name = "P207_7")
    private String p2077;
    @Column(name = "P207_8")
    private String p2078;
    @Column(name = "P207_9")
    private String p2079;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2017Cabecera local2017Cabecera;

    public Local2017Sec207() {
    }

    public Local2017Sec207(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    @XmlElement(name="P207_1")
    public Integer getP2071() {
        return p2071;
    }

    public void setP2071(Integer p2071) {
        this.p2071 = p2071;
    }
    @XmlElement(name="P207_2")
    public Integer getP2072() {
        return p2072;
    }

    public void setP2072(Integer p2072) {
        this.p2072 = p2072;
    }
    @XmlElement(name="P207_3")
    public Integer getP2073() {
        return p2073;
    }

    public void setP2073(Integer p2073) {
        this.p2073 = p2073;
    }
    @XmlElement(name="P207_4")
    public String getP2074() {
        return p2074;
    }

    public void setP2074(String p2074) {
        this.p2074 = p2074;
    }
    @XmlElement(name="P207_5")
    public String getP2075() {
        return p2075;
    }

    public void setP2075(String p2075) {
        this.p2075 = p2075;
    }
    @XmlElement(name="P207_6")
    public String getP2076() {
        return p2076;
    }

    public void setP2076(String p2076) {
        this.p2076 = p2076;
    }
    @XmlElement(name="P207_7")
    public String getP2077() {
        return p2077;
    }

    public void setP2077(String p2077) {
        this.p2077 = p2077;
    }
    @XmlElement(name="P207_8")
    public String getP2078() {
        return p2078;
    }

    public void setP2078(String p2078) {
        this.p2078 = p2078;
    }
    @XmlElement(name="P207_9")
    public String getP2079() {
        return p2079;
    }

    public void setP2079(String p2079) {
        this.p2079 = p2079;
    }
    @XmlTransient
    public Local2017Cabecera getLocal2017Cabecera() {
        return local2017Cabecera;
    }

    public void setLocal2017Cabecera(Local2017Cabecera local2017Cabecera) {
        this.local2017Cabecera = local2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2017Sec207)) {
            return false;
        }
        Local2017Sec207 other = (Local2017Sec207) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo20172.Local2017Sec207[idEnvio=" + idEnvio + "]";
    }

}
