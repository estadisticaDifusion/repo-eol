package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "resultado2017_detalle")
public class Resultado2017Detalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Basic(optional = false)
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;

    @Transient
    private Boolean ultimodet;

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Resultado2017Cabecera cedula;
/*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detalle", fetch = FetchType.EAGER)
    private List<Resultado2017Fila> filas;
*/
    public Resultado2017Detalle() {
    }

    public Resultado2017Detalle(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Resultado2017Detalle(Long idEnvio, String cuadro) {
        this.idEnvio = idEnvio;
        this.cuadro = cuadro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Resultado2017Cabecera getCedula() {
        return cedula;
    }

    @XmlElement(name = "ULTIMODET")
    public Boolean getUltimodet() {
        return ultimodet;
    }

    public void setUltimodet(Boolean ultimodet) {
        this.ultimodet = ultimodet;
    }

    public void setCedula(Resultado2017Cabecera cedula) {
        this.cedula = cedula;
    }
/*
    @XmlElement(name="FILAS")
    public List<Resultado2017Fila> getFilas() {
        return filas;
    }

    public void setFilas(List<Resultado2017Fila> filas) {
        this.filas = filas;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado2017Detalle)) {
            return false;
        }
        Resultado2017Detalle other = (Resultado2017Detalle) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Resultado2017Detalle[idEnvio=" + idEnvio + "]";
    }

}
