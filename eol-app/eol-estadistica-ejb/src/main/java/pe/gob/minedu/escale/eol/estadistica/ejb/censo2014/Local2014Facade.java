/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.ejb.censo2014;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Local2014Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author JMATAMOROS
 */
@Singleton
public class Local2014Facade extends AbstractFacade<Local2014Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2014Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Local2014Facade() {
		super(Local2014Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Local2014Cabecera entity) {
		String sql = "UPDATE Local2014Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();
		/*
		 * OM for (Local2014Sec104 sec104 : entity.getSec104()) {
		 * sec104.setLocal2014Cabecera(entity); } if (entity.getSec300() != null) for
		 * (Local2014Sec300 sec300 : entity.getSec300()) {
		 * sec300.setLocal2014Cabecera(entity); }
		 * 
		 * if (entity.getSec302() != null) for (Local2014Sec302 sec302 :
		 * entity.getSec302()) { sec302.setLocal2014Cabecera(entity); }
		 * 
		 * if (entity.getSec400() != null) for (Local2014Sec400 sec400 :
		 * entity.getSec400()) { sec400.setLocal2014Cabecera(entity); }
		 * 
		 * if (entity.getSec500() != null) for (Local2014Sec500 sec500 :
		 * entity.getSec500()) { sec500.setLocal2014Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();
	}

	public Local2014Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2014Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2014Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
