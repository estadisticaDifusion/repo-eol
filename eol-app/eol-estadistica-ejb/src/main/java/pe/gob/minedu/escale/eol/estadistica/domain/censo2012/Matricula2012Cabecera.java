/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2012_cabecera")
public class Matricula2012Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    public final static String CEDULA_01A = "c01a";
    public final static String CEDULA_02A = "c02a";
    public final static String CEDULA_03A = "c03a";
    public final static String CEDULA_04A = "c04a";    
    public final static String CEDULA_05A = "c05a";
    public final static String CEDULA_06A = "c06a";
    public final static String CEDULA_07A = "c07a";
    public final static String CEDULA_08A = "c08a";
    public final static String CEDULA_09A = "c09a";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "COD_MOD")
    private String codMod;
    @Basic(optional = false)
    @Column(name = "ANEXO")
    private String anexo;
    @Basic(optional = false)
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOPROG")
    private String tipoprog;
    @Column(name = "CEN_EDU")
    private String cenEdu;
    @Column(name = "TELEFONO")
    private String telefono;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Column(name = "NOMBVIA")
    private String nombvia;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "MANZANA")
    private String manzana;
    @Column(name = "LOTE")
    private String lote;
    @Column(name = "CATLUGAR")
    private String catlugar;
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Column(name = "ETAPA")
    private String etapa;
    @Column(name = "SECTOR")
    private String sector;
    @Column(name = "ZONA")
    private String zona;
    @Column(name = "OTRO")
    private String otro;
    @Column(name = "CATCP")
    private String catCP;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "PAG_WEB")
    private String pagWeb;
    @Column(name = "REFERENCIA")
    private String referencia;
    @Basic(optional = false)
    @Column(name = "ID_UBIGEO")
    private String idUbigeo;
    @Basic(optional = false)
    @Column(name = "DISTRITO")
    private String distrito;
    @Basic(optional = false)
    @Column(name = "NIV_MOD")
    private String nivMod;

    @Column(name = "NROCED")
    private String nroCed;

    @Basic(optional = false)
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "FORMATEN")
    private String formaten;
    @Column(name = "MODATEN")
    private String modaten;
    @Column(name = "TIPONEE")
    private String tiponee;
    @Column(name = "SEARTI")
    private String searti;
    @Column(name = "SEISE")
    private String seise;
    @Column(name = "SEANEXO")
    private String seanexo;
    @Column(name = "P101_DD")
    private String p101Dd;
    @Column(name = "P101_MM")
    private String p101Mm;
    @Column(name = "P102_DD")
    private String p102Dd;
    @Column(name = "P102_MM")
    private String p102Mm;
    @Column(name = "P103_HI_H")
    private String p103HiH;
    @Column(name = "P103_HI_M")
    private String p103HiM;
    @Column(name = "P103_HT_H")
    private String p103HtH;
    @Column(name = "P103_HT_M")
    private String p103HtM;
    @Column(name = "P104")
    private String p104;
    @Column(name = "P105_1")
    private String p1051;
    @Column(name = "P105_2")
    private String p1052;
    @Column(name = "P105_3")
    private String p1053;
    @Column(name = "P105_4")
    private String p1054;
    @Column(name = "P105_5")
    private String p1055;
    @Column(name = "SERV_IE_1")
    private String servIe1;
    @Column(name = "SERV_IE_2")
    private String servIe2;
    @Column(name = "SERV_IE_3")
    private String servIe3;
    @Column(name = "SERV_IE_4")
    private String servIe4;
    @Column(name = "SAANEE")
    private String saanee;
    @Column(name = "P201")
    private String p201;
    @Column(name = "P201_ESP")
    private String p201Esp;
    @Column(name = "P202")
    private String p202;
    @Column(name = "P202_ESP")
    private String p202Esp;
    @Column(name = "PCAP_EBI")
    private String pcapEbi;
    @Column(name = "PCAP_EBI_E")
    private Integer pcapEbiE;
    @Column(name = "P203_C3")
    private String p203C3;
    @Column(name = "LENORI_1_0")
    private String lenori10;
    @Column(name = "LENORI_1_1")
    private String lenori11;
    @Column(name = "LENORI_1_2")
    private String lenori12;
    @Column(name = "LENORI_1_3")
    private String lenori13;
    @Column(name = "LENORI_1_4")
    private String lenori14;
    @Column(name = "LENORI_1_5")
    private String lenori15;
    @Column(name = "LENORI_1_6")
    private String lenori16;
    @Column(name = "LENORI_2_0")
    private String lenori20;
    @Column(name = "LENORI_2_1")
    private String lenori21;
    @Column(name = "LENORI_2_2")
    private String lenori22;
    @Column(name = "LENORI_2_3")
    private String lenori23;
    @Column(name = "LENORI_2_4")
    private String lenori24;
    @Column(name = "LENORI_2_5")
    private String lenori25;
    @Column(name = "LENORI_2_6")
    private String lenori26;
    @Column(name = "LENORI_3_0")
    private String lenori30;
    @Column(name = "LENORI_3_1")
    private String lenori31;
    @Column(name = "LENORI_3_2")
    private String lenori32;
    @Column(name = "LENORI_3_3")
    private String lenori33;
    @Column(name = "LENORI_3_4")
    private String lenori34;
    @Column(name = "LENORI_3_5")
    private String lenori35;
    @Column(name = "LENORI_3_6")
    private String lenori36;
    @Column(name = "LENORI_4_0")
    private String lenori40;
    @Column(name = "LENORI_4_1")
    private String lenori41;
    @Column(name = "LENORI_4_2")
    private String lenori42;
    @Column(name = "LENORI_4_3")
    private String lenori43;
    @Column(name = "LENORI_4_4")
    private String lenori44;
    @Column(name = "LENORI_4_5")
    private String lenori45;
    @Column(name = "LENORI_4_6")
    private String lenori46;
    @Column(name = "ENS_LENEXT")
    private String ensLenext;
    @Column(name = "ENS_LEXT_ES")
    private String ensLextEs;
    @Column(name = "LENEXT_1_0")
    private String lenext10;
    @Column(name = "LENEXT_1_1")
    private String lenext11;
    @Column(name = "LENEXT_1_2")
    private String lenext12;
    @Column(name = "LENEXT_1_3")
    private String lenext13;
    @Column(name = "LENEXT_1_4")
    private String lenext14;
    @Column(name = "LENEXT_1_5")
    private String lenext15;
    @Column(name = "LENEXT_1_6")
    private String lenext16;
    @Column(name = "LENEXT_2_0")
    private String lenext20;
    @Column(name = "LENEXT_2_1")
    private String lenext21;
    @Column(name = "LENEXT_2_2")
    private String lenext22;
    @Column(name = "LENEXT_2_3")
    private String lenext23;
    @Column(name = "LENEXT_2_4")
    private String lenext24;
    @Column(name = "LENEXT_2_5")
    private String lenext25;
    @Column(name = "LENEXT_2_6")
    private String lenext26;
    @Column(name = "LENEXT_3_0")
    private String lenext30;
    @Column(name = "LENEXT_3_1")
    private String lenext31;
    @Column(name = "LENEXT_3_2")
    private String lenext32;
    @Column(name = "LENEXT_3_3")
    private String lenext33;
    @Column(name = "LENEXT_3_4")
    private String lenext34;
    @Column(name = "LENEXT_3_5")
    private String lenext35;
    @Column(name = "LENEXT_3_6")
    private String lenext36;
    @Column(name = "LENEXT_4_0")
    private String lenext40;
    @Column(name = "LENEXT_4_1")
    private String lenext41;
    @Column(name = "LENEXT_4_2")
    private String lenext42;
    @Column(name = "LENEXT_4_3")
    private String lenext43;
    @Column(name = "LENEXT_4_4")
    private String lenext44;
    @Column(name = "LENEXT_4_5")
    private String lenext45;
    @Column(name = "LENEXT_4_6")
    private String lenext46;
    @Column(name = "P501")
    private String p501;
    @Column(name = "P502_D")
    private String p502D;
    @Column(name = "P502_M")
    private String p502M;
    @Column(name = "P502_A")
    private String p502A;
    @Column(name = "P504_C12")
    private String p504C12;
    @Column(name = "P505_C3")
    private String p505C3;
    @Column(name = "P505_C12_D")
    private String p505C12D;
    @Column(name = "P505_C12_M")
    private String p505C12M;
    @Column(name = "P505_C12_A")
    private String p505C12A;
    @Column(name = "P506_C3_D")
    private String p506C3D;
    @Column(name = "P506_C3_M")
    private String p506C3M;
    @Column(name = "P506_C3_A")
    private String p506C3A;
    @Column(name = "P508_C3")
    private String p508C3;
    @Column(name = "P509_C3_D")
    private String p509C3D;
    @Column(name = "P509_C3_M")
    private String p509C3M;
    @Column(name = "P509_C3_A")
    private String p509C3A;
    @Column(name = "BBTK_AULA_0")
    private String bbtkAula0;
    @Column(name = "BBTK_AULA_1")
    private String bbtkAula1;
    @Column(name = "BBTK_AULA_2")
    private String bbtkAula2;
    @Column(name = "BBTK_AULA_3")
    private String bbtkAula3;
    @Column(name = "BBTK_AULA_4")
    private String bbtkAula4;
    @Column(name = "BBTK_AULA_5")
    private String bbtkAula5;
    @Column(name = "BBTK_AULA_6")
    private String bbtkAula6;
    @Column(name = "BBTK_AULA_N")
    private String bbtkAulaN;
    @Column(name = "COMP_OPER")
    private String compOper;
    @Column(name = "P606_C3_T")
    private Integer p606C3T;
    @Column(name = "P606_C3_1")
    private Integer p606C31;
    @Column(name = "P606_C3_2")
    private Integer p606C32;
    @Column(name = "P606_C3_3")
    private Integer p606C33;
    @Column(name = "P607_C3")
    private String p607C3;
    @Column(name = "P608_C3_AP")
    private String p608C3Ap;
    @Column(name = "P608_C3_NO")
    private String p608C3No;
    @Column(name = "P608_C3_DNI")
    private String p608C3Dni;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "DIR_APE")
    private String dirApe;
    @Column(name = "DIR_NOM")
    private String dirNom;
    @Column(name = "DIR_SITUA")
    private String dirSitua;
    @Column(name = "DIR_DNI")
    private String dirDni;
    @Column(name = "DIR_EMAIL")
    private String dirEmail;
    @Column(name = "SDIR_APE")
    private String sdirApe;
    @Column(name = "SDIR_NOM")
    private String sdirNom;
    @Column(name = "SDIR_SITUA")
    private String sdirSitua;
    @Column(name = "SDIR_DNI")
    private String sdirDni;
    @Column(name = "SDIR_EMAIL")
    private String sdirEmail;
    @Column(name = "REC_CED_DIA")
    private String recCedDia;
    @Column(name = "REC_CED_MES")
    private String recCedMes;
    @Column(name = "CUL_CED_DIA")
    private String culCedDia;
    @Column(name = "CUL_CED_MES")
    private String culCedMes;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "FUENTE")
    private String fuente;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "EGESTORA")
    private String egestora;
    @Column(name = "TIPODFINA")
    private String tipodfina;
    @Column(name = "NUMRES")
    private String numres;
    @Column(name = "FECHARES_DD")
    private String fecharesDD;
    @Column(name = "FECHARES_MM")
    private String fecharesMM;
    @Column(name = "FECHARES_AA")
    private String fecharesAA;
/*
    @OneToMany(mappedBy = "matricula2012Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2012Personal> matricula2012PersonalList;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2012Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String,Matricula2012Recursos> detalleRecursos;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2012Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Map<String,Matricula2012Matricula> detalleMatricula;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2012Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Map<String,Matricula2012Seccion> detalleSeccion;
    
    
    @OneToMany(mappedBy = "matricula2012Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2012Localpronoei> matricula2012LocalpronoeiList;

    @OneToMany(mappedBy = "matricula2012Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2012Coordpronoei> matricula2012CoordpronoeiList;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2012Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Map<String,Matricula2012Saanee> detalleSaanee;

    @OneToMany(mappedBy = "matricula2012Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2012CetproCurso> matricula2012CetproCursoList;
*/


    @Transient
    private long token;


    @Transient
    private String msg;

    @Transient
    private String estadoRpt;


    public Matricula2012Cabecera() {
    }

    public Matricula2012Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2012Cabecera(Long idEnvio, String codMod, String anexo, String codlocal, String idUbigeo, String distrito, String nivMod, String codugel) {
        this.idEnvio = idEnvio;
        this.codMod = codMod;
        this.anexo = anexo;
        this.codlocal = codlocal;
        this.idUbigeo = idUbigeo;
        this.distrito = distrito;
        this.nivMod = nivMod;
        this.codugel = codugel;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name="ANEXO")
    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    @XmlElement(name="CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name="TIPOPROG")
    public String getTipoprog() {
        return tipoprog;
    }

    public void setTipoprog(String tipoprog) {
        this.tipoprog = tipoprog;
    }

    @XmlElement(name="CEN_EDU")
    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    @XmlElement(name="TELEFONO")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @XmlElement(name="TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name="NOMBVIA")
    public String getNombvia() {
        return nombvia;
    }

    public void setNombvia(String nombvia) {
        this.nombvia = nombvia;
    }

    @XmlElement(name="NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name="MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name="LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name="CATLUGAR")
    public String getCatlugar() {
        return catlugar;
    }

    public void setCatlugar(String catlugar) {
        this.catlugar = catlugar;
    }

    @XmlElement(name="NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name="ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name="SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name="ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name="OTRO")
    public String getOtro() {
        return otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }

    @XmlElement(name="CATCP")
    public String getCatCP() {
        return catCP;
    }

    public void setCatCP(String catCP) {
        this.catCP = catCP;
    }

    @XmlElement(name="CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name="PAG_WEB")
    public String getPagWeb() {
        return pagWeb;
    }

    public void setPagWeb(String pagWeb) {
        this.pagWeb = pagWeb;
    }

    @XmlElement(name="REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name="ID_UBIGEO")
    public String getIdUbigeo() {
        return idUbigeo;
    }

    public void setIdUbigeo(String idUbigeo) {
        this.idUbigeo = idUbigeo;
    }

    @XmlElement(name="DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name="NIV_MOD")
    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    @XmlElement(name="CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name="FORMATEN")
    public String getFormaten() {
        return formaten;
    }

    public void setFormaten(String formaten) {
        this.formaten = formaten;
    }

    @XmlElement(name="TIPONEE")
    public String getTiponee() {
        return tiponee;
    }

    public void setTiponee(String tiponee) {
        this.tiponee = tiponee;
    }

    @XmlElement(name="SEARTI")
    public String getSearti() {
        return searti;
    }

    public void setSearti(String searti) {
        this.searti = searti;
    }

    @XmlElement(name="SEISE")
    public String getSeise() {
        return seise;
    }

    public void setSeise(String seise) {
        this.seise = seise;
    }

    @XmlElement(name="SEANEXO")
    public String getSeanexo() {
        return seanexo;
    }

    public void setSeanexo(String seanexo) {
        this.seanexo = seanexo;
    }

    @XmlElement(name="P101_DD")
    public String getP101Dd() {
        return p101Dd;
    }

    public void setP101Dd(String p101Dd) {
        this.p101Dd = p101Dd;
    }

    @XmlElement(name="P101_MM")
    public String getP101Mm() {
        return p101Mm;
    }

    public void setP101Mm(String p101Mm) {
        this.p101Mm = p101Mm;
    }

    @XmlElement(name="P102_DD")
    public String getP102Dd() {
        return p102Dd;
    }

    public void setP102Dd(String p102Dd) {
        this.p102Dd = p102Dd;
    }

    @XmlElement(name="P102_MM")
    public String getP102Mm() {
        return p102Mm;
    }

    public void setP102Mm(String p102Mm) {
        this.p102Mm = p102Mm;
    }

    @XmlElement(name="P103_HI_H")
    public String getP103HiH() {
        return p103HiH;
    }

    public void setP103HiH(String p103HiH) {
        this.p103HiH = p103HiH;
    }

    @XmlElement(name="P103_HI_M")
    public String getP103HiM() {
        return p103HiM;
    }

    public void setP103HiM(String p103HiM) {
        this.p103HiM = p103HiM;
    }

    @XmlElement(name="P103_HT_H")
    public String getP103HtH() {
        return p103HtH;
    }

    public void setP103HtH(String p103HtH) {
        this.p103HtH = p103HtH;
    }

    @XmlElement(name="P103_HT_M")
    public String getP103HtM() {
        return p103HtM;
    }

    public void setP103HtM(String p103HtM) {
        this.p103HtM = p103HtM;
    }

    @XmlElement(name="P104")
    public String getP104() {
        return p104;
    }

    public void setP104(String p104) {
        this.p104 = p104;
    }

    @XmlElement(name="P105_1")
    public String getP1051() {
        return p1051;
    }

    public void setP1051(String p1051) {
        this.p1051 = p1051;
    }

    @XmlElement(name="P105_2")
    public String getP1052() {
        return p1052;
    }

    public void setP1052(String p1052) {
        this.p1052 = p1052;
    }

    @XmlElement(name="P105_3")
    public String getP1053() {
        return p1053;
    }

    public void setP1053(String p1053) {
        this.p1053 = p1053;
    }

    @XmlElement(name="P105_4")
    public String getP1054() {
        return p1054;
    }

    public void setP1054(String p1054) {
        this.p1054 = p1054;
    }

    @XmlElement(name="P105_5")
    public String getP1055() {
        return p1055;
    }

    public void setP1055(String p1055) {
        this.p1055 = p1055;
    }

    @XmlElement(name="SERV_IE_1")
    public String getServIe1() {
        return servIe1;
    }

    public void setServIe1(String servIe1) {
        this.servIe1 = servIe1;
    }

    @XmlElement(name="SERV_IE_2")
    public String getServIe2() {
        return servIe2;
    }

    public void setServIe2(String servIe2) {
        this.servIe2 = servIe2;
    }

    @XmlElement(name="SERV_IE_3")
    public String getServIe3() {
        return servIe3;
    }

    public void setServIe3(String servIe3) {
        this.servIe3 = servIe3;
    }

    @XmlElement(name="SERV_IE_4")
    public String getServIe4() {
        return servIe4;
    }

    public void setServIe4(String servIe4) {
        this.servIe4 = servIe4;
    }

    @XmlElement(name="SAANEE")
    public String getSaanee() {
        return saanee;
    }

    public void setSaanee(String saanee) {
        this.saanee = saanee;
    }

    @XmlElement(name="P201")
    public String getP201() {
        return p201;
    }

    public void setP201(String p201) {
        this.p201 = p201;
    }

    @XmlElement(name="P201_ESP")
    public String getP201Esp() {
        return p201Esp;
    }

    public void setP201Esp(String p201Esp) {
        this.p201Esp = p201Esp;
    }

    @XmlElement(name="P202")
    public String getP202() {
        return p202;
    }

    public void setP202(String p202) {
        this.p202 = p202;
    }

    @XmlElement(name="P202_ESP")
    public String getP202Esp() {
        return p202Esp;
    }

    public void setP202Esp(String p202Esp) {
        this.p202Esp = p202Esp;
    }

    @XmlElement(name="PCAP_EBI")
    public String getPcapEbi() {
        return pcapEbi;
    }

    public void setPcapEbi(String pcapEbi) {
        this.pcapEbi = pcapEbi;
    }

    @XmlElement(name="PCAP_EBI_E")
    public Integer getPcapEbiE() {
        return pcapEbiE;
    }

    public void setPcapEbiE(Integer pcapEbiE) {
        this.pcapEbiE = pcapEbiE;
    }

    @XmlElement(name="P203_C3")
    public String getP203C3() {
        return p203C3;
    }

    public void setP203C3(String p203C3) {
        this.p203C3 = p203C3;
    }

    @XmlElement(name="LENORI_1_0")
    public String getLenori10() {
        return lenori10;
    }

    public void setLenori10(String lenori10) {
        this.lenori10 = lenori10;
    }

    @XmlElement(name="LENORI_1_1")
    public String getLenori11() {
        return lenori11;
    }

    public void setLenori11(String lenori11) {
        this.lenori11 = lenori11;
    }

    @XmlElement(name="LENORI_1_2")
    public String getLenori12() {
        return lenori12;
    }

    public void setLenori12(String lenori12) {
        this.lenori12 = lenori12;
    }

    @XmlElement(name="LENORI_1_3")
    public String getLenori13() {
        return lenori13;
    }

    public void setLenori13(String lenori13) {
        this.lenori13 = lenori13;
    }

    @XmlElement(name="LENORI_1_4")
    public String getLenori14() {
        return lenori14;
    }

    public void setLenori14(String lenori14) {
        this.lenori14 = lenori14;
    }

    @XmlElement(name="LENORI_1_5")
    public String getLenori15() {
        return lenori15;
    }

    public void setLenori15(String lenori15) {
        this.lenori15 = lenori15;
    }

    @XmlElement(name="LENORI_1_6")
    public String getLenori16() {
        return lenori16;
    }

    public void setLenori16(String lenori16) {
        this.lenori16 = lenori16;
    }

    @XmlElement(name="LENORI_2_0")
    public String getLenori20() {
        return lenori20;
    }

    public void setLenori20(String lenori20) {
        this.lenori20 = lenori20;
    }

    @XmlElement(name="LENORI_2_1")
    public String getLenori21() {
        return lenori21;
    }

    public void setLenori21(String lenori21) {
        this.lenori21 = lenori21;
    }

    @XmlElement(name="LENORI_2_2")
    public String getLenori22() {
        return lenori22;
    }

    public void setLenori22(String lenori22) {
        this.lenori22 = lenori22;
    }

    @XmlElement(name="LENORI_2_3")
    public String getLenori23() {
        return lenori23;
    }

    public void setLenori23(String lenori23) {
        this.lenori23 = lenori23;
    }

    @XmlElement(name="LENORI_2_4")
    public String getLenori24() {
        return lenori24;
    }

    public void setLenori24(String lenori24) {
        this.lenori24 = lenori24;
    }

    @XmlElement(name="LENORI_2_5")
    public String getLenori25() {
        return lenori25;
    }

    public void setLenori25(String lenori25) {
        this.lenori25 = lenori25;
    }

    @XmlElement(name="LENORI_2_6")
    public String getLenori26() {
        return lenori26;
    }

    public void setLenori26(String lenori26) {
        this.lenori26 = lenori26;
    }

    @XmlElement(name="LENORI_3_0")
    public String getLenori30() {
        return lenori30;
    }

    public void setLenori30(String lenori30) {
        this.lenori30 = lenori30;
    }

    @XmlElement(name="LENORI_3_1")
    public String getLenori31() {
        return lenori31;
    }

    public void setLenori31(String lenori31) {
        this.lenori31 = lenori31;
    }

    @XmlElement(name="LENORI_3_2")
    public String getLenori32() {
        return lenori32;
    }

    public void setLenori32(String lenori32) {
        this.lenori32 = lenori32;
    }

    @XmlElement(name="LENORI_3_3")
    public String getLenori33() {
        return lenori33;
    }

    public void setLenori33(String lenori33) {
        this.lenori33 = lenori33;
    }

    @XmlElement(name="LENORI_3_4")
    public String getLenori34() {
        return lenori34;
    }

    public void setLenori34(String lenori34) {
        this.lenori34 = lenori34;
    }

    @XmlElement(name="LENORI_3_5")
    public String getLenori35() {
        return lenori35;
    }

    public void setLenori35(String lenori35) {
        this.lenori35 = lenori35;
    }

    @XmlElement(name="LENORI_3_6")
    public String getLenori36() {
        return lenori36;
    }

    public void setLenori36(String lenori36) {
        this.lenori36 = lenori36;
    }

    @XmlElement(name="LENORI_4_0")
    public String getLenori40() {
        return lenori40;
    }

    public void setLenori40(String lenori40) {
        this.lenori40 = lenori40;
    }

    @XmlElement(name="LENORI_4_1")
    public String getLenori41() {
        return lenori41;
    }

    public void setLenori41(String lenori41) {
        this.lenori41 = lenori41;
    }

    @XmlElement(name="LENORI_4_2")
    public String getLenori42() {
        return lenori42;
    }

    public void setLenori42(String lenori42) {
        this.lenori42 = lenori42;
    }

    @XmlElement(name="LENORI_4_3")
    public String getLenori43() {
        return lenori43;
    }

    public void setLenori43(String lenori43) {
        this.lenori43 = lenori43;
    }

    @XmlElement(name="LENORI_4_4")
    public String getLenori44() {
        return lenori44;
    }

    public void setLenori44(String lenori44) {
        this.lenori44 = lenori44;
    }

    @XmlElement(name="LENORI_4_5")
    public String getLenori45() {
        return lenori45;
    }

    public void setLenori45(String lenori45) {
        this.lenori45 = lenori45;
    }

    @XmlElement(name="LENORI_4_6")
    public String getLenori46() {
        return lenori46;
    }

    public void setLenori46(String lenori46) {
        this.lenori46 = lenori46;
    }

    @XmlElement(name="ENS_LENEXT")
    public String getEnsLenext() {
        return ensLenext;
    }

    public void setEnsLenext(String ensLenext) {
        this.ensLenext = ensLenext;
    }

    @XmlElement(name="ENS_LEXT_ES")
    public String getEnsLextEs() {
        return ensLextEs;
    }

    public void setEnsLextEs(String ensLextEs) {
        this.ensLextEs = ensLextEs;
    }

    @XmlElement(name="LENEXT_1_0")
    public String getLenext10() {
        return lenext10;
    }

    public void setLenext10(String lenext10) {
        this.lenext10 = lenext10;
    }

    @XmlElement(name="LENEXT_1_1")
    public String getLenext11() {
        return lenext11;
    }

    public void setLenext11(String lenext11) {
        this.lenext11 = lenext11;
    }

    @XmlElement(name="LENEXT_1_2")
    public String getLenext12() {
        return lenext12;
    }

    public void setLenext12(String lenext12) {
        this.lenext12 = lenext12;
    }

    @XmlElement(name="LENEXT_1_3")
    public String getLenext13() {
        return lenext13;
    }

    public void setLenext13(String lenext13) {
        this.lenext13 = lenext13;
    }

    @XmlElement(name="LENEXT_1_4")
    public String getLenext14() {
        return lenext14;
    }

    public void setLenext14(String lenext14) {
        this.lenext14 = lenext14;
    }

    @XmlElement(name="LENEXT_1_5")
    public String getLenext15() {
        return lenext15;
    }

    public void setLenext15(String lenext15) {
        this.lenext15 = lenext15;
    }

    @XmlElement(name="LENEXT_1_6")
    public String getLenext16() {
        return lenext16;
    }

    public void setLenext16(String lenext16) {
        this.lenext16 = lenext16;
    }

    @XmlElement(name="LENEXT_2_0")
    public String getLenext20() {
        return lenext20;
    }

    public void setLenext20(String lenext20) {
        this.lenext20 = lenext20;
    }

    @XmlElement(name="LENEXT_2_1")
    public String getLenext21() {
        return lenext21;
    }

    public void setLenext21(String lenext21) {
        this.lenext21 = lenext21;
    }

    @XmlElement(name="LENEXT_2_2")
    public String getLenext22() {
        return lenext22;
    }

    public void setLenext22(String lenext22) {
        this.lenext22 = lenext22;
    }

    @XmlElement(name="LENEXT_2_3")
    public String getLenext23() {
        return lenext23;
    }

    public void setLenext23(String lenext23) {
        this.lenext23 = lenext23;
    }

    @XmlElement(name="LENEXT_2_4")
    public String getLenext24() {
        return lenext24;
    }

    public void setLenext24(String lenext24) {
        this.lenext24 = lenext24;
    }

    @XmlElement(name="LENEXT_2_5")
    public String getLenext25() {
        return lenext25;
    }

    public void setLenext25(String lenext25) {
        this.lenext25 = lenext25;
    }

    @XmlElement(name="LENEXT_2_6")
    public String getLenext26() {
        return lenext26;
    }

    public void setLenext26(String lenext26) {
        this.lenext26 = lenext26;
    }

    @XmlElement(name="LENEXT_3_0")
    public String getLenext30() {
        return lenext30;
    }

    public void setLenext30(String lenext30) {
        this.lenext30 = lenext30;
    }

    @XmlElement(name="LENEXT_3_1")
    public String getLenext31() {
        return lenext31;
    }

    public void setLenext31(String lenext31) {
        this.lenext31 = lenext31;
    }

    @XmlElement(name="LENEXT_3_2")
    public String getLenext32() {
        return lenext32;
    }

    public void setLenext32(String lenext32) {
        this.lenext32 = lenext32;
    }

    @XmlElement(name="LENEXT_3_3")
    public String getLenext33() {
        return lenext33;
    }

    public void setLenext33(String lenext33) {
        this.lenext33 = lenext33;
    }

    @XmlElement(name="LENEXT_3_4")
    public String getLenext34() {
        return lenext34;
    }

    public void setLenext34(String lenext34) {
        this.lenext34 = lenext34;
    }

    @XmlElement(name="LENEXT_3_5")
    public String getLenext35() {
        return lenext35;
    }

    public void setLenext35(String lenext35) {
        this.lenext35 = lenext35;
    }

    @XmlElement(name="LENEXT_3_6")
    public String getLenext36() {
        return lenext36;
    }

    public void setLenext36(String lenext36) {
        this.lenext36 = lenext36;
    }

    @XmlElement(name="LENEXT_4_0")
    public String getLenext40() {
        return lenext40;
    }

    public void setLenext40(String lenext40) {
        this.lenext40 = lenext40;
    }

    @XmlElement(name="LENEXT_4_1")
    public String getLenext41() {
        return lenext41;
    }

    public void setLenext41(String lenext41) {
        this.lenext41 = lenext41;
    }

    @XmlElement(name="LENEXT_4_2")
    public String getLenext42() {
        return lenext42;
    }

    public void setLenext42(String lenext42) {
        this.lenext42 = lenext42;
    }

    @XmlElement(name="LENEXT_4_3")
    public String getLenext43() {
        return lenext43;
    }

    public void setLenext43(String lenext43) {
        this.lenext43 = lenext43;
    }

    @XmlElement(name="LENEXT_4_4")
    public String getLenext44() {
        return lenext44;
    }

    public void setLenext44(String lenext44) {
        this.lenext44 = lenext44;
    }

    @XmlElement(name="LENEXT_4_5")
    public String getLenext45() {
        return lenext45;
    }

    public void setLenext45(String lenext45) {
        this.lenext45 = lenext45;
    }

    @XmlElement(name="LENEXT_4_6")
    public String getLenext46() {
        return lenext46;
    }

    public void setLenext46(String lenext46) {
        this.lenext46 = lenext46;
    }

    @XmlElement(name="P501")
    public String getP501() {
        return p501;
    }

    public void setP501(String p501) {
        this.p501 = p501;
    }

    @XmlElement(name="P502_D")
    public String getP502D() {
        return p502D;
    }

    public void setP502D(String p502D) {
        this.p502D = p502D;
    }

    @XmlElement(name="P502_M")
    public String getP502M() {
        return p502M;
    }

    public void setP502M(String p502M) {
        this.p502M = p502M;
    }

    @XmlElement(name="P502_A")
    public String getP502A() {
        return p502A;
    }

    public void setP502A(String p502A) {
        this.p502A = p502A;
    }

    @XmlElement(name="P504_C12")
    public String getP504C12() {
        return p504C12;
    }

    public void setP504C12(String p504C12) {
        this.p504C12 = p504C12;
    }

    @XmlElement(name="P505_C3")
    public String getP505C3() {
        return p505C3;
    }

    public void setP505C3(String p505C3) {
        this.p505C3 = p505C3;
    }

    @XmlElement(name="P505_C12_D")
    public String getP505C12D() {
        return p505C12D;
    }

    public void setP505C12D(String p505C12D) {
        this.p505C12D = p505C12D;
    }

    @XmlElement(name="P505_C12_M")
    public String getP505C12M() {
        return p505C12M;
    }

    public void setP505C12M(String p505C12M) {
        this.p505C12M = p505C12M;
    }

    @XmlElement(name="P505_C12_A")
    public String getP505C12A() {
        return p505C12A;
    }

    public void setP505C12A(String p505C12A) {
        this.p505C12A = p505C12A;
    }

    @XmlElement(name="P506_C3_D")
    public String getP506C3D() {
        return p506C3D;
    }

    public void setP506C3D(String p506C3D) {
        this.p506C3D = p506C3D;
    }

    @XmlElement(name="P506_C3_M")
    public String getP506C3M() {
        return p506C3M;
    }

    public void setP506C3M(String p506C3M) {
        this.p506C3M = p506C3M;
    }

    @XmlElement(name="P506_C3_A")
    public String getP506C3A() {
        return p506C3A;
    }

    public void setP506C3A(String p506C3A) {
        this.p506C3A = p506C3A;
    }

    @XmlElement(name="P508_C3")
    public String getP508C3() {
        return p508C3;
    }

    public void setP508C3(String p508C3) {
        this.p508C3 = p508C3;
    }

    @XmlElement(name="P509_C3_D")
    public String getP509C3D() {
        return p509C3D;
    }

    public void setP509C3D(String p509C3D) {
        this.p509C3D = p509C3D;
    }

    @XmlElement(name="P509_C3_M")
    public String getP509C3M() {
        return p509C3M;
    }

    public void setP509C3M(String p509C3M) {
        this.p509C3M = p509C3M;
    }

    @XmlElement(name="P509_C3_A")
    public String getP509C3A() {
        return p509C3A;
    }

    public void setP509C3A(String p509C3A) {
        this.p509C3A = p509C3A;
    }

    @XmlElement(name="BBTK_AULA_0")
    public String getBbtkAula0() {
        return bbtkAula0;
    }

    public void setBbtkAula0(String bbtkAula0) {
        this.bbtkAula0 = bbtkAula0;
    }

    @XmlElement(name="BBTK_AULA_1")
    public String getBbtkAula1() {
        return bbtkAula1;
    }

    public void setBbtkAula1(String bbtkAula1) {
        this.bbtkAula1 = bbtkAula1;
    }

    @XmlElement(name="BBTK_AULA_2")
    public String getBbtkAula2() {
        return bbtkAula2;
    }

    public void setBbtkAula2(String bbtkAula2) {
        this.bbtkAula2 = bbtkAula2;
    }

    @XmlElement(name="BBTK_AULA_3")
    public String getBbtkAula3() {
        return bbtkAula3;
    }

    public void setBbtkAula3(String bbtkAula3) {
        this.bbtkAula3 = bbtkAula3;
    }

    @XmlElement(name="BBTK_AULA_4")
    public String getBbtkAula4() {
        return bbtkAula4;
    }

    public void setBbtkAula4(String bbtkAula4) {
        this.bbtkAula4 = bbtkAula4;
    }

    @XmlElement(name="BBTK_AULA_5")
    public String getBbtkAula5() {
        return bbtkAula5;
    }

    public void setBbtkAula5(String bbtkAula5) {
        this.bbtkAula5 = bbtkAula5;
    }

    @XmlElement(name="BBTK_AULA_6")
    public String getBbtkAula6() {
        return bbtkAula6;
    }

    public void setBbtkAula6(String bbtkAula6) {
        this.bbtkAula6 = bbtkAula6;
    }

    @XmlElement(name="BBTK_AULA_N")
    public String getBbtkAulaN() {
        return bbtkAulaN;
    }

    public void setBbtkAulaN(String bbtkAulaN) {
        this.bbtkAulaN = bbtkAulaN;
    }

    @XmlElement(name="COMP_OPER")
    public String getCompOper() {
        return compOper;
    }

    public void setCompOper(String compOper) {
        this.compOper = compOper;
    }

    @XmlElement(name="P606_C3_T")
    public Integer getP606C3T() {
        return p606C3T;
    }

    public void setP606C3T(Integer p606C3T) {
        this.p606C3T = p606C3T;
    }

    @XmlElement(name="P606_C3_1")
    public Integer getP606C31() {
        return p606C31;
    }

    public void setP606C31(Integer p606C31) {
        this.p606C31 = p606C31;
    }

    @XmlElement(name="P606_C3_2")
    public Integer getP606C32() {
        return p606C32;
    }

    public void setP606C32(Integer p606C32) {
        this.p606C32 = p606C32;
    }

    @XmlElement(name="P606_C3_3")
    public Integer getP606C33() {
        return p606C33;
    }

    public void setP606C33(Integer p606C33) {
        this.p606C33 = p606C33;
    }

    @XmlElement(name="P607_C3")
    public String getP607C3() {
        return p607C3;
    }

    public void setP607C3(String p607C3) {
        this.p607C3 = p607C3;
    }

    @XmlElement(name="P608_C3_AP")
    public String getP608C3Ap() {
        return p608C3Ap;
    }

    public void setP608C3Ap(String p608C3Ap) {
        this.p608C3Ap = p608C3Ap;
    }

    @XmlElement(name="P608_C3_NO")
    public String getP608C3No() {
        return p608C3No;
    }

    public void setP608C3No(String p608C3No) {
        this.p608C3No = p608C3No;
    }

    @XmlElement(name="P608_C3_DNI")
    public String getP608C3Dni() {
        return p608C3Dni;
    }

    public void setP608C3Dni(String p608C3Dni) {
        this.p608C3Dni = p608C3Dni;
    }

    @XmlElement(name="ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name="DIR_APE")
    public String getDirApe() {
        return dirApe;
    }

    public void setDirApe(String dirApe) {
        this.dirApe = dirApe;
    }

    @XmlElement(name="DIR_NOM")
    public String getDirNom() {
        return dirNom;
    }

    public void setDirNom(String dirNom) {
        this.dirNom = dirNom;
    }

    @XmlElement(name="DIR_SITUA")
    public String getDirSitua() {
        return dirSitua;
    }

    public void setDirSitua(String dirSitua) {
        this.dirSitua = dirSitua;
    }

    @XmlElement(name="DIR_DNI")
    public String getDirDni() {
        return dirDni;
    }

    public void setDirDni(String dirDni) {
        this.dirDni = dirDni;
    }

    @XmlElement(name="DIR_EMAIL")
    public String getDirEmail() {
        return dirEmail;
    }

    public void setDirEmail(String dirEmail) {
        this.dirEmail = dirEmail;
    }

    @XmlElement(name="SDIR_APE")
    public String getSdirApe() {
        return sdirApe;
    }

    public void setSdirApe(String sdirApe) {
        this.sdirApe = sdirApe;
    }

    @XmlElement(name="SDIR_NOM")
    public String getSdirNom() {
        return sdirNom;
    }

    public void setSdirNom(String sdirNom) {
        this.sdirNom = sdirNom;
    }

    @XmlElement(name="SDIR_SITUA")
    public String getSdirSitua() {
        return sdirSitua;
    }

    public void setSdirSitua(String sdirSitua) {
        this.sdirSitua = sdirSitua;
    }

    @XmlElement(name="SDIR_DNI")
    public String getSdirDni() {
        return sdirDni;
    }

    public void setSdirDni(String sdirDni) {
        this.sdirDni = sdirDni;
    }

    @XmlElement(name="SDIR_EMAIL")
    public String getSdirEmail() {
        return sdirEmail;
    }

    public void setSdirEmail(String sdirEmail) {
        this.sdirEmail = sdirEmail;
    }

    @XmlElement(name="REC_CED_DIA")
    public String getRecCedDia() {
        return recCedDia;
    }

    public void setRecCedDia(String recCedDia) {
        this.recCedDia = recCedDia;
    }

    @XmlElement(name="REC_CED_MES")
    public String getRecCedMes() {
        return recCedMes;
    }

    public void setRecCedMes(String recCedMes) {
        this.recCedMes = recCedMes;
    }

    @XmlElement(name="CUL_CED_DIA")
    public String getCulCedDia() {
        return culCedDia;
    }

    public void setCulCedDia(String culCedDia) {
        this.culCedDia = culCedDia;
    }

    @XmlElement(name="CUL_CED_MES")
    public String getCulCedMes() {
        return culCedMes;
    }

    public void setCulCedMes(String culCedMes) {
        this.culCedMes = culCedMes;
    }

    @XmlElement(name="ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    @XmlElement(name="VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name="FUENTE")
    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    @XmlElement(name="FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
/*
    @XmlElement(name="PERSONAL")
    public List<Matricula2012Personal> getMatricula2012PersonalList() {
        return matricula2012PersonalList;
    }

    public void setMatricula2012PersonalList(List<Matricula2012Personal> matricula2012PersonalList) {
        this.matricula2012PersonalList = matricula2012PersonalList;
    }

    @XmlElement(name="LOCAL_PRONOEI")
    public List<Matricula2012Localpronoei> getMatricula2012LocalpronoeiList() {
        return matricula2012LocalpronoeiList;
    }

    public void setMatricula2012LocalpronoeiList(List<Matricula2012Localpronoei> matricula2012LocalpronoeiList) {
        this.matricula2012LocalpronoeiList = matricula2012LocalpronoeiList;
    }

    @XmlElement(name="COORD_PRONOEI")
    public List<Matricula2012Coordpronoei> getMatricula2012CoordpronoeiList() {
        return matricula2012CoordpronoeiList;
    }

    public void setMatricula2012CoordpronoeiList(List<Matricula2012Coordpronoei> matricula2012CoordpronoeiList) {
        this.matricula2012CoordpronoeiList = matricula2012CoordpronoeiList;
    }

    @XmlElement(name="RECURSOS")
    @XmlJavaTypeAdapter(Matricula2012RecursosMapAdapter.class)
    public Map<String, Matricula2012Recursos> getDetalleRecursos() {
        return detalleRecursos;
    }

    
    public void setDetalleRecursos(Map<String, Matricula2012Recursos> detalleRecursos) {
        this.detalleRecursos = detalleRecursos;
    }

    @XmlElement(name="MATRICULA")
    @XmlJavaTypeAdapter(Matricula2012MatriculaMapAdapter.class)
    public Map<String, Matricula2012Matricula> getDetalleMatricula() {
        return detalleMatricula;
    }


    public void setDetalleMatricula(Map<String, Matricula2012Matricula> detalleMatricula) {
        this.detalleMatricula = detalleMatricula;
    }

    @XmlElement(name="SECCION")
    @XmlJavaTypeAdapter(Matricula2012SeccionMapAdapter.class)
    public Map<String, Matricula2012Seccion> getDetalleSeccion() {
        return detalleSeccion;
    }
 
    public void setDetalleSeccion(Map<String, Matricula2012Seccion> detalleSeccion) {
        this.detalleSeccion = detalleSeccion;
    }

    @XmlElement(name="SAANEES")
    @XmlJavaTypeAdapter(Matricula2012SaaneeMapAdapter.class)
    public Map<String, Matricula2012Saanee> getDetalleSaanee() {
        return detalleSaanee;
    }

    public void setDetalleSaanee(Map<String, Matricula2012Saanee> detalleSaanee) {
        this.detalleSaanee = detalleSaanee;
    }
*/

    @XmlElement(name="NROCED")
    public String getNroCed() {
        return nroCed;
    }

    public void setNroCed(String nroCed) {
        this.nroCed = nroCed;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2012Cabecera)) {
            return false;
        }
        Matricula2012Cabecera other = (Matricula2012Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
/*
    @XmlElement(name="CURSOS_CETPRO")
    public List<Matricula2012CetproCurso> getMatricula2012CetproCursoList() {
        return matricula2012CetproCursoList;
    }

    
    public void setMatricula2012CetproCursoList(List<Matricula2012CetproCurso> matricula2012CetproCursoList) {
        this.matricula2012CetproCursoList = matricula2012CetproCursoList;
    }
*/
    @XmlElement(name="MODATEN")
    public String getModaten() {
        return modaten;
    }


    public void setModaten(String modaten) {
        this.modaten = modaten;
    }

    @XmlElement(name="EGESTORA")
    public String getEgestora() {
        return egestora;
    }

    public void setEgestora(String egestora) {
        this.egestora = egestora;
    }


    @XmlElement(name="TIPODFINA")
    public String getTipodfina() {
        return tipodfina;
    }

    public void setTipodfina(String tipodfina) {
        this.tipodfina = tipodfina;
    }

    @XmlElement(name="NUMRES")
    public String getNumres() {
        return numres;
    }

    public void setNumres(String numres) {
        this.numres = numres;
    }


    @XmlElement(name="FECHARES_DD")
    public String getFecharesDD() {
        return fecharesDD;
    }

    public void setFecharesDD(String fecharesDD) {
        this.fecharesDD = fecharesDD;
    }


    @XmlElement(name="FECHARES_MM")
    public String getFecharesMM() {
        return fecharesMM;
    }

    public void setFecharesMM(String fecharesMM) {
        this.fecharesMM = fecharesMM;
    }


    @XmlElement(name="FECHARES_AA")
   public String getFecharesAA() {
        return fecharesAA;
    }

    public void setFecharesAA(String fecharesAA) {
        this.fecharesAA = fecharesAA;
    }


}
