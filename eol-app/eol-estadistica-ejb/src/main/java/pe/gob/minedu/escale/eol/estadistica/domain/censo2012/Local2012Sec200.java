/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "local2012_sec200")
public class Local2012Sec200 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P202_1")
    private Integer p2021;
    @Column(name = "P202_2")
    private String p2022;
    @Column(name = "P202_3")
    private String p2023;
    @Column(name = "P202_4M1")
    private String p2024m1;
    @Column(name = "P202_4M2")
    private String p2024m2;
    @Column(name = "P202_4M3")
    private String p2024m3;
    @Column(name = "P202_4M4")
    private Integer p2024m4;
    @Column(name = "P202_4T1")
    private String p2024t1;
    @Column(name = "P202_4T2")
    private String p2024t2;
    @Column(name = "P202_4T3")
    private String p2024t3;
    @Column(name = "P202_4T4")
    private Integer p2024t4;
    @Column(name = "P202_4N1")
    private String p2024n1;
    @Column(name = "P202_4N2")
    private String p2024n2;
    @Column(name = "P202_4N3")
    private String p2024n3;
    @Column(name = "P202_4N4")
    private Integer p2024n4;
    @Column(name = "P202_5")
    private Integer p2025;
    @Column(name = "P202_6")
    private String p2026;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2012Cabecera local2012Cabecera;

    public Local2012Sec200() {
    }

    public Local2012Sec200(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P202_1")
    public Integer getP2021() {
        return p2021;
    }

    public void setP2021(Integer p2021) {
        this.p2021 = p2021;
    }

    @XmlElement(name="P202_2")
    public String getP2022() {
        return p2022;
    }

    public void setP2022(String p2022) {
        this.p2022 = p2022;
    }

    @XmlElement(name="P202_3")
    public String getP2023() {
        return p2023;
    }

    public void setP2023(String p2023) {
        this.p2023 = p2023;
    }

    @XmlElement(name="P202_4M1")
    public String getP2024m1() {
        return p2024m1;
    }

    public void setP2024m1(String p2024m1) {
        this.p2024m1 = p2024m1;
    }

    @XmlElement(name="P202_4M2")
    public String getP2024m2() {
        return p2024m2;
    }

    public void setP2024m2(String p2024m2) {
        this.p2024m2 = p2024m2;
    }

    @XmlElement(name="P202_4M3")
    public String getP2024m3() {
        return p2024m3;
    }

    public void setP2024m3(String p2024m3) {
        this.p2024m3 = p2024m3;
    }

    @XmlElement(name="P202_4M4")
    public Integer getP2024m4() {
        return p2024m4;
    }

    public void setP2024m4(Integer p2024m4) {
        this.p2024m4 = p2024m4;
    }

    @XmlElement(name="P202_4T1")
    public String getP2024t1() {
        return p2024t1;
    }

    public void setP2024t1(String p2024t1) {
        this.p2024t1 = p2024t1;
    }

    @XmlElement(name="P202_4T2")
    public String getP2024t2() {
        return p2024t2;
    }

    public void setP2024t2(String p2024t2) {
        this.p2024t2 = p2024t2;
    }

    @XmlElement(name="P202_4T3")
    public String getP2024t3() {
        return p2024t3;
    }

    public void setP2024t3(String p2024t3) {
        this.p2024t3 = p2024t3;
    }

    @XmlElement(name="P202_4T4")
    public Integer getP2024t4() {
        return p2024t4;
    }

    public void setP2024t4(Integer p2024t4) {
        this.p2024t4 = p2024t4;
    }

    @XmlElement(name="P202_4N1")
    public String getP2024n1() {
        return p2024n1;
    }

    public void setP2024n1(String p2024n1) {
        this.p2024n1 = p2024n1;
    }

    @XmlElement(name="P202_4N2")
    public String getP2024n2() {
        return p2024n2;
    }

    public void setP2024n2(String p2024n2) {
        this.p2024n2 = p2024n2;
    }

    @XmlElement(name="P202_4N3")
    public String getP2024n3() {
        return p2024n3;
    }

    public void setP2024n3(String p2024n3) {
        this.p2024n3 = p2024n3;
    }

    @XmlElement(name="P202_4N4")
    public Integer getP2024n4() {
        return p2024n4;
    }

    public void setP2024n4(Integer p2024n4) {
        this.p2024n4 = p2024n4;
    }

    @XmlElement(name="P202_5")
    public Integer getP2025() {
        return p2025;
    }

    public void setP2025(Integer p2025) {
        this.p2025 = p2025;
    }

    @XmlElement(name="P202_6")
    public String getP2026() {
        return p2026;
    }

    public void setP2026(String p2026) {
        this.p2026 = p2026;
    }

    @XmlTransient
    public Local2012Cabecera getLocal2012Cabecera() {
        return local2012Cabecera;
    }

    public void setLocal2012Cabecera(Local2012Cabecera local2012Cabecera) {
        this.local2012Cabecera = local2012Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2012Sec200)) {
            return false;
        }
        Local2012Sec200 other = (Local2012Sec200) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }


}
