
package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2013_cabecera")
public class Matricula2013Cabecera implements Serializable {
	private static final long serialVersionUID = 1L;
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03A = "c03a";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08A = "c08a";
	public final static String CEDULA_09A = "c09a";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;
	@Basic(optional = false)
	@Column(name = "COD_MOD")
	private String codMod;
	@Basic(optional = false)
	@Column(name = "ANEXO")
	private String anexo;
	@Basic(optional = false)
	@Column(name = "CODLOCAL")
	private String codlocal;
	@Column(name = "TIPOPROG")
	private String tipoprog;
	@Column(name = "TIPOIESUP")
	private String tipoiesup;
	@Column(name = "GESTION")
	private String gestion;

	@Column(name = "CEN_EDU")
	private String cenEdu;
	@Column(name = "TELEFONO")
	private String telefono;
	@Basic(optional = false)
	@Column(name = "ID_UBIGEO")
	private String idUbigeo;
	@Basic(optional = false)
	@Column(name = "DISTRITO")
	private String distrito;
	@Basic(optional = false)
	@Column(name = "NIV_MOD")
	private String nivMod;

	@Column(name = "NROCED")
	private String nroCed;

	@Basic(optional = false)
	@Column(name = "CODUGEL")
	private String codugel;
	@Column(name = "FORMATEN")
	private String formaten;
	@Column(name = "MODATEN")
	private String modaten;
	@Column(name = "TIPONEE")
	private String tiponee;
	@Column(name = "SEANEXO")
	private String seanexo;

	@Column(name = "P101_DD")
	private String p101Dd;
	@Column(name = "P101_MM")
	private String p101Mm;

	@Column(name = "P101_C8_11")
	private String p101C811;
	@Column(name = "P101_C8_12")
	private String p101C812;
	@Column(name = "P101_C8_21")
	private String p101C821;
	@Column(name = "P101_C8_22")
	private String p101C822;
	@Column(name = "P101_C8_23")
	private String p101C823;
	@Column(name = "P101_C8_30")
	private String p101C830;
	@Column(name = "P101_C8_40")
	private String p101C840;
	@Column(name = "P101_C8_50")
	private String p101C850;

	@Column(name = "P102_DD")
	private String p102Dd;
	@Column(name = "P102_MM")
	private String p102Mm;
	@Column(name = "P103_HI_H")
	private String p103HiH;
	@Column(name = "P103_HI_M")
	private String p103HiM;
	@Column(name = "P103_HT_H")
	private String p103HtH;
	@Column(name = "P103_HT_M")
	private String p103HtM;
	@Column(name = "P104_HI_H")
	private String p104HiH;
	@Column(name = "P104_HI_M")
	private String p104HiM;
	@Column(name = "P104_HT_H")
	private String p104HtH;
	@Column(name = "P104_HT_M")
	private String p104HtM;
	@Column(name = "P104_C3A")
	private String p104C3a;

	@Column(name = "P105")
	private String p105;
	@Column(name = "P108_2010")
	private String p1082010;
	@Column(name = "P108_2011")
	private String p1082011;
	@Column(name = "P108_2012")
	private String p1082012;
	@Column(name = "P108_C2H1")
	private String p108c2h1;
	@Column(name = "P108_C2M1")
	private String p108c2m1;
	@Column(name = "P108_C2H2")
	private String p108c2h2;
	@Column(name = "P108_C2M2")
	private String p108c2m2;

	@Column(name = "P109_C2")
	private String p109C2;
	@Column(name = "ACTIV_IE_1")
	private String activIe1;
	@Column(name = "ACTIV_IE_2")
	private String activIe2;
	@Column(name = "ACTIV_IE_3")
	private String activIe3;
	@Column(name = "ACTIV_IE_4")
	private String activIe4;
	@Column(name = "ACTIV_IE_5")
	private String activIe5;
	@Column(name = "ACTIV_IE_6")
	private String activIe6;
	@Column(name = "ACTIV_IE_7")
	private String activIe7;
	@Column(name = "ACTIV_C3_5")
	private String activC35;

	@Column(name = "SERV_IE_1")
	private String servIe1;
	@Column(name = "SERV_IE_2")
	private String servIe2;
	@Column(name = "SERV_IE_3")
	private String servIe3;
	@Column(name = "SERV_IE_4")
	private String servIe4;
	@Column(name = "SERV_IE_5")
	private String servIe5;
	@Column(name = "SERV_IE_6")
	private String servIe6;
	@Column(name = "SERV_IE_6E")
	private String servIe6e;
	@Column(name = "SAANEE")
	private String saanee;
	@Column(name = "P201")
	private String p201;
	@Column(name = "P201_ESP")
	private String p201Esp;
	@Column(name = "P202")
	private String p202;
	@Column(name = "P202_ESP")
	private String p202Esp;
	@Column(name = "P203")
	private String p203;
	@Column(name = "P203_ESP")
	private String p203Esp;
	@Column(name = "P203_C3_1")
	private String p203C31;
	@Column(name = "P203_C3_2")
	private String p203C32;
	@Column(name = "P203_C3_3")
	private String p203C33;
	@Column(name = "P203_C3_4")
	private String p203C34;
	@Column(name = "P203_C3_5")
	private String p203C35;
	@Column(name = "P203_C3_6")
	private String p203C36;
	@Column(name = "P204")
	private String p204;
	@Column(name = "P204_ESP")
	private String p204Esp;
	@Column(name = "P204_C2")
	private String p204C2;
	@Column(name = "P204_C2_ESP")
	private String p204C2esp;
	@Column(name = "P205")
	private String p205;
	@Column(name = "P205_1")
	private String p2051;
	@Column(name = "P205_1_RD")
	private String p2051rd;
	@Column(name = "P205_2")
	private String p2052;
	@Column(name = "P205_2_RD")
	private String p2052rd;
	@Column(name = "P205_C2_ESP")
	private String p205C2esp;

	@Column(name = "P501")
	private String p501;
	@Column(name = "P502_D")
	private String p502D;
	@Column(name = "P502_M")
	private String p502M;
	@Column(name = "P502_A")
	private String p502A;
	@Column(name = "P504_C12")
	private String p504C12;
	@Column(name = "P505_C3")
	private String p505C3;
	@Column(name = "P505_C12_D")
	private String p505C12D;
	@Column(name = "P505_C12_M")
	private String p505C12M;
	@Column(name = "P505_C12_A")
	private String p505C12A;
	@Column(name = "P506_C3_D")
	private String p506C3D;
	@Column(name = "P506_C3_M")
	private String p506C3M;
	@Column(name = "P506_C3_A")
	private String p506C3A;
	@Column(name = "P508_C3")
	private String p508C3;
	@Column(name = "P509_C3_D")
	private String p509C3D;
	@Column(name = "P509_C3_M")
	private String p509C3M;
	@Column(name = "P509_C3_A")
	private String p509C3A;
	@Column(name = "BBTK_AULA_0")
	private String bbtkAula0;
	@Column(name = "BBTK_AULA_1")
	private String bbtkAula1;
	@Column(name = "BBTK_AULA_2")
	private String bbtkAula2;
	@Column(name = "BBTK_AULA_3")
	private String bbtkAula3;
	@Column(name = "BBTK_AULA_4")
	private String bbtkAula4;
	@Column(name = "BBTK_AULA_5")
	private String bbtkAula5;
	@Column(name = "BBTK_AULA_6")
	private String bbtkAula6;
	@Column(name = "BBTK_AULA_N")
	private String bbtkAulaN;
	@Column(name = "COMP_OPER")
	private String compOper;
	@Column(name = "P601_C1")
	private String p601C1;

	@Column(name = "P607_C3")
	private String p607C3;
	@Column(name = "P608_C3_AP")
	private String p608C3Ap;
	@Column(name = "P608_C3_NO")
	private String p608C3No;
	@Column(name = "P608_C3_DNI")
	private String p608C3Dni;

	@Column(name = "P610_C3")
	private String p610C3;
	@Column(name = "P610_C3_ESP")
	private String p610C3esp;
	@Column(name = "P610_C3_1")
	private String p610C31;
	@Column(name = "P610_C3_2")
	private String p610C32;
	@Column(name = "P610_C3_3")
	private String p610C33;
	@Column(name = "P610_C3_4")
	private String p610C34;
	@Column(name = "P610_C3_5")
	private String p610C35;
	@Column(name = "P610_C3_6")
	private String p610C36;
	@Column(name = "P610_C3_6E")
	private String p610C36e;

	@Column(name = "ANOTACIONES")
	private String anotaciones;
	@Column(name = "DIR_APE")
	private String dirApe;
	@Column(name = "DIR_NOM")
	private String dirNom;
	@Column(name = "DIR_SITUA")
	private String dirSitua;
	@Column(name = "DIR_DNI")
	private String dirDni;
	@Column(name = "DIR_FONO")
	private String dirFono;
	@Column(name = "DIR_EMAIL")
	private String dirEmail;
	@Column(name = "SDIR_APE")
	private String sdirApe;
	@Column(name = "SDIR_NOM")
	private String sdirNom;
	@Column(name = "SDIR_SITUA")
	private String sdirSitua;
	@Column(name = "SDIR_DNI")
	private String sdirDni;
	@Column(name = "SDIR_FONO")
	private String sdirFono;
	@Column(name = "SDIR_EMAIL")
	private String sdirEmail;
	@Column(name = "REC_CED_DIA")
	private String recCedDia;
	@Column(name = "REC_CED_MES")
	private String recCedMes;
	@Column(name = "CUL_CED_DIA")
	private String culCedDia;
	@Column(name = "CUL_CED_MES")
	private String culCedMes;
	@Column(name = "ULTIMO")
	private Boolean ultimo;
	@Column(name = "VERSION")
	private String version;
	@Column(name = "FUENTE")
	private String fuente;
	@Column(name = "FECHA_ENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;
	@Column(name = "EGESTORA")
	private String egestora;
	@Column(name = "TIPODFINA")
	private String tipodfina;
	@Column(name = "NUMRES")
	private String numres;
	@Column(name = "FECHARES_DD")
	private String fecharesDD;
	@Column(name = "FECHARES_MM")
	private String fecharesMM;
	@Column(name = "FECHARES_AA")
	private String fecharesAA;

	/*
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private List<Matricula2013Personal>
	 * matricula2013PersonalList;
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private List<Matricula2013Perifericos>
	 * matricula2013PerifericosList;
	 * 
	 * @MapKeyColumn(name = "cuadro", length = 5)
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera", cascade = CascadeType.ALL,
	 * fetch = FetchType.EAGER) private Map<String,Matricula2013Recursos>
	 * detalleRecursos;
	 * 
	 * @MapKeyColumn(name = "cuadro", length = 5)
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private Map<String,Matricula2013Matricula>
	 * detalleMatricula;
	 * 
	 * @MapKeyColumn(name = "cuadro", length = 5)
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private Map<String,Matricula2013Seccion>
	 * detalleSeccion;
	 * 
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private List<Matricula2013Localpronoei>
	 * matricula2013LocalpronoeiList;
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private List<Matricula2013Coordpronoei>
	 * matricula2013CoordpronoeiList;
	 * 
	 * @MapKeyColumn(name = "cuadro", length = 5)
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private Map<String,Matricula2013Saanee> detalleSaanee;
	 * 
	 * @OneToMany(mappedBy = "matricula2013Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private List<Matricula2013CetproCurso>
	 * matricula2013CetproCursoList;
	 */

	@Transient
	private long token;

	@Transient
	private String msg;

	@Transient
	private String estadoRpt;

	public Matricula2013Cabecera() {
	}

	public Matricula2013Cabecera(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Matricula2013Cabecera(Long idEnvio, String codMod, String anexo, String codlocal, String idUbigeo,
			String distrito, String nivMod, String codugel) {
		this.idEnvio = idEnvio;
		this.codMod = codMod;
		this.anexo = anexo;
		this.codlocal = codlocal;
		this.idUbigeo = idUbigeo;
		this.distrito = distrito;
		this.nivMod = nivMod;
		this.codugel = codugel;
	}

	@XmlAttribute
	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "COD_MOD")
	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	@XmlElement(name = "ANEXO")
	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	@XmlElement(name = "CODLOCAL")
	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	@XmlElement(name = "TIPOPROG")
	public String getTipoprog() {
		return tipoprog;
	}

	public void setTipoprog(String tipoprog) {
		this.tipoprog = tipoprog;
	}

	@XmlElement(name = "TIPOIESUP")
	public String getTipoiesup() {
		return tipoiesup;
	}

	public void setTipoiesup(String tipoiesup) {
		this.tipoiesup = tipoiesup;
	}

	@XmlElement(name = "GESTION")
	public String getGestion() {
		return gestion;
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	@XmlElement(name = "CEN_EDU")
	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	@XmlElement(name = "TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@XmlElement(name = "ID_UBIGEO")
	public String getIdUbigeo() {
		return idUbigeo;
	}

	public void setIdUbigeo(String idUbigeo) {
		this.idUbigeo = idUbigeo;
	}

	@XmlElement(name = "DISTRITO")
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@XmlElement(name = "NIV_MOD")
	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	@XmlElement(name = "CODUGEL")
	public String getCodugel() {
		return codugel;
	}

	public void setCodugel(String codugel) {
		this.codugel = codugel;
	}

	@XmlElement(name = "FORMATEN")
	public String getFormaten() {
		return formaten;
	}

	public void setFormaten(String formaten) {
		this.formaten = formaten;
	}

	@XmlElement(name = "TIPONEE")
	public String getTiponee() {
		return tiponee;
	}

	public void setTiponee(String tiponee) {
		this.tiponee = tiponee;
	}

	@XmlElement(name = "SEANEXO")
	public String getSeanexo() {
		return seanexo;
	}

	public void setSeanexo(String seanexo) {
		this.seanexo = seanexo;
	}

	@XmlElement(name = "P101_DD")
	public String getP101Dd() {
		return p101Dd;
	}

	public void setP101Dd(String p101Dd) {
		this.p101Dd = p101Dd;
	}

	@XmlElement(name = "P101_MM")
	public String getP101Mm() {
		return p101Mm;
	}

	public void setP101Mm(String p101Mm) {
		this.p101Mm = p101Mm;
	}

	@XmlElement(name = "P101_C8_11")
	public String getP101C811() {
		return p101C811;
	}

	public void setP101C811(String p101C811) {
		this.p101C811 = p101C811;
	}

	@XmlElement(name = "P101_C8_12")
	public String getP101C812() {
		return p101C812;
	}

	public void setP101C812(String p101C812) {
		this.p101C812 = p101C812;
	}

	@XmlElement(name = "P101_C8_21")
	public String getP101C821() {
		return p101C821;
	}

	public void setP101C821(String p101C821) {
		this.p101C821 = p101C821;
	}

	@XmlElement(name = "P101_C8_22")
	public String getP101C822() {
		return p101C822;
	}

	public void setP101C822(String p101C822) {
		this.p101C822 = p101C822;
	}

	@XmlElement(name = "P101_C8_23")
	public String getP101C823() {
		return p101C823;
	}

	public void setP101C823(String p101C823) {
		this.p101C823 = p101C823;
	}

	@XmlElement(name = "P101_C8_30")
	public String getP101C830() {
		return p101C830;
	}

	public void setP101C830(String p101C830) {
		this.p101C830 = p101C830;
	}

	@XmlElement(name = "P101_C8_40")
	public String getP101C840() {
		return p101C840;
	}

	public void setP101C840(String p101C840) {
		this.p101C840 = p101C840;
	}

	@XmlElement(name = "P101_C8_50")
	public String getP101C850() {
		return p101C850;
	}

	public void setP101C850(String p101C850) {
		this.p101C850 = p101C850;
	}

	@XmlElement(name = "P102_DD")
	public String getP102Dd() {
		return p102Dd;
	}

	public void setP102Dd(String p102Dd) {
		this.p102Dd = p102Dd;
	}

	@XmlElement(name = "P102_MM")
	public String getP102Mm() {
		return p102Mm;
	}

	public void setP102Mm(String p102Mm) {
		this.p102Mm = p102Mm;
	}

	@XmlElement(name = "P103_HI_H")
	public String getP103HiH() {
		return p103HiH;
	}

	public void setP103HiH(String p103HiH) {
		this.p103HiH = p103HiH;
	}

	@XmlElement(name = "P103_HI_M")
	public String getP103HiM() {
		return p103HiM;
	}

	public void setP103HiM(String p103HiM) {
		this.p103HiM = p103HiM;
	}

	@XmlElement(name = "P103_HT_H")
	public String getP103HtH() {
		return p103HtH;
	}

	public void setP103HtH(String p103HtH) {
		this.p103HtH = p103HtH;
	}

	@XmlElement(name = "P103_HT_M")
	public String getP103HtM() {
		return p103HtM;
	}

	public void setP103HtM(String p103HtM) {
		this.p103HtM = p103HtM;
	}

	@XmlElement(name = "P104_HT_H")
	public String getP104HtH() {
		return p104HtH;
	}

	public void setP104HtH(String p104HtH) {
		this.p104HtH = p104HtH;
	}

	@XmlElement(name = "P104_HT_M")
	public String getP104HtM() {
		return p104HtM;
	}

	public void setP104HtM(String p104HtM) {
		this.p104HtM = p104HtM;
	}

	@XmlElement(name = "P104_HI_H")
	public String getP104HiH() {
		return p104HiH;
	}

	public void setP104HiH(String p104HiH) {
		this.p104HiH = p104HiH;
	}

	@XmlElement(name = "P104_HI_M")
	public String getP104HiM() {
		return p104HiM;
	}

	public void setP104HiM(String p104HiM) {
		this.p104HiM = p104HiM;
	}

	@XmlElement(name = "P104_C3A")
	public String getP104C3a() {
		return p104C3a;
	}

	public void setP104C3a(String p104C3a) {
		this.p104C3a = p104C3a;
	}

	@XmlElement(name = "P105")
	public String getP105() {
		return p105;
	}

	public void setP105(String p105) {
		this.p105 = p105;
	}

	@XmlElement(name = "ACTIV_IE_1")
	public String getActivIe1() {
		return activIe1;
	}

	public void setActivIe1(String activIe1) {
		this.activIe1 = activIe1;
	}

	@XmlElement(name = "ACTIV_IE_2")
	public String getActivIe2() {
		return activIe2;
	}

	public void setActivIe2(String activIe2) {
		this.activIe2 = activIe2;
	}

	@XmlElement(name = "ACTIV_IE_3")
	public String getActivIe3() {
		return activIe3;
	}

	public void setActivIe3(String activIe3) {
		this.activIe3 = activIe3;
	}

	@XmlElement(name = "ACTIV_IE_4")
	public String getActivIe4() {
		return activIe4;
	}

	public void setActivIe4(String activIe4) {
		this.activIe4 = activIe4;
	}

	@XmlElement(name = "ACTIV_IE_5")
	public String getActivIe5() {
		return activIe5;
	}

	public void setActivIe5(String activIe5) {
		this.activIe5 = activIe5;
	}

	@XmlElement(name = "ACTIV_IE_6")
	public String getActivIe6() {
		return activIe6;
	}

	public void setActivIe6(String activIe6) {
		this.activIe6 = activIe6;
	}

	@XmlElement(name = "ACTIV_IE_7")
	public String getActivIe7() {
		return activIe7;
	}

	public void setActivIe7(String activIe7) {
		this.activIe7 = activIe7;
	}

	@XmlElement(name = "ACTIV_C3_5")
	public String getActivC35() {
		return activC35;
	}

	public void setActivC35(String activC35) {
		this.activC35 = activC35;
	}

	@XmlElement(name = "SERV_IE_1")
	public String getServIe1() {
		return servIe1;
	}

	public void setServIe1(String servIe1) {
		this.servIe1 = servIe1;
	}

	@XmlElement(name = "SERV_IE_2")
	public String getServIe2() {
		return servIe2;
	}

	public void setServIe2(String servIe2) {
		this.servIe2 = servIe2;
	}

	@XmlElement(name = "SERV_IE_3")
	public String getServIe3() {
		return servIe3;
	}

	public void setServIe3(String servIe3) {
		this.servIe3 = servIe3;
	}

	@XmlElement(name = "SERV_IE_4")
	public String getServIe4() {
		return servIe4;
	}

	public void setServIe4(String servIe4) {
		this.servIe4 = servIe4;
	}

	@XmlElement(name = "SERV_IE_5")
	public String getServIe5() {
		return servIe5;
	}

	public void setServIe5(String servIe5) {
		this.servIe5 = servIe5;
	}

	@XmlElement(name = "SERV_IE_6")
	public String getServIe6() {
		return servIe6;
	}

	public void setServIe6(String servIe6) {
		this.servIe6 = servIe6;
	}

	@XmlElement(name = "SERV_IE_6E")
	public String getServIe6e() {
		return servIe6e;
	}

	public void setServIe6e(String servIe6e) {
		this.servIe6e = servIe6e;
	}

	@XmlElement(name = "P108_2010")
	public String getP1082010() {
		return p1082010;
	}

	public void setP1082010(String p1082010) {
		this.p1082010 = p1082010;
	}

	@XmlElement(name = "P108_2011")
	public String getP1082011() {
		return p1082011;
	}

	public void setP1082011(String p1082011) {
		this.p1082011 = p1082011;
	}

	@XmlElement(name = "P108_2012")
	public String getP1082012() {
		return p1082012;
	}

	public void setP1082012(String p1082012) {
		this.p1082012 = p1082012;
	}

	@XmlElement(name = "P108_C2H1")
	public String getP108c2h1() {
		return p108c2h1;
	}

	public void setP108c2h1(String p108c2h1) {
		this.p108c2h1 = p108c2h1;
	}

	@XmlElement(name = "P108_C2M1")
	public String getP108c2m1() {
		return p108c2m1;
	}

	public void setP108c2m1(String p108c2m1) {
		this.p108c2m1 = p108c2m1;
	}

	@XmlElement(name = "P108_C2H2")
	public String getP108c2h2() {
		return p108c2h2;
	}

	public void setP108c2h2(String p108c2h2) {
		this.p108c2h2 = p108c2h2;
	}

	@XmlElement(name = "P108_C2M2")
	public String getP108c2m2() {
		return p108c2m2;
	}

	public void setP108c2m2(String p108c2m2) {
		this.p108c2m2 = p108c2m2;
	}

	@XmlElement(name = "P109_C2")
	public String getP109C2() {
		return p109C2;
	}

	public void setP109C2(String p109C2) {
		this.p109C2 = p109C2;
	}

	@XmlElement(name = "SAANEE")
	public String getSaanee() {
		return saanee;
	}

	public void setSaanee(String saanee) {
		this.saanee = saanee;
	}

	@XmlElement(name = "P201")
	public String getP201() {
		return p201;
	}

	public void setP201(String p201) {
		this.p201 = p201;
	}

	@XmlElement(name = "P201_ESP")
	public String getP201Esp() {
		return p201Esp;
	}

	public void setP201Esp(String p201Esp) {
		this.p201Esp = p201Esp;
	}

	@XmlElement(name = "P202")
	public String getP202() {
		return p202;
	}

	public void setP202(String p202) {
		this.p202 = p202;
	}

	@XmlElement(name = "P202_ESP")
	public String getP202Esp() {
		return p202Esp;
	}

	public void setP202Esp(String p202Esp) {
		this.p202Esp = p202Esp;
	}

	@XmlElement(name = "P203")
	public String getP203() {
		return p203;
	}

	public void setP203(String p203) {
		this.p203 = p203;
	}

	@XmlElement(name = "P203_ESP")
	public String getP203Esp() {
		return p203Esp;
	}

	public void setP203Esp(String p203Esp) {
		this.p203Esp = p203Esp;
	}

	@XmlElement(name = "P203_C3_1")
	public String getP203C31() {
		return p203C31;
	}

	public void setP203C31(String p203C31) {
		this.p203C31 = p203C31;
	}

	@XmlElement(name = "P203_C3_2")
	public String getP203C32() {
		return p203C32;
	}

	public void setP203C32(String p203C32) {
		this.p203C32 = p203C32;
	}

	@XmlElement(name = "P203_C3_3")
	public String getP203C33() {
		return p203C33;
	}

	public void setP203C33(String p203C33) {
		this.p203C33 = p203C33;
	}

	@XmlElement(name = "P203_C3_4")
	public String getP203C34() {
		return p203C34;
	}

	public void setP203C34(String p203C34) {
		this.p203C34 = p203C34;
	}

	@XmlElement(name = "P203_C3_5")
	public String getP203C35() {
		return p203C35;
	}

	public void setP203C35(String p203C35) {
		this.p203C35 = p203C35;
	}

	@XmlElement(name = "P203_C3_6")
	public String getP203C36() {
		return p203C36;
	}

	public void setP203C36(String p203C36) {
		this.p203C36 = p203C36;
	}

	@XmlElement(name = "P204")
	public String getP204() {
		return p204;
	}

	public void setP204(String p204) {
		this.p204 = p204;
	}

	@XmlElement(name = "P204_ESP")
	public String getP204Esp() {
		return p204Esp;
	}

	public void setP204Esp(String p204Esp) {
		this.p204Esp = p204Esp;
	}

	@XmlElement(name = "P204_C2")
	public String getP204C2() {
		return p204C2;
	}

	public void setP204C2(String p204C2) {
		this.p204C2 = p204C2;
	}

	@XmlElement(name = "P204_C2_ESP")
	public String getP204C2esp() {
		return p204C2esp;
	}

	public void setP204C2esp(String p204C2esp) {
		this.p204C2esp = p204C2esp;
	}

	@XmlElement(name = "P205")
	public String getP205() {
		return p205;
	}

	public void setP205(String p205) {
		this.p205 = p205;
	}

	@XmlElement(name = "P205_1")
	public String getP2051() {
		return p2051;
	}

	public void setP2051(String p2051) {
		this.p2051 = p2051;
	}

	@XmlElement(name = "P205_1_RD")
	public String getP2051rd() {
		return p2051rd;
	}

	public void setP2051rd(String p2051rd) {
		this.p2051rd = p2051rd;
	}

	@XmlElement(name = "P205_2")
	public String getP2052() {
		return p2052;
	}

	public void setP2052(String p2052) {
		this.p2052 = p2052;
	}

	@XmlElement(name = "P205_2_RD")
	public String getP2052rd() {
		return p2052rd;
	}

	public void setP2052rd(String p2052rd) {
		this.p2052rd = p2052rd;
	}

	@XmlElement(name = "P205_C2_ESP")
	public String getP205C2esp() {
		return p205C2esp;
	}

	public void setP205C2esp(String p205C2esp) {
		this.p205C2esp = p205C2esp;
	}

	@XmlElement(name = "P501")
	public String getP501() {
		return p501;
	}

	public void setP501(String p501) {
		this.p501 = p501;
	}

	@XmlElement(name = "P502_D")
	public String getP502D() {
		return p502D;
	}

	public void setP502D(String p502D) {
		this.p502D = p502D;
	}

	@XmlElement(name = "P502_M")
	public String getP502M() {
		return p502M;
	}

	public void setP502M(String p502M) {
		this.p502M = p502M;
	}

	@XmlElement(name = "P502_A")
	public String getP502A() {
		return p502A;
	}

	public void setP502A(String p502A) {
		this.p502A = p502A;
	}

	@XmlElement(name = "P504_C12")
	public String getP504C12() {
		return p504C12;
	}

	public void setP504C12(String p504C12) {
		this.p504C12 = p504C12;
	}

	@XmlElement(name = "P505_C3")
	public String getP505C3() {
		return p505C3;
	}

	public void setP505C3(String p505C3) {
		this.p505C3 = p505C3;
	}

	@XmlElement(name = "P505_C12_D")
	public String getP505C12D() {
		return p505C12D;
	}

	public void setP505C12D(String p505C12D) {
		this.p505C12D = p505C12D;
	}

	@XmlElement(name = "P505_C12_M")
	public String getP505C12M() {
		return p505C12M;
	}

	public void setP505C12M(String p505C12M) {
		this.p505C12M = p505C12M;
	}

	@XmlElement(name = "P505_C12_A")
	public String getP505C12A() {
		return p505C12A;
	}

	public void setP505C12A(String p505C12A) {
		this.p505C12A = p505C12A;
	}

	@XmlElement(name = "P506_C3_D")
	public String getP506C3D() {
		return p506C3D;
	}

	public void setP506C3D(String p506C3D) {
		this.p506C3D = p506C3D;
	}

	@XmlElement(name = "P506_C3_M")
	public String getP506C3M() {
		return p506C3M;
	}

	public void setP506C3M(String p506C3M) {
		this.p506C3M = p506C3M;
	}

	@XmlElement(name = "P506_C3_A")
	public String getP506C3A() {
		return p506C3A;
	}

	public void setP506C3A(String p506C3A) {
		this.p506C3A = p506C3A;
	}

	@XmlElement(name = "P508_C3")
	public String getP508C3() {
		return p508C3;
	}

	public void setP508C3(String p508C3) {
		this.p508C3 = p508C3;
	}

	@XmlElement(name = "P509_C3_D")
	public String getP509C3D() {
		return p509C3D;
	}

	public void setP509C3D(String p509C3D) {
		this.p509C3D = p509C3D;
	}

	@XmlElement(name = "P509_C3_M")
	public String getP509C3M() {
		return p509C3M;
	}

	public void setP509C3M(String p509C3M) {
		this.p509C3M = p509C3M;
	}

	@XmlElement(name = "P509_C3_A")
	public String getP509C3A() {
		return p509C3A;
	}

	public void setP509C3A(String p509C3A) {
		this.p509C3A = p509C3A;
	}

	@XmlElement(name = "BBTK_AULA_0")
	public String getBbtkAula0() {
		return bbtkAula0;
	}

	public void setBbtkAula0(String bbtkAula0) {
		this.bbtkAula0 = bbtkAula0;
	}

	@XmlElement(name = "BBTK_AULA_1")
	public String getBbtkAula1() {
		return bbtkAula1;
	}

	public void setBbtkAula1(String bbtkAula1) {
		this.bbtkAula1 = bbtkAula1;
	}

	@XmlElement(name = "BBTK_AULA_2")
	public String getBbtkAula2() {
		return bbtkAula2;
	}

	public void setBbtkAula2(String bbtkAula2) {
		this.bbtkAula2 = bbtkAula2;
	}

	@XmlElement(name = "BBTK_AULA_3")
	public String getBbtkAula3() {
		return bbtkAula3;
	}

	public void setBbtkAula3(String bbtkAula3) {
		this.bbtkAula3 = bbtkAula3;
	}

	@XmlElement(name = "BBTK_AULA_4")
	public String getBbtkAula4() {
		return bbtkAula4;
	}

	public void setBbtkAula4(String bbtkAula4) {
		this.bbtkAula4 = bbtkAula4;
	}

	@XmlElement(name = "BBTK_AULA_5")
	public String getBbtkAula5() {
		return bbtkAula5;
	}

	public void setBbtkAula5(String bbtkAula5) {
		this.bbtkAula5 = bbtkAula5;
	}

	@XmlElement(name = "BBTK_AULA_6")
	public String getBbtkAula6() {
		return bbtkAula6;
	}

	public void setBbtkAula6(String bbtkAula6) {
		this.bbtkAula6 = bbtkAula6;
	}

	@XmlElement(name = "BBTK_AULA_N")
	public String getBbtkAulaN() {
		return bbtkAulaN;
	}

	public void setBbtkAulaN(String bbtkAulaN) {
		this.bbtkAulaN = bbtkAulaN;
	}

	@XmlElement(name = "COMP_OPER")
	public String getCompOper() {
		return compOper;
	}

	public void setCompOper(String compOper) {
		this.compOper = compOper;
	}

	@XmlElement(name = "P601_C1")
	public String getP601C1() {
		return p601C1;
	}

	public void setP601C1(String p601C1) {
		this.p601C1 = p601C1;
	}

	@XmlElement(name = "P607_C3")
	public String getP607C3() {
		return p607C3;
	}

	public void setP607C3(String p607C3) {
		this.p607C3 = p607C3;
	}

	@XmlElement(name = "P608_C3_AP")
	public String getP608C3Ap() {
		return p608C3Ap;
	}

	public void setP608C3Ap(String p608C3Ap) {
		this.p608C3Ap = p608C3Ap;
	}

	@XmlElement(name = "P608_C3_NO")
	public String getP608C3No() {
		return p608C3No;
	}

	public void setP608C3No(String p608C3No) {
		this.p608C3No = p608C3No;
	}

	@XmlElement(name = "P608_C3_DNI")
	public String getP608C3Dni() {
		return p608C3Dni;
	}

	public void setP608C3Dni(String p608C3Dni) {
		this.p608C3Dni = p608C3Dni;
	}

	@XmlElement(name = "ANOTACIONES")
	public String getAnotaciones() {
		return anotaciones;
	}

	public void setAnotaciones(String anotaciones) {
		this.anotaciones = anotaciones;
	}

	@XmlElement(name = "P610_C3")
	public String getP610C3() {
		return p610C3;
	}

	public void setP610C3(String p610C3) {
		this.p610C3 = p610C3;
	}

	@XmlElement(name = "P610_C3_ESP")
	public String getP610C3esp() {
		return p610C3esp;
	}

	public void setP610C3esp(String p610C3esp) {
		this.p610C3esp = p610C3esp;
	}

	@XmlElement(name = "P610_C3_1")
	public String getP610C31() {
		return p610C31;
	}

	public void setP610C31(String p610C31) {
		this.p610C31 = p610C31;
	}

	@XmlElement(name = "P610_C3_2")
	public String getP610C32() {
		return p610C32;
	}

	public void setP610C32(String p610C32) {
		this.p610C32 = p610C32;
	}

	@XmlElement(name = "P610_C3_3")
	public String getP610C33() {
		return p610C33;
	}

	public void setP610C33(String p610C33) {
		this.p610C33 = p610C33;
	}

	@XmlElement(name = "P610_C3_4")
	public String getP610C34() {
		return p610C34;
	}

	public void setP610C34(String p610C34) {
		this.p610C34 = p610C34;
	}

	@XmlElement(name = "P610_C3_5")
	public String getP610C35() {
		return p610C35;
	}

	public void setP610C35(String p610C35) {
		this.p610C35 = p610C35;
	}

	@XmlElement(name = "P610_C3_6")
	public String getP610C36() {
		return p610C36;
	}

	public void setP610C36(String p610C36) {
		this.p610C36 = p610C36;
	}

	@XmlElement(name = "P610_C3_6E")
	public String getP610C36e() {
		return p610C36e;
	}

	public void setP610C36e(String p610C36e) {
		this.p610C36e = p610C36e;
	}

	@XmlElement(name = "DIR_APE")
	public String getDirApe() {
		return dirApe;
	}

	public void setDirApe(String dirApe) {
		this.dirApe = dirApe;
	}

	@XmlElement(name = "DIR_NOM")
	public String getDirNom() {
		return dirNom;
	}

	public void setDirNom(String dirNom) {
		this.dirNom = dirNom;
	}

	@XmlElement(name = "DIR_SITUA")
	public String getDirSitua() {
		return dirSitua;
	}

	public void setDirSitua(String dirSitua) {
		this.dirSitua = dirSitua;
	}

	@XmlElement(name = "DIR_DNI")
	public String getDirDni() {
		return dirDni;
	}

	public void setDirDni(String dirDni) {
		this.dirDni = dirDni;
	}

	@XmlElement(name = "DIR_FONO")
	public String getDirFono() {
		return dirFono;
	}

	public void setDirFono(String dirFono) {
		this.dirFono = dirFono;
	}

	@XmlElement(name = "DIR_EMAIL")
	public String getDirEmail() {
		return dirEmail;
	}

	public void setDirEmail(String dirEmail) {
		this.dirEmail = dirEmail;
	}

	@XmlElement(name = "SDIR_APE")
	public String getSdirApe() {
		return sdirApe;
	}

	public void setSdirApe(String sdirApe) {
		this.sdirApe = sdirApe;
	}

	@XmlElement(name = "SDIR_NOM")
	public String getSdirNom() {
		return sdirNom;
	}

	public void setSdirNom(String sdirNom) {
		this.sdirNom = sdirNom;
	}

	@XmlElement(name = "SDIR_SITUA")
	public String getSdirSitua() {
		return sdirSitua;
	}

	public void setSdirSitua(String sdirSitua) {
		this.sdirSitua = sdirSitua;
	}

	@XmlElement(name = "SDIR_DNI")
	public String getSdirDni() {
		return sdirDni;
	}

	public void setSdirDni(String sdirDni) {
		this.sdirDni = sdirDni;
	}

	@XmlElement(name = "SDIR_FONO")
	public String getSdirFono() {
		return sdirFono;
	}

	public void setSdirFono(String sdirFono) {
		this.sdirFono = sdirFono;
	}

	@XmlElement(name = "SDIR_EMAIL")
	public String getSdirEmail() {
		return sdirEmail;
	}

	public void setSdirEmail(String sdirEmail) {
		this.sdirEmail = sdirEmail;
	}

	@XmlElement(name = "REC_CED_DIA")
	public String getRecCedDia() {
		return recCedDia;
	}

	public void setRecCedDia(String recCedDia) {
		this.recCedDia = recCedDia;
	}

	@XmlElement(name = "REC_CED_MES")
	public String getRecCedMes() {
		return recCedMes;
	}

	public void setRecCedMes(String recCedMes) {
		this.recCedMes = recCedMes;
	}

	@XmlElement(name = "CUL_CED_DIA")
	public String getCulCedDia() {
		return culCedDia;
	}

	public void setCulCedDia(String culCedDia) {
		this.culCedDia = culCedDia;
	}

	@XmlElement(name = "CUL_CED_MES")
	public String getCulCedMes() {
		return culCedMes;
	}

	public void setCulCedMes(String culCedMes) {
		this.culCedMes = culCedMes;
	}

	@XmlElement(name = "ULTIMO")
	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	@XmlElement(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name = "FUENTE")
	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	@XmlElement(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	/*
	 * @XmlElement(name="PERSONAL") public List<Matricula2013Personal>
	 * getMatricula2013PersonalList() { return matricula2013PersonalList; }
	 * 
	 * public void setMatricula2013PersonalList(List<Matricula2013Personal>
	 * matricula2013PersonalList) { this.matricula2013PersonalList =
	 * matricula2013PersonalList; }
	 * 
	 * @XmlElement(name="PERIFERICOS") public List<Matricula2013Perifericos>
	 * getMatricula2013PerifericosList() { return matricula2013PerifericosList; }
	 * 
	 * public void setMatricula2013PerifericosList(List<Matricula2013Perifericos>
	 * matricula2013PerifericosList) { this.matricula2013PerifericosList =
	 * matricula2013PerifericosList; }
	 * 
	 * @XmlElement(name="LOCAL_PRONOEI") public List<Matricula2013Localpronoei>
	 * getMatricula2013LocalpronoeiList() { return matricula2013LocalpronoeiList; }
	 * 
	 * public void setMatricula2013LocalpronoeiList(List<Matricula2013Localpronoei>
	 * matricula2013LocalpronoeiList) { this.matricula2013LocalpronoeiList =
	 * matricula2013LocalpronoeiList; }
	 * 
	 * @XmlElement(name="COORD_PRONOEI") public List<Matricula2013Coordpronoei>
	 * getMatricula2013CoordpronoeiList() { return matricula2013CoordpronoeiList; }
	 * 
	 * public void setMatricula2013CoordpronoeiList(List<Matricula2013Coordpronoei>
	 * matricula2013CoordpronoeiList) { this.matricula2013CoordpronoeiList =
	 * matricula2013CoordpronoeiList; }
	 * 
	 * @XmlElement(name="RECURSOS")
	 * 
	 * @XmlJavaTypeAdapter(Matricula2013RecursosMapAdapter.class) public Map<String,
	 * Matricula2013Recursos> getDetalleRecursos() { return detalleRecursos; }
	 * 
	 * 
	 * public void setDetalleRecursos(Map<String, Matricula2013Recursos>
	 * detalleRecursos) { this.detalleRecursos = detalleRecursos; }
	 * 
	 * @XmlElement(name="MATRICULA")
	 * 
	 * @XmlJavaTypeAdapter(Matricula2013MatriculaMapAdapter.class) public
	 * Map<String, Matricula2013Matricula> getDetalleMatricula() { return
	 * detalleMatricula; }
	 * 
	 * 
	 * public void setDetalleMatricula(Map<String, Matricula2013Matricula>
	 * detalleMatricula) { this.detalleMatricula = detalleMatricula; }
	 * 
	 * @XmlElement(name="SECCION")
	 * 
	 * @XmlJavaTypeAdapter(Matricula2013SeccionMapAdapter.class) public Map<String,
	 * Matricula2013Seccion> getDetalleSeccion() { return detalleSeccion; }
	 * 
	 * public void setDetalleSeccion(Map<String, Matricula2013Seccion>
	 * detalleSeccion) { this.detalleSeccion = detalleSeccion; }
	 * 
	 * @XmlElement(name="SAANEES")
	 * 
	 * @XmlJavaTypeAdapter(Matricula2013SaaneeMapAdapter.class) public Map<String,
	 * Matricula2013Saanee> getDetalleSaanee() { return detalleSaanee; }
	 * 
	 * public void setDetalleSaanee(Map<String, Matricula2013Saanee> detalleSaanee)
	 * { this.detalleSaanee = detalleSaanee; }
	 */

	@XmlElement(name = "NROCED")
	public String getNroCed() {
		return nroCed;
	}

	public void setNroCed(String nroCed) {
		this.nroCed = nroCed;
	}

	@XmlAttribute
	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	@XmlAttribute(name = "MSG")
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@XmlAttribute(name = "ESTADO_RPT")
	public String getEstadoRpt() {
		return estadoRpt;
	}

	public void setEstadoRpt(String estadoRpt) {
		this.estadoRpt = estadoRpt;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2013Cabecera)) {
			return false;
		}
		Matricula2013Cabecera other = (Matricula2013Cabecera) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}
	/*
	 * @XmlElement(name="CURSOS_CETPRO") public List<Matricula2013CetproCurso>
	 * getMatricula2013CetproCursoList() { return matricula2013CetproCursoList; }
	 * 
	 * 
	 * public void setMatricula2013CetproCursoList(List<Matricula2013CetproCurso>
	 * matricula2013CetproCursoList) { this.matricula2013CetproCursoList =
	 * matricula2013CetproCursoList; }
	 */

	@XmlElement(name = "MODATEN")
	public String getModaten() {
		return modaten;
	}

	public void setModaten(String modaten) {
		this.modaten = modaten;
	}

	@XmlElement(name = "EGESTORA")
	public String getEgestora() {
		return egestora;
	}

	public void setEgestora(String egestora) {
		this.egestora = egestora;
	}

	@XmlElement(name = "TIPODFINA")
	public String getTipodfina() {
		return tipodfina;
	}

	public void setTipodfina(String tipodfina) {
		this.tipodfina = tipodfina;
	}

	@XmlElement(name = "NUMRES")
	public String getNumres() {
		return numres;
	}

	public void setNumres(String numres) {
		this.numres = numres;
	}

	@XmlElement(name = "FECHARES_DD")
	public String getFecharesDD() {
		return fecharesDD;
	}

	public void setFecharesDD(String fecharesDD) {
		this.fecharesDD = fecharesDD;
	}

	@XmlElement(name = "FECHARES_MM")
	public String getFecharesMM() {
		return fecharesMM;
	}

	public void setFecharesMM(String fecharesMM) {
		this.fecharesMM = fecharesMM;
	}

	@XmlElement(name = "FECHARES_AA")
	public String getFecharesAA() {
		return fecharesAA;
	}

	public void setFecharesAA(String fecharesAA) {
		this.fecharesAA = fecharesAA;
	}

}
