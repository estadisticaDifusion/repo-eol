package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2015_sec304")
public class Local2015Sec304 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P304_NRO")
    private Integer p304Nro;
    @Column(name = "P304_1")
    private String p3041;
    @Column(name = "P304_2M1")
    private String p3042m1;
    @Column(name = "P304_2M2")
    private String p3042m2;
    @Column(name = "P304_2M3")
    private String p3042m3;
    @Column(name = "P304_2M4")
    private Integer p3042m4;
    @Column(name = "P304_2T1")
    private String p3042t1;
    @Column(name = "P304_2T2")
    private String p3042t2;
    @Column(name = "P304_2T3")
    private String p3042t3;
    @Column(name = "P304_2T4")
    private Integer p3042t4;
    @Column(name = "P304_2N1")
    private String p3042n1;
    @Column(name = "P304_2N2")
    private String p3042n2;
    @Column(name = "P304_2N3")
    private String p3042n3;
    @Column(name = "P304_2N4")
    private Integer p3042n4;
    @Column(name = "P304_3")
    private Integer p3043;
    @Column(name = "P304_4")
    private String p3044;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2015Cabecera local2015Cabecera;

    public Local2015Sec304() {
    }

    public Local2015Sec304(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P304_NRO")
    public Integer getP304Nro() {
        return p304Nro;
    }

    public void setP304Nro(Integer p304Nro) {
        this.p304Nro = p304Nro;
    }

    @XmlElement(name="P304_1")
    public String getP3041() {
        return p3041;
    }

    public void setP3041(String p3041) {
        this.p3041 = p3041;
    }

    @XmlElement(name="P304_2M1")
    public String getP3042m1() {
        return p3042m1;
    }

    public void setP3042m1(String p3042m1) {
        this.p3042m1 = p3042m1;
    }

    @XmlElement(name="P304_2M2")
    public String getP3042m2() {
        return p3042m2;
    }

    public void setP3042m2(String p3042m2) {
        this.p3042m2 = p3042m2;
    }

    @XmlElement(name="P304_2M3")
    public String getP3042m3() {
        return p3042m3;
    }

    public void setP3042m3(String p3042m3) {
        this.p3042m3 = p3042m3;
    }

    @XmlElement(name="P304_2M4")
    public Integer getP3042m4() {
        return p3042m4;
    }

    public void setP3042m4(Integer p3042m4) {
        this.p3042m4 = p3042m4;
    }

    @XmlElement(name="P304_2T1")
    public String getP3042t1() {
        return p3042t1;
    }

    public void setP3042t1(String p3042t1) {
        this.p3042t1 = p3042t1;
    }

    @XmlElement(name="P304_2T2")
    public String getP3042t2() {
        return p3042t2;
    }

    public void setP3042t2(String p3042t2) {
        this.p3042t2 = p3042t2;
    }

    @XmlElement(name="P304_2T3")
    public String getP3042t3() {
        return p3042t3;
    }

    public void setP3042t3(String p3042t3) {
        this.p3042t3 = p3042t3;
    }

    @XmlElement(name="P304_2T4")
    public Integer getP3042t4() {
        return p3042t4;
    }

    public void setP3042t4(Integer p3042t4) {
        this.p3042t4 = p3042t4;
    }

    @XmlElement(name="P304_2N1")
    public String getP3042n1() {
        return p3042n1;
    }

    public void setP3042n1(String p3042n1) {
        this.p3042n1 = p3042n1;
    }

    @XmlElement(name="P304_2N2")
    public String getP3042n2() {
        return p3042n2;
    }

    public void setP3042n2(String p3042n2) {
        this.p3042n2 = p3042n2;
    }

    @XmlElement(name="P304_2N3")
    public String getP3042n3() {
        return p3042n3;
    }

    public void setP3042n3(String p3042n3) {
        this.p3042n3 = p3042n3;
    }

    @XmlElement(name="P304_2N4")
    public Integer getP3042n4() {
        return p3042n4;
    }

    public void setP3042n4(Integer p3042n4) {
        this.p3042n4 = p3042n4;
    }

    @XmlElement(name="P304_3")
    public Integer getP3043() {
        return p3043;
    }

    public void setP3043(Integer p3043) {
        this.p3043 = p3043;
    }

    @XmlElement(name="P304_4")
    public String getP3044() {
        return p3044;
    }

    public void setP3044(String p3044) {
        this.p3044 = p3044;
    }

    @XmlTransient
    public Local2015Cabecera getLocal2015Cabecera() {
        return local2015Cabecera;
    }

    public void setLocal2015Cabecera(Local2015Cabecera local2015Cabecera) {
        this.local2015Cabecera = local2015Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2015Sec304)) {
            return false;
        }
        Local2015Sec304 other = (Local2015Sec304) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
}
