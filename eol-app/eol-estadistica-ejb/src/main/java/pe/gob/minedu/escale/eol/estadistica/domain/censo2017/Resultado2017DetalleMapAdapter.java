package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Resultado2017DetalleMapAdapter.Resultado2017DetalleList;

/**
 *
 * @author JBEDRILLANA
 */
public class Resultado2017DetalleMapAdapter  extends XmlAdapter<Resultado2017DetalleList, Map<String, Resultado2017Detalle>> {


    private static final Logger LOGGER = Logger.getLogger(Resultado2017DetalleMapAdapter.class.getName());

    static class Resultado2017DetalleList {

        private List<Resultado2017Detalle> detalle;

        private Resultado2017DetalleList(ArrayList<Resultado2017Detalle> lista) {
            detalle = lista;
        }

        public Resultado2017DetalleList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Resultado2017Detalle> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Resultado2017Detalle> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Resultado2017Detalle> unmarshal(Resultado2017DetalleList v) throws Exception {
        Map<String, Resultado2017Detalle> map = new HashMap<String, Resultado2017Detalle>();
        for (Resultado2017Detalle detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Resultado2017DetalleList marshal(Map<String, Resultado2017Detalle> v) throws Exception {
        if(v==null)
        {   LOGGER.fine("no hay detalles");
            return null;
        }
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Resultado2017Detalle> lista = new ArrayList<Resultado2017Detalle>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Resultado2017Detalle $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Resultado2017DetalleList list = new Resultado2017DetalleList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }

}
