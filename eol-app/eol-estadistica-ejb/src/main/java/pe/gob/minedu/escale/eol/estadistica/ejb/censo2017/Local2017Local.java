package pe.gob.minedu.escale.eol.estadistica.ejb.censo2017;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Local2017Cabecera;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Local
public interface Local2017Local {
	void create(Local2017Cabecera entity);

	Local2017Cabecera findByCodLocal(String codLocal);

	void createCabecera(Local2017Cabecera entity);
}
