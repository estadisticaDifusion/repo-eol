/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_carreras")
public class Matricula2017Carreras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NRO")
    private String nro;
    @Basic(optional = false)
    @Column(name = "NROCED")
    private String nroced;
    @Basic(optional = false)
    @Column(name = "CUADRO")
    private String cuadro;
    @Column(name = "CODCARR")
    private String codcarr;
    @Basic(optional = false)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "NRO_RES")
    private String nroRes;
    @Column(name = "FECHA_RES")
    private String fechaRes;
    @Column(name = "DURACION")
    private String duracion;
    @Column(name = "R1")
    private String r1;
    @Column(name = "R2")
    private String r2;
    @Column(name = "DS")
    private Integer ds;
    @Column(name = "NHL")
    private Integer nhl;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2017Cabecera matricula2017Cabecera;

    public Matricula2017Carreras() {
    }

    public Matricula2017Carreras(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2017Carreras(Long idEnvio, String nro, String nroced, String cuadro, String descrip) {
        this.idEnvio = idEnvio;
        this.nro = nro;
        this.nroced = nroced;
        this.cuadro = cuadro;
        this.descrip = descrip;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlElement(name = "CODCARR")
    public String getCodcarr() {
        return codcarr;
    }

    public void setCodcarr(String codcarr) {
        this.codcarr = codcarr;
    }

    @XmlElement(name = "DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name = "NRO_RES")
    public String getNroRes() {
        return nroRes;
    }

    public void setNroRes(String nroRes) {
        this.nroRes = nroRes;
    }

    @XmlElement(name = "FECHA_RES")
    public String getFechaRes() {
        return fechaRes;
    }

    public void setFechaRes(String fechaRes) {
        this.fechaRes = fechaRes;
    }

    @XmlElement(name = "DURACION")
    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    @XmlElement(name = "R1")
    public String getR1() {
        return r1;
    }

    public void setR1(String r1) {
        this.r1 = r1;
    }

    @XmlElement(name = "R2")
    public String getR2() {
        return r2;
    }

    public void setR2(String r2) {
        this.r2 = r2;
    }

    @XmlElement(name = "DS")
    public Integer getDs() {
        return ds;
    }

    public void setDs(Integer ds) {
        this.ds = ds;
    }

    @XmlElement(name = "NHL")
    public Integer getNhl() {
        return nhl;
    }

    public void setNhl(Integer nhl) {
        this.nhl = nhl;
    }

    @XmlTransient
    public Matricula2017Cabecera getMatricula2017Cabecera() {
        return matricula2017Cabecera;
    }

    public void setMatricula2017Cabecera(Matricula2017Cabecera matricula2017Cabecera) {
        this.matricula2017Cabecera = matricula2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2017Carreras)) {
            return false;
        }
        Matricula2017Carreras other = (Matricula2017Carreras) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Matricula2017Carreras[idEnvio=" + idEnvio + "]";
    }
}
