/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;

import pe.gob.minedu.escale.eol.portlets.ejb.domain.EolMensajeria;

/**
 *
 * @author JMATAMOROS
 */
@Stateless
public class MensajeriaEOLFacade extends AbstractFacade<EolMensajeria> {
	
	@PersistenceContext(unitName = "eol-PU") // eol-portletPU
	private EntityManager em;
	
	// private static String TODOS = "";

	public MensajeriaEOLFacade() {
		super(EolMensajeria.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public int countFindByusuarioDestino(String usuario) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		javax.persistence.criteria.CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class);
		javax.persistence.criteria.Root<EolMensajeria> rt = cq.from(EolMensajeria.class);
		cq.select(criteriaBuilder.count(rt));
		cq.where(criteriaBuilder.equal(rt.get("usuarioDestino"), usuario));
		javax.persistence.Query q = getEntityManager().createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<EolMensajeria> findByUsuarioDestino(String usuario, int rowIni, int rowMax) {
		Query q = em
				.createQuery("SELECT M FROM EolMensajeria M WHERE M.usuarioDestino=:usuario ORDER BY M.fechaCrea DESC");
		q.setParameter("usuario", usuario);
		q.setFirstResult(rowIni).setMaxResults(rowMax);
		return q.getResultList();

	}

	@SuppressWarnings("unchecked")
	public List<EolMensajeria> findByUsuarioOrigen(String usuario, int rowIni, int rowMax) {
		Query q = em
				.createQuery("SELECT M FROM EolMensajeria M WHERE M.usuarioOrigen=:usuario ORDER BY M.fechaCrea DESC");
		q.setParameter("usuario", usuario);
		q.setFirstResult(rowIni).setMaxResults(rowMax);
		return q.getResultList();

	}

	@SuppressWarnings("unchecked")
	public List<EolMensajeria> findByUsuarioOrigenAndDestino(String uOrigen, String uDestino, int rowIni, int rowMax) {
		StringBuilder strB = new StringBuilder("SELECT M FROM EolMensajeria M");
		Query q = null;
		if (uDestino != null && uOrigen == null) {
			strB.append(" WHERE M.usuarioDestino=:uDestino AND M.usuarioOrigen IS NULL ORDER BY M.fechaCrea DESC");
			q = em.createQuery(strB.toString());
			q.setParameter("uDestino", uDestino);
		} else if (uDestino == null && uOrigen != null) {
			strB.append(" WHERE M.usuarioDestino IS NULL AND M.usuarioOrigen=:uOrigen ORDER BY M.fechaCrea DESC");
			q = em.createQuery(strB.toString());
			q.setParameter("uOrigen", uOrigen);
		} else if (uDestino != null && uOrigen != null) {
			strB.append(" WHERE M.usuarioOrigen=:uOrigen AND M.usuarioDestino=:uDestino ORDER BY M.fechaCrea DESC");
			q = em.createQuery(strB.toString());
			q.setParameter("uOrigen", uOrigen);
			q.setParameter("uDestino", uDestino);
		} else {
			q = em.createQuery(strB.toString());
		}
		q.setFirstResult(rowIni).setMaxResults(rowMax);
		return q.getResultList();
	}

	public int countFindByUsuarioOrigenAndDestino(String uOrigen, String uDestino) {
		StringBuilder strB = new StringBuilder("SELECT COUNT(M) FROM EolMensajeria M");
		Query q = null;
		if (uDestino != null && uOrigen == null) {
			strB.append(" WHERE M.usuarioDestino=:uDestino AND M.usuarioOrigen IS NULL ORDER BY M.fechaCrea DESC");
			q = em.createQuery(strB.toString());
			q.setParameter("uDestino", uDestino);
		} else if (uDestino == null && uOrigen != null) {
			strB.append(" WHERE M.usuarioDestino IS NULL AND M.usuarioOrigen=:uOrigen ORDER BY M.fechaCrea DESC");
			q = em.createQuery(strB.toString());
			q.setParameter("uOrigen", uOrigen);
		} else if (uDestino != null && uOrigen != null) {
			strB.append(" WHERE M.usuarioOrigen=:uOrigen AND M.usuarioDestino=:uDestino ORDER BY M.fechaCrea DESC");
			q = em.createQuery(strB.toString());
			q.setParameter("uOrigen", uOrigen);
			q.setParameter("uDestino", uDestino);
		} else {
			q = em.createQuery(strB.toString());
		}
		return ((Long) q.getSingleResult()).intValue();
	}

	public int countFindByUsuarioOrigenAll(String uOrigen) {
		StringBuilder strB = new StringBuilder("SELECT COUNT(M) FROM EolMensajeria M");
		Query q = null;
		strB.append(" WHERE M.usuarioOrigen=:uOrigen ORDER BY M.fechaCrea DESC");
		q = em.createQuery(strB.toString());
		q.setParameter("uOrigen", uOrigen);

		return ((Long) q.getSingleResult()).intValue();
	}

	public int countFindByUsuarioOrigen(String uOrigen) {
		StringBuilder strB = new StringBuilder("SELECT COUNT(M) FROM EolMensajeria M");
		Query q = null;
		strB.append(" WHERE M.usuarioDestino IS NULL AND M.usuarioOrigen=:uOrigen ORDER BY M.fechaCrea DESC");
		q = em.createQuery(strB.toString());
		q.setParameter("uOrigen", uOrigen);

		return ((Long) q.getSingleResult()).intValue();
	}

}
