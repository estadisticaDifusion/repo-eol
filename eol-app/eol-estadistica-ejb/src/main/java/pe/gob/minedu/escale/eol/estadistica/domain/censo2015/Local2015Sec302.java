package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "local2015_sec302")
public class Local2015Sec302 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NRO")
    private int nro;
    @Basic(optional = false)
    @Column(name = "CUADRO")
    private String cuadro;
    @Column(name = "P302_1")
    private String p3021;
    @Column(name = "P302_1ESP")
    private String p3021esp;
    @Column(name = "P302_2")
    private Integer p3022;
    @Column(name = "P302_3")
    private Integer p3023;
    @Column(name = "P302_4")
    private Integer p3024;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2015Cabecera local2015Cabecera;

    public Local2015Sec302() {
    }

    public Local2015Sec302(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Local2015Sec302(Long idEnvio, int nro, String cuadro) {
        this.idEnvio = idEnvio;
        this.nro = nro;
        this.cuadro = cuadro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="NRO")
    public int getNro() {
        return nro;
    }

    public void setNro(int nro) {
        this.nro = nro;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlElement(name="P302_1")
    public String getP3021() {
        return p3021;
    }

    public void setP3021(String p3021) {
        this.p3021 = p3021;
    }

    @XmlElement(name="P302_1ESP")
    public String getP3021esp() {
        return p3021esp;
    }

    public void setP3021esp(String p3021esp) {
        this.p3021esp = p3021esp;
    }

    @XmlElement(name="P302_2")
    public Integer getP3022() {
        return p3022;
    }

    public void setP3022(Integer p3022) {
        this.p3022 = p3022;
    }

    @XmlElement(name="P302_3")
    public Integer getP3023() {
        return p3023;
    }

    public void setP3023(Integer p3023) {
        this.p3023 = p3023;
    }

    @XmlElement(name="P302_4")
    public Integer getP3024() {
        return p3024;
    }

    public void setP3024(Integer p3024) {
        this.p3024 = p3024;
    }

    @XmlTransient
    public Local2015Cabecera getLocal2015Cabecera() {
        return local2015Cabecera;
    }

    public void setLocal2015Cabecera(Local2015Cabecera local2015Cabecera) {
        this.local2015Cabecera = local2015Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2015Sec302)) {
            return false;
        }
        Local2015Sec302 other = (Local2015Sec302) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
