/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2013.Matricula2013SaaneeMapAdapter.Matricula2013SaaneeList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2013SaaneeMapAdapter extends XmlAdapter<Matricula2013SaaneeList, Map<String, Matricula2013Saanee>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2013SaaneeMapAdapter.class.getName());

    static class Matricula2013SaaneeList {

        private List<Matricula2013Saanee> detalle;

        private Matricula2013SaaneeList(ArrayList<Matricula2013Saanee> lista) {
            detalle = lista;
        }

        public Matricula2013SaaneeList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2013Saanee> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2013Saanee> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2013Saanee> unmarshal(Matricula2013SaaneeList v) throws Exception {
        Map<String, Matricula2013Saanee> map = new HashMap<String, Matricula2013Saanee>();
        for (Matricula2013Saanee detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2013SaaneeList marshal(Map<String, Matricula2013Saanee> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;
        }
        ArrayList<Matricula2013Saanee> lista = new ArrayList<Matricula2013Saanee>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2013Saanee $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2013SaaneeList list = new Matricula2013SaaneeList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
