/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Resultado2015DetalleMapAdapter.Resultado2015DetalleList;

/**
 *
 * @author Administrador
 */
public class Resultado2015DetalleMapAdapter extends XmlAdapter<Resultado2015DetalleList, Map<String, Resultado2015Detalle>> {

    private static final Logger LOGGER = Logger.getLogger(Resultado2015DetalleMapAdapter.class.getName());

    static class Resultado2015DetalleList {

        private List<Resultado2015Detalle> detalle;

        private Resultado2015DetalleList(ArrayList<Resultado2015Detalle> lista) {
            detalle = lista;
        }

        public Resultado2015DetalleList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Resultado2015Detalle> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Resultado2015Detalle> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Resultado2015Detalle> unmarshal(Resultado2015DetalleList v) throws Exception {
        Map<String, Resultado2015Detalle> map = new HashMap<String, Resultado2015Detalle>();
        for (Resultado2015Detalle detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Resultado2015DetalleList marshal(Map<String, Resultado2015Detalle> v) throws Exception {
        if(v==null)
        {   LOGGER.fine("no hay detalles");
            return null;
        }
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Resultado2015Detalle> lista = new ArrayList<Resultado2015Detalle>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Resultado2015Detalle $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Resultado2015DetalleList list = new Resultado2015DetalleList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }

}
