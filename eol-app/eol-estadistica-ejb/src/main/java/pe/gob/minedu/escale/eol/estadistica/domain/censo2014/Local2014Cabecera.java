/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CMOLINA
 */
@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2014_cabecera")
public class Local2014Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Size(max = 6)
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Size(max = 80)
    @Column(name = "NOMBVIA")
    private String nombvia;
    @Size(max = 20)
    @Column(name = "NUMERO")
    private String numero;
    @Size(max = 10)
    @Column(name = "MANZANA")
    private String manzana;
    @Size(max = 15)
    @Column(name = "LOTE")
    private String lote;
    @Size(max = 2)
    @Column(name = "CATEGORIA")
    private String categoria;
    @Size(max = 50)
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Size(max = 15)
    @Column(name = "ETAPA")
    private String etapa;
    @Size(max = 30)
    @Column(name = "SECTOR")
    private String sector;
    @Size(max = 15)
    @Column(name = "ZONA")
    private String zona;
    @Size(max = 30)
    @Column(name = "OTROS")
    private String otros;
    @Size(max = 2)
    @Column(name = "CATEG_CP")
    private String categCp;
    @Size(max = 80)
    @Column(name = "CEN_POB")
    private String cenPob;
    @Size(max = 10)
    @Column(name = "CODCCPP")
    private String codccpp;
    @Size(max = 200)
    @Column(name = "REFERENCIA")
    private String referencia;
    @Size(max = 6)
    @Column(name = "CODGEO")
    private String codgeo;
    @Size(max = 30)
    @Column(name = "DPTO")
    private String dpto;
    @Size(max = 45)
    @Column(name = "PROV")
    private String prov;
    @Size(max = 60)
    @Column(name = "DISTRITO")
    private String distrito;
    @Size(max = 6)
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "COD_AREA")
    private String codArea;
    @Column(name = "COND_TEN")
    private String condTen;
    @Column(name = "PROPLOCAL")
    private String proplocal;
    @Column(name = "GESTION")
    private String gestion;
    @Column(name = "P201")
    private String p201;
    @Column(name = "P202_1")
    private String p2021;
    @Column(name = "P202_2")
    private String p2022;
    @Column(name = "P202_3")
    private String p2023;
    @Column(name = "P202_4")
    private String p2024;
    @Column(name = "P202_5")
    private String p2025;
    @Column(name = "P202_6")
    private String p2026;
    @Column(name = "P202_7")
    private String p2027;
    @Column(name = "P202_8")
    private String p2028;
    @Column(name = "P202_9")
    private String p2029;
    @Column(name = "P202_10")
    private String p20210;
    @Column(name = "P203")
    private String p203;
    @Column(name = "P204")
    private String p204;
    @Column(name = "P205")
    private Integer p205;
    @Column(name = "P206")
    private String p206;
    @Column(name = "P207")
    private String p207;
    @Column(name = "P208")
    private String p208;
    @Column(name = "P209_0TOT")
    private Integer p2090tot;
    @Column(name = "P209_0DSK")
    private Integer p2090dsk;
    @Column(name = "P209_0LXO")
    private Integer p2090lxo;
    @Column(name = "P209_0OTR")
    private Integer p2090otr;
    @Column(name = "P209_1TOT")
    private Integer p2091tot;
    @Column(name = "P209_1DSK")
    private Integer p2091dsk;
    @Column(name = "P209_1LXO")
    private Integer p2091lxo;
    @Column(name = "P209_1OTR")
    private Integer p2091otr;
    @Column(name = "P209_2TOT")
    private Integer p2092tot;
    @Column(name = "P209_2DSK")
    private Integer p2092dsk;
    @Column(name = "P209_2LXO")
    private Integer p2092lxo;
    @Column(name = "P209_2OTR")
    private Integer p2092otr;
    @Column(name = "P209_3TOT")
    private Integer p2093tot;
    @Column(name = "P209_3DSK")
    private Integer p2093dsk;
    @Column(name = "P209_3LXO")
    private Integer p2093lxo;
    @Column(name = "P209_3OTR")
    private Integer p2093otr;
    @Column(name = "P210_0TOT")
    private Integer p2100tot;
    @Column(name = "P210_0CNX")
    private Integer p2100cnx;
    @Column(name = "P210_0NCNX")
    private Integer p2100ncnx;
    @Column(name = "P210_1TOT")
    private Integer p2101tot;
    @Column(name = "P210_1CNX")
    private Integer p2101cnx;
    @Column(name = "P210_1NCNX")
    private Integer p2101ncnx;
    @Column(name = "P210_2TOT")
    private Integer p2102tot;
    @Column(name = "P210_2CNX")
    private Integer p2102cnx;
    @Column(name = "P210_2NCNX")
    private Integer p2102ncnx;
    @Column(name = "P210_3TOT")
    private Integer p2103tot;
    @Column(name = "P210_3CNX")
    private Integer p2103cnx;
    @Column(name = "P210_3NCNX")
    private Integer p2103ncnx;
    @Column(name = "P211_0CA")
    private Integer p2110ca;
    @Column(name = "P211_0SI")
    private Integer p2110si;
    @Column(name = "P211_0ME")
    private Integer p2110me;
    @Column(name = "P211_1CA")
    private Integer p2111ca;
    @Column(name = "P211_1SI")
    private Integer p2111si;
    @Column(name = "P211_1ME")
    private Integer p2111me;
    @Column(name = "P211_2CA")
    private Integer p2112ca;
    @Column(name = "P211_2SI")
    private Integer p2112si;
    @Column(name = "P211_2ME")
    private Integer p2112me;
    @Column(name = "P211_3CA")
    private Integer p2113ca;
    @Column(name = "P211_3SI")
    private Integer p2113si;
    @Column(name = "P211_3ME")
    private Integer p2113me;
    @Column(name = "P212_INI")
    private Integer p212Ini;
    @Column(name = "P212_1Y2P")
    private Integer p2121y2p;
    @Column(name = "P212_3A6P")
    private Integer p2123a6p;
    @Column(name = "P212_SCCS")
    private Integer p212Sccs;
    @Column(name = "P213")
    private Integer p213;
    @Column(name = "P214")
    private String p214;
    @Column(name = "P215")
    private String p215;
    @Size(max = 50)
    @Column(name = "P215_ESP")
    private String p215Esp;
    @Column(name = "P216")
    private String p216;
    @Column(name = "P217")
    private String p217;
    @Column(name = "P218_01")
    private Integer p21801;
    @Column(name = "P218_02")
    private Integer p21802;
    @Column(name = "P218_03")
    private Integer p21803;
    @Column(name = "P218_04")
    private Integer p21804;
    @Column(name = "P218_05")
    private Integer p21805;
    @Column(name = "P218_11")
    private Integer p21811;
    @Column(name = "P218_12")
    private Integer p21812;
    @Column(name = "P218_13")
    private Integer p21813;
    @Column(name = "P218_14")
    private Integer p21814;
    @Column(name = "P218_15")
    private Integer p21815;
    @Column(name = "P218_21")
    private Integer p21821;
    @Column(name = "P218_22")
    private Integer p21822;
    @Column(name = "P218_23")
    private Integer p21823;
    @Column(name = "P218_24")
    private Integer p21824;
    @Column(name = "P218_25")
    private Integer p21825;
    @Column(name = "P218_31")
    private Integer p21831;
    @Column(name = "P218_32")
    private Integer p21832;
    @Column(name = "P218_33")
    private Integer p21833;
    @Column(name = "P218_34")
    private Integer p21834;
    @Column(name = "P218_35")
    private Integer p21835;
    @Column(name = "P219")
    private Integer p219;
    @Column(name = "P220")
    private String p220;
    @Column(name = "P221_11")
    private Integer p22111;
    @Column(name = "P221_12")
    private Integer p22112;
    @Column(name = "P221_13")
    private Integer p22113;
    @Column(name = "P221_14")
    private Integer p22114;
    @Column(name = "P221_21")
    private Integer p22121;
    @Column(name = "P221_22")
    private Integer p22122;
    @Column(name = "P221_23")
    private Integer p22123;
    @Column(name = "P221_24")
    private Integer p22124;
    @Column(name = "P221_31")
    private Integer p22131;
    @Column(name = "P221_32")
    private Integer p22132;
    @Column(name = "P221_33")
    private Integer p22133;
    @Column(name = "P221_34")
    private Integer p22134;
    @Column(name = "P301")
    private Integer p301;
    @Column(name = "P303_11")
    private Integer p30311;
    @Column(name = "P303_12")
    private Integer p30312;
    @Column(name = "P303_13")
    private Integer p30313;
    @Column(name = "P303_21")
    private Integer p30321;
    @Column(name = "P303_22")
    private Integer p30322;
    @Column(name = "P303_23")
    private Integer p30323;
    @Column(name = "P303_31")
    private Integer p30331;
    @Column(name = "P303_32")
    private Integer p30332;
    @Column(name = "P303_33")
    private Integer p30333;
    @Column(name = "P401")
    private Integer p401;
    @Column(name = "P601_1")
    private String p6011;
    @Column(name = "P601_2")
    private String p6012;
    @Column(name = "P601_3")
    private String p6013;
    @Column(name = "P601_4")
    private String p6014;
    @Column(name = "P601_5")
    private String p6015;
    @Column(name = "P601_61")
    private String p60161;
    @Size(max = 30)
    @Column(name = "P601_62")
    private String p60162;
    @Column(name = "P601_7")
    private String p6017;
    @Column(name = "P601_8")
    private String p6018;
    @Column(name = "P601_9")
    private String p6019;
    @Column(name = "P601_10")
    private String p60110;
    @Column(name = "P602_11")
    private String p60211;
    @Column(name = "P602_12")
    private String p60212;
    @Column(name = "P602_13")
    private String p60213;
    @Column(name = "P602_14")
    private String p60214;
    @Column(name = "P602_15")
    private String p60215;
    @Column(name = "P602_16")
    private String p60216;
    @Column(name = "P602_21")
    private String p60221;
    @Column(name = "P602_22")
    private String p60222;
    @Column(name = "P602_23")
    private String p60223;
    @Column(name = "P602_24")
    private String p60224;
    @Column(name = "P602_25")
    private String p60225;
    @Column(name = "P602_26")
    private String p60226;
    @Column(name = "P603_1D")
    private Integer p6031d;
    @Column(name = "P603_1H")
    private Integer p6031h;
    @Column(name = "P603_1M")
    private Integer p6031m;
    @Column(name = "P603_2D")
    private Integer p6032d;
    @Column(name = "P603_2H")
    private Integer p6032h;
    @Column(name = "P603_2M")
    private Integer p6032m;
    @Column(name = "P7011")
    private String p7011;
    @Column(name = "P7012")
    private String p7012;    
    @Size(max = 15)
    @Column(name = "P7013_1")
    private String p70131;
    @Size(max = 15)
    @Column(name = "P7013_2")
    private String p70132;    
    @Column(name = "P7014")
    private String p7014;
    @Column(name = "P7015")
    private String p7015;
    @Size(max = 15)
    @Column(name = "P7016_1")
    private String p70161;
    @Size(max = 15)
    @Column(name = "P7016_2")
    private String p70162;
    @Column(name = "P702")
    private String p702;
    @Column(name = "P702_1")
    private String p7021;
    @Column(name = "P702_2")
    private String p7022;
    @Column(name = "P702_3")
    private String p7023;
    @Column(name = "P702_4")
    private String p7024;
    @Column(name = "P702_5")
    private String p7025;
    @Column(name = "P702_6")
    private String p7026;
    @Column(name = "P702_7")
    private String p7027;
    @Column(name = "P702_8")
    private String p7028;
    @Column(name = "P702_9")
    private String p7029;
    @Column(name = "P702_10")
    private String p70210;
    @Column(name = "P702_11")
    private String p70211;
    @Column(name = "P702_12")
    private String p70212;
    @Column(name = "P702_13")
    private String p70213;
    @Column(name = "P702_14")
    private String p70214;
    @Column(name = "P702_15")
    private String p70215;
    @Column(name = "P702_16")
    private String p70216;
    @Column(name = "P702_17")
    private String p70217;
    @Column(name = "P702_18")
    private String p70218;
    @Size(max = 50)
    @Column(name = "P702_18E")
    private String p70218e;
    @Column(name = "P703")
    private String p703;
    @Size(max = 50)
    @Column(name = "P703_ESP")
    private String p703Esp;
    @Column(name = "P7041")
    private String p7041;
    @Column(name = "P7042_11")
    private String p704211;
    @Size(max = 30)
    @Column(name = "P7042_12")
    private String p704212;
    @Column(name = "P7042_13")
    private String p704213;
    @Size(max = 20)
    @Column(name = "P7042_14")
    private String p704214;
    @Column(name = "P7042_21")
    private String p704221;
    @Size(max = 30)
    @Column(name = "P7042_22")
    private String p704222;
    @Column(name = "P7042_23")
    private String p704223;
    @Size(max = 20)
    @Column(name = "P7042_24")
    private String p704224;
    @Column(name = "P7042_31")
    private String p704231;
    @Size(max = 30)
    @Column(name = "P7042_32")
    private String p704232;
    @Column(name = "P7042_33")
    private String p704233;
    @Size(max = 20)
    @Column(name = "P7042_34")
    private String p704234;
    @Column(name = "P7042_41")
    private String p704241;
    @Size(max = 30)
    @Column(name = "P7042_42")
    private String p704242;
    @Column(name = "P7042_43")
    private String p704243;
    @Size(max = 20)
    @Column(name = "P7042_44")
    private String p704244;
    @Column(name = "P7042_51")
    private String p704251;
    @Size(max = 30)
    @Column(name = "P7042_52")
    private String p704252;
    @Column(name = "P7042_53")
    private String p704253;
    @Size(max = 20)
    @Column(name = "P7042_54")
    private String p704254;
    @Column(name = "P7051")
    private String p7051;
    @Column(name = "P7052")
    private String p7052;
    @Size(max = 200)
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Size(max = 50)
    @Column(name = "APE_DIR")
    private String apeDir;
    @Size(max = 30)
    @Column(name = "NOM_DIR")
    private String nomDir;
    @Column(name = "SITUA_DIR")
    private String situaDir;
    @Size(max = 8)
    @Column(name = "DNI_DIR")
    private String dniDir;
    @Size(max = 60)
    @Column(name = "EMAIL_DIR")
    private String emailDir;
    @Size(max = 20)
    @Column(name = "FONO_DIR")
    private String fonoDir;
    @Size(max = 2)
    @Column(name = "REC_CEDD")
    private String recCedd;
    @Size(max = 2)
    @Column(name = "REC_CEDM")
    private String recCedm;
    @Size(max = 2)
    @Column(name = "CUL_CEDD")
    private String culCedd;
    @Size(max = 2)
    @Column(name = "CUL_CEDM")
    private String culCedm;
    @Size(max = 4)
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaenvio;
    
    /*
    @OneToMany(mappedBy = "local2014Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2014Sec500> sec500;
    @OneToMany(mappedBy = "local2014Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy(value = "p104Nro")
    private List<Local2014Sec104> sec104;
    @OneToMany(mappedBy = "local2014Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2014Sec300> sec300;
    @OneToMany(mappedBy = "local2014Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2014Sec302> sec302;
    @OneToMany(mappedBy = "local2014Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2014Sec400> sec400;
    */
    
    @Transient
    private long token;
    @Transient
    private String msg;
    @Transient
    private String estadoRpt;

    public Local2014Cabecera() {
    }

    public Local2014Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name = "NOMBVIA")
    public String getNombvia() {
        return nombvia;
    }

    public void setNombvia(String nombvia) {
        this.nombvia = nombvia;
    }

    @XmlElement(name = "NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name = "MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name = "LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name = "CATEGORIA")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlElement(name = "NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name = "ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name = "SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name = "ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name = "OTROS")
    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    @XmlElement(name = "CATEG_CP")
    public String getCategCp() {
        return categCp;
    }

    public void setCategCp(String categCp) {
        this.categCp = categCp;
    }

    @XmlElement(name = "CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name = "CODCCPP")
    public String getCodccpp() {
        return codccpp;
    }

    public void setCodccpp(String codccpp) {
        this.codccpp = codccpp;
    }

    @XmlElement(name = "REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "COD_AREA")
    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    @XmlElement(name = "COND_TEN")
    public String getCondTen() {
        return condTen;
    }

    public void setCondTen(String condTen) {
        this.condTen = condTen;
    }

    @XmlElement(name = "PROPLOCAL")
    public String getProplocal() {
        return proplocal;
    }

    public void setProplocal(String proplocal) {
        this.proplocal = proplocal;
    }

    @XmlElement(name = "GESTION")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name = "P201")
    public String getP201() {
        return p201;
    }

    public void setP201(String p201) {
        this.p201 = p201;
    }

    @XmlElement(name = "P301")
    public Integer getP301() {
        return p301;
    }

    public void setP301(Integer p301) {
        this.p301 = p301;
    }

    @XmlElement(name = "P401")
    public Integer getP401() {
        return p401;
    }

    public void setP401(Integer p401) {
        this.p401 = p401;
    }

    @XmlElement(name = "P202_1")
    public String getP2021() {
        return p2021;
    }

    public void setP2021(String p2021) {
        this.p2021 = p2021;
    }

    @XmlElement(name = "P202_2")
    public String getP2022() {
        return p2022;
    }

    public void setP2022(String p2022) {
        this.p2022 = p2022;
    }

    @XmlElement(name = "P202_3")
    public String getP2023() {
        return p2023;
    }

    public void setP2023(String p2023) {
        this.p2023 = p2023;
    }

    @XmlElement(name = "P202_4")
    public String getP2024() {
        return p2024;
    }

    public void setP2024(String p2024) {
        this.p2024 = p2024;
    }

    @XmlElement(name = "P202_5")
    public String getP2025() {
        return p2025;
    }

    public void setP2025(String p2025) {
        this.p2025 = p2025;
    }    

    @XmlElement(name = "P203")
    public String getP203() {
        return p203;
    }

    public void setP203(String p203) {
        this.p203 = p203;
    }

    @XmlElement(name = "P204")
    public String getP204() {
        return p204;
    }

    public void setP204(String p204) {
        this.p204 = p204;
    }

    @XmlElement(name = "P205")
    public Integer getP205() {
        return p205;
    }

    public void setP205(Integer p205) {
        this.p205 = p205;
    }

    @XmlElement(name = "P206")
    public String getP206() {
        return p206;
    }

    public void setP206(String p206) {
        this.p206 = p206;
    }

    @XmlElement(name = "P207")
    public String getP207() {
        return p207;
    }

    public void setP207(String p207) {
        this.p207 = p207;
    }

    @XmlElement(name = "P208")
    public String getP208() {
        return p208;
    }

    public void setP208(String p208) {
        this.p208 = p208;
    }

    @XmlElement(name = "P209_0TOT")
    public Integer getP2090tot() {
        return p2090tot;
    }

    public void setP2090tot(Integer p2090tot) {
        this.p2090tot = p2090tot;
    }

    @XmlElement(name = "P209_0DSK")
    public Integer getP2090dsk() {
        return p2090dsk;
    }

    public void setP2090dsk(Integer p2090dsk) {
        this.p2090dsk = p2090dsk;
    }

    @XmlElement(name = "P209_0LXO")
    public Integer getP2090lxo() {
        return p2090lxo;
    }

    public void setP2090lxo(Integer p2090lxo) {
        this.p2090lxo = p2090lxo;
    }

    @XmlElement(name = "P209_0OTR")
    public Integer getP2090otr() {
        return p2090otr;
    }

    public void setP2090otr(Integer p2090otr) {
        this.p2090otr = p2090otr;
    }

    @XmlElement(name = "P209_1TOT")
    public Integer getP2091tot() {
        return p2091tot;
    }

    public void setP2091tot(Integer p2091tot) {
        this.p2091tot = p2091tot;
    }

    @XmlElement(name = "P209_1DSK")
    public Integer getP2091dsk() {
        return p2091dsk;
    }

    public void setP2091dsk(Integer p2091dsk) {
        this.p2091dsk = p2091dsk;
    }

    @XmlElement(name = "P209_1LXO")
    public Integer getP2091lxo() {
        return p2091lxo;
    }

    public void setP2091lxo(Integer p2091lxo) {
        this.p2091lxo = p2091lxo;
    }

    @XmlElement(name = "P209_1OTR")
    public Integer getP2091otr() {
        return p2091otr;
    }

    public void setP2091otr(Integer p2091otr) {
        this.p2091otr = p2091otr;
    }

    @XmlElement(name = "P209_2TOT")
    public Integer getP2092tot() {
        return p2092tot;
    }

    public void setP2092tot(Integer p2092tot) {
        this.p2092tot = p2092tot;
    }

    @XmlElement(name = "P209_2DSK")
    public Integer getP2092dsk() {
        return p2092dsk;
    }

    public void setP2092dsk(Integer p2092dsk) {
        this.p2092dsk = p2092dsk;
    }

    @XmlElement(name = "P209_2LXO")
    public Integer getP2092lxo() {
        return p2092lxo;
    }

    public void setP2092lxo(Integer p2092lxo) {
        this.p2092lxo = p2092lxo;
    }

    @XmlElement(name = "P209_2OTR")
    public Integer getP2092otr() {
        return p2092otr;
    }

    public void setP2092otr(Integer p2092otr) {
        this.p2092otr = p2092otr;
    }

    @XmlElement(name = "P209_3TOT")
    public Integer getP2093tot() {
        return p2093tot;
    }

    public void setP2093tot(Integer p2093tot) {
        this.p2093tot = p2093tot;
    }

    @XmlElement(name = "P209_3DSK")
    public Integer getP2093dsk() {
        return p2093dsk;
    }

    public void setP2093dsk(Integer p2093dsk) {
        this.p2093dsk = p2093dsk;
    }

    @XmlElement(name = "P209_3LXO")
    public Integer getP2093lxo() {
        return p2093lxo;
    }

    public void setP2093lxo(Integer p2093lxo) {
        this.p2093lxo = p2093lxo;
    }

    @XmlElement(name = "P209_3OTR")
    public Integer getP2093otr() {
        return p2093otr;
    }

    public void setP2093otr(Integer p2093otr) {
        this.p2093otr = p2093otr;
    }

    @XmlElement(name = "P210_0TOT")
    public Integer getP2100tot() {
        return p2100tot;
    }

    public void setP2100tot(Integer p2100tot) {
        this.p2100tot = p2100tot;
    }

    @XmlElement(name = "P210_0CNX")
    public Integer getP2100cnx() {
        return p2100cnx;
    }

    public void setP2100cnx(Integer p2100cnx) {
        this.p2100cnx = p2100cnx;
    }

    @XmlElement(name = "P210_0NCNX")
    public Integer getP2100ncnx() {
        return p2100ncnx;
    }

    public void setP2100ncnx(Integer p2100ncnx) {
        this.p2100ncnx = p2100ncnx;
    }

    @XmlElement(name = "P210_1TOT")
    public Integer getP2101tot() {
        return p2101tot;
    }

    public void setP2101tot(Integer p2101tot) {
        this.p2101tot = p2101tot;
    }

    @XmlElement(name = "P210_1CNX")
    public Integer getP2101cnx() {
        return p2101cnx;
    }

    public void setP2101cnx(Integer p2101cnx) {
        this.p2101cnx = p2101cnx;
    }

    @XmlElement(name = "P210_1NCNX")
    public Integer getP2101ncnx() {
        return p2101ncnx;
    }

    public void setP2101ncnx(Integer p2101ncnx) {
        this.p2101ncnx = p2101ncnx;
    }

    @XmlElement(name = "P210_2TOT")
    public Integer getP2102tot() {
        return p2102tot;
    }

    public void setP2102tot(Integer p2102tot) {
        this.p2102tot = p2102tot;
    }

    @XmlElement(name = "P210_2CNX")
    public Integer getP2102cnx() {
        return p2102cnx;
    }

    public void setP2102cnx(Integer p2102cnx) {
        this.p2102cnx = p2102cnx;
    }

    @XmlElement(name = "P210_2NCNX")
    public Integer getP2102ncnx() {
        return p2102ncnx;
    }

    public void setP2102ncnx(Integer p2102ncnx) {
        this.p2102ncnx = p2102ncnx;
    }

    @XmlElement(name = "P210_3TOT")
    public Integer getP2103tot() {
        return p2103tot;
    }

    public void setP2103tot(Integer p2103tot) {
        this.p2103tot = p2103tot;
    }

    @XmlElement(name = "P210_3CNX")
    public Integer getP2103cnx() {
        return p2103cnx;
    }

    public void setP2103cnx(Integer p2103cnx) {
        this.p2103cnx = p2103cnx;
    }

    @XmlElement(name = "P210_3NCNX")
    public Integer getP2103ncnx() {
        return p2103ncnx;
    }

    public void setP2103ncnx(Integer p2103ncnx) {
        this.p2103ncnx = p2103ncnx;
    }

    @XmlElement(name = "P211_0CA")
    public Integer getP2110ca() {
        return p2110ca;
    }

    public void setP2110ca(Integer p2110ca) {
        this.p2110ca = p2110ca;
    }

    @XmlElement(name = "P211_0SI")
    public Integer getP2110si() {
        return p2110si;
    }

    public void setP2110si(Integer p2110si) {
        this.p2110si = p2110si;
    }

    @XmlElement(name = "P211_0ME")
    public Integer getP2110me() {
        return p2110me;
    }

    public void setP2110me(Integer p2110me) {
        this.p2110me = p2110me;
    }

    @XmlElement(name = "P211_1CA")
    public Integer getP2111ca() {
        return p2111ca;
    }

    public void setP2111ca(Integer p2111ca) {
        this.p2111ca = p2111ca;
    }

    @XmlElement(name = "P211_1SI")
    public Integer getP2111si() {
        return p2111si;
    }

    public void setP2111si(Integer p2111si) {
        this.p2111si = p2111si;
    }

    @XmlElement(name = "P211_1ME")
    public Integer getP2111me() {
        return p2111me;
    }

    public void setP2111me(Integer p2111me) {
        this.p2111me = p2111me;
    }

    @XmlElement(name = "P211_2CA")
    public Integer getP2112ca() {
        return p2112ca;
    }

    public void setP2112ca(Integer p2112ca) {
        this.p2112ca = p2112ca;
    }

    @XmlElement(name = "P211_2SI")
    public Integer getP2112si() {
        return p2112si;
    }

    public void setP2112si(Integer p2112si) {
        this.p2112si = p2112si;
    }

    @XmlElement(name = "P211_2ME")
    public Integer getP2112me() {
        return p2112me;
    }

    public void setP2112me(Integer p2112me) {
        this.p2112me = p2112me;
    }

    @XmlElement(name = "P211_3CA")
    public Integer getP2113ca() {
        return p2113ca;
    }

    public void setP2113ca(Integer p2113ca) {
        this.p2113ca = p2113ca;
    }

    @XmlElement(name = "P211_3SI")
    public Integer getP2113si() {
        return p2113si;
    }

    public void setP2113si(Integer p2113si) {
        this.p2113si = p2113si;
    }

    @XmlElement(name = "P211_3ME")
    public Integer getP2113me() {
        return p2113me;
    }

    public void setP2113me(Integer p2113me) {
        this.p2113me = p2113me;
    }

    @XmlElement(name = "P212_INI")
    public Integer getP212Ini() {
        return p212Ini;
    }

    public void setP212Ini(Integer p212Ini) {
        this.p212Ini = p212Ini;
    }

    @XmlElement(name = "P212_1Y2P")
    public Integer getP2121y2p() {
        return p2121y2p;
    }

    public void setP2121y2p(Integer p2121y2p) {
        this.p2121y2p = p2121y2p;
    }

    @XmlElement(name = "P212_3A6P")
    public Integer getP2123a6p() {
        return p2123a6p;
    }

    public void setP2123a6p(Integer p2123a6p) {
        this.p2123a6p = p2123a6p;
    }

    @XmlElement(name = "P212_SCCS")
    public Integer getP212Sccs() {
        return p212Sccs;
    }

    public void setP212Sccs(Integer p212Sccs) {
        this.p212Sccs = p212Sccs;
    }

    @XmlElement(name = "P213")
    public Integer getP213() {
        return p213;
    }

    public void setP213(Integer p213) {
        this.p213 = p213;
    }

    @XmlElement(name = "P214")
    public String getP214() {
        return p214;
    }

    public void setP214(String p214) {
        this.p214 = p214;
    }

    @XmlElement(name = "P215")
    public String getP215() {
        return p215;
    }

    public void setP215(String p215) {
        this.p215 = p215;
    }

    @XmlElement(name = "P215_ESP")
    public String getP215Esp() {
        return p215Esp;
    }

    public void setP215esp(String p215Esp) {
        this.p215Esp = p215Esp;
    }

    @XmlElement(name = "P216")
    public String getP216() {
        return p216;
    }

    public void setP216(String p216) {
        this.p216 = p216;
    }

    @XmlElement(name = "P217")
    public String getP217() {
        return p217;
    }

    public void setP217(String p217) {
        this.p217 = p217;
    }

    @XmlElement(name = "P218_01")
    public Integer getP21801() {
        return p21801;
    }

    public void setP21801(Integer p21801) {
        this.p21801 = p21801;
    }

    @XmlElement(name = "P218_02")
    public Integer getP21802() {
        return p21802;
    }

    public void setP21802(Integer p21802) {
        this.p21802 = p21802;
    }

    @XmlElement(name = "P218_03")
    public Integer getP21803() {
        return p21803;
    }

    public void setP21803(Integer p21803) {
        this.p21803 = p21803;
    }

    @XmlElement(name = "P218_04")
    public Integer getP21804() {
        return p21804;
    }

    public void setP21804(Integer p21804) {
        this.p21804 = p21804;
    }

    @XmlElement(name = "P218_05")
    public Integer getP21805() {
        return p21805;
    }

    public void setP21805(Integer p21805) {
        this.p21805 = p21805;
    }

    @XmlElement(name = "P218_11")
    public Integer getP21811() {
        return p21811;
    }

    public void setP21811(Integer p21811) {
        this.p21811 = p21811;
    }

    @XmlElement(name = "P218_12")
    public Integer getP21812() {
        return p21812;
    }

    public void setP21812(Integer p21812) {
        this.p21812 = p21812;
    }

    @XmlElement(name = "P218_13")
    public Integer getP21813() {
        return p21813;
    }

    public void setP21813(Integer p21813) {
        this.p21813 = p21813;
    }

    @XmlElement(name = "P218_14")
    public Integer getP21814() {
        return p21814;
    }

    public void setP21814(Integer p21814) {
        this.p21814 = p21814;
    }

    @XmlElement(name = "P218_15")
    public Integer getP21815() {
        return p21815;
    }

    public void setP21815(Integer p21815) {
        this.p21815 = p21815;
    }

    @XmlElement(name = "P218_21")
    public Integer getP21821() {
        return p21821;
    }

    public void setP21821(Integer p21821) {
        this.p21821 = p21821;
    }

    @XmlElement(name = "P218_22")
    public Integer getP21822() {
        return p21822;
    }

    public void setP21822(Integer p21822) {
        this.p21822 = p21822;
    }

    @XmlElement(name = "P218_23")
    public Integer getP21823() {
        return p21823;
    }

    public void setP21823(Integer p21823) {
        this.p21823 = p21823;
    }

    @XmlElement(name = "P218_24")
    public Integer getP21824() {
        return p21824;
    }

    public void setP21824(Integer p21824) {
        this.p21824 = p21824;
    }

    @XmlElement(name = "P218_25")
    public Integer getP21825() {
        return p21825;
    }

    public void setP21825(Integer p21825) {
        this.p21825 = p21825;
    }

    @XmlElement(name = "P218_31")
    public Integer getP21831() {
        return p21831;
    }

    public void setP21831(Integer p21831) {
        this.p21831 = p21831;
    }

    @XmlElement(name = "P218_32")
    public Integer getP21832() {
        return p21832;
    }

    public void setP21832(Integer p21832) {
        this.p21832 = p21832;
    }

    @XmlElement(name = "P218_33")
    public Integer getP21833() {
        return p21833;
    }

    public void setP21833(Integer p21833) {
        this.p21833 = p21833;
    }

    @XmlElement(name = "P218_34")
    public Integer getP21834() {
        return p21834;
    }

    public void setP21834(Integer p21834) {
        this.p21834 = p21834;
    }

    @XmlElement(name = "P218_35")
    public Integer getP21835() {
        return p21835;
    }

    public void setP21835(Integer p21835) {
        this.p21835 = p21835;
    }

    @XmlElement(name = "P219")
    public Integer getP219() {
        return p219;
    }

    public void setP219(Integer p219) {
        this.p219 = p219;
    }

    @XmlElement(name = "P220")
    public String getP220() {
        return p220;
    }

    public void setP220(String p220) {
        this.p220 = p220;
    }

    @XmlElement(name = "P601_1")
    public String getP6011() {
        return p6011;
    }

    public void setP6011(String p6011) {
        this.p6011 = p6011;
    }

    @XmlElement(name = "P601_2")
    public String getP6012() {
        return p6012;
    }

    public void setP6012(String p6012) {
        this.p6012 = p6012;
    }

    @XmlElement(name = "P601_3")
    public String getP6013() {
        return p6013;
    }

    public void setP6013(String p6013) {
        this.p6013 = p6013;
    }

    @XmlElement(name = "P601_4")
    public String getP6014() {
        return p6014;
    }

    public void setP6014(String p6014) {
        this.p6014 = p6014;
    }

    @XmlElement(name = "P601_5")
    public String getP6015() {
        return p6015;
    }

    public void setP6015(String p6015) {
        this.p6015 = p6015;
    }

    @XmlElement(name = "P601_61")
    public String getP60161() {
        return p60161;
    }

    public void setP60161(String p60161) {
        this.p60161 = p60161;
    }

    @XmlElement(name = "P601_62")
    public String getP60162() {
        return p60162;
    }

    public void setP60162(String p60162) {
        this.p60162 = p60162;
    }

    @XmlElement(name = "P601_7")
    public String getP6017() {
        return p6017;
    }

    public void setP6017(String p6017) {
        this.p6017 = p6017;
    }

    @XmlElement(name = "P601_8")
    public String getP6018() {
        return p6018;
    }

    public void setP6018(String p6018) {
        this.p6018 = p6018;
    }

    @XmlElement(name = "P601_9")
    public String getP6019() {
        return p6019;
    }

    public void setP6019(String p6019) {
        this.p6019 = p6019;
    }

    @XmlElement(name = "P601_10")
    public String getP60110() {
        return p60110;
    }

    public void setP60110(String p60110) {
        this.p60110 = p60110;
    }

    @XmlElement(name = "P602_11")
    public String getP60211() {
        return p60211;
    }

    public void setP60211(String p60211) {
        this.p60211 = p60211;
    }

    @XmlElement(name = "P602_12")
    public String getP60212() {
        return p60212;
    }

    public void setP60212(String p60212) {
        this.p60212 = p60212;
    }

    @XmlElement(name = "P602_13")
    public String getP60213() {
        return p60213;
    }

    public void setP60213(String p60213) {
        this.p60213 = p60213;
    }

    @XmlElement(name = "P602_14")
    public String getP60214() {
        return p60214;
    }

    public void setP60214(String p60214) {
        this.p60214 = p60214;
    }

    @XmlElement(name = "P602_15")
    public String getP60215() {
        return p60215;
    }

    public void setP60215(String p60215) {
        this.p60215 = p60215;
    }

    @XmlElement(name = "P602_16")
    public String getP60216() {
        return p60216;
    }

    public void setP60216(String p60216) {
        this.p60216 = p60216;
    }

    @XmlElement(name = "P602_21")
    public String getP60221() {
        return p60221;
    }

    public void setP60221(String p60221) {
        this.p60221 = p60221;
    }

    @XmlElement(name = "P602_22")
    public String getP60222() {
        return p60222;
    }

    public void setP60222(String p60222) {
        this.p60222 = p60222;
    }

    @XmlElement(name = "P602_23")
    public String getP60223() {
        return p60223;
    }

    public void setP60223(String p60223) {
        this.p60223 = p60223;
    }

    @XmlElement(name = "P602_24")
    public String getP60224() {
        return p60224;
    }

    public void setP60224(String p60224) {
        this.p60224 = p60224;
    }

    @XmlElement(name = "P602_25")
    public String getP60225() {
        return p60225;
    }

    public void setP60225(String p60225) {
        this.p60225 = p60225;
    }

    @XmlElement(name = "P602_26")
    public String getP60226() {
        return p60226;
    }

    public void setP60226(String p60226) {
        this.p60226 = p60226;
    }

    @XmlElement(name = "P603_1D")
    public Integer getP6031d() {
        return p6031d;
    }

    public void setP6031d(Integer p6031d) {
        this.p6031d = p6031d;
    }

    @XmlElement(name = "P603_1H")
    public Integer getP6031h() {
        return p6031h;
    }

    public void setP6031h(Integer p6031h) {
        this.p6031h = p6031h;
    }

    @XmlElement(name = "P603_1M")
    public Integer getP6031m() {
        return p6031m;
    }

    public void setP6031m(Integer p6031m) {
        this.p6031m = p6031m;
    }

    @XmlElement(name = "P603_2D")
    public Integer getP6032d() {
        return p6032d;
    }

    public void setP6032d(Integer p6032d) {
        this.p6032d = p6032d;
    }

    @XmlElement(name = "P603_2H")
    public Integer getP6032h() {
        return p6032h;
    }

    public void setP6032h(Integer p6032h) {
        this.p6032h = p6032h;
    }

    @XmlElement(name = "P603_2M")
    public Integer getP6032m() {
        return p6032m;
    }

    public void setP6032m(Integer p6032m) {
        this.p6032m = p6032m;
    }

    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "APE_DIR")
    public String getApeDir() {
        return apeDir;
    }

    public void setApeDir(String apeDir) {
        this.apeDir = apeDir;
    }

    @XmlElement(name = "NOM_DIR")
    public String getNomDir() {
        return nomDir;
    }

    public void setNomDir(String nomDir) {
        this.nomDir = nomDir;
    }

    @XmlElement(name = "SITUA_DIR")
    public String getSituaDir() {
        return situaDir;
    }

    public void setSituaDir(String situaDir) {
        this.situaDir = situaDir;
    }

    @XmlElement(name = "DNI_DIR")
    public String getDniDir() {
        return dniDir;
    }

    public void setDniDir(String dniDir) {
        this.dniDir = dniDir;
    }

    @XmlElement(name = "EMAIL_DIR")
    public String getEmailDir() {
        return emailDir;
    }

    public void setEmailDir(String emailDir) {
        this.emailDir = emailDir;
    }

    @XmlElement(name = "REC_CEDD")
    public String getRecCedd() {
        return recCedd;
    }

    public void setRecCedd(String recCedd) {
        this.recCedd = recCedd;
    }

    @XmlElement(name = "REC_CEDM")
    public String getRecCedm() {
        return recCedm;
    }

    public void setRecCedm(String recCedm) {
        this.recCedm = recCedm;
    }

    @XmlElement(name = "CUL_CEDD")
    public String getCulCedd() {
        return culCedd;
    }

    public void setCulCedd(String culCedd) {
        this.culCedd = culCedd;
    }

    @XmlElement(name = "CUL_CEDM")
    public String getCulCedm() {
        return culCedm;
    }

    public void setCulCedm(String culCedm) {
        this.culCedm = culCedm;
    }

    @XmlElement(name = "DPTO")
    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }

    @XmlElement(name = "PROV")
    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    @XmlElement(name = "P202_6")
    public String getP2026() {
        return p2026;
    }

    public void setP2026(String p2026) {
        this.p2026 = p2026;
    }

    @XmlElement(name = "P202_7")
    public String getP2027() {
        return p2027;
    }
    
    public void setP2027(String p2027) {
        this.p2027 = p2027;
    }

    @XmlElement(name = "P202_8")
    public String getP2028() {
        return p2028;
    }

    public void setP2028(String p2028) {
        this.p2028 = p2028;
    }

    @XmlElement(name = "P202_9")
    public String getP2029() {
        return p2029;
    }

    public void setP2029(String p2029) {
        this.p2029 = p2029;
    }

    @XmlElement(name = "P202_10")
    public String getP20210() {
        return p20210;
    }

    public void setP20210(String p20210) {
        this.p20210 = p20210;
    }

    @XmlElement(name = "P221_11")
    public Integer getP22111() {
        return p22111;
    }

    public void setP22111(Integer p22111) {
        this.p22111 = p22111;
    }

    @XmlElement(name = "P221_12")
    public Integer getP22112() {
        return p22112;
    }

    public void setP22112(Integer p22112) {
        this.p22112 = p22112;
    }

    @XmlElement(name = "P221_13")
    public Integer getP22113() {
        return p22113;
    }

    public void setP22113(Integer p22113) {
        this.p22113 = p22113;
    }

    @XmlElement(name = "P221_14")
    public Integer getP22114() {
        return p22114;
    }

    public void setP22114(Integer p22114) {
        this.p22114 = p22114;
    }

    @XmlElement(name = "P221_21")
    public Integer getP22121() {
        return p22121;
    }

    public void setP22121(Integer p22121) {
        this.p22121 = p22121;
    }

    @XmlElement(name = "P221_22")
    public Integer getP22122() {
        return p22122;
    }

    public void setP22122(Integer p22122) {
        this.p22122 = p22122;
    }

    @XmlElement(name = "P221_23")
    public Integer getP22123() {
        return p22123;
    }

    public void setP22123(Integer p22123) {
        this.p22123 = p22123;
    }

    @XmlElement(name = "P221_24")
    public Integer getP22124() {
        return p22124;
    }

    public void setP22124(Integer p22124) {
        this.p22124 = p22124;
    }

    @XmlElement(name = "P221_31")
    public Integer getP22131() {
        return p22131;
    }

    public void setP22131(Integer p22131) {
        this.p22131 = p22131;
    }

    @XmlElement(name = "P221_32")
    public Integer getP22132() {
        return p22132;
    }

    public void setP22132(Integer p22132) {
        this.p22132 = p22132;
    }

    @XmlElement(name = "P221_33")
    public Integer getP22133() {
        return p22133;
    }

    public void setP22133(Integer p22133) {
        this.p22133 = p22133;
    }

    @XmlElement(name = "P221_34")
    public Integer getP22134() {
        return p22134;
    }

    public void setP22134(Integer p22134) {
        this.p22134 = p22134;
    }

    @XmlElement(name = "P303_11")
    public Integer getP30311() {
        return p30311;
    }

    public void setP30311(Integer p30311) {
        this.p30311 = p30311;
    }

    @XmlElement(name = "P303_12")
    public Integer getP30312() {
        return p30312;
    }

    public void setP30312(Integer p30312) {
        this.p30312 = p30312;
    }

    @XmlElement(name = "P303_13")
    public Integer getP30313() {
        return p30313;
    }

    public void setP30313(Integer p30313) {
        this.p30313 = p30313;
    }

    @XmlElement(name = "P303_21")
    public Integer getP30321() {
        return p30321;
    }

    public void setP30321(Integer p30321) {
        this.p30321 = p30321;
    }

    @XmlElement(name = "P303_22")
    public Integer getP30322() {
        return p30322;
    }

    public void setP30322(Integer p30322) {
        this.p30322 = p30322;
    }

    @XmlElement(name = "P303_23")
    public Integer getP30323() {
        return p30323;
    }

    public void setP30323(Integer p30323) {
        this.p30323 = p30323;
    }

    @XmlElement(name = "P303_31")
    public Integer getP30331() {
        return p30331;
    }

    public void setP30331(Integer p30331) {
        this.p30331 = p30331;
    }

    @XmlElement(name = "P303_32")
    public Integer getP30332() {
        return p30332;
    }

    public void setP30332(Integer p30332) {
        this.p30332 = p30332;
    }

    @XmlElement(name = "P303_33")
    public Integer getP30333() {
        return p30333;
    }

    public void setP30333(Integer p30333) {
        this.p30333 = p30333;
    }

    @XmlElement(name = "P7011")
    public String getP7011() {
        return p7011;
    }

    public void setP7011(String p7011) {
        this.p7011 = p7011;
    }

    @XmlElement(name = "P7012")
    public String getP7012() {
        return p7012;
    }

    public void setP7012(String p7012) {
        this.p7012 = p7012;
    }

    @XmlElement(name = "P7014")
    public String getP7014() {
        return p7014;
    }

    public void setP7014(String p7014) {
        this.p7014 = p7014;
    }

    @XmlElement(name = "P7015")
    public String getP7015() {
        return p7015;
    }

    public void setP7015(String p7015) {
        this.p7015 = p7015;
    }

    @XmlElement(name = "P7016_1")
    public String getP70161() {
        return p70161;
    }

    public void setP70161(String p70161) {
        this.p70161 = p70161;
    }

    @XmlElement(name = "P7016_2")
    public String getP70162() {
        return p70162;
    }

    public void setP70162(String p70162) {
        this.p70162 = p70162;
    }

    @XmlElement(name = "P702")
    public String getP702() {
        return p702;
    }

    public void setP702(String p702) {
        this.p702 = p702;
    }

    @XmlElement(name = "P702_3")
    public String getP7023() {
        return p7023;
    }

    public void setP7023(String p7023) {
        this.p7023 = p7023;
    }

    @XmlElement(name = "P702_6")
    public String getP7026() {
        return p7026;
    }

    public void setP7026(String p7026) {
        this.p7026 = p7026;
    }

    @XmlElement(name = "P702_7")
    public String getP7027() {
        return p7027;
    }

    public void setP7027(String p7027) {
        this.p7027 = p7027;
    }

    @XmlElement(name = "P702_8")
    public String getP7028() {
        return p7028;
    }

    public void setP7028(String p7028) {
        this.p7028 = p7028;
    }

    @XmlElement(name = "P702_9")
    public String getP7029() {
        return p7029;
    }

    public void setP7029(String p7029) {
        this.p7029 = p7029;
    }

    @XmlElement(name = "P702_10")
    public String getP70210() {
        return p70210;
    }

    public void setP70210(String p70210) {
        this.p70210 = p70210;
    }

    @XmlElement(name = "P702_11")
    public String getP70211() {
        return p70211;
    }

    public void setP70211(String p70211) {
        this.p70211 = p70211;
    }

    @XmlElement(name = "P702_12")
    public String getP70212() {
        return p70212;
    }

    public void setP70212(String p70212) {
        this.p70212 = p70212;
    }

    @XmlElement(name = "P702_13")
    public String getP70213() {
        return p70213;
    }

    public void setP70213(String p70213) {
        this.p70213 = p70213;
    }

    @XmlElement(name = "P702_14")
    public String getP70214() {
        return p70214;
    }

    public void setP70214(String p70214) {
        this.p70214 = p70214;
    }

    @XmlElement(name = "P702_15")
    public String getP70215() {
        return p70215;
    }

    public void setP70215(String p70215) {
        this.p70215 = p70215;
    }

    @XmlElement(name = "P702_16")
    public String getP70216() {
        return p70216;
    }

    public void setP70216(String p70216) {
        this.p70216 = p70216;
    }

    @XmlElement(name = "P702_17")
    public String getP70217() {
        return p70217;
    }

    public void setP70217(String p70217) {
        this.p70217 = p70217;
    }

    @XmlElement(name = "P702_18")
    public String getP70218() {
        return p70218;
    }

    public void setP70218(String p70218) {
        this.p70218 = p70218;
    }

    @XmlElement(name = "P702_18E")
    public String getP70218e() {
        return p70218e;
    }

    public void setP70218e(String p70218e) {
        this.p70218e = p70218e;
    }

    @XmlElement(name = "P703_ESP")
    public String getP703Esp() {
        return p703Esp;
    }

    public void setP703Esp(String p703Esp) {
        this.p703Esp = p703Esp;
    }

    @XmlElement(name = "P7041")
    public String getP7041() {
        return p7041;
    }

    public void setP7041(String p7041) {
        this.p7041 = p7041;
    }

    @XmlElement(name = "P7042_11")
    public String getP704211() {
        return p704211;
    }

    public void setP704211(String p704211) {
        this.p704211 = p704211;
    }

    @XmlElement(name = "P7042_12")
    public String getP704212() {
        return p704212;
    }

    public void setP704212(String p704212) {
        this.p704212 = p704212;
    }

    @XmlElement(name = "P7042_13")
    public String getP704213() {
        return p704213;
    }

    public void setP704213(String p704213) {
        this.p704213 = p704213;
    }

    @XmlElement(name = "P7042_14")
    public String getP704214() {
        return p704214;
    }

    public void setP704214(String p704214) {
        this.p704214 = p704214;
    }

    @XmlElement(name = "P7042_21")
    public String getP704221() {
        return p704221;
    }

    public void setP704221(String p704221) {
        this.p704221 = p704221;
    }

    @XmlElement(name = "P7042_22")
    public String getP704222() {
        return p704222;
    }

    public void setP704222(String p704222) {
        this.p704222 = p704222;
    }

    @XmlElement(name = "P7042_23")
    public String getP704223() {
        return p704223;
    }

    public void setP704223(String p704223) {
        this.p704223 = p704223;
    }

    @XmlElement(name = "P7042_24")
    public String getP704224() {
        return p704224;
    }

    public void setP704224(String p704224) {
        this.p704224 = p704224;
    }

    @XmlElement(name = "P7042_31")
    public String getP704231() {
        return p704231;
    }

    public void setP704231(String p704231) {
        this.p704231 = p704231;
    }

    @XmlElement(name = "P7042_32")
    public String getP704232() {
        return p704232;
    }

    public void setP704232(String p704232) {
        this.p704232 = p704232;
    }

    @XmlElement(name = "P7042_33")
    public String getP704233() {
        return p704233;
    }

    public void setP704233(String p704233) {
        this.p704233 = p704233;
    }

    @XmlElement(name = "P7042_34")
    public String getP704234() {
        return p704234;
    }

    public void setP704234(String p704234) {
        this.p704234 = p704234;
    }

    @XmlElement(name = "P7042_41")
    public String getP704241() {
        return p704241;
    }

    public void setP704241(String p704241) {
        this.p704241 = p704241;
    }

    @XmlElement(name = "P7042_42")
    public String getP704242() {
        return p704242;
    }

    public void setP704242(String p704242) {
        this.p704242 = p704242;
    }

    @XmlElement(name = "P7042_43")
    public String getP704243() {
        return p704243;
    }

    public void setP704243(String p704243) {
        this.p704243 = p704243;
    }

    @XmlElement(name = "P7042_44")
    public String getP704244() {
        return p704244;
    }

    public void setP704244(String p704244) {
        this.p704244 = p704244;
    }

    @XmlElement(name = "P7042_51")
    public String getP704251() {
        return p704251;
    }

    public void setP704251(String p704251) {
        this.p704251 = p704251;
    }

    @XmlElement(name = "P7042_52")
    public String getP704252() {
        return p704252;
    }

    public void setP704252(String p704252) {
        this.p704252 = p704252;
    }

    @XmlElement(name = "P7042_53")
    public String getP704253() {
        return p704253;
    }

    public void setP704253(String p704253) {
        this.p704253 = p704253;
    }

    @XmlElement(name = "P7042_54")
    public String getP704254() {
        return p704254;
    }

    public void setP704254(String p704254) {
        this.p704254 = p704254;
    }

    @XmlElement(name = "P7052")
    public String getP7052() {
        return p7052;
    }

    public void setP7052(String p7052) {
        this.p7052 = p7052;
    }

    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }
    
    @XmlElement(name = "P7013_1")
    public String getP70131() {
        return p70131;
    }

    public void setP70131(String p70131) {
        this.p70131 = p70131;
    }

    @XmlElement(name = "P7013_2")
    public String getP70132() {
        return p70132;
    }

    public void setP70132(String p70132) {
        this.p70132 = p70132;
    }

    @XmlElement(name = "P702_4")
    public String getP7024() {
        return p7024;
    }

    public void setP7024(String p7024) {
        this.p7024 = p7024;
    }

    @XmlElement(name = "P702_5")
    public String getP7025() {
        return p7025;
    }

    public void setP7025(String p7025) {
        this.p7025 = p7025;
    }    

    @XmlElement(name = "P703")
    public String getP703() {
        return p703;
    }

    public void setP703(String p703) {
        this.p703 = p703;
    }

    @XmlElement(name = "FONO_DIR")
    public String getFonoDir() {
        return fonoDir;
    }

    public void setFonoDir(String fonoDir) {
        this.fonoDir = fonoDir;
    }

    @XmlElement(name = "P702_1")
    public String getP7021() {
        return p7021;
    }

    public void setP7021(String p7021) {
        this.p7021 = p7021;
    }

    @XmlElement(name = "P702_2")
    public String getP7022() {
        return p7022;
    }

    public void setP7022(String p7022) {
        this.p7022 = p7022;
    }

    @XmlElement(name = "P7051")
    public String getP7051() {
        return p7051;
    }

    public void setP7051(String p7051) {
        this.p7051 = p7051;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2014Cabecera)) {
            return false;
        }
        Local2014Cabecera other = (Local2014Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
/*
    @XmlElement(name = "SEC104")
    public List<Local2014Sec104> getSec104() {
        return sec104;
    }

    public void setSec104(List<Local2014Sec104> sec104) {
        this.sec104 = sec104;
    }

    @XmlElement(name = "SEC500")
    public List<Local2014Sec500> getSec500() {
        return sec500;
    }

    public void setSec500(List<Local2014Sec500> sec500) {
        this.sec500 = sec500;
    }

    @XmlElement(name = "SEC300")
    public List<Local2014Sec300> getSec300() {
        return sec300;
    }

    public void setSec300(List<Local2014Sec300> sec300) {
        this.sec300 = sec300;
    }
    
    @XmlElement(name = "SEC302")
    public List<Local2014Sec302> getSec302() {
        return sec302;
    }

    public void setSec302(List<Local2014Sec302> sec302) {
        this.sec302 = sec302;
    }

    @XmlElement(name = "SEC400")
    public List<Local2014Sec400> getSec400() {
        return sec400;
    }

    public void setSec400(List<Local2014Sec400> sec400) {
        this.sec400 = sec400;
    }*/

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlAttribute(name = "MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute(name = "ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean getUltimo() {
        return ultimo;
    }

    /**
     * @param ultimo the ultimo to set
     */
    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    public Date getFechaenvio() {
        return fechaenvio;
    }

    public void setFechaenvio(Date fechaenvio) {
        this.fechaenvio = fechaenvio;
    }
}
