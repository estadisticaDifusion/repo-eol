/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "local2014_sec500")
public class Local2014Sec500 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "P501_1")
    private String p5011;
    @Column(name = "P501_2")
    private String p5012;
    @Column(name = "P501_3")
    private String p5013;
    @Column(name = "P501_4")
    private String p5014;
    @Column(name = "P501_5")
    private String p5015;
    @Column(name = "P501_6")
    private String p5016;
    @Column(name = "P501_7")
    private String p5017;
    @Column(name = "P501_8")
    private String p5018;
     @Column(name = "P501_9")
    private String p5019;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2014Cabecera local2014Cabecera;

    public Local2014Sec500() {
    }

    public Local2014Sec500(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name="P501_1")
    public String getP5011() {
        return p5011;
    }

    public void setP5011(String p5011) {
        this.p5011 = p5011;
    }

    @XmlTransient
    public Local2014Cabecera getLocal2014Cabecera() {
        return local2014Cabecera;
    }

    public void setLocal2014Cabecera(Local2014Cabecera local2014Cabecera) {
        this.local2014Cabecera = local2014Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2014Sec500)) {
            return false;
        }
        Local2014Sec500 other = (Local2014Sec500) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }


    @XmlElement(name="P501_2")
    public String getP5012() {
        return p5012;
    }

    public void setP5012(String p5012) {
        this.p5012 = p5012;
    }

    @XmlElement(name="P501_3")
    public String getP5013() {
        return p5013;
    }

    public void setP5013(String p5013) {
        this.p5013 = p5013;
    }

    @XmlElement(name="P501_4")
    public String getP5014() {
        return p5014;
    }

    public void setP5014(String p5014) {
        this.p5014 = p5014;
    }

    @XmlElement(name="P501_5")
    public String getP5015() {
        return p5015;
    }

    public void setP5015(String p5015) {
        this.p5015 = p5015;
    }

    @XmlElement(name="P501_6")
    public String getP5016() {
        return p5016;
    }

    public void setP5016(String p5016) {
        this.p5016 = p5016;
    }

    @XmlElement(name="P501_7")
    public String getP5017() {
        return p5017;
    }

    public void setP5017(String p5017) {
        this.p5017 = p5017;
    }

    @XmlElement(name="P501_8")
    public String getP5018() {
        return p5018;
    }

    public void setP5018(String p5018) {
        this.p5018 = p5018;
    }
    
    @XmlElement(name="P501_9")
    public String getP5019() {
        return p5019;
    }

    public void setP5019(String p5019) {
        this.p5019 = p5019;
    }

}
