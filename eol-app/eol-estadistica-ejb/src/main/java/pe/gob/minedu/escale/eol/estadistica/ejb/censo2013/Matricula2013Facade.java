/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb.censo2013;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2013.Matricula2013Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author DSILVA
 */
@Singleton
public class Matricula2013Facade extends AbstractFacade<Matricula2013Cabecera> {

	public static final String CEDULA_MATRICULA = "MATRICULA";
	static final Logger LOGGER = Logger.getLogger(Matricula2013Facade.class.getName());
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03A = "c03a";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08A = "c08a";
	public final static String CEDULA_09A = "c09a";
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Matricula2013Facade() {
		super(Matricula2013Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Matricula2013Cabecera cedula) {
		String sql = "UPDATE Matricula2013Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroCed=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroCed());
		query.executeUpdate();

		/*
		 * if (cedula.getDetalleMatricula() != null) { Set<String> keys =
		 * cedula.getDetalleMatricula().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2013Matricula detalle = cedula.getDetalleMatricula().get(key);
		 * detalle.setMatricula2013Cabecera(cedula); List<Matricula2013MatriculaFila>
		 * filas = detalle.getMatricula2013MatriculaFilaList(); for
		 * (Matricula2013MatriculaFila fila : filas) {
		 * fila.setMatricula2013Matricula(detalle); } } }
		 * 
		 * if (cedula.getDetalleRecursos() != null) { Set<String> keys =
		 * cedula.getDetalleRecursos().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2013Recursos detalle = cedula.getDetalleRecursos().get(key);
		 * detalle.setMatricula2013Cabecera(cedula); List<Matricula2013RecursosFila>
		 * filas = detalle.getMatricula2013RecursosFilaList(); if (filas != null) for
		 * (Matricula2013RecursosFila fila : filas) {
		 * fila.setMatricula2013Recursos(detalle); } } }
		 * 
		 * if (cedula.getDetalleSeccion() != null) { Set<String> keys =
		 * cedula.getDetalleSeccion().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2013Seccion detalle = cedula.getDetalleSeccion().get(key);
		 * detalle.setMatricula2013Cabecera(cedula); List<Matricula2013SeccionFila>
		 * filas = detalle.getMatricula2013SeccionFilaList(); for
		 * (Matricula2013SeccionFila fila : filas) {
		 * fila.setMatricula2013Seccion(detalle); } } }
		 * 
		 * if (cedula.getDetalleSaanee() != null) { Set<String> keys =
		 * cedula.getDetalleSaanee().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next(); Matricula2013Saanee
		 * detalle = cedula.getDetalleSaanee().get(key);
		 * detalle.setMatricula2013Cabecera(cedula); List<Matricula2013SaaneeFila> filas
		 * = detalle.getMatricula2013SaaneeFilaList(); for (Matricula2013SaaneeFila fila
		 * : filas) { fila.setMatricula2013Saanee(detalle); } } }
		 * 
		 * if (cedula.getMatricula2013CoordpronoeiList() != null) { for
		 * (Matricula2013Coordpronoei coord : cedula.getMatricula2013CoordpronoeiList())
		 * { coord.setMatricula2013Cabecera(cedula); } } if
		 * (cedula.getMatricula2013LocalpronoeiList() != null) { for
		 * (Matricula2013Localpronoei loc_pro :
		 * cedula.getMatricula2013LocalpronoeiList()) {
		 * loc_pro.setMatricula2013Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2013PersonalList() != null) { for
		 * (Matricula2013Personal per : cedula.getMatricula2013PersonalList()) {
		 * per.setMatricula2013Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2013PerifericosList() != null) { for
		 * (Matricula2013Perifericos per : cedula.getMatricula2013PerifericosList()) {
		 * per.setMatricula2013Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2013CetproCursoList() != null) { for
		 * (Matricula2013CetproCurso cc : cedula.getMatricula2013CetproCursoList()) {
		 * cc.setMatricula2013Cabecera(cedula); } }
		 * 
		 */
		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

}
