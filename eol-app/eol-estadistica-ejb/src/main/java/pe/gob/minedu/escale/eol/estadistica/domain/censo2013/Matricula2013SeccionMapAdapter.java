/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2013.Matricula2013SeccionMapAdapter.Matricula2013SeccionList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2013SeccionMapAdapter extends XmlAdapter<Matricula2013SeccionList, Map<String, Matricula2013Seccion>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2013SeccionMapAdapter.class.getName());

    static class Matricula2013SeccionList {

        private List<Matricula2013Seccion> detalle;

        private Matricula2013SeccionList(ArrayList<Matricula2013Seccion> lista) {
            detalle = lista;
        }

        public Matricula2013SeccionList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2013Seccion> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2013Seccion> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2013Seccion> unmarshal(Matricula2013SeccionList v) throws Exception {

        Map<String, Matricula2013Seccion> map = new HashMap<String, Matricula2013Seccion>();
        for (Matricula2013Seccion detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2013SeccionList marshal(Map<String, Matricula2013Seccion> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2013Seccion> lista = new ArrayList<Matricula2013Seccion>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2013Seccion $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2013SeccionList list = new Matricula2013SeccionList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
