/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Resultado2011DetalleMapAdapter.Resultado2011DetalleList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Resultado2011DetalleMapAdapter extends XmlAdapter<Resultado2011DetalleList, Map<String, Resultado2011Detalle>> {

    private static final Logger LOGGER = Logger.getLogger(Resultado2011DetalleMapAdapter.class.getName());

    static class Resultado2011DetalleList {

        private List<Resultado2011Detalle> detalle;

        private Resultado2011DetalleList(ArrayList<Resultado2011Detalle> lista) {
            detalle = lista;
        }

        public Resultado2011DetalleList() {
        }

        @XmlElement(name = "cuadros")
        public List<Resultado2011Detalle> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Resultado2011Detalle> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Resultado2011Detalle> unmarshal(Resultado2011DetalleList v) throws Exception {

        Map<String, Resultado2011Detalle> map = new HashMap<String, Resultado2011Detalle>();
        for (Resultado2011Detalle detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Resultado2011DetalleList marshal(Map<String, Resultado2011Detalle> v) throws Exception {
        if(v==null)
        {   LOGGER.fine("no hay detalles");
            return null;            
        }
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Resultado2011Detalle> lista = new ArrayList<Resultado2011Detalle>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Resultado2011Detalle $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Resultado2011DetalleList list = new Resultado2011DetalleList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
