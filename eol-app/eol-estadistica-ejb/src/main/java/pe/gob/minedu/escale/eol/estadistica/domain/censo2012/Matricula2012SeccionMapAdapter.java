/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Matricula2012SeccionMapAdapter.Matricula2012SeccionList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2012SeccionMapAdapter extends XmlAdapter<Matricula2012SeccionList, Map<String, Matricula2012Seccion>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2012SeccionMapAdapter.class.getName());

    static class Matricula2012SeccionList {

        private List<Matricula2012Seccion> detalle;

        private Matricula2012SeccionList(ArrayList<Matricula2012Seccion> lista) {
            detalle = lista;
        }

        public Matricula2012SeccionList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2012Seccion> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2012Seccion> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2012Seccion> unmarshal(Matricula2012SeccionList v) throws Exception {

        Map<String, Matricula2012Seccion> map = new HashMap<String, Matricula2012Seccion>();
        for (Matricula2012Seccion detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2012SeccionList marshal(Map<String, Matricula2012Seccion> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2012Seccion> lista = new ArrayList<Matricula2012Seccion>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2012Seccion $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2012SeccionList list = new Matricula2012SeccionList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
