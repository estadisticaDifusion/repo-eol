package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "cedulaIE")
@Entity
@Table(name = "ie2016_cabecera")
public class Ie2016Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NROCED")
    private String nroced;
    @Basic(optional = false)
    @Column(name = "COD_IED")
    private String codIed;
    @Basic(optional = false)
    @Column(name = "TIPO_IED")
    private String tipoIed;
    @Basic(optional = false)
    @Column(name = "NOMB_IED")
    private String nombIed;
    @Basic(optional = false)
    @Column(name = "GESTION")
    private String gestion;
    @Basic(optional = false)
    @Column(name = "CODUGEL")
    private String codugel;
    @Basic(optional = false)
    @Column(name = "DIRECCION")
    private String direccion;
    @Column(name = "PAGINAWEB")
    private String paginaweb;
    @Column(name = "CORREO")
    private String correo;
    @Column(name = "TCORREO")
    private String tcorreo;
    @Column(name = "TELEFONO")
    private String telefono;
    @Column(name = "TTELEFONO")
    private String ttelefono;
    @Column(name = "APATERNO")
    private String apaterno;
    @Column(name = "AMATERNO")
    private String amaterno;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "NRODOCU")
    private String nrodocu;
    @Column(name = "TIPODOCU")
    private String tipodocu;
    @Column(name = "RZSOCIAL")
    private String rzsocial;
    @Column(name = "NRORUC")
    private String nroruc;
    @Column(name = "NRODOC")
    private String nrodoc;
    @Column(name = "TIPODOC")
    private String tipodoc;
    @Column(name = "COMENTA")
    private String comenta;
    @Column(name = "VARCTRL")
    private String varctrl;
    @Column(name = "VARSTAT")
    private String varstat;
    @Column(name = "FUENTE")
    private String fuente;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ie2016Cabecera", fetch = FetchType.EAGER)
    private List<Ie2016Establecimientos> ie2016EstablecimientosList;
    */
    @Transient
    private long token;
    @Transient
    private String msg;
    @Transient
    private String estadoRpt;

    public Ie2016Cabecera() {
    }

    public Ie2016Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Ie2016Cabecera(Long idEnvio, String nroced, String codIed, String tipoIed, String nombIed, String gestion, String codugel, String direccion) {
        this.idEnvio = idEnvio;
        this.nroced = nroced;
        this.codIed = codIed;
        this.tipoIed = tipoIed;
        this.nombIed = nombIed;
        this.gestion = gestion;
        this.codugel = codugel;
        this.direccion = direccion;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "COD_IED")
    public String getCodIed() {
        return codIed;
    }

    public void setCodIed(String codIed) {
        this.codIed = codIed;
    }

    @XmlElement(name = "TIPO_IED")
    public String getTipoIed() {
        return tipoIed;
    }

    public void setTipoIed(String tipoIed) {
        this.tipoIed = tipoIed;
    }

    @XmlElement(name = "NOMB_IED")
    public String getNombIed() {
        return nombIed;
    }

    public void setNombIed(String nombIed) {
        this.nombIed = nombIed;
    }

    @XmlElement(name = "GESTION")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "DIRECCION")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @XmlElement(name = "PAGINAWEB")
    public String getPaginaweb() {
        return paginaweb;
    }

    public void setPaginaweb(String paginaweb) {
        this.paginaweb = paginaweb;
    }

    @XmlElement(name = "CORREO")
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @XmlElement(name = "TCORREO")
    public String getTcorreo() {
        return tcorreo;
    }

    public void setTcorreo(String tcorreo) {
        this.tcorreo = tcorreo;
    }

    @XmlElement(name = "TELEFONO")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @XmlElement(name = "TTELEFONO")
    public String getTtelefono() {
        return ttelefono;
    }

    public void setTtelefono(String ttelefono) {
        this.ttelefono = ttelefono;
    }

    @XmlElement(name = "APATERNO")
    public String getApaterno() {
        return apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    @XmlElement(name = "AMATERNO")
    public String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }

    @XmlElement(name = "NOMBRES")
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @XmlElement(name = "NRODOCU")
    public String getNrodocu() {
        return nrodocu;
    }

    public void setNrodocu(String nrodocu) {
        this.nrodocu = nrodocu;
    }

    @XmlElement(name = "TIPODOCU")
    public String getTipodocu() {
        return tipodocu;
    }

    public void setTipodocu(String tipodocu) {
        this.tipodocu = tipodocu;
    }

    @XmlElement(name = "RZSOCIAL")
    public String getRzsocial() {
        return rzsocial;
    }

    public void setRzsocial(String rzsocial) {
        this.rzsocial = rzsocial;
    }

    @XmlElement(name = "NRORUC")
    public String getNroruc() {
        return nroruc;
    }

    public void setNroruc(String nroruc) {
        this.nroruc = nroruc;
    }

    @XmlElement(name = "NRODOC")
    public String getNrodoc() {
        return nrodoc;
    }

    public void setNrodoc(String nrodoc) {
        this.nrodoc = nrodoc;
    }

    @XmlElement(name = "TIPODOC")
    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

	@XmlElement(name = "COMENTA")
    public String getComenta() {
        return comenta;
    }

    public void setComenta(String comenta) {
        this.comenta = comenta;
    }
	@XmlElement(name = "VARCTRL")
    public String getVarctrl() {
        return varctrl;
    }

    public void setVarctrl(String varctrl) {
        this.varctrl = varctrl;
    }
	@XmlElement(name = "VARSTAT")
    public String getVarstat() {
        return varstat;
    }

    public void setVarstat(String varstat) {
        this.varstat = varstat;
    }

    @XmlElement(name = "FUENTE")
    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }
/*
    @XmlElement(name = "LOCALES")
    public List<Ie2016Establecimientos> getIe2016EstablecimientosList() {
        return ie2016EstablecimientosList;
    }

    public void setIe2016EstablecimientosList(List<Ie2016Establecimientos> ie2016EstablecimientosList) {
        this.ie2016EstablecimientosList = ie2016EstablecimientosList;
    }
*/
    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ie2016Cabecera)) {
            return false;
        }
        Ie2016Cabecera other = (Ie2016Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Ie2016Cabecera[idEnvio=" + idEnvio + "]";
    }

}
