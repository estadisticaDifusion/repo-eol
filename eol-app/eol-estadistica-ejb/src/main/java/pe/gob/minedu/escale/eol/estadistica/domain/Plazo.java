/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author DSILVA
 */
@Entity
@Table(name = "est_plazos")
@NamedQueries({@NamedQuery(name = "Plazo.findAll", query = "SELECT p FROM Plazo p"),
/*    @NamedQuery(name = "Plazo.findByPeriodo", query = "SELECT p FROM Plazo p WHERE p.id.periodo = :periodo"),
    @NamedQuery(name = "Plazo.findByPeriodoProceso", query = "SELECT p FROM Plazo p WHERE p.id.periodo = :periodo AND p.plazoPK.proceso = :proceso"),
    @NamedQuery(name = "Plazo.findByProceso", query = "SELECT p FROM Plazo p WHERE p.plazoPK.proceso = :proceso"),*/
    @NamedQuery(name = "Plazo.findByFechaInicio", query = "SELECT p FROM Plazo p WHERE p.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Plazo.findByFechaTermino", query = "SELECT p FROM Plazo p WHERE p.fechaTermino = :fechaTermino")})
public class Plazo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private PlazoPK id;
    @Basic(optional = false)
    @Column(name = "fecha_inicio", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @Column(name = "fecha_termino", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaTermino;

    public Plazo() {
    }

    public Plazo(PlazoPK plazoPK) {
        this.id = plazoPK;
    }

    public Plazo(PlazoPK plazoPK, Date fechaInicio, Date fechaTermino) {
        this.id = plazoPK;
        this.fechaInicio = fechaInicio;
        this.fechaTermino = fechaTermino;
    }

    public Plazo(String periodo, String proceso) {
        this.id = new PlazoPK(periodo, proceso);
    }


    public PlazoPK getId() {
		return id;
	}

	public void setId(PlazoPK id) {
		this.id = id;
	}

	public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
		result = prime * result + ((fechaTermino == null) ? 0 : fechaTermino.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plazo other = (Plazo) obj;
		if (fechaInicio == null) {
			if (other.fechaInicio != null)
				return false;
		} else if (!fechaInicio.equals(other.fechaInicio))
			return false;
		if (fechaTermino == null) {
			if (other.fechaTermino != null)
				return false;
		} else if (!fechaTermino.equals(other.fechaTermino))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Plazo [id=" + id + ", fechaInicio=" + fechaInicio + ", fechaTermino=" + fechaTermino + "]";
	}


}
