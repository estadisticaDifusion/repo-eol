/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@XmlRootElement(name = "cedulaResultado2015")
@Entity
@Table(name = "resultado2015_cabecera")
public class Resultado2015Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    public final static String CEDULA_01B = "c01b";
    public final static String CEDULA_02B = "c02b";
    public final static String CEDULA_03B = "c03b";
    public final static String CEDULA_04B = "c04b";
    public final static String CEDULA_05B = "c05b";
    public final static String CEDULA_06B = "c06b";
    public final static String CEDULA_07B = "c07b";
    public final static String CEDULA_08B = "c08b";
    public final static String CEDULA_09B = "c09b";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Transient
    private long token;
    @Basic(optional = false)
    @Column(name = "NROCED")
    private String nroced;
    @Basic(optional = false)
    @Column(name = "COD_MOD")
    private String codMod;
    @Basic(optional = false)
    @Column(name = "ANEXO")
    private String anexo;
    @Column(name = "TIPOREG")
    private String tiporeg;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPPROG")
    private String tipprog;
    @Column(name = "CEN_EDU")
    private String cenEdu;
    @Column(name = "NIV_MOD")
    private String nivMod;
    @Column(name = "CODOOII")
    private String codooii;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "FORMATEN")
    private String formaten;
    @Column(name = "TIPONEE")
    private String tiponee;
    @Column(name = "DNI_CORD")
    private String dniCord;
    @Column(name = "MET_PRES")
    private String metPres;
    @Column(name = "MET_SPRE")
    private String metSpre;
    @Column(name = "MET_CPED")
    private String metCped;
    @Column(name = "MET_ADST")
    private String metAdst;
    @Column(name = "CICLO_INI")
    private String cicloIni;
    @Column(name = "CICLO_INT")
    private String cicloInt;
    @Column(name = "CICLO_AVZ")
    private String cicloAvz;
    @Column(name = "APE_DIR")
    private String apeDir;
    @Column(name = "NOM_DIR")
    private String nomDir;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Basic(optional = false)
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "SITUACION")
    private String situacion;
    @Column(name = "VALIDO")
    private Boolean valido;
    @Column(name = "CONFIRMADO")
    private String confirmado;
/*
    @MapKeyColumn(name = "CUADRO", length = 5)
    @OneToMany(mappedBy = "cedula", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String,Resultado2015Detalle> detalle;*/

    @Transient
    private String msg;

    @Transient
    private String estadoRpt;

    public Resultado2015Cabecera() {
    }

    public Resultado2015Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Resultado2015Cabecera(Long idEnvio, String nroced, String codMod, String anexo, Date fechaEnvio) {
        this.idEnvio = idEnvio;
        this.nroced = nroced;
        this.codMod = codMod;
        this.anexo = anexo;
        this.fechaEnvio = fechaEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name = "ANEXO")
    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    @XmlElement(name = "TIPOREG")
    public String getTiporeg() {
        return tiporeg;
    }

    public void setTiporeg(String tiporeg) {
        this.tiporeg = tiporeg;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPPROG")
    public String getTipprog() {
        return tipprog;
    }

    public void setTipprog(String tipprog) {
        this.tipprog = tipprog;
    }

    @XmlElement(name = "CEN_EDU")
    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    @XmlElement(name = "NIV_MOD")
    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    @XmlElement(name = "CODOOII")
    public String getCodooii() {
        return codooii;
    }

    public void setCodooii(String codooii) {
        this.codooii = codooii;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }
    
    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "FORMATEN")
    public String getFormaten() {
        return formaten;
    }

    public void setFormaten(String formaten) {
        this.formaten = formaten;
    }

    @XmlElement(name = "TIPONEE")
    public String getTiponee() {
        return tiponee;
    }

    public void setTiponee(String tiponee) {
        this.tiponee = tiponee;
    }

    @XmlElement(name = "DNI_CORD")
    public String getDniCord() {
        return dniCord;
    }

    public void setDniCord(String dniCord) {
        this.dniCord = dniCord;
    }

    @XmlElement(name = "MET_PRES")
    public String getMetPres() {
        return metPres;
    }

    public void setMetPres(String metPres) {
        this.metPres = metPres;
    }

    @XmlElement(name = "MET_SPRE")
    public String getMetSpre() {
        return metSpre;
    }

    public void setMetSpre(String metSpre) {
        this.metSpre = metSpre;
    }

    @XmlElement(name = "MET_CPED")
    public String getMetCped() {
        return metCped;
    }

    public void setMetCped(String metCped) {
        this.metCped = metCped;
    }


    @XmlElement(name = "MET_ADST")
    public String getMetAdst() {
        return metAdst;
    }

    public void setMetAdst(String metAdst) {
        this.metAdst = metAdst;
    }

    @XmlElement(name = "CICLO_INI")
    public String getCicloIni() {
        return cicloIni;
    }

    public void setCicloIni(String cicloIni) {
        this.cicloIni = cicloIni;
    }

    @XmlElement(name = "CICLO_INT")
    public String getCicloInt() {
        return cicloInt;
    }

    public void setCicloInt(String cicloInt) {
        this.cicloInt = cicloInt;
    }

    @XmlElement(name = "CICLO_AVZ")
    public String getCicloAvz() {
        return cicloAvz;
    }

    public void setCicloAvz(String cicloAvz) {
        this.cicloAvz = cicloAvz;
    }

    @XmlElement(name = "APE_DIR")
    public String getApeDir() {
        return apeDir;
    }

    public void setApeDir(String apeDir) {
        this.apeDir = apeDir;
    }

    @XmlElement(name = "NOM_DIR")
    public String getNomDir() {
        return nomDir;
    }

    public void setNomDir(String nomDir) {
        this.nomDir = nomDir;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    @XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @XmlTransient
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    @XmlElement(name = "SITUACION")
    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }


    @XmlElement(name = "VALIDO")
    public Boolean getValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }

    @XmlElement(name = "CONFIRMADO")
    public String getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(String confirmado) {
        this.confirmado = confirmado;
    }
/*
    @XmlJavaTypeAdapter(Resultado2015DetalleMapAdapter.class)
    @XmlElement(name = "DETALLE")
    public Map<String, Resultado2015Detalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(Map<String, Resultado2015Detalle> detalle) {
        this.detalle = detalle;
    }*/

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado2015Cabecera)) {
            return false;
        }
        Resultado2015Cabecera other = (Resultado2015Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2015.Resultado2015Cabecera[idEnvio=" + idEnvio + "]";
    }

}
