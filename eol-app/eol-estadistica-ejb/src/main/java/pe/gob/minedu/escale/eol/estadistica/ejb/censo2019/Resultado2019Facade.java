package pe.gob.minedu.escale.eol.estadistica.ejb.censo2019;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jfree.util.Log;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2019.Resultado2019Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;
/**
 * 
 * @author WARODRIGUEZ
 * 
 */

@Singleton
public class Resultado2019Facade extends AbstractFacade<Resultado2019Cabecera> implements Resultado2019Local{

	public static final String CEDULA_RESULTADO = "CEDULA-RESULTADO";
	static final Logger LOGGER = Logger.getLogger(Resultado2019Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Resultado2019Facade() {
		super(Resultado2019Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Resultado2019Cabecera cedula) {
		String sql = "UPDATE Resultado2019Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo and a.nroced=:nroced ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroced", cedula.getNroced());
		query.executeUpdate();

		/*
		 * if (cedula.getResultado2018RespList() != null) { for (Resultado2018Resp resp
		 * : cedula.getResultado2018RespList()) { resp.setResultado2018Cabecera(cedula);
		 * } }
		 */

		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

	public void updatedetail(Resultado2019Cabecera ceddiciembre, Resultado2019Cabecera cedfebrero) {
		/*
		 * Set<String> keys = cedfebrero.getDetalle().keySet(); for (Iterator<String> it
		 * = keys.iterator(); it.hasNext();) { String key = it.next();
		 * Resultado2018Detalle det = cedfebrero.getDetalle().get(key);
		 * det.setCedula(ceddiciembre);
		 * 
		 * 
		 * em.persist(det); em.flush();
		 * 
		 * 
		 * Query q = null;
		 * 
		 * try { String query = "CALL actualizar_datos_resultado_2018(?,?,?,?,?)"; q =
		 * (Query) em.createNativeQuery(query);
		 * 
		 * q.setParameter("1", ceddiciembre.getIdEnvio()); q.setParameter("2", key);
		 * q.setParameter("3", ceddiciembre.getCodMod()); q.setParameter("4",
		 * ceddiciembre.getAnexo()); q.setParameter("5", ceddiciembre.getNivMod());
		 * 
		 * List<Object[]> listObj = q.getResultList();
		 * 
		 * } catch (Exception e) { e.printStackTrace(); }
		 * 
		 * }
		 */
	}

	public Resultado2019Cabecera findByCodModAnNiv(String codmod, String anexo, String nivmod) {

		Resultado2019Cabecera rs=null;
		Query q = em.createQuery(
				"SELECT a FROM Resultado2019Cabecera a WHERE a.ultimo=true AND a.codMod=:codMod AND a.anexo=:anexo and a.nivMod=:nivMod");
//				"SELECT R FROM Resultado2019Cabecera R WHERE R.codMod=:codmod AND R.anexo=:anexo AND R.nivMod=:nivmod AND R.ultimo=1");
			
				q.setParameter("codMod", codmod);
				q.setParameter("anexo", anexo);
				q.setParameter("nivMod", nivmod);
		try {
		
			rs= (Resultado2019Cabecera) q.getSingleResult();
		} catch (NoResultException e) {
			//LOGGER.info("ERROR RESULTADO: "+ e );
			//e.printStackTrace();
			LOGGER.info("NO RESULT EXCEPTION SE DISPARO");
			//return null;
		}
		 return rs;
	}

	public List<Object[]> verificarObservacionCedula(String codmod, String anexo, String nroced, Integer totalH,
			Integer totalM, Integer dato01h, Integer dato01m, Integer dato02h, Integer dato02m, Integer dato03h,
			Integer dato03m, Integer dato04h, Integer dato04m, Integer dato05h, Integer dato05m, Integer dato06h,
			Integer dato06m, Integer dato07h, Integer dato07m, Integer dato08h, Integer dato08m, Integer dato09h,
			Integer dato09m, Integer dato10h, Integer dato10m) {
		Query q = null;

		try {
			String query = "CALL verificar_consistencia_2019(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			q = (Query) em.createNativeQuery(query);

			q.setParameter("1", codmod);
			q.setParameter("2", anexo);
			q.setParameter("3", nroced);
			q.setParameter("4", totalH);
			q.setParameter("5", totalM);
			q.setParameter("6", dato01h);
			q.setParameter("7", dato01m);
			q.setParameter("8", dato02h);
			q.setParameter("9", dato02m);
			q.setParameter("10", dato03h);
			q.setParameter("11", dato03m);
			q.setParameter("12", dato04h);
			q.setParameter("13", dato04m);
			q.setParameter("14", dato05h);
			q.setParameter("15", dato05m);
			q.setParameter("16", dato06h);
			q.setParameter("17", dato06m);
			q.setParameter("18", dato07h);
			q.setParameter("19", dato07m);
			q.setParameter("20", dato08h);
			q.setParameter("21", dato08m);
			q.setParameter("22", dato09h);
			q.setParameter("23", dato09m);
			q.setParameter("24", dato10h);
			q.setParameter("25", dato10m);

			List<Object[]> listObj = q.getResultList();

			return listObj;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Object[]> registrarEnvioResultadoIdentificacion(String codmod, String anexo, String nivmod) {// para que
																												// se
																												// visualize
																												// en el
																												// tablero
																												// la
																												// descarga
																												// del
																												// PDF
																												// se
																												// necesitaria
																												// enviar
																												// el ID
																												// de
																												// envio
																												// de
																												// resultado
																												// cabecera
		Query q = null;

		try {
			String query = "CALL registrar_envio_resultado_2019(?,?,?)";
			q = (Query) em.createNativeQuery(query);

			q.setParameter("1", codmod);
			q.setParameter("2", anexo);
			q.setParameter("3", nivmod);

			List<Object[]> listObj = q.getResultList();

			return listObj;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
