package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Matricula2015RecursosMapAdapter.Matricula2015RecursosList;

public class Matricula2015RecursosMapAdapter  extends XmlAdapter<Matricula2015RecursosList, Map<String, Matricula2015Recursos>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2015RecursosMapAdapter.class.getName());

    static class Matricula2015RecursosList {

        private List<Matricula2015Recursos> detalle;

        private Matricula2015RecursosList(ArrayList<Matricula2015Recursos> lista) {
            detalle = lista;
        }

        public Matricula2015RecursosList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2015Recursos> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2015Recursos> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2015Recursos> unmarshal(Matricula2015RecursosList v) throws Exception {

        Map<String, Matricula2015Recursos> map = new HashMap<String, Matricula2015Recursos>();
        for (Matricula2015Recursos detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2015RecursosList marshal(Map<String, Matricula2015Recursos> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2015Recursos> lista = new ArrayList<Matricula2015Recursos>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2015Recursos $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2015RecursosList list = new Matricula2015RecursosList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
