package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "ie2016_establecimientos")
public class Ie2016Establecimientos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NRO")
    private String nro;
    @Basic(optional = false)
    @Column(name = "CUADRO")
    private String cuadro;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "NOMESTAB")
    private String nomestab;
    @Column(name = "DIRECCION")
    private String direccion;
    @Column(name = "TELEFONO")
    private String telefono;
    @Column(name = "SUMENERG")
    private String sumenerg;
    @Column(name = "PROVSUMI")
    private String provsumi;
    @Column(name = "CMOD_INI")
    private String cmodIni;
    @Column(name = "CMOD_INI1")
    private String cmodIni1;
    @Column(name = "CMOD_INI2")
    private String cmodIni2;
    @Column(name = "CMOD_PRI")
    private String cmodPri;
    @Column(name = "CMOD_PRI1")
    private String cmodPri1;
    @Column(name = "CMOD_PRI2")
    private String cmodPri2;
    @Column(name = "CMOD_PRI3")
    private String cmodPri3;
    @Column(name = "CMOD_PRI4")
    private String cmodPri4;
    @Column(name = "CMOD_PRI5")
    private String cmodPri5;
    @Column(name = "CMOD_PRI6")
    private String cmodPri6;
    @Column(name = "CMOD_SEC")
    private String cmodSec;
    @Column(name = "CMOD_SEC1")
    private String cmodSec1;
    @Column(name = "CMOD_SEC2")
    private String cmodSec2;
    @Column(name = "CMOD_SEC3")
    private String cmodSec3;
    @Column(name = "CMOD_SEC4")
    private String cmodSec4;
    @Column(name = "CMOD_SEC5")
    private String cmodSec5;
    @Column(name = "CMOD_EBAI")
    private String cmodEbai;
    @Column(name = "CMOD_EBAI1")
    private String cmodEbai1;
    @Column(name = "CMOD_EBAI2")
    private String cmodEbai2;
    @Column(name = "CMOD_EBAI3")
    private String cmodEbai3;
    @Column(name = "CMOD_EBAI4")
    private String cmodEbai4;
    @Column(name = "CMOD_EBAI5")
    private String cmodEbai5;
    @Column(name = "CMOD_EBAA")
    private String cmodEbaa;
    @Column(name = "CMOD_EBAA1")
    private String cmodEbaa1;
    @Column(name = "CMOD_EBAA2")
    private String cmodEbaa2;
    @Column(name = "CMOD_EBAA3")
    private String cmodEbaa3;
    @Column(name = "CMOD_EBAA4")
    private String cmodEbaa4;
    @Column(name = "CMOD_EBE")
    private String cmodEbe;
    @Column(name = "CMOD_EBE1")
    private String cmodEbe1;
    @Column(name = "CMOD_EBE2")
    private String cmodEbe2;
    @Column(name = "CMOD_ETP")
    private String cmodEtp;
    @Column(name = "CMOD_ETP1")
    private String cmodEtp1;
    @Column(name = "CMOD_ETP2")
    private String cmodEtp2;
    @Column(name = "CMOD_IST")
    private String cmodIst;
    @Column(name = "CMOD_ISP")
    private String cmodIsp;
    @Column(name = "CMOD_ESFA")
    private String cmodEsfa;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Ie2016Cabecera ie2016Cabecera;

    public Ie2016Establecimientos() {
    }

    public Ie2016Establecimientos(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Ie2016Establecimientos(Long idEnvio, String nro, String cuadro) {
        this.idEnvio = idEnvio;
        this.nro = nro;
        this.cuadro = cuadro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "NOMESTAB")
    public String getNomestab() {
        return nomestab;
    }

    public void setNomestab(String nomestab) {
        this.nomestab = nomestab;
    }

    @XmlElement(name = "DIRECCION")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @XmlElement(name = "TELEFONO")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @XmlElement(name = "SUMENERG")
    public String getSumenerg() {
        return sumenerg;
    }

    public void setSumenerg(String sumenerg) {
        this.sumenerg = sumenerg;
    }

    @XmlElement(name = "PROVSUMI")
    public String getProvsumi() {
        return provsumi;
    }

    public void setProvsumi(String provsumi) {
        this.provsumi = provsumi;
    }

    @XmlElement(name = "CMOD_INI")
    public String getCmodIni() {
        return cmodIni;
    }

    public void setCmodIni(String cmodIni) {
        this.cmodIni = cmodIni;
    }

    @XmlElement(name = "CMOD_INI1")
    public String getCmodIni1() {
        return cmodIni1;
    }

    public void setCmodIni1(String cmodIni1) {
        this.cmodIni1 = cmodIni1;
    }

    @XmlElement(name = "CMOD_INI2")
    public String getCmodIni2() {
        return cmodIni2;
    }

    public void setCmodIni2(String cmodIni2) {
        this.cmodIni2 = cmodIni2;
    }

    @XmlElement(name = "CMOD_PRI")
    public String getCmodPri() {
        return cmodPri;
    }

    public void setCmodPri(String cmodPri) {
        this.cmodPri = cmodPri;
    }

    @XmlElement(name = "CMOD_PRI1")
    public String getCmodPri1() {
        return cmodPri1;
    }

    public void setCmodPri1(String cmodPri1) {
        this.cmodPri1 = cmodPri1;
    }

    @XmlElement(name = "CMOD_PRI2")
    public String getCmodPri2() {
        return cmodPri2;
    }

    public void setCmodPri2(String cmodPri2) {
        this.cmodPri2 = cmodPri2;
    }

    @XmlElement(name = "CMOD_PRI3")
    public String getCmodPri3() {
        return cmodPri3;
    }

    public void setCmodPri3(String cmodPri3) {
        this.cmodPri3 = cmodPri3;
    }

    @XmlElement(name = "CMOD_PRI4")
    public String getCmodPri4() {
        return cmodPri4;
    }

    public void setCmodPri4(String cmodPri4) {
        this.cmodPri4 = cmodPri4;
    }

    @XmlElement(name = "CMOD_PRI5")
    public String getCmodPri5() {
        return cmodPri5;
    }

    public void setCmodPri5(String cmodPri5) {
        this.cmodPri5 = cmodPri5;
    }

    @XmlElement(name = "CMOD_PRI6")
    public String getCmodPri6() {
        return cmodPri6;
    }

    public void setCmodPri6(String cmodPri6) {
        this.cmodPri6 = cmodPri6;
    }

    @XmlElement(name = "CMOD_SEC")
    public String getCmodSec() {
        return cmodSec;
    }

    public void setCmodSec(String cmodSec) {
        this.cmodSec = cmodSec;
    }

    @XmlElement(name = "CMOD_SEC1")
    public String getCmodSec1() {
        return cmodSec1;
    }

    public void setCmodSec1(String cmodSec1) {
        this.cmodSec1 = cmodSec1;
    }

    @XmlElement(name = "CMOD_SEC2")
    public String getCmodSec2() {
        return cmodSec2;
    }

    public void setCmodSec2(String cmodSec2) {
        this.cmodSec2 = cmodSec2;
    }

    @XmlElement(name = "CMOD_SEC3")
    public String getCmodSec3() {
        return cmodSec3;
    }

    public void setCmodSec3(String cmodSec3) {
        this.cmodSec3 = cmodSec3;
    }

    @XmlElement(name = "CMOD_SEC4")
    public String getCmodSec4() {
        return cmodSec4;
    }

    public void setCmodSec4(String cmodSec4) {
        this.cmodSec4 = cmodSec4;
    }

    @XmlElement(name = "CMOD_SEC5")
    public String getCmodSec5() {
        return cmodSec5;
    }

    public void setCmodSec5(String cmodSec5) {
        this.cmodSec5 = cmodSec5;
    }

	@XmlElement(name = "CMOD_EBAI")
    public String getCmodEbai() {
        return cmodEbai;
    }

    public void setCmodEbai(String cmodEbai) {
        this.cmodEbai = cmodEbai;
    }
	@XmlElement(name = "CMOD_EBAI1")
    public String getCmodEbai1() {
        return cmodEbai1;
    }

    public void setCmodEbai1(String cmodEbai1) {
        this.cmodEbai1 = cmodEbai1;
    }
	@XmlElement(name = "CMOD_EBAI2")
    public String getCmodEbai2() {
        return cmodEbai2;
    }

    public void setCmodEbai2(String cmodEbai2) {
        this.cmodEbai2 = cmodEbai2;
    }
	@XmlElement(name = "CMOD_EBAI3")
    public String getCmodEbai3() {
        return cmodEbai3;
    }

    public void setCmodEbai3(String cmodEbai3) {
        this.cmodEbai3 = cmodEbai3;
    }
	@XmlElement(name = "CMOD_EBAI4")
    public String getCmodEbai4() {
        return cmodEbai4;
    }

    public void setCmodEbai4(String cmodEbai4) {
        this.cmodEbai4 = cmodEbai4;
    }
	@XmlElement(name = "CMOD_EBAI5")
    public String getCmodEbai5() {
        return cmodEbai5;
    }

    public void setCmodEbai5(String cmodEbai5) {
        this.cmodEbai5 = cmodEbai5;
    }
	@XmlElement(name = "CMOD_EBAA")
    public String getCmodEbaa() {
        return cmodEbaa;
    }

    public void setCmodEbaa(String cmodEbaa) {
        this.cmodEbaa = cmodEbaa;
    }
	@XmlElement(name = "CMOD_EBAA1")
    public String getCmodEbaa1() {
        return cmodEbaa1;
    }

    public void setCmodEbaa1(String cmodEbaa1) {
        this.cmodEbaa1 = cmodEbaa1;
    }
	@XmlElement(name = "CMOD_EBAA2")
    public String getCmodEbaa2() {
        return cmodEbaa2;
    }

    public void setCmodEbaa2(String cmodEbaa2) {
        this.cmodEbaa2 = cmodEbaa2;
    }
	@XmlElement(name = "CMOD_EBAA3")
    public String getCmodEbaa3() {
        return cmodEbaa3;
    }

    public void setCmodEbaa3(String cmodEbaa3) {
        this.cmodEbaa3 = cmodEbaa3;
    }
	@XmlElement(name = "CMOD_EBAA4")
    public String getCmodEbaa4() {
        return cmodEbaa4;
    }

    public void setCmodEbaa4(String cmodEbaa4) {
        this.cmodEbaa4 = cmodEbaa4;
    }

    @XmlElement(name = "CMOD_EBE")
    public String getCmodEbe() {
        return cmodEbe;
    }

    public void setCmodEbe(String cmodEbe) {
        this.cmodEbe = cmodEbe;
    }

    @XmlElement(name = "CMOD_EBE1")
    public String getCmodEbe1() {
        return cmodEbe1;
    }

    public void setCmodEbe1(String cmodEbe1) {
        this.cmodEbe1 = cmodEbe1;
    }

    @XmlElement(name = "CMOD_EBE2")
    public String getCmodEbe2() {
        return cmodEbe2;
    }

    public void setCmodEbe2(String cmodEbe2) {
        this.cmodEbe2 = cmodEbe2;
    }

	@XmlElement(name = "CMOD_ETP")
    public String getCmodEtp() {
        return cmodEtp;
    }

    public void setCmodEtp(String cmodEtp) {
        this.cmodEtp = cmodEtp;
    }

	@XmlElement(name = "CMOD_ETP1")
    public String getCmodEtp1() {
        return cmodEtp1;
    }

    public void setCmodEtp1(String cmodEtp1) {
        this.cmodEtp1 = cmodEtp1;
    }

	@XmlElement(name = "CMOD_ETP2")
    public String getCmodEtp2() {
        return cmodEtp2;
    }

    public void setCmodEtp2(String cmodEtp2) {
        this.cmodEtp2 = cmodEtp2;
    }

    @XmlElement(name = "CMOD_IST")
    public String getCmodIst() {
        return cmodIst;
    }

    public void setCmodIst(String cmodIst) {
        this.cmodIst = cmodIst;
    }

    @XmlElement(name = "CMOD_ISP")
    public String getCmodIsp() {
        return cmodIsp;
    }

    public void setCmodIsp(String cmodIsp) {
        this.cmodIsp = cmodIsp;
    }

    @XmlElement(name = "CMOD_ESFA")
    public String getCmodEsfa() {
        return cmodEsfa;
    }

    public void setCmodEsfa(String cmodEsfa) {
        this.cmodEsfa = cmodEsfa;
    }

    @XmlTransient
    public Ie2016Cabecera getIe2016Cabecera() {
        return ie2016Cabecera;
    }

    public void setIe2016Cabecera(Ie2016Cabecera ie2016Cabecera) {
        this.ie2016Cabecera = ie2016Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ie2016Establecimientos)) {
            return false;
        }
        Ie2016Establecimientos other = (Ie2016Establecimientos) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Ie2016Establecimientos[idEnvio=" + idEnvio + "]";
    }

}
