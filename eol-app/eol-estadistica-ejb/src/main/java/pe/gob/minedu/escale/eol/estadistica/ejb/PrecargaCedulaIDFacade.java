package pe.gob.minedu.escale.eol.estadistica.ejb;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class PrecargaCedulaIDFacade {

    static final Logger logger = Logger.getLogger(EnvioDocumentosFacade.class.getName());
    @PersistenceContext(unitName = "eol-PU")
    private EntityManager em;
    private StringBuffer cadSQL = new StringBuffer();
    static String BD_ESTADISTICA = "estadistica";
    static String BD_PADRON = "padron";

    public Object[] getPrellenadoCabeceraCedulaID(String codied){

        Object[] cabeceraie = null;
        Query query = null;
        cadSQL.delete(0, cadSQL.length());
        cadSQL.append(" SELECT COD_IED,PREFIJO,NOM_IED,GESTION,CEDULA,");
        cadSQL.append(" DIRECCION,PAGINAWEB,CORREO,TELEFONO,APATERNO,");
        cadSQL.append(" AMATERNO,NOMBRES,NRODOCU,RZSOCIAL,NRORUC,NRODOC ");
        cadSQL.append(" FROM reg_ieduca WHERE COD_IED = ? ");

        query = em.createNativeQuery(cadSQL.toString());
        query.setParameter(1, codied);

        List<Object[]> listResult = query.getResultList();
        if (listResult != null && !listResult.isEmpty()) {
            cabeceraie = listResult.get(0);
        }

        return cabeceraie;
    }
    //imendoza 20170316 inicio
    public Object[] getPrellenadoCabeceraCedulaID2017(String codied){

        Object[] cabeceraie = null;
        Query query = null;
        cadSQL.delete(0, cadSQL.length());
        cadSQL.append(" SELECT COD_IED,PREFIJO,NOM_IED,GESTION,CEDULA,");
        cadSQL.append(" DIRECCION,PAGINAWEB,CORREO,TELEFONO,APATERNO,");
        cadSQL.append(" AMATERNO,NOMBRES,NRODOCU,RZSOCIAL,NRORUC,NRODOC,CM_DOM ");
        cadSQL.append(" FROM reg_ieduca_2017 WHERE COD_IED = ? ");

        query = em.createNativeQuery(cadSQL.toString());
        query.setParameter(1, codied);

        List<Object[]> listResult = query.getResultList();
        if (listResult != null && !listResult.isEmpty()) {
            cabeceraie = listResult.get(0);
        }

        return cabeceraie;
    }
    //imendoza 20170316 fin

    public List<Object[]> getPrellenadoDetalleCedulaID(String codied) {

        Query query = null;
        cadSQL.delete(0, cadSQL.length());
        cadSQL.append(" SELECT COD_IED,CUADRO,NRO,CODLOCAL,NOMESTAB,");
        cadSQL.append(" DIRECCION,TELEFONO,SUMENERG,CM_INI,CM_PRI,");
        cadSQL.append(" CM_SEC,CM_EBAI,CM_EBAA,CM_EBE,CM_ETP,");
        cadSQL.append(" CM_IST,CM_ISP,CM_ESFA ");
        cadSQL.append(" FROM reg_estable WHERE COD_IED = ? ORDER BY NRO");

        query = em.createNativeQuery(cadSQL.toString());
        query.setParameter(1, codied);

        List<Object[]> listResult = query.getResultList();

        return listResult;

    }

    //imendoza 20170310 inicio
    public List<Object[]> getPrellenadoDetalleCedulaID2017(String codied) {

        Query query = null;
        cadSQL.delete(0, cadSQL.length());
        cadSQL.append(" SELECT COD_IED,CUADRO,NRO,CODLOCAL,NOMESTAB,");
        cadSQL.append(" DIRECCION,TELEFONO,SUMENERG,PROVSUMI,CM_INI,CM_PRI,");
        cadSQL.append(" CM_SEC,CM_EBAI,CM_EBAA,CM_EBEI,CM_EBEP,CM_ETP,");
        cadSQL.append(" CM_IST,CM_ISP,CM_ESFA ");
        cadSQL.append(" FROM reg_estable_2017 WHERE COD_IED = ? ORDER BY NRO");

        query = em.createNativeQuery(cadSQL.toString());
        query.setParameter(1, codied);

        List<Object[]> listResult = query.getResultList();

        return listResult;

    }
    //imendoza 20170310 fin

    public List<Object[]> getListaOmisosID() {

        Query query = null;
        cadSQL.delete(0, cadSQL.length());
        cadSQL.append(" select codinst,CMOD,anexo,codooii,ugel from estadistica.cedulaid_fuente ");
        
        query = em.createNativeQuery(cadSQL.toString());
        //query.setParameter(1, codied);

        List<Object[]> listResult = query.getResultList();

        return listResult;

    }

}
