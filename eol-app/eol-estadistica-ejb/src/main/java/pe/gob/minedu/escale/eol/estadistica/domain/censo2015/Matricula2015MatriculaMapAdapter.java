package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Matricula2015MatriculaMapAdapter.Matricula2015MatriculaList;

public class Matricula2015MatriculaMapAdapter extends XmlAdapter<Matricula2015MatriculaList, Map<String, Matricula2015Matricula>>{

    private static final Logger LOGGER = Logger.getLogger(Matricula2015MatriculaMapAdapter.class.getName());

    static class Matricula2015MatriculaList {

        private List<Matricula2015Matricula> detalle;

        private Matricula2015MatriculaList(ArrayList<Matricula2015Matricula> lista) {
            detalle = lista;
        }

        public Matricula2015MatriculaList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2015Matricula> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2015Matricula> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2015Matricula> unmarshal(Matricula2015MatriculaList v) throws Exception {

        Map<String, Matricula2015Matricula> map = new HashMap<String, Matricula2015Matricula>();
        for (Matricula2015Matricula detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2015MatriculaList marshal(Map<String, Matricula2015Matricula> v) throws Exception {
        if(v==null)return null;

        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2015Matricula> lista = new ArrayList<Matricula2015Matricula>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2015Matricula $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2015MatriculaList list = new Matricula2015MatriculaList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
