/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2013_matricula")
public class Matricula2013Matricula implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Transient
    //@Column(name = "CUADRO")
    private String cuadro;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2013Cabecera matricula2013Cabecera;
    
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2013Matricula")
    private List<Matricula2013MatriculaFila> matricula2013MatriculaFilaList;
*/
    public Matricula2013Matricula() {
    }

    public Matricula2013Matricula(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Matricula2013Cabecera getMatricula2013Cabecera() {
        return matricula2013Cabecera;
    }

    public void setMatricula2013Cabecera(Matricula2013Cabecera matricula2013Cabecera) {
        this.matricula2013Cabecera = matricula2013Cabecera;
    }
/*
    @XmlElement(name="MATRICULA_FILAS")
    public List<Matricula2013MatriculaFila> getMatricula2013MatriculaFilaList() {
        return matricula2013MatriculaFilaList;
    }

    public void setMatricula2013MatriculaFilaList(List<Matricula2013MatriculaFila> matricula2013MatriculaFilaList) {
        this.matricula2013MatriculaFilaList = matricula2013MatriculaFilaList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {        
        if (!(object instanceof Matricula2013Matricula)) {
            return false;
        }
        Matricula2013Matricula other = (Matricula2013Matricula) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

 
}
