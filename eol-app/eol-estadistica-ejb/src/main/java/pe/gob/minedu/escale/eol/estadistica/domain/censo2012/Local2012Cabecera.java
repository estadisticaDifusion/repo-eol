/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2012_cabecera")
public class Local2012Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Column(name = "NOMBVIA")
    private String nombvia;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "MANZANA")
    private String manzana;
    @Column(name = "LOTE")
    private String lote;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Column(name = "ETAPA")
    private String etapa;
    @Column(name = "SECTOR")
    private String sector;
    @Column(name = "ZONA")
    private String zona;
    @Column(name = "OTROS")
    private String otros;
    @Column(name = "CATEG_CP")
    private String categCp;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "CODCCPP")
    private String codccpp;
    @Column(name = "REFERENCIA")
    private String referencia;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "COD_AREA")
    private String codArea;
    @Column(name = "COND_TEN")
    private String condTen;
    @Column(name = "PROPLOCAL")
    private String proplocal;
    @Column(name = "GESTION")
    private String gestion;
    @Column(name = "P201")
    private Integer p201;
    @Column(name = "P301")
    private Integer p301;
    @Column(name = "P401_1")
    private String p4011;
    @Column(name = "P401_2")
    private String p4012;
    @Column(name = "P401_3")
    private String p4013;
    @Column(name = "P401_4")
    private String p4014;
    @Column(name = "P401_5")
    private String p4015;
    @Column(name = "P402")
    private String p402;
    @Column(name = "P403")
    private Integer p403;
    @Column(name = "P404")
    private String p404;
    @Column(name = "P405")
    private String p405;
    @Column(name = "P406")
    private String p406;
    @Column(name = "P407_0TOT")
    private Integer p4070tot;
    @Column(name = "P407_0DSK")
    private Integer p4070dsk;
    @Column(name = "P407_0LXO")
    private Integer p4070lxo;
    @Column(name = "P407_0OTR")
    private Integer p4070otr;
    @Column(name = "P407_1TOT")
    private Integer p4071tot;
    @Column(name = "P407_1DSK")
    private Integer p4071dsk;
    @Column(name = "P407_1LXO")
    private Integer p4071lxo;
    @Column(name = "P407_1OTR")
    private Integer p4071otr;
    @Column(name = "P407_2TOT")
    private Integer p4072tot;
    @Column(name = "P407_2DSK")
    private Integer p4072dsk;
    @Column(name = "P407_2LXO")
    private Integer p4072lxo;
    @Column(name = "P407_2OTR")
    private Integer p4072otr;
    @Column(name = "P407_3TOT")
    private Integer p4073tot;
    @Column(name = "P407_3DSK")
    private Integer p4073dsk;
    @Column(name = "P407_3LXO")
    private Integer p4073lxo;
    @Column(name = "P407_3OTR")
    private Integer p4073otr;
    @Column(name = "P408_0TOT")
    private Integer p4080tot;
    @Column(name = "P408_0CNX")
    private Integer p4080cnx;
    @Column(name = "P408_0NCNX")
    private Integer p4080ncnx;
    @Column(name = "P408_1TOT")
    private Integer p4081tot;
    @Column(name = "P408_1CNX")
    private Integer p4081cnx;
    @Column(name = "P408_1NCNX")
    private Integer p4081ncnx;
    @Column(name = "P408_2TOT")
    private Integer p4082tot;
    @Column(name = "P408_2CNX")
    private Integer p4082cnx;
    @Column(name = "P408_2NCNX")
    private Integer p4082ncnx;
    @Column(name = "P408_3TOT")
    private Integer p4083tot;
    @Column(name = "P408_3CNX")
    private Integer p4083cnx;
    @Column(name = "P408_3NCNX")
    private Integer p4083ncnx;
    @Column(name = "P409_0CA")
    private Integer p4090ca;
    @Column(name = "P409_0SI")
    private Integer p4090si;
    @Column(name = "P409_0ME")
    private Integer p4090me;
    @Column(name = "P409_1CA")
    private Integer p4091ca;
    @Column(name = "P409_1SI")
    private Integer p4091si;
    @Column(name = "P409_1ME")
    private Integer p4091me;
    @Column(name = "P409_2CA")
    private Integer p4092ca;
    @Column(name = "P409_2SI")
    private Integer p4092si;
    @Column(name = "P409_2ME")
    private Integer p4092me;
    @Column(name = "P409_3CA")
    private Integer p4093ca;
    @Column(name = "P409_3SI")
    private Integer p4093si;
    @Column(name = "P410_INI")
    private Integer p410Ini;
    @Column(name = "P410_1Y2P")
    private Integer p4101y2p;
    @Column(name = "P410_3A6P")
    private Integer p4103a6p;
    @Column(name = "P410_SCCS")
    private Integer p410Sccs;
    @Column(name = "P411")
    private Integer p411;
    @Column(name = "P412")
    private String p412;
    @Column(name = "P413_1")
    private String p4131;
    @Column(name = "P413_2")
    private String p4132;
    @Column(name = "P414")
    private String p414;
    @Column(name = "P415")
    private String p415;
    @Column(name = "P416_0")
    private Integer p4160;
    @Column(name = "P416_1")
    private Integer p4161;
    @Column(name = "P416_2")
    private Integer p4162;
    @Column(name = "P416_3")
    private Integer p4163;
    @Column(name = "P417")
    private Integer p417;
    @Column(name = "P418")
    private String p418;
    @Column(name = "P601_1")
    private String p6011;
    @Column(name = "P601_2")
    private String p6012;
    @Column(name = "P601_3")
    private String p6013;
    @Column(name = "P601_4")
    private String p6014;
    @Column(name = "P601_51")
    private String p60151;
    @Column(name = "P601_52")
    private String p60152;
    @Column(name = "P601_6")
    private String p6016;
    @Column(name = "P601_7")
    private String p6017;
    @Column(name = "P601_8")
    private String p6018;
    @Column(name = "P602_11")
    private String p60211;
    @Column(name = "P602_12")
    private String p60212;
    @Column(name = "P602_13")
    private String p60213;
    @Column(name = "P602_14")
    private String p60214;
    @Column(name = "P602_15")
    private String p60215;
    @Column(name = "P602_16")
    private String p60216;
    @Column(name = "P602_21")
    private String p60221;
    @Column(name = "P602_22")
    private String p60222;
    @Column(name = "P602_23")
    private String p60223;
    @Column(name = "P602_24")
    private String p60224;
    @Column(name = "P602_25")
    private String p60225;
    @Column(name = "P602_26")
    private String p60226;
    @Column(name = "P603_1D")
    private Integer p6031d;
    @Column(name = "P603_1H")
    private Integer p6031h;
    @Column(name = "P603_1M")
    private Integer p6031m;
    @Column(name = "P603_2D")
    private Integer p6032d;
    @Column(name = "P603_2H")
    private Integer p6032h;
    @Column(name = "P603_2M")
    private Integer p6032m;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "APE_DIR")
    private String apeDir;
    @Column(name = "NOM_DIR")
    private String nomDir;
    @Column(name = "SITUA_DIR")
    private String situaDir;
    @Column(name = "DNI_DIR")
    private String dniDir;
    @Column(name = "EMAIL_DIR")
    private String emailDir;
    @Column(name = "REC_CEDD")
    private String recCedd;
    @Column(name = "REC_CEDM")
    private String recCedm;
    @Column(name = "CUL_CEDD")
    private String culCedd;
    @Column(name = "CUL_CEDM")
    private String culCedm;
    @Column(name = "NRO_INM1")
    private Integer nroInm1;
    @Column(name = "NRO_PREG1")
    private String nroPreg1;
    @Column(name = "SEDE_REG1")
    private String sedeReg1;
    @Column(name = "AREA_REG1")
    private Integer areaReg1;
    @Column(name = "TOMO1")
    private String tomo1;
    @Column(name = "FOLIO1")
    private String folio1;
    @Column(name = "VAL_INM1")
    private Integer valInm1;
    @Column(name = "MINUTA_1")
    private String minuta1;
    @Column(name = "ACTA_1")
    private String acta1;
    @Column(name = "OTROS_1")
    private String otros1;
    @Column(name = "NRO_INM2")
    private Integer nroInm2;
    @Column(name = "NRO_PREG2")
    private String nroPreg2;
    @Column(name = "SEDE_REG2")
    private String sedeReg2;
    @Column(name = "AREA_REG2")
    private Integer areaReg2;
    @Column(name = "TOMO2")
    private String tomo2;
    @Column(name = "FOLIO2")
    private String folio2;
    @Column(name = "VAL_INM2")
    private Integer valInm2;
    @Column(name = "MINUTA_2")
    private String minuta2;
    @Column(name = "ACTA_2")
    private String acta2;
    @Column(name = "OTROS_2")
    private String otros2;
    @Column(name = "NRO_INM3")
    private Integer nroInm3;
    @Column(name = "NRO_PREG3")
    private String nroPreg3;
    @Column(name = "SEDE_REG3")
    private String sedeReg3;
    @Column(name = "AREA_REG3")
    private Integer areaReg3;
    @Column(name = "TOMO3")
    private String tomo3;
    @Column(name = "FOLIO3")
    private String folio3;
    @Column(name = "VAL_INM3")
    private Integer valInm3;
    @Column(name = "MINUTA_3")
    private String minuta3;
    @Column(name = "ACTA_3")
    private String acta3;
    @Column(name = "OTROS_3")
    private String otros3;
    @Column(name = "VALOR_TER")
    private Integer valorTer;
    @Column(name = "ESPECIFICA_1")
    private String especifica1;
    @Column(name = "ESPECIFICA_2")
    private String especifica2;
    @Column(name = "ESPECIFICA_3")
    private String especifica3;
    @Column(name = "CE5_OTROS")
    private String ce5Otros;
    @Column(name = "TIPO_CLIMA")
    private String tipoClima;
    @Column(name = "INT_VIENTO")
    private String intViento;
    @Column(name = "INI_LLUVIA")
    private String iniLluvia;
    @Column(name = "FIN_LLUVIA")
    private String finLluvia;
    @Column(name = "INT_LLUVIA")
    private String intLluvia;
    @Column(name = "HELADAS")
    private String heladas;
    @Column(name = "INI_HELADA")
    private String iniHelada;
    @Column(name = "FIN_HELADA")
    private String finHelada;
    @Column(name = "PELIGROS")
    private String peligros;
    @Column(name = "PELIG_S1")
    private String peligS1;
    @Column(name = "PELIG_S2")
    private String peligS2;
    @Column(name = "PELIG_S3")
    private String peligS3;
    @Column(name = "PELIG_S4")
    private String peligS4;
    @Column(name = "PELIG_S5")
    private String peligS5;
    @Column(name = "PELIG_S6")
    private String peligS6;
    @Column(name = "PELIG_S7")
    private String peligS7;
    @Column(name = "PELIG_S8")
    private String peligS8;
    @Column(name = "PELIG_S9")
    private String peligS9;
    @Column(name = "PELIG_S10")
    private String peligS10;
    @Column(name = "PELIG_S11")
    private String peligS11;
    @Column(name = "PELIG_S12")
    private String peligS12;
    @Column(name = "PELIG_S13")
    private String peligS13;
    @Column(name = "PELIG_S14")
    private String peligS14;
    @Column(name = "PELIG_S15")
    private String peligS15;
    @Column(name = "PELIG_S16")
    private String peligS16;
    @Column(name = "PELIG_S17")
    private String peligS17;
    @Column(name = "PELIG_S17E")
    private String peligS17e;
    @Column(name = "E_BASURA")
    private String eBasura;
    @Column(name = "OBRAS")
    private String obras;
    @Column(name = "ENTIDAD1")
    private String entidad1;
    @Column(name = "CE1_SNIP1")
    private String ce1Snip1;
    @Column(name = "CE1_SNIP2")
    private String ce1Snip2;
    @Column(name = "ENTIDAD2")
    private String entidad2;
    @Column(name = "CE2_SNIP1")
    private String ce2Snip1;
    @Column(name = "CE2_SNIP2")
    private String ce2Snip2;
    @Column(name = "ENTIDAD3")
    private String entidad3;
    @Column(name = "CE3_SNIP1")
    private String ce3Snip1;
    @Column(name = "CE3_SNIP2")
    private String ce3Snip2;
    @Column(name = "ENTIDAD4")
    private String entidad4;
    @Column(name = "CE4_SNIP1")
    private String ce4Snip1;
    @Column(name = "CE4_SNIP2")
    private String ce4Snip2;
    @Column(name = "ENTIDAD5")
    private String entidad5;
    @Column(name = "CE5_SNIP1")
    private String ce5Snip1;
    @Column(name = "CE5_SNIP2")
    private String ce5Snip2;
    @Column(name = "PROT_INC")
    private String protInc;
    @Column(name = "ACCES_DIS")
    private String accesDis;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaenvio;
/*
    @OneToMany(mappedBy = "local2012Cabecera",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2012Sec500> sec500;
    @OneToMany(mappedBy = "local2012Cabecera",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy(value="p104Nro")
    private List<Local2012Sec104> sec104;

    @OneToMany(mappedBy = "local2012Cabecera",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2012Sec300> sec300;
    @OneToMany(mappedBy = "local2012Cabecera",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2012Sec200> sec200;
*/
    @Transient
    private long token;

    @Transient
    private String msg;

    @Transient
    private String estadoRpt;


    public Local2012Cabecera() {
    }

    public Local2012Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name = "NOMBVIA")
    public String getNombvia() {
        return nombvia;
    }

    public void setNombvia(String nombvia) {
        this.nombvia = nombvia;
    }

    @XmlElement(name = "NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name = "MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name = "LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name = "CATEGORIA")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlElement(name = "NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name = "ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name = "SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name = "ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name = "OTROS")
    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    @XmlElement(name = "CATEG_CP")
    public String getCategCp() {
        return categCp;
    }

    public void setCategCp(String categCp) {
        this.categCp = categCp;
    }

    @XmlElement(name = "CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name = "CODCCPP")
    public String getCodccpp() {
        return codccpp;
    }

    public void setCodccpp(String codccpp) {
        this.codccpp = codccpp;
    }

    @XmlElement(name = "REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "COD_AREA")
    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    @XmlElement(name = "COND_TEN")
    public String getCondTen() {
        return condTen;
    }

    public void setCondTen(String condTen) {
        this.condTen = condTen;
    }

    @XmlElement(name = "PROPLOCAL")
    public String getProplocal() {
        return proplocal;
    }

    public void setProplocal(String proplocal) {
        this.proplocal = proplocal;
    }

    @XmlElement(name = "GESTION")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name = "P201")
    public Integer getP201() {
        return p201;
    }

    public void setP201(Integer p201) {
        this.p201 = p201;
    }

    @XmlElement(name = "P301")
    public Integer getP301() {
        return p301;
    }

    public void setP301(Integer p301) {
        this.p301 = p301;
    }

    @XmlElement(name = "P401_1")
    public String getP4011() {
        return p4011;
    }

    public void setP4011(String p4011) {
        this.p4011 = p4011;
    }

    @XmlElement(name = "P401_2")
    public String getP4012() {
        return p4012;
    }

    public void setP4012(String p4012) {
        this.p4012 = p4012;
    }

    @XmlElement(name = "P401_3")
    public String getP4013() {
        return p4013;
    }

    public void setP4013(String p4013) {
        this.p4013 = p4013;
    }

    @XmlElement(name = "P401_4")
    public String getP4014() {
        return p4014;
    }

    public void setP4014(String p4014) {
        this.p4014 = p4014;
    }

    @XmlElement(name = "P401_5")
    public String getP4015() {
        return p4015;
    }

    public void setP4015(String p4015) {
        this.p4015 = p4015;
    }

    @XmlElement(name = "P402")
    public String getP402() {
        return p402;
    }

    public void setP402(String p402) {
        this.p402 = p402;
    }

    @XmlElement(name = "P403")
    public Integer getP403() {
        return p403;
    }

    public void setP403(Integer p403) {
        this.p403 = p403;
    }

    @XmlElement(name = "P404")
    public String getP404() {
        return p404;
    }

    public void setP404(String p404) {
        this.p404 = p404;
    }

    @XmlElement(name = "P405")
    public String getP405() {
        return p405;
    }


    public void setP405(String p405) {
        this.p405 = p405;
    }

    @XmlElement(name = "P406")
    public String getP406() {
        return p406;
    }

    public void setP406(String p406) {
        this.p406 = p406;
    }

    @XmlElement(name = "P407_0TOT")
    public Integer getP4070tot() {
        return p4070tot;
    }


    public void setP4070tot(Integer p4070tot) {
        this.p4070tot = p4070tot;
    }

    @XmlElement(name = "P407_0DSK")
    public Integer getP4070dsk() {
        return p4070dsk;
    }


    public void setP4070dsk(Integer p4070dsk) {
        this.p4070dsk = p4070dsk;
    }

    @XmlElement(name = "P407_0LXO")
    public Integer getP4070lxo() {
        return p4070lxo;
    }


    public void setP4070lxo(Integer p4070lxo) {
        this.p4070lxo = p4070lxo;
    }

    @XmlElement(name = "P407_0OTR")
    public Integer getP4070otr() {
        return p4070otr;
    }


    public void setP4070otr(Integer p4070otr) {
        this.p4070otr = p4070otr;
    }

    @XmlElement(name = "P407_1TOT")
    public Integer getP4071tot() {
        return p4071tot;
    }

    public void setP4071tot(Integer p4071tot) {
        this.p4071tot = p4071tot;
    }

    @XmlElement(name = "P407_1DSK")
    public Integer getP4071dsk() {
        return p4071dsk;
    }

    public void setP4071dsk(Integer p4071dsk) {
        this.p4071dsk = p4071dsk;
    }

    @XmlElement(name = "P407_1LXO")
    public Integer getP4071lxo() {
        return p4071lxo;
    }

    public void setP4071lxo(Integer p4071lxo) {
        this.p4071lxo = p4071lxo;
    }

    @XmlElement(name = "P407_1OTR")
    public Integer getP4071otr() {
        return p4071otr;
    }

    public void setP4071otr(Integer p4071otr) {
        this.p4071otr = p4071otr;
    }

    @XmlElement(name = "P407_2TOT")
    public Integer getP4072tot() {
        return p4072tot;
    }

    public void setP4072tot(Integer p4072tot) {
        this.p4072tot = p4072tot;
    }

    @XmlElement(name = "P407_2DSK")
    public Integer getP4072dsk() {
        return p4072dsk;
    }

    public void setP4072dsk(Integer p4072dsk) {
        this.p4072dsk = p4072dsk;
    }

    @XmlElement(name = "P407_2LXO")
    public Integer getP4072lxo() {
        return p4072lxo;
    }

    public void setP4072lxo(Integer p4072lxo) {
        this.p4072lxo = p4072lxo;
    }

    @XmlElement(name = "P407_2OTR")
    public Integer getP4072otr() {
        return p4072otr;
    }

    public void setP4072otr(Integer p4072otr) {
        this.p4072otr = p4072otr;
    }

    @XmlElement(name = "P407_3TOT")
    public Integer getP4073tot() {
        return p4073tot;
    }

    public void setP4073tot(Integer p4073tot) {
        this.p4073tot = p4073tot;
    }

    @XmlElement(name = "P407_3DSK")
    public Integer getP4073dsk() {
        return p4073dsk;
    }

    public void setP4073dsk(Integer p4073dsk) {
        this.p4073dsk = p4073dsk;
    }

    @XmlElement(name = "P407_3LXO")
    public Integer getP4073lxo() {
        return p4073lxo;
    }

    public void setP4073lxo(Integer p4073lxo) {
        this.p4073lxo = p4073lxo;
    }

    @XmlElement(name = "P407_3OTR")
    public Integer getP4073otr() {
        return p4073otr;
    }

    public void setP4073otr(Integer p4073otr) {
        this.p4073otr = p4073otr;
    }

    @XmlElement(name = "P408_0TOT")
    public Integer getP4080tot() {
        return p4080tot;
    }

    public void setP4080tot(Integer p4080tot) {
        this.p4080tot = p4080tot;
    }

    @XmlElement(name = "P408_0CNX")
    public Integer getP4080cnx() {
        return p4080cnx;
    }

    public void setP4080cnx(Integer p4080cnx) {
        this.p4080cnx = p4080cnx;
    }

    @XmlElement(name = "P408_0NCNX")
    public Integer getP4080ncnx() {
        return p4080ncnx;
    }

    public void setP4080ncnx(Integer p4080ncnx) {
        this.p4080ncnx = p4080ncnx;
    }

    @XmlElement(name = "P408_1TOT")
    public Integer getP4081tot() {
        return p4081tot;
    }

    public void setP4081tot(Integer p4081tot) {
        this.p4081tot = p4081tot;
    }

    @XmlElement(name = "P408_1CNX")
    public Integer getP4081cnx() {
        return p4081cnx;
    }

    public void setP4081cnx(Integer p4081cnx) {
        this.p4081cnx = p4081cnx;
    }

    @XmlElement(name = "P408_1NCNX")
    public Integer getP4081ncnx() {
        return p4081ncnx;
    }

    public void setP4081ncnx(Integer p4081ncnx) {
        this.p4081ncnx = p4081ncnx;
    }

    @XmlElement(name = "P408_2TOT")
    public Integer getP4082tot() {
        return p4082tot;
    }

    public void setP4082tot(Integer p4082tot) {
        this.p4082tot = p4082tot;
    }

    @XmlElement(name = "P408_2CNX")
    public Integer getP4082cnx() {
        return p4082cnx;
    }

    public void setP4082cnx(Integer p4082cnx) {
        this.p4082cnx = p4082cnx;
    }

    @XmlElement(name = "P408_2NCNX")
    public Integer getP4082ncnx() {
        return p4082ncnx;
    }

    public void setP4082ncnx(Integer p4082ncnx) {
        this.p4082ncnx = p4082ncnx;
    }

    @XmlElement(name = "P408_3TOT")
    public Integer getP4083tot() {
        return p4083tot;
    }

    public void setP4083tot(Integer p4083tot) {
        this.p4083tot = p4083tot;
    }

    @XmlElement(name = "P408_3CNX")
    public Integer getP4083cnx() {
        return p4083cnx;
    }

    public void setP4083cnx(Integer p4083cnx) {
        this.p4083cnx = p4083cnx;
    }

    @XmlElement(name = "P408_3NCNX")
    public Integer getP4083ncnx() {
        return p4083ncnx;
    }

    public void setP4083ncnx(Integer p4083ncnx) {
        this.p4083ncnx = p4083ncnx;
    }

    @XmlElement(name = "P409_0CA")
    public Integer getP4090ca() {
        return p4090ca;
    }

    public void setP4090ca(Integer p4090ca) {
        this.p4090ca = p4090ca;
    }

    @XmlElement(name = "P409_0SI")
    public Integer getP4090si() {
        return p4090si;
    }

    public void setP4090si(Integer p4090si) {
        this.p4090si = p4090si;
    }

    @XmlElement(name = "P409_0ME")
    public Integer getP4090me() {
        return p4090me;
    }

    public void setP4090me(Integer p4090me) {
        this.p4090me = p4090me;
    }

    @XmlElement(name = "P409_1CA")
    public Integer getP4091ca() {
        return p4091ca;
    }

    public void setP4091ca(Integer p4091ca) {
        this.p4091ca = p4091ca;
    }

    @XmlElement(name = "P409_1SI")
    public Integer getP4091si() {
        return p4091si;
    }

    public void setP4091si(Integer p4091si) {
        this.p4091si = p4091si;
    }

    @XmlElement(name = "P409_1ME")
    public Integer getP4091me() {
        return p4091me;
    }

    public void setP4091me(Integer p4091me) {
        this.p4091me = p4091me;
    }

    @XmlElement(name = "P409_2CA")
    public Integer getP4092ca() {
        return p4092ca;
    }

    public void setP4092ca(Integer p4092ca) {
        this.p4092ca = p4092ca;
    }

    @XmlElement(name = "P409_2SI")
    public Integer getP4092si() {
        return p4092si;
    }

    public void setP4092si(Integer p4092si) {
        this.p4092si = p4092si;
    }

    @XmlElement(name = "P409_2ME")
    public Integer getP4092me() {
        return p4092me;
    }

    public void setP4092me(Integer p4092me) {
        this.p4092me = p4092me;
    }

    @XmlElement(name = "P409_3CA")
    public Integer getP4093ca() {
        return p4093ca;
    }

    public void setP4093ca(Integer p4093ca) {
        this.p4093ca = p4093ca;
    }

    @XmlElement(name = "P409_3SI")
    public Integer getP4093si() {
        return p4093si;
    }

    public void setP4093si(Integer p4093si) {
        this.p4093si = p4093si;
    }

    @XmlElement(name = "P410_INI")
    public Integer getP410Ini() {
        return p410Ini;
    }

    public void setP410Ini(Integer p410Ini) {
        this.p410Ini = p410Ini;
    }

    @XmlElement(name = "P410_1Y2P")
    public Integer getP4101y2p() {
        return p4101y2p;
    }

    public void setP4101y2p(Integer p4101y2p) {
        this.p4101y2p = p4101y2p;
    }

    @XmlElement(name = "P410_3A6P")
    public Integer getP4103a6p() {
        return p4103a6p;
    }

    public void setP4103a6p(Integer p4103a6p) {
        this.p4103a6p = p4103a6p;
    }

    @XmlElement(name = "P410_SCCS")
    public Integer getP410Sccs() {
        return p410Sccs;
    }

    public void setP410Sccs(Integer p410Sccs) {
        this.p410Sccs = p410Sccs;
    }

    @XmlElement(name = "P411")
    public Integer getP411() {
        return p411;
    }

    public void setP411(Integer p411) {
        this.p411 = p411;
    }

    @XmlElement(name = "P412")
    public String getP412() {
        return p412;
    }

    public void setP412(String p412) {
        this.p412 = p412;
    }

    @XmlElement(name = "P413_1")
    public String getP4131() {
        return p4131;
    }

    public void setP4131(String p4131) {
        this.p4131 = p4131;
    }

    @XmlElement(name = "P413_2")
    public String getP4132() {
        return p4132;
    }

    public void setP4132(String p4132) {
        this.p4132 = p4132;
    }

    @XmlElement(name = "P414")
    public String getP414() {
        return p414;
    }

    public void setP414(String p414) {
        this.p414 = p414;
    }

    @XmlElement(name = "P415")
    public String getP415() {
        return p415;
    }

    public void setP415(String p415) {
        this.p415 = p415;
    }

    @XmlElement(name = "P416_0")
    public Integer getP4160() {
        return p4160;
    }

    public void setP4160(Integer p4160) {
        this.p4160 = p4160;
    }

    @XmlElement(name = "P416_1")
    public Integer getP4161() {
        return p4161;
    }

    public void setP4161(Integer p4161) {
        this.p4161 = p4161;
    }

    @XmlElement(name = "P416_2")
    public Integer getP4162() {
        return p4162;
    }

    public void setP4162(Integer p4162) {
        this.p4162 = p4162;
    }

    @XmlElement(name = "P416_3")
    public Integer getP4163() {
        return p4163;
    }

    public void setP4163(Integer p4163) {
        this.p4163 = p4163;
    }

    @XmlElement(name = "P417")
    public Integer getP417() {
        return p417;
    }

    public void setP417(Integer p417) {
        this.p417 = p417;
    }

    @XmlElement(name = "P418")
    public String getP418() {
        return p418;
    }

    public void setP418(String p418) {
        this.p418 = p418;
    }

    @XmlElement(name = "P601_1")
    public String getP6011() {
        return p6011;
    }

    public void setP6011(String p6011) {
        this.p6011 = p6011;
    }

    @XmlElement(name = "P601_2")
    public String getP6012() {
        return p6012;
    }

    public void setP6012(String p6012) {
        this.p6012 = p6012;
    }

    @XmlElement(name = "P601_3")
    public String getP6013() {
        return p6013;
    }

    public void setP6013(String p6013) {
        this.p6013 = p6013;
    }

    @XmlElement(name = "P601_4")
    public String getP6014() {
        return p6014;
    }

    public void setP6014(String p6014) {
        this.p6014 = p6014;
    }

    @XmlElement(name = "P601_51")
    public String getP60151() {
        return p60151;
    }

    public void setP60151(String p60151) {
        this.p60151 = p60151;
    }

    @XmlElement(name = "P601_52")
    public String getP60152() {
        return p60152;
    }

    public void setP60152(String p60152) {
        this.p60152 = p60152;
    }

    @XmlElement(name = "P601_6")
    public String getP6016() {
        return p6016;
    }

    public void setP6016(String p6016) {
        this.p6016 = p6016;
    }

    @XmlElement(name = "P601_7")
    public String getP6017() {
        return p6017;
    }

    public void setP6017(String p6017) {
        this.p6017 = p6017;
    }

    @XmlElement(name = "P601_8")
    public String getP6018() {
        return p6018;
    }

    public void setP6018(String p6018) {
        this.p6018 = p6018;
    }

    @XmlElement(name = "P602_11")
    public String getP60211() {
        return p60211;
    }

    public void setP60211(String p60211) {
        this.p60211 = p60211;
    }

    @XmlElement(name = "P602_12")
    public String getP60212() {
        return p60212;
    }

    public void setP60212(String p60212) {
        this.p60212 = p60212;
    }

    @XmlElement(name = "P602_13")
    public String getP60213() {
        return p60213;
    }

    public void setP60213(String p60213) {
        this.p60213 = p60213;
    }

    @XmlElement(name = "P602_14")
    public String getP60214() {
        return p60214;
    }

    public void setP60214(String p60214) {
        this.p60214 = p60214;
    }

    @XmlElement(name = "P602_15")
    public String getP60215() {
        return p60215;
    }

    public void setP60215(String p60215) {
        this.p60215 = p60215;
    }

    @XmlElement(name = "P602_16")
    public String getP60216() {
        return p60216;
    }

    public void setP60216(String p60216) {
        this.p60216 = p60216;
    }

    @XmlElement(name = "P602_21")
    public String getP60221() {
        return p60221;
    }

    public void setP60221(String p60221) {
        this.p60221 = p60221;
    }

    @XmlElement(name = "P602_22")
    public String getP60222() {
        return p60222;
    }

    public void setP60222(String p60222) {
        this.p60222 = p60222;
    }

    @XmlElement(name = "P602_23")
    public String getP60223() {
        return p60223;
    }

    public void setP60223(String p60223) {
        this.p60223 = p60223;
    }

    @XmlElement(name = "P602_24")
    public String getP60224() {
        return p60224;
    }

    public void setP60224(String p60224) {
        this.p60224 = p60224;
    }

    @XmlElement(name = "P602_25")
    public String getP60225() {
        return p60225;
    }

    public void setP60225(String p60225) {
        this.p60225 = p60225;
    }

    @XmlElement(name = "P602_26")
    public String getP60226() {
        return p60226;
    }

    public void setP60226(String p60226) {
        this.p60226 = p60226;
    }

    @XmlElement(name = "P603_1D")
    public Integer getP6031d() {
        return p6031d;
    }

    public void setP6031d(Integer p6031d) {
        this.p6031d = p6031d;
    }

    @XmlElement(name = "P603_1H")
    public Integer getP6031h() {
        return p6031h;
    }

    public void setP6031h(Integer p6031h) {
        this.p6031h = p6031h;
    }

    @XmlElement(name = "P603_1M")
    public Integer getP6031m() {
        return p6031m;
    }

    public void setP6031m(Integer p6031m) {
        this.p6031m = p6031m;
    }

    @XmlElement(name = "P603_2D")
    public Integer getP6032d() {
        return p6032d;
    }

    public void setP6032d(Integer p6032d) {
        this.p6032d = p6032d;
    }

    @XmlElement(name = "P603_2H")
    public Integer getP6032h() {
        return p6032h;
    }

    public void setP6032h(Integer p6032h) {
        this.p6032h = p6032h;
    }

    @XmlElement(name = "P603_2M")
    public Integer getP6032m() {
        return p6032m;
    }

    public void setP6032m(Integer p6032m) {
        this.p6032m = p6032m;
    }

    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "APE_DIR")
    public String getApeDir() {
        return apeDir;
    }

    public void setApeDir(String apeDir) {
        this.apeDir = apeDir;
    }

    @XmlElement(name = "NOM_DIR")
    public String getNomDir() {
        return nomDir;
    }

    public void setNomDir(String nomDir) {
        this.nomDir = nomDir;
    }

    @XmlElement(name = "SITUA_DIR")
    public String getSituaDir() {
        return situaDir;
    }

    public void setSituaDir(String situaDir) {
        this.situaDir = situaDir;
    }

    @XmlElement(name = "DNI_DIR")
    public String getDniDir() {
        return dniDir;
    }

    public void setDniDir(String dniDir) {
        this.dniDir = dniDir;
    }

    @XmlElement(name = "EMAIL_DIR")
    public String getEmailDir() {
        return emailDir;
    }

    public void setEmailDir(String emailDir) {
        this.emailDir = emailDir;
    }

    @XmlElement(name = "REC_CEDD")
    public String getRecCedd() {
        return recCedd;
    }

    public void setRecCedd(String recCedd) {
        this.recCedd = recCedd;
    }

    @XmlElement(name = "REC_CEDM")
    public String getRecCedm() {
        return recCedm;
    }

    public void setRecCedm(String recCedm) {
        this.recCedm = recCedm;
    }

    @XmlElement(name = "CUL_CEDD")
    public String getCulCedd() {
        return culCedd;
    }

    public void setCulCedd(String culCedd) {
        this.culCedd = culCedd;
    }

    @XmlElement(name = "CUL_CEDM")
    public String getCulCedm() {
        return culCedm;
    }

    public void setCulCedm(String culCedm) {
        this.culCedm = culCedm;
    }

    @XmlElement(name = "NRO_INM1")
    public Integer getNroInm1() {
        return nroInm1;
    }

    public void setNroInm1(Integer nroInm1) {
        this.nroInm1 = nroInm1;
    }

    @XmlElement(name = "NRO_PREG1")
    public String getNroPreg1() {
        return nroPreg1;
    }

    public void setNroPreg1(String nroPreg1) {
        this.nroPreg1 = nroPreg1;
    }

    @XmlElement(name = "SEDE_REG1")
    public String getSedeReg1() {
        return sedeReg1;
    }

    public void setSedeReg1(String sedeReg1) {
        this.sedeReg1 = sedeReg1;
    }

    @XmlElement(name = "AREA_REG1")
    public Integer getAreaReg1() {
        return areaReg1;
    }

    public void setAreaReg1(Integer areaReg1) {
        this.areaReg1 = areaReg1;
    }

    @XmlElement(name = "TOMO1")
    public String getTomo1() {
        return tomo1;
    }

    public void setTomo1(String tomo1) {
        this.tomo1 = tomo1;
    }

    @XmlElement(name = "FOLIO1")
    public String getFolio1() {
        return folio1;
    }

    public void setFolio1(String folio1) {
        this.folio1 = folio1;
    }

    @XmlElement(name = "VAL_INM1")
    public Integer getValInm1() {
        return valInm1;
    }

    public void setValInm1(Integer valInm1) {
        this.valInm1 = valInm1;
    }

    @XmlElement(name = "MINUTA_1")
    public String getMinuta1() {
        return minuta1;
    }

    public void setMinuta1(String minuta1) {
        this.minuta1 = minuta1;
    }

    @XmlElement(name = "ACTA_1")
    public String getActa1() {
        return acta1;
    }

    public void setActa1(String acta1) {
        this.acta1 = acta1;
    }

    @XmlElement(name = "OTROS_1")
    public String getOtros1() {
        return otros1;
    }

    public void setOtros1(String otros1) {
        this.otros1 = otros1;
    }

    @XmlElement(name = "NRO_INM2")
    public Integer getNroInm2() {
        return nroInm2;
    }

    public void setNroInm2(Integer nroInm2) {
        this.nroInm2 = nroInm2;
    }

    @XmlElement(name = "NRO_PREG2")
    public String getNroPreg2() {
        return nroPreg2;
    }

    public void setNroPreg2(String nroPreg2) {
        this.nroPreg2 = nroPreg2;
    }

    @XmlElement(name = "SEDE_REG2")
    public String getSedeReg2() {
        return sedeReg2;
    }

    public void setSedeReg2(String sedeReg2) {
        this.sedeReg2 = sedeReg2;
    }

    @XmlElement(name = "AREA_REG2")
    public Integer getAreaReg2() {
        return areaReg2;
    }

    public void setAreaReg2(Integer areaReg2) {
        this.areaReg2 = areaReg2;
    }

    @XmlElement(name = "TOMO2")
    public String getTomo2() {
        return tomo2;
    }

    public void setTomo2(String tomo2) {
        this.tomo2 = tomo2;
    }

    @XmlElement(name = "FOLIO2")
    public String getFolio2() {
        return folio2;
    }

    public void setFolio2(String folio2) {
        this.folio2 = folio2;
    }

    @XmlElement(name = "VAL_INM2")
    public Integer getValInm2() {
        return valInm2;
    }

    public void setValInm2(Integer valInm2) {
        this.valInm2 = valInm2;
    }

    @XmlElement(name = "MINUTA_2")
    public String getMinuta2() {
        return minuta2;
    }

    public void setMinuta2(String minuta2) {
        this.minuta2 = minuta2;
    }

    @XmlElement(name = "ACTA_2")
    public String getActa2() {
        return acta2;
    }

    public void setActa2(String acta2) {
        this.acta2 = acta2;
    }

    @XmlElement(name = "OTROS_2")
    public String getOtros2() {
        return otros2;
    }

    public void setOtros2(String otros2) {
        this.otros2 = otros2;
    }

    @XmlElement(name = "NRO_INM3")
    public Integer getNroInm3() {
        return nroInm3;
    }

    public void setNroInm3(Integer nroInm3) {
        this.nroInm3 = nroInm3;
    }

    @XmlElement(name = "NRO_PREG3")
    public String getNroPreg3() {
        return nroPreg3;
    }

    public void setNroPreg3(String nroPreg3) {
        this.nroPreg3 = nroPreg3;
    }

    @XmlElement(name = "SEDE_REG3")
    public String getSedeReg3() {
        return sedeReg3;
    }

    public void setSedeReg3(String sedeReg3) {
        this.sedeReg3 = sedeReg3;
    }

    @XmlElement(name = "AREA_REG3")
    public Integer getAreaReg3() {
        return areaReg3;
    }

    public void setAreaReg3(Integer areaReg3) {
        this.areaReg3 = areaReg3;
    }

    @XmlElement(name = "TOMO3")
    public String getTomo3() {
        return tomo3;
    }

    public void setTomo3(String tomo3) {
        this.tomo3 = tomo3;
    }

    @XmlElement(name = "FOLIO3")
    public String getFolio3() {
        return folio3;
    }

    public void setFolio3(String folio3) {
        this.folio3 = folio3;
    }

    @XmlElement(name = "VAL_INM3")
    public Integer getValInm3() {
        return valInm3;
    }

    public void setValInm3(Integer valInm3) {
        this.valInm3 = valInm3;
    }

    @XmlElement(name = "MINUTA_3")
    public String getMinuta3() {
        return minuta3;
    }

    public void setMinuta3(String minuta3) {
        this.minuta3 = minuta3;
    }

    @XmlElement(name = "ACTA_3")
    public String getActa3() {
        return acta3;
    }

    public void setActa3(String acta3) {
        this.acta3 = acta3;
    }

    @XmlElement(name = "OTROS_3")
    public String getOtros3() {
        return otros3;
    }

    public void setOtros3(String otros3) {
        this.otros3 = otros3;
    }

    @XmlElement(name = "VALOR_TER")
    public Integer getValorTer() {
        return valorTer;
    }

    public void setValorTer(Integer valorTer) {
        this.valorTer = valorTer;
    }

    

    @XmlElement(name = "TIPO_CLIMA")
    public String getTipoClima() {
        return tipoClima;
    }

    public void setTipoClima(String tipoClima) {
        this.tipoClima = tipoClima;
    }

    @XmlElement(name = "INT_VIENTO")
    public String getIntViento() {
        return intViento;
    }

    public void setIntViento(String intViento) {
        this.intViento = intViento;
    }

    @XmlElement(name = "INI_LLUVIA")
    public String getIniLluvia() {
        return iniLluvia;
    }

    public void setIniLluvia(String iniLluvia) {
        this.iniLluvia = iniLluvia;
    }

    @XmlElement(name = "FIN_LLUVIA")
    public String getFinLluvia() {
        return finLluvia;
    }

    public void setFinLluvia(String finLluvia) {
        this.finLluvia = finLluvia;
    }

    @XmlElement(name = "INT_LLUVIA")
    public String getIntLluvia() {
        return intLluvia;
    }

    public void setIntLluvia(String intLluvia) {
        this.intLluvia = intLluvia;
    }

    @XmlElement(name = "HELADAS")
    public String getHeladas() {
        return heladas;
    }

    public void setHeladas(String heladas) {
        this.heladas = heladas;
    }

    @XmlElement(name = "INI_HELADA")
    public String getIniHelada() {
        return iniHelada;
    }

    public void setIniHelada(String iniHelada) {
        this.iniHelada = iniHelada;
    }

    @XmlElement(name = "FIN_HELADA")
    public String getFinHelada() {
        return finHelada;
    }

    public void setFinHelada(String finHelada) {
        this.finHelada = finHelada;
    }

    @XmlElement(name = "PELIGROS")
    public String getPeligros() {
        return peligros;
    }

    public void setPeligros(String peligros) {
        this.peligros = peligros;
    }

    @XmlElement(name = "PELIG_S1")
    public String getPeligS1() {
        return peligS1;
    }

    public void setPeligS1(String peligS1) {
        this.peligS1 = peligS1;
    }
    
    @XmlElement(name = "PELIG_S2")
    public String getPeligS2() {
        return peligS2;
    }

    public void setPeligS2(String peligS2) {
        this.peligS2 = peligS2;
    }

    @XmlElement(name = "PELIG_S3")
    public String getPeligS3() {
        return peligS3;
    }

    public void setPeligS3(String peligS3) {
        this.peligS3 = peligS3;
    }

    @XmlElement(name = "PELIG_S4")
    public String getPeligS4() {
        return peligS4;
    }

    public void setPeligS4(String peligS4) {
        this.peligS4 = peligS4;
    }

    @XmlElement(name = "PELIG_S5")
    public String getPeligS5() {
        return peligS5;
    }

    public void setPeligS5(String peligS5) {
        this.peligS5 = peligS5;
    }

    @XmlElement(name = "PELIG_S6")
    public String getPeligS6() {
        return peligS6;
    }

    public void setPeligS6(String peligS6) {
        this.peligS6 = peligS6;
    }

    @XmlElement(name = "PELIG_S7")
    public String getPeligS7() {
        return peligS7;
    }

    public void setPeligS7(String peligS7) {
        this.peligS7 = peligS7;
    }

    @XmlElement(name = "PELIG_S8")
    public String getPeligS8() {
        return peligS8;
    }

    public void setPeligS8(String peligS8) {
        this.peligS8 = peligS8;
    }

    @XmlElement(name = "PELIG_S9")
    public String getPeligS9() {
        return peligS9;
    }

    public void setPeligS9(String peligS9) {
        this.peligS9 = peligS9;
    }

    @XmlElement(name = "PELIG_S10")
    public String getPeligS10() {
        return peligS10;
    }

    public void setPeligS10(String peligS10) {
        this.peligS10 = peligS10;
    }

    @XmlElement(name = "PELIG_S11")
    public String getPeligS11() {
        return peligS11;
    }

    public void setPeligS11(String peligS11) {
        this.peligS11 = peligS11;
    }

    @XmlElement(name = "PELIG_S12")
    public String getPeligS12() {
        return peligS12;
    }

    public void setPeligS12(String peligS12) {
        this.peligS12 = peligS12;
    }

    @XmlElement(name = "PELIG_S13")
    public String getPeligS13() {
        return peligS13;
    }

    public void setPeligS13(String peligS13) {
        this.peligS13 = peligS13;
    }

    @XmlElement(name = "PELIG_S14")
    public String getPeligS14() {
        return peligS14;
    }

    public void setPeligS14(String peligS14) {
        this.peligS14 = peligS14;
    }

    @XmlElement(name = "PELIG_S15")
    public String getPeligS15() {
        return peligS15;
    }

    public void setPeligS15(String peligS15) {
        this.peligS15 = peligS15;
    }

    @XmlElement(name = "PELIG_S16")
    public String getPeligS16() {
        return peligS16;
    }

    public void setPeligS16(String peligS16) {
        this.peligS16 = peligS16;
    }

    @XmlElement(name = "PELIG_S17")
    public String getPeligS17() {
        return peligS17;
    }

    public void setPeligS17(String peligS17) {
        this.peligS17 = peligS17;
    }

    @XmlElement(name = "PELIG_S17E")
    public String getPeligS17e() {
        return peligS17e;
    }

    public void setPeligS17e(String peligS17e) {
        this.peligS17e = peligS17e;
    }

    @XmlElement(name = "E_BASURA")
    public String getEBasura() {
        return eBasura;
    }

    public void setEBasura(String eBasura) {
        this.eBasura = eBasura;
    }

    @XmlElement(name = "OBRAS")
    public String getObras() {
        return obras;
    }

    public void setObras(String obras) {
        this.obras = obras;
    }

    @XmlElement(name = "ENTIDAD1")
    public String getEntidad1() {
        return entidad1;
    }

    public void setEntidad1(String entidad1) {
        this.entidad1 = entidad1;
    }

    @XmlElement(name = "CE1_SNIP1")
    public String getCe1Snip1() {
        return ce1Snip1;
    }

    public void setCe1Snip1(String ce1Snip1) {
        this.ce1Snip1 = ce1Snip1;
    }

    @XmlElement(name = "CE1_SNIP2")
    public String getCe1Snip2() {
        return ce1Snip2;
    }

    public void setCe1Snip2(String ce1Snip2) {
        this.ce1Snip2 = ce1Snip2;
    }

    @XmlElement(name = "ENTIDAD2")
    public String getEntidad2() {
        return entidad2;
    }

    public void setEntidad2(String entidad2) {
        this.entidad2 = entidad2;
    }

    @XmlElement(name = "CE2_SNIP1")
    public String getCe2Snip1() {
        return ce2Snip1;
    }

    public void setCe2Snip1(String ce2Snip1) {
        this.ce2Snip1 = ce2Snip1;
    }

    @XmlElement(name = "CE2_SNIP2")
    public String getCe2Snip2() {
        return ce2Snip2;
    }

    public void setCe2Snip2(String ce2Snip2) {
        this.ce2Snip2 = ce2Snip2;
    }

    @XmlElement(name = "ENTIDAD3")
    public String getEntidad3() {
        return entidad3;
    }

    public void setEntidad3(String entidad3) {
        this.entidad3 = entidad3;
    }

    @XmlElement(name = "CE3_SNIP1")
    public String getCe3Snip1() {
        return ce3Snip1;
    }

    public void setCe3Snip1(String ce3Snip1) {
        this.ce3Snip1 = ce3Snip1;
    }

    @XmlElement(name = "CE3_SNIP2")
    public String getCe3Snip2() {
        return ce3Snip2;
    }

    public void setCe3Snip2(String ce3Snip2) {
        this.ce3Snip2 = ce3Snip2;
    }

    @XmlElement(name = "ENTIDAD4")
    public String getEntidad4() {
        return entidad4;
    }

    public void setEntidad4(String entidad4) {
        this.entidad4 = entidad4;
    }

    @XmlElement(name = "CE4_SNIP1")
    public String getCe4Snip1() {
        return ce4Snip1;
    }

    public void setCe4Snip1(String ce4Snip1) {
        this.ce4Snip1 = ce4Snip1;
    }

    @XmlElement(name = "CE4_SNIP2")
    public String getCe4Snip2() {
        return ce4Snip2;
    }

    public void setCe4Snip2(String ce4Snip2) {
        this.ce4Snip2 = ce4Snip2;
    }

    @XmlElement(name = "ENTIDAD5")
    public String getEntidad5() {
        return entidad5;
    }

    public void setEntidad5(String entidad5) {
        this.entidad5 = entidad5;
    }

    @XmlElement(name = "CE5_SNIP1")
    public String getCe5Snip1() {
        return ce5Snip1;
    }

    public void setCe5Snip1(String ce5Snip1) {
        this.ce5Snip1 = ce5Snip1;
    }

    @XmlElement(name = "CE5_SNIP2")
    public String getCe5Snip2() {
        return ce5Snip2;
    }

    public void setCe5Snip2(String ce5Snip2) {
        this.ce5Snip2 = ce5Snip2;
    }

    @XmlElement(name = "PROT_INC")
    public String getProtInc() {
        return protInc;
    }

    public void setProtInc(String protInc) {
        this.protInc = protInc;
    }

    @XmlElement(name = "ACCES_DIS")
    public String getAccesDis() {
        return accesDis;
    }

    public void setAccesDis(String accesDis) {
        this.accesDis = accesDis;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2012Cabecera)) {
            return false;
        }
        Local2012Cabecera other = (Local2012Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

/*

    @XmlElement(name = "SEC500")
    public List<Local2012Sec500> getSec500() {
        return sec500;
    }


    public void setSec500(List<Local2012Sec500> sec500) {
        this.sec500 = sec500;
    }

    @XmlElement(name = "SEC104")
    public List<Local2012Sec104> getSec104() {
        return sec104;
    }


    public void setSec104(List<Local2012Sec104> sec104) {
        this.sec104 = sec104;
    }


    @XmlElement(name = "SEC300")
    public List<Local2012Sec300> getSec300() {
        return sec300;
    }


    public void setSec300(List<Local2012Sec300> sec300) {
        this.sec300 = sec300;
    }


    @XmlElement(name = "SEC200")
    public List<Local2012Sec200> getSec200() {
        return sec200;
    }


    public void setSec200(List<Local2012Sec200> sec200) {
        this.sec200 = sec200;
    }
*/
    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlElement(name="VERSION")
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    
    public Boolean getUltimo() {
        return ultimo;
    }

    /**
     * @param ultimo the ultimo to set
     */
    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    
    public Date getFechaenvio() {
        return fechaenvio;
    }

    public void setFechaenvio(Date fechaenvio) {
        this.fechaenvio = fechaenvio;
    }

    @XmlElement(name="ESPECIFICA_1")
    public String getEspecifica1() {
        return especifica1;
    }

    public void setEspecifica1(String especifica1) {
        this.especifica1 = especifica1;
    }

    @XmlElement(name="ESPECIFICA_2")
    public String getEspecifica2() {
        return especifica2;
    }

    public void setEspecifica2(String especifica2) {
        this.especifica2 = especifica2;
    }

    @XmlElement(name="ESPECIFICA_3")
    public String getEspecifica3() {
        return especifica3;
    }

    public void setEspecifica3(String especifica3) {
        this.especifica3 = especifica3;
    }

    @XmlElement(name="CE5_OTROS")
    public String getCe5Otros() {
        return ce5Otros;
    }

    public void setCe5Otros(String ce5Otros) {
        this.ce5Otros = ce5Otros;
    }


}
