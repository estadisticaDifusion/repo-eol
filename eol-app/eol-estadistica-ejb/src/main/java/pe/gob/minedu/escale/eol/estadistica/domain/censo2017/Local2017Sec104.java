/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "local2017_sec104")
public class Local2017Sec104 implements Serializable { private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P104_NRO")
    private Integer p104Nro;
    @Column(name = "P104_CM")
    private String p104Cm;
    @Column(name = "P104_ANX")
    private String p104Anx;
    @Column(name = "P104_IE")
    private String p104Ie;
    @Column(name = "P104_GES")
    private String p104Ges;
    @Column(name = "P104_NM")
    private String p104Nm;
    @Column(name = "P104_TUR")
    private String p104Tur;
    @Column(name = "P104_JEC")
    private String p104Jec;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2017Cabecera local2017Cabecera;

    public Local2017Sec104() {
    }

    public Local2017Sec104(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    @XmlElement(name="P104_NRO")
    public Integer getP104Nro() {
        return p104Nro;
    }

    public void setP104Nro(Integer p104Nro) {
        this.p104Nro = p104Nro;
    }
    @XmlElement(name="P104_CM")
    public String getP104Cm() {
        return p104Cm;
    }

    public void setP104Cm(String p104Cm) {
        this.p104Cm = p104Cm;
    }
    @XmlElement(name="P104_ANX")
    public String getP104Anx() {
        return p104Anx;
    }

    public void setP104Anx(String p104Anx) {
        this.p104Anx = p104Anx;
    }
    @XmlElement(name="P104_IE")
    public String getP104Ie() {
        return p104Ie;
    }

    public void setP104Ges(String p104Ges) {
        this.p104Ges = p104Ges;
    }

    @XmlElement(name="P104_GES")
    public String getP104Ges() {
        return p104Ges;
    }

    public void setP104Ie(String p104Ie) {
        this.p104Ie = p104Ie;
    }
    @XmlElement(name="P104_NM")
    public String getP104Nm() {
        return p104Nm;
    }

    public void setP104Nm(String p104Nm) {
        this.p104Nm = p104Nm;
    }
    @XmlElement(name="P104_TUR")
    public String getP104Tur() {
        return p104Tur;
    }

    public void setP104Tur(String p104Tur) {
        this.p104Tur = p104Tur;
    }
    @XmlElement(name="P104_JEC")
    public String getP104Jec() {
        return p104Jec;
    }

    public void setP104Jec(String p104Jec) {
        this.p104Jec = p104Jec;
    }
    @XmlTransient
    public Local2017Cabecera getLocal2017Cabecera() {
        return local2017Cabecera;
    }

    public void setLocal2017Cabecera(Local2017Cabecera local2017Cabecera) {
        this.local2017Cabecera = local2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2017Sec104)) {
            return false;
        }
        Local2017Sec104 other = (Local2017Sec104) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Local2017Sec104[idEnvio=" + idEnvio + "]";
    }

}
