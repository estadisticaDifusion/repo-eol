/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2012_recursos_fila")

public class Matricula2012RecursosFila implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL")
    private Integer total;
    @Column(name = "D01")
    private Integer d01;
    @Column(name = "D02")
    private Integer d02;
    @Column(name = "D03")
    private Integer d03;
    @Column(name = "D04")
    private Integer d04;
    @Column(name = "D05")
    private Integer d05;
    @Column(name = "D06")
    private Integer d06;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2012Recursos matricula2012Recursos;

    public Matricula2012RecursosFila() {
    }

    public Matricula2012RecursosFila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name="TOTAL")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @XmlElement(name="D01")
    public Integer getD01() {
        return d01;
    }

    public void setD01(Integer d01) {
        this.d01 = d01;
    }

    @XmlElement(name="D02")
    public Integer getD02() {
        return d02;
    }

    public void setD02(Integer d02) {
        this.d02 = d02;
    }

    @XmlElement(name="D03")
    public Integer getD03() {
        return d03;
    }

    public void setD03(Integer d03) {
        this.d03 = d03;
    }

    @XmlElement(name="D04")
    public Integer getD04() {
        return d04;
    }

    public void setD04(Integer d04) {
        this.d04 = d04;
    }

    @XmlElement(name="D05")
    public Integer getD05() {
        return d05;
    }

    public void setD05(Integer d05) {
        this.d05 = d05;
    }

    @XmlElement(name="D06")
    public Integer getD06() {
        return d06;
    }

    public void setD06(Integer d06) {
        this.d06 = d06;
    }

    @XmlTransient
    public Matricula2012Recursos getMatricula2012Recursos() {
        return matricula2012Recursos;
    }

    public void setMatricula2012Recursos(Matricula2012Recursos matricula2012Recursos) {
        this.matricula2012Recursos = matricula2012Recursos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2012RecursosFila)) {
            return false;
        }
        Matricula2012RecursosFila other = (Matricula2012RecursosFila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @XmlElement(name="DESCRIP")
    public String getDescrip() {
        return descrip;
    }

  
    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

}
