/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "resultado2015_detalle")
public class Resultado2015Detalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Basic(optional = false)
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Resultado2015Cabecera cedula;
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detalle", fetch = FetchType.EAGER)
    private List<Resultado2015Fila> filas;
*/
    public Resultado2015Detalle() {
    }

    public Resultado2015Detalle(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Resultado2015Detalle(Long idEnvio, String cuadro) {
        this.idEnvio = idEnvio;
        this.cuadro = cuadro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Resultado2015Cabecera getCedula() {
        return cedula;
    }

    public void setCedula(Resultado2015Cabecera cedula) {
        this.cedula = cedula;
    }
/*
    @XmlElement(name="FILAS")
    public List<Resultado2015Fila> getFilas() {
        return filas;
    }

    public void setFilas(List<Resultado2015Fila> filas) {
        this.filas = filas;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado2015Detalle)) {
            return false;
        }
        Resultado2015Detalle other = (Resultado2015Detalle) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2015.Resultado2015Detalle[idEnvio=" + idEnvio + "]";
    }

}
