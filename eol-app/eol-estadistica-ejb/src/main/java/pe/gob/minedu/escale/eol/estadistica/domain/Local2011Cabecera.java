/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2011_cabecera")
public class Local2011Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Column(name = "NOM_VIA")
    private String nomVia;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "MANZANA")
    private String manzana;
    @Column(name = "LOTE")
    private String lote;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Column(name = "ETAPA")
    private String etapa;
    @Column(name = "SECTOR")
    private String sector;
    @Column(name = "ZONA")
    private String zona;
    @Column(name = "OTROS")
    private String otros;
    @Column(name = "REFERENCIA")
    private String referencia;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "CODIGEL")
    private String codigel;
    @Column(name = "CM_ENVIO")
    private String cmEnvio;
    @Column(name = "ANX_ENVIO")
    private String anxEnvio;
    @Column(name = "P105")
    private String p105;
    @Column(name = "P102")
    private String p102;
    @Column(name = "P103")
    private String p103;
    @Column(name = "P201_0T")
    private Integer p2010t;
    @Column(name = "P201_0U")
    private Integer p2010u;
    @Column(name = "P201_1T")
    private Integer p2011t;
    @Column(name = "P201_1U")
    private Integer p2011u;
    @Column(name = "P201_2T")
    private Integer p2012t;
    @Column(name = "P201_2U")
    private Integer p2012u;
    @Column(name = "P201_3T")
    private Integer p2013t;
    @Column(name = "P201_3U")
    private Integer p2013u;
    @Column(name = "P201_4T")
    private Integer p2014t;
    @Column(name = "P201_4U")
    private Integer p2014u;
    @Column(name = "P201_5T")
    private Integer p2015t;
    @Column(name = "P201_5U")
    private Integer p2015u;
    @Column(name = "P202_1G")
    private String p2021g;
    @Column(name = "P202_2G")
    private String p2022g;
    @Column(name = "P202_3G")
    private String p2023g;
    @Column(name = "P202_4G")
    private String p2024g;
    @Column(name = "P202_5G")
    private String p2025g;
    @Column(name = "P203")
    private String p203;
    @Column(name = "P204")
    private Integer p204;
    @Column(name = "P205")
    private String p205;
    @Column(name = "P206")
    private String p206;
    @Column(name = "P207_1C")
    private String p2071c;
    @Column(name = "P207_2C")
    private String p2072c;
    @Column(name = "P207_3C")
    private String p2073c;
    @Column(name = "P207_4C")
    private String p2074c;
    @Column(name = "P207_4D")
    private String p2074d;
    @Column(name = "P208")
    private String p208;
    @Column(name = "P209_0T")
    private Integer p2090t;
    @Column(name = "P209_0E")
    private Integer p2090e;
    @Column(name = "P209_0X")
    private Integer p2090x;
    @Column(name = "P209_0O")
    private Integer p2090o;
    @Column(name = "P209_1T")
    private Integer p2091t;
    @Column(name = "P209_1E")
    private Integer p2091e;
    @Column(name = "P209_1X")
    private Integer p2091x;
    @Column(name = "P209_1O")
    private Integer p2091o;
    @Column(name = "P209_2T")
    private Integer p2092t;
    @Column(name = "P209_2E")
    private Integer p2092e;
    @Column(name = "P209_2X")
    private Integer p2092x;
    @Column(name = "P209_2O")
    private Integer p2092o;
    @Column(name = "P209_3T")
    private Integer p2093t;
    @Column(name = "P209_3E")
    private Integer p2093e;
    @Column(name = "P209_3X")
    private Integer p2093x;
    @Column(name = "P209_3O")
    private Integer p2093o;
    @Column(name = "P210_0T")
    private Integer p2100t;
    @Column(name = "P210_0C")
    private Integer p2100c;
    @Column(name = "P210_1T")
    private Integer p2101t;
    @Column(name = "P210_1C")
    private Integer p2101c;
    @Column(name = "P210_2T")
    private Integer p2102t;
    @Column(name = "P210_2C")
    private Integer p2102c;
    @Column(name = "P210_3T")
    private Integer p2103t;
    @Column(name = "P210_3C")
    private Integer p2103c;
    @Column(name = "P211_1")
    private String p2111;
    @Column(name = "P211_2")
    private String p2112;
    @Column(name = "P211_3")
    private String p2113;
    @Column(name = "P211_4")
    private String p2114;
    @Column(name = "P212")
    private Integer p212;
    @Column(name = "P213")
    private Integer p213;
    @Column(name = "P214")
    private String p214;
    @Column(name = "P215_0C")
    private Integer p2150c;
    @Column(name = "P215_0S")
    private Integer p2150s;
    @Column(name = "P215_0M")
    private Integer p2150m;
    @Column(name = "P215_1C")
    private Integer p2151c;
    @Column(name = "P215_1S")
    private Integer p2151s;
    @Column(name = "P215_1M")
    private Integer p2151m;
    @Column(name = "P215_2C")
    private Integer p2152c;
    @Column(name = "P215_2S")
    private Integer p2152s;
    @Column(name = "P215_2M")
    private Integer p2152m;
    @Column(name = "P215_3C")
    private Integer p2153c;
    @Column(name = "P215_3S")
    private Integer p2153s;
    @Column(name = "P216_1")
    private Integer p2161;
    @Column(name = "P216_2")
    private Integer p2162;
    @Column(name = "P216_3")
    private Integer p2163;
    @Column(name = "P216_4")
    private Integer p2164;
    @Column(name = "P217")
    private Integer p217;
    @Column(name = "P218")
    private String p218;
    @Column(name = "P219")
    private String p219;
    @Column(name = "P219_D")
    private String p219D;
    @Column(name = "P220")
    private String p220;
    @Column(name = "P221")
    private String p221;
    @Column(name = "P222_0")
    private Integer p2220;
    @Column(name = "P222_1")
    private Integer p2221;
    @Column(name = "P222_2")
    private Integer p2222;
    @Column(name = "P222_3")
    private Integer p2223;
    @Column(name = "P301")
    private Integer p301;
    @Column(name = "P401")
    private Integer p401;
    @Column(name = "P501_11")
    private String p50111;
    @Column(name = "P501_12")
    private String p50112;
    @Column(name = "P501_13")
    private String p50113;
    @Column(name = "P501_14")
    private String p50114;
    @Column(name = "P501_15")
    private String p50115;
    @Column(name = "P501_16")
    private String p50116;
    @Column(name = "P501_17")
    private String p50117;
    @Column(name = "P501_21")
    private String p50121;
    @Column(name = "P501_22")
    private String p50122;
    @Column(name = "P501_23")
    private String p50123;
    @Column(name = "P501_24")
    private String p50124;
    @Column(name = "P501_25")
    private String p50125;
    @Column(name = "P501_26")
    private String p50126;
    @Column(name = "P501_27")
    private String p50127;
    @Column(name = "P501_31")
    private String p50131;
    @Column(name = "P501_32")
    private String p50132;
    @Column(name = "P501_33")
    private String p50133;
    @Column(name = "P501_34")
    private String p50134;
    @Column(name = "P501_35")
    private String p50135;
    @Column(name = "P501_36")
    private String p50136;
    @Column(name = "P501_37")
    private String p50137;
    @Column(name = "P501_41")
    private String p50141;
    @Column(name = "P501_42")
    private String p50142;
    @Column(name = "P501_43")
    private String p50143;
    @Column(name = "P501_44")
    private String p50144;
    @Column(name = "P501_45")
    private String p50145;
    @Column(name = "P501_46")
    private String p50146;
    @Column(name = "P501_47")
    private String p50147;
    @Column(name = "P501_51")
    private String p50151;
    @Column(name = "P501_52")
    private String p50152;
    @Column(name = "P501_53")
    private String p50153;
    @Column(name = "P501_54")
    private String p50154;
    @Column(name = "P501_55")
    private String p50155;
    @Column(name = "P501_56")
    private String p50156;
    @Column(name = "P501_57")
    private String p50157;
    @Column(name = "P501_61")
    private String p50161;
    @Column(name = "P501_62")
    private String p50162;
    @Column(name = "P501_63")
    private String p50163;
    @Column(name = "P501_64")
    private String p50164;
    @Column(name = "P501_65")
    private String p50165;
    @Column(name = "P501_66")
    private String p50166;
    @Column(name = "P501_67")
    private String p50167;
    @Column(name = "P501_71")
    private String p50171;
    @Column(name = "P501_72")
    private String p50172;
    @Column(name = "P501_73")
    private String p50173;
    @Column(name = "P501_74")
    private String p50174;
    @Column(name = "P501_75")
    private String p50175;
    @Column(name = "P501_76")
    private String p50176;
    @Column(name = "P501_77")
    private String p50177;
    @Column(name = "P601_1")
    private String p6011;
    @Column(name = "P601_2")
    private String p6012;
    @Column(name = "P601_3")
    private String p6013;
    @Column(name = "P601_4")
    private String p6014;
    @Column(name = "P601_51")
    private String p60151;
    @Column(name = "P601_52")
    private Integer p60152;
    @Column(name = "P601_6")
    private String p6016;
    @Column(name = "P601_7")
    private String p6017;
    @Column(name = "P601_8")
    private String p6018;
    @Column(name = "P602_11")
    private String p60211;
    @Column(name = "P602_12")
    private String p60212;
    @Column(name = "P602_13")
    private String p60213;
    @Column(name = "P602_14")
    private String p60214;
    @Column(name = "P602_15")
    private String p60215;
    @Column(name = "P602_16")
    private String p60216;
    @Column(name = "P602_21")
    private String p60221;
    @Column(name = "P602_22")
    private String p60222;
    @Column(name = "P602_23")
    private String p60223;
    @Column(name = "P602_24")
    private String p60224;
    @Column(name = "P602_25")
    private String p60225;
    @Column(name = "P602_26")
    private String p60226;
    @Column(name = "P603_11")
    private Integer p60311;
    @Column(name = "P603_12")
    private Integer p60312;
    @Column(name = "P603_13")
    private Integer p60313;
    @Column(name = "P603_21")
    private Integer p60321;
    @Column(name = "P603_22")
    private Integer p60322;
    @Column(name = "P603_23")
    private Integer p60323;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "NOM_DIR")
    private String nomDir;
    @Column(name = "APE_DIR")
    private String apeDir;
    @Column(name = "CONDICION_DIR")
    private String condicionDir;
    @Column(name = "DNI_DIR")
    private String dniDir;
    @Column(name = "EMAIL_DIR")
    private String emailDir;
    @Column(name = "REC_CED_DIA")
    private String recCedDia;
    @Column(name = "REC_CED_MES")
    private String recCedMes;
    @Column(name = "CUL_CED_DIA")
    private String culCedDia;
    @Column(name = "CUL_CED_MES")
    private String culCedMes;    
    @Column(name = "EMAIL_INST")
    private String emailInst;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaenvio;
    @Column(name = "CODIGEL_PADRON")
    private String codigelPadron;

    @Transient
    private long token;
/*
    @OneToMany(mappedBy = "local2011Cabecera",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2011Sec300> local2011Sec300Collection;
    @OneToMany(mappedBy = "local2011Cabecera",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2011Sec400> local2011Sec400Collection;
    @OneToMany(mappedBy = "local2011Cabecera",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2011Sec104> local2011Sec104Collection;
    */
    @Transient
    private String msg;

    @Transient
    private String estadoRpt;

    public Local2011Cabecera() {
    }

    public Local2011Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name = "NOM_VIA")
    public String getNomVia() {
        return nomVia;
    }

    public void setNomVia(String nomVia) {
        this.nomVia = nomVia;
    }

    @XmlElement(name = "NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name = "MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name = "LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name = "CATEGORIA")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlElement(name = "NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name = "ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name = "SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name = "ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name = "OTROS")
    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    @XmlElement(name = "REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlElement(name = "CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name = "CODIGEL")
    public String getCodigel() {
        return codigel;
    }

    public void setCodigel(String codigel) {
        this.codigel = codigel;
    }

    @XmlElement(name = "CM_ENVIO")
    public String getCmEnvio() {
        return cmEnvio;
    }

    public void setCmEnvio(String cmEnvio) {
        this.cmEnvio = cmEnvio;
    }

    @XmlElement(name = "ANX_ENVIO")
    public String getAnxEnvio() {
        return anxEnvio;
    }

    public void setAnxEnvio(String anxEnvio) {
        this.anxEnvio = anxEnvio;
    }

    @XmlElement(name = "P105")
    public String getP105() {
        return p105;
    }

    public void setP105(String p105) {
        this.p105 = p105;
    }

    @XmlElement(name = "P102")
    public String getP102() {
        return p102;
    }

    public void setP102(String p102) {
        this.p102 = p102;
    }


    @XmlElement(name = "P103")
    public String getP103() {
        return p103;
    }

    public void setP103(String p103) {
        this.p103 = p103;
    }


    @XmlElement(name = "P201_0T")
    public Integer getP2010t() {
        return p2010t;
    }

    public void setP2010t(Integer p2010t) {
        this.p2010t = p2010t;
    }

    @XmlElement(name = "P201_0U")
    public Integer getP2010u() {
        return p2010u;
    }

    public void setP2010u(Integer p2010u) {
        this.p2010u = p2010u;
    }

    @XmlElement(name = "P201_1T")
    public Integer getP2011t() {
        return p2011t;
    }

    public void setP2011t(Integer p2011t) {
        this.p2011t = p2011t;
    }

    @XmlElement(name = "P201_1U")
    public Integer getP2011u() {
        return p2011u;
    }

    public void setP2011u(Integer p2011u) {
        this.p2011u = p2011u;
    }

    @XmlElement(name = "P201_2T")
    public Integer getP2012t() {
        return p2012t;
    }

    public void setP2012t(Integer p2012t) {
        this.p2012t = p2012t;
    }

    @XmlElement(name = "P201_2U")
    public Integer getP2012u() {
        return p2012u;
    }

    public void setP2012u(Integer p2012u) {
        this.p2012u = p2012u;
    }

    @XmlElement(name = "P201_3T")
    public Integer getP2013t() {
        return p2013t;
    }

    public void setP2013t(Integer p2013t) {
        this.p2013t = p2013t;
    }

    @XmlElement(name = "P201_3U")
    public Integer getP2013u() {
        return p2013u;
    }

    public void setP2013u(Integer p2013u) {
        this.p2013u = p2013u;
    }

    @XmlElement(name = "P201_4T")
    public Integer getP2014t() {
        return p2014t;
    }

    public void setP2014t(Integer p2014t) {
        this.p2014t = p2014t;
    }

    @XmlElement(name = "P201_4U")
    public Integer getP2014u() {
        return p2014u;
    }

    public void setP2014u(Integer p2014u) {
        this.p2014u = p2014u;
    }

    @XmlElement(name = "P201_5T")
    public Integer getP2015t() {
        return p2015t;
    }

    public void setP2015t(Integer p2015t) {
        this.p2015t = p2015t;
    }

    @XmlElement(name = "P201_5U")
    public Integer getP2015u() {
        return p2015u;
    }

    public void setP2015u(Integer p2015u) {
        this.p2015u = p2015u;
    }

    @XmlElement(name = "P202_1G")
    public String getP2021g() {
        return p2021g;
    }

    public void setP2021g(String p2021g) {
        this.p2021g = p2021g;
    }

    @XmlElement(name = "P202_2G")
    public String getP2022g() {
        return p2022g;
    }

    public void setP2022g(String p2022g) {
        this.p2022g = p2022g;
    }

    @XmlElement(name = "P202_3G")
    public String getP2023g() {
        return p2023g;
    }

    public void setP2023g(String p2023g) {
        this.p2023g = p2023g;
    }

    @XmlElement(name = "P202_4G")
    public String getP2024g() {
        return p2024g;
    }

    public void setP2024g(String p2024g) {
        this.p2024g = p2024g;
    }

    @XmlElement(name = "P202_5G")
    public String getP2025g() {
        return p2025g;
    }

    public void setP2025g(String p2025g) {
        this.p2025g = p2025g;
    }

    @XmlElement(name = "P203")
    public String getP203() {
        return p203;
    }

    public void setP203(String p203) {
        this.p203 = p203;
    }

    @XmlElement(name = "P204")
    public Integer getP204() {
        return p204;
    }

    public void setP204(Integer p204) {
        this.p204 = p204;
    }

    @XmlElement(name = "P205")
    public String getP205() {
        return p205;
    }

    public void setP205(String p205) {
        this.p205 = p205;
    }

    @XmlElement(name = "P206")
    public String getP206() {
        return p206;
    }

    public void setP206(String p206) {
        this.p206 = p206;
    }

    @XmlElement(name = "P207_1C")
    public String getP2071c() {
        return p2071c;
    }

    public void setP2071c(String p2071c) {
        this.p2071c = p2071c;
    }

    @XmlElement(name = "P207_2C")
    public String getP2072c() {
        return p2072c;
    }

    public void setP2072c(String p2072c) {
        this.p2072c = p2072c;
    }

    @XmlElement(name = "P207_3C")
    public String getP2073c() {
        return p2073c;
    }

    public void setP2073c(String p2073c) {
        this.p2073c = p2073c;
    }

    @XmlElement(name = "P207_4C")
    public String getP2074c() {
        return p2074c;
    }

    public void setP2074c(String p2074c) {
        this.p2074c = p2074c;
    }

    @XmlElement(name = "P207_4D")
    public String getP2074d() {
        return p2074d;
    }

    public void setP2074d(String p2074d) {
        this.p2074d = p2074d;
    }

    @XmlElement(name = "P208")
    public String getP208() {
        return p208;
    }

    public void setP208(String p208) {
        this.p208 = p208;
    }

    @XmlElement(name = "P209_0T")
    public Integer getP2090t() {
        return p2090t;
    }

    public void setP2090t(Integer p2090t) {
        this.p2090t = p2090t;
    }

    @XmlElement(name = "P209_0E")
    public Integer getP2090e() {
        return p2090e;
    }

    public void setP2090e(Integer p2090e) {
        this.p2090e = p2090e;
    }

    @XmlElement(name = "P209_0X")
    public Integer getP2090x() {
        return p2090x;
    }

    public void setP2090x(Integer p2090x) {
        this.p2090x = p2090x;
    }

    @XmlElement(name = "P209_0O")
    public Integer getP2090o() {
        return p2090o;
    }

    public void setP2090o(Integer p2090o) {
        this.p2090o = p2090o;
    }

    @XmlElement(name = "P209_1T")
    public Integer getP2091t() {
        return p2091t;
    }

    public void setP2091t(Integer p2091t) {
        this.p2091t = p2091t;
    }

    @XmlElement(name = "P209_1E")
    public Integer getP2091e() {
        return p2091e;
    }

    public void setP2091e(Integer p2091e) {
        this.p2091e = p2091e;
    }

    @XmlElement(name = "P209_1X")
    public Integer getP2091x() {
        return p2091x;
    }

    public void setP2091x(Integer p2091x) {
        this.p2091x = p2091x;
    }

    @XmlElement(name = "P209_1O")
    public Integer getP2091o() {
        return p2091o;
    }

    public void setP2091o(Integer p2091o) {
        this.p2091o = p2091o;
    }

    @XmlElement(name = "P209_2T")
    public Integer getP2092t() {
        return p2092t;
    }

    public void setP2092t(Integer p2092t) {
        this.p2092t = p2092t;
    }

    @XmlElement(name = "P209_2E")
    public Integer getP2092e() {
        return p2092e;
    }

    public void setP2092e(Integer p2092e) {
        this.p2092e = p2092e;
    }

    @XmlElement(name = "P209_2X")
    public Integer getP2092x() {
        return p2092x;
    }

    public void setP2092x(Integer p2092x) {
        this.p2092x = p2092x;
    }

    @XmlElement(name = "P209_2O")
    public Integer getP2092o() {
        return p2092o;
    }

    public void setP2092o(Integer p2092o) {
        this.p2092o = p2092o;
    }

    @XmlElement(name = "P209_3T")
    public Integer getP2093t() {
        return p2093t;
    }

    public void setP2093t(Integer p2093t) {
        this.p2093t = p2093t;
    }

    @XmlElement(name = "P209_3E")
    public Integer getP2093e() {
        return p2093e;
    }

    public void setP2093e(Integer p2093e) {
        this.p2093e = p2093e;
    }

    @XmlElement(name = "P209_3X")
    public Integer getP2093x() {
        return p2093x;
    }

    public void setP2093x(Integer p2093x) {
        this.p2093x = p2093x;
    }

    @XmlElement(name = "P209_3O")
    public Integer getP2093o() {
        return p2093o;
    }

    public void setP2093o(Integer p2093o) {
        this.p2093o = p2093o;
    }

    @XmlElement(name = "P210_0T")
    public Integer getP2100t() {
        return p2100t;
    }

    public void setP2100t(Integer p2100t) {
        this.p2100t = p2100t;
    }

    @XmlElement(name = "P210_0C")
    public Integer getP2100c() {
        return p2100c;
    }

    public void setP2100c(Integer p2100c) {
        this.p2100c = p2100c;
    }

    @XmlElement(name = "P210_1T")
    public Integer getP2101t() {
        return p2101t;
    }

    public void setP2101t(Integer p2101t) {
        this.p2101t = p2101t;
    }

    @XmlElement(name = "P210_1C")
    public Integer getP2101c() {
        return p2101c;
    }

    public void setP2101c(Integer p2101c) {
        this.p2101c = p2101c;
    }

    @XmlElement(name = "P210_2T")
    public Integer getP2102t() {
        return p2102t;
    }

    public void setP2102t(Integer p2102t) {
        this.p2102t = p2102t;
    }

    @XmlElement(name = "P210_2C")
    public Integer getP2102c() {
        return p2102c;
    }

    public void setP2102c(Integer p2102c) {
        this.p2102c = p2102c;
    }

    @XmlElement(name = "P210_3T")
    public Integer getP2103t() {
        return p2103t;
    }

    public void setP2103t(Integer p2103t) {
        this.p2103t = p2103t;
    }

    @XmlElement(name = "P210_3C")
    public Integer getP2103c() {
        return p2103c;
    }

    public void setP2103c(Integer p2103c) {
        this.p2103c = p2103c;
    }

    @XmlElement(name = "P211_1")
    public String getP2111() {
        return p2111;
    }

    public void setP2111(String p2111) {
        this.p2111 = p2111;
    }

    @XmlElement(name = "P211_2")
    public String getP2112() {
        return p2112;
    }

    public void setP2112(String p2112) {
        this.p2112 = p2112;
    }

    @XmlElement(name = "P211_3")
    public String getP2113() {
        return p2113;
    }

    public void setP2113(String p2113) {
        this.p2113 = p2113;
    }

    @XmlElement(name = "P211_4")
    public String getP2114() {
        return p2114;
    }

    public void setP2114(String p2114) {
        this.p2114 = p2114;
    }

    @XmlElement(name = "P212")
    public Integer getP212() {
        return p212;
    }

    public void setP212(Integer p212) {
        this.p212 = p212;
    }

    @XmlElement(name = "P213")
    public Integer getP213() {
        return p213;
    }

    public void setP213(Integer p213) {
        this.p213 = p213;
    }

    @XmlElement(name = "P214")
    public String getP214() {
        return p214;
    }

    public void setP214(String p214) {
        this.p214 = p214;
    }

    @XmlElement(name = "P215_0C")
    public Integer getP2150c() {
        return p2150c;
    }

    public void setP2150c(Integer p2150c) {
        this.p2150c = p2150c;
    }

    @XmlElement(name = "P215_0S")
    public Integer getP2150s() {
        return p2150s;
    }

    public void setP2150s(Integer p2150s) {
        this.p2150s = p2150s;
    }

    @XmlElement(name = "P215_0M")
    public Integer getP2150m() {
        return p2150m;
    }

    public void setP2150m(Integer p2150m) {
        this.p2150m = p2150m;
    }

    @XmlElement(name = "P215_1C")
    public Integer getP2151c() {
        return p2151c;
    }

    public void setP2151c(Integer p2151c) {
        this.p2151c = p2151c;
    }

    @XmlElement(name = "P215_1S")
    public Integer getP2151s() {
        return p2151s;
    }

    public void setP2151s(Integer p2151s) {
        this.p2151s = p2151s;
    }

    @XmlElement(name = "P215_1M")
    public Integer getP2151m() {
        return p2151m;
    }

    public void setP2151m(Integer p2151m) {
        this.p2151m = p2151m;
    }

    @XmlElement(name = "P215_2C")
    public Integer getP2152c() {
        return p2152c;
    }

    public void setP2152c(Integer p2152c) {
        this.p2152c = p2152c;
    }

    @XmlElement(name = "P215_2S")
    public Integer getP2152s() {
        return p2152s;
    }

    public void setP2152s(Integer p2152s) {
        this.p2152s = p2152s;
    }

    @XmlElement(name = "P215_2M")
    public Integer getP2152m() {
        return p2152m;
    }

    public void setP2152m(Integer p2152m) {
        this.p2152m = p2152m;
    }

    @XmlElement(name = "P215_3C")
    public Integer getP2153c() {
        return p2153c;
    }

    public void setP2153c(Integer p2153c) {
        this.p2153c = p2153c;
    }

    @XmlElement(name = "P215_3S")
    public Integer getP2153s() {
        return p2153s;
    }

    public void setP2153s(Integer p2153s) {
        this.p2153s = p2153s;
    }

    @XmlElement(name = "P216_1")
    public Integer getP2161() {
        return p2161;
    }

    public void setP2161(Integer p2161) {
        this.p2161 = p2161;
    }

    @XmlElement(name = "P216_2")
    public Integer getP2162() {
        return p2162;
    }

    public void setP2162(Integer p2162) {
        this.p2162 = p2162;
    }

    @XmlElement(name = "P216_3")
    public Integer getP2163() {
        return p2163;
    }

    public void setP2163(Integer p2163) {
        this.p2163 = p2163;
    }

    @XmlElement(name = "P216_4")
    public Integer getP2164() {
        return p2164;
    }

    public void setP2164(Integer p2164) {
        this.p2164 = p2164;
    }

    @XmlElement(name = "P217")
    public Integer getP217() {
        return p217;
    }

    public void setP217(Integer p217) {
        this.p217 = p217;
    }

    @XmlElement(name = "P218")
    public String getP218() {
        return p218;
    }

    public void setP218(String p218) {
        this.p218 = p218;
    }

    @XmlElement(name = "P219")
    public String getP219() {
        return p219;
    }

    public void setP219(String p219) {
        this.p219 = p219;
    }

    @XmlElement(name = "P219_D")
    public String getP219D() {
        return p219D;
    }

    public void setP219D(String p219D) {
        this.p219D = p219D;
    }

    @XmlElement(name = "P220")
    public String getP220() {
        return p220;
    }

    public void setP220(String p220) {
        this.p220 = p220;
    }

    @XmlElement(name = "P221")
    public String getP221() {
        return p221;
    }

    public void setP221(String p221) {
        this.p221 = p221;
    }

    @XmlElement(name = "P222_0")
    public Integer getP2220() {
        return p2220;
    }

    public void setP2220(Integer p2220) {
        this.p2220 = p2220;
    }

    @XmlElement(name = "P222_1")
    public Integer getP2221() {
        return p2221;
    }

    public void setP2221(Integer p2221) {
        this.p2221 = p2221;
    }

    @XmlElement(name = "P222_2")
    public Integer getP2222() {
        return p2222;
    }

    public void setP2222(Integer p2222) {
        this.p2222 = p2222;
    }

    @XmlElement(name = "P222_3")
    public Integer getP2223() {
        return p2223;
    }

    public void setP2223(Integer p2223) {
        this.p2223 = p2223;
    }

    @XmlElement(name = "P301")
    public Integer getP301() {
        return p301;
    }

    public void setP301(Integer p301) {
        this.p301 = p301;
    }

    @XmlElement(name = "P401")
    public Integer getP401() {
        return p401;
    }

    public void setP401(Integer p401) {
        this.p401 = p401;
    }

    @XmlElement(name = "P501_11")
    public String getP50111() {
        return p50111;
    }

    public void setP50111(String p50111) {
        this.p50111 = p50111;
    }

    @XmlElement(name = "P501_12")
    public String getP50112() {
        return p50112;
    }

    public void setP50112(String p50112) {
        this.p50112 = p50112;
    }

    @XmlElement(name = "P501_13")
    public String getP50113() {
        return p50113;
    }

    public void setP50113(String p50113) {
        this.p50113 = p50113;
    }

    public String getP50114() {
        return p50114;
    }

    @XmlElement(name = "P501_14")
    public void setP50114(String p50114) {
        this.p50114 = p50114;
    }

    @XmlElement(name = "P501_15")
    public String getP50115() {
        return p50115;
    }

    public void setP50115(String p50115) {
        this.p50115 = p50115;
    }

    @XmlElement(name = "P501_16")
    public String getP50116() {
        return p50116;
    }

    public void setP50116(String p50116) {
        this.p50116 = p50116;
    }

    @XmlElement(name = "P501_17")
    public String getP50117() {
        return p50117;
    }

    public void setP50117(String p50117) {
        this.p50117 = p50117;
    }

    @XmlElement(name = "P501_21")
    public String getP50121() {
        return p50121;
    }

    public void setP50121(String p50121) {
        this.p50121 = p50121;
    }

    @XmlElement(name = "P501_22")
    public String getP50122() {
        return p50122;
    }

    public void setP50122(String p50122) {
        this.p50122 = p50122;
    }

    @XmlElement(name = "P501_23")
    public String getP50123() {
        return p50123;
    }

    public void setP50123(String p50123) {
        this.p50123 = p50123;
    }

    @XmlElement(name = "P501_24")
    public String getP50124() {
        return p50124;
    }

    public void setP50124(String p50124) {
        this.p50124 = p50124;
    }

    @XmlElement(name = "P501_25")
    public String getP50125() {
        return p50125;
    }

    public void setP50125(String p50125) {
        this.p50125 = p50125;
    }
    @XmlElement(name = "P501_26")
    public String getP50126() {
        return p50126;
    }

    public void setP50126(String p50126) {
        this.p50126 = p50126;
    }

    @XmlElement(name = "P501_27")
    public String getP50127() {
        return p50127;
    }

    public void setP50127(String p50127) {
        this.p50127 = p50127;
    }

    @XmlElement(name = "P501_31")
    public String getP50131() {
        return p50131;
    }

    public void setP50131(String p50131) {
        this.p50131 = p50131;
    }

    @XmlElement(name = "P501_32")
    public String getP50132() {
        return p50132;
    }

    public void setP50132(String p50132) {
        this.p50132 = p50132;
    }

    @XmlElement(name = "P501_33")
    public String getP50133() {
        return p50133;
    }

    public void setP50133(String p50133) {
        this.p50133 = p50133;
    }

    @XmlElement(name = "P501_34")
    public String getP50134() {
        return p50134;
    }

    public void setP50134(String p50134) {
        this.p50134 = p50134;
    }

    @XmlElement(name = "P501_35")
    public String getP50135() {
        return p50135;
    }

    public void setP50135(String p50135) {
        this.p50135 = p50135;
    }

    @XmlElement(name = "P501_36")
    public String getP50136() {
        return p50136;
    }

    public void setP50136(String p50136) {
        this.p50136 = p50136;
    }

    @XmlElement(name = "P501_37")
    public String getP50137() {
        return p50137;
    }

    public void setP50137(String p50137) {
        this.p50137 = p50137;
    }

    @XmlElement(name = "P501_41")
    public String getP50141() {
        return p50141;
    }

    public void setP50141(String p50141) {
        this.p50141 = p50141;
    }

    @XmlElement(name = "P501_42")
    public String getP50142() {
        return p50142;
    }

    public void setP50142(String p50142) {
        this.p50142 = p50142;
    }

    @XmlElement(name = "P501_43")
    public String getP50143() {
        return p50143;
    }

    public void setP50143(String p50143) {
        this.p50143 = p50143;
    }

    @XmlElement(name = "P501_44")
    public String getP50144() {
        return p50144;
    }

    public void setP50144(String p50144) {
        this.p50144 = p50144;
    }

    @XmlElement(name = "P501_45")
    public String getP50145() {
        return p50145;
    }

    public void setP50145(String p50145) {
        this.p50145 = p50145;
    }

    @XmlElement(name = "P501_46")
    public String getP50146() {
        return p50146;
    }

    public void setP50146(String p50146) {
        this.p50146 = p50146;
    }

    @XmlElement(name = "P501_47")
    public String getP50147() {
        return p50147;
    }

    public void setP50147(String p50147) {
        this.p50147 = p50147;
    }

    @XmlElement(name = "P501_51")
    public String getP50151() {
        return p50151;
    }

    public void setP50151(String p50151) {
        this.p50151 = p50151;
    }

    @XmlElement(name = "P501_52")
    public String getP50152() {
        return p50152;
    }

    public void setP50152(String p50152) {
        this.p50152 = p50152;
    }

    @XmlElement(name = "P501_53")
    public String getP50153() {
        return p50153;
    }

    public void setP50153(String p50153) {
        this.p50153 = p50153;
    }

    @XmlElement(name = "P501_54")
    public String getP50154() {
        return p50154;
    }

    public void setP50154(String p50154) {
        this.p50154 = p50154;
    }

    @XmlElement(name = "P501_55")
    public String getP50155() {
        return p50155;
    }

    public void setP50155(String p50155) {
        this.p50155 = p50155;
    }

    @XmlElement(name = "P501_56")
    public String getP50156() {
        return p50156;
    }

    public void setP50156(String p50156) {
        this.p50156 = p50156;
    }

    @XmlElement(name = "P501_57")
    public String getP50157() {
        return p50157;
    }

    public void setP50157(String p50157) {
        this.p50157 = p50157;
    }

    @XmlElement(name = "P501_61")
    public String getP50161() {
        return p50161;
    }

    public void setP50161(String p50161) {
        this.p50161 = p50161;
    }

    @XmlElement(name = "P501_62")
    public String getP50162() {
        return p50162;
    }

    public void setP50162(String p50162) {
        this.p50162 = p50162;
    }

    @XmlElement(name = "P501_63")
    public String getP50163() {
        return p50163;
    }

    public void setP50163(String p50163) {
        this.p50163 = p50163;
    }

    @XmlElement(name = "P501_64")
    public String getP50164() {
        return p50164;
    }

    public void setP50164(String p50164) {
        this.p50164 = p50164;
    }

    @XmlElement(name = "P501_65")
    public String getP50165() {
        return p50165;
    }

    public void setP50165(String p50165) {
        this.p50165 = p50165;
    }

    @XmlElement(name = "P501_66")
    public String getP50166() {
        return p50166;
    }

    public void setP50166(String p50166) {
        this.p50166 = p50166;
    }

    @XmlElement(name = "P501_67")
    public String getP50167() {
        return p50167;
    }

    public void setP50167(String p50167) {
        this.p50167 = p50167;
    }

    @XmlElement(name = "P501_71")
    public String getP50171() {
        return p50171;
    }

    public void setP50171(String p50171) {
        this.p50171 = p50171;
    }

    @XmlElement(name = "P501_72")
    public String getP50172() {
        return p50172;
    }

    public void setP50172(String p50172) {
        this.p50172 = p50172;
    }

    @XmlElement(name = "P501_73")
    public String getP50173() {
        return p50173;
    }

    public void setP50173(String p50173) {
        this.p50173 = p50173;
    }

    @XmlElement(name = "P501_74")
    public String getP50174() {
        return p50174;
    }

    public void setP50174(String p50174) {
        this.p50174 = p50174;
    }

    @XmlElement(name = "P501_75")
    public String getP50175() {
        return p50175;
    }

    public void setP50175(String p50175) {
        this.p50175 = p50175;
    }

    @XmlElement(name = "P501_76")
    public String getP50176() {
        return p50176;
    }

    public void setP50176(String p50176) {
        this.p50176 = p50176;
    }

    @XmlElement(name = "P501_77")
    public String getP50177() {
        return p50177;
    }

    public void setP50177(String p50177) {
        this.p50177 = p50177;
    }

    @XmlElement(name = "P601_1")
    public String getP6011() {
        return p6011;
    }

    public void setP6011(String p6011) {
        this.p6011 = p6011;
    }

    @XmlElement(name = "P601_2")
    public String getP6012() {
        return p6012;
    }

    public void setP6012(String p6012) {
        this.p6012 = p6012;
    }

    @XmlElement(name = "P601_3")
    public String getP6013() {
        return p6013;
    }

    public void setP6013(String p6013) {
        this.p6013 = p6013;
    }

    @XmlElement(name = "P601_4")
    public String getP6014() {
        return p6014;
    }

    public void setP6014(String p6014) {
        this.p6014 = p6014;
    }

    @XmlElement(name = "P601_51")
    public String getP60151() {
        return p60151;
    }

    public void setP60151(String p60151) {
        this.p60151 = p60151;
    }

    @XmlElement(name = "P601_52")
    public Integer getP60152() {
        return p60152;
    }

    public void setP60152(Integer p60152) {
        this.p60152 = p60152;
    }

    @XmlElement(name = "P601_6")
    public String getP6016() {
        return p6016;
    }

    public void setP6016(String p6016) {
        this.p6016 = p6016;
    }

    @XmlElement(name = "P601_7")
    public String getP6017() {
        return p6017;
    }

    public void setP6017(String p6017) {
        this.p6017 = p6017;
    }

    @XmlElement(name = "P601_8")
    public String getP6018() {
        return p6018;
    }

    public void setP6018(String p6018) {
        this.p6018 = p6018;
    }

    @XmlElement(name = "P602_11")
    public String getP60211() {
        return p60211;
    }

    public void setP60211(String p60211) {
        this.p60211 = p60211;
    }

    @XmlElement(name = "P602_12")
    public String getP60212() {
        return p60212;
    }

    public void setP60212(String p60212) {
        this.p60212 = p60212;
    }

    @XmlElement(name = "P602_13")
    public String getP60213() {
        return p60213;
    }

    public void setP60213(String p60213) {
        this.p60213 = p60213;
    }

    @XmlElement(name = "P602_14")
    public String getP60214() {
        return p60214;
    }

    public void setP60214(String p60214) {
        this.p60214 = p60214;
    }

    @XmlElement(name = "P602_15")
    public String getP60215() {
        return p60215;
    }

    public void setP60215(String p60215) {
        this.p60215 = p60215;
    }

    @XmlElement(name = "P602_16")
    public String getP60216() {
        return p60216;
    }

    public void setP60216(String p60216) {
        this.p60216 = p60216;
    }

    @XmlElement(name = "P602_21")
    public String getP60221() {
        return p60221;
    }

    public void setP60221(String p60221) {
        this.p60221 = p60221;
    }

    @XmlElement(name = "P602_22")
    public String getP60222() {
        return p60222;
    }

    public void setP60222(String p60222) {
        this.p60222 = p60222;
    }

    @XmlElement(name = "P602_23")
    public String getP60223() {
        return p60223;
    }

    public void setP60223(String p60223) {
        this.p60223 = p60223;
    }

    @XmlElement(name = "P602_24")
    public String getP60224() {
        return p60224;
    }

    public void setP60224(String p60224) {
        this.p60224 = p60224;
    }

    @XmlElement(name = "P602_25")
    public String getP60225() {
        return p60225;
    }

    public void setP60225(String p60225) {
        this.p60225 = p60225;
    }

    @XmlElement(name = "P602_26")
    public String getP60226() {
        return p60226;
    }

    public void setP60226(String p60226) {
        this.p60226 = p60226;
    }

    @XmlElement(name = "P603_11")
    public Integer getP60311() {
        return p60311;
    }

    public void setP60311(Integer p60311) {
        this.p60311 = p60311;
    }

    @XmlElement(name = "P603_12")
    public Integer getP60312() {
        return p60312;
    }

    public void setP60312(Integer p60312) {
        this.p60312 = p60312;
    }

    @XmlElement(name = "P603_13")
    public Integer getP60313() {
        return p60313;
    }

    public void setP60313(Integer p60313) {
        this.p60313 = p60313;
    }

    @XmlElement(name = "P603_21")
    public Integer getP60321() {
        return p60321;
    }

    public void setP60321(Integer p60321) {
        this.p60321 = p60321;
    }

    @XmlElement(name = "P603_22")
    public Integer getP60322() {
        return p60322;
    }

    public void setP60322(Integer p60322) {
        this.p60322 = p60322;
    }

    @XmlElement(name = "P603_23")
    public Integer getP60323() {
        return p60323;
    }

    public void setP60323(Integer p60323) {
        this.p60323 = p60323;
    }

    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "NOM_DIR")
    public String getNomDir() {
        return nomDir;
    }

    public void setNomDir(String nomDir) {
        this.nomDir = nomDir;
    }

    @XmlElement(name = "APE_DIR")
    public String getApeDir() {
        return apeDir;
    }

    public void setApeDir(String apeDir) {
        this.apeDir = apeDir;
    }

    @XmlElement(name = "CONDICION_DIR")
    public String getCondicionDir() {
        return condicionDir;
    }

    public void setCondicionDir(String condicionDir) {
        this.condicionDir = condicionDir;
    }

    @XmlElement(name = "DNI_DIR")
    public String getDniDir() {
        return dniDir;
    }

    public void setDniDir(String dniDir) {
        this.dniDir = dniDir;
    }

    @XmlElement(name = "EMAIL_DIR")
    public String getEmailDir() {
        return emailDir;
    }

    public void setEmailDir(String emailDir) {
        this.emailDir = emailDir;
    }

    @XmlElement(name = "REC_CED_DIA")
    public String getRecCedDia() {
        return recCedDia;
    }

    public void setRecCedDia(String recCedDia) {
        this.recCedDia = recCedDia;
    }

    @XmlElement(name = "REC_CED_MES")
    public String getRecCedMes() {
        return recCedMes;
    }

    public void setRecCedMes(String recCedMes) {
        this.recCedMes = recCedMes;
    }

    @XmlElement(name = "CUL_CED_DIA")
    public String getCulCedDia() {
        return culCedDia;
    }

    public void setCulCedDia(String culCedDia) {
        this.culCedDia = culCedDia;
    }

    @XmlElement(name = "CUL_CED_MES")
    public String getCulCedMes() {
        return culCedMes;
    }

    public void setCulCedMes(String culCedMes) {
        this.culCedMes = culCedMes;
    }

    @XmlElement(name = "EMAIL_INST")
    public String getEmailInst() {
        return emailInst;
    }

    public void setEmailInst(String emailInst) {
        this.emailInst = emailInst;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    /*
    @XmlElement(name="SEC300")
    public List<Local2011Sec300> getLocal2011Sec300Collection() {
        return local2011Sec300Collection;
    }

    public void setLocal2011Sec300Collection(List<Local2011Sec300> local2011Sec300Collection) {
        this.local2011Sec300Collection = local2011Sec300Collection;
    }

    @XmlElement(name="SEC400")
    public List<Local2011Sec400> getLocal2011Sec400Collection() {
        return local2011Sec400Collection;
    }

    public void setLocal2011Sec400Collection(List<Local2011Sec400> local2011Sec400Collection) {
        this.local2011Sec400Collection = local2011Sec400Collection;
    }

    @XmlElement(name="SEC104")
    public List<Local2011Sec104> getLocal2011Sec104Collection() {
        return local2011Sec104Collection;
    }

    public void setLocal2011Sec104Collection(List<Local2011Sec104> local2011Sec104Collection) {
        this.local2011Sec104Collection = local2011Sec104Collection;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2011Cabecera)) {
            return false;
        }
        Local2011Cabecera other = (Local2011Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + idEnvio + "]";
    }

    /**
     * @return the ultimo
     */
    public Boolean getUltimo() {
        return ultimo;
    }

    /**
     * @param ultimo the ultimo to set
     */
    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    
    public void setToken(long token) {
        this.token = token;
    }

    /**
     * @return the fechaenvio
     */
    public Date getFechaenvio() {
        return fechaenvio;
    }

    /**
     * @param fechaenvio the fechaenvio to set
     */
    public void setFechaenvio(Date fechaenvio) {
        this.fechaenvio = fechaenvio;
    }

    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    /**
     * @param estadoRpt the estadoRpt to set
     */
    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    /**
     * @return the codigelPadron
     */
    public String getCodigelPadron() {
        return codigelPadron;
    }

    /**
     * @param codigelPadron the codigelPadron to set
     */
    public void setCodigelPadron(String codigelPadron) {
        this.codigelPadron = codigelPadron;
    }

}
