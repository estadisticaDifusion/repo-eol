/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CMOLINA
 */
@Entity
@Table(name = "eol_indicador_variable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EolIndicadorVariable.findAll", query = "SELECT e FROM EolIndicadorVariable e"),
    @NamedQuery(name = "EolIndicadorVariable.findByIdVariable", query = "SELECT e FROM EolIndicadorVariable e WHERE e.idVariable = :idVariable"),
    @NamedQuery(name = "EolIndicadorVariable.findByNombVar", query = "SELECT e FROM EolIndicadorVariable e WHERE e.nombVar = :nombVar"),
    @NamedQuery(name = "EolIndicadorVariable.findByDescripcion", query = "SELECT e FROM EolIndicadorVariable e WHERE e.descripcion = :descripcion")})
public class EolIndicadorVariable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_VARIABLE")
    private Long idVariable;
    @Size(max = 50)
    @Column(name = "NOMB_VAR")
    private String nombVar;
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    public EolIndicadorVariable() {
    }

    public EolIndicadorVariable(Long idVariable) {
        this.idVariable = idVariable;
    }

    public Long getIdVariable() {
        return idVariable;
    }

    public void setIdVariable(Long idVariable) {
        this.idVariable = idVariable;
    }

    public String getNombVar() {
        return nombVar;
    }

    public void setNombVar(String nombVar) {
        this.nombVar = nombVar;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVariable != null ? idVariable.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EolIndicadorVariable)) {
            return false;
        }
        EolIndicadorVariable other = (EolIndicadorVariable) object;
        if ((this.idVariable == null && other.idVariable != null) || (this.idVariable != null && !this.idVariable.equals(other.idVariable))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.EolIndicadorVariable[ idVariable=" + idVariable + " ]";
    }
    
}
