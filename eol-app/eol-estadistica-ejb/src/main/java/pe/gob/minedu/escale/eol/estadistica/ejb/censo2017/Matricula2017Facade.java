package pe.gob.minedu.escale.eol.estadistica.ejb.censo2017;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Matricula2017Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class Matricula2017Facade extends AbstractFacade<Matricula2017Cabecera> {

	/*
	 * Se modifico la nueva funcion
	 * CedulaMatricula2017Resources.verificarPlazoEntrega(String tipoDocumento,
	 * String anio) public static final String CEDULA_MATRICULA = "MATRICULA";
	 */
	public static final String CEDULA_MATRICULA = "CENSO-MATRICULA";
	static final Logger LOGGER = Logger.getLogger(Matricula2017Facade.class.getName());
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03AP = "c03ap";
	public final static String CEDULA_03AS = "c03as";
	public final static String CEDULA_04AI = "c04ai";
	public final static String CEDULA_04AA = "c04aa";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08AI = "c08ai";
	public final static String CEDULA_08AP = "c08ap";
	public final static String CEDULA_09A = "c09a";

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Matricula2017Facade() {
		super(Matricula2017Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Matricula2017Cabecera cedula) {
		String sql = "UPDATE Matricula2017Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroced=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroced());
		query.executeUpdate();

		/*
		 * if (cedula.getDetalleMatricula() != null) { Set<String> keys =
		 * cedula.getDetalleMatricula().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2017Matricula detalle = cedula.getDetalleMatricula().get(key);
		 * detalle.setMatricula2017Cabecera(cedula); List<Matricula2017MatriculaFila>
		 * filas = detalle.getMatricula2017MatriculaFilaList(); for
		 * (Matricula2017MatriculaFila fila : filas) {
		 * fila.setMatricula2017Matricula(detalle); } } }
		 * 
		 * if (cedula.getDetalleSeccion() != null) { Set<String> keys =
		 * cedula.getDetalleSeccion().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2017Seccion detalle = cedula.getDetalleSeccion().get(key);
		 * detalle.setMatricula2017Cabecera(cedula); List<Matricula2017SeccionFila>
		 * filas = detalle.getMatricula2017SeccionFilaList(); for
		 * (Matricula2017SeccionFila fila : filas) {
		 * fila.setMatricula2017Seccion(detalle); } } }
		 * 
		 * if (cedula.getDetalleRecursos() != null) { Set<String> keys =
		 * cedula.getDetalleRecursos().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2017Recursos detalle = cedula.getDetalleRecursos().get(key);
		 * detalle.setMatricula2017Cabecera(cedula); List<Matricula2017RecursosFila>
		 * filas = detalle.getMatricula2017RecursosFilaList(); for
		 * (Matricula2017RecursosFila fila : filas) {
		 * fila.setMatricula2017Recursos(detalle); } } }
		 * 
		 * if (cedula.getMatricula2017PersonalList() != null) { for
		 * (Matricula2017Personal per : cedula.getMatricula2017PersonalList()) {
		 * per.setMatricula2017Cabecera(cedula); } } if
		 * (cedula.getMatricula2017LocalpronoeiList() != null) { for
		 * (Matricula2017Localpronoei loc : cedula.getMatricula2017LocalpronoeiList()) {
		 * loc.setMatricula2017Cabecera(cedula); } } if
		 * (cedula.getMatricula2017CarrerasList() != null) { for (Matricula2017Carreras
		 * carr : cedula.getMatricula2017CarrerasList()) {
		 * carr.setMatricula2017Cabecera(cedula); } } if
		 * (cedula.getMatricula2017SaaneeList() != null) { for (Matricula2017Saanee saan
		 * : cedula.getMatricula2017SaaneeList()) {
		 * saan.setMatricula2017Cabecera(cedula); } } if
		 * (cedula.getMatricula2017EbaList() != null) { for (Matricula2017Eba eba :
		 * cedula.getMatricula2017EbaList()) { eba.setMatricula2017Cabecera(cedula); } }
		 */
		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

	public void createCabecera(Matricula2017Cabecera cedula) {
		String sql = "UPDATE Matricula2017Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroced=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroced());
		query.executeUpdate();

		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		// em.flush();
	}

}
