package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "local2016_sec200")
public class Local2016Sec200 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "PREGUNTA")
    private String pregunta;
    @Column(name = "ORDEN")
    private Short orden;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL")
    private Integer total;
    @Column(name = "DAT01")
    private Integer dat01;
    @Column(name = "DAT02")
    private Integer dat02;
    @Column(name = "DAT03")
    private Integer dat03;
    @Column(name = "DAT04")
    private Integer dat04;
    @Column(name = "DAT05")
    private Integer dat05;
    @Column(name = "DAT06")
    private Integer dat06;
    @Column(name = "DAT07")
    private Integer dat07;
    @Column(name = "DAT08")
    private Integer dat08;
    @Column(name = "DAT09")
    private Integer dat09;
    @Column(name = "CHK01")
    private String chk01;
    @Column(name = "CHK02")
    private String chk02;
    @Column(name = "CHK03")
    private String chk03;
    @Column(name = "CHK04")
    private String chk04;
    @Column(name = "CHK05")
    private String chk05;
    @Column(name = "CHK06")
    private String chk06;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2016Cabecera local2016Cabecera;

    public Local2016Sec200() {
    }

    public Local2016Sec200(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "PREGUNTA")
    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @XmlElement(name = "ORDEN")
    public Short getOrden() {
        return orden;
    }

    public void setOrden(Short orden) {
        this.orden = orden;
    }

    @XmlElement(name = "DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name = "TOTAL")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
    @XmlElement(name="DAT01")
    public Integer getDat01() {
        return dat01;
    }

    public void setDat01(Integer dat01) {
        this.dat01 = dat01;
    }
    @XmlElement(name="DAT02")
    public Integer getDat02() {
        return dat02;
    }

    public void setDat02(Integer dat02) {
        this.dat02 = dat02;
    }
    @XmlElement(name="DAT03")
    public Integer getDat03() {
        return dat03;
    }

    public void setDat03(Integer dat03) {
        this.dat03 = dat03;
    }
    @XmlElement(name="DAT04")
    public Integer getDat04() {
        return dat04;
    }

    public void setDat04(Integer dat04) {
        this.dat04 = dat04;
    }
    @XmlElement(name="DAT05")
    public Integer getDat05() {
        return dat05;
    }

    public void setDat05(Integer dat05) {
        this.dat05 = dat05;
    }
    @XmlElement(name="DAT06")
    public Integer getDat06() {
        return dat06;
    }

    public void setDat06(Integer dat06) {
        this.dat06 = dat06;
    }
    @XmlElement(name="DAT07")
    public Integer getDat07() {
        return dat07;
    }

    public void setDat07(Integer dat07) {
        this.dat07 = dat07;
    }
    @XmlElement(name="DAT08")
    public Integer getDat08() {
        return dat08;
    }

    public void setDat08(Integer dat08) {
        this.dat08 = dat08;
    }
    @XmlElement(name="DAT09")
    public Integer getDat09() {
        return dat09;
    }

    public void setDat09(Integer dat09) {
        this.dat09 = dat09;
    }
    @XmlElement(name="CHK01")
    public String getChk01() {
        return chk01;
    }

    public void setChk01(String chk01) {
        this.chk01 = chk01;
    }
    @XmlElement(name="CHK02")
    public String getChk02() {
        return chk02;
    }

    public void setChk02(String chk02) {
        this.chk02 = chk02;
    }
    @XmlElement(name="CHK03")
    public String getChk03() {
        return chk03;
    }

    public void setChk03(String chk03) {
        this.chk03 = chk03;
    }
    @XmlElement(name="CHK04")
    public String getChk04() {
        return chk04;
    }

    public void setChk04(String chk04) {
        this.chk04 = chk04;
    }
    @XmlElement(name="CHK05")
    public String getChk05() {
        return chk05;
    }

    public void setChk05(String chk05) {
        this.chk05 = chk05;
    }
    @XmlElement(name="CHK06")
    public String getChk06() {
        return chk06;
    }

    public void setChk06(String chk06) {
        this.chk06 = chk06;
    }
    @XmlTransient
    public Local2016Cabecera getLocal2016Cabecera() {
        return local2016Cabecera;
    }

    public void setLocal2016Cabecera(Local2016Cabecera local2016Cabecera) {
        this.local2016Cabecera = local2016Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2016Sec200)) {
            return false;
        }
        Local2016Sec200 other = (Local2016Sec200) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Local2016Sec200[idEnvio=" + idEnvio + "]";
    }

}
