/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "resultado2018_detalle", catalog = "estadistica", schema = "")
public class Resultado2018Detalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Basic(optional = false)
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
    //@Basic(optional = false)
    //@Column(name = "ULTIMODET")
    @Transient
    private boolean ultimodet;

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Resultado2018Cabecera cedula;
  /*  
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detalle", fetch = FetchType.EAGER)
    private List<Resultado2018Fila> filas;
*/
    public Resultado2018Detalle() {
    }

    public Resultado2018Detalle(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Resultado2018Detalle(Long idEnvio, String cuadro, boolean ultimodet) {
        this.idEnvio = idEnvio;
        this.cuadro = cuadro;
        //this.ultimodet = ultimodet;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlElement(name = "ULTIMODET")
    public boolean getUltimodet() {
        return ultimodet;
    }

    public void setUltimodet(boolean ultimodet) {
        this.ultimodet = ultimodet;
    }

    @XmlTransient
    public Resultado2018Cabecera getCedula() {
        return cedula;
    }
    
    public void setCedula(Resultado2018Cabecera cedula) {
        this.cedula = cedula;
    }
/*
    @XmlElement(name="FILAS")
    public List<Resultado2018Fila> getFilas() {
        return filas;
    }

    public void setFilas(List<Resultado2018Fila> filas) {
        this.filas = filas;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado2018Detalle)) {
            return false;
        }
        Resultado2018Detalle other = (Resultado2018Detalle) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Resultado2018Detalle[idEnvio=" + idEnvio + "]";
    }

}
