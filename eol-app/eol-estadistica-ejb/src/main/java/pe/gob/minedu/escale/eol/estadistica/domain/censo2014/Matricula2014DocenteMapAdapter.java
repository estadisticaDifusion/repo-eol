/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Matricula2014DocenteMapAdapter.Matricula2014DocenteList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2014DocenteMapAdapter extends XmlAdapter<Matricula2014DocenteList, Map<String, Matricula2014Docente>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2014DocenteMapAdapter.class.getName());

    static class Matricula2014DocenteList {

        private List<Matricula2014Docente> detalle;

        private Matricula2014DocenteList(ArrayList<Matricula2014Docente> lista) {
            detalle = lista;
        }

        public Matricula2014DocenteList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2014Docente> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2014Docente> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2014Docente> unmarshal(Matricula2014DocenteList v) throws Exception {

        Map<String, Matricula2014Docente> map = new HashMap<String, Matricula2014Docente>();
        for (Matricula2014Docente detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2014DocenteList marshal(Map<String, Matricula2014Docente> v) throws Exception {
        if(v==null)return null;

        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2014Docente> lista = new ArrayList<Matricula2014Docente>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2014Docente $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2014DocenteList list = new Matricula2014DocenteList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
