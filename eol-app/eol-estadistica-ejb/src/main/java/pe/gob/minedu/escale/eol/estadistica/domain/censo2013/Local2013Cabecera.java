/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulaLocal")
@Entity
@Table(name = "local2013_cabecera")
public class Local2013Cabecera implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "TIPOVIA")
    private String tipovia;
    @Column(name = "NOMBVIA")
    private String nombvia;
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "MANZANA")
    private String manzana;
    @Column(name = "LOTE")
    private String lote;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "NOMLUGAR")
    private String nomlugar;
    @Column(name = "ETAPA")
    private String etapa;
    @Column(name = "SECTOR")
    private String sector;
    @Column(name = "ZONA")
    private String zona;
    @Column(name = "OTROS")
    private String otros;
    @Column(name = "CATEG_CP")
    private String categCp;
    @Column(name = "CEN_POB")
    private String cenPob;
    @Column(name = "CODCCPP")
    private String codccpp;
    @Column(name = "REFERENCIA")
    private String referencia;
    @Column(name = "CODGEO")
    private String codgeo;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "COD_AREA")
    private String codArea;
    @Column(name = "COND_TEN")
    private String condTen;
    @Column(name = "PROPLOCAL")
    private String proplocal;
    @Column(name = "GESTION")
    private String gestion;
    @Column(name = "P201")
    private String p201;
    @Column(name = "P301")
    private Integer p301;
    @Column(name = "P401")
    private Integer p401;
    @Column(name = "P202_1")
    private String p2021;
    @Column(name = "P202_2")
    private String p2022;
    @Column(name = "P202_3")
    private String p2023;
    @Column(name = "P202_4")
    private String p2024;
    @Column(name = "P202_5")
    private String p2025;
    @Column(name = "P203")
    private String p203;
    @Column(name = "P204")
    private String p204;
    @Column(name = "P205")
    private Integer p205;
    @Column(name = "P206")
    private String p206;
    @Column(name = "P207")
    private String p207;
    @Column(name = "P208")
    private String p208;
    @Column(name = "P209_0TOT")
    private Integer p2090tot;
    @Column(name = "P209_0DSK")
    private Integer p2090dsk;
    @Column(name = "P209_0LXO")
    private Integer p2090lxo;
    @Column(name = "P209_0OTR")
    private Integer p2090otr;
    @Column(name = "P209_1TOT")
    private Integer p2091tot;
    @Column(name = "P209_1DSK")
    private Integer p2091dsk;
    @Column(name = "P209_1LXO")
    private Integer p2091lxo;
    @Column(name = "P209_1OTR")
    private Integer p2091otr;
    @Column(name = "P209_2TOT")
    private Integer p2092tot;
    @Column(name = "P209_2DSK")
    private Integer p2092dsk;
    @Column(name = "P209_2LXO")
    private Integer p2092lxo;
    @Column(name = "P209_2OTR")
    private Integer p2092otr;
    @Column(name = "P209_3TOT")
    private Integer p2093tot;
    @Column(name = "P209_3DSK")
    private Integer p2093dsk;
    @Column(name = "P209_3LXO")
    private Integer p2093lxo;
    @Column(name = "P209_3OTR")
    private Integer p2093otr;
    @Column(name = "P210_0TOT")
    private Integer p2100tot;
    @Column(name = "P210_0CNX")
    private Integer p2100cnx;
    @Column(name = "P210_0NCNX")
    private Integer p2100ncnx;
    @Column(name = "P210_1TOT")
    private Integer p2101tot;
    @Column(name = "P210_1CNX")
    private Integer p2101cnx;
    @Column(name = "P210_1NCNX")
    private Integer p2101ncnx;
    @Column(name = "P210_2TOT")
    private Integer p2102tot;
    @Column(name = "P210_2CNX")
    private Integer p2102cnx;
    @Column(name = "P210_2NCNX")
    private Integer p2102ncnx;
    @Column(name = "P210_3TOT")
    private Integer p2103tot;
    @Column(name = "P210_3CNX")
    private Integer p2103cnx;
    @Column(name = "P210_3NCNX")
    private Integer p2103ncnx;
    @Column(name = "P211_0CA")
    private Integer p2110ca;
    @Column(name = "P211_0SI")
    private Integer p2110si;
    @Column(name = "P211_0ME")
    private Integer p2110me;
    @Column(name = "P211_1CA")
    private Integer p2111ca;
    @Column(name = "P211_1SI")
    private Integer p2111si;
    @Column(name = "P211_1ME")
    private Integer p2111me;
    @Column(name = "P211_2CA")
    private Integer p2112ca;
    @Column(name = "P211_2SI")
    private Integer p2112si;
    @Column(name = "P211_2ME")
    private Integer p2112me;
    @Column(name = "P211_3CA")
    private Integer p2113ca;
    @Column(name = "P211_3SI")
    private Integer p2113si;
    @Column(name = "P211_3ME")
    private Integer p2113me;
    @Column(name = "P212_INI")
    private Integer p212Ini;
    @Column(name = "P212_1Y2P")
    private Integer p2121y2p;
    @Column(name = "P212_3A6P")
    private Integer p2123a6p;
    @Column(name = "P212_SCCS")
    private Integer p212Sccs;
    @Column(name = "P213")
    private Integer p213;
    @Column(name = "P214")
    private String p214;
    @Column(name = "P215")
    private String p215;
    @Column(name = "P215_esp")
    private String p215esp;
    @Column(name = "P216")
    private String p216;
    @Column(name = "P217")
    private String p217;
    @Column(name = "P218_01")
    private Integer p21801;
    @Column(name = "P218_02")
    private Integer p21802;
    @Column(name = "P218_03")
    private Integer p21803;
    @Column(name = "P218_04")
    private Integer p21804;
    @Column(name = "P218_05")
    private Integer p21805;
    @Column(name = "P218_11")
    private Integer p21811;
    @Column(name = "P218_12")
    private Integer p21812;
    @Column(name = "P218_13")
    private Integer p21813;
    @Column(name = "P218_14")
    private Integer p21814;
    @Column(name = "P218_15")
    private Integer p21815;
    @Column(name = "P218_21")
    private Integer p21821;
    @Column(name = "P218_22")
    private Integer p21822;
    @Column(name = "P218_23")
    private Integer p21823;
    @Column(name = "P218_24")
    private Integer p21824;
    @Column(name = "P218_25")
    private Integer p21825;
    @Column(name = "P218_31")
    private Integer p21831;
    @Column(name = "P218_32")
    private Integer p21832;
    @Column(name = "P218_33")
    private Integer p21833;
    @Column(name = "P218_34")
    private Integer p21834;
    @Column(name = "P218_35")
    private Integer p21835;
    @Column(name = "P219")
    private Integer p219;
    @Column(name = "P220")
    private String p220;    
    @Column(name = "P601_1")
    private String p6011;
    @Column(name = "P601_2")
    private String p6012;
    @Column(name = "P601_3")
    private String p6013;
    @Column(name = "P601_4")
    private String p6014;
    @Column(name = "P601_5")
    private String p6015;
    @Column(name = "P601_61")
    private String p60161;
    @Column(name = "P601_62")
    private String p60162;
    @Column(name = "P601_7")
    private String p6017;
    @Column(name = "P601_8")
    private String p6018;
    @Column(name = "P601_9")
    private String p6019;
    @Column(name = "P601_10")
    private String p60110;
    @Column(name = "P602_11")
    private String p60211;
    @Column(name = "P602_12")
    private String p60212;
    @Column(name = "P602_13")
    private String p60213;
    @Column(name = "P602_14")
    private String p60214;
    @Column(name = "P602_15")
    private String p60215;
    @Column(name = "P602_16")
    private String p60216;
    @Column(name = "P602_21")
    private String p60221;
    @Column(name = "P602_22")
    private String p60222;
    @Column(name = "P602_23")
    private String p60223;
    @Column(name = "P602_24")
    private String p60224;
    @Column(name = "P602_25")
    private String p60225;
    @Column(name = "P602_26")
    private String p60226;
    @Column(name = "P603_1D")
    private Integer p6031d;
    @Column(name = "P603_1H")
    private Integer p6031h;
    @Column(name = "P603_1M")
    private Integer p6031m;
    @Column(name = "P603_2D")
    private Integer p6032d;
    @Column(name = "P603_2H")
    private Integer p6032h;
    @Column(name = "P603_2M")
    private Integer p6032m;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "APE_DIR")
    private String apeDir;
    @Column(name = "NOM_DIR")
    private String nomDir;
    @Column(name = "SITUA_DIR")
    private String situaDir;
    @Column(name = "DNI_DIR")
    private String dniDir;
    @Column(name = "EMAIL_DIR")
    private String emailDir;
    @Column(name = "REC_CEDD")
    private String recCedd;
    @Column(name = "REC_CEDM")
    private String recCedm;
    @Column(name = "CUL_CEDD")
    private String culCedd;
    @Column(name = "CUL_CEDM")
    private String culCedm;
    @Column(name = "P701_10")
    private Integer p70110;
    @Column(name = "P701_11")
    private String p70111;
    @Column(name = "P701_12")
    private String p70112;
    @Column(name = "P701_13")
    private Integer p70113;
    @Column(name = "P701_14")
    private String p70114;
    @Column(name = "P701_15")
    private String p70115;
    @Column(name = "P701_16")
    private Integer p70116;
    @Column(name = "P701_17")
    private String p70117;
    @Column(name = "P701_18")
    private String p70118;
    @Column(name = "P701_19")
    private String p70119;
    @Column(name = "P701_1ESP")
    private String p7011esp;
    @Column(name = "P701_20")
    private Integer p70120;
    @Column(name = "P701_21")
    private String p70121;
    @Column(name = "P701_22")
    private String p70122;
    @Column(name = "P701_23")
    private Integer p70123;
    @Column(name = "P701_24")
    private String p70124;
    @Column(name = "P701_25")
    private String p70125;
    @Column(name = "P701_26")
    private Integer p70126;
    @Column(name = "P701_27")
    private String p70127;
    @Column(name = "P701_28")
    private String p70128;
    @Column(name = "P701_29")
    private String p70129;
    @Column(name = "P701_2ESP")
    private String p7012esp;
    @Column(name = "P701_30")
    private Integer p70130;
    @Column(name = "P701_31")
    private String p70131;
    @Column(name = "P701_32")
    private String p70132;
    @Column(name = "P701_33")
    private Integer p70133;
    @Column(name = "P701_34")
    private String p70134;
    @Column(name = "P701_35")
    private String p70135;
    @Column(name = "P701_36")
    private Integer p70136;
    @Column(name = "P701_37")
    private String p70137;
    @Column(name = "P701_38")
    private String p70138;
    @Column(name = "P701_39")
    private String p70139;
    @Column(name = "P701_3ESP")
    private String p7013esp;
    @Column(name = "P701_4VTA")
    private Integer p7014vta;
    @Column(name = "P7021")
    private String p7021;
    @Column(name = "P7022")
    private String p7022;
    @Column(name = "P7023_1")
    private String p70231;
    @Column(name = "P7023_2")
    private String p70232;
    @Column(name = "P7024")
    private String p7024;
    @Column(name = "P7025")
    private String p7025;
    @Column(name = "P7026_1")
    private String p70261;
    @Column(name = "P7026_2")
    private String p70262;
    
    @Column(name = "P703")
    private String p703;
    @Column(name = "P703_1")
    private String p7031;
    @Column(name = "P703_2")
    private String p7032;
    @Column(name = "P703_3")
    private String p7033;
    @Column(name = "P703_4")
    private String p7034;
    @Column(name = "P703_5")
    private String p7035;
    @Column(name = "P703_6")
    private String p7036;
    @Column(name = "P703_7")
    private String p7037;
    @Column(name = "P703_8")
    private String p7038;
    @Column(name = "P703_9")
    private String p7039;
    @Column(name = "P703_10")
    private String p70310;
    @Column(name = "P703_11")
    private String p70311;
    @Column(name = "P703_12")
    private String p70312;
    @Column(name = "P703_13")
    private String p70313;
    @Column(name = "P703_14")
    private String p70314;
    @Column(name = "P703_15")
    private String p70315;
    @Column(name = "P703_16")
    private String p70316;
    @Column(name = "P703_17")
    private String p70317;
    @Column(name = "P703_18")
    private String p70318;
    @Column(name = "P703_18E")
    private String p70318e;
    @Column(name = "P704")
    private String p704;
    @Column(name = "P704_ESP")
    private String p704esp;
    @Column(name = "P7051")
    private String p7051;
    @Column(name = "P7052_11")
    private String p705211;
    @Column(name = "P7052_12")
    private String p705212;
    @Column(name = "P7052_13")
    private String p705213;
    @Column(name = "P7052_14")
    private String p705214;
    @Column(name = "P7052_21")
    private String p705221;
    @Column(name = "P7052_22")
    private String p705222;
    @Column(name = "P7052_23")
    private String p705223;
    @Column(name = "P7052_24")
    private String p705224;
    @Column(name = "P7052_31")
    private String p705231;
    @Column(name = "P7052_32")
    private String p705232;
    @Column(name = "P7052_33")
    private String p705233;
    @Column(name = "P7052_34")
    private String p705234;
    @Column(name = "P7052_41")
    private String p705241;
    @Column(name = "P7052_42")
    private String p705242;
    @Column(name = "P7052_43")
    private String p705243;
    @Column(name = "P7052_44")
    private String p705244;
    @Column(name = "P7052_51")
    private String p705251;
    @Column(name = "P7052_52")
    private String p705252;
    @Column(name = "P7052_53")
    private String p705253;
    @Column(name = "P7052_54")
    private String p705254;
    @Column(name = "P7052_55E")
    private String p705255e;    
    @Column(name = "P7061")
    private String p7061;
    @Column(name = "P7062")
    private String p7062;
    @Column(name = "FONO_DIR")
    private String fonodir;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "FECHA_ENVIO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaenvio;
    
    /*
    @OneToMany(mappedBy = "local2013Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2013Sec500> sec500;
    @OneToMany(mappedBy = "local2013Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy(value = "p104Nro")
    private List<Local2013Sec104> sec104;
    @OneToMany(mappedBy = "local2013Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2013Sec300> sec300;
    @OneToMany(mappedBy = "local2013Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Local2013Sec400> sec400;
    */
    
    @Transient
    private long token;
    @Transient
    private String msg;
    @Transient
    private String estadoRpt;

    public Local2013Cabecera() {
    }

    public Local2013Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "TIPOVIA")
    public String getTipovia() {
        return tipovia;
    }

    public void setTipovia(String tipovia) {
        this.tipovia = tipovia;
    }

    @XmlElement(name = "NOMBVIA")
    public String getNombvia() {
        return nombvia;
    }

    public void setNombvia(String nombvia) {
        this.nombvia = nombvia;
    }

    @XmlElement(name = "NUMERO")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @XmlElement(name = "MANZANA")
    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @XmlElement(name = "LOTE")
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @XmlElement(name = "CATEGORIA")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlElement(name = "NOMLUGAR")
    public String getNomlugar() {
        return nomlugar;
    }

    public void setNomlugar(String nomlugar) {
        this.nomlugar = nomlugar;
    }

    @XmlElement(name = "ETAPA")
    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    @XmlElement(name = "SECTOR")
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @XmlElement(name = "ZONA")
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @XmlElement(name = "OTROS")
    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    @XmlElement(name = "CATEG_CP")
    public String getCategCp() {
        return categCp;
    }

    public void setCategCp(String categCp) {
        this.categCp = categCp;
    }

    @XmlElement(name = "CEN_POB")
    public String getCenPob() {
        return cenPob;
    }

    public void setCenPob(String cenPob) {
        this.cenPob = cenPob;
    }

    @XmlElement(name = "CODCCPP")
    public String getCodccpp() {
        return codccpp;
    }

    public void setCodccpp(String codccpp) {
        this.codccpp = codccpp;
    }

    @XmlElement(name = "REFERENCIA")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @XmlElement(name = "CODGEO")
    public String getCodgeo() {
        return codgeo;
    }

    public void setCodgeo(String codgeo) {
        this.codgeo = codgeo;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "COD_AREA")
    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    @XmlElement(name = "COND_TEN")
    public String getCondTen() {
        return condTen;
    }

    public void setCondTen(String condTen) {
        this.condTen = condTen;
    }

    @XmlElement(name = "PROPLOCAL")
    public String getProplocal() {
        return proplocal;
    }

    public void setProplocal(String proplocal) {
        this.proplocal = proplocal;
    }

    @XmlElement(name = "GESTION")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name = "P201")
    public String getP201() {
        return p201;
    }

    public void setP201(String p201) {
        this.p201 = p201;
    }

    @XmlElement(name = "P301")
    public Integer getP301() {
        return p301;
    }

    public void setP301(Integer p301) {
        this.p301 = p301;
    }

    @XmlElement(name = "P401")
    public Integer getP401() {
        return p401;
    }

    public void setP401(Integer p401) {
        this.p401 = p401;
    }

    @XmlElement(name = "P202_1")
    public String getP2021() {
        return p2021;
    }

    public void setP2021(String p2021) {
        this.p2021 = p2021;
    }

    @XmlElement(name = "P202_2")
    public String getP2022() {
        return p2022;
    }

    public void setP2022(String p2022) {
        this.p2022 = p2022;
    }

    @XmlElement(name = "P202_3")
    public String getP2023() {
        return p2023;
    }

    public void setP2023(String p2023) {
        this.p2023 = p2023;
    }

    @XmlElement(name = "P202_4")
    public String getP2024() {
        return p2024;
    }

    public void setP2024(String p2024) {
        this.p2024 = p2024;
    }

    @XmlElement(name = "P202_5")
    public String getP2025() {
        return p2025;
    }

    public void setP2025(String p2025) {
        this.p2025 = p2025;
    }
    /*    @Column(name = "P205")
     private String p205;
     @Column(name = "P206")
     private Integer p206;
     @Column(name = "P207")
     private String p207;
     @Column(name = "P208")
     private String p208;   
     */

    @XmlElement(name = "P203")
    public String getP203() {
        return p203;
    }

    public void setP203(String p203) {
        this.p203 = p203;
    }

    @XmlElement(name = "P204")
    public String getP204() {
        return p204;
    }

    public void setP204(String p204) {
        this.p204 = p204;
    }

    @XmlElement(name = "P205")
    public Integer getP205() {
        return p205;
    }

    public void setP205(Integer p205) {
        this.p205 = p205;
    }

    @XmlElement(name = "P206")
    public String getP206() {
        return p206;
    }

    public void setP206(String p206) {
        this.p206 = p206;
    }

    @XmlElement(name = "P207")
    public String getP207() {
        return p207;
    }

    public void setP207(String p207) {
        this.p207 = p207;
    }

    @XmlElement(name = "P208")
    public String getP208() {
        return p208;
    }

    public void setP208(String p208) {
        this.p208 = p208;
    }

    @XmlElement(name = "P209_0TOT")
    public Integer getP2090tot() {
        return p2090tot;
    }

    public void setP2090tot(Integer p2090tot) {
        this.p2090tot = p2090tot;
    }

    @XmlElement(name = "P209_0DSK")
    public Integer getP2090dsk() {
        return p2090dsk;
    }

    public void setP2090dsk(Integer p2090dsk) {
        this.p2090dsk = p2090dsk;
    }

    @XmlElement(name = "P209_0LXO")
    public Integer getP2090lxo() {
        return p2090lxo;
    }

    public void setP2090lxo(Integer p2090lxo) {
        this.p2090lxo = p2090lxo;
    }

    @XmlElement(name = "P209_0OTR")
    public Integer getP2090otr() {
        return p2090otr;
    }

    public void setP2090otr(Integer p2090otr) {
        this.p2090otr = p2090otr;
    }

    @XmlElement(name = "P209_1TOT")
    public Integer getP2091tot() {
        return p2091tot;
    }

    public void setP2091tot(Integer p2091tot) {
        this.p2091tot = p2091tot;
    }

    @XmlElement(name = "P209_1DSK")
    public Integer getP2091dsk() {
        return p2091dsk;
    }

    public void setP2091dsk(Integer p2091dsk) {
        this.p2091dsk = p2091dsk;
    }

    @XmlElement(name = "P209_1LXO")
    public Integer getP2091lxo() {
        return p2091lxo;
    }

    public void setP2091lxo(Integer p2091lxo) {
        this.p2091lxo = p2091lxo;
    }

    @XmlElement(name = "P209_1OTR")
    public Integer getP2091otr() {
        return p2091otr;
    }

    public void setP2091otr(Integer p2091otr) {
        this.p2091otr = p2091otr;
    }

    @XmlElement(name = "P209_2TOT")
    public Integer getP2092tot() {
        return p2092tot;
    }

    public void setP2092tot(Integer p2092tot) {
        this.p2092tot = p2092tot;
    }

    @XmlElement(name = "P209_2DSK")
    public Integer getP2092dsk() {
        return p2092dsk;
    }

    public void setP2092dsk(Integer p2092dsk) {
        this.p2092dsk = p2092dsk;
    }

    @XmlElement(name = "P209_2LXO")
    public Integer getP2092lxo() {
        return p2092lxo;
    }

    public void setP2092lxo(Integer p2092lxo) {
        this.p2092lxo = p2092lxo;
    }

    @XmlElement(name = "P209_2OTR")
    public Integer getP2092otr() {
        return p2092otr;
    }

    public void setP2092otr(Integer p2092otr) {
        this.p2092otr = p2092otr;
    }

    @XmlElement(name = "P209_3TOT")
    public Integer getP2093tot() {
        return p2093tot;
    }

    public void setP2093tot(Integer p2093tot) {
        this.p2093tot = p2093tot;
    }

    @XmlElement(name = "P209_3DSK")
    public Integer getP2093dsk() {
        return p2093dsk;
    }

    public void setP2093dsk(Integer p2093dsk) {
        this.p2093dsk = p2093dsk;
    }

    @XmlElement(name = "P209_3LXO")
    public Integer getP2093lxo() {
        return p2093lxo;
    }

    public void setP2093lxo(Integer p2093lxo) {
        this.p2093lxo = p2093lxo;
    }

    @XmlElement(name = "P209_3OTR")
    public Integer getP2093otr() {
        return p2093otr;
    }

    public void setP2093otr(Integer p2093otr) {
        this.p2093otr = p2093otr;
    }

    @XmlElement(name = "P210_0TOT")
    public Integer getP2100tot() {
        return p2100tot;
    }

    public void setP2100tot(Integer p2100tot) {
        this.p2100tot = p2100tot;
    }

    @XmlElement(name = "P210_0CNX")
    public Integer getP2100cnx() {
        return p2100cnx;
    }

    public void setP2100cnx(Integer p2100cnx) {
        this.p2100cnx = p2100cnx;
    }

    @XmlElement(name = "P210_0NCNX")
    public Integer getP2100ncnx() {
        return p2100ncnx;
    }

    public void setP2100ncnx(Integer p2100ncnx) {
        this.p2100ncnx = p2100ncnx;
    }

    @XmlElement(name = "P210_1TOT")
    public Integer getP2101tot() {
        return p2101tot;
    }

    public void setP2101tot(Integer p2101tot) {
        this.p2101tot = p2101tot;
    }

    @XmlElement(name = "P210_1CNX")
    public Integer getP2101cnx() {
        return p2101cnx;
    }

    public void setP2101cnx(Integer p2101cnx) {
        this.p2101cnx = p2101cnx;
    }

    @XmlElement(name = "P210_1NCNX")
    public Integer getP2101ncnx() {
        return p2101ncnx;
    }

    public void setP2101ncnx(Integer p2101ncnx) {
        this.p2101ncnx = p2101ncnx;
    }

    @XmlElement(name = "P210_2TOT")
    public Integer getP2102tot() {
        return p2102tot;
    }

    public void setP2102tot(Integer p2102tot) {
        this.p2102tot = p2102tot;
    }

    @XmlElement(name = "P210_2CNX")
    public Integer getP2102cnx() {
        return p2102cnx;
    }

    public void setP2102cnx(Integer p2102cnx) {
        this.p2102cnx = p2102cnx;
    }

    @XmlElement(name = "P210_2NCNX")
    public Integer getP2102ncnx() {
        return p2102ncnx;
    }

    public void setP2102ncnx(Integer p2102ncnx) {
        this.p2102ncnx = p2102ncnx;
    }

    @XmlElement(name = "P210_3TOT")
    public Integer getP2103tot() {
        return p2103tot;
    }

    public void setP2103tot(Integer p2103tot) {
        this.p2103tot = p2103tot;
    }

    @XmlElement(name = "P210_3CNX")
    public Integer getP2103cnx() {
        return p2103cnx;
    }

    public void setP2103cnx(Integer p2103cnx) {
        this.p2103cnx = p2103cnx;
    }

    @XmlElement(name = "P210_3NCNX")
    public Integer getP2103ncnx() {
        return p2103ncnx;
    }

    public void setP2103ncnx(Integer p2103ncnx) {
        this.p2103ncnx = p2103ncnx;
    }

    @XmlElement(name = "P211_0CA")
    public Integer getP2110ca() {
        return p2110ca;
    }

    public void setP2110ca(Integer p2110ca) {
        this.p2110ca = p2110ca;
    }

    @XmlElement(name = "P211_0SI")
    public Integer getP2110si() {
        return p2110si;
    }

    public void setP2110si(Integer p2110si) {
        this.p2110si = p2110si;
    }

    @XmlElement(name = "P211_0ME")
    public Integer getP2110me() {
        return p2110me;
    }

    public void setP2110me(Integer p2110me) {
        this.p2110me = p2110me;
    }

    @XmlElement(name = "P211_1CA")
    public Integer getP2111ca() {
        return p2111ca;
    }

    public void setP2111ca(Integer p2111ca) {
        this.p2111ca = p2111ca;
    }

    @XmlElement(name = "P211_1SI")
    public Integer getP2111si() {
        return p2111si;
    }

    public void setP2111si(Integer p2111si) {
        this.p2111si = p2111si;
    }

    @XmlElement(name = "P211_1ME")
    public Integer getP2111me() {
        return p2111me;
    }

    public void setP2111me(Integer p2111me) {
        this.p2111me = p2111me;
    }

    @XmlElement(name = "P211_2CA")
    public Integer getP2112ca() {
        return p2112ca;
    }

    public void setP2112ca(Integer p2112ca) {
        this.p2112ca = p2112ca;
    }

    @XmlElement(name = "P211_2SI")
    public Integer getP2112si() {
        return p2112si;
    }

    public void setP2112si(Integer p2112si) {
        this.p2112si = p2112si;
    }

    @XmlElement(name = "P211_2ME")
    public Integer getP2112me() {
        return p2112me;
    }

    public void setP2112me(Integer p2112me) {
        this.p2112me = p2112me;
    }

    @XmlElement(name = "P211_3CA")
    public Integer getP2113ca() {
        return p2113ca;
    }

    public void setP2113ca(Integer p2113ca) {
        this.p2113ca = p2113ca;
    }

    @XmlElement(name = "P211_3SI")
    public Integer getP2113si() {
        return p2113si;
    }

    public void setP2113si(Integer p2113si) {
        this.p2113si = p2113si;
    }

    @XmlElement(name = "P211_3ME")
    public Integer getP2113me() {
        return p2113me;
    }

    public void setP2113me(Integer p2113me) {
        this.p2113me = p2113me;
    }

    @XmlElement(name = "P212_INI")
    public Integer getP212Ini() {
        return p212Ini;
    }

    public void setP212Ini(Integer p212Ini) {
        this.p212Ini = p212Ini;
    }

    @XmlElement(name = "P212_1Y2P")
    public Integer getP2121y2p() {
        return p2121y2p;
    }

    public void setP2121y2p(Integer p2121y2p) {
        this.p2121y2p = p2121y2p;
    }

    @XmlElement(name = "P212_3A6P")
    public Integer getP2123a6p() {
        return p2123a6p;
    }

    public void setP2123a6p(Integer p2123a6p) {
        this.p2123a6p = p2123a6p;
    }

    @XmlElement(name = "P212_SCCS")
    public Integer getP212Sccs() {
        return p212Sccs;
    }

    public void setP212Sccs(Integer p212Sccs) {
        this.p212Sccs = p212Sccs;
    }

    @XmlElement(name = "P213")
    public Integer getP213() {
        return p213;
    }

    public void setP213(Integer p213) {
        this.p213 = p213;
    }

    @XmlElement(name = "P214")
    public String getP214() {
        return p214;
    }

    public void setP214(String p214) {
        this.p214 = p214;
    }

    @XmlElement(name = "P215")
    public String getP215() {
        return p215;
    }

    public void setP215(String p215) {
        this.p215 = p215;
    }

    @XmlElement(name = "P215_esp")
    public String getP215esp() {
        return p215esp;
    }

    public void setP215esp(String p215esp) {
        this.p215esp = p215esp;
    }

    @XmlElement(name = "P216")
    public String getP216() {
        return p216;
    }

    public void setP216(String p216) {
        this.p216 = p216;
    }

    @XmlElement(name = "P217")
    public String getP217() {
        return p217;
    }

    public void setP217(String p217) {
        this.p217 = p217;
    }

    @XmlElement(name = "P218_01")
    public Integer getP21801() {
        return p21801;
    }

    public void setP21801(Integer p21801) {
        this.p21801 = p21801;
    }

    @XmlElement(name = "P218_02")
    public Integer getP21802() {
        return p21802;
    }

    public void setP21802(Integer p21802) {
        this.p21802 = p21802;
    }

    @XmlElement(name = "P218_03")
    public Integer getP21803() {
        return p21803;
    }

    public void setP21803(Integer p21803) {
        this.p21803 = p21803;
    }

    @XmlElement(name = "P218_04")
    public Integer getP21804() {
        return p21804;
    }

    public void setP21804(Integer p21804) {
        this.p21804 = p21804;
    }

    @XmlElement(name = "P218_05")
    public Integer getP21805() {
        return p21805;
    }

    public void setP21805(Integer p21805) {
        this.p21805 = p21805;
    }

    @XmlElement(name = "P218_11")
    public Integer getP21811() {
        return p21811;
    }

    public void setP21811(Integer p21811) {
        this.p21811 = p21811;
    }

    @XmlElement(name = "P218_12")
    public Integer getP21812() {
        return p21812;
    }

    public void setP21812(Integer p21812) {
        this.p21812 = p21812;
    }

    @XmlElement(name = "P218_13")
    public Integer getP21813() {
        return p21813;
    }

    public void setP21813(Integer p21813) {
        this.p21813 = p21813;
    }

    @XmlElement(name = "P218_14")
    public Integer getP21814() {
        return p21814;
    }

    public void setP21814(Integer p21814) {
        this.p21814 = p21814;
    }

    @XmlElement(name = "P218_15")
    public Integer getP21815() {
        return p21815;
    }

    public void setP21815(Integer p21815) {
        this.p21815 = p21815;
    }

    @XmlElement(name = "P218_21")
    public Integer getP21821() {
        return p21821;
    }

    public void setP21821(Integer p21821) {
        this.p21821 = p21821;
    }

    @XmlElement(name = "P218_22")
    public Integer getP21822() {
        return p21822;
    }

    public void setP21822(Integer p21822) {
        this.p21822 = p21822;
    }

    @XmlElement(name = "P218_23")
    public Integer getP21823() {
        return p21823;
    }

    public void setP21823(Integer p21823) {
        this.p21823 = p21823;
    }

    @XmlElement(name = "P218_24")
    public Integer getP21824() {
        return p21824;
    }

    public void setP21824(Integer p21824) {
        this.p21824 = p21824;
    }

    @XmlElement(name = "P218_25")
    public Integer getP21825() {
        return p21825;
    }

    public void setP21825(Integer p21825) {
        this.p21825 = p21825;
    }

    @XmlElement(name = "P218_31")
    public Integer getP21831() {
        return p21831;
    }

    public void setP21831(Integer p21831) {
        this.p21831 = p21831;
    }

    @XmlElement(name = "P218_32")
    public Integer getP21832() {
        return p21832;
    }

    public void setP21832(Integer p21832) {
        this.p21832 = p21832;
    }

    @XmlElement(name = "P218_33")
    public Integer getP21833() {
        return p21833;
    }

    public void setP21833(Integer p21833) {
        this.p21833 = p21833;
    }

    @XmlElement(name = "P218_34")
    public Integer getP21834() {
        return p21834;
    }

    public void setP21834(Integer p21834) {
        this.p21834 = p21834;
    }

    @XmlElement(name = "P218_35")
    public Integer getP21835() {
        return p21835;
    }

    public void setP21835(Integer p21835) {
        this.p21835 = p21835;
    }

    @XmlElement(name = "P219")
    public Integer getP219() {
        return p219;
    }

    public void setP219(Integer p219) {
        this.p219 = p219;
    }

    @XmlElement(name = "P220")
    public String getP220() {
        return p220;
    }

    public void setP220(String p220) {
        this.p220 = p220;
    }

    @XmlElement(name = "P601_1")
    public String getP6011() {
        return p6011;
    }

    public void setP6011(String p6011) {
        this.p6011 = p6011;
    }

    @XmlElement(name = "P601_2")
    public String getP6012() {
        return p6012;
    }

    public void setP6012(String p6012) {
        this.p6012 = p6012;
    }

    @XmlElement(name = "P601_3")
    public String getP6013() {
        return p6013;
    }

    public void setP6013(String p6013) {
        this.p6013 = p6013;
    }

    @XmlElement(name = "P601_4")
    public String getP6014() {
        return p6014;
    }

    public void setP6014(String p6014) {
        this.p6014 = p6014;
    }

    @XmlElement(name = "P601_5")
    public String getP6015() {
        return p6015;
    }

    public void setP6015(String p6015) {
        this.p6015 = p6015;
    }

    @XmlElement(name = "P601_61")
    public String getP60161() {
        return p60161;
    }

    public void setP60161(String p60161) {
        this.p60161 = p60161;
    }

    @XmlElement(name = "P601_62")
    public String getP60162() {
        return p60162;
    }

    public void setP60162(String p60162) {
        this.p60162 = p60162;
    }

    @XmlElement(name = "P601_7")
    public String getP6017() {
        return p6017;
    }

    public void setP6017(String p6017) {
        this.p6017 = p6017;
    }

    @XmlElement(name = "P601_8")
    public String getP6018() {
        return p6018;
    }

    public void setP6018(String p6018) {
        this.p6018 = p6018;
    }

    @XmlElement(name = "P601_9")
    public String getP6019() {
        return p6019;
    }

    public void setP6019(String p6019) {
        this.p6019 = p6019;
    }

    @XmlElement(name = "P601_10")
    public String getP60110() {
        return p60110;
    }

    public void setP60110(String p60110) {
        this.p60110 = p60110;
    }

    @XmlElement(name = "P602_11")
    public String getP60211() {
        return p60211;
    }

    public void setP60211(String p60211) {
        this.p60211 = p60211;
    }

    @XmlElement(name = "P602_12")
    public String getP60212() {
        return p60212;
    }

    public void setP60212(String p60212) {
        this.p60212 = p60212;
    }

    @XmlElement(name = "P602_13")
    public String getP60213() {
        return p60213;
    }

    public void setP60213(String p60213) {
        this.p60213 = p60213;
    }

    @XmlElement(name = "P602_14")
    public String getP60214() {
        return p60214;
    }

    public void setP60214(String p60214) {
        this.p60214 = p60214;
    }

    @XmlElement(name = "P602_15")
    public String getP60215() {
        return p60215;
    }

    public void setP60215(String p60215) {
        this.p60215 = p60215;
    }

    @XmlElement(name = "P602_16")
    public String getP60216() {
        return p60216;
    }

    public void setP60216(String p60216) {
        this.p60216 = p60216;
    }

    @XmlElement(name = "P602_21")
    public String getP60221() {
        return p60221;
    }

    public void setP60221(String p60221) {
        this.p60221 = p60221;
    }

    @XmlElement(name = "P602_22")
    public String getP60222() {
        return p60222;
    }

    public void setP60222(String p60222) {
        this.p60222 = p60222;
    }

    @XmlElement(name = "P602_23")
    public String getP60223() {
        return p60223;
    }

    public void setP60223(String p60223) {
        this.p60223 = p60223;
    }

    @XmlElement(name = "P602_24")
    public String getP60224() {
        return p60224;
    }

    public void setP60224(String p60224) {
        this.p60224 = p60224;
    }

    @XmlElement(name = "P602_25")
    public String getP60225() {
        return p60225;
    }

    public void setP60225(String p60225) {
        this.p60225 = p60225;
    }

    @XmlElement(name = "P602_26")
    public String getP60226() {
        return p60226;
    }

    public void setP60226(String p60226) {
        this.p60226 = p60226;
    }

    @XmlElement(name = "P603_1D")
    public Integer getP6031d() {
        return p6031d;
    }

    public void setP6031d(Integer p6031d) {
        this.p6031d = p6031d;
    }

    @XmlElement(name = "P603_1H")
    public Integer getP6031h() {
        return p6031h;
    }

    public void setP6031h(Integer p6031h) {
        this.p6031h = p6031h;
    }

    @XmlElement(name = "P603_1M")
    public Integer getP6031m() {
        return p6031m;
    }

    public void setP6031m(Integer p6031m) {
        this.p6031m = p6031m;
    }

    @XmlElement(name = "P603_2D")
    public Integer getP6032d() {
        return p6032d;
    }

    public void setP6032d(Integer p6032d) {
        this.p6032d = p6032d;
    }

    @XmlElement(name = "P603_2H")
    public Integer getP6032h() {
        return p6032h;
    }

    public void setP6032h(Integer p6032h) {
        this.p6032h = p6032h;
    }

    @XmlElement(name = "P603_2M")
    public Integer getP6032m() {
        return p6032m;
    }

    public void setP6032m(Integer p6032m) {
        this.p6032m = p6032m;
    }

    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "APE_DIR")
    public String getApeDir() {
        return apeDir;
    }

    public void setApeDir(String apeDir) {
        this.apeDir = apeDir;
    }

    @XmlElement(name = "NOM_DIR")
    public String getNomDir() {
        return nomDir;
    }

    public void setNomDir(String nomDir) {
        this.nomDir = nomDir;
    }

    @XmlElement(name = "SITUA_DIR")
    public String getSituaDir() {
        return situaDir;
    }

    public void setSituaDir(String situaDir) {
        this.situaDir = situaDir;
    }

    @XmlElement(name = "DNI_DIR")
    public String getDniDir() {
        return dniDir;
    }

    public void setDniDir(String dniDir) {
        this.dniDir = dniDir;
    }

    @XmlElement(name = "EMAIL_DIR")
    public String getEmailDir() {
        return emailDir;
    }

    public void setEmailDir(String emailDir) {
        this.emailDir = emailDir;
    }

    @XmlElement(name = "REC_CEDD")
    public String getRecCedd() {
        return recCedd;
    }

    public void setRecCedd(String recCedd) {
        this.recCedd = recCedd;
    }

    @XmlElement(name = "REC_CEDM")
    public String getRecCedm() {
        return recCedm;
    }

    public void setRecCedm(String recCedm) {
        this.recCedm = recCedm;
    }

    @XmlElement(name = "CUL_CEDD")
    public String getCulCedd() {
        return culCedd;
    }

    public void setCulCedd(String culCedd) {
        this.culCedd = culCedd;
    }

    @XmlElement(name = "CUL_CEDM")
    public String getCulCedm() {
        return culCedm;
    }

    public void setCulCedm(String culCedm) {
        this.culCedm = culCedm;
    }

    @XmlElement(name = "P701_10")
    public Integer getP70110() {
        return p70110;
    }

    public void setP70110(Integer p70110) {
        this.p70110 = p70110;
    }

    @XmlElement(name = "P701_11")
    public String getP70111() {
        return p70111;
    }

    public void setP70111(String p70111) {
        this.p70111 = p70111;
    }

    @XmlElement(name = "P701_12")
    public String getP70112() {
        return p70112;
    }

    public void setP70112(String p70112) {
        this.p70112 = p70112;
    }

    @XmlElement(name = "P701_13")
    public Integer getP70113() {
        return p70113;
    }

    public void setP70113(Integer p70113) {
        this.p70113 = p70113;
    }

    @XmlElement(name = "P701_14")
    public String getP70114() {
        return p70114;
    }

    public void setP70114(String p70114) {
        this.p70114 = p70114;
    }

    @XmlElement(name = "P701_15")
    public String getP70115() {
        return p70115;
    }

    public void setP70115(String p70115) {
        this.p70115 = p70115;
    }

    @XmlElement(name = "P701_16")
    public Integer getP70116() {
        return p70116;
    }

    public void setP70116(Integer p70116) {
        this.p70116 = p70116;
    }

    @XmlElement(name = "P701_17")
    public String getP70117() {
        return p70117;
    }

    public void setP70117(String p70117) {
        this.p70117 = p70117;
    }

    @XmlElement(name = "P701_18")
    public String getP70118() {
        return p70118;
    }

    public void setP70118(String p70118) {
        this.p70118 = p70118;
    }

    @XmlElement(name = "P701_19")
    public String getP70119() {
        return p70119;
    }

    public void setP70119(String p70119) {
        this.p70119 = p70119;
    }

    @XmlElement(name = "P701_1ESP")
    public String getP7011esp() {
        return p7011esp;
    }

    public void setP7011esp(String p7011esp) {
        this.p7011esp = p7011esp;
    }

    @XmlElement(name = "P701_20")
    public Integer getP70120() {
        return p70120;
    }

    public void setP70120(Integer p70120) {
        this.p70120 = p70120;
    }

    @XmlElement(name = "P701_21")
    public String getP70121() {
        return p70121;
    }

    public void setP70121(String p70121) {
        this.p70121 = p70121;
    }

    @XmlElement(name = "P701_22")
    public String getP70122() {
        return p70122;
    }

    public void setP70122(String p70122) {
        this.p70122 = p70122;
    }

    @XmlElement(name = "P701_23")
    public Integer getP70123() {
        return p70123;
    }

    public void setP70123(Integer p70123) {
        this.p70123 = p70123;
    }

    @XmlElement(name = "P701_24")
    public String getP70124() {
        return p70124;
    }

    public void setP70124(String p70124) {
        this.p70124 = p70124;
    }

    @XmlElement(name = "P701_25")
    public String getP70125() {
        return p70125;
    }

    public void setP70125(String p70125) {
        this.p70125 = p70125;
    }

    @XmlElement(name = "P701_26")
    public Integer getP70126() {
        return p70126;
    }

    public void setP70126(Integer p70126) {
        this.p70126 = p70126;
    }

    @XmlElement(name = "P701_27")
    public String getP70127() {
        return p70127;
    }

    public void setP70127(String p70127) {
        this.p70127 = p70127;
    }

    @XmlElement(name = "P701_28")
    public String getP70128() {
        return p70128;
    }

    public void setP70128(String p70128) {
        this.p70128 = p70128;
    }

    @XmlElement(name = "P701_29")
    public String getP70129() {
        return p70129;
    }

    public void setP70129(String p70129) {
        this.p70129 = p70129;
    }

    @XmlElement(name = "P701_2ESP")
    public String getP7012esp() {
        return p7012esp;
    }

    public void setP7012esp(String p7012esp) {
        this.p7012esp = p7012esp;
    }

    @XmlElement(name = "P701_30")
    public Integer getP70130() {
        return p70130;
    }

    public void setP70130(Integer p70130) {
        this.p70130 = p70130;
    }

    @XmlElement(name = "P701_31")
    public String getP70131() {
        return p70131;
    }

    public void setP70131(String p70131) {
        this.p70131 = p70131;
    }

    @XmlElement(name = "P701_32")
    public String getP70132() {
        return p70132;
    }

    public void setP70132(String p70132) {
        this.p70132 = p70132;
    }

    @XmlElement(name = "P701_33")
    public Integer getP70133() {
        return p70133;
    }

    public void setP70133(Integer p70133) {
        this.p70133 = p70133;
    }

    @XmlElement(name = "P701_34")
    public String getP70134() {
        return p70134;
    }

    public void setP70134(String p70134) {
        this.p70134 = p70134;
    }

    @XmlElement(name = "P701_35")
    public String getP70135() {
        return p70135;
    }

    public void setP70135(String p70135) {
        this.p70135 = p70135;
    }

    @XmlElement(name = "P701_36")
    public Integer getP70136() {
        return p70136;
    }

    public void setP70136(Integer p70136) {
        this.p70136 = p70136;
    }

    @XmlElement(name = "P701_37")
    public String getP70137() {
        return p70137;
    }

    public void setP70137(String p70137) {
        this.p70137 = p70137;
    }

    @XmlElement(name = "P701_38")
    public String getP70138() {
        return p70138;
    }

    public void setP70138(String p70138) {
        this.p70138 = p70138;
    }

    @XmlElement(name = "P701_39")
    public String getP70139() {
        return p70139;
    }

    public void setP70139(String p70139) {
        this.p70139 = p70139;
    }

    @XmlElement(name = "P701_3ESP")
    public String getP7013esp() {
        return p7013esp;
    }

    public void setP7013esp(String p7013esp) {
        this.p7013esp = p7013esp;
    }

    @XmlElement(name = "P701_4VTA")
    public Integer getP7014vta() {
        return p7014vta;
    }

    public void setP7014vta(Integer p7014vta) {
        this.p7014vta = p7014vta;
    }

    @XmlElement(name = "P7021")
    public String getP7021() {
        return p7021;
    }

    public void setP7021(String p7021) {
        this.p7021 = p7021;
    }

    @XmlElement(name = "P7022")
    public String getP7022() {
        return p7022;
    }

    public void setP7022(String p7022) {
        this.p7022 = p7022;
    }

    @XmlElement(name = "P7023_1")
    public String getP70231() {
        return p70231;
    }

    public void setP70231(String p70231) {
        this.p70231 = p70231;
    }

    @XmlElement(name = "P7023_2")
    public String getP70232() {
        return p70232;
    }

    public void setP70232(String p70232) {
        this.p70232 = p70232;
    }
    
    @XmlElement(name = "P7024")
    public String getP7024() {
        return p7024;
    }

    public void setP7024(String p7024) {
        this.p7024 = p7024;
    }
    
    @XmlElement(name = "P7025")
    public String getP7025() {
        return p7025;
    }

    public void setP7025(String p7025) {
        this.p7025 = p7025;
    }
    
    @XmlElement(name = "P7026_1")
    public String getP70261() {
        return p70261;
    }

    public void setP70261(String p70261) {
        this.p70261 = p70261;
    }
    
    @XmlElement(name = "P7026_2")
    public String getP70262() {
        return p70262;
    }

    public void setP70262(String p70262) {
        this.p70262 = p70262;
    }

    @XmlElement(name = "P703")
    public String getP703() {
        return p703;
    }

    public void setP703(String p703) {
        this.p703 = p703;
    }

    @XmlElement(name = "P703_1")
    public String getP7031() {
        return p7031;
    }

    public void setP7031(String p7031) {
        this.p7031 = p7031;
    }

    @XmlElement(name = "P703_2")
    public String getP7032() {
        return p7032;
    }

    public void setP7032(String p7032) {
        this.p7032 = p7032;
    }

    @XmlElement(name = "P703_3")
    public String getP7033() {
        return p7033;
    }

    public void setP7033(String p7033) {
        this.p7033 = p7033;
    }

    @XmlElement(name = "P703_4")
    public String getP7034() {
        return p7034;
    }

    public void setP7034(String p7034) {
        this.p7034 = p7034;
    }

    @XmlElement(name = "P703_5")
    public String getP7035() {
        return p7035;
    }

    public void setP7035(String p7035) {
        this.p7035 = p7035;
    }

    @XmlElement(name = "P703_6")
    public String getP7036() {
        return p7036;
    }

    public void setP7036(String p7036) {
        this.p7036 = p7036;
    }

    @XmlElement(name = "P703_7")
    public String getP7037() {
        return p7037;
    }

    public void setP7037(String p7037) {
        this.p7037 = p7037;
    }

    @XmlElement(name = "P703_8")
    public String getP7038() {
        return p7038;
    }

    public void setP7038(String p7038) {
        this.p7038 = p7038;
    }

    @XmlElement(name = "P703_9")
    public String getP7039() {
        return p7039;
    }

    public void setP7039(String p7039) {
        this.p7039 = p7039;
    }

    @XmlElement(name = "P703_10")
    public String getP70310() {
        return p70310;
    }

    public void setP70310(String p70310) {
        this.p70310 = p70310;
    }

    @XmlElement(name = "P703_11")
    public String getP70311() {
        return p70311;
    }

    public void setP70311(String p70311) {
        this.p70311 = p70311;
    }

    @XmlElement(name = "P703_12")
    public String getP70312() {
        return p70312;
    }

    public void setP70312(String p70312) {
        this.p70312 = p70312;
    }

    @XmlElement(name = "P703_13")
    public String getP70313() {
        return p70313;
    }

    public void setP70313(String p70313) {
        this.p70313 = p70313;
    }

    @XmlElement(name = "P703_14")
    public String getP70314() {
        return p70314;
    }

    public void setP70314(String p70314) {
        this.p70314 = p70314;
    }

    @XmlElement(name = "P703_15")
    public String getP70315() {
        return p70315;
    }

    public void setP70315(String p70315) {
        this.p70315 = p70315;
    }

    @XmlElement(name = "P703_16")
    public String getP70316() {
        return p70316;
    }

    public void setP70316(String p70316) {
        this.p70316 = p70316;
    }

    @XmlElement(name = "P703_17")
    public String getP70317() {
        return p70317;
    }

    public void setP70317(String p70317) {
        this.p70317 = p70317;
    }

    @XmlElement(name = "P703_18")
    public String getP70318() {
        return p70318;
    }

    public void setP70318(String p70318) {
        this.p70318 = p70318;
    }

    @XmlElement(name = "P703_18E")
    public String getP70318e() {
        return p70318e;
    }

    public void setP70318e(String p70318e) {
        this.p70318e = p70318e;
    }

    @XmlElement(name = "P704")
    public String getP704() {
        return p704;
    }

    public void setP704(String p704) {
        this.p704 = p704;
    }

    @XmlElement(name = "P704_ESP")
    public String getP704Esp() {
        return p704esp;
    }

    public void setP704Esp(String p704esp) {
        this.p704esp = p704esp;
    }

    @XmlElement(name = "P7051")
    public String getP7051() {
        return p7051;
    }

    public void setP7051(String p7051) {
        this.p7051 = p7051;
    }

    @XmlElement(name = "P7052_11")
    public String getP705211() {
        return p705211;
    }

    public void setP705211(String p705211) {
        this.p705211 = p705211;
    }

    @XmlElement(name = "P7052_12")
    public String getP705212() {
        return p705212;
    }

    public void setP705212(String p705212) {
        this.p705212 = p705212;
    }

    @XmlElement(name = "P7052_13")
    public String getP705213() {
        return p705213;
    }

    public void setP705213(String p705213) {
        this.p705213 = p705213;
    }

    @XmlElement(name = "P7052_14")
    public String getP705214() {
        return p705214;
    }

    public void setP705214(String p705214) {
        this.p705214 = p705214;
    }

    @XmlElement(name = "P7052_21")
    public String getP705221() {
        return p705221;
    }

    public void setP705221(String p705221) {
        this.p705221 = p705221;
    }

    @XmlElement(name = "P7052_22")
    public String getP705222() {
        return p705222;
    }

    public void setP705222(String p705222) {
        this.p705222 = p705222;
    }

    @XmlElement(name = "P7052_23")
    public String getP705223() {
        return p705223;
    }

    public void setP705223(String p705223) {
        this.p705223 = p705223;
    }

    @XmlElement(name = "P7052_24")
    public String getP705224() {
        return p705224;
    }

    public void setP705224(String p705224) {
        this.p705224 = p705224;
    }

    @XmlElement(name = "P7052_31")
    public String getP705231() {
        return p705231;
    }

    public void setP705231(String p705231) {
        this.p705231 = p705231;
    }

    @XmlElement(name = "P7052_32")
    public String getP705232() {
        return p705232;
    }

    public void setP705232(String p705232) {
        this.p705232 = p705232;
    }

    @XmlElement(name = "P7052_33")
    public String getP705233() {
        return p705233;
    }

    public void setP705233(String p705233) {
        this.p705233 = p705233;
    }

    @XmlElement(name = "P7052_34")
    public String getP705234() {
        return p705234;
    }

    public void setP705234(String p705234) {
        this.p705234 = p705234;
    }

    @XmlElement(name = "P7052_41")
    public String getP705241() {
        return p705241;
    }

    public void setP705241(String p705241) {
        this.p705241 = p705241;
    }

    @XmlElement(name = "P7052_42")
    public String getP705242() {
        return p705242;
    }

    public void setP705242(String p705242) {
        this.p705242 = p705242;
    }

    @XmlElement(name = "P7052_43")
    public String getP705243() {
        return p705243;
    }

    public void setP705243(String p705243) {
        this.p705243 = p705243;
    }

    @XmlElement(name = "P7052_44")
    public String getP705244() {
        return p705244;
    }

    public void setP705244(String p705244) {
        this.p705244 = p705244;
    }

    @XmlElement(name = "P7052_51")
    public String getP705251() {
        return p705251;
    }

    public void setP705251(String p705251) {
        this.p705251 = p705251;
    }

    @XmlElement(name = "P7052_52")
    public String getP705252() {
        return p705252;
    }

    public void setP705252(String p705252) {
        this.p705252 = p705252;
    }

    @XmlElement(name = "P7052_53")
    public String getP705253() {
        return p705253;
    }

    public void setP705253(String p705253) {
        this.p705253 = p705253;
    }

    @XmlElement(name = "P7052_54")
    public String getP705254() {
        return p705254;
    }

    public void setP705254(String p705254) {
        this.p705254 = p705254;
    }

    @XmlElement(name = "P7052_55E")
    public String getP705255E() {
        return p705255e;
    }

    public void setP705255E(String p705255e) {
        this.p705255e = p705255e;
    }

    @XmlElement(name = "P7061")
    public String getP7061() {
        return p7061;
    }

    public void setP7061(String p7061) {
        this.p7061 = p7061;
    }

    @XmlElement(name = "P7062")
    public String getP7062() {
        return p7062;
    }

    public void setP7062(String p7062) {
        this.p7062 = p7062;
    }

    @XmlElement(name = "FONO_DIR")
    public String getFonoDir() {
        return fonodir;
    }

    public void setFonoDir(String fonodir) {
        this.fonodir = fonodir;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2013Cabecera)) {
            return false;
        }
        Local2013Cabecera other = (Local2013Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
/*
    @XmlElement(name = "SEC104")
    public List<Local2013Sec104> getSec104() {
        return sec104;
    }

    public void setSec104(List<Local2013Sec104> sec104) {
        this.sec104 = sec104;
    }

    @XmlElement(name = "SEC500")
    public List<Local2013Sec500> getSec500() {
        return sec500;
    }

    public void setSec500(List<Local2013Sec500> sec500) {
        this.sec500 = sec500;
    }

    @XmlElement(name = "SEC300")
    public List<Local2013Sec300> getSec300() {
        return sec300;
    }

    public void setSec300(List<Local2013Sec300> sec300) {
        this.sec300 = sec300;
    }

    @XmlElement(name = "SEC400")
    public List<Local2013Sec400> getSec400() {
        return sec400;
    }

    public void setSec400(List<Local2013Sec400> sec400) {
        this.sec400 = sec400;
    }*/

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlAttribute(name = "MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlAttribute(name = "ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean getUltimo() {
        return ultimo;
    }

    /**
     * @param ultimo the ultimo to set
     */
    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    public Date getFechaenvio() {
        return fechaenvio;
    }

    public void setFechaenvio(Date fechaenvio) {
        this.fechaenvio = fechaenvio;
    }
}
