package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2016.Matricula2016MatriculaMapAdapter.Matricula2016MatriculaList;

public class Matricula2016MatriculaMapAdapter extends XmlAdapter<Matricula2016MatriculaList, Map<String, Matricula2016Matricula>>{

    private static final Logger LOGGER = Logger.getLogger(Matricula2016MatriculaMapAdapter.class.getName());

    static class Matricula2016MatriculaList {

        private List<Matricula2016Matricula> detalle;

        private Matricula2016MatriculaList(ArrayList<Matricula2016Matricula> lista) {
            detalle = lista;
        }

        public Matricula2016MatriculaList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2016Matricula> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2016Matricula> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2016Matricula> unmarshal(Matricula2016MatriculaList v) throws Exception {

        Map<String, Matricula2016Matricula> map = new HashMap<String, Matricula2016Matricula>();
        for (Matricula2016Matricula detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2016MatriculaList marshal(Map<String, Matricula2016Matricula> v) throws Exception {
        if(v==null)return null;

        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2016Matricula> lista = new ArrayList<Matricula2016Matricula>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2016Matricula $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2016MatriculaList list = new Matricula2016MatriculaList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
