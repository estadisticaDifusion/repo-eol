package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2018.Resultado2018DetalleMapAdapter.Resultado2018DetalleList;

/**
 *
 * @author JBEDRILLANA
 */
public class Resultado2018DetalleMapAdapter
		extends XmlAdapter<Resultado2018DetalleList, Map<String, Resultado2018Detalle>> {

	private static final Logger LOGGER = Logger.getLogger(Resultado2018DetalleMapAdapter.class.getName());

	static class Resultado2018DetalleList {

		private List<Resultado2018Detalle> detalle;

		private Resultado2018DetalleList(ArrayList<Resultado2018Detalle> lista) {
			detalle = lista;
		}

		public Resultado2018DetalleList() {
		}

		@XmlElement(name = "CUADROS")
		public List<Resultado2018Detalle> getDetalle() {
			return detalle;
		}

		public void setDetalle(List<Resultado2018Detalle> detalle) {
			this.detalle = detalle;
		}
	}

	@Override
	public Map<String, Resultado2018Detalle> unmarshal(Resultado2018DetalleList v) throws Exception {
		Map<String, Resultado2018Detalle> map = new HashMap<String, Resultado2018Detalle>();
		for (Resultado2018Detalle detalle : v.getDetalle()) {
			map.put(detalle.getCuadro(), detalle);
		}
		LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[] { v, map });
		return map;
	}

	@Override
	public Resultado2018DetalleList marshal(Map<String, Resultado2018Detalle> v) throws Exception {
		if (v == null) {
			LOGGER.fine("no hay detalles");
			return null;
		}
		Set<String> keySet = v.keySet();
		if (keySet.isEmpty()) {
			LOGGER.fine("no hay claves");
			return null;

		}
		ArrayList<Resultado2018Detalle> lista = new ArrayList<Resultado2018Detalle>();
		for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
			String key = it.next();
			Resultado2018Detalle $detalle = v.get(key);
			$detalle.setCuadro(key);
			lista.add($detalle);
		}
		Resultado2018DetalleList list = new Resultado2018DetalleList(lista);
		LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[] { v, lista });
		return list;
	}

}
