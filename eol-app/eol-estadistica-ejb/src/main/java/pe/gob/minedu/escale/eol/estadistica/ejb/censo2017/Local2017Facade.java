package pe.gob.minedu.escale.eol.estadistica.ejb.censo2017;

//import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Local2017Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.portlets.ejb.facade.CensoFacade;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Singleton
public class Local2017Facade extends AbstractFacade<Local2017Cabecera> implements Local2017Local {

	private Logger logger = Logger.getLogger(CensoFacade.class);

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Local2017Facade() {
		super(Local2017Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Local2017Cabecera entity) {
		logger.info(":: Local2017Facade.create :: Starting execution...");
		String sql = "UPDATE Local2017Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();
		/*
		 * OM for (Local2017Sec104 sec104 : entity.getLocal2017Sec104List()) {
		 * sec104.setLocal2017Cabecera(entity); }
		 * 
		 * if (entity.getLocal2017Sec207List() != null) for (Local2017Sec207 02 :
		 * entity.getLocal2017Sec207List()) { sec302.setLocal2017Cabecera(entity); }
		 * 
		 * if (entity.getLocal2017Sec300List() != null) for (Local2017Sec300 sec300 :
		 * entity.getLocal2017Sec300List()) { sec300.setLocal2017Cabecera(entity); }
		 * 
		 * if (entity.getLocal2017Sec304List() != null) for (Local2017Sec304 sec304 :
		 * entity.getLocal2017Sec304List()) { sec304.setLocal2017Cabecera(entity); }
		 * 
		 * if (entity.getLocal2017Sec400List() != null) for (Local2017Sec400 sec400 :
		 * entity.getLocal2017Sec400List()) { sec400.setLocal2017Cabecera(entity); }
		 * 
		 * if (entity.getLocal2017Sec500List() != null) for (Local2017Sec500 sec500 :
		 * entity.getLocal2017Sec500List()) { sec500.setLocal2017Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();
		logger.info(":: Local2017Facade.create :: Execution finish.");
	}

	public Local2017Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2017Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2017Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void createCabecera(Local2017Cabecera entity) {
		String sql = "UPDATE Local2017Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();

		em.persist(entity);
		// em.flush();
	}
}
