/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2013_coordpronoei")
public class Matricula2013Coordpronoei implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    
    @Column(name = "NRO")
    private String nro;
    @Column(name = "PER01")
    private String per01;    
    @Column(name = "PER02")
    private String per02;
    @Column(name = "PER03")
    private String per03;
    @Column(name = "PER04")
    private String per04;
    @Column(name = "PER05")
    private String per05;
    @Column(name = "PER06")
    private String per06;
    @Column(name = "PER07")
    private String per07;
    @Column(name = "PER08")
    private String per08;
    @Column(name = "PER09")
    private String per09;
    @Column(name = "PER10")
    private String per10;
    @Column(name = "PER11")
    private String per11;
    @Column(name = "PER12")
    private String per12;
    @Column(name = "PER13_1")
    private String per131;
    @Column(name = "PER13_2")
    private String per132;
    @Column(name = "PER14_1")
    private String per141;
    @Column(name = "PER14_2")
    private String per142;    
    @Column(name = "PER15")
    private String per15;
    @Column(name = "PER16")
    private String per16;
    @Column(name = "PER17")
    private String per17;
    @Column(name = "PER18")
    private String per18;
    
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2013Cabecera matricula2013Cabecera;

    public Matricula2013Coordpronoei() {
    }

    public Matricula2013Coordpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    @XmlElement(name="PER01")
    public String getPer01() {
        return per01;
    }

    public void setPer01(String per01) {
        this.per01 = per01;
    }
    
     @XmlElement(name="PER02")
    public String getPer02() {
        return per02;
    }

    public void setPer02(String per02) {
        this.per02 = per02;
    }
    
    @XmlElement(name="PER03")
    public String getPer03() {
        return per03;
    }

    public void setPer03(String per03) {
        this.per03 = per03;
    }
    
     @XmlElement(name="PER04")
    public String getPer04() {
        return per04;
    }

    public void setPer04(String per04) {
        this.per04 = per04;
    }
    
     @XmlElement(name="PER05")
    public String getPer05() {
        return per05;
    }

    public void setPer05(String per05) {
        this.per05 = per05;
    }

     @XmlElement(name="PER06")
    public String getPer06() {
        return per06;
    }

    public void setPer06(String per06) {
        this.per06 = per06;
    }
    
     @XmlElement(name="PER07")
    public String getPer07() {
        return per07;
    }

    public void setPer07(String per07) {
        this.per07 = per07;
    }
    
     @XmlElement(name="PER08")
    public String getPer08() {
        return per08;
    }

    public void setPer08(String per08) {
        this.per08 = per08;
    }
    
     @XmlElement(name="PER09")
    public String getPer09() {
        return per09;
    }

    public void setPer09(String per09) {
        this.per09 = per09;
    }
    
    @XmlElement(name="PER10")
    public String getPer10() {
        return per10;
    }

    public void setPer10(String per10) {
        this.per10 = per10;
    }
    
     @XmlElement(name="PER11")
    public String getPer11() {
        return per11;
    }

    public void setPer11(String per11) {
        this.per11 = per11;
    }
    
     @XmlElement(name="PER12")
    public String getPer12() {
        return per12;
    }

    public void setPer12(String per12) {
        this.per12 = per12;
    }
    
     @XmlElement(name="PER13_1")
    public String getPer131() {
        return per131;
    }

    public void setPer131(String per131) {
        this.per131 = per131;
    }
    
    @XmlElement(name="PER13_2")
    public String getPer132() {
        return per132;
    }

    public void setPer132(String per132) {
        this.per132 = per132;
    }
    
    @XmlElement(name="PER14_1")
    public String getPer141() {
        return per141;
    }

    public void setPer141(String per141) {
        this.per141 = per141;
    }
    
    @XmlElement(name="PER14_2")
    public String getPer142() {
        return per142;
    }

    public void setPer142(String per142) {
        this.per142 = per142;
    }
    
    @XmlElement(name="PER15")
    public String getPer15() {
        return per15;
    }

    public void setPer15(String per15) {
        this.per15 = per15;
    }
    
    @XmlElement(name="PER16")
    public String getPer16() {
        return per16;
    }

    public void setPer16(String per16) {
        this.per16 = per16;
    }
     
    @XmlElement(name="PER17")
    public String getPer17() {
        return per17;
    }

    public void setPer17(String per17) {
        this.per17 = per17;
    }
    
     @XmlElement(name="PER18")
    public String getPer18() {
        return per18;
    }

    public void setPer18(String per18) {
        this.per18 = per18;
    }

    @XmlTransient
    public Matricula2013Cabecera getMatricula2013Cabecera() {
        return matricula2013Cabecera;
    }

    public void setMatricula2013Cabecera(Matricula2013Cabecera matricula2013Cabecera) {
        this.matricula2013Cabecera = matricula2013Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2013Coordpronoei)) {
            return false;
        }
        Matricula2013Coordpronoei other = (Matricula2013Coordpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

   
}
