/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "constantes")
@NamedQueries({
    @NamedQuery(name = "Constantes.findAll", query = "SELECT c FROM Constantes c"),
    @NamedQuery(name = "Constantes.findByIdConst", query = "SELECT c FROM Constantes c WHERE c.idConst = :idConst"),
    @NamedQuery(name = "Constantes.findByTipConst", query = "SELECT c FROM Constantes c WHERE c.tipConst = :tipConst"),
    @NamedQuery(name = "Constantes.findByCodConst", query = "SELECT c FROM Constantes c WHERE c.codConst = :codConst"),
    @NamedQuery(name = "Constantes.findByDescConst", query = "SELECT c FROM Constantes c WHERE c.descConst = :descConst"),
    @NamedQuery(name = "Constantes.findByAbrConst", query = "SELECT c FROM Constantes c WHERE c.abrConst = :abrConst"),
    @NamedQuery(name = "Constantes.findByAcrConst", query = "SELECT c FROM Constantes c WHERE c.acrConst = :acrConst"),
    @NamedQuery(name = "Constantes.findByValStr", query = "SELECT c FROM Constantes c WHERE c.valStr = :valStr"),
    @NamedQuery(name = "Constantes.findByValDate", query = "SELECT c FROM Constantes c WHERE c.valDate = :valDate"),
    @NamedQuery(name = "Constantes.findByValDatetime", query = "SELECT c FROM Constantes c WHERE c.valDatetime = :valDatetime"),
    @NamedQuery(name = "Constantes.findByValNum1", query = "SELECT c FROM Constantes c WHERE c.valNum1 = :valNum1"),
    @NamedQuery(name = "Constantes.findByValeNum2", query = "SELECT c FROM Constantes c WHERE c.valeNum2 = :valeNum2"),
    @NamedQuery(name = "Constantes.findByValStr1", query = "SELECT c FROM Constantes c WHERE c.valStr1 = :valStr1"),
    @NamedQuery(name = "Constantes.findByValStr2", query = "SELECT c FROM Constantes c WHERE c.valStr2 = :valStr2"),
    @NamedQuery(name = "Constantes.findByUltimo", query = "SELECT c FROM Constantes c WHERE c.ultimo = :ultimo")})
public class Constantes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CONST")
    private Long idConst;
    @Basic(optional = false)
    @Column(name = "TIP_CONST")
    private String tipConst;
    @Basic(optional = false)
    @Column(name = "COD_CONST")
    private String codConst;
    @Column(name = "DESC_CONST")
    private String descConst;
    @Column(name = "ABR_CONST")
    private String abrConst;
    @Column(name = "ACR_CONST")
    private String acrConst;
    @Column(name = "VAL_STR")
    private String valStr;
    @Column(name = "VAL_DATE")
    @Temporal(TemporalType.DATE)
    private Date valDate;
    @Column(name = "VAL_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date valDatetime;
    @Column(name = "VAL_NUM1")
    private Integer valNum1;
    @Column(name = "VALE_NUM2")
    private BigDecimal valeNum2;
    @Column(name = "VAL_STR1")
    private String valStr1;
    @Column(name = "VAL_STR2")
    private String valStr2;
    @Column(name = "ULTIMO")
    private BigInteger ultimo;

    public Constantes() {
    }

    public Constantes(Long idConst) {
        this.idConst = idConst;
    }

    public Constantes(Long idConst, String tipConst, String codConst) {
        this.idConst = idConst;
        this.tipConst = tipConst;
        this.codConst = codConst;
    }

    public Long getIdConst() {
        return idConst;
    }

    public void setIdConst(Long idConst) {
        this.idConst = idConst;
    }

    public String getTipConst() {
        return tipConst;
    }

    public void setTipConst(String tipConst) {
        this.tipConst = tipConst;
    }

    public String getCodConst() {
        return codConst;
    }

    public void setCodConst(String codConst) {
        this.codConst = codConst;
    }

    public String getDescConst() {
        return descConst;
    }

    public void setDescConst(String descConst) {
        this.descConst = descConst;
    }

    public String getAbrConst() {
        return abrConst;
    }

    public void setAbrConst(String abrConst) {
        this.abrConst = abrConst;
    }

    public String getAcrConst() {
        return acrConst;
    }

    public void setAcrConst(String acrConst) {
        this.acrConst = acrConst;
    }

    public String getValStr() {
        return valStr;
    }

    public void setValStr(String valStr) {
        this.valStr = valStr;
    }

    public Date getValDate() {
        return valDate;
    }

    public void setValDate(Date valDate) {
        this.valDate = valDate;
    }

    public Date getValDatetime() {
        return valDatetime;
    }

    public void setValDatetime(Date valDatetime) {
        this.valDatetime = valDatetime;
    }

    public Integer getValNum1() {
        return valNum1;
    }

    public void setValNum1(Integer valNum1) {
        this.valNum1 = valNum1;
    }

    public BigDecimal getValeNum2() {
        return valeNum2;
    }

    public void setValeNum2(BigDecimal valeNum2) {
        this.valeNum2 = valeNum2;
    }

    public String getValStr1() {
        return valStr1;
    }

    public void setValStr1(String valStr1) {
        this.valStr1 = valStr1;
    }

    public String getValStr2() {
        return valStr2;
    }

    public void setValStr2(String valStr2) {
        this.valStr2 = valStr2;
    }

    public BigInteger getUltimo() {
        return ultimo;
    }

    public void setUltimo(BigInteger ultimo) {
        this.ultimo = ultimo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConst != null ? idConst.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Constantes)) {
            return false;
        }
        Constantes other = (Constantes) object;
        if ((this.idConst == null && other.idConst != null) || (this.idConst != null && !this.idConst.equals(other.idConst))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pruebaconstante.domain.Constantes[idConst=" + idConst + "]";
    }

}
