/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "local2011_sec104")
public class Local2011Sec104 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P104_NRO")
    private Integer p104Nro;
    @Column(name = "P104_CM")
    private String p104Cm;
    @Column(name = "P104_ANX")
    private String p104Anx;
    @Column(name = "P104_IE")
    private String p104Ie;
    @Column(name = "P104_NM")
    private String p104Nm;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2011Cabecera local2011Cabecera;

    public Local2011Sec104() {
    }

    public Local2011Sec104(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlTransient
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "P104_CM")
    public String getP104Cm() {
        return p104Cm;
    }

    public void setP104Cm(String p104Cm) {
        this.p104Cm = p104Cm;
    }

    @XmlElement(name = "P104_ANX")
    public String getP104Anx() {
        return p104Anx;
    }

    public void setP104Anx(String p104Anx) {
        this.p104Anx = p104Anx;
    }

    @XmlElement(name = "P104_IE")
    public String getP104Ie() {
        return p104Ie;
    }

    public void setP104Ie(String p104Ie) {
        this.p104Ie = p104Ie;
    }

    @XmlElement(name = "P104_NM")
    public String getP104Nm() {
        return p104Nm;
    }

    public void setP104Nm(String p104Nm) {
        this.p104Nm = p104Nm;
    }

    @XmlTransient
    public Local2011Cabecera getLocal2011Cabecera() {
        return local2011Cabecera;
    }

    public void setLocal2011Cabecera(Local2011Cabecera local2011Cabecera) {
        this.local2011Cabecera = local2011Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2011Sec104)) {
            return false;
        }
        Local2011Sec104 other = (Local2011Sec104) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Local2011Sec104[idEnvio=" + idEnvio + "]";
    }

    @XmlElement(name = "P104_NRO")
    public Integer getP104Nro() {
        return p104Nro;
    }

    /**
     * @param p104Nro the p104Nro to set
     */
    public void setP104Nro(Integer p104Nro) {
        this.p104Nro = p104Nro;
    }

}
