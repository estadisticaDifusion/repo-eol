/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Matricula2012SaaneeMapAdapter.Matricula2012SaaneeList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2012SaaneeMapAdapter extends XmlAdapter<Matricula2012SaaneeList, Map<String, Matricula2012Saanee>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2012SaaneeMapAdapter.class.getName());

    static class Matricula2012SaaneeList {

        private List<Matricula2012Saanee> detalle;

        private Matricula2012SaaneeList(ArrayList<Matricula2012Saanee> lista) {
            detalle = lista;
        }

        public Matricula2012SaaneeList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2012Saanee> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2012Saanee> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2012Saanee> unmarshal(Matricula2012SaaneeList v) throws Exception {
        Map<String, Matricula2012Saanee> map = new HashMap<String, Matricula2012Saanee>();
        for (Matricula2012Saanee detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2012SaaneeList marshal(Map<String, Matricula2012Saanee> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;
        }
        ArrayList<Matricula2012Saanee> lista = new ArrayList<Matricula2012Saanee>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2012Saanee $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2012SaaneeList list = new Matricula2012SaaneeList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
