/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_localpronoei")
public class Matricula2017Localpronoei implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P501_01")
    private String p50101;
    @Column(name = "P501_02")
    private Integer p50102;
    @Column(name = "P501_11")
    private String p50111;
    @Column(name = "P501_12")
    private Integer p50112;
    @Column(name = "P501_21")
    private String p50121;
    @Column(name = "P501_22")
    private Integer p50122;
    @Column(name = "P501_31")
    private String p50131;
    @Column(name = "P501_32")
    private Integer p50132;
    @Column(name = "P501_41")
    private String p50141;
    @Column(name = "P501_42")
    private Integer p50142;
    @Column(name = "P501_51")
    private String p50151;
    @Column(name = "P501_52")
    private Integer p50152;
    @Column(name = "P501_61")
    private String p50161;
    @Column(name = "P502_A")
    private String p502A;
    @Column(name = "P502_M")
    private String p502M;
    @Column(name = "P503")
    private String p503;
    @Column(name = "P503_ESP")
    private String p503Esp;
    @Column(name = "P504")
    private String p504;
    @Column(name = "P504_7_CM")
    private String p5047Cm;
    @Column(name = "P504_7_NM")
    private String p5047Nm;
    @Column(name = "P504_ESP")
    private String p504Esp;
    @Column(name = "P505")
    private String p505;
    @Column(name = "P506")
    private String p506;
    @Column(name = "P507")
    private String p507;
    @Column(name = "P508")
    private String p508;
    @Column(name = "P508_SUM")
    private String p508Sum;
    @Column(name = "P509")
    private String p509;
    @Column(name = "P509_SUM")
    private String p509Sum;
    @Column(name = "P509_ESP")
    private String p509Esp;
    @Column(name = "P510")
    private String p510;
    @Column(name = "P511")
    private String p511;
    @Column(name = "P512")
    private String p512;
    @Column(name = "P513")
    private String p513;
    @Column(name = "P514")
    private Integer p514;
    @Column(name = "P515")
    private String p515;
    @Column(name = "P516_MAT")
    private String p516Mat;
    @Column(name = "P516_EST")
    private String p516Est;
    @Column(name = "P518")
    private String p518;
    @Column(name = "P518_A")
    private Integer p518A;
    @Column(name = "P518_M")
    private Integer p518M;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2017Cabecera matricula2017Cabecera;

    public Matricula2017Localpronoei() {
    }

    public Matricula2017Localpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "P501_01")
    public String getP50101() {
        return p50101;
    }

    public void setP50101(String p50101) {
        this.p50101 = p50101;
    }

    @XmlElement(name = "P501_02")
    public Integer getP50102() {
        return p50102;
    }

    public void setP50102(Integer p50102) {
        this.p50102 = p50102;
    }

    @XmlElement(name = "P501_11")
    public String getP50111() {
        return p50111;
    }

    public void setP50111(String p50111) {
        this.p50111 = p50111;
    }

    @XmlElement(name = "P501_12")
    public Integer getP50112() {
        return p50112;
    }

    public void setP50112(Integer p50112) {
        this.p50112 = p50112;
    }

    @XmlElement(name = "P501_21")
    public String getP50121() {
        return p50121;
    }

    public void setP50121(String p50121) {
        this.p50121 = p50121;
    }

    @XmlElement(name = "P501_22")
    public Integer getP50122() {
        return p50122;
    }

    public void setP50122(Integer p50122) {
        this.p50122 = p50122;
    }

    @XmlElement(name = "P501_31")
    public String getP50131() {
        return p50131;
    }

    public void setP50131(String p50131) {
        this.p50131 = p50131;
    }

    @XmlElement(name = "P501_32")
    public Integer getP50132() {
        return p50132;
    }

    public void setP50132(Integer p50132) {
        this.p50132 = p50132;
    }

    @XmlElement(name = "P501_41")
    public String getP50141() {
        return p50141;
    }

    public void setP50141(String p50141) {
        this.p50141 = p50141;
    }

    @XmlElement(name = "P501_42")
    public Integer getP50142() {
        return p50142;
    }

    public void setP50142(Integer p50142) {
        this.p50142 = p50142;
    }

    @XmlElement(name = "P501_51")
    public String getP50151() {
        return p50151;
    }

    public void setP50151(String p50151) {
        this.p50151 = p50151;
    }

    @XmlElement(name = "P501_52")
    public Integer getP50152() {
        return p50152;
    }

    public void setP50152(Integer p50152) {
        this.p50152 = p50152;
    }

    @XmlElement(name = "P501_61")
    public String getP50161() {
        return p50161;
    }

    public void setP50161(String p50161) {
        this.p50161 = p50161;
    }

    @XmlElement(name = "P502_A")
    public String getP502A() {
        return p502A;
    }

    public void setP502A(String p502A) {
        this.p502A = p502A;
    }

    @XmlElement(name = "P502_M")
    public String getP502M() {
        return p502M;
    }

    public void setP502M(String p502M) {
        this.p502M = p502M;
    }

    @XmlElement(name = "P503")
    public String getP503() {
        return p503;
    }

    public void setP503(String p503) {
        this.p503 = p503;
    }

    @XmlElement(name = "P503_ESP")
    public String getP503Esp() {
        return p503Esp;
    }

    public void setP503Esp(String p503Esp) {
        this.p503Esp = p503Esp;
    }

    @XmlElement(name = "P504")
    public String getP504() {
        return p504;
    }

    public void setP504(String p504) {
        this.p504 = p504;
    }

    @XmlElement(name = "P504_7_CM")
    public String getP5047Cm() {
        return p5047Cm;
    }

    public void setP5047Cm(String p5047Cm) {
        this.p5047Cm = p5047Cm;
    }

    @XmlElement(name = "P504_7_NM")
    public String getP5047Nm() {
        return p5047Nm;
    }

    public void setP5047Nm(String p5047Nm) {
        this.p5047Nm = p5047Nm;
    }

    @XmlElement(name = "P504_ESP")
    public String getP504Esp() {
        return p504Esp;
    }

    public void setP504Esp(String p504Esp) {
        this.p504Esp = p504Esp;
    }

    @XmlElement(name = "P505")
    public String getP505() {
        return p505;
    }

    public void setP505(String p505) {
        this.p505 = p505;
    }

    @XmlElement(name = "P506")
    public String getP506() {
        return p506;
    }

    public void setP506(String p506) {
        this.p506 = p506;
    }

    @XmlElement(name = "P507")
    public String getP507() {
        return p507;
    }

    public void setP507(String p507) {
        this.p507 = p507;
    }

    @XmlElement(name = "P508")
    public String getP508() {
        return p508;
    }

    public void setP508(String p508) {
        this.p508 = p508;
    }

    @XmlElement(name = "P508_SUM")
    public String getP508Sum() {
        return p508Sum;
    }

    public void setP508Sum(String p508Sum) {
        this.p508Sum = p508Sum;
    }

    @XmlElement(name = "P509")
    public String getP509() {
        return p509;
    }

    public void setP509(String p509) {
        this.p509 = p509;
    }

    @XmlElement(name = "P509_SUM")
    public String getP509Sum() {
        return p509Sum;
    }

    public void setP509Sum(String p509Sum) {
        this.p509Sum = p509Sum;
    }

    @XmlElement(name = "P509_ESP")
    public String getP509Esp() {
        return p509Esp;
    }

    public void setP509Esp(String p509Esp) {
        this.p509Esp = p509Esp;
    }

    @XmlElement(name = "P510")
    public String getP510() {
        return p510;
    }

    public void setP510(String p510) {
        this.p510 = p510;
    }

    @XmlElement(name = "P511")
    public String getP511() {
        return p511;
    }

    public void setP511(String p511) {
        this.p511 = p511;
    }

    @XmlElement(name = "P512")
    public String getP512() {
        return p512;
    }

    public void setP512(String p512) {
        this.p512 = p512;
    }

    @XmlElement(name = "P513")
    public String getP513() {
        return p513;
    }

    public void setP513(String p513) {
        this.p513 = p513;
    }

    @XmlElement(name = "P514")
    public Integer getP514() {
        return p514;
    }

    public void setP514(Integer p514) {
        this.p514 = p514;
    }

    @XmlElement(name = "P515")
    public String getP515() {
        return p515;
    }

    public void setP515(String p515) {
        this.p515 = p515;
    }

    @XmlElement(name = "P516_MAT")
    public String getP516Mat() {
        return p516Mat;
    }

    public void setP516Mat(String p516Mat) {
        this.p516Mat = p516Mat;
    }

    @XmlElement(name = "P516_EST")
    public String getP516Est() {
        return p516Est;
    }

    public void setP516Est(String p516Est) {
        this.p516Est = p516Est;
    }

    @XmlElement(name = "P518")
    public String getP518() {
        return p518;
    }

    public void setP518(String p518) {
        this.p518 = p518;
    }

    @XmlElement(name = "P518_A")
    public Integer getP518A() {
        return p518A;
    }

    public void setP518A(Integer p518A) {
        this.p518A = p518A;
    }

    @XmlElement(name = "P518_M")
    public Integer getP518M() {
        return p518M;
    }

    public void setP518M(Integer p518M) {
        this.p518M = p518M;
    }

    @XmlTransient
    public Matricula2017Cabecera getMatricula2017Cabecera() {
        return matricula2017Cabecera;
    }

    public void setMatricula2017Cabecera(Matricula2017Cabecera matricula2017Cabecera) {
        this.matricula2017Cabecera = matricula2017Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2017Localpronoei)) {
            return false;
        }
        Matricula2017Localpronoei other = (Matricula2017Localpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Matricula2017Localpronoei[idEnvio=" + idEnvio + "]";
    }
}
