/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "local2011_sec300")
public class Local2011Sec300 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P302_1")
    private Integer p3021;
    @Column(name = "P302_2")
    private String p3022;
    @Column(name = "P302_3M1")
    private String p3023m1;
    @Column(name = "P302_3M2")
    private String p3023m2;
    @Column(name = "P302_3M3")
    private String p3023m3;
    @Column(name = "P302_3M4")
    private Integer p3023m4;
    @Column(name = "P302_3T1")
    private String p3023t1;
    @Column(name = "P302_3T2")
    private String p3023t2;
    @Column(name = "P302_3T3")
    private String p3023t3;
    @Column(name = "P302_3T4")
    private Integer p3023t4;
    @Column(name = "P302_3N1")
    private String p3023n1;
    @Column(name = "P302_3N2")
    private String p3023n2;
    @Column(name = "P302_3N3")
    private String p3023n3;
    @Column(name = "P302_3N4")
    private Integer p3023n4;
    @Column(name = "P302_4")
    private Integer p3024;
    @Column(name = "P302_5")
    private String p3025;
    
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2011Cabecera local2011Cabecera;

    public Local2011Sec300() {
    }

    public Local2011Sec300(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlTransient
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "P302_1")
    public Integer getP3021() {
        return p3021;
    }

    public void setP3021(Integer p3021) {
        this.p3021 = p3021;
    }

    @XmlElement(name = "P302_2")
    public String getP3022() {
        return p3022;
    }

    public void setP3022(String p3022) {
        this.p3022 = p3022;
    }

    @XmlElement(name = "P302_3M1")
    public String getP3023m1() {
        return p3023m1;
    }

    public void setP3023m1(String p3023m1) {
        this.p3023m1 = p3023m1;
    }

    @XmlElement(name = "P302_3M2")
    public String getP3023m2() {
        return p3023m2;
    }

    public void setP3023m2(String p3023m2) {
        this.p3023m2 = p3023m2;
    }

    @XmlElement(name = "P302_3M3")
    public String getP3023m3() {
        return p3023m3;
    }

    public void setP3023m3(String p3023m3) {
        this.p3023m3 = p3023m3;
    }

    @XmlElement(name = "P302_3M4")
    public Integer getP3023m4() {
        return p3023m4;
    }

    public void setP3023m4(Integer p3023m4) {
        this.p3023m4 = p3023m4;
    }

    @XmlElement(name = "P302_3T1")
    public String getP3023t1() {
        return p3023t1;
    }

    public void setP3023t1(String p3023t1) {
        this.p3023t1 = p3023t1;
    }

    @XmlElement(name = "P302_3T2")
    public String getP3023t2() {
        return p3023t2;
    }

    public void setP3023t2(String p3023t2) {
        this.p3023t2 = p3023t2;
    }

    @XmlElement(name = "P302_3T3")
    public String getP3023t3() {
        return p3023t3;
    }

    public void setP3023t3(String p3023t3) {
        this.p3023t3 = p3023t3;
    }

    @XmlElement(name = "P302_3T4")
    public Integer getP3023t4() {
        return p3023t4;
    }

    public void setP3023t4(Integer p3023t4) {
        this.p3023t4 = p3023t4;
    }

    @XmlElement(name = "P302_3N1")
    public String getP3023n1() {
        return p3023n1;
    }

    public void setP3023n1(String p3023n1) {
        this.p3023n1 = p3023n1;
    }

    @XmlElement(name = "P302_3N2")
    public String getP3023n2() {
        return p3023n2;
    }

    public void setP3023n2(String p3023n2) {
        this.p3023n2 = p3023n2;
    }

    @XmlElement(name = "P302_3N3")
    public String getP3023n3() {
        return p3023n3;
    }

    public void setP3023n3(String p3023n3) {
        this.p3023n3 = p3023n3;
    }

    @XmlElement(name = "P302_3N4")
    public Integer getP3023n4() {
        return p3023n4;
    }

    public void setP3023n4(Integer p3023n4) {
        this.p3023n4 = p3023n4;
    }

    @XmlElement(name = "P302_4")
    public Integer getP3024() {
        return p3024;
    }

    public void setP3024(Integer p3024) {
        this.p3024 = p3024;
    }

    @XmlElement(name = "P302_5")
    public String getP3025() {
        return p3025;
    }

    public void setP3025(String p3025) {
        this.p3025 = p3025;
    }

    @XmlTransient
    public Local2011Cabecera getLocal2011Cabecera() {
        return local2011Cabecera;
    }

    public void setLocal2011Cabecera(Local2011Cabecera local2011Cabecera) {
        this.local2011Cabecera = local2011Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2011Sec300)) {
            return false;
        }
        Local2011Sec300 other = (Local2011Sec300) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jsonproy.domain.Local2011Sec300[idEnvio=" + idEnvio + "]";
    }

}
