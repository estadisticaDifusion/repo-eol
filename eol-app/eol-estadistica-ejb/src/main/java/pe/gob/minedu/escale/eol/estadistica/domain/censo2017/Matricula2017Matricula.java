/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_matricula")
public class Matricula2017Matricula implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)

	@Column(name = "ID_ENVIO")
	private Long idEnvio;

	@Column(name = "CUADRO")
	private String cuadro;
	
    //@ManyToOne
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
	private Matricula2017Cabecera matricula2017Cabecera;
	/*
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Matricula")
	private List<Matricula2017MatriculaFila> matricula2017MatriculaFilaList;
*/
	public Matricula2017Matricula() {
	}

	public Matricula2017Matricula(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	//@XmlElement(name = "CUADRO")
	public String getCuadro() {
		return cuadro;
	}

	public void setCuadro(String cuadro) {
		this.cuadro = cuadro;
	}

	//@XmlTransient
	public Matricula2017Cabecera getMatricula2017Cabecera() {
		return matricula2017Cabecera;
	}

	public void setMatricula2017Cabecera(Matricula2017Cabecera matricula2017Cabecera) {
		this.matricula2017Cabecera = matricula2017Cabecera;
	}

	/*
	@XmlElement(name = "MATRICULA_FILAS")
	public List<Matricula2017MatriculaFila> getMatricula2017MatriculaFilaList() {
		return matricula2017MatriculaFilaList;
	}

	public void setMatricula2017MatriculaFilaList(List<Matricula2017MatriculaFila> matricula2017MatriculaFilaList) {
		this.matricula2017MatriculaFilaList = matricula2017MatriculaFilaList;
	}
*/
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2017Matricula)) {
			return false;
		}
		Matricula2017Matricula other = (Matricula2017Matricula) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.domain.censo2017.Matricula2017Matricula[idEnvio=" + idEnvio + "]";
	}

}
