/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "local2012_sec300")
public class Local2012Sec300 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P302_1")
    private Integer p3021;
    @Column(name = "P302_2")
    private Integer p3022;
    @Column(name = "P302_3")
    private Integer p3023;
    @Column(name = "P302_4")
    private String p3024;
    @Column(name = "P302_5")
    private String p3025;
    @Column(name = "P302_6")
    private String p3026;
    @Column(name = "P302_7")
    private String p3027;
    @Column(name = "P302_8")
    private String p3028;
    @Column(name = "P302_9")
    private Integer p3029;
    @Column(name = "P302_10")
    private Integer p30210;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2012Cabecera local2012Cabecera;

    public Local2012Sec300() {
    }

    public Local2012Sec300(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P302_1")
    public Integer getP3021() {
        return p3021;
    }

    public void setP3021(Integer p3021) {
        this.p3021 = p3021;
    }

    @XmlElement(name="P302_2")
    public Integer getP3022() {
        return p3022;
    }

    public void setP3022(Integer p3022) {
        this.p3022 = p3022;
    }

    @XmlElement(name="P302_3")
    public Integer getP3023() {
        return p3023;
    }

    public void setP3023(Integer p3023) {
        this.p3023 = p3023;
    }

    @XmlElement(name="P302_4")
    public String getP3024() {
        return p3024;
    }

    public void setP3024(String p3024) {
        this.p3024 = p3024;
    }

    @XmlElement(name="P302_5")
    public String getP3025() {
        return p3025;
    }

    public void setP3025(String p3025) {
        this.p3025 = p3025;
    }

    @XmlElement(name="P302_6")
    public String getP3026() {
        return p3026;
    }

    public void setP3026(String p3026) {
        this.p3026 = p3026;
    }

    @XmlElement(name="P302_7")
    public String getP3027() {
        return p3027;
    }

    public void setP3027(String p3027) {
        this.p3027 = p3027;
    }

    @XmlElement(name="P302_8")
    public String getP3028() {
        return p3028;
    }

    public void setP3028(String p3028) {
        this.p3028 = p3028;
    }

    @XmlElement(name="P302_9")
    public Integer getP3029() {
        return p3029;
    }

    public void setP3029(Integer p3029) {
        this.p3029 = p3029;
    }

    @XmlElement(name="P302_10")
    public Integer getP30210() {
        return p30210;
    }

    public void setP30210(Integer p30210) {
        this.p30210 = p30210;
    }

    @XmlTransient
    public Local2012Cabecera getLocal2012Cabecera() {
        return local2012Cabecera;
    }

    public void setLocal2012Cabecera(Local2012Cabecera local2012Cabecera) {
        this.local2012Cabecera = local2012Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2012Sec300)) {
            return false;
        }
        Local2012Sec300 other = (Local2012Sec300) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    

}
