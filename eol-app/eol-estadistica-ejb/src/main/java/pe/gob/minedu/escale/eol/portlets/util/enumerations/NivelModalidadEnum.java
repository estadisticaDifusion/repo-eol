/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.util.enumerations;

/**
 *
 * @author JMATAMOROS
 */
public enum NivelModalidadEnum {
    INICIAL_CUNA("A1","INICIAL_CUNA"),
    INICIAL_JARDIN("A2","INICIAL JARDIN"),
    INICIAL_CUNA_Y_JARDIN("A3","INICIAL CUNA Y JARDIN"),
    INICIAL_CUNA_Y_JARDIN_ARTI("A4","INICIAL ARTICULACION"),
    INICIAL_NO_ESCOLARIZADO_A5("A5","INICIAL NO ESCOLARIZADO"),
    INICIAL_NO_ESCOLARIZADO_A_("A_","INICIAL NO ESCOLARIZADO"),
    PRIMARIA_DE_MENORES("B0","PRIMARIA DE MENORES"),
    PRIMARIA_DE_ADULTOS("C0","PRIMARIA DE ADULTOS"),
    BASICA_ALTERNATIVA("DO","BASICA ALTERNATIVA"),
    EDUCACION_ESPECIAL("E0","EDUCACION ESPECIAL"),
    SECUNDARIA_DE_MENORES("F0","SECUNDARIA DE MENORES"),
    SECUNDARIA_DE_ADULTOS("G0","SECUNDARIA DE ADULTOS"),
    FORMACION_MAGISTERIAL_ISP("K0","FORMACION MAGISTERIAL ISP"),
    CETPRO_("L0","CETPRO"),
    ESCUELAS_DE_FORMACION_ARTISTICA("M0","ESCUELAS DE FORMACION ARTISTICA"),
    SUPERIOR_TECNOLOGICA_IST("T0","SUPERIOR TECNOLOGICA IST");
     

    private String cod;
    private String descri;


    NivelModalidadEnum(String cod,String descri)
    {   this.cod=cod;
        
        this.descri=descri;
    }

    /**
     * @return the cod
     */
    public String getCod() {
        return cod;
    }



    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

}
