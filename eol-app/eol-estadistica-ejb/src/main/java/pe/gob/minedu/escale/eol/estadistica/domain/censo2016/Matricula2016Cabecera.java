package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2016_cabecera")
public class Matricula2016Cabecera implements Serializable {

    private static final long serialVersionUID = 1L;

    public final static String CEDULA_01A = "c01a";
    public final static String CEDULA_02A = "c02a";
    public final static String CEDULA_03A = "c03a";
    public final static String CEDULA_04A = "c04a";
    public final static String CEDULA_04AI = "c04ai";
    public final static String CEDULA_04AA = "c04aa";
    public final static String CEDULA_05A = "c05a";
    public final static String CEDULA_06A = "c06a";
    public final static String CEDULA_07A = "c07a";
    public final static String CEDULA_08A = "c08a";
    public final static String CEDULA_09A = "c09a";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NROCED")
    private String nroced;
    @Basic(optional = false)
    @Column(name = "COD_MOD")
    private String codMod;
    @Basic(optional = false)
    @Column(name = "ANEXO")
    private String anexo;
    @Basic(optional = false)
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Basic(optional = false)
    @Column(name = "CEN_EDU")
    private String cenEdu;
    @Basic(optional = false)
    @Column(name = "NIV_MOD")
    private String nivMod;
    @Column(name = "MODATEN")
    private String modaten;
    @Column(name = "FORMATEN")
    private String formaten;
    @Column(name = "TIPOPROG")
    private String tipoprog;
    @Column(name = "GESTION")
    private String gestion;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "LOCALIDAD")
    private String localidad;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "TIPONEE")
    private String tiponee;
    @Column(name = "FORMA_PR")
    private String formaPr;
    @Column(name = "FORMA_SP")
    private String formaSp;
    @Column(name = "FORMA_AD")
    private String formaAd;
    @Column(name = "DNI_CORD")
    private String dniCord;
    @Column(name = "TIPOIESUP")
    private String tipoiesup;
    @Column(name = "EGESTORA")
    private String egestora;
    @Column(name = "TIPODFINA_1")
    private String tipodfina1;
    @Column(name = "TIPODFINA_2")
    private String tipodfina2;
    @Column(name = "TIPODFINA_3")
    private String tipodfina3;
    @Column(name = "TIPODFINA_4")
    private String tipodfina4;
    @Column(name = "NRORD_CRE")
    private String nrordCre;
    @Column(name = "FECRES_C_D")
    private String fecresCD;
    @Column(name = "FECRES_C_M")
    private String fecresCM;
    @Column(name = "FECRES_C_A")
    private String fecresCA;
    @Column(name = "NRORD_REN")
    private String nrordRen;
    @Column(name = "FECRES_R_D")
    private String fecresRD;
    @Column(name = "FECRES_R_M")
    private String fecresRM;
    @Column(name = "FECRES_R_A")
    private String fecresRA;
    @Column(name = "P101_C5")
    private String p101C5;
    @Column(name = "P101_C5_1")
    private String p101C51;
    @Column(name = "P101_C5_2")
    private String p101C52;
    @Column(name = "P101_C5_3")
    private String p101C53;
    @Column(name = "P101_DD")
    private String p101Dd;
    @Column(name = "P101_MM")
    private String p101Mm;
    @Column(name = "P101_C2")
    private String p101C2;
    @Column(name = "P102_DD")
    private String p102Dd;
    @Column(name = "P102_MM")
    private String p102Mm;
    @Column(name = "P103_13")
    private String p10313;
    @Column(name = "P102_C8")
    private String p102C8;
    @Column(name = "SERV_IE_1")
    private String servIe1;
    @Column(name = "SERV_IE_2")
    private String servIe2;
    @Column(name = "SERV_IE_3")
    private String servIe3;
    @Column(name = "SERV_IE_4")
    private String servIe4;
    @Column(name = "SERV_IE_5")
    private String servIe5;
    @Column(name = "SERV_IE_6")
    private String servIe6;
    @Column(name = "SERV_IE_7")
    private String servIe7;
    @Column(name = "P104_C2")
    private String p104C2;
    @Column(name = "P105_C3")
    private String p105C3;
    @Column(name = "P105_C3_1")
    private String p105C31;
    @Column(name = "P105_C3_1RD")
    private String p105C31rd;
    @Column(name = "P105_C3_2")
    private String p105C32;
    @Column(name = "P105_C3_2RD")
    private String p105C32rd;
    @Column(name = "P105_106")
    private String p105106;
    @Column(name = "P105_106_ESP")
    private String p105106Esp;
    @Column(name = "P101_C4_1PE")
    private String p101C41pe;
    @Column(name = "P101_C4_1ID")
    private String p101C41id;
    @Column(name = "P101_C4_1IM")
    private String p101C41im;
    @Column(name = "P101_C4_1IA")
    private String p101C41ia;
    @Column(name = "P101_C4_1FD")
    private String p101C41fd;
    @Column(name = "P101_C4_1FM")
    private String p101C41fm;
    @Column(name = "P101_C4_1FA")
    private String p101C41fa;
    @Column(name = "P101_C4_2PE")
    private String p101C42pe;
    @Column(name = "P101_C4_2ID")
    private String p101C42id;
    @Column(name = "P101_C4_2IM")
    private String p101C42im;
    @Column(name = "P101_C4_2IA")
    private String p101C42ia;
    @Column(name = "P101_C4_2FD")
    private String p101C42fd;
    @Column(name = "P101_C4_2FM")
    private String p101C42fm;
    @Column(name = "P101_C4_2FA")
    private String p101C42fa;
    @Column(name = "P101_C4_3PE")
    private String p101C43pe;
    @Column(name = "P101_C4_3ID")
    private String p101C43id;
    @Column(name = "P101_C4_3IM")
    private String p101C43im;
    @Column(name = "P101_C4_3IA")
    private String p101C43ia;
    @Column(name = "P101_C4_3FD")
    private String p101C43fd;
    @Column(name = "P101_C4_3FM")
    private String p101C43fm;
    @Column(name = "P101_C4_3FA")
    private String p101C43fa;
    @Column(name = "P101_C4_4PE")
    private String p101C44pe;
    @Column(name = "P101_C4_4ID")
    private String p101C44id;
    @Column(name = "P101_C4_4IM")
    private String p101C44im;
    @Column(name = "P101_C4_4IA")
    private String p101C44ia;
    @Column(name = "P101_C4_4FD")
    private String p101C44fd;
    @Column(name = "P101_C4_4FM")
    private String p101C44fm;
    @Column(name = "P101_C4_4FA")
    private String p101C44fa;
    @Column(name = "P102_C4")
    private String p102C4;
    @Column(name = "P103_C4")
    private Integer p103C4;
    @Column(name = "P104_C4_LU")
    private String p104C4Lu;
    @Column(name = "P104_C4_QL")
    private Integer p104C4Ql;
    @Column(name = "P104_C4_MA")
    private String p104C4Ma;
    @Column(name = "P104_C4_QM")
    private Integer p104C4Qm;
    @Column(name = "P104_C4_MI")
    private String p104C4Mi;
    @Column(name = "P104_C4_QN")
    private Integer p104C4Qn;
    @Column(name = "P104_C4_JU")
    private String p104C4Ju;
    @Column(name = "P104_C4_QJ")
    private Integer p104C4Qj;
    @Column(name = "P104_C4_VI")
    private String p104C4Vi;
    @Column(name = "P104_C4_QV")
    private Integer p104C4Qv;
    @Column(name = "P104_C4_SA")
    private String p104C4Sa;
    @Column(name = "P104_C4_QS")
    private Integer p104C4Qs;
    @Column(name = "P104_C4_DO")
    private String p104C4Do;
    @Column(name = "P104_C4_QD")
    private Integer p104C4Qd;
    @Column(name = "P105_C4A_1")
    private String p105C4a1;
    @Column(name = "P105_C4A_2")
    private String p105C4a2;
    @Column(name = "P105_C4A_3")
    private String p105C4a3;
    @Column(name = "P105_C4A_4")
    private String p105C4a4;
    @Column(name = "P105_6_C41")
    private String p1056C41;
    @Column(name = "P105_6_C42")
    private String p1056C42;
    @Column(name = "P105_6_C43")
    private String p1056C43;
    @Column(name = "P105_6_C44")
    private String p1056C44;
    @Column(name = "P105_6_C45")
    private String p1056C45;
    @Column(name = "P105_6_C46")
    private String p1056C46;
    @Column(name = "P105_6_C47")
    private String p1056C47;
    @Column(name = "P105_6_C48")
    private String p1056C48;
    @Column(name = "P105_6_C4E")
    private String p1056C4e;
    @Column(name = "P106_C4I_C")
    private String p106C4iC;
    @Column(name = "P107_C4I_Q")
    private Integer p107C4iQ;
    @Column(name = "P108_C4_LU")
    private String p108C4Lu;
    @Column(name = "P108_C4_QL")
    private Integer p108C4Ql;
    @Column(name = "P108_C4_MA")
    private String p108C4Ma;
    @Column(name = "P108_C4_QM")
    private Integer p108C4Qm;
    @Column(name = "P108_C4_MI")
    private String p108C4Mi;
    @Column(name = "P108_C4_QN")
    private Integer p108C4Qn;
    @Column(name = "P108_C4_JU")
    private String p108C4Ju;
    @Column(name = "P108_C4_QJ")
    private Integer p108C4Qj;
    @Column(name = "P108_C4_VI")
    private String p108C4Vi;
    @Column(name = "P108_C4_QV")
    private Integer p108C4Qv;
    @Column(name = "P108_C4_SA")
    private String p108C4Sa;
    @Column(name = "P108_C4_QS")
    private Integer p108C4Qs;
    @Column(name = "P108_C4_DO")
    private String p108C4Do;
    @Column(name = "P108_C4_QD")
    private Integer p108C4Qd;
    @Column(name = "PEIB_1")
    private String peib1;
    @Column(name = "PEIB_1_RES")
    private String peib1Res;
    @Column(name = "PEIB_2")
    private String peib2;
    @Column(name = "PEIB_3")
    private String peib3;
    @Column(name = "PEIB_3_ESP")
    private String peib3Esp;
    @Column(name = "PEIB_4")
    private String peib4;
    @Column(name = "P1055_C2_1")
    private String p1055C21;
    @Column(name = "P1055_C2_2")
    private String p1055C22;
    @Column(name = "P1074_C3_1")
    private String p1074C31;
    @Column(name = "P1074_C3_2")
    private String p1074C32;
    @Column(name = "P1075_C3")
    private String p1075C3;
    @Column(name = "P1075_C3_ESP")
    private String p1075C3Esp;
    @Column(name = "P1075_C3_1")
    private String p1075C31;
    @Column(name = "P1075_C3_2")
    private String p1075C32;
    @Column(name = "P1075_C3_3")
    private String p1075C33;
    @Column(name = "P1075_C3_4")
    private String p1075C34;
    @Column(name = "P1075_C3_5")
    private String p1075C35;
    @Column(name = "P1075_C3_6")
    private String p1075C36;
    @Column(name = "P1076_C3")
    private String p1076C3;
    @Column(name = "P1081_C3_1")
    private String p1081C31;
    @Column(name = "P1081_C3_2")
    private Integer p1081C32;
    @Column(name = "P1082_C3_11")
    private String p1082C311;
    @Column(name = "P1082_C3_12")
    private String p1082C312;
    @Column(name = "P1082_C3_13")
    private String p1082C313;
    @Column(name = "P1082_C3_14")
    private String p1082C314;
    @Column(name = "P1082_C3_21")
    private String p1082C321;
    @Column(name = "P1082_C3_22")
    private String p1082C322;
    @Column(name = "P1082_C3_23")
    private String p1082C323;
    @Column(name = "P1082_C3_24")
    private String p1082C324;
    @Column(name = "P1082_C3_31")
    private String p1082C331;
    @Column(name = "P1082_C3_32")
    private String p1082C332;
    @Column(name = "P1082_C3_33")
    private String p1082C333;
    @Column(name = "P1082_C3_34")
    private String p1082C334;
    @Column(name = "P1082_C3_41")
    private String p1082C341;
    @Column(name = "P1082_C3_42")
    private String p1082C342;
    @Column(name = "P1082_C3_43")
    private String p1082C343;
    @Column(name = "P1082_C3_44")
    private String p1082C344;
    @Column(name = "P1082_C3_51")
    private String p1082C351;
    @Column(name = "P1082_C3_52")
    private String p1082C352;
    @Column(name = "P1082_C3_53")
    private String p1082C353;
    @Column(name = "P1082_C3_54")
    private String p1082C354;
    @Column(name = "P401_C12")
    private String p401C12;
    @Column(name = "P402_C12_A")
    private String p402C12A;
    @Column(name = "P402_C12_M")
    private String p402C12M;
    @Column(name = "P403_C12_1")
    private Integer p403C121;
    @Column(name = "P403_C12_2")
    private Integer p403C122;
    @Column(name = "P403_C12_3")
    private Integer p403C123;
    @Column(name = "P404_C12")
    private String p404C12;
    @Column(name = "P405_C12_A")
    private String p405C12A;
    @Column(name = "P405_C12_M")
    private String p405C12M;
    @Column(name = "P406_C12_1")
    private Integer p406C121;
    @Column(name = "P406_C12_2")
    private Integer p406C122;
    @Column(name = "P406_C12_3")
    private Integer p406C123;
    @Column(name = "P407_C1")
    private String p407C1;
    @Column(name = "P401_C3")
    private String p401C3;
    @Column(name = "P402_C3_A")
    private String p402C3A;
    @Column(name = "P402_C3_M")
    private String p402C3M;
    @Column(name = "P405_C3")
    private String p405C3;
    @Column(name = "P406_C3_A")
    private String p406C3A;
    @Column(name = "P406_C3_M")
    private String p406C3M;
    @Column(name = "P4081_C3")
    private String p4081C3;
    @Column(name = "P4082_C3_A")
    private String p4082C3A;
    @Column(name = "P4082_C3_M")
    private String p4082C3M;
    @Column(name = "P401_C4")
    private String p401C4;
    @Column(name = "P405_C4")
    private String p405C4;
    @Column(name = "P406_C4_1")
    private String p406C41;
    @Column(name = "P406_C4_2")
    private String p406C42;
    @Column(name = "P406_C4_3")
    private String p406C43;
    @Column(name = "P406_C4_4")
    private String p406C44;
    @Column(name = "P406_C4_5")
    private String p406C45;
    @Column(name = "P406_C4_6")
    private String p406C46;
    @Column(name = "P406_C4_7")
    private String p406C47;
    @Column(name = "P406_C4_8")
    private String p406C48;
    @Column(name = "P406_C4_9")
    private String p406C49;
    @Column(name = "P406_C4_9E")
    private String p406C49e;
    @Column(name = "P406_C4_10")
    private String p406C410;
    @Column(name = "P406_C4_10E")
    private String p406C410e;
    @Column(name = "P406_C4_11")
    private String p406C411;
    @Column(name = "P407_C4_11")
    private String p407C411;
    @Column(name = "P407_C4_12")
    private Integer p407C412;
    @Column(name = "P407_C4_13")
    private Integer p407C413;
    @Column(name = "P407_C4_21")
    private String p407C421;
    @Column(name = "P407_C4_22")
    private Integer p407C422;
    @Column(name = "P407_C4_23")
    private Integer p407C423;
    @Column(name = "P407_C4_31")
    private String p407C431;
    @Column(name = "P407_C4_32")
    private Integer p407C432;
    @Column(name = "P407_C4_33")
    private Integer p407C433;
    @Column(name = "P407_C4_41")
    private String p407C441;
    @Column(name = "P408_C4_1")
    private Integer p408C41;
    @Column(name = "P408_C4_2")
    private Integer p408C42;
    @Column(name = "P408_C4_3")
    private Integer p408C43;
    @Column(name = "P502_C1")
    private String p502C1;
    @Column(name = "P502_C1_D")
    private String p502C1D;
    @Column(name = "P502_C1_M")
    private String p502C1M;
    @Column(name = "P502_C1_A")
    private String p502C1A;
    @Column(name = "TDOCENTES")
    private Integer tdocentes;
    @Column(name = "TDOCAULA")
    private Integer tdocaula;
    @Column(name = "TAUXILIAR")
    private Integer tauxiliar;
    @Column(name = "TADMINIST")
    private Integer tadminist;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "FUENTE")
    private String fuente;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;

/*

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2016Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2016Saanee> matricula2016SaaneeList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2016Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2016Carreras> matricula2016CarrerasList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2016Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2016Personal> matricula2016PersonalList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2016Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2016Localpronoei> matricula2016LocalpronoeiList;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2016Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Map<String,Matricula2016Seccion> detalleSeccion;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2016Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String,Matricula2016Recursos> detalleRecursos;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2016Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Map<String,Matricula2016Matricula> detalleMatricula;
*/
    @Transient
    private long token;

    @Transient
    private String msg;

    @Transient
    private String estadoRpt;

    public Matricula2016Cabecera() {
    }

    public Matricula2016Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2016Cabecera(Long idEnvio, String nroced, String codMod, String anexo, String codlocal, String cenEdu, String nivMod) {
        this.idEnvio = idEnvio;
        this.nroced = nroced;
        this.codMod = codMod;
        this.anexo = anexo;
        this.codlocal = codlocal;
        this.cenEdu = cenEdu;
        this.nivMod = nivMod;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="NROCED")
    public String getNroced() {
        return nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name="COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name="ANEXO")
    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getCodlocal() {
        return codlocal;
    }

    @XmlElement(name="CODLOCAL")
    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name="CEN_EDU")
    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    @XmlElement(name="NIV_MOD")
    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    @XmlElement(name="MODATEN")
    public String getModaten() {
        return modaten;
    }

    public void setModaten(String modaten) {
        this.modaten = modaten;
    }

    @XmlElement(name="FORMATEN")
    public String getFormaten() {
        return formaten;
    }

    public void setFormaten(String formaten) {
        this.formaten = formaten;
    }

    @XmlElement(name="TIPOPROG")
    public String getTipoprog() {
        return tipoprog;
    }

    public void setTipoprog(String tipoprog) {
        this.tipoprog = tipoprog;
    }

    @XmlElement(name="GESTION")
    public String getGestion() {
        return gestion;
    }


    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name="DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name="LOCALIDAD")
    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
    @XmlElement(name="CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name="TIPONEE")
    public String getTiponee() {
        return tiponee;
    }


    public void setTiponee(String tiponee) {
        this.tiponee = tiponee;
    }

    @XmlElement(name="FORMA_PR")
    public String getFormaPr() {
        return formaPr;
    }

    public void setFormaPr(String formaPr) {
        this.formaPr = formaPr;
    }

    @XmlElement(name="FORMA_SP")
    public String getFormaSp() {
        return formaSp;
    }

    public void setFormaSp(String formaSp) {
        this.formaSp = formaSp;
    }

    @XmlElement(name="FORMA_AD")
    public String getFormaAd() {
        return formaAd;
    }

    public void setFormaAd(String formaAd) {
        this.formaAd = formaAd;
    }

    @XmlElement(name="DNI_CORD")
    public String getDniCord() {
        return dniCord;
    }

    public void setDniCord(String dniCord) {
        this.dniCord = dniCord;
    }

    @XmlElement(name="TIPOIESUP")
    public String getTipoiesup() {
        return tipoiesup;
    }

    public void setTipoiesup(String tipoiesup) {
        this.tipoiesup = tipoiesup;
    }

    @XmlElement(name="EGESTORA")
    public String getEgestora() {
        return egestora;
    }

    public void setEgestora(String egestora) {
        this.egestora = egestora;
    }

    @XmlElement(name="TIPODFINA_1")
    public String getTipodfina1() {
        return tipodfina1;
    }

    public void setTipodfina1(String tipodfina1) {
        this.tipodfina1 = tipodfina1;
    }

    public String getTipodfina2() {
        return tipodfina2;
    }

    @XmlElement(name="TIPODFINA_2")
    public void setTipodfina2(String tipodfina2) {
        this.tipodfina2 = tipodfina2;
    }
    @XmlElement(name="TIPODFINA_3")
    public String getTipodfina3() {
        return tipodfina3;
    }

    public void setTipodfina3(String tipodfina3) {
        this.tipodfina3 = tipodfina3;
    }
    @XmlElement(name="TIPODFINA_4")
    public String getTipodfina4() {
        return tipodfina4;
    }

    public void setTipodfina4(String tipodfina4) {
        this.tipodfina4 = tipodfina4;
    }
    @XmlElement(name="NRORD_CRE")
    public String getNrordCre() {
        return nrordCre;
    }

    public void setNrordCre(String nrordCre) {
        this.nrordCre = nrordCre;
    }
    @XmlElement(name="FECRES_C_D")
    public String getFecresCD() {
        return fecresCD;
    }

    public void setFecresCD(String fecresCD) {
        this.fecresCD = fecresCD;
    }
    @XmlElement(name="FECRES_C_M")
    public String getFecresCM() {
        return fecresCM;
    }

    public void setFecresCM(String fecresCM) {
        this.fecresCM = fecresCM;
    }
    @XmlElement(name="FECRES_C_A")
    public String getFecresCA() {
        return fecresCA;
    }

    public void setFecresCA(String fecresCA) {
        this.fecresCA = fecresCA;
    }
    @XmlElement(name="NRORD_REN")
    public String getNrordRen() {
        return nrordRen;
    }

    public void setNrordRen(String nrordRen) {
        this.nrordRen = nrordRen;
    }
    @XmlElement(name="FECRES_R_D")
    public String getFecresRD() {
        return fecresRD;
    }

    public void setFecresRD(String fecresRD) {
        this.fecresRD = fecresRD;
    }
    @XmlElement(name="FECRES_R_M")
    public String getFecresRM() {
        return fecresRM;
    }

    public void setFecresRM(String fecresRM) {
        this.fecresRM = fecresRM;
    }
    @XmlElement(name="FECRES_R_A")
    public String getFecresRA() {
        return fecresRA;
    }

    public void setFecresRA(String fecresRA) {
        this.fecresRA = fecresRA;
    }
    @XmlElement(name="P101_C5")
    public String getP101C5() {
        return p101C5;
    }

    public void setP101C5(String p101C5) {
        this.p101C5 = p101C5;
    }
    @XmlElement(name="P101_C5_1")
    public String getP101C51() {
        return p101C51;
    }

    public void setP101C51(String p101C51) {
        this.p101C51 = p101C51;
    }
    @XmlElement(name="P101_C5_2")
    public String getP101C52() {
        return p101C52;
    }

    public void setP101C52(String p101C52) {
        this.p101C52 = p101C52;
    }
    @XmlElement(name="P101_C5_3")
    public String getP101C53() {
        return p101C53;
    }

    public void setP101C53(String p101C53) {
        this.p101C53 = p101C53;
    }
    @XmlElement(name="P101_DD")
    public String getP101Dd() {
        return p101Dd;
    }

    public void setP101Dd(String p101Dd) {
        this.p101Dd = p101Dd;
    }
    @XmlElement(name="P101_MM")
    public String getP101Mm() {
        return p101Mm;
    }

    public void setP101Mm(String p101Mm) {
        this.p101Mm = p101Mm;
    }
    @XmlElement(name="P101_C2")
    public String getP101C2() {
        return p101C2;
    }

    public void setP101C2(String p101C2) {
        this.p101C2 = p101C2;
    }
    @XmlElement(name="P102_DD")
    public String getP102Dd() {
        return p102Dd;
    }

    public void setP102Dd(String p102Dd) {
        this.p102Dd = p102Dd;
    }
    @XmlElement(name="P102_MM")
    public String getP102Mm() {
        return p102Mm;
    }

    public void setP102Mm(String p102Mm) {
        this.p102Mm = p102Mm;
    }
    @XmlElement(name="P103_13")
    public String getP10313() {
        return p10313;
    }

    public void setP10313(String p10313) {
        this.p10313 = p10313;
    }
    @XmlElement(name="P102_C8")
    public String getP102C8() {
        return p102C8;
    }

    public void setP102C8(String p102C8) {
        this.p102C8 = p102C8;
    }
    @XmlElement(name="SERV_IE_1")
    public String getServIe1() {
        return servIe1;
    }

    public void setServIe1(String servIe1) {
        this.servIe1 = servIe1;
    }
    @XmlElement(name="SERV_IE_2")
    public String getServIe2() {
        return servIe2;
    }

    public void setServIe2(String servIe2) {
        this.servIe2 = servIe2;
    }
    @XmlElement(name="SERV_IE_3")
    public String getServIe3() {
        return servIe3;
    }

    public void setServIe3(String servIe3) {
        this.servIe3 = servIe3;
    }
    @XmlElement(name="SERV_IE_4")
    public String getServIe4() {
        return servIe4;
    }

    public void setServIe4(String servIe4) {
        this.servIe4 = servIe4;
    }
    @XmlElement(name="SERV_IE_5")
    public String getServIe5() {
        return servIe5;
    }

    public void setServIe5(String servIe5) {
        this.servIe5 = servIe5;
    }
    @XmlElement(name="SERV_IE_6")
    public String getServIe6() {
        return servIe6;
    }

    public void setServIe6(String servIe6) {
        this.servIe6 = servIe6;
    }
    @XmlElement(name="SERV_IE_7")
    public String getServIe7() {
        return servIe7;
    }

    public void setServIe7(String servIe7) {
        this.servIe7 = servIe7;
    }
    @XmlElement(name="P104_C2")
    public String getP104C2() {
        return p104C2;
    }

    public void setP104C2(String p104C2) {
        this.p104C2 = p104C2;
    }
    @XmlElement(name="P105_C3")
    public String getP105C3() {
        return p105C3;
    }

    public void setP105C3(String p105C3) {
        this.p105C3 = p105C3;
    }
    @XmlElement(name="P105_C3_1")
    public String getP105C31() {
        return p105C31;
    }

    public void setP105C31(String p105C31) {
        this.p105C31 = p105C31;
    }
    @XmlElement(name="P105_C3_1RD")
    public String getP105C31rd() {
        return p105C31rd;
    }

    public void setP105C31rd(String p105C31rd) {
        this.p105C31rd = p105C31rd;
    }
    @XmlElement(name="P105_C3_2")
    public String getP105C32() {
        return p105C32;
    }

    public void setP105C32(String p105C32) {
        this.p105C32 = p105C32;
    }
    @XmlElement(name="P105_C3_2RD")
    public String getP105C32rd() {
        return p105C32rd;
    }

    public void setP105C32rd(String p105C32rd) {
        this.p105C32rd = p105C32rd;
    }
    @XmlElement(name="P105_106")
    public String getP105106() {
        return p105106;
    }

    public void setP105106(String p105106) {
        this.p105106 = p105106;
    }
    @XmlElement(name="P105_106_ESP")
    public String getP105106Esp() {
        return p105106Esp;
    }

    public void setP105106Esp(String p105106Esp) {
        this.p105106Esp = p105106Esp;
    }
    @XmlElement(name="P101_C4_1PE")
    public String getP101C41pe() {
        return p101C41pe;
    }

    public void setP101C41pe(String p101C41pe) {
        this.p101C41pe = p101C41pe;
    }
    @XmlElement(name="P101_C4_1ID")
    public String getP101C41id() {
        return p101C41id;
    }

    public void setP101C41id(String p101C41id) {
        this.p101C41id = p101C41id;
    }
    @XmlElement(name="P101_C4_1IM")
    public String getP101C41im() {
        return p101C41im;
    }

    public void setP101C41im(String p101C41im) {
        this.p101C41im = p101C41im;
    }
    @XmlElement(name="P101_C4_1IA")
    public String getP101C41ia() {
        return p101C41ia;
    }

    public void setP101C41ia(String p101C41ia) {
        this.p101C41ia = p101C41ia;
    }
    @XmlElement(name="P101_C4_1FD")
    public String getP101C41fd() {
        return p101C41fd;
    }

    public void setP101C41fd(String p101C41fd) {
        this.p101C41fd = p101C41fd;
    }
    @XmlElement(name="P101_C4_1FM")
    public String getP101C41fm() {
        return p101C41fm;
    }

    public void setP101C41fm(String p101C41fm) {
        this.p101C41fm = p101C41fm;
    }
    @XmlElement(name="P101_C4_1FA")
    public String getP101C41fa() {
        return p101C41fa;
    }

    public void setP101C41fa(String p101C41fa) {
        this.p101C41fa = p101C41fa;
    }
    @XmlElement(name="P101_C4_2PE")
    public String getP101C42pe() {
        return p101C42pe;
    }

    public void setP101C42pe(String p101C42pe) {
        this.p101C42pe = p101C42pe;
    }
    @XmlElement(name="P101_C4_2ID")
    public String getP101C42id() {
        return p101C42id;
    }

    public void setP101C42id(String p101C42id) {
        this.p101C42id = p101C42id;
    }
    @XmlElement(name="P101_C4_2IM")
    public String getP101C42im() {
        return p101C42im;
    }

    public void setP101C42im(String p101C42im) {
        this.p101C42im = p101C42im;
    }
    @XmlElement(name="P101_C4_2IA")
    public String getP101C42ia() {
        return p101C42ia;
    }

    public void setP101C42ia(String p101C42ia) {
        this.p101C42ia = p101C42ia;
    }
    @XmlElement(name="P101_C4_2FD")
    public String getP101C42fd() {
        return p101C42fd;
    }

    public void setP101C42fd(String p101C42fd) {
        this.p101C42fd = p101C42fd;
    }
    @XmlElement(name="P101_C4_2FM")
    public String getP101C42fm() {
        return p101C42fm;
    }

    public void setP101C42fm(String p101C42fm) {
        this.p101C42fm = p101C42fm;
    }
    @XmlElement(name="P101_C4_2FA")
    public String getP101C42fa() {
        return p101C42fa;
    }

    public void setP101C42fa(String p101C42fa) {
        this.p101C42fa = p101C42fa;
    }
    @XmlElement(name="P101_C4_3PE")
    public String getP101C43pe() {
        return p101C43pe;
    }

    public void setP101C43pe(String p101C43pe) {
        this.p101C43pe = p101C43pe;
    }
    @XmlElement(name="P101_C4_3ID")
    public String getP101C43id() {
        return p101C43id;
    }

    public void setP101C43id(String p101C43id) {
        this.p101C43id = p101C43id;
    }
    @XmlElement(name="P101_C4_3IM")
    public String getP101C43im() {
        return p101C43im;
    }

    public void setP101C43im(String p101C43im) {
        this.p101C43im = p101C43im;
    }
    @XmlElement(name="P101_C4_3IA")
    public String getP101C43ia() {
        return p101C43ia;
    }

    public void setP101C43ia(String p101C43ia) {
        this.p101C43ia = p101C43ia;
    }
    @XmlElement(name="P101_C4_3FD")
    public String getP101C43fd() {
        return p101C43fd;
    }

    public void setP101C43fd(String p101C43fd) {
        this.p101C43fd = p101C43fd;
    }
    @XmlElement(name="P101_C4_3FM")
    public String getP101C43fm() {
        return p101C43fm;
    }

    public void setP101C43fm(String p101C43fm) {
        this.p101C43fm = p101C43fm;
    }
    @XmlElement(name="P101_C4_3FA")
    public String getP101C43fa() {
        return p101C43fa;
    }

    public void setP101C43fa(String p101C43fa) {
        this.p101C43fa = p101C43fa;
    }
    @XmlElement(name="P101_C4_4PE")
    public String getP101C44pe() {
        return p101C44pe;
    }

    public void setP101C44pe(String p101C44pe) {
        this.p101C44pe = p101C44pe;
    }
    @XmlElement(name="P101_C4_4ID")
    public String getP101C44id() {
        return p101C44id;
    }

    public void setP101C44id(String p101C44id) {
        this.p101C44id = p101C44id;
    }
    @XmlElement(name="P101_C4_4IM")
    public String getP101C44im() {
        return p101C44im;
    }

    public void setP101C44im(String p101C44im) {
        this.p101C44im = p101C44im;
    }
    @XmlElement(name="P101_C4_4IA")
    public String getP101C44ia() {
        return p101C44ia;
    }

    public void setP101C44ia(String p101C44ia) {
        this.p101C44ia = p101C44ia;
    }
    @XmlElement(name="P101_C4_4FD")
    public String getP101C44fd() {
        return p101C44fd;
    }

    public void setP101C44fd(String p101C44fd) {
        this.p101C44fd = p101C44fd;
    }
    @XmlElement(name="P101_C4_4FM")
    public String getP101C44fm() {
        return p101C44fm;
    }

    public void setP101C44fm(String p101C44fm) {
        this.p101C44fm = p101C44fm;
    }
    @XmlElement(name="P101_C4_4FA")
    public String getP101C44fa() {
        return p101C44fa;
    }

    public void setP101C44fa(String p101C44fa) {
        this.p101C44fa = p101C44fa;
    }
    @XmlElement(name="P102_C4")
    public String getP102C4() {
        return p102C4;
    }

    public void setP102C4(String p102C4) {
        this.p102C4 = p102C4;
    }
    @XmlElement(name="P103_C4")
    public Integer getP103C4() {
        return p103C4;
    }

    public void setP103C4(Integer p103C4) {
        this.p103C4 = p103C4;
    }
    @XmlElement(name="P104_C4_LU")
    public String getP104C4Lu() {
        return p104C4Lu;
    }

    public void setP104C4Lu(String p104C4Lu) {
        this.p104C4Lu = p104C4Lu;
    }
    @XmlElement(name="P104_C4_QL")
    public Integer getP104C4Ql() {
        return p104C4Ql;
    }

    public void setP104C4Ql(Integer p104C4Ql) {
        this.p104C4Ql = p104C4Ql;
    }
    @XmlElement(name="P104_C4_MA")
    public String getP104C4Ma() {
        return p104C4Ma;
    }

    public void setP104C4Ma(String p104C4Ma) {
        this.p104C4Ma = p104C4Ma;
    }
    @XmlElement(name="P104_C4_QM")
    public Integer getP104C4Qm() {
        return p104C4Qm;
    }

    public void setP104C4Qm(Integer p104C4Qm) {
        this.p104C4Qm = p104C4Qm;
    }
    @XmlElement(name="P104_C4_MI")
    public String getP104C4Mi() {
        return p104C4Mi;
    }

    public void setP104C4Mi(String p104C4Mi) {
        this.p104C4Mi = p104C4Mi;
    }
    @XmlElement(name="P104_C4_QN")
    public Integer getP104C4Qn() {
        return p104C4Qn;
    }

    public void setP104C4Qn(Integer p104C4Qn) {
        this.p104C4Qn = p104C4Qn;
    }
    @XmlElement(name="P104_C4_JU")
    public String getP104C4Ju() {
        return p104C4Ju;
    }

    public void setP104C4Ju(String p104C4Ju) {
        this.p104C4Ju = p104C4Ju;
    }
    @XmlElement(name="P104_C4_QJ")
    public Integer getP104C4Qj() {
        return p104C4Qj;
    }

    public void setP104C4Qj(Integer p104C4Qj) {
        this.p104C4Qj = p104C4Qj;
    }
    @XmlElement(name="P104_C4_VI")
    public String getP104C4Vi() {
        return p104C4Vi;
    }

    public void setP104C4Vi(String p104C4Vi) {
        this.p104C4Vi = p104C4Vi;
    }
    @XmlElement(name="P104_C4_QV")
    public Integer getP104C4Qv() {
        return p104C4Qv;
    }

    public void setP104C4Qv(Integer p104C4Qv) {
        this.p104C4Qv = p104C4Qv;
    }
    @XmlElement(name="P104_C4_SA")
    public String getP104C4Sa() {
        return p104C4Sa;
    }

    public void setP104C4Sa(String p104C4Sa) {
        this.p104C4Sa = p104C4Sa;
    }
    @XmlElement(name="P104_C4_QS")
    public Integer getP104C4Qs() {
        return p104C4Qs;
    }

    public void setP104C4Qs(Integer p104C4Qs) {
        this.p104C4Qs = p104C4Qs;
    }
    @XmlElement(name="P104_C4_DO")
    public String getP104C4Do() {
        return p104C4Do;
    }

    public void setP104C4Do(String p104C4Do) {
        this.p104C4Do = p104C4Do;
    }
    @XmlElement(name="P104_C4_QD")
    public Integer getP104C4Qd() {
        return p104C4Qd;
    }

    public void setP104C4Qd(Integer p104C4Qd) {
        this.p104C4Qd = p104C4Qd;
    }
    @XmlElement(name="P105_C4A_1")
    public String getP105C4a1() {
        return p105C4a1;
    }

    public void setP105C4a1(String p105C4a1) {
        this.p105C4a1 = p105C4a1;
    }
    @XmlElement(name="P105_C4A_2")
    public String getP105C4a2() {
        return p105C4a2;
    }

    public void setP105C4a2(String p105C4a2) {
        this.p105C4a2 = p105C4a2;
    }
    @XmlElement(name="P105_C4A_3")
    public String getP105C4a3() {
        return p105C4a3;
    }

    public void setP105C4a3(String p105C4a3) {
        this.p105C4a3 = p105C4a3;
    }
    @XmlElement(name="P105_C4A_4")
    public String getP105C4a4() {
        return p105C4a4;
    }

    public void setP105C4a4(String p105C4a4) {
        this.p105C4a4 = p105C4a4;
    }
    @XmlElement(name="P105_6_C41")
    public String getP1056C41() {
        return p1056C41;
    }

    public void setP1056C41(String p1056C41) {
        this.p1056C41 = p1056C41;
    }
    @XmlElement(name="P105_6_C42")
    public String getP1056C42() {
        return p1056C42;
    }

    public void setP1056C42(String p1056C42) {
        this.p1056C42 = p1056C42;
    }
    @XmlElement(name="P105_6_C43")
    public String getP1056C43() {
        return p1056C43;
    }

    public void setP1056C43(String p1056C43) {
        this.p1056C43 = p1056C43;
    }
    @XmlElement(name="P105_6_C44")
    public String getP1056C44() {
        return p1056C44;
    }

    public void setP1056C44(String p1056C44) {
        this.p1056C44 = p1056C44;
    }
    @XmlElement(name="P105_6_C45")
    public String getP1056C45() {
        return p1056C45;
    }

    public void setP1056C45(String p1056C45) {
        this.p1056C45 = p1056C45;
    }
    @XmlElement(name="P105_6_C46")
    public String getP1056C46() {
        return p1056C46;
    }

    public void setP1056C46(String p1056C46) {
        this.p1056C46 = p1056C46;
    }
    @XmlElement(name="P105_6_C47")
    public String getP1056C47() {
        return p1056C47;
    }

    public void setP1056C47(String p1056C47) {
        this.p1056C47 = p1056C47;
    }
    @XmlElement(name="P105_6_C48")
    public String getP1056C48() {
        return p1056C48;
    }

    public void setP1056C48(String p1056C48) {
        this.p1056C48 = p1056C48;
    }
    @XmlElement(name="P105_6_C4E")
    public String getP1056C4e() {
        return p1056C4e;
    }

    public void setP1056C4e(String p1056C4e) {
        this.p1056C4e = p1056C4e;
    }
    @XmlElement(name="P106_C4I_C")
    public String getP106C4iC() {
        return p106C4iC;
    }

    public void setP106C4iC(String p106C4iC) {
        this.p106C4iC = p106C4iC;
    }
    @XmlElement(name="P107_C4I_Q")
    public Integer getP107C4iQ() {
        return p107C4iQ;
    }

    public void setP107C4iQ(Integer p107C4iQ) {
        this.p107C4iQ = p107C4iQ;
    }
    @XmlElement(name="P108_C4_LU")
    public String getP108C4Lu() {
        return p108C4Lu;
    }

    public void setP108C4Lu(String p108C4Lu) {
        this.p108C4Lu = p108C4Lu;
    }
    @XmlElement(name="P108_C4_QL")
    public Integer getP108C4Ql() {
        return p108C4Ql;
    }

    public void setP108C4Ql(Integer p108C4Ql) {
        this.p108C4Ql = p108C4Ql;
    }
    @XmlElement(name="P108_C4_MA")
    public String getP108C4Ma() {
        return p108C4Ma;
    }

    public void setP108C4Ma(String p108C4Ma) {
        this.p108C4Ma = p108C4Ma;
    }
    @XmlElement(name="P108_C4_QM")
    public Integer getP108C4Qm() {
        return p108C4Qm;
    }

    public void setP108C4Qm(Integer p108C4Qm) {
        this.p108C4Qm = p108C4Qm;
    }
    @XmlElement(name="P108_C4_MI")
    public String getP108C4Mi() {
        return p108C4Mi;
    }

    public void setP108C4Mi(String p108C4Mi) {
        this.p108C4Mi = p108C4Mi;
    }
    @XmlElement(name="P108_C4_QN")
    public Integer getP108C4Qn() {
        return p108C4Qn;
    }

    public void setP108C4Qn(Integer p108C4Qn) {
        this.p108C4Qn = p108C4Qn;
    }
    @XmlElement(name="P108_C4_JU")
    public String getP108C4Ju() {
        return p108C4Ju;
    }

    public void setP108C4Ju(String p108C4Ju) {
        this.p108C4Ju = p108C4Ju;
    }
    @XmlElement(name="P108_C4_QJ")
    public Integer getP108C4Qj() {
        return p108C4Qj;
    }

    public void setP108C4Qj(Integer p108C4Qj) {
        this.p108C4Qj = p108C4Qj;
    }
    @XmlElement(name="P108_C4_VI")
    public String getP108C4Vi() {
        return p108C4Vi;
    }

    public void setP108C4Vi(String p108C4Vi) {
        this.p108C4Vi = p108C4Vi;
    }
    @XmlElement(name="P108_C4_QV")
    public Integer getP108C4Qv() {
        return p108C4Qv;
    }

    public void setP108C4Qv(Integer p108C4Qv) {
        this.p108C4Qv = p108C4Qv;
    }
    @XmlElement(name="P108_C4_SA")
    public String getP108C4Sa() {
        return p108C4Sa;
    }

    public void setP108C4Sa(String p108C4Sa) {
        this.p108C4Sa = p108C4Sa;
    }
    @XmlElement(name="P108_C4_QS")
    public Integer getP108C4Qs() {
        return p108C4Qs;
    }

    public void setP108C4Qs(Integer p108C4Qs) {
        this.p108C4Qs = p108C4Qs;
    }
    @XmlElement(name="P108_C4_DO")
    public String getP108C4Do() {
        return p108C4Do;
    }

    public void setP108C4Do(String p108C4Do) {
        this.p108C4Do = p108C4Do;
    }
    @XmlElement(name="P108_C4_QD")
    public Integer getP108C4Qd() {
        return p108C4Qd;
    }

    public void setP108C4Qd(Integer p108C4Qd) {
        this.p108C4Qd = p108C4Qd;
    }
    @XmlElement(name="PEIB_1")
    public String getPeib1() {
        return peib1;
    }

    public void setPeib1(String peib1) {
        this.peib1 = peib1;
    }
    @XmlElement(name="PEIB_1_RES")
    public String getPeib1Res() {
        return peib1Res;
    }

    public void setPeib1Res(String peib1Res) {
        this.peib1Res = peib1Res;
    }
    @XmlElement(name="PEIB_2")
    public String getPeib2() {
        return peib2;
    }

    public void setPeib2(String peib2) {
        this.peib2 = peib2;
    }
    @XmlElement(name="PEIB_3")
    public String getPeib3() {
        return peib3;
    }

    public void setPeib3(String peib3) {
        this.peib3 = peib3;
    }
    @XmlElement(name="PEIB_3_ESP")
    public String getPeib3Esp() {
        return peib3Esp;
    }

    public void setPeib3Esp(String peib3Esp) {
        this.peib3Esp = peib3Esp;
    }
    @XmlElement(name="PEIB_4")
    public String getPeib4() {
        return peib4;
    }

    public void setPeib4(String peib4) {
        this.peib4 = peib4;
    }
    @XmlElement(name="P1055_C2_1")
    public String getP1055C21() {
        return p1055C21;
    }

    public void setP1055C21(String p1055C21) {
        this.p1055C21 = p1055C21;
    }
    @XmlElement(name="P1055_C2_2")
    public String getP1055C22() {
        return p1055C22;
    }

    public void setP1055C22(String p1055C22) {
        this.p1055C22 = p1055C22;
    }
    @XmlElement(name="P1074_C3_1")
    public String getP1074C31() {
        return p1074C31;
    }

    public void setP1074C31(String p1074C31) {
        this.p1074C31 = p1074C31;
    }
    @XmlElement(name="P1074_C3_2")
    public String getP1074C32() {
        return p1074C32;
    }

    public void setP1074C32(String p1074C32) {
        this.p1074C32 = p1074C32;
    }
    @XmlElement(name="P1075_C3")
    public String getP1075C3() {
        return p1075C3;
    }

    public void setP1075C3(String p1075C3) {
        this.p1075C3 = p1075C3;
    }
    @XmlElement(name="P1075_C3_ESP")
    public String getP1075C3Esp() {
        return p1075C3Esp;
    }

    public void setP1075C3Esp(String p1075C3Esp) {
        this.p1075C3Esp = p1075C3Esp;
    }
    @XmlElement(name="P1075_C3_1")
    public String getP1075C31() {
        return p1075C31;
    }

    public void setP1075C31(String p1075C31) {
        this.p1075C31 = p1075C31;
    }
    @XmlElement(name="P1075_C3_2")
    public String getP1075C32() {
        return p1075C32;
    }

    public void setP1075C32(String p1075C32) {
        this.p1075C32 = p1075C32;
    }
    @XmlElement(name="P1075_C3_3")
    public String getP1075C33() {
        return p1075C33;
    }

    public void setP1075C33(String p1075C33) {
        this.p1075C33 = p1075C33;
    }
    @XmlElement(name="P1075_C3_4")
    public String getP1075C34() {
        return p1075C34;
    }

    public void setP1075C34(String p1075C34) {
        this.p1075C34 = p1075C34;
    }
    @XmlElement(name="P1075_C3_5")
    public String getP1075C35() {
        return p1075C35;
    }

    public void setP1075C35(String p1075C35) {
        this.p1075C35 = p1075C35;
    }
    @XmlElement(name="P1075_C3_6")
    public String getP1075C36() {
        return p1075C36;
    }

    public void setP1075C36(String p1075C36) {
        this.p1075C36 = p1075C36;
    }
    @XmlElement(name="P1076_C3")
    public String getP1076C3() {
        return p1076C3;
    }

    public void setP1076C3(String p1076C3) {
        this.p1076C3 = p1076C3;
    }
    @XmlElement(name="P1081_C3_1")
    public String getP1081C31() {
        return p1081C31;
    }

    public void setP1081C31(String p1081C31) {
        this.p1081C31 = p1081C31;
    }
    @XmlElement(name="P1081_C3_2")
    public Integer getP1081C32() {
        return p1081C32;
    }

    public void setP1081C32(Integer p1081C32) {
        this.p1081C32 = p1081C32;
    }
    @XmlElement(name="P1082_C3_11")
    public String getP1082C311() {
        return p1082C311;
    }

    public void setP1082C311(String p1082C311) {
        this.p1082C311 = p1082C311;
    }
    @XmlElement(name="P1082_C3_12")
    public String getP1082C312() {
        return p1082C312;
    }

    public void setP1082C312(String p1082C312) {
        this.p1082C312 = p1082C312;
    }
    @XmlElement(name="P1082_C3_13")
    public String getP1082C313() {
        return p1082C313;
    }

    public void setP1082C313(String p1082C313) {
        this.p1082C313 = p1082C313;
    }
    @XmlElement(name="P1082_C3_14")
    public String getP1082C314() {
        return p1082C314;
    }

    public void setP1082C314(String p1082C314) {
        this.p1082C314 = p1082C314;
    }
    @XmlElement(name="P1082_C3_21")
    public String getP1082C321() {
        return p1082C321;
    }

    public void setP1082C321(String p1082C321) {
        this.p1082C321 = p1082C321;
    }
    @XmlElement(name="P1082_C3_22")
    public String getP1082C322() {
        return p1082C322;
    }

    public void setP1082C322(String p1082C322) {
        this.p1082C322 = p1082C322;
    }
    @XmlElement(name="P1082_C3_23")
    public String getP1082C323() {
        return p1082C323;
    }

    public void setP1082C323(String p1082C323) {
        this.p1082C323 = p1082C323;
    }
    @XmlElement(name="P1082_C3_24")
    public String getP1082C324() {
        return p1082C324;
    }

    public void setP1082C324(String p1082C324) {
        this.p1082C324 = p1082C324;
    }
    @XmlElement(name="P1082_C3_31")
    public String getP1082C331() {
        return p1082C331;
    }

    public void setP1082C331(String p1082C331) {
        this.p1082C331 = p1082C331;
    }
    @XmlElement(name="P1082_C3_32")
    public String getP1082C332() {
        return p1082C332;
    }

    public void setP1082C332(String p1082C332) {
        this.p1082C332 = p1082C332;
    }
    @XmlElement(name="P1082_C3_33")
    public String getP1082C333() {
        return p1082C333;
    }

    public void setP1082C333(String p1082C333) {
        this.p1082C333 = p1082C333;
    }
    @XmlElement(name="P1082_C3_34")
    public String getP1082C334() {
        return p1082C334;
    }

    public void setP1082C334(String p1082C334) {
        this.p1082C334 = p1082C334;
    }
    @XmlElement(name="P1082_C3_41")
    public String getP1082C341() {
        return p1082C341;
    }

    public void setP1082C341(String p1082C341) {
        this.p1082C341 = p1082C341;
    }
    @XmlElement(name="P1082_C3_42")
    public String getP1082C342() {
        return p1082C342;
    }

    public void setP1082C342(String p1082C342) {
        this.p1082C342 = p1082C342;
    }
    @XmlElement(name="P1082_C3_43")
    public String getP1082C343() {
        return p1082C343;
    }

    public void setP1082C343(String p1082C343) {
        this.p1082C343 = p1082C343;
    }
    @XmlElement(name="P1082_C3_44")
    public String getP1082C344() {
        return p1082C344;
    }

    public void setP1082C344(String p1082C344) {
        this.p1082C344 = p1082C344;
    }
    @XmlElement(name="P1082_C3_51")
    public String getP1082C351() {
        return p1082C351;
    }

    public void setP1082C351(String p1082C351) {
        this.p1082C351 = p1082C351;
    }
    @XmlElement(name="P1082_C3_52")
    public String getP1082C352() {
        return p1082C352;
    }

    public void setP1082C352(String p1082C352) {
        this.p1082C352 = p1082C352;
    }
    @XmlElement(name="P1082_C3_53")
    public String getP1082C353() {
        return p1082C353;
    }

    public void setP1082C353(String p1082C353) {
        this.p1082C353 = p1082C353;
    }
    @XmlElement(name="P1082_C3_54")
    public String getP1082C354() {
        return p1082C354;
    }

    public void setP1082C354(String p1082C354) {
        this.p1082C354 = p1082C354;
    }
    @XmlElement(name="P401_C12")
    public String getP401C12() {
        return p401C12;
    }

    public void setP401C12(String p401C12) {
        this.p401C12 = p401C12;
    }
    @XmlElement(name="P402_C12_A")
    public String getP402C12A() {
        return p402C12A;
    }

    public void setP402C12A(String p402C12A) {
        this.p402C12A = p402C12A;
    }

//    public void setP402C12D(String p402C12A) {
//        this.p402C12A = p402C12A;
//    }
    @XmlElement(name="P402_C12_M")
    public String getP402C12M() {
        return p402C12M;
    }

    public void setP402C12M(String p402C12M) {
        this.p402C12M = p402C12M;
    }
    @XmlElement(name="P403_C12_1")
    public Integer getP403C121() {
        return p403C121;
    }

    public void setP403C121(Integer p403C121) {
        this.p403C121 = p403C121;
    }
    @XmlElement(name="P403_C12_2")
    public Integer getP403C122() {
        return p403C122;
    }

    public void setP403C122(Integer p403C122) {
        this.p403C122 = p403C122;
    }
    @XmlElement(name="P403_C12_3")
    public Integer getP403C123() {
        return p403C123;
    }

    public void setP403C123(Integer p403C123) {
        this.p403C123 = p403C123;
    }
    @XmlElement(name="P404_C12")
    public String getP404C12() {
        return p404C12;
    }

    public void setP404C12(String p404C12) {
        this.p404C12 = p404C12;
    }
    @XmlElement(name="P405_C12_A")
    public String getP405C12A() {
        return p405C12A;
    }

    public void setP405C12A(String p405C12A) {
        this.p405C12A = p405C12A;
    }
    @XmlElement(name="P405_C12_M")
    public String getP405C12M() {
        return p405C12M;
    }

    public void setP405C12M(String p405C12M) {
        this.p405C12M = p405C12M;
    }
    @XmlElement(name="P406_C12_1")
    public Integer getP406C121() {
        return p406C121;
    }

    public void setP406C121(Integer p406C121) {
        this.p406C121 = p406C121;
    }
    @XmlElement(name="P406_C12_2")
    public Integer getP406C122() {
        return p406C122;
    }

    public void setP406C122(Integer p406C122) {
        this.p406C122 = p406C122;
    }
    @XmlElement(name="P406_C12_3")
    public Integer getP406C123() {
        return p406C123;
    }

    public void setP406C123(Integer p406C123) {
        this.p406C123 = p406C123;
    }
    @XmlElement(name="P407_C1")
    public String getP407C1() {
        return p407C1;
    }

    public void setP407C1(String p407C1) {
        this.p407C1 = p407C1;
    }
    @XmlElement(name="P401_C3")
    public String getP401C3() {
        return p401C3;
    }

    public void setP401C3(String p401C3) {
        this.p401C3 = p401C3;
    }
    @XmlElement(name="P402_C3_A")
    public String getP402C3A() {
        return p402C3A;
    }

    public void setP402C3A(String p402C3A) {
        this.p402C3A = p402C3A;
    }
    @XmlElement(name="P402_C3_M")
    public String getP402C3M() {
        return p402C3M;
    }

    public void setP402C3M(String p402C3M) {
        this.p402C3M = p402C3M;
    }
    @XmlElement(name="P405_C3")
    public String getP405C3() {
        return p405C3;
    }

    public void setP405C3(String p405C3) {
        this.p405C3 = p405C3;
    }
    @XmlElement(name="P406_C3_A")
    public String getP406C3A() {
        return p406C3A;
    }

    public void setP406C3A(String p406C3A) {
        this.p406C3A = p406C3A;
    }
    @XmlElement(name="P406_C3_M")
    public String getP406C3M() {
        return p406C3M;
    }

    public void setP406C3M(String p406C3M) {
        this.p406C3M = p406C3M;
    }
    @XmlElement(name="P4081_C3")
    public String getP4081C3() {
        return p4081C3;
    }

    public void setP4081C3(String p4081C3) {
        this.p4081C3 = p4081C3;
    }
    @XmlElement(name="P4082_C3_A")
    public String getP4082C3A() {
        return p4082C3A;
    }

    public void setP4082C3A(String p4082C3A) {
        this.p4082C3A = p4082C3A;
    }
    @XmlElement(name="P4082_C3_M")
    public String getP4082C3M() {
        return p4082C3M;
    }

    public void setP4082C3M(String p4082C3M) {
        this.p4082C3M = p4082C3M;
    }
    @XmlElement(name="P401_C4")
    public String getP401C4() {
        return p401C4;
    }

    public void setP401C4(String p401C4) {
        this.p401C4 = p401C4;
    }
    @XmlElement(name="P405_C4")
    public String getP405C4() {
        return p405C4;
    }

    public void setP405C4(String p405C4) {
        this.p405C4 = p405C4;
    }
    @XmlElement(name="P406_C4_1")
    public String getP406C41() {
        return p406C41;
    }

    public void setP406C41(String p406C41) {
        this.p406C41 = p406C41;
    }
    @XmlElement(name="P406_C4_2")
    public String getP406C42() {
        return p406C42;
    }

    public void setP406C42(String p406C42) {
        this.p406C42 = p406C42;
    }
    @XmlElement(name="P406_C4_3")
    public String getP406C43() {
        return p406C43;
    }

    public void setP406C43(String p406C43) {
        this.p406C43 = p406C43;
    }
    @XmlElement(name="P406_C4_4")
    public String getP406C44() {
        return p406C44;
    }

    public void setP406C44(String p406C44) {
        this.p406C44 = p406C44;
    }
    @XmlElement(name="P406_C4_5")
    public String getP406C45() {
        return p406C45;
    }

    public void setP406C45(String p406C45) {
        this.p406C45 = p406C45;
    }
    @XmlElement(name="P406_C4_6")
    public String getP406C46() {
        return p406C46;
    }

    public void setP406C46(String p406C46) {
        this.p406C46 = p406C46;
    }
    @XmlElement(name="P406_C4_7")
    public String getP406C47() {
        return p406C47;
    }

    public void setP406C47(String p406C47) {
        this.p406C47 = p406C47;
    }
    @XmlElement(name="P406_C4_8")
    public String getP406C48() {
        return p406C48;
    }

    public void setP406C48(String p406C48) {
        this.p406C48 = p406C48;
    }
    @XmlElement(name="P406_C4_9")
    public String getP406C49() {
        return p406C49;
    }

    public void setP406C49(String p406C49) {
        this.p406C49 = p406C49;
    }
    @XmlElement(name="P406_C4_9E")
    public String getP406C49e() {
        return p406C49e;
    }

    public void setP406C49e(String p406C49e) {
        this.p406C49e = p406C49e;
    }
    @XmlElement(name="P406_C4_10")
    public String getP406C410() {
        return p406C410;
    }

    public void setP406C410(String p406C410) {
        this.p406C410 = p406C410;
    }
    @XmlElement(name="P406_C4_10E")
    public String getP406C410e() {
        return p406C410e;
    }

    public void setP406C410e(String p406C410e) {
        this.p406C410e = p406C410e;
    }
    @XmlElement(name="P406_C4_11")
    public String getP406C411() {
        return p406C411;
    }

    public void setP406C411(String p406C411) {
        this.p406C411 = p406C411;
    }
    @XmlElement(name="P407_C4_11")
    public String getP407C411() {
        return p407C411;
    }

    public void setP407C411(String p407C411) {
        this.p407C411 = p407C411;
    }
    @XmlElement(name="P407_C4_12")
    public Integer getP407C412() {
        return p407C412;
    }

    public void setP407C412(Integer p407C412) {
        this.p407C412 = p407C412;
    }
    @XmlElement(name="P407_C4_13")
    public Integer getP407C413() {
        return p407C413;
    }

    public void setP407C413(Integer p407C413) {
        this.p407C413 = p407C413;
    }
    @XmlElement(name="P407_C4_21")
    public String getP407C421() {
        return p407C421;
    }

    public void setP407C421(String p407C421) {
        this.p407C421 = p407C421;
    }
    @XmlElement(name="P407_C4_22")
    public Integer getP407C422() {
        return p407C422;
    }

    public void setP407C422(Integer p407C422) {
        this.p407C422 = p407C422;
    }
    @XmlElement(name="P407_C4_23")
    public Integer getP407C423() {
        return p407C423;
    }

    public void setP407C423(Integer p407C423) {
        this.p407C423 = p407C423;
    }
    @XmlElement(name="P407_C4_31")
    public String getP407C431() {
        return p407C431;
    }

    public void setP407C431(String p407C431) {
        this.p407C431 = p407C431;
    }
    @XmlElement(name="P407_C4_32")
    public Integer getP407C432() {
        return p407C432;
    }

    public void setP407C432(Integer p407C432) {
        this.p407C432 = p407C432;
    }
    @XmlElement(name="P407_C4_33")
    public Integer getP407C433() {
        return p407C433;
    }

    public void setP407C433(Integer p407C433) {
        this.p407C433 = p407C433;
    }
    @XmlElement(name="P407_C4_41")
    public String getP407C441() {
        return p407C441;
    }

    public void setP407C441(String p407C441) {
        this.p407C441 = p407C441;
    }
    @XmlElement(name="P408_C4_1")
    public Integer getP408C41() {
        return p408C41;
    }

    public void setP408C41(Integer p408C41) {
        this.p408C41 = p408C41;
    }
    @XmlElement(name="P408_C4_2")
    public Integer getP408C42() {
        return p408C42;
    }

    public void setP408C42(Integer p408C42) {
        this.p408C42 = p408C42;
    }
    @XmlElement(name="P408_C4_3")
    public Integer getP408C43() {
        return p408C43;
    }

    public void setP408C43(Integer p408C43) {
        this.p408C43 = p408C43;
    }
    @XmlElement(name="P502_C1")
    public String getP502C1() {
        return p502C1;
    }

    public void setP502C1(String p502C1) {
        this.p502C1 = p502C1;
    }
    @XmlElement(name="P502_C1_D")
    public String getP502C1D() {
        return p502C1D;
    }

    public void setP502C1D(String p502C1D) {
        this.p502C1D = p502C1D;
    }
    @XmlElement(name="P502_C1_M")
    public String getP502C1M() {
        return p502C1M;
    }

    public void setP502C1M(String p502C1M) {
        this.p502C1M = p502C1M;
    }
    @XmlElement(name="P502_C1_A")
    public String getP502C1A() {
        return p502C1A;
    }

    public void setP502C1A(String p502C1A) {
        this.p502C1A = p502C1A;
    }
    @XmlElement(name="TDOCENTES")
    public Integer getTdocentes() {
        return tdocentes;
    }

    public void setTdocentes(Integer tdocentes) {
        this.tdocentes = tdocentes;
    }
    @XmlElement(name="TDOCAULA")
    public Integer getTdocaula() {
        return tdocaula;
    }

    public void setTdocaula(Integer tdocaula) {
        this.tdocaula = tdocaula;
    }
    @XmlElement(name="TAUXILIAR")
    public Integer getTauxiliar() {
        return tauxiliar;
    }

    public void setTauxiliar(Integer tauxiliar) {
        this.tauxiliar = tauxiliar;
    }
    @XmlElement(name="TADMINIST")
    public Integer getTadminist() {
        return tadminist;
    }

    public void setTadminist(Integer tadminist) {
        this.tadminist = tadminist;
    }
    @XmlElement(name="ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }
    @XmlElement(name="FUENTE")
    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }
    @XmlElement(name="FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
    @XmlElement(name="VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    @XmlElement(name="TIPO_ENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }
    @XmlElement(name="ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }
    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }
    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

/*
    @XmlElement(name="PRONOEI")
    public List<Matricula2016Localpronoei> getMatricula2016LocalpronoeiList() {
        return matricula2016LocalpronoeiList;
    }

    public void setMatricula2016LocalpronoeiList(List<Matricula2016Localpronoei> matricula2016LocalpronoeiList) {
        this.matricula2016LocalpronoeiList = matricula2016LocalpronoeiList;
    }

    @XmlElement(name="PERSONAL")
    public List<Matricula2016Personal> getMatricula2016PersonalList() {
        return matricula2016PersonalList;
    }

    public void setMatricula2016PersonalList(List<Matricula2016Personal> matricula2016PersonalList) {
        this.matricula2016PersonalList = matricula2016PersonalList;
    }



    public void setDetalleRecursos(Map<String, Matricula2016Recursos> detalleRecursos) {
        this.detalleRecursos = detalleRecursos;
    }

    @XmlElement(name="SAANEES")
    public List<Matricula2016Saanee> getMatricula2016SaaneeList() {
        return matricula2016SaaneeList;
    }

    public void setMatricula2016SaaneeList(List<Matricula2016Saanee> matricula2016SaaneeList) {
        this.matricula2016SaaneeList = matricula2016SaaneeList;
    }

    @XmlElement(name="CARRERAS")
    public List<Matricula2016Carreras> getMatricula2016CarrerasList() {
        return matricula2016CarrerasList;
    }

    public void setMatricula2016CarrerasList(List<Matricula2016Carreras> matricula2016CarrerasList) {
        this.matricula2016CarrerasList = matricula2016CarrerasList;
    }

    @XmlElement(name="MATRICULA")
    @XmlJavaTypeAdapter(Matricula2016MatriculaMapAdapter.class)
    public Map<String, Matricula2016Matricula> getDetalleMatricula() {
        return detalleMatricula;
    }

    public void setDetalleMatricula(Map<String, Matricula2016Matricula> detalleMatricula) {
        this.detalleMatricula = detalleMatricula;
    }

    @XmlElement(name="SECCION")
    @XmlJavaTypeAdapter(Matricula2016SeccionMapAdapter.class)
    public Map<String, Matricula2016Seccion> getDetalleSeccion() {
        return detalleSeccion;
    }

    public void setDetalleSeccion(Map<String, Matricula2016Seccion> detalleSeccion) {
        this.detalleSeccion = detalleSeccion;
    }

    @XmlElement(name="RECURSOS")
    @XmlJavaTypeAdapter(Matricula2016RecursosMapAdapter.class)
    public Map<String, Matricula2016Recursos> getDetalleRecursos() {
        return detalleRecursos;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2016Cabecera)) {
            return false;
        }
        Matricula2016Cabecera other = (Matricula2016Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Matricula2016Cabecera[idEnvio=" + idEnvio + "]";
    }

}
