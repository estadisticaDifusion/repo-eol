/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_recursos_fila")
public class Matricula2017RecursosFila implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "VCTRL_1")
    private String vctrl1;
    @Column(name = "VCTRL_2")
    private String vctrl2;
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL")
    private Integer total;
    @Column(name = "DATO01")
    private Integer dato01;
    @Column(name = "DATO02")
    private Integer dato02;
    @Column(name = "DATO03")
    private Integer dato03;
    @Column(name = "DATO04")
    private Integer dato04;
    @Column(name = "DATO05")
    private Integer dato05;
    @Column(name = "DATO06")
    private Integer dato06;
    @Column(name = "CHK1")
    private String chk1;
    @Column(name = "CHK2")
    private String chk2;
    @Column(name = "CHK3")
    private String chk3;
    @Column(name = "CHK4")
    private String chk4;
    @Column(name = "CHK5")
    private String chk5;
    @Column(name = "DIA")
    private String dia;
    @Column(name = "MES")
    private String mes;
    @Column(name = "ANIO")
    private String anio;
    @JoinColumn(name = "DETALLE_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2017Recursos matricula2017Recursos;

    public Matricula2017RecursosFila() {
    }

    public Matricula2017RecursosFila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "VCTRL_1")
    public String getVctrl1() {
        return vctrl1;
    }

    public void setVctrl1(String vctrl1) {
        this.vctrl1 = vctrl1;
    }

    @XmlElement(name = "VCTRL_2")
    public String getVctrl2() {
        return vctrl2;
    }

    public void setVctrl2(String vctrl2) {
        this.vctrl2 = vctrl2;
    }

    @XmlElement(name = "TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name = "DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name = "TOTAL")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @XmlElement(name = "DATO01")
    public Integer getDato01() {
        return dato01;
    }

    public void setDato01(Integer dato01) {
        this.dato01 = dato01;
    }

    @XmlElement(name = "DATO02")
    public Integer getDato02() {
        return dato02;
    }

    public void setDato02(Integer dato02) {
        this.dato02 = dato02;
    }

    @XmlElement(name = "DATO03")
    public Integer getDato03() {
        return dato03;
    }

    public void setDato03(Integer dato03) {
        this.dato03 = dato03;
    }

    @XmlElement(name = "DATO04")
    public Integer getDato04() {
        return dato04;
    }

    public void setDato04(Integer dato04) {
        this.dato04 = dato04;
    }

    @XmlElement(name = "DATO05")
    public Integer getDato05() {
        return dato05;
    }

    public void setDato05(Integer dato05) {
        this.dato05 = dato05;
    }

    @XmlElement(name = "DATO06")
    public Integer getDato06() {
        return dato06;
    }

    public void setDato06(Integer dato06) {
        this.dato06 = dato06;
    }

    @XmlElement(name = "CHK1")
    public String getChk1() {
        return chk1;
    }

    public void setChk1(String chk1) {
        this.chk1 = chk1;
    }

    @XmlElement(name = "CHK2")
    public String getChk2() {
        return chk2;
    }

    public void setChk2(String chk2) {
        this.chk2 = chk2;
    }

    @XmlElement(name = "CHK3")
    public String getChk3() {
        return chk3;
    }

    public void setChk3(String chk3) {
        this.chk3 = chk3;
    }

    @XmlElement(name = "CHK4")
    public String getChk4() {
        return chk4;
    }

    public void setChk4(String chk4) {
        this.chk4 = chk4;
    }

    @XmlElement(name = "CHK5")
    public String getChk5() {
        return chk5;
    }

    public void setChk5(String chk5) {
        this.chk5 = chk5;
    }

    @XmlElement(name = "DIA")
    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    @XmlElement(name = "MES")
    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    @XmlElement(name = "ANIO")
    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    @XmlTransient
    public Matricula2017Recursos getMatricula2017Recursos() {
        return matricula2017Recursos;
    }

    public void setMatricula2017Recursos(Matricula2017Recursos matricula2017Recursos) {
        this.matricula2017Recursos = matricula2017Recursos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2017RecursosFila)) {
            return false;
        }
        Matricula2017RecursosFila other = (Matricula2017RecursosFila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Matricula2017RecursosFila[idEnvio=" + idEnvio + "]";
    }
}
