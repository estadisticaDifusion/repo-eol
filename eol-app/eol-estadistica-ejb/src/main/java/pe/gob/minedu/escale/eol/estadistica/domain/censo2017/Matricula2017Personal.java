/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_personal")
public class Matricula2017Personal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "NRO")
    private Integer nro;
    @Column(name = "PERDNI")
    private String perdni;
    @Column(name = "PER01")
    private String per01;
    @Column(name = "PER02")
    private Integer per02;
    @Column(name = "PER03")
    private String per03;
    @Column(name = "PER04")
    private String per04;
    @Column(name = "PER05")
    private String per05;
    @Column(name = "PER06")
    private String per06;
    @Column(name = "PER07")
    private String per07;
    @Column(name = "PER08")
    private String per08;
    @Column(name = "PER09")
    private String per09;
    @Column(name = "PER10")
    private String per10;
    @Column(name = "PER11")
    private String per11;
    @Column(name = "PER12")
    private String per12;
    @Column(name = "PER13")
    private Integer per13;
    @Column(name = "PER14")
    private Integer per14;
    @Column(name = "PER15")
    private Integer per15;
    @Column(name = "PER16")
    private String per16;
    @Column(name = "PER17")
    private String per17;
    @Column(name = "PER18")
    private String per18;
    @Column(name = "PER18_EST")
    private String per18Est;
    @Column(name = "PER19")
    private String per19;
    @Column(name = "PER20")
    private String per20;
    @Column(name = "PER21")
    private String per21;
    @Column(name = "PER22")
    private String per22;
    @Column(name = "PER23")
    private String per23;
    @Column(name = "PER24")
    private String per24;
    @Column(name = "PER25")
    private String per25;
    @Column(name = "PER26")
    private Integer per26;
    @Column(name = "PER27")
    private Integer per27;
    @Column(name = "PER28")
    private String per28;
    @Column(name = "PER29")
    private Integer per29;
    @Column(name = "PER30")
    private String per30;
    @Column(name = "PER31")
    private String per31;
    @Column(name = "PER32")
    private String per32;
    @Column(name = "PER33")
    private String per33;
    @Column(name = "PER34")
    private String per34;
    @Column(name = "PER35")
    private String per35;
    @Column(name = "PER36")
    private Integer per36;
    @Column(name = "PER37")
    private String per37;
    @Column(name = "PER38")
    private String per38;
    @Column(name = "PER39")
    private String per39;
    @Column(name = "PER40")
    private String per40;
    @Column(name = "PER41")
    private String per41;
    @Column(name = "PER42")
    private String per42;
    @Column(name = "PER43")
    private String per43;
    @Column(name = "PER44")
    private String per44;
    @Column(name = "PER45")
    private String per45;
    @Column(name = "PER46")
    private String per46;
    @Column(name = "PER47")
    private String per47;
    @Column(name = "PER48")
    private String per48;
    @Column(name = "PER49")
    private String per49;
    @Column(name = "PER50")
    private String per50;
    @Column(name = "PER51")
    private String per51;
    @Column(name = "PER52")
    private String per52;
    @Column(name = "PER_A_00")
    private String perA00;
    @Column(name = "PER_A_01")
    private String perA01;
    @Column(name = "PER_A_02")
    private String perA02;
    @Column(name = "PER_A_03")
    private String perA03;
    @Column(name = "PER_A_04")
    private String perA04;
    @Column(name = "PER_A_05")
    private String perA05;
    @Column(name = "PER_A_06")
    private String perA06;
    @Column(name = "PER_A_07")
    private String perA07;
    @Column(name = "PER_A_08")
    private String perA08;
    @Column(name = "PER_A_09")
    private String perA09;
    @Column(name = "PER_A_10")
    private String perA10;
    @Column(name = "A_CARGO_01")
    private String aCargo01;
    @Column(name = "A_CARGO_02")
    private String aCargo02;
    @Column(name = "A_CARGO_03")
    private String aCargo03;
    @Column(name = "A_CARGO_04")
    private String aCargo04;
    @Column(name = "A_CARGO_05")
    private String aCargo05;
    @Column(name = "A_CARGO_06")
    private String aCargo06;
    @Column(name = "A_CARGO_07")
    private String aCargo07;
    @Column(name = "A_CARGO_08")
    private String aCargo08;
    @Column(name = "A_CARGO_09")
    private String aCargo09;
    @Column(name = "A_CARGO_10")
    private String aCargo10;
    @Column(name = "A_CARGO_11")
    private String aCargo11;
    @Column(name = "A_CARGO_12")
    private String aCargo12;
    @Column(name = "A_CARGO_13")
    private String aCargo13;
    @Column(name = "A_CARGO_14")
    private String aCargo14;
    @Column(name = "A_CARGO_15")
    private String aCargo15;
    @Column(name = "A_CARGO_16")
    private String aCargo16;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2017Cabecera matricula2017Cabecera;

    public Matricula2017Personal() {
    }

    public Matricula2017Personal(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NRO")
    public Integer getNro() {
        return nro;
    }

    public void setNro(Integer nro) {
        this.nro = nro;
    }
    @XmlElement(name = "PERDNI")
    public String getPerdni() {
        return perdni;
    }

    public void setPerdni(String perdni) {
        this.perdni = perdni;
    }
    @XmlElement(name = "PER01")
    public String getPer01() {
        return per01;
    }

    public void setPer01(String per01) {
        this.per01 = per01;
    }

    @XmlElement(name = "PER02")
    public Integer getPer02() {
        return per02;
    }

    public void setPer02(Integer per02) {
        this.per02 = per02;
    }

    @XmlElement(name = "PER03")
    public String getPer03() {
        return per03;
    }

    public void setPer03(String per03) {
        this.per03 = per03;
    }

    @XmlElement(name = "PER04")
    public String getPer04() {
        return per04;
    }

    public void setPer04(String per04) {
        this.per04 = per04;
    }

    @XmlElement(name = "PER05")
    public String getPer05() {
        return per05;
    }

    public void setPer05(String per05) {
        this.per05 = per05;
    }

    @XmlElement(name = "PER06")
    public String getPer06() {
        return per06;
    }

    public void setPer06(String per06) {
        this.per06 = per06;
    }

    @XmlElement(name = "PER07")
    public String getPer07() {
        return per07;
    }

    public void setPer07(String per07) {
        this.per07 = per07;
    }

    @XmlElement(name = "PER08")
    public String getPer08() {
        return per08;
    }

    public void setPer08(String per08) {
        this.per08 = per08;
    }

    @XmlElement(name = "PER09")
    public String getPer09() {
        return per09;
    }

    public void setPer09(String per09) {
        this.per09 = per09;
    }

    @XmlElement(name = "PER10")
    public String getPer10() {
        return per10;
    }

    public void setPer10(String per10) {
        this.per10 = per10;
    }

    @XmlElement(name = "PER11")
    public String getPer11() {
        return per11;
    }

    public void setPer11(String per11) {
        this.per11 = per11;
    }

    @XmlElement(name = "PER12")
    public String getPer12() {
        return per12;
    }

    public void setPer12(String per12) {
        this.per12 = per12;
    }

    @XmlElement(name = "PER13")
    public Integer getPer13() {
        return per13;
    }

    public void setPer13(Integer per13) {
        this.per13 = per13;
    }

    @XmlElement(name = "PER14")
    public Integer getPer14() {
        return per14;
    }

    public void setPer14(Integer per14) {
        this.per14 = per14;
    }

    @XmlElement(name = "PER15")
    public Integer getPer15() {
        return per15;
    }

    public void setPer15(Integer per15) {
        this.per15 = per15;
    }

    @XmlElement(name = "PER16")
    public String getPer16() {
        return per16;
    }

    public void setPer16(String per16) {
        this.per16 = per16;
    }

    @XmlElement(name = "PER17")
    public String getPer17() {
        return per17;
    }

    public void setPer17(String per17) {
        this.per17 = per17;
    }

    @XmlElement(name = "PER18")
    public String getPer18() {
        return per18;
    }

    public void setPer18(String per18) {
        this.per18 = per18;
    }

    @XmlElement(name = "PER18_EST")
    public String getPer18Est() {
        return per18Est;
    }

    public void setPer18Est(String per18Est) {
        this.per18Est = per18Est;
    }

    @XmlElement(name = "PER19")
    public String getPer19() {
        return per19;
    }

    public void setPer19(String per19) {
        this.per19 = per19;
    }

    @XmlElement(name = "PER20")
    public String getPer20() {
        return per20;
    }

    public void setPer20(String per20) {
        this.per20 = per20;
    }

    @XmlElement(name = "PER21")
    public String getPer21() {
        return per21;
    }

    public void setPer21(String per21) {
        this.per21 = per21;
    }

    @XmlElement(name = "PER22")
    public String getPer22() {
        return per22;
    }

    public void setPer22(String per22) {
        this.per22 = per22;
    }

    @XmlElement(name = "PER23")
    public String getPer23() {
        return per23;
    }

    public void setPer23(String per23) {
        this.per23 = per23;
    }

    @XmlElement(name = "PER24")
    public String getPer24() {
        return per24;
    }

    public void setPer24(String per24) {
        this.per24 = per24;
    }

    @XmlElement(name = "PER25")
    public String getPer25() {
        return per25;
    }

    public void setPer25(String per25) {
        this.per25 = per25;
    }

    @XmlElement(name = "PER26")
    public Integer getPer26() {
        return per26;
    }

    public void setPer26(Integer per26) {
        this.per26 = per26;
    }

    @XmlElement(name = "PER27")
    public Integer getPer27() {
        return per27;
    }

    public void setPer27(Integer per27) {
        this.per27 = per27;
    }

    @XmlElement(name = "PER28")
    public String getPer28() {
        return per28;
    }

    public void setPer28(String per28) {
        this.per28 = per28;
    }

    @XmlElement(name = "PER29")
    public Integer getPer29() {
        return per29;
    }

    public void setPer29(Integer per29) {
        this.per29 = per29;
    }

    @XmlElement(name = "PER30")
    public String getPer30() {
        return per30;
    }

    public void setPer30(String per30) {
        this.per30 = per30;
    }

    @XmlElement(name = "PER31")
    public String getPer31() {
        return per31;
    }

    public void setPer31(String per31) {
        this.per31 = per31;
    }

    @XmlElement(name = "PER32")
    public String getPer32() {
        return per32;
    }

    public void setPer32(String per32) {
        this.per32 = per32;
    }

    @XmlElement(name = "PER33")
    public String getPer33() {
        return per33;
    }

    public void setPer33(String per33) {
        this.per33 = per33;
    }

    @XmlElement(name = "PER34")
    public String getPer34() {
        return per34;
    }

    public void setPer34(String per34) {
        this.per34 = per34;
    }

    @XmlElement(name = "PER35")
    public String getPer35() {
        return per35;
    }

    public void setPer35(String per35) {
        this.per35 = per35;
    }

    @XmlElement(name = "PER36")
    public Integer getPer36() {
        return per36;
    }

    public void setPer36(Integer per36) {
        this.per36 = per36;
    }
    @XmlElement(name = "PER37")
    public String getPer37() {
        return per37;
    }

    public void setPer37(String per37) {
        this.per37 = per37;
    }
    @XmlElement(name = "PER38")
    public String getPer38() {
        return per38;
    }

    public void setPer38(String per38) {
        this.per38 = per38;
    }
    @XmlElement(name = "PER39")
    public String getPer39() {
        return per39;
    }

    public void setPer39(String per39) {
        this.per39 = per39;
    }
    @XmlElement(name = "PER40")
    public String getPer40() {
        return per40;
    }

    public void setPer40(String per40) {
        this.per40 = per40;
    }
    @XmlElement(name = "PER41")
    public String getPer41() {
        return per41;
    }

    public void setPer41(String per41) {
        this.per41 = per41;
    }
    @XmlElement(name = "PER42")
    public String getPer42() {
        return per42;
    }

    public void setPer42(String per42) {
        this.per42 = per42;
    }
    @XmlElement(name = "PER43")
    public String getPer43() {
        return per43;
    }

    public void setPer43(String per43) {
        this.per43 = per43;
    }
    @XmlElement(name = "PER44")
    public String getPer44() {
        return per44;
    }

    public void setPer44(String per44) {
        this.per44 = per44;
    }
    @XmlElement(name = "PER45")
    public String getPer45() {
        return per45;
    }

    public void setPer45(String per45) {
        this.per45 = per45;
    }
    @XmlElement(name = "PER46")
    public String getPer46() {
        return per46;
    }

    public void setPer46(String per46) {
        this.per46 = per46;
    }
    @XmlElement(name = "PER47")
    public String getPer47() {
        return per47;
    }

    public void setPer47(String per47) {
        this.per47 = per47;
    }
    @XmlElement(name = "PER48")
    public String getPer48() {
        return per48;
    }

    public void setPer48(String per48) {
        this.per48 = per48;
    }
    @XmlElement(name = "PER49")
    public String getPer49() {
        return per49;
    }

    public void setPer49(String per49) {
        this.per49 = per49;
    }
    @XmlElement(name = "PER50")
    public String getPer50() {
        return per50;
    }

    public void setPer50(String per50) {
        this.per50 = per50;
    }
    @XmlElement(name = "PER51")
    public String getPer51() {
        return per51;
    }

    public void setPer51(String per51) {
        this.per51 = per51;
    }

    @XmlElement(name = "PER52")
    public String getPer52() {
        return per52;
    }

    public void setPer52(String per52) {
        this.per52 = per52;
    }
    @XmlElement(name = "PER_A_00")
    public String getPerA00() {
        return perA00;
    }

    public void setPerA00(String perA00) {
        this.perA00 = perA00;
    }

    @XmlElement(name = "PER_A_01")
    public String getPerA01() {
        return perA01;
    }

    public void setPerA01(String perA01) {
        this.perA01 = perA01;
    }

    @XmlElement(name = "PER_A_02")
    public String getPerA02() {
        return perA02;
    }

    public void setPerA02(String perA02) {
        this.perA02 = perA02;
    }

    @XmlElement(name = "PER_A_03")
    public String getPerA03() {
        return perA03;
    }

    public void setPerA03(String perA03) {
        this.perA03 = perA03;
    }

    @XmlElement(name = "PER_A_04")
    public String getPerA04() {
        return perA04;
    }

    public void setPerA04(String perA04) {
        this.perA04 = perA04;
    }

    @XmlElement(name = "PER_A_05")
    public String getPerA05() {
        return perA05;
    }

    public void setPerA05(String perA05) {
        this.perA05 = perA05;
    }

    @XmlElement(name = "PER_A_06")
    public String getPerA06() {
        return perA06;
    }

    public void setPerA06(String perA06) {
        this.perA06 = perA06;
    }

    @XmlElement(name = "PER_A_07")
    public String getPerA07() {
        return perA07;
    }

    public void setPerA07(String perA07) {
        this.perA07 = perA07;
    }

    @XmlElement(name = "PER_A_08")
    public String getPerA08() {
        return perA08;
    }

    public void setPerA08(String perA08) {
        this.perA08 = perA08;
    }

    @XmlElement(name = "PER_A_09")
    public String getPerA09() {
        return perA09;
    }

    public void setPerA09(String perA09) {
        this.perA09 = perA09;
    }

    @XmlElement(name = "PER_A_10")
    public String getPerA10() {
        return perA10;
    }

    public void setPerA10(String perA10) {
        this.perA10 = perA10;
    }

    @XmlElement(name = "A_CARGO_01")
    public String getACargo01() {
        return aCargo01;
    }

    public void setACargo01(String aCargo01) {
        this.aCargo01 = aCargo01;
    }

    @XmlElement(name = "A_CARGO_02")
    public String getACargo02() {
        return aCargo02;
    }

    public void setACargo02(String aCargo02) {
        this.aCargo02 = aCargo02;
    }

    @XmlElement(name = "A_CARGO_03")
    public String getACargo03() {
        return aCargo03;
    }

    public void setACargo03(String aCargo03) {
        this.aCargo03 = aCargo03;
    }

    @XmlElement(name = "A_CARGO_04")
    public String getACargo04() {
        return aCargo04;
    }

    public void setACargo04(String aCargo04) {
        this.aCargo04 = aCargo04;
    }

    @XmlElement(name = "A_CARGO_05")
    public String getACargo05() {
        return aCargo05;
    }

    public void setACargo05(String aCargo05) {
        this.aCargo05 = aCargo05;
    }

    @XmlElement(name = "A_CARGO_06")
    public String getACargo06() {
        return aCargo06;
    }

    public void setACargo06(String aCargo06) {
        this.aCargo06 = aCargo06;
    }

    @XmlElement(name = "A_CARGO_07")
    public String getACargo07() {
        return aCargo07;
    }

    public void setACargo07(String aCargo07) {
        this.aCargo07 = aCargo07;
    }

    @XmlElement(name = "A_CARGO_08")
    public String getACargo08() {
        return aCargo08;
    }

    public void setACargo08(String aCargo08) {
        this.aCargo08 = aCargo08;
    }

    @XmlElement(name = "A_CARGO_09")
    public String getACargo09() {
        return aCargo09;
    }

    public void setACargo09(String aCargo09) {
        this.aCargo09 = aCargo09;
    }

    @XmlElement(name = "A_CARGO_10")
    public String getACargo10() {
        return aCargo10;
    }

    public void setACargo10(String aCargo10) {
        this.aCargo10 = aCargo10;
    }

    @XmlElement(name = "A_CARGO_11")
    public String getACargo11() {
        return aCargo11;
    }

    public void setACargo11(String aCargo11) {
        this.aCargo11 = aCargo11;
    }

    @XmlElement(name = "A_CARGO_12")
    public String getACargo12() {
        return aCargo12;
    }

    public void setACargo12(String aCargo12) {
        this.aCargo12 = aCargo12;
    }

    @XmlElement(name = "A_CARGO_13")
    public String getACargo13() {
        return aCargo13;
    }

    public void setACargo13(String aCargo13) {
        this.aCargo13 = aCargo13;
    }

    @XmlElement(name = "A_CARGO_14")
    public String getACargo14() {
        return aCargo14;
    }

    public void setACargo14(String aCargo14) {
        this.aCargo14 = aCargo14;
    }

    @XmlElement(name = "A_CARGO_15")
    public String getACargo15() {
        return aCargo15;
    }

    public void setACargo15(String aCargo15) {
        this.aCargo15 = aCargo15;
    }

    @XmlElement(name = "A_CARGO_16")
    public String getACargo16() {
        return aCargo16;
    }

    public void setACargo16(String aCargo16) {
        this.aCargo16 = aCargo16;
    }

    @XmlTransient
    public Matricula2017Cabecera getMatricula2017Cabecera() {
        return matricula2017Cabecera;
    }

    public void setMatricula2017Cabecera(Matricula2017Cabecera matricula2017Cabecera) {
        this.matricula2017Cabecera = matricula2017Cabecera;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Matricula2017Personal other = (Matricula2017Personal) obj;
        if (this.idEnvio != other.idEnvio && (this.idEnvio == null || !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.idEnvio != null ? this.idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Matricula2017Personal{" + "idEnvio=" + idEnvio + '}';
    }
}
