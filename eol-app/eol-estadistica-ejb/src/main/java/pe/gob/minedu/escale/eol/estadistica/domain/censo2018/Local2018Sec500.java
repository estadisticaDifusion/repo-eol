/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "local2018_sec500")
public class Local2018Sec500 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "PREGUNTA")
    private String pregunta;
    @Column(name = "ORDEN")
    private Short orden;
    @Column(name = "P500_ESP")
    private String p500Esp;
    @Column(name = "P500_CHK1")
    private String p500Chk1;
    @Column(name = "P500_CHK2")
    private String p500Chk2;
    @Column(name = "P500_CHK3")
    private String p500Chk3;
    @Column(name = "TOTAL")
    private Integer total;
    @Column(name = "P500_1")
    private Integer p5001;
    @Column(name = "P500_2")
    private Integer p5002;
    @Column(name = "P500_3")
    private Integer p5003;
    @Column(name = "P500_4")
    private Integer p5004;
    @Column(name = "P500_5")
    private Integer p5005;
    @Column(name = "P500_6")
    private Integer p5006;
    @Column(name = "P500_7")
    private Integer p5007;
    @Column(name = "P500_8")
    private Integer p5008;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2018Cabecera local2018Cabecera;

    public Local2018Sec500() {
    }

    public Local2018Sec500(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="PREGUNTA")
    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @XmlElement(name="ORDEN")
    public Short getOrden() {
        return orden;
    }

    public void setOrden(Short orden) {
        this.orden = orden;
    }

    @XmlElement(name="P500_ESP")
    public String getP500Esp() {
        return p500Esp;
    }

    public void setP500Esp(String p500Esp) {
        this.p500Esp = p500Esp;
    }

    @XmlElement(name="P500_CHK1")
    public String getP500Chk1() {
        return p500Chk1;
    }

    public void setP500Chk1(String p500Chk1) {
        this.p500Chk1 = p500Chk1;
    }

    @XmlElement(name="P500_CHK2")
    public String getP500Chk2() {
        return p500Chk2;
    }

    public void setP500Chk2(String p500Chk2) {
        this.p500Chk2 = p500Chk2;
    }

    @XmlElement(name="P500_CHK3")
    public String getP500Chk3() {
        return p500Chk3;
    }

    public void setP500Chk3(String p500Chk3) {
        this.p500Chk3 = p500Chk3;
    }

    @XmlElement(name="TOTAL")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
    
    @XmlElement(name="P500_1")
    public Integer getP5001() {
        return p5001;
    }

    public void setP5001(Integer p5001) {
        this.p5001 = p5001;
    }

    @XmlElement(name="P500_2")
    public Integer getP5002() {
        return p5002;
    }

    public void setP5002(Integer p5002) {
        this.p5002 = p5002;
    }

    @XmlElement(name="P500_3")
    public Integer getP5003() {
        return p5003;
    }

    public void setP5003(Integer p5003) {
        this.p5003 = p5003;
    }

    @XmlElement(name="P500_4")
    public Integer getP5004() {
        return p5004;
    }

    public void setP5004(Integer p5004) {
        this.p5004 = p5004;
    }

    @XmlElement(name="P500_5")
    public Integer getP5005() {
        return p5005;
    }

    public void setP5005(Integer p5005) {
        this.p5005 = p5005;
    }

    @XmlElement(name="P500_6")
    public Integer getP5006() {
        return p5006;
    }

    public void setP5006(Integer p5006) {
        this.p5006 = p5006;
    }

    @XmlElement(name="P500_7")
    public Integer getP5007() {
        return p5007;
    }

    public void setP5007(Integer p5007) {
        this.p5007 = p5007;
    }

    @XmlElement(name="P500_8")
    public Integer getP5008() {
        return p5008;
    }

    public void setP5008(Integer p5008) {
        this.p5008 = p5008;
    }

    @XmlTransient
    public Local2018Cabecera getLocal2018Cabecera() {
        return local2018Cabecera;
    }

    public void setLocal2018Cabecera(Local2018Cabecera local2018Cabecera) {
        this.local2018Cabecera = local2018Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2018Sec500)) {
            return false;
        }
        Local2018Sec500 other = (Local2018Sec500) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Local2018Sec500[idEnvio=" + idEnvio + "]";
    }

}
