/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2014_recursos_fila")

public class Matricula2014RecursosFila implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL")
    private Integer total;
    @Column(name = "DATO01")
    private Integer dato01;
    @Column(name = "DATO02")
    private Integer dato02;
    @Column(name = "DATO03")
    private Integer dato03;
    @Column(name = "DATO04")
    private Integer dato04;
    @Column(name = "DATO05")
    private Integer dato05;
    @Column(name = "DATO06")
    private Integer dato06;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2014Recursos matricula2014Recursos;

    public Matricula2014RecursosFila() {
    }

    public Matricula2014RecursosFila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name="TOTAL")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @XmlElement(name="DATO01")
    public Integer getDATO01() {
        return dato01;
    }

    public void setDATO01(Integer dato01) {
        this.dato01 = dato01;
    }

    @XmlElement(name="DATO02")
    public Integer getDATO02() {
        return dato02;
    }

    public void setDATO02(Integer dato02) {
        this.dato02 = dato02;
    }

    @XmlElement(name="DATO03")
    public Integer getDATO03() {
        return dato03;
    }

    public void setDATO03(Integer dato03) {
        this.dato03 = dato03;
    }

    @XmlElement(name="DATO04")
    public Integer getDATO04() {
        return dato04;
    }

    public void setDATO04(Integer dato04) {
        this.dato04 = dato04;
    }

    @XmlElement(name="DATO05")
    public Integer getDATO05() {
        return dato05;
    }

    public void setDATO05(Integer dato05) {
        this.dato05 = dato05;
    }

    @XmlElement(name="DATO06")
    public Integer getDATO06() {
        return dato06;
    }

    public void setDATO06(Integer dato06) {
        this.dato06 = dato06;
    }

    @XmlTransient
    public Matricula2014Recursos getMatricula2014Recursos() {
        return matricula2014Recursos;
    }

    public void setMatricula2014Recursos(Matricula2014Recursos matricula2014Recursos) {
        this.matricula2014Recursos = matricula2014Recursos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2014RecursosFila)) {
            return false;
        }
        Matricula2014RecursosFila other = (Matricula2014RecursosFila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @XmlElement(name="DESCRIP")
    public String getDescrip() {
        return descrip;
    }

  
    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

}
