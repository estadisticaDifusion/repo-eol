package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "matricula2015_localpronoei")
public class Matricula2015Localpronoei implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P501_A")
    private String p501A;
    @Column(name = "P501_M")
    private String p501M;
    @Column(name = "P502")
    private String p502;
    @Column(name = "P502_ESP")
    private String p502Esp;
    @Column(name = "P503")
    private String p503;
    @Column(name = "P503_7_CM")
    private String p5037Cm;
    @Column(name = "P503_7_NM")
    private String p5037Nm;
    @Column(name = "P503_ESP")
    private String p503Esp;
    @Column(name = "P504")
    private String p504;
    @Column(name = "P505")
    private String p505;
    @Column(name = "P506")
    private String p506;
    @Column(name = "P507")
    private String p507;
    @Column(name = "P507_NSUM")
    private String p507Nsum;
    @Column(name = "P508")
    private String p508;
    @Column(name = "P508_ESP")
    private String p508Esp;
    @Column(name = "P508_NSUM")
    private String p508Nsum;
    @Column(name = "P509")
    private String p509;
    @Column(name = "P510")
    private String p510;
    @Column(name = "P511")
    private String p511;
    @Column(name = "P512_00")
    private Integer p51200;
    @Column(name = "P512_01")
    private Integer p51201;
    @Column(name = "P512_02")
    private Integer p51202;
    @Column(name = "P512_03")
    private Integer p51203;
    @Column(name = "P512_10")
    private Integer p51210;
    @Column(name = "P512_11")
    private Integer p51211;
    @Column(name = "P512_12")
    private Integer p51212;
    @Column(name = "P512_13")
    private Integer p51213;
    @Column(name = "P512_20")
    private Integer p51220;
    @Column(name = "P512_21")
    private Integer p51221;
    @Column(name = "P512_22")
    private Integer p51222;
    @Column(name = "P512_23")
    private Integer p51223;
    @Column(name = "P512_30")
    private Integer p51230;
    @Column(name = "P512_31")
    private Integer p51231;
    @Column(name = "P512_32")
    private Integer p51232;
    @Column(name = "P512_33")
    private Integer p51233;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2015Cabecera matricula2015Cabecera;

    public Matricula2015Localpronoei() {
    }

    public Matricula2015Localpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P501_A")
    public String getP501A() {
        return p501A;
    }

    public void setP501A(String p501A) {
        this.p501A = p501A;
    }

    @XmlElement(name="P501_M")
    public String getP501M() {
        return p501M;
    }

    public void setP501M(String p501M) {
        this.p501M = p501M;
    }

    @XmlElement(name="P502")
    public String getP502() {
        return p502;
    }

    public void setP502(String p502) {
        this.p502 = p502;
    }

    @XmlElement(name="P502_ESP")
    public String getP502Esp() {
        return p502Esp;
    }

    public void setP502Esp(String p502Esp) {
        this.p502Esp = p502Esp;
    }

    @XmlElement(name="P503")
    public String getP503() {
        return p503;
    }

    public void setP503(String p503) {
        this.p503 = p503;
    }

    @XmlElement(name="P503_7_CM")
    public String getP5037Cm() {
        return p5037Cm;
    }

    public void setP5037Cm(String p5037Cm) {
        this.p5037Cm = p5037Cm;
    }

    @XmlElement(name="P503_7_NM")
    public String getP5037Nm() {
        return p5037Nm;
    }

    public void setP5037Nm(String p5037Nm) {
        this.p5037Nm = p5037Nm;
    }

    @XmlElement(name="P503_ESP")
    public String getP503Esp() {
        return p503Esp;
    }

    public void setP503Esp(String p503Esp) {
        this.p503Esp = p503Esp;
    }

    @XmlElement(name="P504")
    public String getP504() {
        return p504;
    }

    public void setP504(String p504) {
        this.p504 = p504;
    }

    @XmlElement(name="P505")
    public String getP505() {
        return p505;
    }

    public void setP505(String p505) {
        this.p505 = p505;
    }

    @XmlElement(name="P506")
    public String getP506() {
        return p506;
    }

    public void setP506(String p506) {
        this.p506 = p506;
    }

    @XmlElement(name="P507")
    public String getP507() {
        return p507;
    }

    public void setP507(String p507) {
        this.p507 = p507;
    }

    @XmlElement(name="P507_NSUM")
    public String getP507Nsum() {
        return p507Nsum;
    }

    public void setP507Nsum(String p507Nsum) {
        this.p507Nsum = p507Nsum;
    }

    @XmlElement(name="P508")
    public String getP508() {
        return p508;
    }

    public void setP508(String p508) {
        this.p508 = p508;
    }

    @XmlElement(name="P508_ESP")
    public String getP508Esp() {
        return p508Esp;
    }

    public void setP508Esp(String p508Esp) {
        this.p508Esp = p508Esp;
    }

    @XmlElement(name="P508_NSUM")
    public String getP508Nsum() {
        return p508Nsum;
    }

    public void setP508Nsum(String p508Nsum) {
        this.p508Nsum = p508Nsum;
    }

    @XmlElement(name="P509")
    public String getP509() {
        return p509;
    }

    public void setP509(String p509) {
        this.p509 = p509;
    }

    @XmlElement(name="P510")
    public String getP510() {
        return p510;
    }

    public void setP510(String p510) {
        this.p510 = p510;
    }

    @XmlElement(name="P511")
    public String getP511() {
        return p511;
    }

    public void setP511(String p511) {
        this.p511 = p511;
    }

    @XmlElement(name="P512_00")
    public Integer getP51200() {
        return p51200;
    }

    public void setP51200(Integer p51200) {
        this.p51200 = p51200;
    }

    @XmlElement(name="P512_01")
    public Integer getP51201() {
        return p51201;
    }

    public void setP51201(Integer p51201) {
        this.p51201 = p51201;
    }

    @XmlElement(name="P512_02")
    public Integer getP51202() {
        return p51202;
    }

    public void setP51202(Integer p51202) {
        this.p51202 = p51202;
    }

    @XmlElement(name="P512_03")
    public Integer getP51203() {
        return p51203;
    }

    public void setP51203(Integer p51203) {
        this.p51203 = p51203;
    }

    @XmlElement(name="P512_10")
    public Integer getP51210() {
        return p51210;
    }

    public void setP51210(Integer p51210) {
        this.p51210 = p51210;
    }

    @XmlElement(name="P512_11")
    public Integer getP51211() {
        return p51211;
    }

    public void setP51211(Integer p51211) {
        this.p51211 = p51211;
    }

    @XmlElement(name="P512_12")
    public Integer getP51212() {
        return p51212;
    }

    public void setP51212(Integer p51212) {
        this.p51212 = p51212;
    }

    @XmlElement(name="P512_13")
    public Integer getP51213() {
        return p51213;
    }

    public void setP51213(Integer p51213) {
        this.p51213 = p51213;
    }

    @XmlElement(name="P512_20")
    public Integer getP51220() {
        return p51220;
    }

    public void setP51220(Integer p51220) {
        this.p51220 = p51220;
    }

    @XmlElement(name="P512_21")
    public Integer getP51221() {
        return p51221;
    }

    public void setP51221(Integer p51221) {
        this.p51221 = p51221;
    }

    @XmlElement(name="P512_22")
    public Integer getP51222() {
        return p51222;
    }

    public void setP51222(Integer p51222) {
        this.p51222 = p51222;
    }

    @XmlElement(name="P512_23")
    public Integer getP51223() {
        return p51223;
    }

    public void setP51223(Integer p51223) {
        this.p51223 = p51223;
    }

    @XmlElement(name="P512_30")
    public Integer getP51230() {
        return p51230;
    }

    public void setP51230(Integer p51230) {
        this.p51230 = p51230;
    }

    @XmlElement(name="P512_31")
    public Integer getP51231() {
        return p51231;
    }

    public void setP51231(Integer p51231) {
        this.p51231 = p51231;
    }

    @XmlElement(name="P512_32")
    public Integer getP51232() {
        return p51232;
    }

    public void setP51232(Integer p51232) {
        this.p51232 = p51232;
    }

    @XmlElement(name="P512_33")
    public Integer getP51233() {
        return p51233;
    }

    public void setP51233(Integer p51233) {
        this.p51233 = p51233;
    }

    @XmlTransient
    public Matricula2015Cabecera getMatricula2015Cabecera() {
        return matricula2015Cabecera;
    }

    public void setMatricula2015Cabecera(Matricula2015Cabecera matricula2015Cabecera) {
        this.matricula2015Cabecera = matricula2015Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2015Localpronoei)) {
            return false;
        }
        Matricula2015Localpronoei other = (Matricula2015Localpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
