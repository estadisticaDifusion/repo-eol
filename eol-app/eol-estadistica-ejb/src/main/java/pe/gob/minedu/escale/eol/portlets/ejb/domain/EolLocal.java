/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.ejb.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author JMATAMOROS
 */
@Entity(name = "EolLocal")
@Table(schema = "padron", name = "locales")
public class EolLocal implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CODLOCAL")
	private String codlocal;

	@Column(name = "DIR_CEN")
	private String dirCen;

	@Column(name = "TELEFONO")
	private String telefono;

	@ManyToOne
	@JoinColumn(referencedColumnName = "id_distrito", name = "CODGEO")
	private Distrito distrito;

	@ManyToOne
	@JoinColumn(referencedColumnName = "codigo", name = "CODOOII")
	private DreUgel dreUgel;

	@OneToMany(mappedBy = "eolLocal", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Collection<EolCenso> envios;

	@Column(name = "CODIGEL")
	private String codigel;

	@Column(name = "TIPOIGEL")
	private String tipoigel;

	@Column(name = "CEN_POB")
	private String cenPob;

	@Column(name = "CODCCPP")
	private String codccpp;

	@Column(name = "COD_AREA")
	private String codArea;

	@Column(name = "ESTADO")
	private String estado;

	@Column(name = "AREA_SIG")
	private String areaSig;

	@Column(name = "NIV_MOD")
	private String nivMod;

	@Column(name = "GESTION")
	private String gestion;

	@Column(name = "CEN_EDU")
	private String cenEdu;

	@Column(name = "GES_DEP")
	private String gesDep;

	@Column(name = "DIRECTOR")
	private String director;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "PAGWEB")
	private String pagWeb;

	@Column(name = "LOCALIDAD")
	private String localidad;

	@Column(name = "DRE")
	private String dre;

	public EolLocal() {
	}

	public EolLocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getDirCen() {
		return dirCen;
	}

	public void setDirCen(String dirCen) {
		this.dirCen = dirCen;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCodigel() {
		return codigel;
	}

	public void setCodigel(String codigel) {
		this.codigel = codigel;
	}

	public String getTipoigel() {
		return tipoigel;
	}

	public void setTipoigel(String tipoigel) {
		this.tipoigel = tipoigel;
	}

	public String getCenPob() {
		return cenPob;
	}

	public void setCenPob(String cenPob) {
		this.cenPob = cenPob;
	}

	public String getCodccpp() {
		return codccpp;
	}

	public void setCodccpp(String codccpp) {
		this.codccpp = codccpp;
	}

	public String getCodArea() {
		return codArea;
	}

	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getAreaSig() {
		return areaSig;
	}

	public void setAreaSig(String areaSig) {
		this.areaSig = areaSig;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codlocal != null ? codlocal.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof EolLocal)) {
			return false;
		}
		EolLocal other = (EolLocal) object;
		if ((this.codlocal == null && other.codlocal != null)
				|| (this.codlocal != null && !this.codlocal.equals(other.codlocal))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.eol.portlets.ejb.domain.EolLocal[codlocal=" + codlocal + "]";
	}

	public Distrito getDistrito() {
		return distrito;
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}

	/*
	 * public Ugel getUgel() { return ugel; }
	 * 
	 * public void setUgel(Ugel ugel) { this.ugel = ugel; }
	 */

	public Collection<EolCenso> getEnvios() {
		return envios;
	}

	public void setEnvios(Collection<EolCenso> envios) {
		this.envios = envios;
	}

	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	/**
	 * @return the gestion
	 */
	public String getGestion() {
		return gestion;
	}

	/**
	 * @param gestion the gestion to set
	 */
	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	public String getGesDep() {
		return gesDep;
	}

	public void setGesDep(String gesDep) {
		this.gesDep = gesDep;
	}

	/**
	 * @return the director
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * @param director the director to set
	 */
	public void setDirector(String director) {
		this.director = director;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the pagWeb
	 */
	public String getPagWeb() {
		return pagWeb;
	}

	/**
	 * @param pagWeb the pagWeb to set
	 */
	public void setPagWeb(String pagWeb) {
		this.pagWeb = pagWeb;
	}

	/**
	 * @return the localidad
	 */
	public String getLocalidad() {
		return localidad;
	}

	/**
	 * @param localidad the localidad to set
	 */
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	/**
	 * @return the dre
	 */
	public String getDre() {
		return dre;
	}

	/**
	 * @param dre the dre to set
	 */
	public void setDre(String dre) {
		this.dre = dre;
	}

	public DreUgel getDreUgel() {
		return dreUgel;
	}

	public void setDreUgel(DreUgel dreUgel) {
		this.dreUgel = dreUgel;
	}
}
