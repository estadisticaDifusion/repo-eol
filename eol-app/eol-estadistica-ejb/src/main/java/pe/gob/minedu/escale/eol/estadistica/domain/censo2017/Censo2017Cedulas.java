/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author warodriguez
 */
@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2017_cedulas")
public class Censo2017Cedulas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "NROCED")
    private String nroced;
    @Column(name = "COD_MOD")
    private String codMod;
    @Column(name = "CEN_EDU")
    private String cenEdu;
    @Column(name = "ANEXO")
    private String anexo;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "P101")
    private String p101;
    @Column(name = "P102")
    private String p102;
    @Column(name = "P103")
    private String p103;
    @Column(name = "P104")
    private String p104;
    @Column(name = "P1051")
    private String p1051;
    @Column(name = "P1052")
    private String p1052;
    @Column(name = "P1053")
    private String p1053;
    @Column(name = "P1054")
    private String p1054;
    @Column(name = "P1055")
    private String p1055;
    @Column(name = "P1056")
    private String p1056;
    @Column(name = "P106")
    private String p106;
    @Column(name = "P107")
    private String p107;
    @Column(name = "P108")
    private String p108;
    @Column(name = "P2011")
    private String p2011;
    @Column(name = "P2012")
    private String p2012;
    @Column(name = "P2013")
    private String p2013;
    @Column(name = "P2014")
    private String p2014;
    @Column(name = "P2015")
    private String p2015;
    @Column(name = "P2016")
    private String p2016;
    @Column(name = "P202")
    private String p202;
    @Column(name = "P202_E")
    private String p202E;
    @Column(name = "P203")
    private String p203;
    @Column(name = "APATERNO")
    private String apaterno;
    @Column(name = "AMATERNO")
    private String amaterno;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "FUENTE")
    private String fuente;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "SITUACION")
    private String situacion;

    @Transient
    private String ESTADO_RPT;
    @Transient
    private String MSG;

    @Transient
    private long token;
   

    @Transient
    private String DESC_CONST;

    public Censo2017Cedulas() {
    }

    public Censo2017Cedulas(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Censo2017Cedulas(Long idEnvio, String nroced, String codMod, String cenEdu, String anexo, Date fechaEnvio) {
        this.idEnvio = idEnvio;
        this.nroced = nroced;
        this.codMod = codMod;
        this.cenEdu = cenEdu;
        this.anexo = anexo;
        this.fechaEnvio = fechaEnvio;
    }

    @XmlAttribute
    public Long getIdEnvio()                                        {       return idEnvio;}

    public void setIdEnvio(Long idEnvio)                            {       this.idEnvio = idEnvio;}

    @XmlElement(name = "NROCED")
    public String getNroced()                                       {       return nroced;}

    public void setNroced(String nroced)                            {       this.nroced = nroced;}

    @XmlElement(name = "COD_MOD")
    public String getCodMod()                                       {       return codMod;}

    public void setCodMod(String codMod)                            {       this.codMod = codMod;}

    @XmlElement(name = "CEN_EDU")
    public String getCenEdu()                                       {       return cenEdu;}

    public void setCenEdu(String cenEdu)                            {       this.cenEdu = cenEdu;}

    @XmlElement(name = "ANEXO")
    public String getAnexo()                                        {       return anexo;}

    public void setAnexo(String anexo)                              {       this.anexo = anexo;}

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal()                                     {       return codlocal;}

    public void setCodlocal(String codlocal)                        {       this.codlocal = codlocal;}

    @XmlElement(name = "CODUGEL")
    public String getCodugel()                                      {       return codugel;}

    public void setCodugel(String codugel)                          {       this.codugel = codugel;}

    @XmlElement(name = "P101")
    public String getP101()                                         {       return p101;}

    public void setP101(String p101)                                {       this.p101 = p101;}

    @XmlElement(name = "P102")
    public String getP102()                                         {       return p102;}

    public void setP102(String p102)                                {       this.p102 = p102;}

    @XmlElement(name = "P103")
    public String getP103()                                         {       return p103;}

    public void setP103(String p103)                                {       this.p103 = p103;}

    @XmlElement(name = "P104")
    public String getP104()                                         {       return p104;}

    public void setP104(String p104)                                {       this.p104 = p104;}

    @XmlElement(name = "P1051")
    public String getP1051()                                        {       return p1051;}

    public void setP1051(String p1051)                              {       this.p1051 = p1051;}

    @XmlElement(name = "P1052")
    public String getP1052()                                        {       return p1052;}

    public void setP1052(String p1052)                              {       this.p1052 = p1052;}

    @XmlElement(name = "P1053")
    public String getP1053()                                        {       return p1053;}

    public void setP1053(String p1053)                              {       this.p1053 = p1053;}

    @XmlElement(name = "P1054")
    public String getP1054()                                        {       return p1054;}

    public void setP1054(String p1054)                              {       this.p1054 = p1054;}

    @XmlElement(name = "P1055")
    public String getP1055()                                        {       return p1055;}

    public void setP1055(String p1055)                              {       this.p1055 = p1055;}

    @XmlElement(name = "P1056")
    public String getP1056()                                        {       return p1056;}

    public void setP1056(String p1056)                              {       this.p1056 = p1056;}

    @XmlElement(name = "P106")
    public String getP106()                                         {       return p106;}

    public void setP106(String p106)                                {       this.p106 = p106;}

    @XmlElement(name = "P107")
    public String getP107()                                         {       return p107;}

    public void setP107(String p107)                                {       this.p107 = p107;}

    @XmlElement(name = "P108")
    public String getP108()                                         {       return p108;}

    public void setP108(String p108)                                {       this.p108 = p108;}

    @XmlElement(name = "P2011")
    public String getP2011()                                        {       return p2011;}

    public void setP2011(String p2011)                              {       this.p2011 = p2011;}

    @XmlElement(name = "P2012")
    public String getP2012()                                        {       return p2012;}

    public void setP2012(String p2012)                              {       this.p2012 = p2012;}

    @XmlElement(name = "P2013")
    public String getP2013()                                        {       return p2013;}

    public void setP2013(String p2013)                              {       this.p2013 = p2013;}

    @XmlElement(name = "P2014")
    public String getP2014()                                        {       return p2014;}

    public void setP2014(String p2014)                              {       this.p2014 = p2014;}

    @XmlElement(name = "P2015")
    public String getP2015()                                        {       return p2015;}

    public void setP2015(String p2015)                              {       this.p2015 = p2015;}

    @XmlElement(name = "P2016")
    public String getP2016()                                        {       return p2016;}

    public void setP2016(String p2016)                              {       this.p2016 = p2016;}

    @XmlElement(name = "P202")
    public String getP202()                                         {       return p202;}

    public void setP202(String p202)                                {       this.p202 = p202;}

    @XmlElement(name = "P202_E")
    public String getP202E()                                        {       return p202E;}

    public void setP202E(String p202E)                              {       this.p202E = p202E;}

    @XmlElement(name = "P203")
    public String getP203()                                         {       return p203;}

    public void setP203(String p203)                                {       this.p203 = p203;}

    @XmlElement(name = "APATERNO")
    public String getApaterno()                                     {       return apaterno;}

    public void setApaterno(String apaterno)                        {       this.apaterno = apaterno;}

    @XmlElement(name = "AMATERNO")
    public String getAmaterno()                                     {       return amaterno;}

    public void setAmaterno(String amaterno)                        {       this.amaterno = amaterno;}

    @XmlElement(name = "NOMBRES")
    public String getNombres()                                      {       return nombres;}

    public void setNombres(String nombres)                          {       this.nombres = nombres;}

    @XmlElement(name = "FUENTE")
    public String getFuente()                                       {       return fuente;}

    public void setFuente(String fuente)                            {       this.fuente = fuente;}

    @XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio()                                     {       return fechaEnvio;}

    public void setFechaEnvio(Date fechaEnvio)                      {       this.fechaEnvio = fechaEnvio;}

    @XmlElement(name = "ULTIMO")
    public Boolean getUltimo()                                      {       return ultimo;}

    public void setUltimo(Boolean ultimo)                           {       this.ultimo = ultimo;}

    @XmlElement(name = "VERSION")
    public String getVersion()                                      {       return version;}

    public void setVersion(String version)                          {       this.version = version;}

    @XmlElement(name = "SITUACION")
    public String getSituacion()                                    {       return situacion;}

    public void setSituacion(String situacion)                      {       this.situacion = situacion;}

    @XmlAttribute(name="ESTADO_RPT")
    public String getESTADO_RPT()                                   {       return ESTADO_RPT;}

    public void setESTADO_RPT(String ESTADO_RPT)                    {       this.ESTADO_RPT = ESTADO_RPT;}



    @XmlAttribute(name = "MSG")
    public String getMSG()                                          {       return MSG;}

    public void setMSG(String MSG)                                  {       this.MSG = MSG;}

    @XmlAttribute
    public long getToken()                                          {       return token;}

    public void setToken(long token)                                {       this.token = token;}

    @XmlAttribute(name = "DESC_CONST")
    public String getDESC_CONST()                                   {       return DESC_CONST;}

    public void setDESC_CONST(String DESC_CONST)                    {       this.DESC_CONST = DESC_CONST;}



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Censo2017Cedulas)) {
            return false;
        }
        Censo2017Cedulas other = (Censo2017Cedulas) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Censo2017Cedulas{" + "idEnvio=" + idEnvio + "nroced=" + nroced + "codMod=" + codMod + "cenEdu=" + cenEdu + "anexo=" + anexo + "codlocal=" + codlocal + "codugel=" + codugel + "p101=" + p101 + "p102=" + p102 + "p103=" + p103 + "p104=" + p104 + "p1051=" + p1051 + "p1052=" + p1052 + "p1053=" + p1053 + "p1054=" + p1054 + "p1055=" + p1055 + "p1056=" + p1056 + "p106=" + p106 + "p107=" + p107 + "p108=" + p108 + "p2011=" + p2011 + "p2012=" + p2012 + "p2013=" + p2013 + "p2014=" + p2014 + "p2015=" + p2015 + "p2016=" + p2016 + "p202=" + p202 + "p202E=" + p202E + "p203=" + p203 + "apaterno=" + apaterno + "amaterno=" + amaterno + "nombres=" + nombres + "fuente=" + fuente + "fechaEnvio=" + fechaEnvio + "ultimo=" + ultimo + "version=" + version + "situacion=" + situacion + "ESTADO_RPT=" + ESTADO_RPT + "MSG=" + MSG + "token=" + token + "DESC_CONST=" + DESC_CONST + '}';
    }

   

   

  

}
