/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.ejb.censo2017;

import java.util.Date;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Censo2017Cedulas;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author warodriguez
 */
@Singleton
public class CedulaSFacade extends AbstractFacade<Censo2017Cedulas> {

	public static final String CEDULA_MATRICULA_CEDULA_S = "EVALUACION-SIAGIE";
	// public static final String CED2017 = "2017";

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public CedulaSFacade() {
		super(Censo2017Cedulas.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/*
	 * Insert de cedula S.
	 * 
	 * @Param (codigo Modular)
	 */
	@Override
	public void create(Censo2017Cedulas cedulas) {
		String sql = "UPDATE Censo2017Cedulas a SET a.ultimo=false WHERE  a.codMod=:codMod";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedulas.getCodMod());
		query.executeUpdate();
		cedulas.setUltimo(true);
		if (cedulas.getFechaEnvio() == null)
			cedulas.setFechaEnvio(new Date());

		em.persist(cedulas);
		em.flush();

	}

}
