/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb.censo2013;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2013.Resultado2013Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author DSILVA
 */
@Singleton
public class Resultado2013Facade extends AbstractFacade<Resultado2013Cabecera> {

	public static final String CEDULA_RESULTADO = "CEDULA-RESULTADO";
	static final Logger LOGGER = Logger.getLogger(Resultado2013Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Resultado2013Facade() {
		super(Resultado2013Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Resultado2013Cabecera cedula) {
		String sql = "UPDATE Resultado2013Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo and a.nroced=:nroced ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroced", cedula.getNroced());
		query.executeUpdate();
		/*
		 * Set<String> keys = cedula.getDetalle().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Resultado2011Detalle detalle = cedula.getDetalle().get(key);
		 * detalle.setCedula(cedula); List<Resultado2011Fila> filas =
		 * detalle.getFilas(); for (Resultado2011Fila fila : filas) {
		 * fila.setDetalle(detalle); } }
		 */
		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

}
