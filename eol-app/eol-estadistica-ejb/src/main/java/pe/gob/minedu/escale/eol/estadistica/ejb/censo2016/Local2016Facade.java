package pe.gob.minedu.escale.eol.estadistica.ejb.censo2016;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2016.Local2016Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class Local2016Facade extends AbstractFacade<Local2016Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2016Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Local2016Facade() {
		super(Local2016Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Local2016Cabecera entity) {
		String sql = "UPDATE Local2016Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();
		/*
		 * OM for (Local2016Sec104 sec104 : entity.getLocal2016Sec104List()) {
		 * sec104.setLocal2016Cabecera(entity); }
		 * 
		 * if (entity.getLocal2016Sec200List() != null) for (Local2016Sec200 sec302 :
		 * entity.getLocal2016Sec200List()) { sec302.setLocal2016Cabecera(entity); }
		 * 
		 * if (entity.getLocal2016Sec300List() != null) for (Local2016Sec300 sec300 :
		 * entity.getLocal2016Sec300List()) { sec300.setLocal2016Cabecera(entity); }
		 * 
		 * if (entity.getLocal2016Sec304List() != null) for (Local2016Sec304 sec304 :
		 * entity.getLocal2016Sec304List()) { sec304.setLocal2016Cabecera(entity); }
		 * 
		 * if (entity.getLocal2016Sec402List() != null) for (Local2016Sec402 sec400 :
		 * entity.getLocal2016Sec402List()) { sec400.setLocal2016Cabecera(entity); }
		 * 
		 * if (entity.getLocal2016Sec500List() != null) for (Local2016Sec500 sec500 :
		 * entity.getLocal2016Sec500List()) { sec500.setLocal2016Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();
	}

	public Local2016Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2016Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2016Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
