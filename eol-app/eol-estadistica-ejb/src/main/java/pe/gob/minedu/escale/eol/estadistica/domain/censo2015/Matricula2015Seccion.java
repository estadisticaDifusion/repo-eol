/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "matricula2015_seccion")
public class Matricula2015Seccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2015Cabecera matricula2015Cabecera;
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2015Seccion")
    private List<Matricula2015SeccionFila> matricula2015SeccionFilaList;
*/
    public Matricula2015Seccion() {
    }

    public Matricula2015Seccion(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Matricula2015Cabecera getMatricula2015Cabecera() {
        return matricula2015Cabecera;
    }

    public void setMatricula2015Cabecera(Matricula2015Cabecera matricula2015Cabecera) {
        this.matricula2015Cabecera = matricula2015Cabecera;
    }
/*
    @XmlElement(name="SECCION_FILAS")
    public List<Matricula2015SeccionFila> getMatricula2015SeccionFilaList() {
        return matricula2015SeccionFilaList;
    }

    public void setMatricula2015SeccionFilaList(List<Matricula2015SeccionFila> matricula2015SeccionFilaList) {
        this.matricula2015SeccionFilaList = matricula2015SeccionFilaList;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2015Seccion)) {
            return false;
        }
        Matricula2015Seccion other = (Matricula2015Seccion) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
