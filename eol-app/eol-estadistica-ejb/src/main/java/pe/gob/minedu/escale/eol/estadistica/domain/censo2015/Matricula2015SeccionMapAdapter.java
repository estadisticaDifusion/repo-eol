package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Matricula2015SeccionMapAdapter.Matricula2015SeccionList;

public class Matricula2015SeccionMapAdapter extends XmlAdapter<Matricula2015SeccionList, Map<String, Matricula2015Seccion>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2015SeccionMapAdapter.class.getName());

    static class Matricula2015SeccionList {

        private List<Matricula2015Seccion> detalle;

        private Matricula2015SeccionList(ArrayList<Matricula2015Seccion> lista) {
            detalle = lista;
        }

        public Matricula2015SeccionList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2015Seccion> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2015Seccion> detalle) {
            this.detalle = detalle;
        }
    }


    @Override
    public Map<String, Matricula2015Seccion> unmarshal(Matricula2015SeccionList v) throws Exception {

        Map<String, Matricula2015Seccion> map = new HashMap<String, Matricula2015Seccion>();
        for (Matricula2015Seccion detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2015SeccionList marshal(Map<String, Matricula2015Seccion> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2015Seccion> lista = new ArrayList<Matricula2015Seccion>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2015Seccion $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2015SeccionList list = new Matricula2015SeccionList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }

}
