/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.ejb.censo2011;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.Local2011Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;


/**
 *
 * @author JMATAMOROS
 */
@Singleton
public class Censo2011LocalFacade extends AbstractFacade<Local2011Cabecera>{

    static final Logger LOGGER = Logger.getLogger(Censo2011LocalFacade.class.getName());
    @PersistenceContext(unitName = "eol-PU")
    private EntityManager em;

    public Censo2011LocalFacade()
    {   super(Local2011Cabecera.class);
    }

    @Override
    protected EntityManager getEntityManager() {
       return em;
    }



    @Override
    public void create(Local2011Cabecera entity) {
        String sql = "UPDATE Local2011Cabecera a SET a.ultimo=false WHERE a.cmEnvio=:cod_mod "
                + " AND a.anxEnvio=:anexo AND a.codlocal=:codlocal";
        Query query = em.createQuery(sql);        
        query.setParameter("cod_mod", entity.getCmEnvio());
        query.setParameter("anexo", entity.getAnxEnvio());
        query.setParameter("codlocal", entity.getCodlocal());
        query.executeUpdate();
        /*
        for(Local2011Sec104 sec104:entity.getLocal2011Sec104Collection())
        { sec104.setLocal2011Cabecera(entity);
        }
        if(entity.getLocal2011Sec300Collection()!=null)
        for(Local2011Sec300 sec300:entity.getLocal2011Sec300Collection())
        { sec300.setLocal2011Cabecera(entity);
        }
        if(entity.getLocal2011Sec400Collection()!=null)
        for(Local2011Sec400 sec400:entity.getLocal2011Sec400Collection())
        { sec400.setLocal2011Cabecera(entity);
        }
        */
        em.persist(entity);
        em.flush();
    }

    public Local2011Cabecera findByCodLocal(String codLocal) {
        Query q = em.createQuery("SELECT L FROM Local2011Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo= true");
        q.setParameter("codlocal", codLocal);
        try {
            return (Local2011Cabecera) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
