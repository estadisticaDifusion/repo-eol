/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "local2018_sec400")
public class Local2018Sec400 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P400_NRO")
    private Integer p400Nro;
    @Column(name = "P400_1")
    private String p4001;
    @Column(name = "P400_2M1")
    private String p4002m1;
    @Column(name = "P400_2M2")
    private String p4002m2;
    @Column(name = "P400_2M3")
    private String p4002m3;
    @Column(name = "P400_2M4")
    private Integer p4002m4;
    @Column(name = "P400_2T1")
    private String p4002t1;
    @Column(name = "P400_2T2")
    private String p4002t2;
    @Column(name = "P400_2T3")
    private String p4002t3;
    @Column(name = "P400_2T4")
    private Integer p4002t4;
    @Column(name = "P400_2N1")
    private String p4002n1;
    @Column(name = "P400_2N2")
    private String p4002n2;
    @Column(name = "P400_2N3")
    private String p4002n3;
    @Column(name = "P400_2N4")
    private Integer p4002n4;
    @Column(name = "P400_3")
    private Integer p4003;
    @Column(name = "P400_41PA")
    private String p40041pa;
    @Column(name = "P400_42PA")
    private String p40042pa;
    @Column(name = "P400_43PA")
    private String p40043pa;
    @Column(name = "P400_41TE")
    private String p40041te;
    @Column(name = "P400_42TE")
    private String p40042te;
    @Column(name = "P400_43TE")
    private String p40043te;
    @Column(name = "P400_41PI")
    private String p40041pi;
    @Column(name = "P400_42PI")
    private String p40042pi;
    @Column(name = "P400_43PI")
    private String p40043pi;
    @Column(name = "P400_51PU")
    private String p40051pu;
    @Column(name = "P400_52PU")
    private String p40052pu;
    @Column(name = "P400_53PU")
    private String p40053pu;
    @Column(name = "P400_51VE")
    private String p40051ve;
    @Column(name = "P400_52VE")
    private String p40052ve;
    @Column(name = "P400_53VE")
    private String p40053ve;
    @Column(name = "P400_61CA")
    private String p40061ca;
    @Column(name = "P400_62CA")
    private Integer p40062ca;
    @Column(name = "P400_63CA")
    private Integer p40063ca;
    @Column(name = "P400_61ME")
    private String p40061me;
    @Column(name = "P400_62ME")
    private Integer p40062me;
    @Column(name = "P400_63ME")
    private Integer p40063me;
    @Column(name = "P400_61SI")
    private String p40061si;
    @Column(name = "P400_62SI")
    private Integer p40062si;
    @Column(name = "P400_63SI")
    private Integer p40063si;
    @Column(name = "P400_61PI")
    private String p40061pi;
    @Column(name = "P400_62PI")
    private Integer p40062pi;
    @Column(name = "P400_63PI")
    private Integer p40063pi;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2018Cabecera local2018Cabecera;

    public Local2018Sec400() {
    }

    public Local2018Sec400(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P400_NRO")
    public Integer getP400Nro() {
        return p400Nro;
    }

    public void setP400Nro(Integer p400Nro) {
        this.p400Nro = p400Nro;
    }

    @XmlElement(name="P400_1")
    public String getP4001() {
        return p4001;
    }

    public void setP4001(String p4001) {
        this.p4001 = p4001;
    }

    @XmlElement(name="P400_2M1")
    public String getP4002m1() {
        return p4002m1;
    }

    public void setP4002m1(String p4002m1) {
        this.p4002m1 = p4002m1;
    }

    @XmlElement(name="P400_2M2")
    public String getP4002m2() {
        return p4002m2;
    }

    public void setP4002m2(String p4002m2) {
        this.p4002m2 = p4002m2;
    }

    @XmlElement(name="P400_2M3")
    public String getP4002m3() {
        return p4002m3;
    }

    public void setP4002m3(String p4002m3) {
        this.p4002m3 = p4002m3;
    }

    @XmlElement(name="P400_2M4")
    public Integer getP4002m4() {
        return p4002m4;
    }

    public void setP4002m4(Integer p4002m4) {
        this.p4002m4 = p4002m4;
    }

    @XmlElement(name="P400_2T1")
    public String getP4002t1() {
        return p4002t1;
    }

    public void setP4002t1(String p4002t1) {
        this.p4002t1 = p4002t1;
    }

    @XmlElement(name="P400_2T2")
    public String getP4002t2() {
        return p4002t2;
    }

    public void setP4002t2(String p4002t2) {
        this.p4002t2 = p4002t2;
    }

    @XmlElement(name="P400_2T3")
    public String getP4002t3() {
        return p4002t3;
    }

    public void setP4002t3(String p4002t3) {
        this.p4002t3 = p4002t3;
    }

    @XmlElement(name="P400_2T4")
    public Integer getP4002t4() {
        return p4002t4;
    }

    public void setP4002t4(Integer p4002t4) {
        this.p4002t4 = p4002t4;
    }

    @XmlElement(name="P400_2N1")
    public String getP4002n1() {
        return p4002n1;
    }

    public void setP4002n1(String p4002n1) {
        this.p4002n1 = p4002n1;
    }

    @XmlElement(name="P400_2N2")
    public String getP4002n2() {
        return p4002n2;
    }

    public void setP4002n2(String p4002n2) {
        this.p4002n2 = p4002n2;
    }

    @XmlElement(name="P400_2N3")
    public String getP4002n3() {
        return p4002n3;
    }

    public void setP4002n3(String p4002n3) {
        this.p4002n3 = p4002n3;
    }

    @XmlElement(name="P400_2N4")
    public Integer getP4002n4() {
        return p4002n4;
    }

    public void setP4002n4(Integer p4002n4) {
        this.p4002n4 = p4002n4;
    }

    @XmlElement(name="P400_3")
    public Integer getP4003() {
        return p4003;
    }

    public void setP4003(Integer p4003) {
        this.p4003 = p4003;
    }

    @XmlElement(name="P400_41PA")
    public String getP40041pa() {
        return p40041pa;
    }

    public void setP40041pa(String p40041pa) {
        this.p40041pa = p40041pa;
    }

    @XmlElement(name="P400_42PA")
    public String getP40042pa() {
        return p40042pa;
    }

    public void setP40042pa(String p40042pa) {
        this.p40042pa = p40042pa;
    }

    @XmlElement(name="P400_43PA")
    public String getP40043pa() {
        return p40043pa;
    }

    public void setP40043pa(String p40043pa) {
        this.p40043pa = p40043pa;
    }

    @XmlElement(name="P400_41TE")
    public String getP40041te() {
        return p40041te;
    }

    public void setP40041te(String p40041te) {
        this.p40041te = p40041te;
    }

    @XmlElement(name="P400_42TE")
    public String getP40042te() {
        return p40042te;
    }

    public void setP40042te(String p40042te) {
        this.p40042te = p40042te;
    }

    @XmlElement(name="P400_43TE")
    public String getP40043te() {
        return p40043te;
    }

    public void setP40043te(String p40043te) {
        this.p40043te = p40043te;
    }

    @XmlElement(name="P400_41PI")
    public String getP40041pi() {
        return p40041pi;
    }

    public void setP40041pi(String p40041pi) {
        this.p40041pi = p40041pi;
    }

    @XmlElement(name="P400_42PI")
    public String getP40042pi() {
        return p40042pi;
    }

    public void setP40042pi(String p40042pi) {
        this.p40042pi = p40042pi;
    }

    @XmlElement(name="P400_43PI")
    public String getP40043pi() {
        return p40043pi;
    }

    public void setP40043pi(String p40043pi) {
        this.p40043pi = p40043pi;
    }

    @XmlElement(name="P400_51PU")
    public String getP40051pu() {
        return p40051pu;
    }

    public void setP40051pu(String p40051pu) {
        this.p40051pu = p40051pu;
    }

    @XmlElement(name="P400_52PU")
    public String getP40052pu() {
        return p40052pu;
    }

    public void setP40052pu(String p40052pu) {
        this.p40052pu = p40052pu;
    }

    @XmlElement(name="P400_53PU")
    public String getP40053pu() {
        return p40053pu;
    }

    public void setP40053pu(String p40053pu) {
        this.p40053pu = p40053pu;
    }

    @XmlElement(name="P400_51VE")
    public String getP40051ve() {
        return p40051ve;
    }

    public void setP40051ve(String p40051ve) {
        this.p40051ve = p40051ve;
    }

    @XmlElement(name="P400_52VE")
    public String getP40052ve() {
        return p40052ve;
    }

    public void setP40052ve(String p40052ve) {
        this.p40052ve = p40052ve;
    }

    @XmlElement(name="P400_53VE")
    public String getP40053ve() {
        return p40053ve;
    }

    public void setP40053ve(String p40053ve) {
        this.p40053ve = p40053ve;
    }

    @XmlElement(name="P400_61CA")
    public String getP40061ca() {
        return p40061ca;
    }

    public void setP40061ca(String p40061ca) {
        this.p40061ca = p40061ca;
    }

    @XmlElement(name="P400_62CA")
    public Integer getP40062ca() {
        return p40062ca;
    }

    public void setP40062ca(Integer p40062ca) {
        this.p40062ca = p40062ca;
    }

    @XmlElement(name="P400_63CA")
    public Integer getP40063ca() {
        return p40063ca;
    }

    public void setP40063ca(Integer p40063ca) {
        this.p40063ca = p40063ca;
    }

    @XmlElement(name="P400_61ME")
    public String getP40061me() {
        return p40061me;
    }

    public void setP40061me(String p40061me) {
        this.p40061me = p40061me;
    }

    @XmlElement(name="P400_62ME")
    public Integer getP40062me() {
        return p40062me;
    }

    public void setP40062me(Integer p40062me) {
        this.p40062me = p40062me;
    }

    @XmlElement(name="P400_63ME")
    public Integer getP40063me() {
        return p40063me;
    }

    public void setP40063me(Integer p40063me) {
        this.p40063me = p40063me;
    }

    @XmlElement(name="P400_61SI")
    public String getP40061si() {
        return p40061si;
    }

    public void setP40061si(String p40061si) {
        this.p40061si = p40061si;
    }

    @XmlElement(name="P400_62SI")
    public Integer getP40062si() {
        return p40062si;
    }

    public void setP40062si(Integer p40062si) {
        this.p40062si = p40062si;
    }

    @XmlElement(name="P400_63SI")
    public Integer getP40063si() {
        return p40063si;
    }

    public void setP40063si(Integer p40063si) {
        this.p40063si = p40063si;
    }

    @XmlElement(name="P400_61PI")
    public String getP40061pi() {
        return p40061pi;
    }

    public void setP40061pi(String p40061pi) {
        this.p40061pi = p40061pi;
    }

    @XmlElement(name="P400_62PI")
    public Integer getP40062pi() {
        return p40062pi;
    }

    public void setP40062pi(Integer p40062pi) {
        this.p40062pi = p40062pi;
    }

    @XmlElement(name="P400_63PI")
    public Integer getP40063pi() {
        return p40063pi;
    }

    public void setP40063pi(Integer p40063pi) {
        this.p40063pi = p40063pi;
    }

    @XmlTransient
    public Local2018Cabecera getLocal2018Cabecera() {
        return local2018Cabecera;
    }

    public void setLocal2018Cabecera(Local2018Cabecera local2018Cabecera) {
        this.local2018Cabecera = local2018Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2018Sec400)) {
            return false;
        }
        Local2018Sec400 other = (Local2018Sec400) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Local2018Sec400[idEnvio=" + idEnvio + "]";
    }

}
