/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.ejb.censo2017;

import java.util.Date;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Censo2017Cedulad;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
*
* @author Ing. Oscar Mateo
* 
*/

@Singleton
public class CedulaDFacade extends AbstractFacade<Censo2017Cedulad> implements CedulaDLocal {

	private Logger logger = Logger.getLogger(CedulaDFacade.class);

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public CedulaDFacade() {
		super(Censo2017Cedulad.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/*
	 * Insert de cedulad
	 */
	@Override
	public void create(Censo2017Cedulad cedulad) {
		logger.info(":: CedulaDFacade.create :: Starting execution...");
		System.out.println("Las cedulas d que llegaron al facade[-->]" + cedulad);
		String sql = "UPDATE Censo2017Cedulad a SET a.ultimo=false WHERE  a.codlocal=:codLocal";
		Query query = em.createQuery(sql);
		query.setParameter("codLocal", cedulad.getCodlocal());
		cedulad.setUltimo(true);
		if (cedulad.getFechaEnvio() == null)
			cedulad.setFechaEnvio(new Date());

		em.persist(cedulad);
		em.flush();
		logger.info(":: CedulaDFacade.create :: Execution finish.");
	}
}
