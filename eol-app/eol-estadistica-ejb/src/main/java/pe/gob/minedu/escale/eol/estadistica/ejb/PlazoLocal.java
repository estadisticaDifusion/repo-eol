package pe.gob.minedu.escale.eol.estadistica.ejb;

import javax.ejb.Local;

import pe.gob.minedu.escale.eol.estadistica.domain.Plazo;

/**
 *
 * @author Ing. Oscar Mateo
 */

@Local
public interface PlazoLocal {
	Plazo find(String periodo, String proceso);

	String verificarPlazoEntrega(String anio, String tipoDocumento);
}
