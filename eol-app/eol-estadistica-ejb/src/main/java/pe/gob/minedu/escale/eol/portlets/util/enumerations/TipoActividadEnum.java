/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.util.enumerations;

/**
 *
 * @author JMATAMOROS
 */
public enum TipoActividadEnum {
    CENSO_MATRICULA(1,"CENSO-MATRICULA"),
    CENSO_LOCAL(2,"CENSO-LOCAL"),
    CEDULA_RESULTADO(3,"CENSO-RESULTADO"),
    CENSO_ID(4,"CENSO-ID");
    
    private int cod;
    private String descri;

    TipoActividadEnum(int cod,String descri)
    {   this.cod=cod;
        this.descri=descri;
    }

    /**
     * @return the cod
     */
    public int getCod() {
        return cod;
    }

    /**
     * @param cod the cod to set
     */
    public void setCod(int cod) {
        this.cod = cod;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

    /**
     * @param descri the descri to set
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }

}
