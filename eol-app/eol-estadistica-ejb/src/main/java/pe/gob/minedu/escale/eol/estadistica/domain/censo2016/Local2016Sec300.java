package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "local2016_sec300")
public class Local2016Sec300 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "CUADRO")
    private String cuadro;
    @Basic(optional = false)
    @Column(name = "NRO")
    private int nro;
    @Column(name = "P300_ESP")
    private String p300Esp;
    @Column(name = "P300_1")
    private Integer p3001;
    @Column(name = "P300_2")
    private Integer p3002;
    @Column(name = "P300_3")
    private Integer p3003;
    @Column(name = "P300_4")
    private Integer p3004;
    @Column(name = "P300_5")
    private Integer p3005;
    @Column(name = "P300_6")
    private Integer p3006;
    @Column(name = "P300_7")
    private Integer p3007;
    @Column(name = "P300_8")
    private Integer p3008;
    @Column(name = "P300_9")
    private Integer p3009;
    @Column(name = "P300_10")
    private Integer p30010;
    @Column(name = "P300_11")
    private Integer p30011;
    @Column(name = "P300_12")
    private Integer p30012;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2016Cabecera local2016Cabecera;

    public Local2016Sec300() {
    }

    public Local2016Sec300(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Local2016Sec300(Long idEnvio, String cuadro, int nro) {
        this.idEnvio = idEnvio;
        this.cuadro = cuadro;
        this.nro = nro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }
    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }
    @XmlElement(name = "NRO")
    public int getNro() {
        return nro;
    }

    public void setNro(int nro) {
        this.nro = nro;
    }
    @XmlElement(name = "P300_ESP")
    public String getP300Esp() {
        return p300Esp;
    }

    public void setP300Esp(String p300Esp) {
        this.p300Esp = p300Esp;
    }
    @XmlElement(name = "P300_1")
    public Integer getP3001() {
        return p3001;
    }

    public void setP3001(Integer p3001) {
        this.p3001 = p3001;
    }
    @XmlElement(name = "P300_2")
    public Integer getP3002() {
        return p3002;
    }

    public void setP3002(Integer p3002) {
        this.p3002 = p3002;
    }
    @XmlElement(name = "P300_3")
    public Integer getP3003() {
        return p3003;
    }

    public void setP3003(Integer p3003) {
        this.p3003 = p3003;
    }
    @XmlElement(name = "P300_4")
    public Integer getP3004() {
        return p3004;
    }

    public void setP3004(Integer p3004) {
        this.p3004 = p3004;
    }
    @XmlElement(name = "P300_5")
    public Integer getP3005() {
        return p3005;
    }

    public void setP3005(Integer p3005) {
        this.p3005 = p3005;
    }
    @XmlElement(name = "P300_6")
    public Integer getP3006() {
        return p3006;
    }

    public void setP3006(Integer p3006) {
        this.p3006 = p3006;
    }
    @XmlElement(name = "P300_7")
    public Integer getP3007() {
        return p3007;
    }

    public void setP3007(Integer p3007) {
        this.p3007 = p3007;
    }
    @XmlElement(name = "P300_8")
    public Integer getP3008() {
        return p3008;
    }

    public void setP3008(Integer p3008) {
        this.p3008 = p3008;
    }
    @XmlElement(name = "P300_9")
    public Integer getP3009() {
        return p3009;
    }

    public void setP3009(Integer p3009) {
        this.p3009 = p3009;
    }
    @XmlElement(name = "P300_10")
    public Integer getP30010() {
        return p30010;
    }

    public void setP30010(Integer p30010) {
        this.p30010 = p30010;
    }
    @XmlElement(name = "P300_11")
    public Integer getP30011() {
        return p30011;
    }

    public void setP30011(Integer p30011) {
        this.p30011 = p30011;
    }
    @XmlElement(name = "P300_12")
    public Integer getP30012() {
        return p30012;
    }

    public void setP30012(Integer p30012) {
        this.p30012 = p30012;
    }
    @XmlTransient
    public Local2016Cabecera getLocal2016Cabecera() {
        return local2016Cabecera;
    }

    public void setLocal2016Cabecera(Local2016Cabecera local2016Cabecera) {
        this.local2016Cabecera = local2016Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2016Sec300)) {
            return false;
        }
        Local2016Sec300 other = (Local2016Sec300) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2016.Local2016Sec300[idEnvio=" + idEnvio + "]";
    }

}
