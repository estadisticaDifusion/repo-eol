package pe.gob.minedu.escale.eol.estadistica.ejb.censo2018;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2018.Local2018Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class Local2018Facade extends AbstractFacade<Local2018Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2018Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Local2018Facade() {
		super(Local2018Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Local2018Cabecera entity) {
		String sql = "UPDATE Local2018Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();
		/*
		 * for (Local2018Sec114 sec104 : entity.getLocal2018Sec114List()) {
		 * sec104.setLocal2018Cabecera(entity); }
		 * 
		 * if (entity.getLocal2018Sec203List() != null) for (Local2018Sec203 sec302 :
		 * entity.getLocal2018Sec203List()) { sec302.setLocal2018Cabecera(entity); }
		 * 
		 * if (entity.getLocal2018Sec300List() != null) for (Local2018Sec300 sec300 :
		 * entity.getLocal2018Sec300List()) { sec300.setLocal2018Cabecera(entity); }
		 * 
		 * if (entity.getLocal2018Sec400List() != null) for (Local2018Sec400 sec400 :
		 * entity.getLocal2018Sec400List()) { sec400.setLocal2018Cabecera(entity); }
		 * 
		 * if (entity.getLocal2018Sec500List() != null) for (Local2018Sec500 sec500 :
		 * entity.getLocal2018Sec500List()) { sec500.setLocal2018Cabecera(entity); }
		 * 
		 * if (entity.getLocal2018Sec600List() != null) for (Local2018Sec600 sec600 :
		 * entity.getLocal2018Sec600List()) { sec600.setLocal2018Cabecera(entity); }
		 * 
		 * if (entity.getLocal2018RespList() != null) for (Local2018Resp resp :
		 * entity.getLocal2018RespList()) { resp.setLocal2018Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();
	}

	public Local2018Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2018Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2018Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void createCabecera(Local2018Cabecera entity) {
		String sql = "UPDATE Local2018Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();

		em.persist(entity);
		// em.flush();
	}
}
