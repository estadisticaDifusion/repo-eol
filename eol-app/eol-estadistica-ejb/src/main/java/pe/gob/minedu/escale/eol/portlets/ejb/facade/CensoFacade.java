/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.ejb.facade;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.converter.EolCedulaConverter;
import pe.gob.minedu.escale.eol.converter.EolIECensoConverter;
import pe.gob.minedu.escale.eol.dto.estadistica.EolEnvioIECensoDTO;
import pe.gob.minedu.escale.eol.portlets.ejb.AbstractFacade;
import pe.gob.minedu.escale.eol.portlets.ejb.CensoLocal;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.Distrito;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.EolActividad;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.EolCenso;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.Provincia;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.Region;

/**
*
* @author Ing. Oscar Mateo
* 
*/

@Singleton
public class CensoFacade extends AbstractFacade<EolCenso> implements CensoLocal {

	private Logger logger = Logger.getLogger(CensoFacade.class);

	@PersistenceContext(unitName = "eol-PU") // eol-portletPU
	private EntityManager em;

	public CensoFacade() {
		super(EolCenso.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	<T> Set<Predicate> crearCondiciones(CriteriaBuilder criteriaBuilder, Root<T> institucion, String codmod,
			String anexo, String codlocal, String codooii, String ubigeo, String estado, List<String> niveles,
			List<String> gestiones, List<String> areas, List<String> tipoices, List<String> formas,
			List<String> estados, String idActividad) {
		if (niveles != null) {
			niveles.remove("");
		}
		if (areas != null) {
			areas.remove("");
		}
		if (gestiones != null) {
			gestiones.remove("");
		}
		if (estados != null) {
			estados.remove("");
		}

		Set<Predicate> restrictions = new LinkedHashSet<Predicate>();

		if (idActividad != null && !idActividad.isEmpty()) {
			restrictions
					.add(criteriaBuilder.equal(institucion.get("eolActividad").get("id"), new Integer(idActividad)));
		}

		/**
		 * por codigo modular *
		 */
		if (codmod != null && !codmod.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("eolPadron").get("codMod"), codmod));
		}

		/**
		 * por anexo*
		 */
		if (anexo != null && !anexo.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("eolPadron").get("anexo"), anexo));
		}

		/**
		 * por codigo de local *
		 */
		if (codlocal != null && !codlocal.isEmpty()) {
			restrictions.add(criteriaBuilder.equal(institucion.get("eolLocal").get("codlocal"), codlocal));
		}

		/**
		 * por niveles *
		 */
		if (niveles != null && !niveles.isEmpty()) {
			restrictions.add(institucion.get("nivMod").in(niveles));
		}
		if (areas != null && !areas.isEmpty()) {
			restrictions.add(institucion.get("area").in(areas));
		}

		if (gestiones != null && !gestiones.isEmpty()) {
			restrictions.add(institucion.get("gestion").in(gestiones));
		}

		/*
		 * if(ubigeo!=null && !ubigeo.isEmpty()){
		 * restrictions.add(criteriaBuilder.equal(institucion.get("codgeo"),ubigeo)); }
		 */
		if (ubigeo != null && !ubigeo.isEmpty()) {
			Predicate $ubigeo = null;
			switch (ubigeo.length()) {
			case 2:
				Region region = em.find(Region.class, ubigeo);
				if (region != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito").get("provincia").get("region"), region);
				}
				break;
			case 4:
				Provincia provincia = em.find(Provincia.class, ubigeo);// new Provincia(ubigeo);
				if (provincia != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito").get("provincia"), provincia);
				}
				break;
			case 6:
				Distrito distrito = em.find(Distrito.class, ubigeo);// new Distrito(ubigeo);
				if (distrito != null) {
					$ubigeo = criteriaBuilder.equal(institucion.get("distrito"), distrito);
				}
				break;
			}
			if ($ubigeo != null) {
				restrictions.add($ubigeo);

			}
		}
		if (codooii != null && !codooii.isEmpty()) {
			restrictions.add(criteriaBuilder.like(institucion.get("codooii").as(String.class), codooii.concat("%")));

		}

		return restrictions;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<EolCenso> findPadron(String codmod, String anexo, String codlocal, String ubigeo, String codooii,
			List<String> niveles, List<String> gestiones, List<String> areas, String estado, List<String> tipoices,
			List<String> formas, List<String> estados, String idActividad) {
		logger.info(":: CensoFacade.findPadron :: Starting execution...");
		logger.info("codmod = " + codmod);
		logger.info("anexo = " + anexo);
		logger.info("codlocal = " + codlocal);
		logger.info("ubigeo = " + ubigeo);
		logger.info("codooii = " + codooii);
		logger.info("niveles = " + niveles);
		logger.info("gestiones = " + gestiones);
		logger.info("areas = " + areas);
		logger.info("estado = " + estado);
		logger.info("tipoices = " + tipoices);
		logger.info("formas = " + formas);
		logger.info("estados = " + estados);
		logger.info("idActividad = " + idActividad);

		List<EolCenso> entities = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery(EolCenso.class);
		Root<EolCenso> ieRoot = cq.from(EolCenso.class);
		cq.select(ieRoot);
		Set<Predicate> restriction = crearCondiciones(cb, ieRoot, codmod, anexo, codlocal, codooii, ubigeo, estado,
				niveles, gestiones, areas, tipoices, formas, estados, idActividad);
		if (restriction != null && !restriction.isEmpty()) {
			cq.where(cb.and(restriction.toArray(new Predicate[0])));
			entities = em.createQuery(cq).getResultList();

		} else {
			entities = em.createQuery(cq).getResultList();
		}
		logger.info(":: CensoFacade.findPadron :: Execution finish.");
		return entities;
	}

	public Long cantidadPadron(String codmod, String anexo, String codlocal, String ubigeo, String codooii,
			List<String> niveles, List<String> gestiones, List<String> areas, String estado, List<String> tipoices,
			List<String> formas, List<String> estados, String idActividad) {
		logger.info(":: CensoFacade.cantidadPadron :: Starting execution...");
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<EolCenso> institucion = criteriaQuery.from(EolCenso.class);

		criteriaQuery.select(criteriaBuilder.count(institucion));

		Set<Predicate> restrictions = crearCondiciones(criteriaBuilder, institucion, codmod, anexo, codlocal, codooii,
				ubigeo, estado, niveles, gestiones, areas, tipoices, formas, estados, idActividad);
		Long result = 0L;
		if (restrictions != null && !restrictions.isEmpty()) {
			criteriaQuery.where(criteriaBuilder.and(restrictions.toArray(new Predicate[0])));
			result = em.createQuery(criteriaQuery).getSingleResult();

		} else {
			result = em.createQuery(criteriaQuery).getSingleResult();
		}
		logger.info("result = " + result);
		logger.info(":: CensoFacade.cantidadPadron :: Execution finish.");
		return result;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<EolIECensoConverter> getConverterCenso(String codmod, String nivel, String anexo, String codlocal) {

		List<EolIECensoConverter> censoConverters = new ArrayList();
		StringBuilder str = new StringBuilder();

		if (codmod != null && !codmod.isEmpty() && nivel != null && !nivel.isEmpty() && !anexo.isEmpty()) {
			str.append("");
			return null;
		} else if (codlocal != null && !codlocal.isEmpty()) {
			return null;
		}
		return censoConverters;
	}

	@SuppressWarnings("unchecked")
	public List<EolIECensoConverter> cadCensoByCodmod(String nivel, String codmod, String anexo) {
		logger.info(":: CensoFacade.cadCensoByCodmod :: Starting execution...");
		logger.info("nivel = " + nivel);
		logger.info("codmod = " + codmod);
		logger.info("anexo = " + anexo);

		StringBuilder sBuild = new StringBuilder();
		List<EolIECensoConverter> censos = new ArrayList<EolIECensoConverter>();

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,  ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal, ");
		sBuild.append(" p.cen_edu,cod.valor AS 'nivelModalidad' ");
		sBuild.append(" FROM estadistica.eol_censo c ,padron.padron p, padron.codigos cod ");
		sBuild.append(
				"WHERE c.cod_mod=? AND c.anexo=? AND c.niv_mod=? AND c.cod_mod=p.cod_mod AND c.anexo=p.anexo AND cod.id=c.niv_mod AND c.id_actividad in (19,21,23) ");
		logger.info("query = " + sBuild.toString());

		Query q = em.createNativeQuery(sBuild.toString());
		q.setParameter(1, codmod);
		q.setParameter(2, anexo);
		q.setParameter(3, nivel);
		// System.out.println("SQL:::::::::::::::::::::::::::::"+sBuild.toString());
		List<Object[]> list = q.getResultList();
		// System.out.println("SQL:"+sBuild.toString());

		// System.out.println("CANTIDAD:"+list.size());
		for (Object[] o : list) {
			EolIECensoConverter censo = new EolIECensoConverter();
			censo.setIdActividad((Integer) o[0]);
			censo.setNroEnvio((Integer) o[1]);
			censo.setFechaEnvio((Date) o[2]);
			censo.setCodooii(o[3] != null ? o[3].toString() : "");
			censo.setSituacion(o[4] != null ? o[4].toString() : "");
			censo.setCodMod(o[5] != null ? o[5].toString() : "");
			censo.setAnexo(o[6] != null ? o[6].toString() : "");
			censo.setNivMod(o[7] != null ? o[7].toString() : "");
			censo.setCodLocal(o[8] != null ? o[8].toString() : "");
			censo.setCenEdu(o[9] != null ? o[9].toString() : "");
			censo.setNivelModalidad(o[10] != null ? o[10].toString() : "");

			censos.add(censo);
		}
		logger.info(":: CensoFacade.cadCensoByCodmod :: Execution finish.");
		return censos;
	}

	@SuppressWarnings("unchecked")
	public List<EolIECensoConverter> cadCensoCedulaSByCodmod(String nivel, String codmod, String anexo) {

		// System.out.println("CONSULTA CENSO COD_MOD:"+codmod);
		// System.out.println("CONSULTA CENSO ANEXO:"+anexo);
		// System.out.println("CONSULTA CENSO NIVEL:"+nivel);
		StringBuilder sBuild = new StringBuilder();
		List<EolIECensoConverter> censos = new ArrayList<EolIECensoConverter>();

		// System.out.println("");
		// NOTA IMPORTANTE : SE TIENE QUE REALIZAR UN CAMBIO DE CAMPOS PARA ESTA TABLA
		// DEBIDO A QUE MANEJA COMO FILTRO LOS MISMOS CAMPOS PARA EL CENSO DE MATRICULA
		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,  ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal, ");
		sBuild.append(" p.cen_edu,cod.valor AS 'nivelModalidad' ");
		sBuild.append(" FROM estadistica.eol_censo c ,padron.padron p, padron.codigos cod ");
		sBuild.append(
				"WHERE c.cod_mod=? AND c.anexo=? AND c.cod_mod=p.cod_mod AND c.anexo=p.anexo AND c.id_actividad = 27 ");
		Query q = em.createNativeQuery(sBuild.toString());
		q.setParameter(1, codmod);
		q.setParameter(2, anexo);
		// q.setParameter(3, nivel);
		// System.out.println("SQL:::::::::::::::::::::::::::::"+sBuild.toString());
		List<Object[]> list = q.getResultList();
		// System.out.println("SQL:"+sBuild.toString());

		// System.out.println("CANTIDAD:"+list.size());
		for (Object[] o : list) {
			EolIECensoConverter censo = new EolIECensoConverter();
			censo.setIdActividad((Integer) o[0]);
			censo.setNroEnvio((Integer) o[1]);
			censo.setFechaEnvio((Date) o[2]);
			censo.setCodooii(o[3] != null ? o[3].toString() : "");
			censo.setSituacion(o[4] != null ? o[4].toString() : "");
			censo.setCodMod(o[5] != null ? o[5].toString() : "");
			censo.setAnexo(o[6] != null ? o[6].toString() : "");
			censo.setNivMod(o[7] != null ? o[7].toString() : "");
			censo.setCodLocal(o[8] != null ? o[8].toString() : "");
			censo.setCenEdu(o[9] != null ? o[9].toString() : "");
			censo.setNivelModalidad(o[10] != null ? o[10].toString() : "");

			censos.add(censo);
		}
		return censos;
	}

	@SuppressWarnings("unchecked")
	public List<EolIECensoConverter> cadCensoByCodLocal(String codlocal) {
		StringBuilder sBuild = new StringBuilder();
		List<EolIECensoConverter> censos = new ArrayList<EolIECensoConverter>();

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,  ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal, ");
		sBuild.append(" p.cen_edu ");
		sBuild.append(" FROM estadistica.eol_censo c ,padron.padron p ");
		sBuild.append(" WHERE c.codlocal=? AND c.codlocal=p.codlocal  AND c.id_actividad in (20,24) ");
		Query q = em.createNativeQuery(sBuild.toString());

		q.setParameter(1, codlocal);
		List<Object[]> list = q.getResultList();

		// System.out.println("LISTA LOCAL:::::::::::::"+list.size());
		for (Object[] o : list) {
			EolIECensoConverter censo = new EolIECensoConverter();
			censo.setIdActividad((Integer) o[0]);
			censo.setNroEnvio((Integer) o[1]);
			censo.setFechaEnvio((Date) o[2]);
			censo.setCodooii(o[3] != null ? o[3].toString() : "");
			censo.setSituacion(o[4] != null ? o[4].toString() : "");
			censo.setCodMod(o[5] != null ? o[5].toString() : "");
			censo.setAnexo(o[6] != null ? o[6].toString() : "");
			censo.setNivMod(o[7] != null ? o[7].toString() : "");
			censo.setCodLocal(o[8] != null ? o[8].toString() : "");
			censo.setCenEdu(o[9] != null ? o[9].toString() : "");
			// censo.setNivelModalidad(o[10]!=null?o[10].toString():"");
			censos.add(censo);
		}
		return censos;
	}

	@SuppressWarnings("unchecked")
	public List<EolIECensoConverter> cadCedulaDCodLocal(String codlocal) {
		StringBuilder sBuild = new StringBuilder();
		List<EolIECensoConverter> censos = new ArrayList<EolIECensoConverter>();

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,  ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal, ");
		sBuild.append(" p.cen_edu ");
		sBuild.append(" FROM estadistica.eol_censo c ,padron.padron p ");
		sBuild.append(" WHERE c.codlocal=? AND c.codlocal=p.codlocal AND c.id_actividad = 28 ");
		Query q = em.createNativeQuery(sBuild.toString());

		q.setParameter(1, codlocal);
		List<Object[]> list = q.getResultList();

		// System.out.println("LISTA LOCAL:::::::::::::"+list.size());
		for (Object[] o : list) {
			EolIECensoConverter censo = new EolIECensoConverter();
			censo.setIdActividad((Integer) o[0]);
			censo.setNroEnvio((Integer) o[1]);
			censo.setFechaEnvio((Date) o[2]);
			censo.setCodooii(o[3] != null ? o[3].toString() : "");
			censo.setSituacion(o[4] != null ? o[4].toString() : "");
			censo.setCodMod(o[5] != null ? o[5].toString() : "");
			censo.setAnexo(o[6] != null ? o[6].toString() : "");
			censo.setNivMod(o[7] != null ? o[7].toString() : "");
			censo.setCodLocal(o[8] != null ? o[8].toString() : "");
			censo.setCenEdu(o[9] != null ? o[9].toString() : "");
			// censo.setNivelModalidad(o[10]!=null?o[10].toString():"");
			censos.add(censo);
		}
		return censos;
	}

	@SuppressWarnings("unchecked")
	public List<EolIECensoConverter> cadCensoByCodID(String codId) {
		StringBuilder sBuild = new StringBuilder();
		List<EolIECensoConverter> censos = new ArrayList<EolIECensoConverter>();

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,  ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal, ");
		sBuild.append(" p.cen_edu, p.codinst  ");
		sBuild.append(" FROM estadistica.eol_censo c ,padron.padron p ");
		sBuild.append(" WHERE p.codinst=? AND c.codinst=p.codinst ");
		Query q = em.createNativeQuery(sBuild.toString());

		q.setParameter(1, codId);
		List<Object[]> list = q.getResultList();

		// System.out.println("LISTA ID:::::::::::::"+list.size());
		for (Object[] o : list) {
			EolIECensoConverter censo = new EolIECensoConverter();
			censo.setIdActividad((Integer) o[0]);
			censo.setNroEnvio((Integer) o[1]);
			censo.setFechaEnvio((Date) o[2]);
			censo.setCodooii(o[3] != null ? o[3].toString() : "");
			censo.setSituacion(o[4] != null ? o[4].toString() : "");
			censo.setCodMod(o[5] != null ? o[5].toString() : "");
			censo.setAnexo(o[6] != null ? o[6].toString() : "");
			censo.setNivMod(o[7] != null ? o[7].toString() : "");
			censo.setCodLocal(o[8] != null ? o[8].toString() : "");
			censo.setCenEdu(o[9] != null ? o[9].toString() : "");
			censo.setCodId(o[10] != null ? o[10].toString() : "");
			censos.add(censo);
		}
		return censos;
	}

	@SuppressWarnings("unchecked")
	public List<EolIECensoConverter> cadCensoByCodIDNivMod(String codId, String codmod, String nivmod) {

		StringBuilder sBuild = new StringBuilder();
		List<EolIECensoConverter> censos = new ArrayList<EolIECensoConverter>();

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,   c.estado AS 'situacion', ");
		sBuild.append(" c.cod_mod,c.anexo,c.niv_mod, c.codlocal,  p.cen_edu, p.codinst   ");
		sBuild.append(" FROM estadistica.eol_censo c , padron.padron p  , ");
		sBuild.append(" estadistica.ie2017_cabecera ca, estadistica.ie2017_establecimientos es ");
		sBuild.append(" WHERE ");
		sBuild.append(" p.codinst= ? AND c.codinst=p.codinst ");
		sBuild.append(" and c.codinst = ca.cod_ied and ca.id_envio = es.cabecera_idenvio ");

		if (nivmod.equals("A1") || nivmod.equals("A2") || nivmod.equals("A3")) {
			sBuild.append(" AND es.cmod_ini = ? ");
		} else if (nivmod.equals("B0")) {
			sBuild.append(" AND es.cmod_pri = ? ");
		} else if (nivmod.equals("F0")) {
			sBuild.append(" AND es.cmod_sec = ? ");
		} else if (nivmod.equals("D1")) {
			sBuild.append(" AND es.cmod_ebai = ? ");
		} else if (nivmod.equals("D2")) {
			sBuild.append(" AND es.cmod_ebaa = ? ");
		} else if (nivmod.equals("E1") || nivmod.equals("E0")) {
			sBuild.append(" AND es.cmod_ebei = ? ");
		} else if (nivmod.equals("E2")) {
			sBuild.append(" AND es.cmod_ebep = ? ");
		} else if (nivmod.equals("L0")) {
			sBuild.append(" AND es.cmod_etp = ? ");
		} else if (nivmod.equals("T0")) {
			sBuild.append(" AND es.cmod_ist = ? ");
		} else if (nivmod.equals("K0")) {
			sBuild.append(" AND es.cmod_isp = ? ");
		} else if (nivmod.equals("M0")) {
			sBuild.append(" AND es.cmod_esfa = ? ");
		}

		Query q = em.createNativeQuery(sBuild.toString());

		q.setParameter(1, codId);
		q.setParameter(2, codmod);
		List<Object[]> list = q.getResultList();

		// System.out.println("LISTA ID:::::::::::::"+list.size());
		for (Object[] o : list) {
			EolIECensoConverter censo = new EolIECensoConverter();
			censo.setIdActividad((Integer) o[0]);
			censo.setNroEnvio((Integer) o[1]);
			censo.setFechaEnvio((Date) o[2]);
			censo.setCodooii(o[3] != null ? o[3].toString() : "");
			censo.setSituacion(o[4] != null ? o[4].toString() : "");
			censo.setCodMod(o[5] != null ? o[5].toString() : "");
			censo.setAnexo(o[6] != null ? o[6].toString() : "");
			censo.setNivMod(o[7] != null ? o[7].toString() : "");
			censo.setCodLocal(o[8] != null ? o[8].toString() : "");
			censo.setCenEdu(o[9] != null ? o[9].toString() : "");
			censo.setCodId(o[10] != null ? o[10].toString() : "");
			censos.add(censo);
		}
		return censos;
	}

	@SuppressWarnings("unchecked")
	public List<EolIECensoConverter> cadCensoTotalporCodigos(String nivmod, String codmod, String anexo,
			String codlocal, String codinst) {

		StringBuilder sBuild = new StringBuilder();
		List<EolIECensoConverter> censos = new ArrayList<EolIECensoConverter>();
/*
		String nivelid = "";
		if (nivmod.equals("A1") || nivmod.equals("A2") || nivmod.equals("A3")) {
			nivelid = " AND es.cmod_ini = '" + codmod + "' ";
		} else if (nivmod.equals("B0")) {
			nivelid = " AND es.cmod_pri = '" + codmod + "' ";
		} else if (nivmod.equals("F0")) {
			nivelid = " AND es.cmod_sec = '" + codmod + "' ";
		} else if (nivmod.equals("D1")) {
			nivelid = " AND es.cmod_ebai = '" + codmod + "' ";
		} else if (nivmod.equals("D2")) {
			nivelid = " AND es.cmod_ebaa = '" + codmod + "' ";
		} else if (nivmod.equals("E1") || nivmod.equals("E0")) {
			nivelid = " AND es.cmod_ebei = '" + codmod + "' ";
		} else if (nivmod.equals("E2")) {
			nivelid = " AND es.cmod_ebep = '" + codmod + "' ";
		} else if (nivmod.equals("L0")) {
			nivelid = " AND es.cmod_etp = '" + codmod + "' ";
		} else if (nivmod.equals("T0")) {
			nivelid = " AND es.cmod_ist = '" + codmod + "' ";
		} else if (nivmod.equals("K0")) {
			nivelid = " AND es.cmod_isp = '" + codmod + "' ";
		} else if (nivmod.equals("M0")) {
			nivelid = " AND es.cmod_esfa = '" + codmod + "' ";
		}*/

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,   ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal,  ");
		sBuild.append(" NULL AS cen_edu,c.codinst ");
		sBuild.append(" FROM estadistica.eol_censo c ");
		sBuild.append(" WHERE  ");
		sBuild.append(" c.cod_mod = '").append(codmod).append("' AND c.anexo = '").append(anexo)
				.append("' AND c.niv_mod = '").append(nivmod).append("' ");
		sBuild.append(" AND c.id_actividad IN (30,32,33,34,35,37,38,39) ");

		sBuild.append(" UNION ");

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,   ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal,  ");
		sBuild.append(" NULL AS cen_edu,c.codinst ");
		sBuild.append(" FROM estadistica.eol_censo c ");
		sBuild.append(" WHERE  ");
		sBuild.append(" c.codlocal = '").append(codlocal).append("' ");
		sBuild.append(" AND c.id_actividad IN (31,36) ");

		/*
		sBuild.append(" UNION ");

		sBuild.append(" SELECT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,   ");
		sBuild.append(" c.estado AS 'situacion',c.cod_mod,c.anexo,c.niv_mod, c.codlocal,  ");
		sBuild.append(" NULL AS cen_edu,c.codinst ");
		sBuild.append(" FROM estadistica.eol_censo c ");
		sBuild.append(" WHERE  ");
		sBuild.append(" c.cod_mod = '").append(codmod).append("' AND c.anexo = '").append(anexo).append("' ");
		sBuild.append(" AND c.id_actividad IN (27) ");

		sBuild.append(" UNION ");

		sBuild.append(
				" SELECT DISTINCT c.id_actividad,c.nro_envio,c.fecha_envio,c.codooii,   c.estado AS 'situacion',  ");
		sBuild.append(" c.cod_mod,c.anexo,c.niv_mod, c.codlocal,  NULL AS cen_edu, c.codinst    ");
		sBuild.append(
				" FROM estadistica.eol_censo c LEFT JOIN  estadistica.ie2017_cabecera ca ON c.codinst = ca.cod_ied  ");
		sBuild.append(" LEFT JOIN estadistica.ie2017_establecimientos es ON ca.id_envio = es.cabecera_idenvio ");
		sBuild.append(" WHERE  ");
		sBuild.append(" c.codinst = '").append(codinst).append("' AND (c.id_actividad= 26 ").append(nivelid)
				.append(")");*/

		Query q = em.createNativeQuery(sBuild.toString());

		List<Object[]> list = q.getResultList();

		for (Object[] o : list) {
			EolIECensoConverter censo = new EolIECensoConverter();
			censo.setIdActividad((Integer) o[0]);
			censo.setNroEnvio((Integer) o[1]);
			censo.setFechaEnvio((Date) o[2]);
			censo.setCodooii(o[3] != null ? o[3].toString() : "");
			censo.setSituacion(o[4] != null ? o[4].toString() : "");
			censo.setCodMod(o[5] != null ? o[5].toString() : "");
			censo.setAnexo(o[6] != null ? o[6].toString() : "");
			censo.setNivMod(o[7] != null ? o[7].toString() : "");
			censo.setCodLocal(o[8] != null ? o[8].toString() : "");
			censo.setCenEdu(o[9] != null ? o[9].toString() : "");
			censo.setCodId(o[10] != null ? o[10].toString() : "");
			censos.add(censo);
		}
		return censos;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public List<EolCedulaConverter> cadNivAndAnio(String nivel, String anio) {
		StringBuilder sBuild = new StringBuilder();
		List<EolCedulaConverter> cedulas = new ArrayList<EolCedulaConverter>();

		sBuild.append(" SELECT c.id, c.nombre,c.estado,c.nivel,c.VERSION,a.id,a.nombre, ");
		sBuild.append(" a.descripcion,a.fecha_inicio,a.fecha_termino,a.fecha_limite,    ");
		sBuild.append(" a.url_formato,a.url_constancia,a.url_cobertura,a.url_situacion, ");
		sBuild.append(" a.url_omisos,a.estado,a.situacion,a.nombre_cedula ");
		sBuild.append(" FROM estadistica.eol_cedula c  ");
		sBuild.append(" INNER JOIN estadistica.eol_actividad a ON c.id_actividad=a.id  ");
		sBuild.append(" INNER JOIN estadistica.eol_periodo p ON p.id=a.id_periodo ");
		sBuild.append(" LEFT JOIN padron.codigos cod ON c.nivel=cod.id AND cod.campo='NIV_MOD'  ");
		sBuild.append(" WHERE	p.anio=? AND c.nivel=? ");

		Query q = em.createNativeQuery(sBuild.toString());

		q.setParameter(0, anio);
		q.setParameter(1, nivel);
		q.setParameter(2, anio);
		List<Object[]> envios = q.getResultList();
		int pos = 1;
		int posA = -1;

		for (Object[] o : envios) {
			EolCedulaConverter cedula = new EolCedulaConverter();
			cedula.setId((Long) o[0]);
			cedula.setNombre(o[1].toString());
			cedula.setEstado((Boolean) o[2]);
			cedula.setNivel(o[3].toString());
			cedula.setVersion(o[4].toString());
			EolActividad tmpAct = new EolActividad();

			// pos=Nadiene.binarySearch(envios,tmpAct,buscarActividad);
			if (pos != -1) {
				tmpAct.setDescripcion(tmpAct.getDescripcion());
			} else {

			}

			// cedulas.add(cedula);
		}
		return cedulas;
	}

	public List<EolEnvioIECensoDTO> censoTotalListByCodigo(String nivmod, String codmod, String anexo, String codlocal,
			String codinst) {
		List<EolEnvioIECensoDTO> censoListDTO = new ArrayList<EolEnvioIECensoDTO>();

		List<EolIECensoConverter> listCenso = cadCensoTotalporCodigos(nivmod, codmod, anexo, codlocal, codinst);
		if (listCenso != null) {
			EolEnvioIECensoDTO eolIECensoDTO = null;
			for (EolIECensoConverter ieCenso : listCenso) {
				eolIECensoDTO = new EolEnvioIECensoDTO();

				eolIECensoDTO.setIdActividad(ieCenso.getIdActividad());
				eolIECensoDTO.setNroEnvio(ieCenso.getNroEnvio());
				eolIECensoDTO.setFechaEnvio(ieCenso.getFechaEnvio());
				eolIECensoDTO.setCodooii(ieCenso.getCodooii());
				eolIECensoDTO.setSituacion(ieCenso.getSituacion());
				eolIECensoDTO.setCodMod(ieCenso.getCodMod());
				eolIECensoDTO.setAnexo(ieCenso.getAnexo());
				eolIECensoDTO.setNivMod(ieCenso.getNivMod());
				eolIECensoDTO.setCodLocal(ieCenso.getCodLocal());
				eolIECensoDTO.setCenEdu(ieCenso.getCenEdu());
				eolIECensoDTO.setCodId(ieCenso.getCodId());

				censoListDTO.add(eolIECensoDTO);
			}
		}

		return censoListDTO;
	}

	static Comparator<EolIECensoConverter> buscarActividad = new Comparator<EolIECensoConverter>() {
		public int compare(EolIECensoConverter o1, EolIECensoConverter o2) {
			return (o1.getIdActividad()).compareTo(o2.getIdActividad());
		}
	};

	static Comparator<EolActividad> buscarActividad1 = new Comparator<EolActividad>() {
		public int compare(EolActividad o1, EolActividad o2) {
			return (o1.getId()).compareTo(o2.getId());
		}
	};
}
