/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.ejb.censo2012;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Local2012Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

/**
 *
 * @author JMATAMOROS
 */
@Singleton
public class Local2012Facade extends AbstractFacade<Local2012Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2012Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Local2012Facade() {
		super(Local2012Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Local2012Cabecera entity) {
		String sql = "UPDATE Local2012Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();
		/*
		 * for (Local2012Sec104 sec104 : entity.getSec104()) {
		 * sec104.setLocal2012Cabecera(entity); } if (entity.getSec200() != null) for
		 * (Local2012Sec200 sec200 : entity.getSec200()) {
		 * sec200.setLocal2012Cabecera(entity); }
		 * 
		 * if (entity.getSec300() != null) for (Local2012Sec300 sec300 :
		 * entity.getSec300()) { sec300.setLocal2012Cabecera(entity); }
		 * 
		 * if (entity.getSec500() != null) for (Local2012Sec500 sec500 :
		 * entity.getSec500()) { sec500.setLocal2012Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();
	}

	public Local2012Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2012Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2012Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
