/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Resultado2012DetalleMapAdapter.Resultado2012DetalleList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Resultado2012DetalleMapAdapter extends XmlAdapter<Resultado2012DetalleList, Map<String, Resultado2012Detalle>> {

    private static final Logger LOGGER = Logger.getLogger(Resultado2012DetalleMapAdapter.class.getName());

    static class Resultado2012DetalleList {

        private List<Resultado2012Detalle> detalle;

        private Resultado2012DetalleList(ArrayList<Resultado2012Detalle> lista) {
            detalle = lista;
        }

        public Resultado2012DetalleList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Resultado2012Detalle> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Resultado2012Detalle> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Resultado2012Detalle> unmarshal(Resultado2012DetalleList v) throws Exception {

        Map<String, Resultado2012Detalle> map = new HashMap<String, Resultado2012Detalle>();
        for (Resultado2012Detalle detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Resultado2012DetalleList marshal(Map<String, Resultado2012Detalle> v) throws Exception {
        if(v==null)
        {   LOGGER.fine("no hay detalles");
            return null;            
        }
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Resultado2012Detalle> lista = new ArrayList<Resultado2012Detalle>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Resultado2012Detalle $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Resultado2012DetalleList list = new Resultado2012DetalleList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
