package pe.gob.minedu.escale.eol.estadistica.ejb.censo2015;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Ie2015Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class CedulaID2015Facade extends AbstractFacade<Ie2015Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2015Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public CedulaID2015Facade() {
		super(Ie2015Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Ie2015Cabecera entity) {

		String sql = "UPDATE Ie2015Cabecera a SET a.ultimo=false WHERE  a.codIed=:codied";
		Query query = em.createQuery(sql);
		query.setParameter("codied", entity.getCodIed());
		query.executeUpdate();
		/*
		 * OM if (entity.getIe2015EstablecimientosList() != null) for
		 * (Ie2015Establecimientos est : entity.getIe2015EstablecimientosList()) {
		 * est.setIe2015Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();

	}

	public int findByCedulaID(String codIed) {
		Query q = em.createQuery(
				"SELECT COUNT(ie.idEnvio) FROM Ie2015Cabecera ie WHERE ie.codIed=:codIed AND ie.ultimo=true");
		q.setParameter("codIed", codIed);
		try {
			return Integer.parseInt(q.getSingleResult().toString());
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public Long findByCedulaIDYNivel(String codIed, String query) {

		Long idenvio = 0L;
		Query q = em.createNativeQuery(query);
		q.setParameter("1", codIed);
		try {

			List<Long> lres = q.getResultList();
			if (lres.size() > 0) {
				idenvio = lres.get(0);
			}

			return idenvio;

		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}
}
