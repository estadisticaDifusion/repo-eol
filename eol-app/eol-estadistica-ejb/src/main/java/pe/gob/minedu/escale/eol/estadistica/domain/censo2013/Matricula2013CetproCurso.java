/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2013_cetpro_curso")
public class Matricula2013CetproCurso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NRO")
    private String nro;
    @Column(name = "CODFAM")
    private String codfam;    
    @Basic(optional = false)
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "NRO_RES")
    private String nrores;
    @Column(name = "FECHA_RES")
    private String fechares;
    @Column(name = "SITUA_CARR")
    private String situacarr;    
    @Column(name = "R1")
    private String r1;
    @Column(name = "R2")
    private String r2;    
    @Column(name = "DS")
    private Integer ds;
    @Column(name = "NHL")
    private Integer nhl;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2013Cabecera matricula2013Cabecera;

    public Matricula2013CetproCurso() {
    }

    public Matricula2013CetproCurso(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2013CetproCurso(Long idEnvio, String nro, String tipdato) {
        this.idEnvio = idEnvio;
        this.nro = nro;
        this.tipdato = tipdato;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }
    
    @XmlElement(name="CODFAM")
    public String getCodfam() {
        return codfam;
    }

    public void setCodfam(String codfam) {
        this.codfam = codfam;
    }

    @XmlElement(name="TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }
    
    @XmlElement(name="DESCRIP")
    public String getDescrip() {
        return descrip;
    }
    
    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }
    
    @XmlElement(name="NRO_RES")
    public String getNrores() {
        return nrores;
    }

    public void setNrores(String nrores) {
        this.nrores = nrores;
    }

    @XmlElement(name="FECHA_RES")
    public String getFechares() {
        return fechares;
    }

    public void setFechares(String fechares) {
        this.fechares = fechares;
    }
    
    @XmlElement(name="SITUA_CARR")
    public String getSituacarr() {
        return situacarr;
    }

    public void setSituacarr(String situacarr) {
        this.situacarr = situacarr;
    }

    @XmlElement(name="R1")
    public String getR1() {
        return r1;
    }

    public void setR1(String r1) {
        this.r1 = r1;
    }

    @XmlElement(name="R2")
    public String getR2() {
        return r2;
    }

    public void setR2(String r2) {
        this.r2 = r2;
    }
    
    @XmlElement(name="DS")
    public Integer getDs() {
        return ds;
    }

    public void setDs(Integer ds) {
        this.ds = ds;
    }

    @XmlElement(name="NHL")
    public Integer getNhl() {
        return nhl;
    }

    public void setNhl(Integer nhl) {
        this.nhl = nhl;
    }

    @XmlTransient
    public Matricula2013Cabecera getMatricula2013Cabecera() {
        return matricula2013Cabecera;
    }

    public void setMatricula2013Cabecera(Matricula2013Cabecera matricula2013Cabecera) {
        this.matricula2013Cabecera = matricula2013Cabecera;
    }

}
