package pe.gob.minedu.escale.eol.portlets.ejb.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "padron", name = "regiones")
public class Region implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_region")
	private String idRegion;

	@Column(name = "region")
	private String nombreRegion;

	@Column(name = "point_x")
	private double pointX;

	@Column(name = "point_y")
	private double pointY;

	@Column(name = "zoom")
	private int zoom;

	public Region() {
	}

	public Region(String idRegion) {
		this.idRegion = idRegion;
	}

	public String getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	public String getNombreRegion() {
		return nombreRegion;
	}

	public void setNombreRegion(String nombreRegion) {
		this.nombreRegion = nombreRegion;
	}

	public double getPointX() {
		return pointX;
	}

	public void setPointX(double pointX) {
		this.pointX = pointX;
	}

	public double getPointY() {
		return pointY;
	}

	public void setPointY(double pointY) {
		this.pointY = pointY;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = zoom;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Region other = (Region) obj;
		if ((this.idRegion == null) ? (other.idRegion != null) : !this.idRegion.equals(other.idRegion)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 97 * hash + (this.idRegion != null ? this.idRegion.hashCode() : 0);
		return hash;
	}

}
