
package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2015_cabecera")
public class Matricula2015Cabecera implements Serializable {
	private static final long serialVersionUID = 1L;
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03A = "c03a";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08A = "c08a";
	public final static String CEDULA_09A = "c09a";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_ENVIO")
	private Long idEnvio;
	@Basic(optional = false)
	@Column(name = "NROCED")
	private String nroced;
	@Basic(optional = false)
	@Column(name = "COD_MOD")
	private String codMod;
	@Basic(optional = false)
	@Column(name = "ANEXO")
	private String anexo;
	@Basic(optional = false)
	@Column(name = "CODLOCAL")
	private String codlocal;
	@Basic(optional = false)
	@Column(name = "CEN_EDU")
	private String cenEdu;
	@Basic(optional = false)
	@Column(name = "NIV_MOD")
	private String nivMod;
	@Column(name = "GESTION")
	private String gestion;
	@Column(name = "FORMATEN")
	private String formaten;
	@Column(name = "TIPOPROG")
	private String tipoprog;
	@Column(name = "ID_UBIGEO")
	private String idUbigeo;
	@Column(name = "DISTRITO")
	private String distrito;
	@Column(name = "LOCALIDAD")
	private String localidad;
	@Column(name = "CODUGEL")
	private String codugel;
	@Column(name = "MODATEN")
	private String modaten;
	@Column(name = "TIPONEE")
	private String tiponee;
	@Column(name = "CICLO_C4_1")
	private String cicloC41;
	@Column(name = "CICLO_C4_2")
	private String cicloC42;
	@Column(name = "CICLO_C4_3")
	private String cicloC43;
	@Column(name = "DNI_CORD")
	private String dniCord;
	@Column(name = "TIPOIESUP")
	private String tipoiesup;
	@Column(name = "EGESTORA")
	private String egestora;
	@Column(name = "TIPODFINA_1")
	private String tipodfina1;
	@Column(name = "TIPODFINA_2")
	private String tipodfina2;
	@Column(name = "TIPODFINA_3")
	private String tipodfina3;
	@Column(name = "TIPODFINA_4")
	private String tipodfina4;
	@Column(name = "TIPODFINA_5")
	private String tipodfina5;
	@Column(name = "NUMRES")
	private String numres;
	@Column(name = "FECHARES_DD")
	private String fecharesDd;
	@Column(name = "FECHARES_MM")
	private String fecharesMm;
	@Column(name = "FECHARES_AA")
	private String fecharesAa;
	@Column(name = "P101_DD")
	private String p101Dd;
	@Column(name = "P101_MM")
	private String p101Mm;
	@Column(name = "P101_C5")
	private String p101C5;
	@Column(name = "P101_C5_N")
	private String p101C5N;
	@Column(name = "P102_DD")
	private String p102Dd;
	@Column(name = "P102_MM")
	private String p102Mm;
	@Column(name = "P102_C8")
	private String p102C8;
	@Column(name = "P103_123")
	private String p103123;
	@Column(name = "P103_C4")
	private String p103C4;
	@Column(name = "P104_C4")
	private Short p104C4;
	@Column(name = "P105_C4")
	private Short p105C4;
	@Column(name = "P106_C4")
	private String p106C4;
	@Column(name = "P107_C4")
	private String p107C4;
	@Column(name = "P108_C4")
	private String p108C4;
	@Column(name = "SERV_IE_1")
	private String servIe1;
	@Column(name = "SERV_IE_2")
	private String servIe2;
	@Column(name = "SERV_IE_3")
	private String servIe3;
	@Column(name = "SERV_IE_4")
	private String servIe4;
	@Column(name = "SERV_IE_5")
	private String servIe5;
	@Column(name = "SERV_IE_6")
	private String servIe6;
	@Column(name = "SERV_IE_7")
	private String servIe7;
	@Column(name = "P105_106")
	private String p105106;
	@Column(name = "P105_106_ESP")
	private String p105106Esp;
	@Column(name = "P105_C3")
	private String p105C3;
	@Column(name = "P105_C3_1")
	private String p105C31;
	@Column(name = "P105_C3_1RD")
	private String p105C31rd;
	@Column(name = "P105_C3_2")
	private String p105C32;
	@Column(name = "P105_C3_2RD")
	private String p105C32rd;
	@Column(name = "P106_107")
	private String p106107;
	@Column(name = "P105_107ESP")
	private String p105107esp;
	@Column(name = "PEIB_1")
	private String peib1;
	@Column(name = "PEIB_1_ESP")
	private String peib1Esp;
	@Column(name = "PEIB_2")
	private String peib2;
	@Column(name = "PEIB_3")
	private String peib3;
	@Column(name = "PEIB_3_ESP")
	private String peib3Esp;
	@Column(name = "PEIB_4")
	private String peib4;
	@Column(name = "PEIB_4_ESP")
	private String peib4Esp;
	@Column(name = "P1084_C3_1")
	private String p1084C31;
	@Column(name = "P1084_C3_2")
	private String p1084C32;
	@Column(name = "P1084_C3_3")
	private String p1084C33;
	@Column(name = "P1084_C3_4")
	private String p1084C34;
	@Column(name = "P1084_C3_5")
	private String p1084C35;
	@Column(name = "P1084_C3_6")
	private String p1084C36;
	@Column(name = "PEIB_5")
	private String peib5;
	@Column(name = "PEIB_6")
	private String peib6;
	@Column(name = "PEIB_6_RES")
	private String peib6Res;
	@Column(name = "PEIB_7")
	private String peib7;
	@Column(name = "PEIB_7_NRO")
	private Integer peib7Nro;
	@Column(name = "P401_123")
	private String p401123;
	@Column(name = "P402_D123")
	private String p402D123;
	@Column(name = "P402_M123")
	private String p402M123;
	@Column(name = "P402_A123")
	private String p402A123;
	@Column(name = "P404_123")
	private String p404123;
	@Column(name = "P405_D123")
	private String p405D123;
	@Column(name = "P405_M123")
	private String p405M123;
	@Column(name = "P405_A123")
	private String p405A123;
	@Column(name = "P406_C4")
	private String p406C4;
	@Column(name = "P407_C4_1")
	private String p407C41;
	@Column(name = "P407_C4_2")
	private String p407C42;
	@Column(name = "P407_C4_3")
	private String p407C43;
	@Column(name = "P407_C4_4")
	private String p407C44;
	@Column(name = "P407_C4_5")
	private String p407C45;
	@Column(name = "P407_C4_6")
	private String p407C46;
	@Column(name = "P407_C4_7")
	private String p407C47;
	@Column(name = "P407_C4_8")
	private String p407C48;
	@Column(name = "P407_C4_9")
	private String p407C49;
	@Column(name = "P407_C4_9ESP")
	private String p407C49esp;
	@Column(name = "P407_C4_10")
	private String p407C410;
	@Column(name = "P407_C4_10ESP")
	private String p407C410esp;
	@Column(name = "P407_C4_N")
	private String p407C4N;
	@Column(name = "P408_C3")
	private String p408C3;
	@Column(name = "P409_C3_D")
	private String p409C3D;
	@Column(name = "P409_C3_M")
	private String p409C3M;
	@Column(name = "P409_C3_A")
	private String p409C3A;
	@Column(name = "BBTK_AULA_0")
	private String bbtkAula0;
	@Column(name = "BBTK_AULA_1")
	private String bbtkAula1;
	@Column(name = "BBTK_AULA_2")
	private String bbtkAula2;
	@Column(name = "BBTK_AULA_3")
	private String bbtkAula3;
	@Column(name = "BBTK_AULA_4")
	private String bbtkAula4;
	@Column(name = "BBTK_AULA_5")
	private String bbtkAula5;
	@Column(name = "BBTK_AULA_N")
	private String bbtkAulaN;
	@Column(name = "P502_C1")
	private String p502C1;
	@Column(name = "P502_C1_D")
	private String p502C1D;
	@Column(name = "P502_C1_M")
	private String p502C1M;
	@Column(name = "P502_C1_A")
	private String p502C1A;
	@Column(name = "P502_C34")
	private String p502C34;
	@Column(name = "P508_C3_1")
	private String p508C31;
	@Column(name = "P508_C3_2")
	private String p508C32;
	@Column(name = "P508_C3_3")
	private String p508C33;
	@Column(name = "P508_C3_4")
	private String p508C34;
	@Column(name = "P508_C3_5")
	private String p508C35;
	@Column(name = "P508_C3_6")
	private String p508C36;
	@Column(name = "P508_C3_6ESP")
	private String p508C36esp;
	@Column(name = "ANOTACIONES")
	private String anotaciones;
	@Column(name = "FUENTE")
	private String fuente;
	@Column(name = "FECHA_ENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;
	@Column(name = "VERSION")
	private String version;
	@Column(name = "TIPO_ENVIO")
	private String tipoEnvio;
	@Column(name = "ULTIMO")
	private Boolean ultimo;
	@Column(name = "TDOCENTES")
	private Integer tdocentes;
	@Column(name = "TADMINIST")
	private Integer tadminist;
	@Column(name = "TAUXILIAR")
	private Integer tauxiliar;
	@Column(name = "FORMA_PR")
	private String formaPr;
	@Column(name = "FORMA_SP")
	private String formaSp;
	@Column(name = "FORMA_AD")
	private String formaAd;
	/*
	 * @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2015Cabecera",
	 * fetch = FetchType.EAGER) private List<Matricula2015Perifericos>
	 * matricula2015PerifericosList;
	 * 
	 * @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2015Cabecera",
	 * fetch = FetchType.EAGER) private List<Matricula2015Localpronoei>
	 * matricula2015LocalpronoeiList;
	 * 
	 * @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2015Cabecera",
	 * fetch = FetchType.EAGER) private List<Matricula2015Personal>
	 * matricula2015PersonalList;
	 * 
	 * @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2015Cabecera",
	 * fetch = FetchType.EAGER) private List<Matricula2015Saanee>
	 * matricula2015SaaneeList;
	 * 
	 * @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2015Cabecera",
	 * fetch = FetchType.EAGER) private List<Matricula2015Carreras>
	 * matricula2015CarrerasList;
	 * 
	 * @MapKeyColumn(name = "cuadro", length = 5)
	 * 
	 * @OneToMany(mappedBy = "matricula2015Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private Map<String,Matricula2015Matricula>
	 * detalleMatricula;
	 * 
	 * @MapKeyColumn(name = "cuadro", length = 5)
	 * 
	 * @OneToMany(mappedBy = "matricula2015Cabecera",cascade = CascadeType.ALL,
	 * fetch=FetchType.EAGER) private Map<String,Matricula2015Seccion>
	 * detalleSeccion;
	 * 
	 * @MapKeyColumn(name = "cuadro", length = 5)
	 * 
	 * @OneToMany(mappedBy = "matricula2015Cabecera", cascade = CascadeType.ALL,
	 * fetch = FetchType.EAGER) private Map<String,Matricula2015Recursos>
	 * detalleRecursos;
	 */
	@Transient
	private long token;

	@Transient
	private String msg;

	@Transient
	private String estadoRpt;

	public Matricula2015Cabecera() {
	}

	public Matricula2015Cabecera(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public Matricula2015Cabecera(Long idEnvio, String nroced, String codMod, String anexo, String codlocal,
			String cenEdu, String nivMod) {
		this.idEnvio = idEnvio;
		this.nroced = nroced;
		this.codMod = codMod;
		this.anexo = anexo;
		this.codlocal = codlocal;
		this.cenEdu = cenEdu;
		this.nivMod = nivMod;
	}

	@XmlAttribute
	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@XmlElement(name = "NROCED")
	public String getNroced() {
		return nroced;
	}

	public void setNroced(String nroced) {
		this.nroced = nroced;
	}

	@XmlElement(name = "COD_MOD")
	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	@XmlElement(name = "ANEXO")
	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	@XmlElement(name = "CODLOCAL")
	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	@XmlElement(name = "CEN_EDU")
	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	@XmlElement(name = "NIV_MOD")
	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	@XmlElement(name = "GESTION")
	public String getGestion() {
		return gestion;
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	@XmlElement(name = "FORMATEN")
	public String getFormaten() {
		return formaten;
	}

	public void setFormaten(String formaten) {
		this.formaten = formaten;
	}

	@XmlElement(name = "TIPOPROG")
	public String getTipoprog() {
		return tipoprog;
	}

	public void setTipoprog(String tipoprog) {
		this.tipoprog = tipoprog;
	}

	@XmlElement(name = "ID_UBIGEO")
	public String getIdUbigeo() {
		return idUbigeo;
	}

	public void setIdUbigeo(String idUbigeo) {
		this.idUbigeo = idUbigeo;
	}

	@XmlElement(name = "DISTRITO")
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@XmlElement(name = "LOCALIDAD")
	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	@XmlElement(name = "CODUGEL")
	public String getCodugel() {
		return codugel;
	}

	public void setCodugel(String codugel) {
		this.codugel = codugel;
	}

	@XmlElement(name = "MODATEN")
	public String getModaten() {
		return modaten;
	}

	public void setModaten(String modaten) {
		this.modaten = modaten;
	}

	@XmlElement(name = "TIPONEE")
	public String getTiponee() {
		return tiponee;
	}

	public void setTiponee(String tiponee) {
		this.tiponee = tiponee;
	}

	@XmlElement(name = "CICLO_C4_1")
	public String getCicloC41() {
		return cicloC41;
	}

	public void setCicloC41(String cicloC41) {
		this.cicloC41 = cicloC41;
	}

	@XmlElement(name = "CICLO_C4_2")
	public String getCicloC42() {
		return cicloC42;
	}

	public void setCicloC42(String cicloC42) {
		this.cicloC42 = cicloC42;
	}

	@XmlElement(name = "CICLO_C4_3")
	public String getCicloC43() {
		return cicloC43;
	}

	public void setCicloC43(String cicloC43) {
		this.cicloC43 = cicloC43;
	}

	@XmlElement(name = "DNI_CORD")
	public String getDniCord() {
		return dniCord;
	}

	public void setDniCord(String dniCord) {
		this.dniCord = dniCord;
	}

	@XmlElement(name = "TIPOIESUP")
	public String getTipoiesup() {
		return tipoiesup;
	}

	public void setTipoiesup(String tipoiesup) {
		this.tipoiesup = tipoiesup;
	}

	@XmlElement(name = "EGESTORA")
	public String getEgestora() {
		return egestora;
	}

	public void setEgestora(String egestora) {
		this.egestora = egestora;
	}

	@XmlElement(name = "TIPODFINA_1")
	public String getTipodfina1() {
		return tipodfina1;
	}

	public void setTipodfina1(String tipodfina1) {
		this.tipodfina1 = tipodfina1;
	}

	@XmlElement(name = "TIPODFINA_2")
	public String getTipodfina2() {
		return tipodfina2;
	}

	public void setTipodfina2(String tipodfina2) {
		this.tipodfina2 = tipodfina2;
	}

	@XmlElement(name = "TIPODFINA_3")
	public String getTipodfina3() {
		return tipodfina3;
	}

	public void setTipodfina3(String tipodfina3) {
		this.tipodfina3 = tipodfina3;
	}

	@XmlElement(name = "TIPODFINA_4")
	public String getTipodfina4() {
		return tipodfina4;
	}

	public void setTipodfina4(String tipodfina4) {
		this.tipodfina4 = tipodfina4;
	}

	@XmlElement(name = "TIPODFINA_5")
	public String getTipodfina5() {
		return tipodfina5;
	}

	public void setTipodfina5(String tipodfina5) {
		this.tipodfina5 = tipodfina5;
	}

	@XmlElement(name = "NUMRES")
	public String getNumres() {
		return numres;
	}

	public void setNumres(String numres) {
		this.numres = numres;
	}

	@XmlElement(name = "FECHARES_DD")
	public String getFecharesDd() {
		return fecharesDd;
	}

	public void setFecharesDd(String fecharesDd) {
		this.fecharesDd = fecharesDd;
	}

	@XmlElement(name = "FECHARES_MM")
	public String getFecharesMm() {
		return fecharesMm;
	}

	public void setFecharesMm(String fecharesMm) {
		this.fecharesMm = fecharesMm;
	}

	@XmlElement(name = "FECHARES_AA")
	public String getFecharesAa() {
		return fecharesAa;
	}

	public void setFecharesAa(String fecharesAa) {
		this.fecharesAa = fecharesAa;
	}

	@XmlElement(name = "P101_DD")
	public String getP101Dd() {
		return p101Dd;
	}

	public void setP101Dd(String p101Dd) {
		this.p101Dd = p101Dd;
	}

	@XmlElement(name = "P101_MM")
	public String getP101Mm() {
		return p101Mm;
	}

	public void setP101Mm(String p101Mm) {
		this.p101Mm = p101Mm;
	}

	@XmlElement(name = "P101_C5")
	public String getP101C5() {
		return p101C5;
	}

	public void setP101C5(String p101C5) {
		this.p101C5 = p101C5;
	}

	@XmlElement(name = "P101_C5_N")
	public String getP101C5N() {
		return p101C5N;
	}

	public void setP101C5N(String p101C5N) {
		this.p101C5N = p101C5N;
	}

	@XmlElement(name = "P102_DD")
	public String getP102Dd() {
		return p102Dd;
	}

	public void setP102Dd(String p102Dd) {
		this.p102Dd = p102Dd;
	}

	@XmlElement(name = "P102_MM")
	public String getP102Mm() {
		return p102Mm;
	}

	public void setP102Mm(String p102Mm) {
		this.p102Mm = p102Mm;
	}

	@XmlElement(name = "P102_C8")
	public String getP102C8() {
		return p102C8;
	}

	public void setP102C8(String p102C8) {
		this.p102C8 = p102C8;
	}

	@XmlElement(name = "P103_123")
	public String getP103123() {
		return p103123;
	}

	public void setP103123(String p103123) {
		this.p103123 = p103123;
	}

	@XmlElement(name = "P103_C4")
	public String getP103C4() {
		return p103C4;
	}

	public void setP103C4(String p103C4) {
		this.p103C4 = p103C4;
	}

	@XmlElement(name = "P104_C4")
	public Short getP104C4() {
		return p104C4;
	}

	public void setP104C4(Short p104C4) {
		this.p104C4 = p104C4;
	}

	@XmlElement(name = "P105_C4")
	public Short getP105C4() {
		return p105C4;
	}

	public void setP105C4(Short p105C4) {
		this.p105C4 = p105C4;
	}

	@XmlElement(name = "P106_C4")
	public String getP106C4() {
		return p106C4;
	}

	public void setP106C4(String p106C4) {
		this.p106C4 = p106C4;
	}

	@XmlElement(name = "P107_C4")
	public String getP107C4() {
		return p107C4;
	}

	public void setP107C4(String p107C4) {
		this.p107C4 = p107C4;
	}

	@XmlElement(name = "P108_C4")
	public String getP108C4() {
		return p108C4;
	}

	public void setP108C4(String p108C4) {
		this.p108C4 = p108C4;
	}

	@XmlElement(name = "SERV_IE_1")
	public String getServIe1() {
		return servIe1;
	}

	public void setServIe1(String servIe1) {
		this.servIe1 = servIe1;
	}

	@XmlElement(name = "SERV_IE_2")
	public String getServIe2() {
		return servIe2;
	}

	public void setServIe2(String servIe2) {
		this.servIe2 = servIe2;
	}

	@XmlElement(name = "SERV_IE_3")
	public String getServIe3() {
		return servIe3;
	}

	public void setServIe3(String servIe3) {
		this.servIe3 = servIe3;
	}

	@XmlElement(name = "SERV_IE_4")
	public String getServIe4() {
		return servIe4;
	}

	public void setServIe4(String servIe4) {
		this.servIe4 = servIe4;
	}

	@XmlElement(name = "SERV_IE_5")
	public String getServIe5() {
		return servIe5;
	}

	public void setServIe5(String servIe5) {
		this.servIe5 = servIe5;
	}

	@XmlElement(name = "SERV_IE_6")
	public String getServIe6() {
		return servIe6;
	}

	public void setServIe6(String servIe6) {
		this.servIe6 = servIe6;
	}

	@XmlElement(name = "SERV_IE_7")
	public String getServIe7() {
		return servIe7;
	}

	public void setServIe7(String servIe7) {
		this.servIe7 = servIe7;
	}

	@XmlElement(name = "P105_106")
	public String getP105106() {
		return p105106;
	}

	public void setP105106(String p105106) {
		this.p105106 = p105106;
	}

	@XmlElement(name = "P105_106_ESP")
	public String getP105106Esp() {
		return p105106Esp;
	}

	public void setP105106Esp(String p105106Esp) {
		this.p105106Esp = p105106Esp;
	}

	@XmlElement(name = "P105_C3")
	public String getP105C3() {
		return p105C3;
	}

	public void setP105C3(String p105C3) {
		this.p105C3 = p105C3;
	}

	@XmlElement(name = "P105_C3_1")
	public String getP105C31() {
		return p105C31;
	}

	public void setP105C31(String p105C31) {
		this.p105C31 = p105C31;
	}

	@XmlElement(name = "P105_C3_1RD")
	public String getP105C31rd() {
		return p105C31rd;
	}

	public void setP105C31rd(String p105C31rd) {
		this.p105C31rd = p105C31rd;
	}

	@XmlElement(name = "P105_C3_2")
	public String getP105C32() {
		return p105C32;
	}

	public void setP105C32(String p105C32) {
		this.p105C32 = p105C32;
	}

	@XmlElement(name = "P105_C3_2RD")
	public String getP105C32rd() {
		return p105C32rd;
	}

	public void setP105C32rd(String p105C32rd) {
		this.p105C32rd = p105C32rd;
	}

	@XmlElement(name = "P106_107")
	public String getP106107() {
		return p106107;
	}

	public void setP106107(String p106107) {
		this.p106107 = p106107;
	}

	@XmlElement(name = "P105_107ESP")
	public String getP105107esp() {
		return p105107esp;
	}

	public void setP105107esp(String p105107esp) {
		this.p105107esp = p105107esp;
	}

	@XmlElement(name = "PEIB_1")
	public String getPeib1() {
		return peib1;
	}

	public void setPeib1(String peib1) {
		this.peib1 = peib1;
	}

	@XmlElement(name = "PEIB_1_ESP")
	public String getPeib1Esp() {
		return peib1Esp;
	}

	public void setPeib1Esp(String peib1Esp) {
		this.peib1Esp = peib1Esp;
	}

	@XmlElement(name = "PEIB_2")
	public String getPeib2() {
		return peib2;
	}

	public void setPeib2(String peib2) {
		this.peib2 = peib2;
	}

	@XmlElement(name = "PEIB_3")
	public String getPeib3() {
		return peib3;
	}

	public void setPeib3(String peib3) {
		this.peib3 = peib3;
	}

	@XmlElement(name = "PEIB_3_ESP")
	public String getPeib3Esp() {
		return peib3Esp;
	}

	public void setPeib3Esp(String peib3Esp) {
		this.peib3Esp = peib3Esp;
	}

	@XmlElement(name = "PEIB_4")
	public String getPeib4() {
		return peib4;
	}

	public void setPeib4(String peib4) {
		this.peib4 = peib4;
	}

	@XmlElement(name = "PEIB_4_ESP")
	public String getPeib4Esp() {
		return peib4Esp;
	}

	public void setPeib4Esp(String peib4Esp) {
		this.peib4Esp = peib4Esp;
	}

	@XmlElement(name = "P1084_C3_1")
	public String getP1084C31() {
		return p1084C31;
	}

	public void setP1084C31(String p1084C31) {
		this.p1084C31 = p1084C31;
	}

	@XmlElement(name = "P1084_C3_2")
	public String getP1084C32() {
		return p1084C32;
	}

	public void setP1084C32(String p1084C32) {
		this.p1084C32 = p1084C32;
	}

	@XmlElement(name = "P1084_C3_3")
	public String getP1084C33() {
		return p1084C33;
	}

	public void setP1084C33(String p1084C33) {
		this.p1084C33 = p1084C33;
	}

	@XmlElement(name = "P1084_C3_4")
	public String getP1084C34() {
		return p1084C34;
	}

	public void setP1084C34(String p1084C34) {
		this.p1084C34 = p1084C34;
	}

	@XmlElement(name = "P1084_C3_5")
	public String getP1084C35() {
		return p1084C35;
	}

	public void setP1084C35(String p1084C35) {
		this.p1084C35 = p1084C35;
	}

	@XmlElement(name = "P1084_C3_6")
	public String getP1084C36() {
		return p1084C36;
	}

	public void setP1084C36(String p1084C36) {
		this.p1084C36 = p1084C36;
	}

	@XmlElement(name = "PEIB_5")
	public String getPeib5() {
		return peib5;
	}

	public void setPeib5(String peib5) {
		this.peib5 = peib5;
	}

	@XmlElement(name = "PEIB_6")
	public String getPeib6() {
		return peib6;
	}

	public void setPeib6(String peib6) {
		this.peib6 = peib6;
	}

	@XmlElement(name = "PEIB_6_RES")
	public String getPeib6Res() {
		return peib6Res;
	}

	public void setPeib6Res(String peib6Res) {
		this.peib6Res = peib6Res;
	}

	@XmlElement(name = "PEIB_7")
	public String getPeib7() {
		return peib7;
	}

	public void setPeib7(String peib7) {
		this.peib7 = peib7;
	}

	@XmlElement(name = "PEIB_7_NRO")
	public Integer getPeib7Nro() {
		return peib7Nro;
	}

	public void setPeib7Nro(Integer peib7Nro) {
		this.peib7Nro = peib7Nro;
	}

	@XmlElement(name = "P401_123")
	public String getP401123() {
		return p401123;
	}

	public void setP401123(String p401123) {
		this.p401123 = p401123;
	}

	@XmlElement(name = "P402_D123")
	public String getP402D123() {
		return p402D123;
	}

	public void setP402D123(String p402D123) {
		this.p402D123 = p402D123;
	}

	@XmlElement(name = "P402_M123")
	public String getP402M123() {
		return p402M123;
	}

	public void setP402M123(String p402M123) {
		this.p402M123 = p402M123;
	}

	@XmlElement(name = "P402_A123")
	public String getP402A123() {
		return p402A123;
	}

	public void setP402A123(String p402A123) {
		this.p402A123 = p402A123;
	}

	@XmlElement(name = "P404_123")
	public String getP404123() {
		return p404123;
	}

	public void setP404123(String p404123) {
		this.p404123 = p404123;
	}

	@XmlElement(name = "P405_D123")
	public String getP405D123() {
		return p405D123;
	}

	public void setP405D123(String p405D123) {
		this.p405D123 = p405D123;
	}

	@XmlElement(name = "P405_M123")
	public String getP405M123() {
		return p405M123;
	}

	public void setP405M123(String p405M123) {
		this.p405M123 = p405M123;
	}

	@XmlElement(name = "P405_A123")
	public String getP405A123() {
		return p405A123;
	}

	public void setP405A123(String p405A123) {
		this.p405A123 = p405A123;
	}

	@XmlElement(name = "P406_C4")
	public String getP406C4() {
		return p406C4;
	}

	public void setP406C4(String p406C4) {
		this.p406C4 = p406C4;
	}

	@XmlElement(name = "P407_C4_1")
	public String getP407C41() {
		return p407C41;
	}

	public void setP407C41(String p407C41) {
		this.p407C41 = p407C41;
	}

	@XmlElement(name = "P407_C4_2")
	public String getP407C42() {
		return p407C42;
	}

	public void setP407C42(String p407C42) {
		this.p407C42 = p407C42;
	}

	@XmlElement(name = "P407_C4_3")
	public String getP407C43() {
		return p407C43;
	}

	public void setP407C43(String p407C43) {
		this.p407C43 = p407C43;
	}

	@XmlElement(name = "P407_C4_4")
	public String getP407C44() {
		return p407C44;
	}

	public void setP407C44(String p407C44) {
		this.p407C44 = p407C44;
	}

	@XmlElement(name = "P407_C4_5")
	public String getP407C45() {
		return p407C45;
	}

	public void setP407C45(String p407C45) {
		this.p407C45 = p407C45;
	}

	@XmlElement(name = "P407_C4_6")
	public String getP407C46() {
		return p407C46;
	}

	public void setP407C46(String p407C46) {
		this.p407C46 = p407C46;
	}

	@XmlElement(name = "P407_C4_7")
	public String getP407C47() {
		return p407C47;
	}

	public void setP407C47(String p407C47) {
		this.p407C47 = p407C47;
	}

	@XmlElement(name = "P407_C4_8")
	public String getP407C48() {
		return p407C48;
	}

	public void setP407C48(String p407C48) {
		this.p407C48 = p407C48;
	}

	@XmlElement(name = "P407_C4_9")
	public String getP407C49() {
		return p407C49;
	}

	public void setP407C49(String p407C49) {
		this.p407C49 = p407C49;
	}

	@XmlElement(name = "P407_C4_9ESP")
	public String getP407C49esp() {
		return p407C49esp;
	}

	public void setP407C49esp(String p407C49esp) {
		this.p407C49esp = p407C49esp;
	}

	@XmlElement(name = "P407_C4_10")
	public String getP407C410() {
		return p407C410;
	}

	public void setP407C410(String p407C410) {
		this.p407C410 = p407C410;
	}

	@XmlElement(name = "P407_C4_10ESP")
	public String getP407C410esp() {
		return p407C410esp;
	}

	public void setP407C410esp(String p407C410esp) {
		this.p407C410esp = p407C410esp;
	}

	@XmlElement(name = "P407_C4_N")
	public String getP407C4N() {
		return p407C4N;
	}

	public void setP407C4N(String p407C4N) {
		this.p407C4N = p407C4N;
	}

	@XmlElement(name = "P408_C3")
	public String getP408C3() {
		return p408C3;
	}

	public void setP408C3(String p408C3) {
		this.p408C3 = p408C3;
	}

	@XmlElement(name = "P409_C3_D")
	public String getP409C3D() {
		return p409C3D;
	}

	public void setP409C3D(String p409C3D) {
		this.p409C3D = p409C3D;
	}

	@XmlElement(name = "P409_C3_M")
	public String getP409C3M() {
		return p409C3M;
	}

	public void setP409C3M(String p409C3M) {
		this.p409C3M = p409C3M;
	}

	@XmlElement(name = "P409_C3_A")
	public String getP409C3A() {
		return p409C3A;
	}

	public void setP409C3A(String p409C3A) {
		this.p409C3A = p409C3A;
	}

	@XmlElement(name = "BBTK_AULA_0")
	public String getBbtkAula0() {
		return bbtkAula0;
	}

	public void setBbtkAula0(String bbtkAula0) {
		this.bbtkAula0 = bbtkAula0;
	}

	@XmlElement(name = "BBTK_AULA_1")
	public String getBbtkAula1() {
		return bbtkAula1;
	}

	public void setBbtkAula1(String bbtkAula1) {
		this.bbtkAula1 = bbtkAula1;
	}

	@XmlElement(name = "BBTK_AULA_2")
	public String getBbtkAula2() {
		return bbtkAula2;
	}

	public void setBbtkAula2(String bbtkAula2) {
		this.bbtkAula2 = bbtkAula2;
	}

	@XmlElement(name = "BBTK_AULA_3")
	public String getBbtkAula3() {
		return bbtkAula3;
	}

	public void setBbtkAula3(String bbtkAula3) {
		this.bbtkAula3 = bbtkAula3;
	}

	@XmlElement(name = "BBTK_AULA_4")
	public String getBbtkAula4() {
		return bbtkAula4;
	}

	public void setBbtkAula4(String bbtkAula4) {
		this.bbtkAula4 = bbtkAula4;
	}

	@XmlElement(name = "BBTK_AULA_5")
	public String getBbtkAula5() {
		return bbtkAula5;
	}

	public void setBbtkAula5(String bbtkAula5) {
		this.bbtkAula5 = bbtkAula5;
	}

	@XmlElement(name = "BBTK_AULA_N")
	public String getBbtkAulaN() {
		return bbtkAulaN;
	}

	public void setBbtkAulaN(String bbtkAulaN) {
		this.bbtkAulaN = bbtkAulaN;
	}

	@XmlElement(name = "P502_C1")
	public String getP502C1() {
		return p502C1;
	}

	public void setP502C1(String p502C1) {
		this.p502C1 = p502C1;
	}

	@XmlElement(name = "P502_C1_D")
	public String getP502C1D() {
		return p502C1D;
	}

	public void setP502C1D(String p502C1D) {
		this.p502C1D = p502C1D;
	}

	@XmlElement(name = "P502_C1_M")
	public String getP502C1M() {
		return p502C1M;
	}

	public void setP502C1M(String p502C1M) {
		this.p502C1M = p502C1M;
	}

	@XmlElement(name = "P502_C1_A")
	public String getP502C1A() {
		return p502C1A;
	}

	public void setP502C1A(String p502C1A) {
		this.p502C1A = p502C1A;
	}

	@XmlElement(name = "P502_C34")
	public String getP502C34() {
		return p502C34;
	}

	public void setP502C34(String p502C34) {
		this.p502C34 = p502C34;
	}

	@XmlElement(name = "P508_C3_1")
	public String getP508C31() {
		return p508C31;
	}

	public void setP508C31(String p508C31) {
		this.p508C31 = p508C31;
	}

	@XmlElement(name = "P508_C3_2")
	public String getP508C32() {
		return p508C32;
	}

	public void setP508C32(String p508C32) {
		this.p508C32 = p508C32;
	}

	@XmlElement(name = "P508_C3_3")
	public String getP508C33() {
		return p508C33;
	}

	public void setP508C33(String p508C33) {
		this.p508C33 = p508C33;
	}

	@XmlElement(name = "P508_C3_4")
	public String getP508C34() {
		return p508C34;
	}

	public void setP508C34(String p508C34) {
		this.p508C34 = p508C34;
	}

	@XmlElement(name = "P508_C3_5")
	public String getP508C35() {
		return p508C35;
	}

	public void setP508C35(String p508C35) {
		this.p508C35 = p508C35;
	}

	@XmlElement(name = "P508_C3_6")
	public String getP508C36() {
		return p508C36;
	}

	public void setP508C36(String p508C36) {
		this.p508C36 = p508C36;
	}

	@XmlElement(name = "P508_C3_6ESP")
	public String getP508C36esp() {
		return p508C36esp;
	}

	public void setP508C36esp(String p508C36esp) {
		this.p508C36esp = p508C36esp;
	}

	@XmlElement(name = "ANOTACIONES")
	public String getAnotaciones() {
		return anotaciones;
	}

	public void setAnotaciones(String anotaciones) {
		this.anotaciones = anotaciones;
	}

	@XmlElement(name = "FUENTE")
	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	@XmlElement(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	@XmlElement(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name = "TIPOENVIO")
	public String getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(String tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	@XmlElement(name = "ULTIMO")
	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	@XmlElement(name = "TADMINIST")
	public Integer getTadminist() {
		return tadminist;
	}

	public void setTadminist(Integer tadminist) {
		this.tadminist = tadminist;
	}

	@XmlElement(name = "TAUXILIAR")
	public Integer getTauxiliar() {
		return tauxiliar;
	}

	public void setTauxiliar(Integer tauxiliar) {
		this.tauxiliar = tauxiliar;
	}

	@XmlElement(name = "TDOCENTES")
	public Integer getTdocentes() {
		return tdocentes;
	}

	public void setTdocentes(Integer tdocentes) {
		this.tdocentes = tdocentes;
	}

	@XmlElement(name = "FORMA_PR")
	public String getFormaPr() {
		return formaPr;
	}

	public void setFormaPr(String formaPr) {
		this.formaPr = formaPr;
	}

	@XmlElement(name = "FORMA_SP")
	public String getFormaSp() {
		return formaSp;
	}

	public void setFormaSp(String formaSp) {
		this.formaSp = formaSp;
	}

	@XmlElement(name = "FORMA_AD")
	public String getFormaAd() {
		return formaAd;
	}

	public void setFormaAd(String formaAd) {
		this.formaAd = formaAd;
	}

	/*
	 * @XmlElement(name="PERIFERICOS") public List<Matricula2015Perifericos>
	 * getMatricula2015PerifericosList() { return matricula2015PerifericosList; }
	 * 
	 * public void setMatricula2015PerifericosList(List<Matricula2015Perifericos>
	 * matricula2015PerifericosList) { this.matricula2015PerifericosList =
	 * matricula2015PerifericosList; }
	 * 
	 * @XmlElement(name="PRONOEI") public List<Matricula2015Localpronoei>
	 * getMatricula2015LocalpronoeiList() { return matricula2015LocalpronoeiList; }
	 * 
	 * public void setMatricula2015LocalpronoeiList(List<Matricula2015Localpronoei>
	 * matricula2015LocalpronoeiList) { this.matricula2015LocalpronoeiList =
	 * matricula2015LocalpronoeiList; }
	 * 
	 * @XmlElement(name="PERSONAL") public List<Matricula2015Personal>
	 * getMatricula2015PersonalList() { return matricula2015PersonalList; }
	 * 
	 * public void setMatricula2015PersonalList(List<Matricula2015Personal>
	 * matricula2015PersonalList) { this.matricula2015PersonalList =
	 * matricula2015PersonalList; }
	 * 
	 * @XmlElement(name="RECURSOS")
	 * 
	 * @XmlJavaTypeAdapter(Matricula2015RecursosMapAdapter.class) public Map<String,
	 * Matricula2015Recursos> getDetalleRecursos() { return detalleRecursos; }
	 * 
	 * public void setDetalleRecursos(Map<String, Matricula2015Recursos>
	 * detalleRecursos) { this.detalleRecursos = detalleRecursos; }
	 * 
	 * @XmlElement(name="SAANEES") public List<Matricula2015Saanee>
	 * getMatricula2015SaaneeList() { return matricula2015SaaneeList; }
	 * 
	 * public void setMatricula2015SaaneeList(List<Matricula2015Saanee>
	 * matricula2015SaaneeList) { this.matricula2015SaaneeList =
	 * matricula2015SaaneeList; }
	 * 
	 * @XmlElement(name="CARRERAS") public List<Matricula2015Carreras>
	 * getMatricula2015CarrerasList() { return matricula2015CarrerasList; }
	 * 
	 * public void setMatricula2015CarrerasList(List<Matricula2015Carreras>
	 * matricula2015CarrerasList) { this.matricula2015CarrerasList =
	 * matricula2015CarrerasList; }
	 * 
	 * @XmlElement(name="MATRICULA")
	 * 
	 * @XmlJavaTypeAdapter(Matricula2015MatriculaMapAdapter.class) public
	 * Map<String, Matricula2015Matricula> getDetalleMatricula() { return
	 * detalleMatricula; }
	 * 
	 * public void setDetalleMatricula(Map<String, Matricula2015Matricula>
	 * detalleMatricula) { this.detalleMatricula = detalleMatricula; }
	 * 
	 * @XmlElement(name="SECCION")
	 * 
	 * @XmlJavaTypeAdapter(Matricula2015SeccionMapAdapter.class) public Map<String,
	 * Matricula2015Seccion> getDetalleSeccion() { return detalleSeccion; }
	 * 
	 * public void setDetalleSeccion(Map<String, Matricula2015Seccion>
	 * detalleSeccion) { this.detalleSeccion = detalleSeccion; }
	 */
	@XmlAttribute
	public long getToken() {
		return token;
	}

	public void setToken(long token) {
		this.token = token;
	}

	@XmlAttribute(name = "MSG")
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@XmlAttribute(name = "ESTADO_RPT")
	public String getEstadoRpt() {
		return estadoRpt;
	}

	public void setEstadoRpt(String estadoRpt) {
		this.estadoRpt = estadoRpt;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEnvio != null ? idEnvio.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Matricula2015Cabecera)) {
			return false;
		}
		Matricula2015Cabecera other = (Matricula2015Cabecera) object;
		if ((this.idEnvio == null && other.idEnvio != null)
				|| (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
			return false;
		}
		return true;
	}

}
