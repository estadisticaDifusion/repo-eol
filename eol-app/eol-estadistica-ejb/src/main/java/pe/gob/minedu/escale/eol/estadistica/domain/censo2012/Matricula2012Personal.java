/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2012_personal")
public class Matricula2012Personal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;

    @Column(name = "NRO")
    private Integer nro;

    @Column(name = "PER01")
    private String per01;
    @Column(name = "PER02")
    private String per02;
    @Column(name = "PER03")
    private String per03;
    @Column(name = "PER04")
    private String per04;
    @Column(name = "PER05")
    private Integer per05;
    @Column(name = "PER06")
    private String per06;
    @Column(name = "PER07")
    private String per07;
    @Column(name = "PER08")
    private String per08;
    @Column(name = "PER09")
    private String per09;
    @Column(name = "PER10")
    private String per10;
    @Column(name = "PER11")
    private String per11;
    @Column(name = "PER12")
    private String per12;
    @Column(name = "PER13")
    private String per13;
    @Column(name = "PER14")
    private Integer per14;
    @Column(name = "PER15")
    private Integer per15;
    @Column(name = "PER16")
    private String per16;
    @Column(name = "PER17")
    private String per17;
    @Column(name = "PER18")
    private String per18;
    @Column(name = "PER19")
    private String per19;
    @Column(name = "PER20")
    private String per20;
    @Column(name = "PER21")
    private String per21;
    @Column(name = "PER22")
    private Integer per22;
    @Column(name = "PER_A_00")
    private String perA00;
    @Column(name = "PER_A_01")
    private String perA01;
    @Column(name = "PER_A_02")
    private String perA02;
    @Column(name = "PER_A_03")
    private String perA03;
    @Column(name = "PER_A_04")
    private String perA04;
    @Column(name = "PER_A_05")
    private String perA05;
    @Column(name = "PER_A_06")
    private String perA06;
    @Column(name = "PER_A_07")
    private String perA07;
    @Column(name = "PER_A_08")
    private String perA08;
    @Column(name = "PER_A_09")
    private String perA09;
    @Column(name = "PER_A_10")
    private String perA10;
    @Column(name = "A_CARGO_01")
    private String aCargo01;
    @Column(name = "A_CARGO_02")
    private String aCargo02;
    @Column(name = "A_CARGO_03")
    private String aCargo03;
    @Column(name = "A_CARGO_04")
    private String aCargo04;
    @Column(name = "A_CARGO_05")
    private String aCargo05;
    @Column(name = "A_CARGO_06")
    private String aCargo06;
    @Column(name = "A_CARGO_07")
    private String aCargo07;
    @Column(name = "A_CARGO_08")
    private String aCargo08;
    @Column(name = "A_CARGO_09")
    private String aCargo09;
    @Column(name = "A_CARGO_10")
    private String aCargo10;
    @Column(name = "A_CARGO_11")
    private String aCargo11;
    @Column(name = "A_CARGO_12")
    private String aCargo12;
    @Column(name = "A_CARGO_13")
    private String aCargo13;
    @Column(name = "NH_SALA_COM")
    private Integer nhSalaCom;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2012Cabecera matricula2012Cabecera;

    public Matricula2012Personal() {
    }

    public Matricula2012Personal(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="PER01")
    public String getPer01() {
        return per01;
    }

    public void setPer01(String per01) {
        this.per01 = per01;
    }

    @XmlElement(name="PER02")
    public String getPer02() {
        return per02;
    }

    public void setPer02(String per02) {
        this.per02 = per02;
    }

    @XmlElement(name="PER03")
    public String getPer03() {
        return per03;
    }

    public void setPer03(String per03) {
        this.per03 = per03;
    }

    @XmlElement(name="PER04")
    public String getPer04() {
        return per04;
    }

    public void setPer04(String per04) {
        this.per04 = per04;
    }

    @XmlElement(name="PER05")
    public Integer getPer05() {
        return per05;
    }

    public void setPer05(Integer per05) {
        this.per05 = per05;
    }

    @XmlElement(name="PER06")
    public String getPer06() {
        return per06;
    }

    public void setPer06(String per06) {
        this.per06 = per06;
    }

    @XmlElement(name="PER07")
    public String getPer07() {
        return per07;
    }

    public void setPer07(String per07) {
        this.per07 = per07;
    }

    @XmlElement(name="PER08")
    public String getPer08() {
        return per08;
    }

    public void setPer08(String per08) {
        this.per08 = per08;
    }

    @XmlElement(name="PER09")
    public String getPer09() {
        return per09;
    }

    public void setPer09(String per09) {
        this.per09 = per09;
    }

    @XmlElement(name="PER10")
    public String getPer10() {
        return per10;
    }

    public void setPer10(String per10) {
        this.per10 = per10;
    }

    @XmlElement(name="PER11")
    public String getPer11() {
        return per11;
    }

    public void setPer11(String per11) {
        this.per11 = per11;
    }

    @XmlElement(name="PER12")
    public String getPer12() {
        return per12;
    }

    public void setPer12(String per12) {
        this.per12 = per12;
    }

    @XmlElement(name="PER13")
    public String getPer13() {
        return per13;
    }

    public void setPer13(String per13) {
        this.per13 = per13;
    }

    @XmlElement(name="PER14")
    public Integer getPer14() {
        return per14;
    }

    public void setPer14(Integer per14) {
        this.per14 = per14;
    }

    @XmlElement(name="PER15")
    public Integer getPer15() {
        return per15;
    }

    public void setPer15(Integer per15) {
        this.per15 = per15;
    }

    @XmlElement(name="PER16")
    public String getPer16() {
        return per16;
    }

    public void setPer16(String per16) {
        this.per16 = per16;
    }

    @XmlElement(name="PER17")
    public String getPer17() {
        return per17;
    }

    public void setPer17(String per17) {
        this.per17 = per17;
    }

    @XmlElement(name="PER18")
    public String getPer18() {
        return per18;
    }

    public void setPer18(String per18) {
        this.per18 = per18;
    }

    @XmlElement(name="PER19")
    public String getPer19() {
        return per19;
    }

    public void setPer19(String per19) {
        this.per19 = per19;
    }

    @XmlElement(name="PER20")
    public String getPer20() {
        return per20;
    }

    public void setPer20(String per20) {
        this.per20 = per20;
    }

    @XmlElement(name="PER21")
    public String getPer21() {
        return per21;
    }

    public void setPer21(String per21) {
        this.per21 = per21;
    }

    @XmlElement(name="PER22")
    public Integer getPer22() {
        return per22;
    }

    public void setPer22(Integer per22) {
        this.per22 = per22;
    }

    @XmlElement(name="PER_A_00")
    public String getPerA00() {
        return perA00;
    }

    public void setPerA00(String perA00) {
        this.perA00 = perA00;
    }

    @XmlElement(name="PER_A_01")
    public String getPerA01() {
        return perA01;
    }

    public void setPerA01(String perA01) {
        this.perA01 = perA01;
    }

    @XmlElement(name="PER_A_02")
    public String getPerA02() {
        return perA02;
    }

    public void setPerA02(String perA02) {
        this.perA02 = perA02;
    }

    @XmlElement(name="PER_A_03")
    public String getPerA03() {
        return perA03;
    }

    public void setPerA03(String perA03) {
        this.perA03 = perA03;
    }

    @XmlElement(name="PER_A_04")
    public String getPerA04() {
        return perA04;
    }

    public void setPerA04(String perA04) {
        this.perA04 = perA04;
    }

    @XmlElement(name="PER_A_05")
    public String getPerA05() {
        return perA05;
    }

    public void setPerA05(String perA05) {
        this.perA05 = perA05;
    }

    @XmlElement(name="PER_A_06")
    public String getPerA06() {
        return perA06;
    }

    public void setPerA06(String perA06) {
        this.perA06 = perA06;
    }

    @XmlElement(name="PER_A_07")
    public String getPerA07() {
        return perA07;
    }

    public void setPerA07(String perA07) {
        this.perA07 = perA07;
    }

    @XmlElement(name="PER_A_08")
    public String getPerA08() {
        return perA08;
    }

    public void setPerA08(String perA08) {
        this.perA08 = perA08;
    }

    @XmlElement(name="PER_A_09")
    public String getPerA09() {
        return perA09;
    }

    public void setPerA09(String perA09) {
        this.perA09 = perA09;
    }

    @XmlElement(name="PER_A_10")
    public String getPerA10() {
        return perA10;
    }

    public void setPerA10(String perA10) {
        this.perA10 = perA10;
    }

    @XmlElement(name="A_CARGO_01")
    public String getACargo01() {
        return aCargo01;
    }

    public void setACargo01(String aCargo01) {
        this.aCargo01 = aCargo01;
    }

    @XmlElement(name="A_CARGO_02")
    public String getACargo02() {
        return aCargo02;
    }

    public void setACargo02(String aCargo02) {
        this.aCargo02 = aCargo02;
    }

    @XmlElement(name="A_CARGO_03")
    public String getACargo03() {
        return aCargo03;
    }

    public void setACargo03(String aCargo03) {
        this.aCargo03 = aCargo03;
    }

    @XmlElement(name="A_CARGO_04")
    public String getACargo04() {
        return aCargo04;
    }

    public void setACargo04(String aCargo04) {
        this.aCargo04 = aCargo04;
    }

    @XmlElement(name="A_CARGO_05")
    public String getACargo05() {
        return aCargo05;
    }

    public void setACargo05(String aCargo05) {
        this.aCargo05 = aCargo05;
    }

    @XmlElement(name="A_CARGO_06")
    public String getACargo06() {
        return aCargo06;
    }

    public void setACargo06(String aCargo06) {
        this.aCargo06 = aCargo06;
    }

    @XmlElement(name="A_CARGO_07")
    public String getACargo07() {
        return aCargo07;
    }

    public void setACargo07(String aCargo07) {
        this.aCargo07 = aCargo07;
    }

    @XmlElement(name="A_CARGO_08")
    public String getACargo08() {
        return aCargo08;
    }

    public void setACargo08(String aCargo08) {
        this.aCargo08 = aCargo08;
    }

    @XmlElement(name="A_CARGO_09")
    public String getACargo09() {
        return aCargo09;
    }

    public void setACargo09(String aCargo09) {
        this.aCargo09 = aCargo09;
    }

    @XmlElement(name="A_CARGO_10")
    public String getACargo10() {
        return aCargo10;
    }

    public void setACargo10(String aCargo10) {
        this.aCargo10 = aCargo10;
    }

    @XmlElement(name="A_CARGO_11")
    public String getACargo11() {
        return aCargo11;
    }

    public void setACargo11(String aCargo11) {
        this.aCargo11 = aCargo11;
    }

    @XmlElement(name="A_CARGO_12")
    public String getACargo12() {
        return aCargo12;
    }

    public void setACargo12(String aCargo12) {
        this.aCargo12 = aCargo12;
    }

    @XmlElement(name="A_CARGO_13")
    public String getACargo13() {
        return aCargo13;
    }

    public void setACargo13(String aCargo13) {
        this.aCargo13 = aCargo13;
    }

    @XmlElement(name="NH_SALA_COM")
    public Integer getNhSalaCom() {
        return nhSalaCom;
    }

    public void setNhSalaCom(Integer nhSalaCom) {
        this.nhSalaCom = nhSalaCom;
    }

    @XmlTransient
    public Matricula2012Cabecera getMatricula2012Cabecera() {
        return matricula2012Cabecera;
    }

    public void setMatricula2012Cabecera(Matricula2012Cabecera matricula2012Cabecera) {
        this.matricula2012Cabecera = matricula2012Cabecera;
    }

    @XmlElement(name="NRO")
    public Integer getNro() {
        return nro;
    }

    public void setNro(Integer nro) {
        this.nro = nro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2012Personal)) {
            return false;
        }
        Matricula2012Personal other = (Matricula2012Personal) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }



}
