package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "matricula2015_recursos")
public class Matricula2015Recursos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2015Cabecera matricula2015Cabecera;
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2015Recursos")
    private List<Matricula2015RecursosFila> matricula2015RecursosFilaList;
*/
    public Matricula2015Recursos() {
    }

    public Matricula2015Recursos(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Matricula2015Cabecera getMatricula2015Cabecera() {
        return matricula2015Cabecera;
    }

    public void setMatricula2015Cabecera(Matricula2015Cabecera matricula2015Cabecera) {
        this.matricula2015Cabecera = matricula2015Cabecera;
    }
/*
    @XmlElement(name="RECURSOS_FILAS")
    public List<Matricula2015RecursosFila> getMatricula2015RecursosFilaList() {
        return matricula2015RecursosFilaList;
    }

    public void setMatricula2015RecursosFilaList(List<Matricula2015RecursosFila> matricula2015RecursosFilaList) {
        this.matricula2015RecursosFilaList = matricula2015RecursosFilaList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2015Recursos)) {
            return false;
        }
        Matricula2015Recursos other = (Matricula2015Recursos) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
}
