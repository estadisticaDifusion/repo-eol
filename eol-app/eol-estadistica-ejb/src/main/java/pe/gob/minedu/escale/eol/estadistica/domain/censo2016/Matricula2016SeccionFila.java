package pe.gob.minedu.escale.eol.estadistica.domain.censo2016;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "matricula2016_seccion_fila")
public class Matricula2016SeccionFila implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL")
    private Integer total;
    @Column(name = "DATO01")
    private Integer dato01;
    @Column(name = "DATO02")
    private Integer dato02;
    @Column(name = "DATO03")
    private Integer dato03;
    @Column(name = "DATO04")
    private Integer dato04;
    @Column(name = "DATO05")
    private Integer dato05;
    @Column(name = "DATO06")
    private Integer dato06;
    @Column(name = "DATO07")
    private Integer dato07;
    @Column(name = "DATO08")
    private Integer dato08;
    @Column(name = "DATO09")
    private Integer dato09;
    @Column(name = "DATO10")
    private Integer dato10;
    @Column(name = "DATO11")
    private Integer dato11;
    @Column(name = "DATO12")
    private Integer dato12;
    @Column(name = "DATO13")
    private Integer dato13;
    @Column(name = "DATO14")
    private Integer dato14;
    @Column(name = "DATO15")
    private Integer dato15;
    @JoinColumn(name = "DETALLE_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2016Seccion matricula2016Seccion;

    public Matricula2016SeccionFila() {
    }

    public Matricula2016SeccionFila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name="DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name="TOTAL")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @XmlElement(name="DATO01")
    public Integer getDato01() {
        return dato01;
    }

    public void setDato01(Integer dato01) {
        this.dato01 = dato01;
    }

    @XmlElement(name="DATO02")
    public Integer getDato02() {
        return dato02;
    }

    public void setDato02(Integer dato02) {
        this.dato02 = dato02;
    }

    @XmlElement(name="DATO03")
    public Integer getDato03() {
        return dato03;
    }

    public void setDato03(Integer dato03) {
        this.dato03 = dato03;
    }

    @XmlElement(name="DATO04")
    public Integer getDato04() {
        return dato04;
    }

    public void setDato04(Integer dato04) {
        this.dato04 = dato04;
    }

    @XmlElement(name="DATO05")
    public Integer getDato05() {
        return dato05;
    }

    public void setDato05(Integer dato05) {
        this.dato05 = dato05;
    }

    @XmlElement(name="DATO06")
    public Integer getDato06() {
        return dato06;
    }

    public void setDato06(Integer dato06) {
        this.dato06 = dato06;
    }

    @XmlElement(name="DATO07")
    public Integer getDato07() {
        return dato07;
    }

    public void setDato07(Integer dato07) {
        this.dato07 = dato07;
    }

    @XmlElement(name="DATO08")
    public Integer getDato08() {
        return dato08;
    }

    public void setDato08(Integer dato08) {
        this.dato08 = dato08;
    }

    @XmlElement(name="DATO09")
    public Integer getDato09() {
        return dato09;
    }

    public void setDato09(Integer dato09) {
        this.dato09 = dato09;
    }

    @XmlElement(name="DATO10")
    public Integer getDato10() {
        return dato10;
    }
    @XmlElement(name="DATO11")
    public Integer getDato11() {
        return dato11;
    }

    public void setDato11(Integer dato11) {
        this.dato11 = dato11;
    }
    @XmlElement(name="DATO12")
    public Integer getDato12() {
        return dato12;
    }

    public void setDato12(Integer dato12) {
        this.dato12 = dato12;
    }
    @XmlElement(name="DATO13")
    public Integer getDato13() {
        return dato13;
    }

    public void setDato13(Integer dato13) {
        this.dato13 = dato13;
    }
    @XmlElement(name="DATO14")
    public Integer getDato14() {
        return dato14;
    }

    public void setDato14(Integer dato14) {
        this.dato14 = dato14;
    }
    @XmlElement(name="DATO15")
    public Integer getDato15() {
        return dato15;
    }

    public void setDato15(Integer dato15) {
        this.dato15 = dato15;
    }




    public void setDato10(Integer dato10) {
        this.dato10 = dato10;
    }

    @XmlTransient
    public Matricula2016Seccion getMatricula2016Seccion() {
        return matricula2016Seccion;
    }

    public void setMatricula2016Seccion(Matricula2016Seccion matricula2016Seccion) {
        this.matricula2016Seccion = matricula2016Seccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2016SeccionFila)) {
            return false;
        }
        Matricula2016SeccionFila other = (Matricula2016SeccionFila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
