/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.ejb.censo2013;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2013.Local2013Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;


/**
 *
 * @author JMATAMOROS
 */
@Singleton
public class Local2013Facade extends AbstractFacade<Local2013Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2013Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Local2013Facade() {
		super(Local2013Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Local2013Cabecera entity) {
		String sql = "UPDATE Local2013Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();
/*
		for (Local2013Sec104 sec104 : entity.getSec104()) {
			sec104.setLocal2013Cabecera(entity);
		}
		if (entity.getSec300() != null)
			for (Local2013Sec300 sec300 : entity.getSec300()) {
				sec300.setLocal2013Cabecera(entity);
			}

		if (entity.getSec400() != null)
			for (Local2013Sec400 sec400 : entity.getSec400()) {
				sec400.setLocal2013Cabecera(entity);
			}

		if (entity.getSec500() != null)
			for (Local2013Sec500 sec500 : entity.getSec500()) {
				sec500.setLocal2013Cabecera(entity);
			}
*/
		em.persist(entity);
		em.flush();
	}

	public Local2013Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2013Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2013Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
