/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.domain.Plazo;
import pe.gob.minedu.escale.eol.estadistica.domain.PlazoPK;
import pe.gob.minedu.escale.eol.portlets.ejb.AbstractFacade;

/**
 *
 * @author Ing. Oscar Mateo
 * 
 */

@Singleton
public class PlazoFacade extends AbstractFacade<Plazo> implements PlazoLocal {

	private Logger logger = Logger.getLogger(PlazoFacade.class);

	private DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public PlazoFacade() {
		super(Plazo.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public Plazo find(String periodo, String proceso) {
		PlazoPK pk = new PlazoPK(periodo, proceso);
		return em.find(Plazo.class, pk);
	}
	/*
	 * public List<Plazo> findAll() { return
	 * em.createQuery("select object(o) from Plazo as o").getResultList(); }
	 */

	@SuppressWarnings("unused")
	public String verificarPlazoEntrega(String anio, String tipoDocumento) {
		logger.info(":: PlazoFacade.verificarPlazoEntrega :: Starting execution...");
		Plazo plazo = find(anio, tipoDocumento);
		Date ahora = new Date();
		Date fechTe = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(plazo.getFechaTermino());
		c.add(Calendar.DATE, 1);
		fechTe = c.getTime();

		logger.info("AHORA :" + DATE_FORMAT.format(ahora));
		logger.info("TERMINO :" + DATE_FORMAT.format(fechTe));
		logger.info("plazo Inicio :" + DATE_FORMAT.format(plazo.getFechaInicio()));
		logger.info("plazo Fin :" + DATE_FORMAT.format(plazo.getFechaTermino()));

		String ret = null;
		if (plazo == null) {
			ret = "Aun no se ha establecido una fecha de entrega electronica para este tipo de documento";
		}
		if (!((ahora.equals(plazo.getFechaInicio()) || ahora.after(plazo.getFechaInicio()))
				&& (ahora.equals(fechTe) || ahora.before(fechTe)))) {
			ret = ("El plazo de entrega electrónica para este tipo documento es desde el día "
					+ DATE_FORMAT.format(plazo.getFechaInicio()) + " hasta el día "
					+ DATE_FORMAT.format(plazo.getFechaTermino()));
		}
		logger.info("ret = " + ret);
		logger.info(":: PlazoFacade.verificarPlazoEntrega :: Execution finish.");
		return ret;
	}
}
