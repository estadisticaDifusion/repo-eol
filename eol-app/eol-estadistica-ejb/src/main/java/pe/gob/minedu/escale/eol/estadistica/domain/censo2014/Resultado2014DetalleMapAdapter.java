/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Resultado2014DetalleMapAdapter.Resultado2014DetalleList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Resultado2014DetalleMapAdapter extends XmlAdapter<Resultado2014DetalleList, Map<String, Resultado2014Detalle>> {

    private static final Logger LOGGER = Logger.getLogger(Resultado2014DetalleMapAdapter.class.getName());

    static class Resultado2014DetalleList {

        private List<Resultado2014Detalle> detalle;

        private Resultado2014DetalleList(ArrayList<Resultado2014Detalle> lista) {
            detalle = lista;
        }

        public Resultado2014DetalleList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Resultado2014Detalle> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Resultado2014Detalle> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Resultado2014Detalle> unmarshal(Resultado2014DetalleList v) throws Exception {

        Map<String, Resultado2014Detalle> map = new HashMap<String, Resultado2014Detalle>();
        for (Resultado2014Detalle detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Resultado2014DetalleList marshal(Map<String, Resultado2014Detalle> v) throws Exception {
        if(v==null)
        {   LOGGER.fine("no hay detalles");
            return null;            
        }
        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Resultado2014Detalle> lista = new ArrayList<Resultado2014Detalle>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Resultado2014Detalle $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Resultado2014DetalleList list = new Resultado2014DetalleList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
