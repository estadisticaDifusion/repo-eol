/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.ejb;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.rpc.ServiceException;

import com.liferay.client.soap.portal.model.RoleSoap;
import com.liferay.client.soap.portal.model.UserSoap;
import com.liferay.client.soap.portal.service.http.Portal_RoleServiceSoapBindingStub;
import com.liferay.client.soap.portal.service.http.Portal_UserServiceSoapBindingStub;
import com.liferay.client.soap.portal.service.http.RoleServiceSoap;
import com.liferay.client.soap.portal.service.http.RoleServiceSoapServiceLocator;
import com.liferay.client.soap.portal.service.http.UserServiceSoap;
import com.liferay.client.soap.portal.service.http.UserServiceSoapServiceLocator;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.domain.SesionUsuario;

/**
 *
 * @author DSILVA
 */
@Stateless
public class LoginFacade {

	static final Logger LOGGER = Logger.getLogger(LoginFacade.class.getName());
	static final String USER_SERVICE_URL = "http://escale.minedu.gob.pe/tunnel-web/secure/axis/Portal_UserService";
	private static String ROLE_SERVICE_URL = "http://escale.minedu.gob.pe/tunnel-web/secure/axis/Portal_RoleService";
	static final long COMPANY_ID = 10131;
	static final String DIRECTOR_ROLE_NAME = "director";
	static final String DIRECTOR_ROLE_NAME_AUTH = "CE";
	static final String UGEL_ROLE_NAME_AUTH = "UGEL";

	@EJB
	private SesionUsuarioFacade usuarioFacade;

	public SesionUsuario loginDirector(String usuario, String contrasenia) {
		try {
			UserServiceSoapServiceLocator userServiceLocator = new UserServiceSoapServiceLocator();
			UserServiceSoap userService = userServiceLocator.getPortal_UserService(new URL(USER_SERVICE_URL));
			((Portal_UserServiceSoapBindingStub) userService).setUsername(usuario);
			((Portal_UserServiceSoapBindingStub) userService).setPassword(contrasenia);

			UserSoap user = ((Portal_UserServiceSoapBindingStub) userService).getUserByScreenName(COMPANY_ID, usuario);

			RoleServiceSoapServiceLocator roleServiceLocator = new RoleServiceSoapServiceLocator();
			RoleServiceSoap roleService = roleServiceLocator.getPortal_RoleService(new URL(ROLE_SERVICE_URL));
			// roleService.
			((Portal_RoleServiceSoapBindingStub) roleService).setUsername(usuario);
			((Portal_RoleServiceSoapBindingStub) roleService).setPassword(contrasenia);
			RoleSoap[] roles = ((Portal_RoleServiceSoapBindingStub) roleService).getUserRoles(user.getUserId());
			boolean esDirector = false;
			for (RoleSoap roleSoap : roles) {
				if (DIRECTOR_ROLE_NAME.equals(roleSoap.getName())) {
					esDirector = true;
					break;
				}
			}
			if (!esDirector) {
				return null;
			}

			// creando sesion...
			SesionUsuario sesion = new SesionUsuario();
			sesion.setUsuario(usuario);
			sesion.setInicio(new Date());
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR_OF_DAY, 1); // caduca en una hora
			sesion.setCaduca(cal.getTime());

			usuarioFacade.create(sesion);
			return sesion;
		} catch (RemoteException ex) {
			LOGGER.log(Level.WARNING, ex.getMessage());
		} catch (ServiceException ex) {
			LOGGER.log(Level.WARNING, ex.getMessage());
		} catch (MalformedURLException ex) {
			LOGGER.log(Level.WARNING, ex.getMessage());

		}
		return null;
	}

	public SesionUsuario loginDirectorAuth(String usuario, String contrasenia) {
		try {
			AuthUsuario user = usuarioFacade.userAuthValid(usuario, contrasenia);

			if (user == null) {
				return null;
			}

			String tipo = user.getTipo() != null ? user.getTipo() : "";
			if (!(DIRECTOR_ROLE_NAME_AUTH.equals(tipo) || UGEL_ROLE_NAME_AUTH.equals(tipo))) {
				return null;
			}

			// creando sesion...
			SesionUsuario sesion = new SesionUsuario();
			sesion.setUsuario(usuario);
			sesion.setInicio(new Date());
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR_OF_DAY, 1); // caduca en una hora
			sesion.setCaduca(cal.getTime());

			usuarioFacade.create(sesion);
			return sesion;
		} catch (Exception ex) {
			LOGGER.log(Level.WARNING, ex.getMessage());
		}
		return null;
	}
}
