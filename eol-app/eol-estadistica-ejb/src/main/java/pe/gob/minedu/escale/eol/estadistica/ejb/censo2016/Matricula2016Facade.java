package pe.gob.minedu.escale.eol.estadistica.ejb.censo2016;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2016.Matricula2016Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class Matricula2016Facade extends AbstractFacade<Matricula2016Cabecera> {

	/*
	 * Se modifico la nueva funcion
	 * CedulaMatricula2016Resources.verificarPlazoEntrega(String tipoDocumento,
	 * String anio) public static final String CEDULA_MATRICULA = "MATRICULA";
	 */
	public static final String CEDULA_MATRICULA = "CENSO-MATRICULA";
	static final Logger LOGGER = Logger.getLogger(Matricula2016Facade.class.getName());
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03A = "c03a";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08A = "c08a";
	public final static String CEDULA_09A = "c09a";

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Matricula2016Facade() {
		super(Matricula2016Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Matricula2016Cabecera cedula) {
		String sql = "UPDATE Matricula2016Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroced=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroced());
		query.executeUpdate();

		/*
		 * if (cedula.getDetalleMatricula() != null) { Set<String> keys =
		 * cedula.getDetalleMatricula().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2016Matricula detalle = cedula.getDetalleMatricula().get(key);
		 * detalle.setMatricula2016Cabecera(cedula); List<Matricula2016MatriculaFila>
		 * filas = detalle.getMatricula2016MatriculaFilaList(); for
		 * (Matricula2016MatriculaFila fila : filas) {
		 * fila.setMatricula2016Matricula(detalle); } } }
		 * 
		 * if (cedula.getDetalleSeccion() != null) { Set<String> keys =
		 * cedula.getDetalleSeccion().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2016Seccion detalle = cedula.getDetalleSeccion().get(key);
		 * detalle.setMatricula2016Cabecera(cedula); List<Matricula2016SeccionFila>
		 * filas = detalle.getMatricula2016SeccionFilaList(); for
		 * (Matricula2016SeccionFila fila : filas) {
		 * fila.setMatricula2016Seccion(detalle); } } }
		 * 
		 * if (cedula.getDetalleRecursos() != null) { Set<String> keys =
		 * cedula.getDetalleRecursos().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2016Recursos detalle = cedula.getDetalleRecursos().get(key);
		 * detalle.setMatricula2016Cabecera(cedula); List<Matricula2016RecursosFila>
		 * filas = detalle.getMatricula2016RecursosFilaList(); for
		 * (Matricula2016RecursosFila fila : filas) {
		 * fila.setMatricula2016Recursos(detalle); } } }
		 * 
		 * if (cedula.getMatricula2016PersonalList() != null) { for
		 * (Matricula2016Personal per : cedula.getMatricula2016PersonalList()) {
		 * per.setMatricula2016Cabecera(cedula); } }
		 * 
		 * if (cedula.getMatricula2016LocalpronoeiList() != null) { for
		 * (Matricula2016Localpronoei loc : cedula.getMatricula2016LocalpronoeiList()) {
		 * loc.setMatricula2016Cabecera(cedula); } } if
		 * (cedula.getMatricula2016CarrerasList() != null) { for (Matricula2016Carreras
		 * carr : cedula.getMatricula2016CarrerasList()) {
		 * carr.setMatricula2016Cabecera(cedula); } } if
		 * (cedula.getMatricula2016SaaneeList() != null) { for (Matricula2016Saanee saan
		 * : cedula.getMatricula2016SaaneeList()) {
		 * saan.setMatricula2016Cabecera(cedula); } }
		 */
		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

}
