/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2012_saanee_fila")
public class Matricula2012SaaneeFila implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "NRO")
    private String nro;
    @Column(name = "COD_MOD")
    private String codMod;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "DATO01")
    private String dato01;
    @Column(name = "DATO02")
    private String dato02;
    @Column(name = "DATO03")
    private String dato03;
    @Column(name = "DATO04")
    private String dato04;
    @Column(name = "DATO05")
    private String dato05;
    @Column(name = "DATO06")
    private String dato06;
    @Column(name = "DATO07")
    private String dato07;

    @Column(name = "NIV_MOD")
    private String nivMod;

    @Column(name = "DISTRITO")
    private String distrito;

    @Column(name = "NRO_ALUM")
    private Integer nroAlum;


    @JoinColumn(name = "DETALLE_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2012Saanee matricula2012Saanee;

    public Matricula2012SaaneeFila() {
    }

    public Matricula2012SaaneeFila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    @XmlElement(name="DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name="DATO01")
    public String getDato01() {
        return dato01;
    }

    public void setDato01(String dato01) {
        this.dato01 = dato01;
    }

    @XmlElement(name="DATO02")
    public String getDato02() {
        return dato02;
    }

    public void setDato02(String dato02) {
        this.dato02 = dato02;
    }

    @XmlElement(name="DATO03")
    public String getDato03() {
        return dato03;
    }

    public void setDato03(String dato03) {
        this.dato03 = dato03;
    }

    @XmlElement(name="DATO04")
    public String getDato04() {
        return dato04;
    }

    public void setDato04(String dato04) {
        this.dato04 = dato04;
    }

    @XmlElement(name="DATO05")
    public String getDato05() {
        return dato05;
    }

    public void setDato05(String dato05) {
        this.dato05 = dato05;
    }

    @XmlElement(name="DATO06")
    public String getDato06() {
        return dato06;
    }

    public void setDato06(String dato06) {
        this.dato06 = dato06;
    }

    @XmlElement(name="DATO07")
    public String getDato07() {
        return dato07;
    }

    public void setDato07(String dato07) {
        this.dato07 = dato07;
    }

    @XmlTransient
    public Matricula2012Saanee getMatricula2012Saanee() {
        return matricula2012Saanee;
    }

    public void setMatricula2012Saanee(Matricula2012Saanee matricula2012Saanee) {
        this.matricula2012Saanee = matricula2012Saanee;
    }

@XmlElement(name="COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2012SaaneeFila)) {
            return false;
        }
        Matricula2012SaaneeFila other = (Matricula2012SaaneeFila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @XmlElement(name="NIV_MOD")
    public String getNivMod() {
        return nivMod;
    }

    
    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    @XmlElement(name="DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }


    @XmlElement(name="NRO_ALUM")
    public Integer getNroAlum() {
        return nroAlum;
    }
    public void setNroAlum(Integer nroAlum) {
        this.nroAlum = nroAlum;
    }


}
