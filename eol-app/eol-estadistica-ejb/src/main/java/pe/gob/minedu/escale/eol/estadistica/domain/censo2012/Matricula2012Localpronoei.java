/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2012_localpronoei")
public class Matricula2012Localpronoei implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P601_A")
    private String p601A;
    @Column(name = "P601_M")
    private String p601M;
    @Column(name = "P602")
    private String p602;
    @Column(name = "P602_ESP")
    private String p602Esp;
    @Column(name = "P603")
    private String p603;
    @Column(name = "P604")
    private String p604;
    @Column(name = "P605")
    private String p605;
    @Column(name = "P606")
    private String p606;
    @Column(name = "P607")
    private String p607;
    @Column(name = "P607_ESP")
    private String p607Esp;
    @Column(name = "P608")
    private String p608;
    @Column(name = "P609")
    private String p609;
    @Column(name = "P610_0")
    private Integer p6100;
    @Column(name = "P610_1")
    private Integer p6101;
    @Column(name = "P610_2")
    private Integer p6102;
    @Column(name = "P610_3")
    private Integer p6103;
    @Column(name = "P611")
    private String p611;
    @Column(name = "P612_1_PIE")
    private String p6121Pie;
    @Column(name = "P612_1_ACE")
    private String p6121Ace;
    @Column(name = "P612_1_OMN")
    private String p6121Omn;
    @Column(name = "P612_1_CAN")
    private String p6121Can;
    @Column(name = "P612_1_LAN")
    private String p6121Lan;
    @Column(name = "P612_1_AVI")
    private String p6121Avi;
    @Column(name = "P612_2_PIE")
    private String p6122Pie;
    @Column(name = "P612_2_ACE")
    private String p6122Ace;
    @Column(name = "P612_2_OMN")
    private String p6122Omn;
    @Column(name = "P612_2_CAN")
    private String p6122Can;
    @Column(name = "P612_2_LAN")
    private String p6122Lan;
    @Column(name = "P612_2_AVI")
    private String p6122Avi;
    @Column(name = "P612_3_PIE")
    private String p6123Pie;
    @Column(name = "P612_3_ACE")
    private String p6123Ace;
    @Column(name = "P612_3_OMN")
    private String p6123Omn;
    @Column(name = "P612_3_CAN")
    private String p6123Can;
    @Column(name = "P612_3_LAN")
    private String p6123Lan;
    @Column(name = "P612_3_AVI")
    private String p6123Avi;
    @Column(name = "P613_1_DIA")
    private String p6131Dia;
    @Column(name = "P613_1_HOR")
    private String p6131Hor;
    @Column(name = "P613_1_MIN")
    private String p6131Min;
    @Column(name = "P613_2_DIA")
    private String p6132Dia;
    @Column(name = "P613_2_HOR")
    private String p6132Hor;
    @Column(name = "P613_2_MIN")
    private String p6132Min;
    @Column(name = "P613_3_DIA")
    private String p6133Dia;
    @Column(name = "P613_3_HOR")
    private String p6133Hor;
    @Column(name = "P613_3_MIN")
    private String p6133Min;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2012Cabecera matricula2012Cabecera;

    public Matricula2012Localpronoei() {
    }

    public Matricula2012Localpronoei(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P601_A")
    public String getP601A() {
        return p601A;
    }

    public void setP601A(String p601A) {
        this.p601A = p601A;
    }

    @XmlElement(name="P601_M")
    public String getP601M() {
        return p601M;
    }

    public void setP601M(String p601M) {
        this.p601M = p601M;
    }

    @XmlElement(name="P602")
    public String getP602() {
        return p602;
    }

    public void setP602(String p602) {
        this.p602 = p602;
    }

    @XmlElement(name="P602_ESP")
    public String getP602Esp() {
        return p602Esp;
    }

    public void setP602Esp(String p602Esp) {
        this.p602Esp = p602Esp;
    }

    @XmlElement(name="P603")
    public String getP603() {
        return p603;
    }

    public void setP603(String p603) {
        this.p603 = p603;
    }

    @XmlElement(name="P604")
    public String getP604() {
        return p604;
    }

    public void setP604(String p604) {
        this.p604 = p604;
    }

    @XmlElement(name="P605")
    public String getP605() {
        return p605;
    }

    public void setP605(String p605) {
        this.p605 = p605;
    }

    @XmlElement(name="P606")
    public String getP606() {
        return p606;
    }

    public void setP606(String p606) {
        this.p606 = p606;
    }

    @XmlElement(name="P607")
    public String getP607() {
        return p607;
    }

    public void setP607(String p607) {
        this.p607 = p607;
    }

    @XmlElement(name="P607_ESP")
    public String getP607Esp() {
        return p607Esp;
    }

    public void setP607Esp(String p607Esp) {
        this.p607Esp = p607Esp;
    }

    @XmlElement(name="P608")
    public String getP608() {
        return p608;
    }

    public void setP608(String p608) {
        this.p608 = p608;
    }

    @XmlElement(name="P609")
    public String getP609() {
        return p609;
    }

    public void setP609(String p609) {
        this.p609 = p609;
    }

    @XmlElement(name="P610_0")
    public Integer getP6100() {
        return p6100;
    }

    public void setP6100(Integer p6100) {
        this.p6100 = p6100;
    }

    @XmlElement(name="P610_1")
    public Integer getP6101() {
        return p6101;
    }

    public void setP6101(Integer p6101) {
        this.p6101 = p6101;
    }

    @XmlElement(name="P610_2")
    public Integer getP6102() {
        return p6102;
    }

    public void setP6102(Integer p6102) {
        this.p6102 = p6102;
    }

    @XmlElement(name="P610_3")
    public Integer getP6103() {
        return p6103;
    }

    public void setP6103(Integer p6103) {
        this.p6103 = p6103;
    }

    @XmlElement(name="P611")
    public String getP611() {
        return p611;
    }

    public void setP611(String p611) {
        this.p611 = p611;
    }

    @XmlElement(name="P612_1_PIE")
    public String getP6121Pie() {
        return p6121Pie;
    }

    public void setP6121Pie(String p6121Pie) {
        this.p6121Pie = p6121Pie;
    }

    @XmlElement(name="P612_1_ACE")
    public String getP6121Ace() {
        return p6121Ace;
    }

    public void setP6121Ace(String p6121Ace) {
        this.p6121Ace = p6121Ace;
    }

    @XmlElement(name="P612_1_OMN")
    public String getP6121Omn() {
        return p6121Omn;
    }

    public void setP6121Omn(String p6121Omn) {
        this.p6121Omn = p6121Omn;
    }

    @XmlElement(name="P612_1_CAN")
    public String getP6121Can() {
        return p6121Can;
    }

    public void setP6121Can(String p6121Can) {
        this.p6121Can = p6121Can;
    }

    @XmlElement(name="P612_1_LAN")
    public String getP6121Lan() {
        return p6121Lan;
    }

    public void setP6121Lan(String p6121Lan) {
        this.p6121Lan = p6121Lan;
    }

    @XmlElement(name="P612_1_AVI")
    public String getP6121Avi() {
        return p6121Avi;
    }

    public void setP6121Avi(String p6121Avi) {
        this.p6121Avi = p6121Avi;
    }

    @XmlElement(name="P612_2_PIE")
    public String getP6122Pie() {
        return p6122Pie;
    }

    public void setP6122Pie(String p6122Pie) {
        this.p6122Pie = p6122Pie;
    }

    @XmlElement(name="P612_2_ACE")
    public String getP6122Ace() {
        return p6122Ace;
    }

    public void setP6122Ace(String p6122Ace) {
        this.p6122Ace = p6122Ace;
    }

    @XmlElement(name="P612_2_OMN")
    public String getP6122Omn() {
        return p6122Omn;
    }

    public void setP6122Omn(String p6122Omn) {
        this.p6122Omn = p6122Omn;
    }

    @XmlElement(name="P612_2_CAN")
    public String getP6122Can() {
        return p6122Can;
    }

    public void setP6122Can(String p6122Can) {
        this.p6122Can = p6122Can;
    }

    @XmlElement(name="P612_2_LAN")
    public String getP6122Lan() {
        return p6122Lan;
    }

    public void setP6122Lan(String p6122Lan) {
        this.p6122Lan = p6122Lan;
    }

    @XmlElement(name="P612_2_AVI")
    public String getP6122Avi() {
        return p6122Avi;
    }

    public void setP6122Avi(String p6122Avi) {
        this.p6122Avi = p6122Avi;
    }

    @XmlElement(name="P612_3_PIE")
    public String getP6123Pie() {
        return p6123Pie;
    }

    public void setP6123Pie(String p6123Pie) {
        this.p6123Pie = p6123Pie;
    }

    @XmlElement(name="P612_3_ACE")
    public String getP6123Ace() {
        return p6123Ace;
    }

    public void setP6123Ace(String p6123Ace) {
        this.p6123Ace = p6123Ace;
    }

    @XmlElement(name="P612_3_OMN")
    public String getP6123Omn() {
        return p6123Omn;
    }

    public void setP6123Omn(String p6123Omn) {
        this.p6123Omn = p6123Omn;
    }

    @XmlElement(name="P612_3_CAN")
    public String getP6123Can() {
        return p6123Can;
    }

    public void setP6123Can(String p6123Can) {
        this.p6123Can = p6123Can;
    }

    @XmlElement(name="P612_3_LAN")
    public String getP6123Lan() {
        return p6123Lan;
    }

    public void setP6123Lan(String p6123Lan) {
        this.p6123Lan = p6123Lan;
    }

    @XmlElement(name="P612_3_AVI")
    public String getP6123Avi() {
        return p6123Avi;
    }

    public void setP6123Avi(String p6123Avi) {
        this.p6123Avi = p6123Avi;
    }

    @XmlElement(name="P613_1_DIA")
    public String getP6131Dia() {
        return p6131Dia;
    }

    public void setP6131Dia(String p6131Dia) {
        this.p6131Dia = p6131Dia;
    }

    @XmlElement(name="P613_1_HOR")
    public String getP6131Hor() {
        return p6131Hor;
    }

    public void setP6131Hor(String p6131Hor) {
        this.p6131Hor = p6131Hor;
    }

    @XmlElement(name="P613_1_MIN")
    public String getP6131Min() {
        return p6131Min;
    }

    public void setP6131Min(String p6131Min) {
        this.p6131Min = p6131Min;
    }

    @XmlElement(name="P613_2_DIA")
    public String getP6132Dia() {
        return p6132Dia;
    }

    public void setP6132Dia(String p6132Dia) {
        this.p6132Dia = p6132Dia;
    }

    @XmlElement(name="P613_2_HOR")
    public String getP6132Hor() {
        return p6132Hor;
    }

    public void setP6132Hor(String p6132Hor) {
        this.p6132Hor = p6132Hor;
    }

    @XmlElement(name="P613_2_MIN")
    public String getP6132Min() {
        return p6132Min;
    }

    public void setP6132Min(String p6132Min) {
        this.p6132Min = p6132Min;
    }

    @XmlElement(name="P613_3_DIA")
    public String getP6133Dia() {
        return p6133Dia;
    }

    public void setP6133Dia(String p6133Dia) {
        this.p6133Dia = p6133Dia;
    }

    @XmlElement(name="P613_3_HOR")
    public String getP6133Hor() {
        return p6133Hor;
    }

    public void setP6133Hor(String p6133Hor) {
        this.p6133Hor = p6133Hor;
    }

    @XmlElement(name="P613_3_MIN")
    public String getP6133Min() {
        return p6133Min;
    }

    public void setP6133Min(String p6133Min) {
        this.p6133Min = p6133Min;
    }

    @XmlTransient
    public Matricula2012Cabecera getMatricula2012Cabecera() {
        return matricula2012Cabecera;
    }

    public void setMatricula2012Cabecera(Matricula2012Cabecera matricula2012Cabecera) {
        this.matricula2012Cabecera = matricula2012Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2012Localpronoei)) {
            return false;
        }
        Matricula2012Localpronoei other = (Matricula2012Localpronoei) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

}
