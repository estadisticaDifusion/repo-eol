package pe.gob.minedu.escale.eol.estadistica.ejb.censo2015;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Local2015Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class Local2015Facade extends AbstractFacade<Local2015Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2015Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Local2015Facade() {
		super(Local2015Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Local2015Cabecera entity) {
		String sql = "UPDATE Local2015Cabecera a SET a.ultimo=false WHERE  a.codlocal=:codlocal";
		Query query = em.createQuery(sql);
		query.setParameter("codlocal", entity.getCodlocal());
		query.executeUpdate();
		/*
		 * OM for (Local2015Sec104 sec104 : entity.getLocal2015Sec104List()) {
		 * sec104.setLocal2015Cabecera(entity); }
		 * 
		 * if (entity.getLocal2015Sec302List() != null) for (Local2015Sec302 sec302 :
		 * entity.getLocal2015Sec302List()) { sec302.setLocal2015Cabecera(entity); }
		 * 
		 * if (entity.getLocal2015Sec304List() != null) for (Local2015Sec304 sec300 :
		 * entity.getLocal2015Sec304List()) { sec300.setLocal2015Cabecera(entity); }
		 * 
		 * if (entity.getLocal2015Sec400List() != null) for (Local2015Sec400 sec400 :
		 * entity.getLocal2015Sec400List()) { sec400.setLocal2015Cabecera(entity); }
		 * 
		 * if (entity.getLocal2015Sec500List() != null) for (Local2015Sec500 sec500 :
		 * entity.getLocal2015Sec500List()) { sec500.setLocal2015Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();
	}

	public Local2015Cabecera findByCodLocal(String codLocal) {
		Query q = em.createQuery("SELECT L FROM Local2015Cabecera L WHERE L.codlocal=:codlocal AND L.ultimo=true");
		q.setParameter("codlocal", codLocal);
		try {
			return (Local2015Cabecera) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
