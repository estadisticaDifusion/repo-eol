/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "matricula2018_eba")
public class Matricula2018Eba implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "PREGUNTA")
    private String pregunta;
    @Column(name = "PERIODO")
    private String periodo;
    @Column(name = "INI_DIA")
    private String iniDia;
    @Column(name = "INI_MES")
    private String iniMes;
    @Column(name = "INI_ANIO")
    private String iniAnio;
    @Column(name = "FIN_DIA")
    private String finDia;
    @Column(name = "FIN_MES")
    private String finMes;
    @Column(name = "FIN_ANIO")
    private String finAnio;
    @Column(name = "TURNO_MA")
    private String turnoMa;
    @Column(name = "TURNO_TA")
    private String turnoTa;
    @Column(name = "TURNO_NO")
    private String turnoNo;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2018Cabecera matricula2018Cabecera;

    public Matricula2018Eba() {
    }

    public Matricula2018Eba(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2018Eba(Long idEnvio, String pregunta, String periodo, String iniMes, String finMes) {
        this.idEnvio = idEnvio;
        this.pregunta = pregunta;
        this.periodo = periodo;
        this.iniMes = iniMes;
        this.finMes = finMes;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "PREGUNTA")
    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @XmlElement(name = "PERIODO")
    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @XmlElement(name = "INI_DIA")
    public String getIniDia() {
        return iniDia;
    }

    public void setIniDia(String iniDia) {
        this.iniDia = iniDia;
    }

    @XmlElement(name = "INI_MES")
    public String getIniMes() {
        return iniMes;
    }

    public void setIniMes(String iniMes) {
        this.iniMes = iniMes;
    }

    @XmlElement(name = "INI_ANIO")
    public String getIniAnio() {
        return iniAnio;
    }

    public void setIniAnio(String iniAnio) {
        this.iniAnio = iniAnio;
    }

    @XmlElement(name = "FIN_DIA")
    public String getFinDia() {
        return finDia;
    }

    public void setFinDia(String finDia) {
        this.finDia = finDia;
    }

    @XmlElement(name = "FIN_MES")
    public String getFinMes() {
        return finMes;
    }

    public void setFinMes(String finMes) {
        this.finMes = finMes;
    }

    @XmlElement(name = "FIN_ANIO")
    public String getFinAnio() {
        return finAnio;
    }

    public void setFinAnio(String finAnio) {
        this.finAnio = finAnio;
    }

    @XmlElement(name = "TURNO_MA")
    public String getTurnoMa() {
        return turnoMa;
    }

    public void setTurnoMa(String turnoMa) {
        this.turnoMa = turnoMa;
    }

    @XmlElement(name = "TURNO_TA")
    public String getTurnoTa() {
        return turnoTa;
    }

    public void setTurnoTa(String turnoTa) {
        this.turnoTa = turnoTa;
    }

    @XmlElement(name = "TURNO_NO")
    public String getTurnoNo() {
        return turnoNo;
    }

    public void setTurnoNo(String turnoNo) {
        this.turnoNo = turnoNo;
    }

    @XmlTransient
    public Matricula2018Cabecera getMatricula2018Cabecera() {
        return matricula2018Cabecera;
    }

    public void setMatricula2018Cabecera(Matricula2018Cabecera matricula2018Cabecera) {
        this.matricula2018Cabecera = matricula2018Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2018Eba)) {
            return false;
        }
        Matricula2018Eba other = (Matricula2018Eba) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Matricula2018Eba[idEnvio=" + idEnvio + "]";
    }

}
