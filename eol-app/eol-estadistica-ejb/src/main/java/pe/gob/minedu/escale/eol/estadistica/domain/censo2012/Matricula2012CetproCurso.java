/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2012_cetpro_curso")
public class Matricula2012CetproCurso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NRO")
    private String nro;
    @Basic(optional = false)
    @Column(name = "TIPDATO")
    private String tipdato;
    @Column(name = "R1")
    private String r1;
    @Column(name = "R2")
    private String r2;
    @Column(name = "R3")
    private String r3;
    @Column(name = "DS")
    private Integer ds;
    @Column(name = "NHL")
    private Integer nhl;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2012Cabecera matricula2012Cabecera;

    public Matricula2012CetproCurso() {
    }

    public Matricula2012CetproCurso(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2012CetproCurso(Long idEnvio, String nro, String tipdato) {
        this.idEnvio = idEnvio;
        this.nro = nro;
        this.tipdato = tipdato;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    @XmlElement(name="TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name="R1")
    public String getR1() {
        return r1;
    }

    public void setR1(String r1) {
        this.r1 = r1;
    }

    @XmlElement(name="R2")
    public String getR2() {
        return r2;
    }

    public void setR2(String r2) {
        this.r2 = r2;
    }

    @XmlElement(name="R3")
    public String getR3() {
        return r3;
    }

    public void setR3(String r3) {
        this.r3 = r3;
    }

    @XmlElement(name="DS")
    public Integer getDs() {
        return ds;
    }

    public void setDs(Integer ds) {
        this.ds = ds;
    }

    @XmlElement(name="NHL")
    public Integer getNhl() {
        return nhl;
    }

    public void setNhl(Integer nhl) {
        this.nhl = nhl;
    }

    @XmlTransient
    public Matricula2012Cabecera getMatricula2012Cabecera() {
        return matricula2012Cabecera;
    }

    public void setMatricula2012Cabecera(Matricula2012Cabecera matricula2012Cabecera) {
        this.matricula2012Cabecera = matricula2012Cabecera;
    }

}
