/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "matricula2017_matricula_fila")
public class Matricula2017MatriculaFila implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "TIPDATO")
    private String tipdato;
    @Basic(optional = false)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "TOTAL_1")
    private Integer total1;
    @Column(name = "TOTAL_2")
    private Integer total2;
    @Column(name = "TOTAL_3")
    private Integer total3;
    @Column(name = "DATO01")
    private Integer dato01;
    @Column(name = "DATO02")
    private Integer dato02;
    @Column(name = "DATO03")
    private Integer dato03;
    @Column(name = "DATO04")
    private Integer dato04;
    @Column(name = "DATO05")
    private Integer dato05;
    @Column(name = "DATO06")
    private Integer dato06;
    @Column(name = "DATO07")
    private Integer dato07;
    @Column(name = "DATO08")
    private Integer dato08;
    @Column(name = "DATO09")
    private Integer dato09;
    @Column(name = "DATO10")
    private Integer dato10;
    @Column(name = "DATO11")
    private Integer dato11;
    @Column(name = "DATO12")
    private Integer dato12;
    @Column(name = "DATO13")
    private Integer dato13;
    @Column(name = "DATO14")
    private Integer dato14;
    @Column(name = "DATO15")
    private Integer dato15;
    @Column(name = "DATO16")
    private Integer dato16;
    @Column(name = "DATO17")
    private Integer dato17;
    @Column(name = "DATO18")
    private Integer dato18;
    @Column(name = "DATO19")
    private Integer dato19;
    @Column(name = "DATO20")
    private Integer dato20;
    @Column(name = "DATO21")
    private Integer dato21;
    @Column(name = "DATO22")
    private Integer dato22;
    @Column(name = "DATO23")
    private Integer dato23;
    @Column(name = "DATO24")
    private Integer dato24;
    @Column(name = "DATO25")
    private Integer dato25;
    @Column(name = "DATO26")
    private Integer dato26;
    @Column(name = "DATO27")
    private Integer dato27;
    @Column(name = "DATO28")
    private Integer dato28;
    @Column(name = "DATO29")
    private Integer dato29;
    @Column(name = "DATO30")
    private Integer dato30;
    
    @JoinColumn(name = "DETALLE_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Matricula2017Matricula matricula2017Matricula;

    public Matricula2017MatriculaFila() {
    }

    public Matricula2017MatriculaFila(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2017MatriculaFila(Long idEnvio, String tipdato, String descrip) {
        this.idEnvio = idEnvio;
        this.tipdato = tipdato;
        this.descrip = descrip;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "TIPDATO")
    public String getTipdato() {
        return tipdato;
    }

    public void setTipdato(String tipdato) {
        this.tipdato = tipdato;
    }

    @XmlElement(name = "DESCRIP")
    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    @XmlElement(name = "TOTAL_1")
    public Integer getTotal1() {
        return total1;
    }

    public void setTotal1(Integer total1) {
        this.total1 = total1;
    }

    @XmlElement(name = "TOTAL_2")
    public Integer getTotal2() {
        return total2;
    }

    public void setTotal2(Integer total2) {
        this.total2 = total2;
    }

    @XmlElement(name = "DATO01")
    public Integer getDato01() {
        return dato01;
    }

    public void setDato01(Integer dato01) {
        this.dato01 = dato01;
    }

    @XmlElement(name = "DATO02")
    public Integer getDato02() {
        return dato02;
    }

    public void setDato02(Integer dato02) {
        this.dato02 = dato02;
    }

    @XmlElement(name = "DATO03")
    public Integer getDato03() {
        return dato03;
    }

    public void setDato03(Integer dato03) {
        this.dato03 = dato03;
    }

    @XmlElement(name = "DATO04")
    public Integer getDato04() {
        return dato04;
    }

    public void setDato04(Integer dato04) {
        this.dato04 = dato04;
    }

    @XmlElement(name = "DATO05")
    public Integer getDato05() {
        return dato05;
    }

    public void setDato05(Integer dato05) {
        this.dato05 = dato05;
    }

    @XmlElement(name = "DATO06")
    public Integer getDato06() {
        return dato06;
    }

    public void setDato06(Integer dato06) {
        this.dato06 = dato06;
    }

    @XmlElement(name = "DATO07")
    public Integer getDato07() {
        return dato07;
    }

    public void setDato07(Integer dato07) {
        this.dato07 = dato07;
    }

    @XmlElement(name = "DATO08")
    public Integer getDato08() {
        return dato08;
    }

    public void setDato08(Integer dato08) {
        this.dato08 = dato08;
    }

    @XmlElement(name = "DATO09")
    public Integer getDato09() {
        return dato09;
    }

    public void setDato09(Integer dato09) {
        this.dato09 = dato09;
    }

    @XmlElement(name = "DATO10")
    public Integer getDato10() {
        return dato10;
    }

    public void setDato10(Integer dato10) {
        this.dato10 = dato10;
    }

    @XmlElement(name = "DATO11")
    public Integer getDato11() {
        return dato11;
    }

    public void setDato11(Integer dato11) {
        this.dato11 = dato11;
    }

    @XmlElement(name = "DATO12")
    public Integer getDato12() {
        return dato12;
    }

    public void setDato12(Integer dato12) {
        this.dato12 = dato12;
    }

    @XmlElement(name = "DATO13")
    public Integer getDato13() {
        return dato13;
    }

    public void setDato13(Integer dato13) {
        this.dato13 = dato13;
    }

    @XmlElement(name = "DATO14")
    public Integer getDato14() {
        return dato14;
    }

    public void setDato14(Integer dato14) {
        this.dato14 = dato14;
    }

    @XmlElement(name = "DATO15")
    public Integer getDato15() {
        return dato15;
    }

    public void setDato15(Integer dato15) {
        this.dato15 = dato15;
    }

    @XmlElement(name = "DATO16")
    public Integer getDato16() {
        return dato16;
    }

    public void setDato16(Integer dato16) {
        this.dato16 = dato16;
    }

    @XmlElement(name = "DATO17")
    public Integer getDato17() {
        return dato17;
    }

    public void setDato17(Integer dato17) {
        this.dato17 = dato17;
    }

    @XmlElement(name = "DATO18")
    public Integer getDato18() {
        return dato18;
    }

    public void setDato18(Integer dato18) {
        this.dato18 = dato18;
    }

    @XmlElement(name = "DATO19")
    public Integer getDato19() {
        return dato19;
    }

    public void setDato19(Integer dato19) {
        this.dato19 = dato19;
    }

    @XmlElement(name = "DATO20")
    public Integer getDato20() {
        return dato20;
    }

    public void setDato20(Integer dato20) {
        this.dato20 = dato20;
    }

    @XmlElement(name = "DATO21")
    public Integer getDato21() {
        return dato21;
    }

    public void setDato21(Integer dato21) {
        this.dato21 = dato21;
    }

    @XmlElement(name = "DATO22")
    public Integer getDato22() {
        return dato22;
    }

    public void setDato22(Integer dato22) {
        this.dato22 = dato22;
    }

    @XmlElement(name = "DATO23")
    public Integer getDato23() {
        return dato23;
    }

    public void setDato23(Integer dato23) {
        this.dato23 = dato23;
    }

    @XmlElement(name = "DATO24")
    public Integer getDato24() {
        return dato24;
    }

    public void setDato24(Integer dato24) {
        this.dato24 = dato24;
    }

    @XmlElement(name = "DATO25")
    public Integer getDato25() {
        return dato25;
    }

    public void setDato25(Integer dato25) {
        this.dato25 = dato25;
    }

    @XmlElement(name = "DATO26")
    public Integer getDato26() {
        return dato26;
    }

    public void setDato26(Integer dato26) {
        this.dato26 = dato26;
    }

    @XmlElement(name = "DATO27")
    public Integer getDato27() {
        return dato27;
    }

    public void setDato27(Integer dato27) {
        this.dato27 = dato27;
    }

    @XmlElement(name = "DATO28")
    public Integer getDato28() {
        return dato28;
    }

    public void setDato28(Integer dato28) {
        this.dato28 = dato28;
    }

    @XmlElement(name = "DATO29")
    public Integer getDato29() {
        return dato29;
    }

    public void setDato29(Integer dato29) {
        this.dato29 = dato29;
    }

    @XmlElement(name = "DATO30")
    public Integer getDato30() {
        return dato30;
    }

    public void setDato30(Integer dato30) {
        this.dato30 = dato30;
    }

    @XmlElement(name = "TOTAL_3")
    public Integer getTotal3() {
        return total3;
    }

    public void setTotal3(Integer total3) {
        this.total3 = total3;
    }

    @XmlTransient
    public Matricula2017Matricula getMatricula2017Matricula() {
        return matricula2017Matricula;
    }

    public void setMatricula2017Matricula(Matricula2017Matricula matricula2017Matricula) {
        this.matricula2017Matricula = matricula2017Matricula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2017MatriculaFila)) {
            return false;
        }
        Matricula2017MatriculaFila other = (Matricula2017MatriculaFila) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Matricula2017MatriculaFila[idEnvio=" + idEnvio + "]";
    }
}
