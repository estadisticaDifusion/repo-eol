/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "resultado2014_detalle")
public class Resultado2014Detalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Transient
    //@Column(name = "CUADRO")
    private String cuadro;

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Resultado2014Cabecera cedula;
    /*
    @OneToMany(mappedBy = "detalle", fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    private List<Resultado2014Fila> filas;
*/
    public Resultado2014Detalle() {
    }

    public Resultado2014Detalle(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Resultado2014Detalle(Long idEnvio, String cuadro) {
        this.idEnvio = idEnvio;
        this.cuadro = cuadro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }


    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlTransient
    public Resultado2014Cabecera getCedula() {
        return cedula;
    }

    public void setCedula(Resultado2014Cabecera cedula) {
        this.cedula = cedula;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Resultado2014Detalle)) {
            return false;
        }
        Resultado2014Detalle other = (Resultado2014Detalle) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
    /*
    @XmlElement(name="FILAS")
    public List<Resultado2014Fila> getFilas() {
        return filas;
    }
   
    public void setFilas(List<Resultado2014Fila> filas) {
        this.filas = filas;
    }*/
}
