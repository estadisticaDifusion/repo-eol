/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2017_cabecera")
public class Matricula2017Cabecera implements Serializable {

    private static final long serialVersionUID = 1L;

    public final static String CEDULA_01A = "c01a";
    public final static String CEDULA_02A = "c02a";
    public final static String CEDULA_03AP = "c03ap";
    public final static String CEDULA_03AS = "c03as";
    public final static String CEDULA_04A = "c04a";
    public final static String CEDULA_04AI = "c04ai";
    public final static String CEDULA_04AA = "c04aa";
    public final static String CEDULA_05A = "c05a";
    public final static String CEDULA_06A = "c06a";
    public final static String CEDULA_07A = "c07a";
    public final static String CEDULA_08AI = "c08ai";
    public final static String CEDULA_08AP = "c08ap";
    public final static String CEDULA_09A = "c09a";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "NROCED")
    private String nroced;
    @Column(name = "COD_MOD")
    private String codMod;
    @Column(name = "ANEXO")
    private String anexo;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "CEN_EDU")
    private String cenEdu;
    @Column(name = "NIV_MOD")
    private String nivMod;
    @Column(name = "DNI_CORD")
    private String dniCord;
    @Column(name = "TIPOPROG")
    private String tipoprog;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "LOCALIDAD")
    private String localidad;
    @Column(name = "EGESTORA")
    private String egestora;
    @Column(name = "TIPOFINA_1")
    private String tipofina1;
    @Column(name = "TIPOFINA_2")
    private String tipofina2;
    @Column(name = "TIPOFINA_3")
    private String tipofina3;
    @Column(name = "TIPOFINA_4")
    private String tipofina4;
    @Column(name = "NRORD_CRE")
    private String nrordCre;
    @Column(name = "FECRES_C_D")
    private String fecresCD;
    @Column(name = "FECRES_C_M")
    private String fecresCM;
    @Column(name = "FECRES_C_A")
    private String fecresCA;
    @Column(name = "NRORD_REN")
    private String nrordRen;
    @Column(name = "FECRES_R_D")
    private String fecresRD;
    @Column(name = "FECRES_R_M")
    private String fecresRM;
    @Column(name = "FECRES_R_A")
    private String fecresRA;
    @Column(name = "FORMATEN")
    private String formaten;
    @Column(name = "MODATEN")
    private String modaten;
    @Column(name = "METATEN")
    private String metaten;
    @Column(name = "GESTION")
    private String gestion;
    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "TIPOIESUP")
    private String tipoiesup;
    @Column(name = "P101_DD")
    private String p101Dd;
    @Column(name = "P101_MM")
    private String p101Mm;
    @Column(name = "P101_C2_1")
    private String p101C21;
    @Column(name = "P101_C2_2")
    private String p101C22;
    @Column(name = "P101_C2_3")
    private String p101C23;
    @Column(name = "P101_C2_4")
    private String p101C24;
    @Column(name = "P102_DD")
    private String p102Dd;
    @Column(name = "P102_MM")
    private String p102Mm;
    @Column(name = "P101_C5A")
    private String p101C5a;
    @Column(name = "P101_C5A_I")
    private String p101C5aI;
    @Column(name = "P101_C5A_P")
    private String p101C5aP;
    @Column(name = "P101_C5A_S")
    private String p101C5aS;
    @Column(name = "P102_C5A")
    private String p102C5a;
    @Column(name = "P102_C5A_1")
    private String p102C5a1;
    @Column(name = "P103_MHI")
    private String p103Mhi;
    @Column(name = "P103_MMI")
    private String p103Mmi;
    @Column(name = "P103_MHT")
    private String p103Mht;
    @Column(name = "P103_MMT")
    private String p103Mmt;
    @Column(name = "P103_THI")
    private String p103Thi;
    @Column(name = "P103_TMI")
    private String p103Tmi;
    @Column(name = "P103_THT")
    private String p103Tht;
    @Column(name = "P103_TMT")
    private String p103Tmt;
    @Column(name = "P104_C13P")
    private String p104C13p;
    @Column(name = "P104_C3AS")
    private String p104C3as;
    @Column(name = "P105_C13PS")
    private String p105C13ps;
    @Column(name = "P105_C2")
    private String p105C2;
    @Column(name = "P106_C13PS")
    private String p106C13ps;
    @Column(name = "P106_C2_LU")
    private String p106C2Lu;
    @Column(name = "P106_C2_MA")
    private String p106C2Ma;
    @Column(name = "P106_C2_MI")
    private String p106C2Mi;
    @Column(name = "P106_C2_JU")
    private String p106C2Ju;
    @Column(name = "P106_C2_VI")
    private String p106C2Vi;
    @Column(name = "P106_C2_SA")
    private String p106C2Sa;
    @Column(name = "SERV_NE_1")
    private String servNe1;
    @Column(name = "SERV_NE_2")
    private String servNe2;
    @Column(name = "SERV_NE_3")
    private String servNe3;
    @Column(name = "SERV_NE_4")
    private String servNe4;
    @Column(name = "SERV_NE_5")
    private String servNe5;
    @Column(name = "SERV_NE_6")
    private String servNe6;
    @Column(name = "SERV_NE_6E")
    private String servNe6e;
    @Column(name = "SERV_NE_7")
    private String servNe7;
    @Column(name = "P107_C13PS")
    private String p107C13ps;
    @Column(name = "P108_IPS1")
    private String p108Ips1;
    @Column(name = "P108_IPS2")
    private String p108Ips2;
    @Column(name = "P108_IPS3")
    private String p108Ips3;
    @Column(name = "P108_IPS4")
    private String p108Ips4;
    @Column(name = "P108_IPS5")
    private String p108Ips5;
    @Column(name = "P108_IPS6")
    private String p108Ips6;
    @Column(name = "P108_IPS7")
    private String p108Ips7;
    @Column(name = "P109_C13PS")
    private String p109C13ps;
    @Column(name = "P110_C13PS")
    private String p110C13ps;
    @Column(name = "P111_C13PS")
    private String p111C13ps;
    @Column(name = "P112_C13S")
    private String p112C13s;
    @Column(name = "CENT_APP")
    private String centApp;
    @Column(name = "PRAC_UOI")
    private String pracUoi;
    @Column(name = "PRAC_UOIQ")
    private Integer pracUoiq;
    @Column(name = "ENZ_LEXT")
    private String enzLext;
    @Column(name = "ESP_LEXTE1")
    private String espLexte1;
    @Column(name = "ESP_LEXTE2")
    private String espLexte2;
    @Column(name = "PEIB_1")
    private String peib1;
    @Column(name = "PEIB_1EST")
    private String peib1est;
    @Column(name = "PEIB_1_RES")
    private String peib1Res;
    @Column(name = "PEIB_2")
    private String peib2;
    @Column(name = "PEIB_3")
    private String peib3;
    @Column(name = "PEIB_3_QN")
    private Integer peib3Qn;
    @Column(name = "PEIB_4_C2A")
    private String peib4C2a;
    @Column(name = "PEIB_5")
    private String peib5;
    @Column(name = "PEIB_5_1")
    private String peib51;
    @Column(name = "PEIB_5_1AC")
    private String peib51ac;
    @Column(name = "PEIB_5_2")
    private String peib52;
    @Column(name = "PEIB_5_2AC")
    private String peib52ac;
    @Column(name = "PEIB_5_3")
    private String peib53;
    @Column(name = "PEIB_5_3AC")
    private String peib53ac;
    @Column(name = "PEIB_5_4")
    private String peib54;
    @Column(name = "PEIB_5_4AC")
    private String peib54ac;
    @Column(name = "PEIB_5_5")
    private String peib55;
    @Column(name = "PEIB_5_5AC")
    private String peib55ac;
    @Column(name = "PEIB_5_6")
    private String peib56;
    @Column(name = "PEIB_5_6AC")
    private String peib56ac;
    @Column(name = "P1181_C3S")
    private String p1181C3s;
    @Column(name = "P1181_C3SQ")
    private Integer p1181C3sq;
    @Column(name = "P1182_3S11")
    private String p11823s11;
    @Column(name = "P1182_3S12")
    private String p11823s12;
    @Column(name = "P1182_3S13")
    private String p11823s13;
    @Column(name = "P1182_3S14")
    private String p11823s14;
    @Column(name = "P1182_3S21")
    private String p11823s21;
    @Column(name = "P1182_3S22")
    private String p11823s22;
    @Column(name = "P1182_3S23")
    private String p11823s23;
    @Column(name = "P1182_3S24")
    private String p11823s24;
    @Column(name = "P401_C12")
    private String p401C12;
    @Column(name = "P402_TOT")
    private Integer p402Tot;
    @Column(name = "P402_EDA4")
    private Integer p402Eda4;
    @Column(name = "P402_EDA5")
    private Integer p402Eda5;
    @Column(name = "P402_ANIO")
    private String p402Anio;
    @Column(name = "P402_MES")
    private String p402Mes;
    @Column(name = "P403_C12")
    private String p403C12;
    @Column(name = "P404_TOT")
    private Integer p404Tot;
    @Column(name = "P404_EDA4")
    private Integer p404Eda4;
    @Column(name = "P404_EDA5")
    private Integer p404Eda5;
    @Column(name = "P404_ANIO")
    private String p404Anio;
    @Column(name = "P404_MES")
    private String p404Mes;
    @Column(name = "P405_C12")
    private String p405C12;
    @Column(name = "P405_I")
    private String p405I;
    @Column(name = "P405_II")
    private String p405Ii;
    @Column(name = "P502_C1")
    private String p502C1;
    @Column(name = "P502_C1_D")
    private String p502C1D;
    @Column(name = "P502_C1_M")
    private String p502C1M;
    @Column(name = "P502_C1_A")
    private String p502C1A;
    @Column(name = "P401_C3PS")
    private String p401C3ps;
    @Column(name = "P403_C3PS")
    private String p403C3ps;
    @Column(name = "P4051_C3PS")
    private String p4051C3ps;
    @Column(name = "RUTA_SBI")
    private String rutaSbi;
    @Column(name = "RUTA_SBI_1")
    private Integer rutaSbi1;
    @Column(name = "RUTA_SBI_2")
    private String rutaSbi2;
    @Column(name = "RUTA_SLA")
    private String rutaSla;
    @Column(name = "RUTA_SLA_1")
    private Integer rutaSla1;
    @Column(name = "RUTA_SLA_2")
    private String rutaSla2;
    @Column(name = "P6012")
    private String p6012;
    @Column(name = "P6012_QH")
    private Integer p6012Qh;
    @Column(name = "P6012_QM")
    private Integer p6012Qm;
    @Column(name = "P6013")
    private String p6013;
    @Column(name = "P6014")
    private String p6014;
    @Column(name = "P6015")
    private String p6015;
    @Column(name = "P6021_1")
    private String p60211;
    @Column(name = "P6021_1Q")
    private Integer p60211q;
    @Column(name = "P6021_2")
    private String p60212;
    @Column(name = "P6021_2Q")
    private Integer p60212q;
    @Column(name = "P6041")
    private String p6041;
    @Column(name = "P6041_QH")
    private Integer p6041Qh;
    @Column(name = "P6041_QD")
    private Integer p6041Qd;
    @Column(name = "P6042")
    private String p6042;
    @Column(name = "P6042_Q")
    private Integer p6042Q;
    @Column(name = "P6043")
    private String p6043;
    @Column(name = "P6051")
    private String p6051;
    @Column(name = "P6052_1")
    private String p60521;
    @Column(name = "P6052_2")
    private String p60522;
    @Column(name = "P6052_3")
    private String p60523;
    @Column(name = "P6052_4")
    private String p60524;
    @Column(name = "P6052_5")
    private String p60525;
    @Column(name = "P6052_6")
    private String p60526;
    @Column(name = "P6052_7")
    private String p60527;
    @Column(name = "P6052_7ESP")
    private String p60527esp;
    @Column(name = "P102_C4")
    private String p102C4;
    @Column(name = "P103_C4")
    private Integer p103C4;
    @Column(name = "P104_C4_LU")
    private Integer p104C4Lu;
    @Column(name = "P104_C4_MA")
    private Integer p104C4Ma;
    @Column(name = "P104_C4_MI")
    private Integer p104C4Mi;
    @Column(name = "P104_C4_JU")
    private Integer p104C4Ju;
    @Column(name = "P104_C4_VI")
    private Integer p104C4Vi;
    @Column(name = "P104_C4_SA")
    private Integer p104C4Sa;
    @Column(name = "P104_C4_DO")
    private Integer p104C4Do;
    @Column(name = "P105_C4A_1")
    private String p105C4a1;
    @Column(name = "P105_C4A_2")
    private String p105C4a2;
    @Column(name = "P105_C4A_3")
    private String p105C4a3;
    @Column(name = "P105_C4A_4")
    private String p105C4a4;
    @Column(name = "P105_C4A_5")
    private String p105C4a5;
    @Column(name = "P105_C4A_E")
    private String p105C4aE;
    @Column(name = "P105_6_C41")
    private String p1056C41;
    @Column(name = "P105_6_C42")
    private String p1056C42;
    @Column(name = "P105_6_C43")
    private String p1056C43;
    @Column(name = "P105_6_C44")
    private String p1056C44;
    @Column(name = "P105_6_C45")
    private String p1056C45;
    @Column(name = "P105_6_C46")
    private String p1056C46;
    @Column(name = "P105_6_C47")
    private String p1056C47;
    @Column(name = "P105_6_C48")
    private String p1056C48;
    @Column(name = "P105_6_C4E")
    private String p1056C4e;
    @Column(name = "P106_C4AI")
    private String p106C4ai;
    @Column(name = "P107_C4AI")
    private Integer p107C4ai;
    @Column(name = "P108_C4_LU")
    private Integer p108C4Lu;
    @Column(name = "P108_C4_MA")
    private Integer p108C4Ma;
    @Column(name = "P108_C4_MI")
    private Integer p108C4Mi;
    @Column(name = "P108_C4_JU")
    private Integer p108C4Ju;
    @Column(name = "P108_C4_VI")
    private Integer p108C4Vi;
    @Column(name = "P108_C4_SA")
    private Integer p108C4Sa;
    @Column(name = "P108_C4_DO")
    private Integer p108C4Do;
    @Column(name = "P107_109")
    private String p107109;
    @Column(name = "P108_110_1")
    private String p1081101;
    @Column(name = "P108_110_2")
    private String p1081102;
    @Column(name = "P108_110_3")
    private String p1081103;
    @Column(name = "P108_110_4")
    private String p1081104;
    @Column(name = "P108_110E")
    private String p108110e;
    @Column(name = "P109_111_1")
    private String p1091111;
    @Column(name = "P109_111_2")
    private String p1091112;
    @Column(name = "P109_111_3")
    private String p1091113;
    @Column(name = "P109_111_4")
    private String p1091114;
    @Column(name = "P109_111E")
    private String p109111e;
    @Column(name = "P110_112")
    private String p110112;
    @Column(name = "P110_112E")
    private String p110112e;
    @Column(name = "P401_C4")
    private String p401C4;
    @Column(name = "P405_C4")
    private String p405C4;
    @Column(name = "P406_C4_1")
    private String p406C41;
    @Column(name = "P406_C4_2")
    private String p406C42;
    @Column(name = "P406_C4_3")
    private String p406C43;
    @Column(name = "P406_C4_4")
    private String p406C44;
    @Column(name = "P406_C4_5")
    private String p406C45;
    @Column(name = "P406_C4_6")
    private String p406C46;
    @Column(name = "P406_C4_7")
    private String p406C47;
    @Column(name = "P406_C4_8")
    private String p406C48;
    @Column(name = "P406_C4_9")
    private String p406C49;
    @Column(name = "P406_C4_9E")
    private String p406C49e;
    @Column(name = "P406_C4_10")
    private String p406C410;
    @Column(name = "P406_C4_10E")
    private String p406C410e;
    @Column(name = "P406_C4_11")
    private String p406C411;
    @Column(name = "P407_C4_11")
    private String p407C411;
    @Column(name = "P407_C4_12")
    private Integer p407C412;
    @Column(name = "P407_C4_21")
    private String p407C421;
    @Column(name = "P407_C4_22")
    private Integer p407C422;
    @Column(name = "P407_C4_31")
    private String p407C431;
    @Column(name = "P407_C4_32")
    private Integer p407C432;
    @Column(name = "P407_C4_41")
    private String p407C441;
    @Column(name = "P408_C4_1")
    private Integer p408C41;
    @Column(name = "P408_C4_2")
    private Integer p408C42;
    @Column(name = "P408_C4_3")
    private Integer p408C43;
    @Column(name = "P409_C4_1")
    private String p409C41;
    @Column(name = "P409_C4_2")
    private String p409C42;
    @Column(name = "P409_C4_3")
    private String p409C43;
    @Column(name = "P409_C4_4")
    private String p409C44;
    @Column(name = "P409_C4_5")
    private String p409C45;
    @Column(name = "TDOCENTES")
    private Integer tdocentes;
    @Column(name = "TDOCAULA")
    private Integer tdocaula;
    @Column(name = "TAUXILIAR")
    private Integer tauxiliar;
    @Column(name = "TADMINIST")
    private Integer tadminist;
    @Column(name = "ANOTACIONES")
    private String anotaciones;
    @Column(name = "APATERNO")
    private String apaterno;
    @Column(name = "AMATERNO")
    private String amaterno;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "FUENTE")
    private String fuente;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;
    
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2017Saanee> matricula2017SaaneeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2017Carreras> matricula2017CarrerasList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2017Personal> matricula2017PersonalList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2017Localpronoei> matricula2017LocalpronoeiList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2017Cabecera", fetch = FetchType.EAGER)
    private List<Matricula2017Eba> matricula2017EbaList;
    
    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String, Matricula2017Seccion> detalleSeccion;
    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String, Matricula2017Recursos> detalleRecursos;
    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2017Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String, Matricula2017Matricula> detalleMatricula;
    */
    
    @Transient
    private long token;
    @Transient
    private String msg;
    @Transient
    private String estadoRpt;
    @Transient
    private String fechasigied;

    public Matricula2017Cabecera() {
    }

    public Matricula2017Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2017Cabecera(Long idEnvio, String nroced, String codMod, String anexo, String codlocal, String cenEdu, String nivMod) {
        this.idEnvio = idEnvio;
        this.nroced = nroced;
        this.codMod = codMod;
        this.anexo = anexo;
        this.codlocal = codlocal;
        this.cenEdu = cenEdu;
        this.nivMod = nivMod;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "AMATERNO")
    public String getAmaterno() {
        return this.amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }

    @XmlElement(name = "ANEXO")
    public String getAnexo() {
        return this.anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    @XmlElement(name = "ANOTACIONES")
    public String getAnotaciones() {
        return this.anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    @XmlElement(name = "APATERNO")
    public String getApaterno() {
        return this.apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    @XmlElement(name = "CEN_EDU")
    public String getCenEdu() {
        return this.cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    @XmlElement(name = "CENT_APP")
    public String getCentApp() {
        return this.centApp;
    }

    public void setCentApp(String centApp) {
        this.centApp = centApp;
    }

    @XmlElement(name = "COD_MOD")
    public String getCodMod() {
        return this.codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return this.codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "CODUGEL")
    public String getCodugel() {
        return this.codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name = "DISTRITO")
    public String getDistrito() {
        return this.distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name = "DNI_CORD")
    public String getDniCord() {
        return this.dniCord;
    }

    public void setDniCord(String dniCord) {
        this.dniCord = dniCord;
    }

    @XmlElement(name = "EGESTORA")
    public String getEgestora() {
        return this.egestora;
    }

    public void setEgestora(String egestora) {
        this.egestora = egestora;
    }

    @XmlElement(name = "ENZ_LEXT")
    public String getEnzLext() {
        return this.enzLext;
    }

    public void setEnzLext(String enzLext) {
        this.enzLext = enzLext;
    }

    @XmlElement(name = "ESP_LEXTE1")
    public String getEspLexte1() {
        return this.espLexte1;
    }

    public void setEspLexte1(String espLexte1) {
        this.espLexte1 = espLexte1;
    }

    @XmlElement(name = "ESP_LEXTE2")
    public String getEspLexte2() {
        return this.espLexte2;
    }

    public void setEspLexte2(String espLexte2) {
        this.espLexte2 = espLexte2;
    }

    @XmlElement(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return this.fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @XmlElement(name = "FECRES_C_A")
    public String getFecresCA() {
        return this.fecresCA;
    }

    public void setFecresCA(String fecresCA) {
        this.fecresCA = fecresCA;
    }

    @XmlElement(name = "FECRES_C_D")
    public String getFecresCD() {
        return this.fecresCD;
    }

    public void setFecresCD(String fecresCD) {
        this.fecresCD = fecresCD;
    }

    @XmlElement(name = "FECRES_C_M")
    public String getFecresCM() {
        return this.fecresCM;
    }

    public void setFecresCM(String fecresCM) {
        this.fecresCM = fecresCM;
    }

    @XmlElement(name = "FECRES_R_A")
    public String getFecresRA() {
        return this.fecresRA;
    }

    public void setFecresRA(String fecresRA) {
        this.fecresRA = fecresRA;
    }

    @XmlElement(name = "FECRES_R_D")
    public String getFecresRD() {
        return this.fecresRD;
    }

    public void setFecresRD(String fecresRD) {
        this.fecresRD = fecresRD;
    }

    @XmlElement(name = "FECRES_R_M")
    public String getFecresRM() {
        return this.fecresRM;
    }

    public void setFecresRM(String fecresRM) {
        this.fecresRM = fecresRM;
    }

    @XmlElement(name = "FORMATEN")
    public String getFormaten() {
        return this.formaten;
    }

    public void setFormaten(String formaten) {
        this.formaten = formaten;
    }

    @XmlElement(name = "FUENTE")
    public String getFuente() {
        return this.fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    @XmlElement(name = "GESTION")
    public String getGestion() {
        return this.gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name = "LOCALIDAD")
    public String getLocalidad() {
        return this.localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @XmlElement(name = "METATEN")
    public String getMetaten() {
        return this.metaten;
    }

    public void setMetaten(String metaten) {
        this.metaten = metaten;
    }

    @XmlElement(name = "MODATEN")
    public String getModaten() {
        return this.modaten;
    }

    public void setModaten(String modaten) {
        this.modaten = modaten;
    }

    @XmlElement(name = "NIV_MOD")
    public String getNivMod() {
        return this.nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    @XmlElement(name = "NOMBRES")
    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @XmlElement(name = "NROCED")
    public String getNroced() {
        return this.nroced;
    }

    public void setNroced(String nroced) {
        this.nroced = nroced;
    }

    @XmlElement(name = "NRORD_CRE")
    public String getNrordCre() {
        return this.nrordCre;
    }

    public void setNrordCre(String nrordCre) {
        this.nrordCre = nrordCre;
    }

    @XmlElement(name = "NRORD_REN")
    public String getNrordRen() {
        return this.nrordRen;
    }

    public void setNrordRen(String nrordRen) {
        this.nrordRen = nrordRen;
    }

    @XmlElement(name = "P101_C2_1")
    public String getP101C21() {
        return this.p101C21;
    }

    public void setP101C21(String p101C21) {
        this.p101C21 = p101C21;
    }

    @XmlElement(name = "P101_C2_2")
    public String getP101C22() {
        return this.p101C22;
    }

    public void setP101C22(String p101C22) {
        this.p101C22 = p101C22;
    }

    @XmlElement(name = "P101_C2_3")
    public String getP101C23() {
        return this.p101C23;
    }

    public void setP101C23(String p101C23) {
        this.p101C23 = p101C23;
    }

    @XmlElement(name = "P101_C2_4")
    public String getP101C24() {
        return this.p101C24;
    }

    public void setP101C24(String p101C24) {
        this.p101C24 = p101C24;
    }

    @XmlElement(name = "P101_C5A")
    public String getP101C5a() {
        return this.p101C5a;
    }

    public void setP101C5a(String p101C5a) {
        this.p101C5a = p101C5a;
    }

    @XmlElement(name = "P101_C5A_I")
    public String getP101C5aI() {
        return this.p101C5aI;
    }

    public void setP101C5aI(String p101C5aI) {
        this.p101C5aI = p101C5aI;
    }

    @XmlElement(name = "P101_C5A_P")
    public String getP101C5aP() {
        return this.p101C5aP;
    }

    public void setP101C5aP(String p101C5aP) {
        this.p101C5aP = p101C5aP;
    }

    @XmlElement(name = "P101_C5A_S")
    public String getP101C5aS() {
        return this.p101C5aS;
    }

    public void setP101C5aS(String p101C5aS) {
        this.p101C5aS = p101C5aS;
    }

    @XmlElement(name = "P101_DD")
    public String getP101Dd() {
        return this.p101Dd;
    }

    public void setP101Dd(String p101Dd) {
        this.p101Dd = p101Dd;
    }

    @XmlElement(name = "P101_MM")
    public String getP101Mm() {
        return this.p101Mm;
    }

    public void setP101Mm(String p101Mm) {
        this.p101Mm = p101Mm;
    }

    @XmlElement(name = "P102_C4")
    public String getP102C4() {
        return this.p102C4;
    }

    public void setP102C4(String p102C4) {
        this.p102C4 = p102C4;
    }

    @XmlElement(name = "P102_C5A")
    public String getP102C5a() {
        return this.p102C5a;
    }

    public void setP102C5a(String p102C5a) {
        this.p102C5a = p102C5a;
    }

    @XmlElement(name = "P102_C5A_1")
    public String getP102C5a1() {
        return this.p102C5a1;
    }

    public void setP102C5a1(String p102C5a1) {
        this.p102C5a1 = p102C5a1;
    }

    @XmlElement(name = "P102_DD")
    public String getP102Dd() {
        return this.p102Dd;
    }

    public void setP102Dd(String p102Dd) {
        this.p102Dd = p102Dd;
    }

    @XmlElement(name = "P102_MM")
    public String getP102Mm() {
        return this.p102Mm;
    }

    public void setP102Mm(String p102Mm) {
        this.p102Mm = p102Mm;
    }

    @XmlElement(name = "P103_C4")
    public Integer getP103C4() {
        return this.p103C4;
    }

    public void setP103C4(Integer p103C4) {
        this.p103C4 = p103C4;
    }

    @XmlElement(name = "P103_MHI")
    public String getP103Mhi() {
        return this.p103Mhi;
    }

    public void setP103Mhi(String p103Mhi) {
        this.p103Mhi = p103Mhi;
    }

    @XmlElement(name = "P103_MHT")
    public String getP103Mht() {
        return this.p103Mht;
    }

    public void setP103Mht(String p103Mht) {
        this.p103Mht = p103Mht;
    }

    @XmlElement(name = "P103_MMI")
    public String getP103Mmi() {
        return this.p103Mmi;
    }

    public void setP103Mmi(String p103Mmi) {
        this.p103Mmi = p103Mmi;
    }

    @XmlElement(name = "P103_MMT")
    public String getP103Mmt() {
        return this.p103Mmt;
    }

    public void setP103Mmt(String p103Mmt) {
        this.p103Mmt = p103Mmt;
    }

    @XmlElement(name = "P103_THI")
    public String getP103Thi() {
        return this.p103Thi;
    }

    public void setP103Thi(String p103Thi) {
        this.p103Thi = p103Thi;
    }

    @XmlElement(name = "P103_THT")
    public String getP103Tht() {
        return this.p103Tht;
    }

    public void setP103Tht(String p103Tht) {
        this.p103Tht = p103Tht;
    }

    @XmlElement(name = "P103_TMI")
    public String getP103Tmi() {
        return this.p103Tmi;
    }

    public void setP103Tmi(String p103Tmi) {
        this.p103Tmi = p103Tmi;
    }

    @XmlElement(name = "P103_TMT")
    public String getP103Tmt() {
        return this.p103Tmt;
    }

    public void setP103Tmt(String p103Tmt) {
        this.p103Tmt = p103Tmt;
    }

    @XmlElement(name = "P104_C13P")
    public String getP104C13p() {
        return this.p104C13p;
    }

    public void setP104C13p(String p104C13p) {
        this.p104C13p = p104C13p;
    }

    @XmlElement(name = "P104_C3AS")
    public String getP104C3as() {
        return this.p104C3as;
    }

    public void setP104C3as(String p104C3as) {
        this.p104C3as = p104C3as;
    }

    @XmlElement(name = "P104_C4_DO")
    public Integer getP104C4Do() {
        return this.p104C4Do;
    }

    public void setP104C4Do(Integer p104C4Do) {
        this.p104C4Do = p104C4Do;
    }

    @XmlElement(name = "P104_C4_JU")
    public Integer getP104C4Ju() {
        return this.p104C4Ju;
    }

    public void setP104C4Ju(Integer p104C4Ju) {
        this.p104C4Ju = p104C4Ju;
    }

    @XmlElement(name = "P104_C4_LU")
    public Integer getP104C4Lu() {
        return this.p104C4Lu;
    }

    public void setP104C4Lu(Integer p104C4Lu) {
        this.p104C4Lu = p104C4Lu;
    }

    @XmlElement(name = "P104_C4_MA")
    public Integer getP104C4Ma() {
        return this.p104C4Ma;
    }

    public void setP104C4Ma(Integer p104C4Ma) {
        this.p104C4Ma = p104C4Ma;
    }

    @XmlElement(name = "P104_C4_MI")
    public Integer getP104C4Mi() {
        return this.p104C4Mi;
    }

    public void setP104C4Mi(Integer p104C4Mi) {
        this.p104C4Mi = p104C4Mi;
    }

    @XmlElement(name = "P104_C4_SA")
    public Integer getP104C4Sa() {
        return this.p104C4Sa;
    }

    public void setP104C4Sa(Integer p104C4Sa) {
        this.p104C4Sa = p104C4Sa;
    }

    @XmlElement(name = "P104_C4_VI")
    public Integer getP104C4Vi() {
        return this.p104C4Vi;
    }

    public void setP104C4Vi(Integer p104C4Vi) {
        this.p104C4Vi = p104C4Vi;
    }

    @XmlElement(name = "P105_6_C41")
    public String getP1056C41() {
        return this.p1056C41;
    }

    public void setP1056C41(String p1056C41) {
        this.p1056C41 = p1056C41;
    }

    @XmlElement(name = "P105_6_C42")
    public String getP1056C42() {
        return this.p1056C42;
    }

    public void setP1056C42(String p1056C42) {
        this.p1056C42 = p1056C42;
    }

    @XmlElement(name = "P105_6_C43")
    public String getP1056C43() {
        return this.p1056C43;
    }

    public void setP1056C43(String p1056C43) {
        this.p1056C43 = p1056C43;
    }

    @XmlElement(name = "P105_6_C44")
    public String getP1056C44() {
        return this.p1056C44;
    }

    public void setP1056C44(String p1056C44) {
        this.p1056C44 = p1056C44;
    }

    @XmlElement(name = "P105_6_C45")
    public String getP1056C45() {
        return this.p1056C45;
    }

    public void setP1056C45(String p1056C45) {
        this.p1056C45 = p1056C45;
    }

    @XmlElement(name = "P105_6_C46")
    public String getP1056C46() {
        return this.p1056C46;
    }

    public void setP1056C46(String p1056C46) {
        this.p1056C46 = p1056C46;
    }

    @XmlElement(name = "P105_6_C47")
    public String getP1056C47() {
        return this.p1056C47;
    }

    public void setP1056C47(String p1056C47) {
        this.p1056C47 = p1056C47;
    }

    @XmlElement(name = "P105_6_C48")
    public String getP1056C48() {
        return this.p1056C48;
    }

    public void setP1056C48(String p1056C48) {
        this.p1056C48 = p1056C48;
    }

    @XmlElement(name = "P105_6_C4E")
    public String getP1056C4e() {
        return this.p1056C4e;
    }

    public void setP1056C4e(String p1056C4e) {
        this.p1056C4e = p1056C4e;
    }

    @XmlElement(name = "P105_C13PS")
    public String getP105C13ps() {
        return this.p105C13ps;
    }

    public void setP105C13ps(String p105C13ps) {
        this.p105C13ps = p105C13ps;
    }

    @XmlElement(name = "P105_C2")
    public String getP105C2() {
        return this.p105C2;
    }

    public void setP105C2(String p105C2) {
        this.p105C2 = p105C2;
    }

    @XmlElement(name = "P105_C4A_1")
    public String getP105C4a1() {
        return this.p105C4a1;
    }

    public void setP105C4a1(String p105C4a1) {
        this.p105C4a1 = p105C4a1;
    }

    @XmlElement(name = "P105_C4A_2")
    public String getP105C4a2() {
        return this.p105C4a2;
    }

    public void setP105C4a2(String p105C4a2) {
        this.p105C4a2 = p105C4a2;
    }

    @XmlElement(name = "P105_C4A_3")
    public String getP105C4a3() {
        return this.p105C4a3;
    }

    public void setP105C4a3(String p105C4a3) {
        this.p105C4a3 = p105C4a3;
    }

    @XmlElement(name = "P105_C4A_4")
    public String getP105C4a4() {
        return this.p105C4a4;
    }

    public void setP105C4a4(String p105C4a4) {
        this.p105C4a4 = p105C4a4;
    }

    @XmlElement(name = "P105_C4A_5")
    public String getP105C4a5() {
        return this.p105C4a5;
    }

    public void setP105C4a5(String p105C4a5) {
        this.p105C4a5 = p105C4a5;
    }

    @XmlElement(name = "P105_C4A_E")
    public String getP105C4aE() {
        return this.p105C4aE;
    }

    public void setP105C4aE(String p105C4aE) {
        this.p105C4aE = p105C4aE;
    }

    @XmlElement(name = "P106_C13PS")
    public String getP106C13ps() {
        return this.p106C13ps;
    }

    public void setP106C13ps(String p106C13ps) {
        this.p106C13ps = p106C13ps;
    }

    @XmlElement(name = "P106_C2_JU")
    public String getP106C2Ju() {
        return this.p106C2Ju;
    }

    public void setP106C2Ju(String p106C2Ju) {
        this.p106C2Ju = p106C2Ju;
    }

    @XmlElement(name = "P106_C2_LU")
    public String getP106C2Lu() {
        return this.p106C2Lu;
    }

    public void setP106C2Lu(String p106C2Lu) {
        this.p106C2Lu = p106C2Lu;
    }

    @XmlElement(name = "P106_C2_MA")
    public String getP106C2Ma() {
        return this.p106C2Ma;
    }

    public void setP106C2Ma(String p106C2Ma) {
        this.p106C2Ma = p106C2Ma;
    }

    @XmlElement(name = "P106_C2_MI")
    public String getP106C2Mi() {
        return this.p106C2Mi;
    }

    public void setP106C2Mi(String p106C2Mi) {
        this.p106C2Mi = p106C2Mi;
    }

    @XmlElement(name = "P106_C2_SA")
    public String getP106C2Sa() {
        return this.p106C2Sa;
    }

    public void setP106C2Sa(String p106C2Sa) {
        this.p106C2Sa = p106C2Sa;
    }

    @XmlElement(name = "P106_C2_VI")
    public String getP106C2Vi() {
        return this.p106C2Vi;
    }

    public void setP106C2Vi(String p106C2Vi) {
        this.p106C2Vi = p106C2Vi;
    }

    @XmlElement(name = "P106_C4AI")
    public String getP106C4ai() {
        return this.p106C4ai;
    }

    public void setP106C4ai(String p106C4ai) {
        this.p106C4ai = p106C4ai;
    }

    @XmlElement(name = "P107_109")
    public String getP107109() {
        return this.p107109;
    }

    public void setP107109(String p107109) {
        this.p107109 = p107109;
    }

    @XmlElement(name = "P107_C13PS")
    public String getP107C13ps() {
        return this.p107C13ps;
    }

    public void setP107C13ps(String p107C13ps) {
        this.p107C13ps = p107C13ps;
    }

    @XmlElement(name = "P107_C4AI")
    public Integer getP107C4ai() {
        return this.p107C4ai;
    }

    public void setP107C4ai(Integer p107C4ai) {
        this.p107C4ai = p107C4ai;
    }

    @XmlElement(name = "P108_110_1")
    public String getP1081101() {
        return this.p1081101;
    }

    public void setP1081101(String p1081101) {
        this.p1081101 = p1081101;
    }

    @XmlElement(name = "P108_110_2")
    public String getP1081102() {
        return this.p1081102;
    }

    public void setP1081102(String p1081102) {
        this.p1081102 = p1081102;
    }

    @XmlElement(name = "P108_110_3")
    public String getP1081103() {
        return this.p1081103;
    }

    public void setP1081103(String p1081103) {
        this.p1081103 = p1081103;
    }

    @XmlElement(name = "P108_110_4")
    public String getP1081104() {
        return this.p1081104;
    }

    public void setP1081104(String p1081104) {
        this.p1081104 = p1081104;
    }

    @XmlElement(name = "P108_110E")
    public String getP108110e() {
        return this.p108110e;
    }

    public void setP108110e(String p108110e) {
        this.p108110e = p108110e;
    }

    @XmlElement(name = "P108_C4_DO")
    public Integer getP108C4Do() {
        return this.p108C4Do;
    }

    public void setP108C4Do(Integer p108C4Do) {
        this.p108C4Do = p108C4Do;
    }

    @XmlElement(name = "P108_C4_JU")
    public Integer getP108C4Ju() {
        return this.p108C4Ju;
    }

    public void setP108C4Ju(Integer p108C4Ju) {
        this.p108C4Ju = p108C4Ju;
    }

    @XmlElement(name = "P108_C4_LU")
    public Integer getP108C4Lu() {
        return this.p108C4Lu;
    }

    public void setP108C4Lu(Integer p108C4Lu) {
        this.p108C4Lu = p108C4Lu;
    }

    @XmlElement(name = "P108_C4_MA")
    public Integer getP108C4Ma() {
        return this.p108C4Ma;
    }

    public void setP108C4Ma(Integer p108C4Ma) {
        this.p108C4Ma = p108C4Ma;
    }

    @XmlElement(name = "P108_C4_MI")
    public Integer getP108C4Mi() {
        return this.p108C4Mi;
    }

    public void setP108C4Mi(Integer p108C4Mi) {
        this.p108C4Mi = p108C4Mi;
    }

    @XmlElement(name = "P108_C4_SA")
    public Integer getP108C4Sa() {
        return this.p108C4Sa;
    }

    public void setP108C4Sa(Integer p108C4Sa) {
        this.p108C4Sa = p108C4Sa;
    }

    @XmlElement(name = "P108_C4_VI")
    public Integer getP108C4Vi() {
        return this.p108C4Vi;
    }

    public void setP108C4Vi(Integer p108C4Vi) {
        this.p108C4Vi = p108C4Vi;
    }

    @XmlElement(name = "P108_IPS1")
    public String getP108Ips1() {
        return this.p108Ips1;
    }

    public void setP108Ips1(String p108Ips1) {
        this.p108Ips1 = p108Ips1;
    }

    @XmlElement(name = "P108_IPS2")
    public String getP108Ips2() {
        return this.p108Ips2;
    }

    public void setP108Ips2(String p108Ips2) {
        this.p108Ips2 = p108Ips2;
    }

    @XmlElement(name = "P108_IPS3")
    public String getP108Ips3() {
        return this.p108Ips3;
    }

    public void setP108Ips3(String p108Ips3) {
        this.p108Ips3 = p108Ips3;
    }

    @XmlElement(name = "P108_IPS4")
    public String getP108Ips4() {
        return this.p108Ips4;
    }

    public void setP108Ips4(String p108Ips4) {
        this.p108Ips4 = p108Ips4;
    }

    @XmlElement(name = "P108_IPS5")
    public String getP108Ips5() {
        return this.p108Ips5;
    }

    public void setP108Ips5(String p108Ips5) {
        this.p108Ips5 = p108Ips5;
    }

    @XmlElement(name = "P108_IPS6")
    public String getP108Ips6() {
        return this.p108Ips6;
    }

    public void setP108Ips6(String p108Ips6) {
        this.p108Ips6 = p108Ips6;
    }

    @XmlElement(name = "P108_IPS7")
    public String getP108Ips7() {
        return this.p108Ips7;
    }

    public void setP108Ips7(String p108Ips7) {
        this.p108Ips7 = p108Ips7;
    }

    @XmlElement(name = "P109_111_1")
    public String getP1091111() {
        return this.p1091111;
    }

    public void setP1091111(String p1091111) {
        this.p1091111 = p1091111;
    }

    @XmlElement(name = "P109_111_2")
    public String getP1091112() {
        return this.p1091112;
    }

    public void setP1091112(String p1091112) {
        this.p1091112 = p1091112;
    }

    @XmlElement(name = "P109_111_3")
    public String getP1091113() {
        return this.p1091113;
    }

    public void setP1091113(String p1091113) {
        this.p1091113 = p1091113;
    }

    @XmlElement(name = "P109_111_4")
    public String getP1091114() {
        return this.p1091114;
    }

    public void setP1091114(String p1091114) {
        this.p1091114 = p1091114;
    }

    @XmlElement(name = "P109_111E")
    public String getP109111e() {
        return this.p109111e;
    }

    public void setP109111e(String p109111e) {
        this.p109111e = p109111e;
    }

    @XmlElement(name = "P109_C13PS")
    public String getP109C13ps() {
        return this.p109C13ps;
    }

    public void setP109C13ps(String p109C13ps) {
        this.p109C13ps = p109C13ps;
    }

    @XmlElement(name = "P110_112")
    public String getP110112() {
        return this.p110112;
    }

    public void setP110112(String p110112) {
        this.p110112 = p110112;
    }

    @XmlElement(name = "P110_112E")
    public String getP110112e() {
        return this.p110112e;
    }

    public void setP110112e(String p110112e) {
        this.p110112e = p110112e;
    }

    @XmlElement(name = "P110_C13PS")
    public String getP110C13ps() {
        return this.p110C13ps;
    }

    public void setP110C13ps(String p110C13ps) {
        this.p110C13ps = p110C13ps;
    }

    @XmlElement(name = "P111_C13PS")
    public String getP111C13ps() {
        return this.p111C13ps;
    }

    public void setP111C13ps(String p111C13ps) {
        this.p111C13ps = p111C13ps;
    }

    @XmlElement(name = "P112_C13S")
    public String getP112C13s() {
        return this.p112C13s;
    }

    public void setP112C13s(String p112C13s) {
        this.p112C13s = p112C13s;
    }

    @XmlElement(name = "P1181_C3S")
    public String getP1181C3s() {
        return this.p1181C3s;
    }

    public void setP1181C3s(String p1181C3s) {
        this.p1181C3s = p1181C3s;
    }

    @XmlElement(name = "P1181_C3SQ")
    public Integer getP1181C3sq() {
        return this.p1181C3sq;
    }

    public void setP1181C3sq(Integer p1181C3sq) {
        this.p1181C3sq = p1181C3sq;
    }

    @XmlElement(name = "P1182_3S11")
    public String getP11823s11() {
        return this.p11823s11;
    }

    public void setP11823s11(String p11823s11) {
        this.p11823s11 = p11823s11;
    }

    @XmlElement(name = "P1182_3S12")
    public String getP11823s12() {
        return this.p11823s12;
    }

    public void setP11823s12(String p11823s12) {
        this.p11823s12 = p11823s12;
    }

    @XmlElement(name = "P1182_3S13")
    public String getP11823s13() {
        return this.p11823s13;
    }

    public void setP11823s13(String p11823s13) {
        this.p11823s13 = p11823s13;
    }

    @XmlElement(name = "P1182_3S14")
    public String getP11823s14() {
        return this.p11823s14;
    }

    public void setP11823s14(String p11823s14) {
        this.p11823s14 = p11823s14;
    }

    @XmlElement(name = "P1182_3S21")
    public String getP11823s21() {
        return this.p11823s21;
    }

    public void setP11823s21(String p11823s21) {
        this.p11823s21 = p11823s21;
    }

    @XmlElement(name = "P1182_3S22")
    public String getP11823s22() {
        return this.p11823s22;
    }

    public void setP11823s22(String p11823s22) {
        this.p11823s22 = p11823s22;
    }

    @XmlElement(name = "P1182_3S23")
    public String getP11823s23() {
        return this.p11823s23;
    }

    public void setP11823s23(String p11823s23) {
        this.p11823s23 = p11823s23;
    }

    @XmlElement(name = "P1182_3S24")
    public String getP11823s24() {
        return this.p11823s24;
    }

    public void setP11823s24(String p11823s24) {
        this.p11823s24 = p11823s24;
    }

    @XmlElement(name = "P401_C12")
    public String getP401C12() {
        return this.p401C12;
    }

    public void setP401C12(String p401C12) {
        this.p401C12 = p401C12;
    }

    @XmlElement(name = "P401_C3PS")
    public String getP401C3ps() {
        return this.p401C3ps;
    }

    public void setP401C3ps(String p401C3ps) {
        this.p401C3ps = p401C3ps;
    }

    @XmlElement(name = "P401_C4")
    public String getP401C4() {
        return this.p401C4;
    }

    public void setP401C4(String p401C4) {
        this.p401C4 = p401C4;
    }

    @XmlElement(name = "P402_ANIO")
    public String getP402Anio() {
        return this.p402Anio;
    }

    public void setP402Anio(String p402Anio) {
        this.p402Anio = p402Anio;
    }

    @XmlElement(name = "P402_EDA4")
    public Integer getP402Eda4() {
        return this.p402Eda4;
    }

    public void setP402Eda4(Integer p402Eda4) {
        this.p402Eda4 = p402Eda4;
    }

    @XmlElement(name = "P402_EDA5")
    public Integer getP402Eda5() {
        return this.p402Eda5;
    }

    public void setP402Eda5(Integer p402Eda5) {
        this.p402Eda5 = p402Eda5;
    }

    @XmlElement(name = "P402_MES")
    public String getP402Mes() {
        return this.p402Mes;
    }

    public void setP402Mes(String p402Mes) {
        this.p402Mes = p402Mes;
    }

    @XmlElement(name = "P402_TOT")
    public Integer getP402Tot() {
        return this.p402Tot;
    }

    public void setP402Tot(Integer p402Tot) {
        this.p402Tot = p402Tot;
    }

    @XmlElement(name = "P403_C12")
    public String getP403C12() {
        return this.p403C12;
    }

    public void setP403C12(String p403C12) {
        this.p403C12 = p403C12;
    }

    @XmlElement(name = "P403_C3PS")
    public String getP403C3ps() {
        return this.p403C3ps;
    }

    public void setP403C3ps(String p403C3ps) {
        this.p403C3ps = p403C3ps;
    }

    @XmlElement(name = "P404_ANIO")
    public String getP404Anio() {
        return this.p404Anio;
    }

    public void setP404Anio(String p404Anio) {
        this.p404Anio = p404Anio;
    }

    @XmlElement(name = "P404_EDA4")
    public Integer getP404Eda4() {
        return this.p404Eda4;
    }

    public void setP404Eda4(Integer p404Eda4) {
        this.p404Eda4 = p404Eda4;
    }

    @XmlElement(name = "P404_EDA5")
    public Integer getP404Eda5() {
        return this.p404Eda5;
    }

    public void setP404Eda5(Integer p404Eda5) {
        this.p404Eda5 = p404Eda5;
    }

    @XmlElement(name = "P404_MES")
    public String getP404Mes() {
        return this.p404Mes;
    }

    public void setP404Mes(String p404Mes) {
        this.p404Mes = p404Mes;
    }

    @XmlElement(name = "P404_TOT")
    public Integer getP404Tot() {
        return this.p404Tot;
    }

    public void setP404Tot(Integer p404Tot) {
        this.p404Tot = p404Tot;
    }

    @XmlElement(name = "P405_C12")
    public String getP405C12() {
        return this.p405C12;
    }

    public void setP405C12(String p405C12) {
        this.p405C12 = p405C12;
    }

    @XmlElement(name = "P405_C4")
    public String getP405C4() {
        return this.p405C4;
    }

    public void setP405C4(String p405C4) {
        this.p405C4 = p405C4;
    }

    @XmlElement(name = "P405_I")
    public String getP405I() {
        return this.p405I;
    }

    public void setP405I(String p405I) {
        this.p405I = p405I;
    }

    @XmlElement(name = "P405_II")
    public String getP405Ii() {
        return this.p405Ii;
    }

    public void setP405Ii(String p405Ii) {
        this.p405Ii = p405Ii;
    }

    @XmlElement(name = "P4051_C3PS")
    public String getP4051C3ps() {
        return this.p4051C3ps;
    }

    public void setP4051C3ps(String p4051C3ps) {
        this.p4051C3ps = p4051C3ps;
    }

    @XmlElement(name = "P406_C4_1")
    public String getP406C41() {
        return this.p406C41;
    }

    public void setP406C41(String p406C41) {
        this.p406C41 = p406C41;
    }

    @XmlElement(name = "P406_C4_10")
    public String getP406C410() {
        return this.p406C410;
    }

    public void setP406C410(String p406C410) {
        this.p406C410 = p406C410;
    }

    @XmlElement(name = "P406_C4_10E")
    public String getP406C410e() {
        return this.p406C410e;
    }

    public void setP406C410e(String p406C410e) {
        this.p406C410e = p406C410e;
    }

    @XmlElement(name = "P406_C4_11")
    public String getP406C411() {
        return this.p406C411;
    }

    public void setP406C411(String p406C411) {
        this.p406C411 = p406C411;
    }

    @XmlElement(name = "P406_C4_2")
    public String getP406C42() {
        return this.p406C42;
    }

    public void setP406C42(String p406C42) {
        this.p406C42 = p406C42;
    }

    @XmlElement(name = "P406_C4_3")
    public String getP406C43() {
        return this.p406C43;
    }

    public void setP406C43(String p406C43) {
        this.p406C43 = p406C43;
    }

    @XmlElement(name = "P406_C4_4")
    public String getP406C44() {
        return this.p406C44;
    }

    public void setP406C44(String p406C44) {
        this.p406C44 = p406C44;
    }

    @XmlElement(name = "P406_C4_5")
    public String getP406C45() {
        return this.p406C45;
    }

    public void setP406C45(String p406C45) {
        this.p406C45 = p406C45;
    }

    @XmlElement(name = "P406_C4_6")
    public String getP406C46() {
        return this.p406C46;
    }

    public void setP406C46(String p406C46) {
        this.p406C46 = p406C46;
    }

    @XmlElement(name = "P406_C4_7")
    public String getP406C47() {
        return this.p406C47;
    }

    public void setP406C47(String p406C47) {
        this.p406C47 = p406C47;
    }

    @XmlElement(name = "P406_C4_8")
    public String getP406C48() {
        return this.p406C48;
    }

    public void setP406C48(String p406C48) {
        this.p406C48 = p406C48;
    }

    @XmlElement(name = "P406_C4_9")
    public String getP406C49() {
        return this.p406C49;
    }

    public void setP406C49(String p406C49) {
        this.p406C49 = p406C49;
    }

    @XmlElement(name = "P406_C4_9E")
    public String getP406C49e() {
        return this.p406C49e;
    }

    public void setP406C49e(String p406C49e) {
        this.p406C49e = p406C49e;
    }

    @XmlElement(name = "P407_C4_11")
    public String getP407C411() {
        return this.p407C411;
    }

    public void setP407C411(String p407C411) {
        this.p407C411 = p407C411;
    }

    @XmlElement(name = "P407_C4_12")
    public Integer getP407C412() {
        return this.p407C412;
    }

    public void setP407C412(Integer p407C412) {
        this.p407C412 = p407C412;
    }

    @XmlElement(name = "P407_C4_21")
    public String getP407C421() {
        return this.p407C421;
    }

    public void setP407C421(String p407C421) {
        this.p407C421 = p407C421;
    }

    @XmlElement(name = "P407_C4_22")
    public Integer getP407C422() {
        return this.p407C422;
    }

    public void setP407C422(Integer p407C422) {
        this.p407C422 = p407C422;
    }

    @XmlElement(name = "P407_C4_31")
    public String getP407C431() {
        return this.p407C431;
    }

    public void setP407C431(String p407C431) {
        this.p407C431 = p407C431;
    }

    @XmlElement(name = "P407_C4_32")
    public Integer getP407C432() {
        return this.p407C432;
    }

    public void setP407C432(Integer p407C432) {
        this.p407C432 = p407C432;
    }

    @XmlElement(name = "P407_C4_41")
    public String getP407C441() {
        return this.p407C441;
    }

    public void setP407C441(String p407C441) {
        this.p407C441 = p407C441;
    }

    @XmlElement(name = "P408_C4_1")
    public Integer getP408C41() {
        return this.p408C41;
    }

    public void setP408C41(Integer p408C41) {
        this.p408C41 = p408C41;
    }

    @XmlElement(name = "P408_C4_2")
    public Integer getP408C42() {
        return this.p408C42;
    }

    public void setP408C42(Integer p408C42) {
        this.p408C42 = p408C42;
    }

    @XmlElement(name = "P408_C4_3")
    public Integer getP408C43() {
        return this.p408C43;
    }

    public void setP408C43(Integer p408C43) {
        this.p408C43 = p408C43;
    }

    @XmlElement(name = "P409_C4_1")
    public String getP409C41() {
        return this.p409C41;
    }

    public void setP409C41(String p409C41) {
        this.p409C41 = p409C41;
    }

    @XmlElement(name = "P409_C4_2")
    public String getP409C42() {
        return this.p409C42;
    }

    public void setP409C42(String p409C42) {
        this.p409C42 = p409C42;
    }

    @XmlElement(name = "P409_C4_3")
    public String getP409C43() {
        return this.p409C43;
    }

    public void setP409C43(String p409C43) {
        this.p409C43 = p409C43;
    }

    @XmlElement(name = "P409_C4_4")
    public String getP409C44() {
        return this.p409C44;
    }

    public void setP409C44(String p409C44) {
        this.p409C44 = p409C44;
    }

    @XmlElement(name = "P409_C4_5")
    public String getP409C45() {
        return this.p409C45;
    }

    public void setP409C45(String p409C45) {
        this.p409C45 = p409C45;
    }

    @XmlElement(name = "P502_C1")
    public String getP502C1() {
        return this.p502C1;
    }

    public void setP502C1(String p502C1) {
        this.p502C1 = p502C1;
    }

    @XmlElement(name = "P502_C1_A")
    public String getP502C1A() {
        return this.p502C1A;
    }

    public void setP502C1A(String p502C1A) {
        this.p502C1A = p502C1A;
    }

    @XmlElement(name = "P502_C1_D")
    public String getP502C1D() {
        return this.p502C1D;
    }

    public void setP502C1D(String p502C1D) {
        this.p502C1D = p502C1D;
    }

    @XmlElement(name = "P502_C1_M")
    public String getP502C1M() {
        return this.p502C1M;
    }

    public void setP502C1M(String p502C1M) {
        this.p502C1M = p502C1M;
    }

    @XmlElement(name = "P6012")
    public String getP6012() {
        return this.p6012;
    }

    public void setP6012(String p6012) {
        this.p6012 = p6012;
    }

    @XmlElement(name = "P6012_QH")
    public Integer getP6012Qh() {
        return this.p6012Qh;
    }

    public void setP6012Qh(Integer p6012Qh) {
        this.p6012Qh = p6012Qh;
    }

    @XmlElement(name = "P6012_QM")
    public Integer getP6012Qm() {
        return this.p6012Qm;
    }

    public void setP6012Qm(Integer p6012Qm) {
        this.p6012Qm = p6012Qm;
    }
    @XmlElement(name = "P6013")
    public String getP6013() {
        return this.p6013;
    }

    public void setP6013(String p6013) {
        this.p6013 = p6013;
    }
    @XmlElement(name = "P6014")
    public String getP6014() {
        return this.p6014;
    }

    public void setP6014(String p6014) {
        this.p6014 = p6014;
    }
    @XmlElement(name = "P6015")
    public String getP6015() {
        return this.p6015;
    }

    public void setP6015(String p6015) {
        this.p6015 = p6015;
    }

    @XmlElement(name = "P6021_1")
    public String getP60211() {
        return this.p60211;
    }

    public void setP60211(String p60211) {
        this.p60211 = p60211;
    }

    @XmlElement(name = "P6021_1Q")
    public Integer getP60211q() {
        return this.p60211q;
    }

    public void setP60211q(Integer p60211q) {
        this.p60211q = p60211q;
    }

    @XmlElement(name = "P6021_2")
    public String getP60212() {
        return this.p60212;
    }

    public void setP60212(String p60212) {
        this.p60212 = p60212;
    }

    @XmlElement(name = "P6021_2Q")
    public Integer getP60212q() {
        return this.p60212q;
    }

    public void setP60212q(Integer p60212q) {
        this.p60212q = p60212q;
    }
    @XmlElement(name = "P6041")
    public String getP6041() {
        return this.p6041;
    }

    public void setP6041(String p6041) {
        this.p6041 = p6041;
    }

    @XmlElement(name = "P6041_QD")
    public Integer getP6041Qd() {
        return this.p6041Qd;
    }

    public void setP6041Qd(Integer p6041Qd) {
        this.p6041Qd = p6041Qd;
    }

    @XmlElement(name = "P6041_QH")
    public Integer getP6041Qh() {
        return this.p6041Qh;
    }

    public void setP6041Qh(Integer p6041Qh) {
        this.p6041Qh = p6041Qh;
    }
    @XmlElement(name = "P6042")
    public String getP6042() {
        return this.p6042;
    }

    public void setP6042(String p6042) {
        this.p6042 = p6042;
    }

    @XmlElement(name = "P6042_Q")
    public Integer getP6042Q() {
        return this.p6042Q;
    }

    public void setP6042Q(Integer p6042Q) {
        this.p6042Q = p6042Q;
    }
    @XmlElement(name = "P6043")
    public String getP6043() {
        return this.p6043;
    }

    public void setP6043(String p6043) {
        this.p6043 = p6043;
    }
    @XmlElement(name = "P6051")
    public String getP6051() {
        return this.p6051;
    }

    public void setP6051(String p6051) {
        this.p6051 = p6051;
    }

    @XmlElement(name = "P6052_1")
    public String getP60521() {
        return this.p60521;
    }

    public void setP60521(String p60521) {
        this.p60521 = p60521;
    }

    @XmlElement(name = "P6052_2")
    public String getP60522() {
        return this.p60522;
    }

    public void setP60522(String p60522) {
        this.p60522 = p60522;
    }

    @XmlElement(name = "P6052_3")
    public String getP60523() {
        return this.p60523;
    }

    public void setP60523(String p60523) {
        this.p60523 = p60523;
    }

    @XmlElement(name = "P6052_4")
    public String getP60524() {
        return this.p60524;
    }

    public void setP60524(String p60524) {
        this.p60524 = p60524;
    }

    @XmlElement(name = "P6052_5")
    public String getP60525() {
        return this.p60525;
    }

    public void setP60525(String p60525) {
        this.p60525 = p60525;
    }

    @XmlElement(name = "P6052_6")
    public String getP60526() {
        return this.p60526;
    }

    public void setP60526(String p60526) {
        this.p60526 = p60526;
    }

    @XmlElement(name = "P6052_7")
    public String getP60527() {
        return this.p60527;
    }

    public void setP60527(String p60527) {
        this.p60527 = p60527;
    }

    @XmlElement(name = "P6052_7ESP")
    public String getP60527esp() {
        return this.p60527esp;
    }

    public void setP60527esp(String p60527esp) {
        this.p60527esp = p60527esp;
    }

    @XmlElement(name = "PEIB_1")
    public String getPeib1() {
        return this.peib1;
    }

    public void setPeib1(String peib1) {
        this.peib1 = peib1;
    }

    @XmlElement(name = "PEIB_1_RES")
    public String getPeib1Res() {
        return this.peib1Res;
    }

    public void setPeib1Res(String peib1Res) {
        this.peib1Res = peib1Res;
    }

    @XmlElement(name = "PEIB_1EST")
    public String getPeib1est() {
        return this.peib1est;
    }

    public void setPeib1est(String peib1est) {
        this.peib1est = peib1est;
    }

    @XmlElement(name = "PEIB_2")
    public String getPeib2() {
        return this.peib2;
    }

    public void setPeib2(String peib2) {
        this.peib2 = peib2;
    }

    @XmlElement(name = "PEIB_3")
    public String getPeib3() {
        return this.peib3;
    }

    public void setPeib3(String peib3) {
        this.peib3 = peib3;
    }

    @XmlElement(name = "PEIB_3_QN")
    public Integer getPeib3Qn() {
        return this.peib3Qn;
    }

    public void setPeib3Qn(Integer peib3Qn) {
        this.peib3Qn = peib3Qn;
    }

    @XmlElement(name = "PEIB_4_C2A")
    public String getPeib4C2a() {
        return this.peib4C2a;
    }

    public void setPeib4C2a(String peib4C2a) {
        this.peib4C2a = peib4C2a;
    }

    @XmlElement(name = "PEIB_5")
    public String getPeib5() {
        return this.peib5;
    }

    public void setPeib5(String peib5) {
        this.peib5 = peib5;
    }

    @XmlElement(name = "PEIB_5_1")
    public String getPeib51() {
        return this.peib51;
    }

    public void setPeib51(String peib51) {
        this.peib51 = peib51;
    }

    @XmlElement(name = "PEIB_5_1AC")
    public String getPeib51ac() {
        return this.peib51ac;
    }

    public void setPeib51ac(String peib51ac) {
        this.peib51ac = peib51ac;
    }

    @XmlElement(name = "PEIB_5_2")
    public String getPeib52() {
        return this.peib52;
    }

    public void setPeib52(String peib52) {
        this.peib52 = peib52;
    }

    @XmlElement(name = "PEIB_5_2AC")
    public String getPeib52ac() {
        return this.peib52ac;
    }

    public void setPeib52ac(String peib52ac) {
        this.peib52ac = peib52ac;
    }

    @XmlElement(name = "PEIB_5_3")
    public String getPeib53() {
        return this.peib53;
    }

    public void setPeib53(String peib53) {
        this.peib53 = peib53;
    }

    @XmlElement(name = "PEIB_5_3AC")
    public String getPeib53ac() {
        return this.peib53ac;
    }

    public void setPeib53ac(String peib53ac) {
        this.peib53ac = peib53ac;
    }

    @XmlElement(name = "PEIB_5_4")
    public String getPeib54() {
        return this.peib54;
    }

    public void setPeib54(String peib54) {
        this.peib54 = peib54;
    }

    @XmlElement(name = "PEIB_5_4AC")
    public String getPeib54ac() {
        return this.peib54ac;
    }

    public void setPeib54ac(String peib54ac) {
        this.peib54ac = peib54ac;
    }

    @XmlElement(name = "PEIB_5_5")
    public String getPeib55() {
        return this.peib55;
    }

    public void setPeib55(String peib55) {
        this.peib55 = peib55;
    }

    @XmlElement(name = "PEIB_5_5AC")
    public String getPeib55ac() {
        return this.peib55ac;
    }

    public void setPeib55ac(String peib55ac) {
        this.peib55ac = peib55ac;
    }

    @XmlElement(name = "PEIB_5_6")
    public String getPeib56() {
        return this.peib56;
    }

    public void setPeib56(String peib56) {
        this.peib56 = peib56;
    }

    @XmlElement(name = "PEIB_5_6AC")
    public String getPeib56ac() {
        return this.peib56ac;
    }

    public void setPeib56ac(String peib56ac) {
        this.peib56ac = peib56ac;
    }

    @XmlElement(name = "PRAC_UOI")
    public String getPracUoi() {
        return this.pracUoi;
    }

    public void setPracUoi(String pracUoi) {
        this.pracUoi = pracUoi;
    }

    @XmlElement(name = "PRAC_UOIQ")
    public Integer getPracUoiq() {
        return this.pracUoiq;
    }

    public void setPracUoiq(Integer pracUoiq) {
        this.pracUoiq = pracUoiq;
    }

    @XmlElement(name = "RUTA_SBI")
    public String getRutaSbi() {
        return this.rutaSbi;
    }

    public void setRutaSbi(String rutaSbi) {
        this.rutaSbi = rutaSbi;
    }

    @XmlElement(name = "RUTA_SBI_1")
    public Integer getRutaSbi1() {
        return this.rutaSbi1;
    }

    public void setRutaSbi1(Integer rutaSbi1) {
        this.rutaSbi1 = rutaSbi1;
    }

    @XmlElement(name = "RUTA_SBI_2")
    public String getRutaSbi2() {
        return this.rutaSbi2;
    }

    public void setRutaSbi2(String rutaSbi2) {
        this.rutaSbi2 = rutaSbi2;
    }

    @XmlElement(name = "RUTA_SLA")
    public String getRutaSla() {
        return this.rutaSla;
    }

    public void setRutaSla(String rutaSla) {
        this.rutaSla = rutaSla;
    }

    @XmlElement(name = "RUTA_SLA_1")
    public Integer getRutaSla1() {
        return this.rutaSla1;
    }

    public void setRutaSla1(Integer rutaSla1) {
        this.rutaSla1 = rutaSla1;
    }

    @XmlElement(name = "RUTA_SLA_2")
    public String getRutaSla2() {
        return this.rutaSla2;
    }

    public void setRutaSla2(String rutaSla2) {
        this.rutaSla2 = rutaSla2;
    }

    @XmlElement(name = "SERV_NE_1")
    public String getServNe1() {
        return this.servNe1;
    }

    public void setServNe1(String servNe1) {
        this.servNe1 = servNe1;
    }

    @XmlElement(name = "SERV_NE_2")
    public String getServNe2() {
        return this.servNe2;
    }

    public void setServNe2(String servNe2) {
        this.servNe2 = servNe2;
    }

    @XmlElement(name = "SERV_NE_3")
    public String getServNe3() {
        return this.servNe3;
    }

    public void setServNe3(String servNe3) {
        this.servNe3 = servNe3;
    }

    @XmlElement(name = "SERV_NE_4")
    public String getServNe4() {
        return this.servNe4;
    }

    public void setServNe4(String servNe4) {
        this.servNe4 = servNe4;
    }

    @XmlElement(name = "SERV_NE_5")
    public String getServNe5() {
        return this.servNe5;
    }

    public void setServNe5(String servNe5) {
        this.servNe5 = servNe5;
    }

    @XmlElement(name = "SERV_NE_6")
    public String getServNe6() {
        return this.servNe6;
    }

    public void setServNe6(String servNe6) {
        this.servNe6 = servNe6;
    }

    @XmlElement(name = "SERV_NE_6E")
    public String getServNe6e() {
        return this.servNe6e;
    }

    public void setServNe6e(String servNe6e) {
        this.servNe6e = servNe6e;
    }

    @XmlElement(name = "SERV_NE_7")
    public String getServNe7() {
        return this.servNe7;
    }

    public void setServNe7(String servNe7) {
        this.servNe7 = servNe7;
    }

    @XmlElement(name = "TADMINIST")
    public Integer getTadminist() {
        return this.tadminist;
    }

    public void setTadminist(Integer tadminist) {
        this.tadminist = tadminist;
    }

    @XmlElement(name = "TAUXILIAR")
    public Integer getTauxiliar() {
        return this.tauxiliar;
    }

    public void setTauxiliar(Integer tauxiliar) {
        this.tauxiliar = tauxiliar;
    }

    @XmlElement(name = "TDOCAULA")
    public Integer getTdocaula() {
        return this.tdocaula;
    }

    public void setTdocaula(Integer tdocaula) {
        this.tdocaula = tdocaula;
    }

    @XmlElement(name = "TDOCENTES")
    public Integer getTdocentes() {
        return this.tdocentes;
    }

    public void setTdocentes(Integer tdocentes) {
        this.tdocentes = tdocentes;
    }

    @XmlElement(name = "TIPO_ENVIO")
    public String getTipoEnvio() {
        return this.tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    @XmlElement(name = "TIPOIESUP")
    public String getTipoiesup() {
        return this.tipoiesup;
    }

    public void setTipoiesup(String tipoiesup) {
        this.tipoiesup = tipoiesup;
    }

    @XmlElement(name = "TIPOPROG")
    public String getTipoprog() {
        return this.tipoprog;
    }

    public void setTipoprog(String tipoprog) {
        this.tipoprog = tipoprog;
    }

    @XmlAttribute(name = "ESTADO_RPT")
    public String getEstadoRpt() {
        return this.estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @XmlAttribute(name = "MSG")
    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @XmlElement(name = "TIPOFINA_1")
    public String getTipofina1() {
        return tipofina1;
    }

    public void setTipofina1(String tipofina1) {
        this.tipofina1 = tipofina1;
    }

    @XmlElement(name = "TIPOFINA_2")
    public String getTipofina2() {
        return tipofina2;
    }

    public void setTipofina2(String tipofina2) {
        this.tipofina2 = tipofina2;
    }

    @XmlElement(name = "TIPOFINA_3")
    public String getTipofina3() {
        return tipofina3;
    }

    public void setTipofina3(String tipofina3) {
        this.tipofina3 = tipofina3;
    }

    @XmlElement(name = "TIPOFINA_4")
    public String getTipofina4() {
        return tipofina4;
    }

    public void setTipofina4(String tipofina4) {
        this.tipofina4 = tipofina4;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlElement
    public String getFechasigied() {
        return fechasigied;
    }

    public void setFechasigied(String fechasigied) {
        this.fechasigied = fechasigied;
    }

    @XmlElement(name = "ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    @XmlElement(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
/*
    @XmlElement(name = "MATRICULA")
    @XmlJavaTypeAdapter(Matricula2017MatriculaMapAdapter.class)
    public Map<String, Matricula2017Matricula> getDetalleMatricula() {
        return detalleMatricula;
    }

    public void setDetalleMatricula(Map<String, Matricula2017Matricula> detalleMatricula) {
        this.detalleMatricula = detalleMatricula;
    }

    @XmlElement(name = "RECURSOS")
    @XmlJavaTypeAdapter(Matricula2017RecursosMapAdapter.class)
    public Map<String, Matricula2017Recursos> getDetalleRecursos() {
        return detalleRecursos;
    }

    public void setDetalleRecursos(Map<String, Matricula2017Recursos> detalleRecursos) {
        this.detalleRecursos = detalleRecursos;
    }

    @XmlElement(name = "SECCION")
    @XmlJavaTypeAdapter(Matricula2017SeccionMapAdapter.class)
    public Map<String, Matricula2017Seccion> getDetalleSeccion() {
        return detalleSeccion;
    }

    public void setDetalleSeccion(Map<String, Matricula2017Seccion> detalleSeccion) {
        this.detalleSeccion = detalleSeccion;
    }

    @XmlElement(name = "CARRERAS")
    public List<Matricula2017Carreras> getMatricula2017CarrerasList() {
        return matricula2017CarrerasList;
    }

    public void setMatricula2017CarrerasList(List<Matricula2017Carreras> matricula2017CarrerasList) {
        this.matricula2017CarrerasList = matricula2017CarrerasList;
    }

    @XmlElement(name = "EBA")
    public List<Matricula2017Eba> getMatricula2017EbaList() {
        return matricula2017EbaList;
    }

    public void setMatricula2017EbaList(List<Matricula2017Eba> matricula2017EbaList) {
        this.matricula2017EbaList = matricula2017EbaList;
    }

    @XmlElement(name = "PRONOEI")
    public List<Matricula2017Localpronoei> getMatricula2017LocalpronoeiList() {
        return matricula2017LocalpronoeiList;
    }

    public void setMatricula2017LocalpronoeiList(List<Matricula2017Localpronoei> matricula2017LocalpronoeiList) {
        this.matricula2017LocalpronoeiList = matricula2017LocalpronoeiList;
    }

    @XmlElement(name = "PERSONAL")
    public List<Matricula2017Personal> getMatricula2017PersonalList() {
        return matricula2017PersonalList;
    }

    public void setMatricula2017PersonalList(List<Matricula2017Personal> matricula2017PersonalList) {
        this.matricula2017PersonalList = matricula2017PersonalList;
    }

    @XmlElement(name = "SAANEES")
    public List<Matricula2017Saanee> getMatricula2017SaaneeList() {
        return matricula2017SaaneeList;
    }

    public void setMatricula2017SaaneeList(List<Matricula2017Saanee> matricula2017SaaneeList) {
        this.matricula2017SaaneeList = matricula2017SaaneeList;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2017Cabecera)) {
            return false;
        }
        Matricula2017Cabecera other = (Matricula2017Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2017.Matricula2017Cabecera[idEnvio=" + idEnvio + "]";
    }
}
