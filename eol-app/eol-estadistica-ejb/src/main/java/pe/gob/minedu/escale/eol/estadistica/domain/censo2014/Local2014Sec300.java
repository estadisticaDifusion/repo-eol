/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "local2014_sec300")
public class Local2014Sec300 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P304_1")
    private Integer p3041;
    @Column(name = "P304_2")
    private String p3042;
    @Column(name = "P304_3")
    private String p3043;
    @Column(name = "P304_4M1")
    private String p3044m1;
    @Column(name = "P304_4M2")
    private String p3044m2;
    @Column(name = "P304_4M3")
    private String p3044m3;
    @Column(name = "P304_4M4")
    private Integer p3044m4;
    @Column(name = "P304_4T1")
    private String p3044t1;
    @Column(name = "P304_4T2")
    private String p3044t2;
    @Column(name = "P304_4T3")
    private String p3044t3;
    @Column(name = "P304_4T4")
    private Integer p3044t4;
    @Column(name = "P304_4N1")
    private String p3044n1;
    @Column(name = "P304_4N2")
    private String p3044n2;
    @Column(name = "P304_4N3")
    private String p3044n3;
    @Column(name = "P304_4N4")
    private Integer p3044n4;
    @Column(name = "P304_5")
    private Integer p3045;
    @Column(name = "P304_6")
    private String p3046;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2014Cabecera local2014Cabecera;

    public Local2014Sec300() {
    }

    public Local2014Sec300(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="P304_1")
    public Integer getP3041() {
        return p3041;
    }

    public void setP3041(Integer p3041) {
        this.p3041 = p3041;
    }

    @XmlElement(name="P304_2")
    public String getP3042() {
        return p3042;
    }

    public void setP3042(String p3042) {
        this.p3042 = p3042;
    }

    @XmlElement(name="P304_3")
    public String getP3043() {
        return p3043;
    }

    public void setP3043(String p3043) {
        this.p3043 = p3043;
    }

    @XmlElement(name="P304_4M1")
    public String getP3044m1() {
        return p3044m1;
    }

    public void setP3044m1(String p3044m1) {
        this.p3044m1 = p3044m1;
    }

    @XmlElement(name="P304_4M2")
    public String getP3044m2() {
        return p3044m2;
    }

    public void setP3044m2(String p3044m2) {
        this.p3044m2 = p3044m2;
    }

    @XmlElement(name="P304_4M3")
    public String getP3044m3() {
        return p3044m3;
    }

    public void setP3044m3(String p3044m3) {
        this.p3044m3 = p3044m3;
    }

    @XmlElement(name="P304_4M4")
    public Integer getP3044m4() {
        return p3044m4;
    }

    public void setP3044m4(Integer p3044m4) {
        this.p3044m4 = p3044m4;
    }

    @XmlElement(name="P304_4T1")
    public String getP3044t1() {
        return p3044t1;
    }

    public void setP3044t1(String p3044t1) {
        this.p3044t1 = p3044t1;
    }

    @XmlElement(name="P304_4T2")
    public String getP3044t2() {
        return p3044t2;
    }

    public void setP3044t2(String p3044t2) {
        this.p3044t2 = p3044t2;
    }

    @XmlElement(name="P304_4T3")
    public String getP3044t3() {
        return p3044t3;
    }

    public void setP3044t3(String p3044t3) {
        this.p3044t3 = p3044t3;
    }

    @XmlElement(name="P304_4T4")
    public Integer getP3044t4() {
        return p3044t4;
    }

    public void setP3044t4(Integer p3044t4) {
        this.p3044t4 = p3044t4;
    }

    @XmlElement(name="P304_4N1")
    public String getP3044n1() {
        return p3044n1;
    }

    public void setP3044n1(String p3044n1) {
        this.p3044n1 = p3044n1;
    }

    @XmlElement(name="P304_4N2")
    public String getP3044n2() {
        return p3044n2;
    }

    public void setP3044n2(String p3044n2) {
        this.p3044n2 = p3044n2;
    }

    @XmlElement(name="P304_4N3")
    public String getP3044n3() {
        return p3044n3;
    }

    public void setP3044n3(String p3044n3) {
        this.p3044n3 = p3044n3;
    }

    @XmlElement(name="P304_4N4")
    public Integer getP3044n4() {
        return p3044n4;
    }

    public void setP3044n4(Integer p3044n4) {
        this.p3044n4 = p3044n4;
    }

    @XmlElement(name="P304_5")
    public Integer getP3045() {
        return p3045;
    }

    public void setP3045(Integer p3045) {
        this.p3045 = p3045;
    }

    @XmlElement(name="P304_6")
    public String getP3046() {
        return p3046;
    }

    public void setP3046(String p3046) {
        this.p3046 = p3046;
    }

    @XmlTransient
    public Local2014Cabecera getLocal2014Cabecera() {
        return local2014Cabecera;
    }

    public void setLocal2014Cabecera(Local2014Cabecera local2014Cabecera) {
        this.local2014Cabecera = local2014Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2014Sec300)) {
            return false;
        }
        Local2014Sec300 other = (Local2014Sec300) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

   
}
