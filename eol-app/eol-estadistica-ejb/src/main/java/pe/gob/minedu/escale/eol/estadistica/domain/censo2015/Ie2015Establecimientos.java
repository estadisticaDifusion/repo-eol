package pe.gob.minedu.escale.eol.estadistica.domain.censo2015;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "ie2015_establecimientos")
public class Ie2015Establecimientos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "NRO")
    private String nro;
    @Basic(optional = false)
    @Column(name = "CUADRO")
    private String cuadro;
    @Column(name = "CODLOCAL")
    private String codlocal;
    @Column(name = "TIPOLOCAL")
    private String tipolocal;
    @Column(name = "NOMESTAB")
    private String nomestab;
    @Column(name = "DIRECCION")
    private String direccion;
    @Column(name = "TELEFONO")
    private String telefono;
    @Column(name = "SUMENERG")
    private String sumenerg;
    @Column(name = "PROVSUMI")
    private String provsumi;
    @Column(name = "CMOD_INI")
    private String cmodIni;
    @Column(name = "CMOD_INI1")
    private String cmodIni1;
    @Column(name = "CMOD_INI2")
    private String cmodIni2;
    @Column(name = "CMOD_PRI")
    private String cmodPri;
    @Column(name = "CMOD_PRI1")
    private String cmodPri1;
    @Column(name = "CMOD_PRI2")
    private String cmodPri2;
    @Column(name = "CMOD_PRI3")
    private String cmodPri3;
    @Column(name = "CMOD_PRI4")
    private String cmodPri4;
    @Column(name = "CMOD_PRI5")
    private String cmodPri5;
    @Column(name = "CMOD_PRI6")
    private String cmodPri6;
    @Column(name = "CMOD_SEC")
    private String cmodSec;
    @Column(name = "CMOD_SEC1")
    private String cmodSec1;
    @Column(name = "CMOD_SEC2")
    private String cmodSec2;
    @Column(name = "CMOD_SEC3")
    private String cmodSec3;
    @Column(name = "CMOD_SEC4")
    private String cmodSec4;
    @Column(name = "CMOD_SEC5")
    private String cmodSec5;
    @Column(name = "CMOD_EBA")
    private String cmodEba;
    @Column(name = "CMOD_EBA1")
    private String cmodEba1;
    @Column(name = "CMOD_EBA2")
    private String cmodEba2;
    @Column(name = "CMOD_EBA3")
    private String cmodEba3;
    @Column(name = "CMOD_EBE")
    private String cmodEbe;
    @Column(name = "CMOD_EBE1")
    private String cmodEbe1;
    @Column(name = "CMOD_EBE2")
    private String cmodEbe2;
    @Column(name = "CMOD_EPT")
    private String cmodEpt;
    @Column(name = "CMOD_EPT1")
    private String cmodEpt1;
    @Column(name = "CMOD_EPT2")
    private String cmodEpt2;
    @Column(name = "CMOD_IST")
    private String cmodIst;
    @Column(name = "CMOD_ISP")
    private String cmodIsp;
    @Column(name = "CMOD_ESFA")
    private String cmodEsfa;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne(optional = false)
    private Ie2015Cabecera ie2015Cabecera;

    public Ie2015Establecimientos() {
    }

    public Ie2015Establecimientos(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Ie2015Establecimientos(Long idEnvio, String nro, String cuadro) {
        this.idEnvio = idEnvio;
        this.nro = nro;
        this.cuadro = cuadro;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "NRO")
    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    @XmlElement(name = "CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    @XmlElement(name = "CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name = "TIPOLOCAL")
    public String getTipolocal() {
        return tipolocal;
    }

    public void setTipolocal(String tipolocal) {
        this.tipolocal = tipolocal;
    }

    @XmlElement(name = "NOMESTAB")
    public String getNomestab() {
        return nomestab;
    }

    public void setNomestab(String nomestab) {
        this.nomestab = nomestab;
    }

    @XmlElement(name = "DIRECCION")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @XmlElement(name = "TELEFONO")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @XmlElement(name = "SUMENERG")
    public String getSumenerg() {
        return sumenerg;
    }

    public void setSumenerg(String sumenerg) {
        this.sumenerg = sumenerg;
    }

    @XmlElement(name = "PROVSUMI")
    public String getProvsumi() {
        return provsumi;
    }

    public void setProvsumi(String provsumi) {
        this.provsumi = provsumi;
    }

    @XmlElement(name = "CMOD_INI")
    public String getCmodIni() {
        return cmodIni;
    }

    public void setCmodIni(String cmodIni) {
        this.cmodIni = cmodIni;
    }

    @XmlElement(name = "CMOD_INI1")
    public String getCmodIni1() {
        return cmodIni1;
    }

    public void setCmodIni1(String cmodIni1) {
        this.cmodIni1 = cmodIni1;
    }

    @XmlElement(name = "CMOD_INI2")
    public String getCmodIni2() {
        return cmodIni2;
    }

    public void setCmodIni2(String cmodIni2) {
        this.cmodIni2 = cmodIni2;
    }

    @XmlElement(name = "CMOD_PRI")
    public String getCmodPri() {
        return cmodPri;
    }

    public void setCmodPri(String cmodPri) {
        this.cmodPri = cmodPri;
    }

    @XmlElement(name = "CMOD_PRI1")
    public String getCmodPri1() {
        return cmodPri1;
    }

    public void setCmodPri1(String cmodPri1) {
        this.cmodPri1 = cmodPri1;
    }

    @XmlElement(name = "CMOD_PRI2")
    public String getCmodPri2() {
        return cmodPri2;
    }

    public void setCmodPri2(String cmodPri2) {
        this.cmodPri2 = cmodPri2;
    }

    @XmlElement(name = "CMOD_PRI3")
    public String getCmodPri3() {
        return cmodPri3;
    }

    public void setCmodPri3(String cmodPri3) {
        this.cmodPri3 = cmodPri3;
    }

    @XmlElement(name = "CMOD_PRI4")
    public String getCmodPri4() {
        return cmodPri4;
    }

    public void setCmodPri4(String cmodPri4) {
        this.cmodPri4 = cmodPri4;
    }

    @XmlElement(name = "CMOD_PRI5")
    public String getCmodPri5() {
        return cmodPri5;
    }

    public void setCmodPri5(String cmodPri5) {
        this.cmodPri5 = cmodPri5;
    }

    @XmlElement(name = "CMOD_PRI6")
    public String getCmodPri6() {
        return cmodPri6;
    }

    public void setCmodPri6(String cmodPri6) {
        this.cmodPri6 = cmodPri6;
    }

    @XmlElement(name = "CMOD_SEC")
    public String getCmodSec() {
        return cmodSec;
    }

    public void setCmodSec(String cmodSec) {
        this.cmodSec = cmodSec;
    }

    @XmlElement(name = "CMOD_SEC1")
    public String getCmodSec1() {
        return cmodSec1;
    }

    public void setCmodSec1(String cmodSec1) {
        this.cmodSec1 = cmodSec1;
    }

    @XmlElement(name = "CMOD_SEC2")
    public String getCmodSec2() {
        return cmodSec2;
    }

    public void setCmodSec2(String cmodSec2) {
        this.cmodSec2 = cmodSec2;
    }

    @XmlElement(name = "CMOD_SEC3")
    public String getCmodSec3() {
        return cmodSec3;
    }

    public void setCmodSec3(String cmodSec3) {
        this.cmodSec3 = cmodSec3;
    }

    @XmlElement(name = "CMOD_SEC4")
    public String getCmodSec4() {
        return cmodSec4;
    }

    public void setCmodSec4(String cmodSec4) {
        this.cmodSec4 = cmodSec4;
    }

    @XmlElement(name = "CMOD_SEC5")
    public String getCmodSec5() {
        return cmodSec5;
    }

    public void setCmodSec5(String cmodSec5) {
        this.cmodSec5 = cmodSec5;
    }

    @XmlElement(name = "CMOD_EBA")
    public String getCmodEba() {
        return cmodEba;
    }

    public void setCmodEba(String cmodEba) {
        this.cmodEba = cmodEba;
    }

    @XmlElement(name = "CMOD_EBA1")
    public String getCmodEba1() {
        return cmodEba1;
    }

    public void setCmodEba1(String cmodEba1) {
        this.cmodEba1 = cmodEba1;
    }

    @XmlElement(name = "CMOD_EBA2")
    public String getCmodEba2() {
        return cmodEba2;
    }

    public void setCmodEba2(String cmodEba2) {
        this.cmodEba2 = cmodEba2;
    }

    @XmlElement(name = "CMOD_EBA3")
    public String getCmodEba3() {
        return cmodEba3;
    }

    public void setCmodEba3(String cmodEba3) {
        this.cmodEba3 = cmodEba3;
    }

    @XmlElement(name = "CMOD_EBE")
    public String getCmodEbe() {
        return cmodEbe;
    }

    public void setCmodEbe(String cmodEbe) {
        this.cmodEbe = cmodEbe;
    }

    @XmlElement(name = "CMOD_EBE1")
    public String getCmodEbe1() {
        return cmodEbe1;
    }

    public void setCmodEbe1(String cmodEbe1) {
        this.cmodEbe1 = cmodEbe1;
    }

    @XmlElement(name = "CMOD_EBE2")
    public String getCmodEbe2() {
        return cmodEbe2;
    }

    public void setCmodEbe2(String cmodEbe2) {
        this.cmodEbe2 = cmodEbe2;
    }

    @XmlElement(name = "CMOD_EPT")
    public String getCmodEpt() {
        return cmodEpt;
    }

    public void setCmodEpt(String cmodEpt) {
        this.cmodEpt = cmodEpt;
    }

    @XmlElement(name = "CMOD_EPT1")
    public String getCmodEpt1() {
        return cmodEpt1;
    }

    public void setCmodEpt1(String cmodEpt1) {
        this.cmodEpt1 = cmodEpt1;
    }

    @XmlElement(name = "CMOD_EPT2")
    public String getCmodEpt2() {
        return cmodEpt2;
    }

    public void setCmodEpt2(String cmodEpt2) {
        this.cmodEpt2 = cmodEpt2;
    }

    @XmlElement(name = "CMOD_IST")
    public String getCmodIst() {
        return cmodIst;
    }

    public void setCmodIst(String cmodIst) {
        this.cmodIst = cmodIst;
    }

    @XmlElement(name = "CMOD_ISP")
    public String getCmodIsp() {
        return cmodIsp;
    }

    public void setCmodIsp(String cmodIsp) {
        this.cmodIsp = cmodIsp;
    }

    @XmlElement(name = "CMOD_ESFA")
    public String getCmodEsfa() {
        return cmodEsfa;
    }

    public void setCmodEsfa(String cmodEsfa) {
        this.cmodEsfa = cmodEsfa;
    }

    @XmlTransient
    public Ie2015Cabecera getIe2015Cabecera() {
        return ie2015Cabecera;
    }

    public void setIe2015Cabecera(Ie2015Cabecera ie2015Cabecera) {
        this.ie2015Cabecera = ie2015Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ie2015Establecimientos)) {
            return false;
        }
        Ie2015Establecimientos other = (Ie2015Establecimientos) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2015.Ie2015Establecimientos[idEnvio=" + idEnvio + "]";
    }

}
