/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2018;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JBEDRILLANA
 */
@Entity
@Table(name = "local2018_sec203")
public class Local2018Sec203 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Column(name = "P203_1")
    private Integer p2031;
    @Column(name = "P203_2")
    private Integer p2032;
    @Column(name = "P203_3")
    private String p2033;
    @Column(name = "P203_4")
    private Integer p2034;
    @Column(name = "P203_5")
    private String p2035;
    @Column(name = "P203_6")
    private String p2036;
    @Column(name = "P203_7")
    private String p2037;
    @Column(name = "P203_8")
    private String p2038;
    @Column(name = "P203_9")
    private String p2039;
    @Column(name = "P203_10")
    private String p20310;
    @Column(name = "P203_11")
    private String p20311;
    @Column(name = "P203_12")
    private String p20312;
    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Local2018Cabecera local2018Cabecera;

    public Local2018Sec203() {
    }

    public Local2018Sec203(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name = "P203_1")
    public Integer getP2031() {
        return p2031;
    }

    public void setP2031(Integer p2031) {
        this.p2031 = p2031;
    }

    @XmlElement(name = "P203_2")
    public Integer getP2032() {
        return p2032;
    }

    public void setP2032(Integer p2032) {
        this.p2032 = p2032;
    }

    @XmlElement(name = "P203_3")
    public String getP2033() {
        return p2033;
    }

    public void setP2033(String p2033) {
        this.p2033 = p2033;
    }

    @XmlElement(name = "P203_4")
    public Integer getP2034() {
        return p2034;
    }

    public void setP2034(Integer p2034) {
        this.p2034 = p2034;
    }

    @XmlElement(name = "P203_5")
    public String getP2035() {
        return p2035;
    }

    public void setP2035(String p2035) {
        this.p2035 = p2035;
    }

    @XmlElement(name = "P203_6")
    public String getP2036() {
        return p2036;
    }

    public void setP2036(String p2036) {
        this.p2036 = p2036;
    }

    @XmlElement(name = "P203_7")
    public String getP2037() {
        return p2037;
    }

    public void setP2037(String p2037) {
        this.p2037 = p2037;
    }

    @XmlElement(name = "P203_8")
    public String getP2038() {
        return p2038;
    }

    public void setP2038(String p2038) {
        this.p2038 = p2038;
    }

    @XmlElement(name = "P203_9")
    public String getP2039() {
        return p2039;
    }

    public void setP2039(String p2039) {
        this.p2039 = p2039;
    }

    @XmlElement(name = "P203_10")
    public String getP20310() {
        return p20310;
    }

    public void setP20310(String p20310) {
        this.p20310 = p20310;
    }

    @XmlElement(name = "P203_11")
    public String getP20311() {
        return p20311;
    }

    public void setP20311(String p20311) {
        this.p20311 = p20311;
    }

    @XmlElement(name = "P203_12")
    public String getP20312() {
        return p20312;
    }

    public void setP20312(String p20312) {
        this.p20312 = p20312;
    }

    @XmlTransient
    public Local2018Cabecera getLocal2018Cabecera() {
        return local2018Cabecera;
    }

    public void setLocal2018Cabecera(Local2018Cabecera local2018Cabecera) {
        this.local2018Cabecera = local2018Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local2018Sec203)) {
            return false;
        }
        Local2018Sec203 other = (Local2018Sec203) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.eol.domain.censo2018.Local2018Sec203[idEnvio=" + idEnvio + "]";
    }

}
