package pe.gob.minedu.escale.eol.estadistica.ejb.censo2015;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2015.Matricula2015Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class Matricula2015Facade extends AbstractFacade<Matricula2015Cabecera> {

	public static final String CEDULA_MATRICULA = "MATRICULA";
	static final Logger LOGGER = Logger.getLogger(Matricula2015Facade.class.getName());
	public final static String CEDULA_01A = "c01a";
	public final static String CEDULA_02A = "c02a";
	public final static String CEDULA_03A = "c03a";
	public final static String CEDULA_04A = "c04a";
	public final static String CEDULA_05A = "c05a";
	public final static String CEDULA_06A = "c06a";
	public final static String CEDULA_07A = "c07a";
	public final static String CEDULA_08A = "c08a";
	public final static String CEDULA_09A = "c09a";

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public Matricula2015Facade() {
		super(Matricula2015Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Matricula2015Cabecera cedula) {
		String sql = "UPDATE Matricula2015Cabecera a SET a.ultimo=false WHERE a.codMod=:codMod AND a.anexo=:anexo AND a.nroced=:nroCed ";
		Query query = em.createQuery(sql);
		query.setParameter("codMod", cedula.getCodMod());
		query.setParameter("anexo", cedula.getAnexo());
		query.setParameter("nroCed", cedula.getNroced());
		query.executeUpdate();
		/*
		 * OM
		 * 
		 * if (cedula.getDetalleMatricula() != null) { Set<String> keys =
		 * cedula.getDetalleMatricula().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2015Matricula detalle = cedula.getDetalleMatricula().get(key);
		 * detalle.setMatricula2015Cabecera(cedula); List<Matricula2015MatriculaFila>
		 * filas = detalle.getMatricula2015MatriculaFilaList(); for
		 * (Matricula2015MatriculaFila fila : filas) {
		 * fila.setMatricula2015Matricula(detalle); } } }
		 * 
		 * if (cedula.getDetalleSeccion() != null) { Set<String> keys =
		 * cedula.getDetalleSeccion().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2015Seccion detalle = cedula.getDetalleSeccion().get(key);
		 * detalle.setMatricula2015Cabecera(cedula); List<Matricula2015SeccionFila>
		 * filas = detalle.getMatricula2015SeccionFilaList(); for
		 * (Matricula2015SeccionFila fila : filas) {
		 * fila.setMatricula2015Seccion(detalle); } } }
		 * 
		 * if (cedula.getDetalleRecursos() != null) { Set<String> keys =
		 * cedula.getDetalleRecursos().keySet(); for (Iterator<String> it =
		 * keys.iterator(); it.hasNext();) { String key = it.next();
		 * Matricula2015Recursos detalle = cedula.getDetalleRecursos().get(key);
		 * detalle.setMatricula2015Cabecera(cedula); List<Matricula2015RecursosFila>
		 * filas = detalle.getMatricula2015RecursosFilaList(); for
		 * (Matricula2015RecursosFila fila : filas) {
		 * fila.setMatricula2015Recursos(detalle); } } }
		 * 
		 * if (cedula.getMatricula2015PersonalList() != null) { for
		 * (Matricula2015Personal per : cedula.getMatricula2015PersonalList()) {
		 * per.setMatricula2015Cabecera(cedula); } } if
		 * (cedula.getMatricula2015PerifericosList() != null) { for
		 * (Matricula2015Perifericos peri : cedula.getMatricula2015PerifericosList()) {
		 * peri.setMatricula2015Cabecera(cedula); } } if
		 * (cedula.getMatricula2015LocalpronoeiList() != null) { for
		 * (Matricula2015Localpronoei loc : cedula.getMatricula2015LocalpronoeiList()) {
		 * loc.setMatricula2015Cabecera(cedula); } } if
		 * (cedula.getMatricula2015CarrerasList() != null) { for (Matricula2015Carreras
		 * carr : cedula.getMatricula2015CarrerasList()) {
		 * carr.setMatricula2015Cabecera(cedula); } } if
		 * (cedula.getMatricula2015SaaneeList() != null) { for (Matricula2015Saanee saan
		 * : cedula.getMatricula2015SaaneeList()) {
		 * saan.setMatricula2015Cabecera(cedula); } }
		 */
		cedula.setUltimo(true);
		if (cedula.getFechaEnvio() == null)
			cedula.setFechaEnvio(new Date());

		em.persist(cedula);
		em.flush();
	}

}
