package pe.gob.minedu.escale.eol.estadistica.domain.censo2017;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Matricula2017SeccionMapAdapter.Matricula2017SeccionList;

public class Matricula2017SeccionMapAdapter extends XmlAdapter<Matricula2017SeccionList, Map<String, Matricula2017Seccion>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2017SeccionMapAdapter.class.getName());

    static class Matricula2017SeccionList {

        private List<Matricula2017Seccion> detalle;

        private Matricula2017SeccionList(ArrayList<Matricula2017Seccion> lista) {
            detalle = lista;
        }

        public Matricula2017SeccionList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2017Seccion> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2017Seccion> detalle) {
            this.detalle = detalle;
        }
    }


    @Override
    public Map<String, Matricula2017Seccion> unmarshal(Matricula2017SeccionList v) throws Exception {

        Map<String, Matricula2017Seccion> map = new HashMap<String, Matricula2017Seccion>();
        for (Matricula2017Seccion detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2017SeccionList marshal(Map<String, Matricula2017Seccion> v) throws Exception {
        if(v==null)return null;
        Set<String> keySet = v.keySet();

        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2017Seccion> lista = new ArrayList<Matricula2017Seccion>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2017Seccion $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2017SeccionList list = new Matricula2017SeccionList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }

}
