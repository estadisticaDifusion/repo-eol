
 
package pe.gob.minedu.escale.eol.estadistica.domain.censo2014;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulaMatricula")
@Entity
@Table(name = "matricula2014_cabecera")
public class Matricula2014Cabecera implements Serializable {
    private static final long serialVersionUID = 1L;
    public final static String CEDULA_01A = "c01a";
    public final static String CEDULA_02A = "c02a";
    public final static String CEDULA_03A = "c03a";
    public final static String CEDULA_04A = "c04a";    
    public final static String CEDULA_05A = "c05a";
    public final static String CEDULA_06A = "c06a";
    public final static String CEDULA_07A = "c07a";
    public final static String CEDULA_08A = "c08a";
    public final static String CEDULA_09A = "c09a";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    @Basic(optional = false)
    @Column(name = "COD_MOD")
    private String codMod;
    @Basic(optional = false)
    @Column(name = "ANEXO")
    private String anexo;
    @Basic(optional = false)
    @Column(name = "CODLOCAL")
    private String codlocal;        
    @Column(name = "GESTION")
    private String gestion;
    
    @Column(name = "CEN_EDU")
    private String cenEdu;

    @Column(name = "TELEFONO")
    private String telefono;    
    @Column(name = "NROCED")
    private String nroCed;

    @Column(name = "ID_UBIGEO")
    private String idUbigeo;

    @Column(name = "DISTRITO")
    private String distrito;

    @Column(name = "RUC")
    private String ruc;
    @Basic(optional = false)

    @Column(name = "NIV_MOD")
    private String nivMod;

    @Column(name = "CODUGEL")
    private String codugel;
    @Column(name = "FORMATEN")
    private String formaten;
    @Column(name = "MODATEN")
    private String modaten;

    @Column(name = "TIPOPROG")
    private String tipoprog;
    @Column(name = "TIPONEE")
    private String tiponee;
    @Column(name = "CICLO_1")
    private String ciclo1;  
    @Column(name = "CICLO_2")
    private String ciclo2;
    @Column(name = "CICLO_3")
    private String ciclo3;
    @Column(name = "DNI_CORD")
    private String dniCord;
    @Column(name = "SEANEXO")
    private String seanexo;    
    @Column(name = "TIPOIESUP")
    private String tipoiesup;
    @Column(name = "LOCALIDAD")
    private String localidad;
    @Column(name = "EGESTORA")
    private String egestora;
    @Column(name = "TIPODFINA_1")
    private String tipodfina1;    
    @Column(name = "TIPODFINA_2")
    private String tipodfina2;
    @Column(name = "TIPODFINA_3")
    private String tipodfina3;
    @Column(name = "TIPODFINA_4")
    private String tipodfina4;
    @Column(name = "TIPODFINA_5")
    private String tipodfina5;
    @Column(name = "TIPODFINA_6")
    private String tipodfina6;        
    @Column(name = "NUMRES")
    private String numres;
    @Column(name = "FECHARES_DD")
    private String fecharesDD;
    @Column(name = "FECHARES_MM")
    private String fecharesMM;
    @Column(name = "FECHARES_AA")
    private String fecharesAA;
    @Column(name = "P101_DD")
    private String p101Dd;
    @Column(name = "P101_MM")
    private String p101Mm;
    @Column(name = "P101_C5")
    private String p101C5;
    @Column(name = "P101_C5_N")
    private String p101C5N;
    @Column(name = "P101_C7")
    private String p101C7;
    @Column(name = "P101_C7_LEY")
    private String p101C7Ley;
    @Column(name = "P101_C8_11")
    private String p101C811;
    @Column(name = "P101_C8_12")
    private String p101C812;
    @Column(name = "P101_C8_21")
    private String p101C821;
    @Column(name = "P101_C8_22")
    private String p101C822;
    @Column(name = "P101_C8_23")
    private String p101C823;
    @Column(name = "P101_C8_30")
    private String p101C830;
    @Column(name = "P101_C8_40")
    private String p101C840;
    @Column(name = "P101_C8_50")
    private String p101C850;
    @Column(name = "P102_DD")
    private String p102Dd;
    @Column(name = "P102_MM")
    private String p102Mm;
    @Column(name = "P102_C5")
    private String p102C5;
    @Column(name = "P102_C5_LEY")
    private String p102C5Ley;
    @Column(name = "P102_C8")
    private String p102C8;
    @Column(name = "P103_1HI_H")
    private String p1031hiH;
    @Column(name = "P103_1HI_M")
    private String p1031hiM;
    @Column(name = "P103_1HT_H")
    private String p1031htH;
    @Column(name = "P103_1HT_M")
    private String p1031htM;
    @Column(name = "P103_2HI_H")
    private String p1032hiH;
    @Column(name = "P103_2HI_M")
    private String p1032hiM;
    @Column(name = "P103_2HT_H")
    private String p1032htH;
    @Column(name = "P103_2HT_M")
    private String p1032htM;
    @Column(name = "P103_C4")
    private String p103C4;
    @Column(name = "P104_C4")
    private String p104C4;
    @Column(name = "P105_C4")
    private String p105C4;
    @Column(name = "P106_C4")
    private String p106C4;
    @Column(name = "P107_C4")
    private String p107C4;
    @Column(name = "P108_C4")
    private String p108C4;
    @Column(name = "P109_C4")
    private String p109C4;
    @Column(name = "P104_C123")
    private String p104C123;
    @Column(name = "ACTIV_IE_1")
    private String activIe1;
    @Column(name = "ACTIV_IE_2")
    private String activIe2;
    @Column(name = "ACTIV_IE_3")
    private String activIe3;
    @Column(name = "ACTIV_IE_4")
    private String activIe4;
    @Column(name = "ACTIV_IE_5")
    private String activIe5;
    @Column(name = "ACTIV_IE_6")
    private String activIe6;
    @Column(name = "ACTIV_IE_7")
    private String activIe7;
    @Column(name = "ACTIV_IE_8")
    private String activIe8;
    @Column(name = "SERV_IE_1")
    private String servIe1;
    @Column(name = "SERV_IE_2")
    private String servIe2;
    @Column(name = "SERV_IE_3")
    private String servIe3;
    @Column(name = "SERV_IE_4")
    private String servIe4;
    @Column(name = "P107")
    private String p107;
    @Column(name = "P108_C2_H1")
    private String p108C2H1;
    @Column(name = "P108_C2_M1")
    private String p108C2M1;
    @Column(name = "P108_C2_H2")
    private String p108C2H2;
    @Column(name = "P108_C2_M2")
    private String p108C2M2;
    @Column(name = "P208_2010_1")
    private String p20820101;
    @Column(name = "P208_2010_2")
    private String p20820102;
    @Column(name = "P208_2011_1")
    private String p20820111;
    @Column(name = "P208_2011_2")
    private String p20820112;
    @Column(name = "P208_2012_1")
    private String p20820121;
    @Column(name = "P208_2012_2")
    private String p20820122;
    @Column(name = "P208_2013_1")
    private String p20820131;
    @Column(name = "P208_2013_2")
    private String p20820132;
    @Column(name = "P108_C3")
    private String p108C3;
    @Column(name = "P108_C3_1")
    private String p108C31;
    @Column(name = "P108_C3_1RD")
    private String p108C31rd;
    @Column(name = "P108_C3_2")
    private String p108C32;
    @Column(name = "P108_C3_2RD")
    private String p108C32rd;
    @Column(name = "P109_1")
    private String p1091;
    @Column(name = "P109_1_ESP")
    private String p1091Esp;
    @Column(name = "P109_2")
    private String p1092;
    @Column(name = "P109_3")
    private String p1093;
    @Column(name = "P109_3_ESP")
    private String p1093Esp;
    @Column(name = "P109_4")
    private String p1094;
    @Column(name = "P109_4_ESP")
    private String p1094Esp;
    @Column(name = "P109_5")
    private String p1095;
    @Column(name = "P109_5_RES")
    private String p1095Res;
    @Column(name = "P109_6")
    private String p1096;
    @Column(name = "P109_6_NRO")
    private Integer p1096Nro;
    @Column(name = "P109_7")
    private String p1097;
    @Column(name = "P109_8")
    private String p1098;
    @Column(name = "P109_8_ESP")
    private String p1098Esp;
    @Column(name = "P109_C3_1")
    private String p109C31;
    @Column(name = "P109_C3_2")
    private String p109C32;
    @Column(name = "P109_C3_3")
    private String p109C33;
    @Column(name = "P109_C3_4")
    private String p109C34;
    @Column(name = "P109_C3_5")
    private String p109C35;
    @Column(name = "P109_C3_6")
    private String p109C36;
    @Column(name = "P110_123")
    private String p110123;
    @Column(name = "P401_123")
    private String p401123;

    @Column(name = "P402_D123")
    private String p402D123;

    @Column(name = "P402_M123")
    private String p402M123;
    @Column(name = "P402_A123")
    private String p402A123;
    @Column(name = "P404_123")
    private String p404123;

    @Column(name = "P405_D123")
    private String p405D123;

    @Column(name = "P405_M123")
    private String p405M123;

    @Column(name = "P405_A123")
    private String p405A123;
    @Column(name = "P408_C3")
    private String p408C3;
    @Column(name = "BBTK_AULA_0")
    private String bbtkAula0;
    @Column(name = "BBTK_AULA_1")
    private String bbtkAula1;
    @Column(name = "BBTK_AULA_2")
    private String bbtkAula2;
    @Column(name = "BBTK_AULA_3")
    private String bbtkAula3;
    @Column(name = "BBTK_AULA_4")
    private String bbtkAula4;
    @Column(name = "BBTK_AULA_5")
    private String bbtkAula5;
    @Column(name = "BBTK_AULA_N")
    private String bbtkAulaN;

    @Column(name = "P409_C3_D")
    private String p409C3D;

    @Column(name = "P409_C3_M")
    private String p409C3M;

    @Column(name = "P409_C3_A")
    private String p409C3A;
    @Column(name = "P401_C4_1")
    private String p401C41;
    @Column(name = "P401_C4_2")
    private String p401C42;
    @Column(name = "P401_C4_3")
    private String p401C43;
    @Column(name = "P401_C4_N")
    private String p401C4N;
    @Column(name = "P402_C4")
    private String p402C4;
    @Column(name = "P406_C4")
    private String p406C4;
    @Column(name = "P407_C4_1")
    private String p407C41;
    @Column(name = "P407_C4_2")
    private String p407C42;
    @Column(name = "P407_C4_3")
    private String p407C43;
    @Column(name = "P407_C4_4")
    private String p407C44;
    @Column(name = "P407_C4_5")
    private String p407C45;
    @Column(name = "P407_C4_6")
    private String p407C46;
    @Column(name = "P407_C4_7")
    private String p407C47;
    @Column(name = "P407_C4_8")
    private String p407C48;
    @Column(name = "P407_C4_9")
    private String p407C49;

    @Column(name = "P407_C4_9E")
    private String p407C49e;
    @Column(name = "P407_C4_10")
    private String p407C410;

    @Column(name = "P407_C4_10E")
    private String p407C410e;
    @Column(name = "P407_C4_11N")
    private String p407C411n;
    @Column(name = "P502_13")
    private String p50213;
    @Column(name = "P507_C3")
    private String p507C3;

    @Column(name = "P508_C3_AP")
    private String p508C3Ap;

    @Column(name = "P508_C3_NO")
    private String p508C3No;

    @Column(name = "P508_C3_DNI")
    private String p508C3Dni;
    @Column(name = "P510_C3_1")
    private String p510C31;
    @Column(name = "P510_C3_2")
    private String p510C32;
    @Column(name = "P510_C3_3")
    private String p510C33;
    @Column(name = "P510_C3_4")
    private String p510C34;
    @Column(name = "P510_C3_5")
    private String p510C35;
    @Column(name = "P510_C3_6")
    private String p510C36;

    @Column(name = "P510_C3_6E")
    private String p510C36e;

    @Column(name = "ANOTACIONES")
    private String anotaciones;

    @Column(name = "DIR_APE")
    private String dirApe;

    @Column(name = "DIR_NOM")
    private String dirNom;
    @Column(name = "DIR_SITUA")
    private String dirSitua;

    @Column(name = "DIR_DNI")
    private String dirDni;

    @Column(name = "DIR_EMAIL")
    private String dirEmail;

    @Column(name = "DIR_FONO")
    private String dirFono;

    @Column(name = "SDIR_APE")
    private String sdirApe;

    @Column(name = "SDIR_NOM")
    private String sdirNom;
    @Column(name = "SDIR_SITUA")
    private String sdirSitua;
    
    @Column(name = "SDIR_DNI")
    private String sdirDni;
    
    @Column(name = "SDIR_EMAIL")
    private String sdirEmail;
    
    @Column(name = "SDIR_FONO")
    private String sdirFono;
  
    @Column(name = "REC_CED_DIA")
    private String recCedDia;
    
    @Column(name = "REC_CED_MES")
    private String recCedMes;
    
    @Column(name = "CUL_CED_DIA")
    private String culCedDia;
    
    @Column(name = "CUL_CED_MES")
    private String culCedMes;
   
    @Column(name = "FUENTE")
    private String fuente;
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;

    @Column(name = "VERSION")
    private String version;
    @Column(name = "TIPO_ENVIO")
    private String tipoEnvio;
    @Column(name = "ULTIMO")
    private Boolean ultimo;


/*
    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2014Personal> matricula2014PersonalList;
    
    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2014Perifericos> matricula2014PerifericosList;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2014Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String,Matricula2014Recursos> detalleRecursos;
    
    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2014Cabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Map<String,Matricula2014Docente> detalleDocente;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Map<String,Matricula2014Matricula> detalleMatricula;

    @MapKeyColumn(name = "cuadro", length = 5)
    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Map<String,Matricula2014Seccion> detalleSeccion;
    
    
    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2014Localpronoei> matricula2014LocalpronoeiList;

    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2014Coordpronoei> matricula2014CoordpronoeiList;

    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
     private List<Matricula2014Saanee> matricula2014SaaneeList;

    @OneToMany(mappedBy = "matricula2014Cabecera",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Matricula2014CetproCurso> matricula2014CetproCursoList;

*/

    @Transient
    private long token;


    @Transient
    private String msg;

    @Transient
    private String estadoRpt;


    public Matricula2014Cabecera() {
    }

    public Matricula2014Cabecera(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Matricula2014Cabecera(Long idEnvio, String codMod, String anexo, String codlocal, String idUbigeo, String distrito, String nivMod, String codugel) {
        this.idEnvio = idEnvio;
        this.codMod = codMod;
        this.anexo = anexo;
        this.codlocal = codlocal;
        this.idUbigeo = idUbigeo;
        this.distrito = distrito;
        this.nivMod = nivMod;
        this.codugel = codugel;
    }

    @XmlAttribute
    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="COD_MOD")
    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    @XmlElement(name="ANEXO")
    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    @XmlElement(name="CODLOCAL")
    public String getCodlocal() {
        return codlocal;
    }

    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }

    @XmlElement(name="TIPOPROG")
    public String getTipoprog() {
        return tipoprog;
    }

    public void setTipoprog(String tipoprog) {
        this.tipoprog = tipoprog;
    }
    
    @XmlElement(name="TIPOIESUP")
    public String getTipoiesup() {
        return tipoiesup;
    }

    public void setTipoiesup(String tipoiesup) {
        this.tipoiesup = tipoiesup;
    }
    @XmlElement(name="GESTION")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @XmlElement(name="CEN_EDU")
    public String getCenEdu() {
        return cenEdu;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    @XmlElement(name="TELEFONO")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
  
   @XmlElement(name="ID_UBIGEO")
    public String getIdUbigeo() {
        return idUbigeo;
    }

    public void setIdUbigeo(String idUbigeo) {
        this.idUbigeo = idUbigeo;
    }

    @XmlElement(name="DISTRITO")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @XmlElement(name="NIV_MOD")
    public String getNivMod() {
        return nivMod;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    @XmlElement(name="CODUGEL")
    public String getCodugel() {
        return codugel;
    }

    public void setCodugel(String codugel) {
        this.codugel = codugel;
    }

    @XmlElement(name="FORMATEN")
    public String getFormaten() {
        return formaten;
    }

    public void setFormaten(String formaten) {
        this.formaten = formaten;
    }

    @XmlElement(name="TIPONEE")
    public String getTiponee() {
        return tiponee;
    }

    public void setTiponee(String tiponee) {
        this.tiponee = tiponee;
    }   

    @XmlElement(name="SEANEXO")
    public String getSeanexo() {
        return seanexo;
    }

    public void setSeanexo(String seanexo) {
        this.seanexo = seanexo;
    }    
   
    @XmlElement(name="P101_DD")
    public String getP101Dd() {
        return p101Dd;
    }

    public void setP101Dd(String p101Dd) {
        this.p101Dd = p101Dd;
    }

    @XmlElement(name="P101_MM")
    public String getP101Mm() {
        return p101Mm;
    }

    public void setP101Mm(String p101Mm) {
        this.p101Mm = p101Mm;
    }      
    
    @XmlElement(name="P101_C8_11")
    public String getP101C811() {
        return p101C811;
    }

    public void setP101C811(String p101C811) {
        this.p101C811 = p101C811;
    }
    
    @XmlElement(name="P101_C8_12")
    public String getP101C812() {
        return p101C812;
    }

    public void setP101C812(String p101C812) {
        this.p101C812 = p101C812;
    }
    
    @XmlElement(name="P101_C8_21")
    public String getP101C821() {
        return p101C821;
    }

    public void setP101C821(String p101C821) {
        this.p101C821 = p101C821;
    }
    
    @XmlElement(name="P101_C8_22")
    public String getP101C822() {
        return p101C822;
    }

    public void setP101C822(String p101C822) {
        this.p101C822 = p101C822;
    }
    
    @XmlElement(name="P101_C8_23")
    public String getP101C823() {
        return p101C823;
    }

    public void setP101C823(String p101C823) {
        this.p101C823 = p101C823;
    }
    
    @XmlElement(name="P101_C8_30")
    public String getP101C830() {
        return p101C830;
    }

    public void setP101C830(String p101C830) {
        this.p101C830 = p101C830;
    }
    @XmlElement(name="P101_C8_40")
    public String getP101C840() {
        return p101C840;
    }

    public void setP101C840(String p101C840) {
        this.p101C840 = p101C840;
    }
    
    @XmlElement(name="P101_C8_50")
    public String getP101C850() {
        return p101C850;
    }

    public void setP101C850(String p101C850) {
        this.p101C850 = p101C850;
    }

    @XmlElement(name="P102_DD")
    public String getP102Dd() {
        return p102Dd;
    }

    public void setP102Dd(String p102Dd) {
        this.p102Dd = p102Dd;
    }

    @XmlElement(name="P102_MM")
    public String getP102Mm() {
        return p102Mm;
    }

    public void setP102Mm(String p102Mm) {
        this.p102Mm = p102Mm;
    }
    
    @XmlElement(name="RUC")
    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    @XmlElement(name="CICLO_1")   
     public String getCiclo1() {
        return ciclo1;
    }

    public void setCiclo1(String ciclo1) {
        this.ciclo1 = ciclo1;
    }

    @XmlElement(name="CICLO_2") 
    public String getCiclo2() {
        return ciclo2;
    }

    public void setCiclo2(String ciclo2) {
        this.ciclo2 = ciclo2;
    }

    @XmlElement(name="CICLO_3") 
    public String getCiclo3() {
        return ciclo3;
    }

    public void setCiclo3(String ciclo3) {
        this.ciclo3 = ciclo3;
    }    
    
    @XmlElement(name="DNI_CORD")
    public String getDniCord() {
        return dniCord;
    }

    public void setDniCord(String dniCord) {
        this.dniCord = dniCord;
    }

    @XmlElement(name="LOCALIDAD")
    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @XmlElement(name="P101_C5")
    public String getP101C5() {
        return p101C5;
    }

    public void setP101C5(String p101C5) {
        this.p101C5 = p101C5;
    }

    @XmlElement(name="P101_C5_N")
    public String getP101C5N() {
        return p101C5N;
    }

    public void setP101C5N(String p101C5N) {
        this.p101C5N = p101C5N;
    }

    @XmlElement(name="P101_C7")
    public String getP101C7() {
        return p101C7;
    }

    public void setP101C7(String p101C7) {
        this.p101C7 = p101C7;
    }

    @XmlElement(name="P101_C7_LEY")
    public String getP101C7Ley() {
        return p101C7Ley;
    }

    public void setP101C7Ley(String p101C7Ley) {
        this.p101C7Ley = p101C7Ley;
    }

    @XmlElement(name="P102_C5")
    public String getP102C5() {
        return p102C5;
    }

    public void setP102C5(String p102C5) {
        this.p102C5 = p102C5;
    }

    @XmlElement(name="P102_C5_LEY")
    public String getP102C5Ley() {
        return p102C5Ley;
    }

    public void setP102C5Ley(String p102C5Ley) {
        this.p102C5Ley = p102C5Ley;
    }

    @XmlElement(name="P102_C8")
    public String getP102C8() {
        return p102C8;
    }

    public void setP102C8(String p102C8) {
        this.p102C8 = p102C8;
    }

    @XmlElement(name="P103_1HI_H")
    public String getP1031hiH() {
        return p1031hiH;
    }

    public void setP1031hiH(String p1031hiH) {
        this.p1031hiH = p1031hiH;
    }

    @XmlElement(name="P103_1HI_M")
    public String getP1031hiM() {
        return p1031hiM;
    }

    public void setP1031hiM(String p1031hiM) {
        this.p1031hiM = p1031hiM;
    }

    @XmlElement(name="P103_1HT_H")
    public String getP1031htH() {
        return p1031htH;
    }

    public void setP1031htH(String p1031htH) {
        this.p1031htH = p1031htH;
    }

    @XmlElement(name="P103_1HT_M")
    public String getP1031htM() {
        return p1031htM;
    }

    public void setP1031htM(String p1031htM) {
        this.p1031htM = p1031htM;
    }

    @XmlElement(name="P103_2HI_H")
    public String getP1032hiH() {
        return p1032hiH;
    }

    public void setP1032hiH(String p1032hiH) {
        this.p1032hiH = p1032hiH;
    }

    @XmlElement(name="P103_2HI_M")
    public String getP1032hiM() {
        return p1032hiM;
    }

    public void setP1032hiM(String p1032hiM) {
        this.p1032hiM = p1032hiM;
    }

    @XmlElement(name="P103_2HT_H")
    public String getP1032htH() {
        return p1032htH;
    }

    public void setP1032htH(String p1032htH) {
        this.p1032htH = p1032htH;
    }

    @XmlElement(name="P103_2HT_M")
    public String getP1032htM() {
        return p1032htM;
    }

    public void setP1032htM(String p1032htM) {
        this.p1032htM = p1032htM;
    }

    @XmlElement(name="P103_C4")
    public String getP103C4() {
        return p103C4;
    }

    public void setP103C4(String p103C4) {
        this.p103C4 = p103C4;
    }

    @XmlElement(name="P104_C4")
    public String getP104C4() {
        return p104C4;
    }

    public void setP104C4(String p104C4) {
        this.p104C4 = p104C4;
    }

    @XmlElement(name="P105_C4")
    public String getP105C4() {
        return p105C4;
    }

    public void setP105C4(String p105C4) {
        this.p105C4 = p105C4;
    }

    @XmlElement(name="P106_C4")
    public String getP106C4() {
        return p106C4;
    }

    public void setP106C4(String p106C4) {
        this.p106C4 = p106C4;
    }

    @XmlElement(name="P107_C4")
    public String getP107C4() {
        return p107C4;
    }

    public void setP107C4(String p107C4) {
        this.p107C4 = p107C4;
    }

    @XmlElement(name="P108_C4")
    public String getP108C4() {
        return p108C4;
    }

    public void setP108C4(String p108C4) {
        this.p108C4 = p108C4;
    }

    @XmlElement(name="P109_C4")
    public String getP109C4() {
        return p109C4;
    }

    public void setP109C4(String p109C4) {
        this.p109C4 = p109C4;
    }

    @XmlElement(name="P104_C123")
    public String getP104C123() {
        return p104C123;
    }

    public void setP104C123(String p104C123) {
        this.p104C123 = p104C123;
    }

    @XmlElement(name="ACTIV_IE_8")
    public String getActivIe8() {
        return activIe8;
    }

    public void setActivIe8(String activIe8) {
        this.activIe8 = activIe8;
    }

    @XmlElement(name="P107")
    public String getP107() {
        return p107;
    }

    public void setP107(String p107) {
        this.p107 = p107;
    }

    @XmlElement(name="P108_C2_H1")
    public String getP108C2H1() {
        return p108C2H1;
    }

    public void setP108C2H1(String p108C2H1) {
        this.p108C2H1 = p108C2H1;
    }

    @XmlElement(name="P108_C2_M1")
    public String getP108C2M1() {
        return p108C2M1;
    }

    public void setP108C2M1(String p108C2M1) {
        this.p108C2M1 = p108C2M1;
    }

    @XmlElement(name="P108_C2_H2")
    public String getP108C2H2() {
        return p108C2H2;
    }

    public void setP108C2H2(String p108C2H2) {
        this.p108C2H2 = p108C2H2;
    }

    @XmlElement(name="P108_C2_M2")
    public String getP108C2M2() {
        return p108C2M2;
    }

    public void setP108C2M2(String p108C2M2) {
        this.p108C2M2 = p108C2M2;
    }

    @XmlElement(name="P208_2010_1")
    public String getP20820101() {
        return p20820101;
    }

    public void setP20820101(String p20820101) {
        this.p20820101 = p20820101;
    }

    @XmlElement(name="P208_2010_2")
    public String getP20820102() {
        return p20820102;
    }

    public void setP20820102(String p20820102) {
        this.p20820102 = p20820102;
    }

    @XmlElement(name="P208_2011_1")
    public String getP20820111() {
        return p20820111;
    }

    public void setP20820111(String p20820111) {
        this.p20820111 = p20820111;
    }

    @XmlElement(name="P208_2011_2")    
    public String getP20820112() {
        return p20820112;
    }

    public void setP20820112(String p20820112) {
        this.p20820112 = p20820112;
    }

    @XmlElement(name="P208_2012_1")
    public String getP20820121() {
        return p20820121;
    }

    public void setP20820121(String p20820121) {
        this.p20820121 = p20820121;
    }

    @XmlElement(name="P208_2012_2")
    public String getP20820122() {
        return p20820122;
    }

    public void setP20820122(String p20820122) {
        this.p20820122 = p20820122;
    }

    @XmlElement(name="P208_2013_1")
    public String getP20820131() {
        return p20820131;
    }

    public void setP20820131(String p20820131) {
        this.p20820131 = p20820131;
    }
    
    @XmlElement(name="P208_2013_2")
    public String getP20820132() {
        return p20820132;
    }

    public void setP20820132(String p20820132) {
        this.p20820132 = p20820132;
    }

    @XmlElement(name="P108_C3")
    public String getP108C3() {
        return p108C3;
    }

    public void setP108C3(String p108C3) {
        this.p108C3 = p108C3;
    }

    @XmlElement(name="P108_C3_1")
    public String getP108C31() {
        return p108C31;
    }

    public void setP108C31(String p108C31) {
        this.p108C31 = p108C31;
    }

    @XmlElement(name="P108_C3_1RD")
    public String getP108C31rd() {
        return p108C31rd;
    }

    public void setP108C31rd(String p108C31rd) {
        this.p108C31rd = p108C31rd;
    }

    @XmlElement(name="P108_C3_2")
    public String getP108C32() {
        return p108C32;
    }

    public void setP108C32(String p108C32) {
        this.p108C32 = p108C32;
    }

    @XmlElement(name="P108_C3_2RD")
    public String getP108C32rd() {
        return p108C32rd;
    }

    public void setP108C32rd(String p108C32rd) {
        this.p108C32rd = p108C32rd;
    }

    @XmlElement(name="P109_1")
    public String getP1091() {
        return p1091;
    }

    public void setP1091(String p1091) {
        this.p1091 = p1091;
    }

    @XmlElement(name="P109_1_ESP")
    public String getP1091Esp() {
        return p1091Esp;
    }

    public void setP1091Esp(String p1091Esp) {
        this.p1091Esp = p1091Esp;
    }

    @XmlElement(name="P109_2")
    public String getP1092() {
        return p1092;
    }

    public void setP1092(String p1092) {
        this.p1092 = p1092;
    }

    @XmlElement(name="P109_3")
    public String getP1093() {
        return p1093;
    }

    public void setP1093(String p1093) {
        this.p1093 = p1093;
    }

    @XmlElement(name="P109_3_ESP")
    public String getP1093Esp() {
        return p1093Esp;
    }

    public void setP1093Esp(String p1093Esp) {
        this.p1093Esp = p1093Esp;
    }

    @XmlElement(name="P109_4")
    public String getP1094() {
        return p1094;
    }

    public void setP1094(String p1094) {
        this.p1094 = p1094;
    }

    @XmlElement(name="P109_4_ESP")
    public String getP1094Esp() {
        return p1094Esp;
    }

    public void setP1094Esp(String p1094Esp) {
        this.p1094Esp = p1094Esp;
    }

    @XmlElement(name="P109_5")
    public String getP1095() {
        return p1095;
    }

    public void setP1095(String p1095) {
        this.p1095 = p1095;
    }

    @XmlElement(name="P109_5_RES")
    public String getP1095Res() {
        return p1095Res;
    }

    public void setP1095Res(String p1095Res) {
        this.p1095Res = p1095Res;
    }

    @XmlElement(name="P109_6")
    public String getP1096() {
        return p1096;
    }

    public void setP1096(String p1096) {
        this.p1096 = p1096;
    }

    @XmlElement(name="P109_6_NRO")
    public Integer getP1096Nro() {
        return p1096Nro;
    }

    public void setP1096Nro(Integer p1096Nro) {
        this.p1096Nro = p1096Nro;
    }

    @XmlElement(name="P109_7")
    public String getP1097() {
        return p1097;
    }

    public void setP1097(String p1097) {
        this.p1097 = p1097;
    }

    @XmlElement(name="P109_8")
    public String getP1098() {
        return p1098;
    }

    public void setP1098(String p1098) {
        this.p1098 = p1098;
    }

    @XmlElement(name="P109_8_ESP")
    public String getP1098Esp() {
        return p1098Esp;
    }

    public void setP1098Esp(String p1098Esp) {
        this.p1098Esp = p1098Esp;
    }

    @XmlElement(name="P109_C3_1")
    public String getP109C31() {
        return p109C31;
    }
    
    public void setP109C31(String p109C31) {
        this.p109C31 = p109C31;
    }

    @XmlElement(name="P109_C3_2")
    public String getP109C32() {
        return p109C32;
    }
    
    public void setP109C32(String p109C32) {
        this.p109C32 = p109C32;
    }

    @XmlElement(name="P109_C3_3")
    public String getP109C33() {
        return p109C33;
    }
    
    public void setP109C33(String p109C33) {
        this.p109C33 = p109C33;
    }

    @XmlElement(name="P109_C3_4")
    public String getP109C34() {
        return p109C34;
    }
    
    public void setP109C34(String p109C34) {
        this.p109C34 = p109C34;
    }

    @XmlElement(name="P109_C3_5")
    public String getP109C35() {
        return p109C35;
    }
    
    public void setP109C35(String p109C35) {
        this.p109C35 = p109C35;
    }

    @XmlElement(name="P109_C3_6")
    public String getP109C36() {
        return p109C36;
    }
    
    public void setP109C36(String p109C36) {
        this.p109C36 = p109C36;
    }

    @XmlElement(name="P110_123")
    public String getP110123() {
        return p110123;
    }
    
    public void setP110123(String p110123) {
        this.p110123 = p110123;
    }

    @XmlElement(name="P401_123")
    public String getP401123() {
        return p401123;
    }
    
    public void setP401123(String p401123) {
        this.p401123 = p401123;
    }

    @XmlElement(name="P402_D123")
    public String getP402D123() {
        return p402D123;
    }
    
    public void setP402D123(String p402D123) {
        this.p402D123 = p402D123;
    }

    @XmlElement(name="P402_M123")
    public String getP402M123() {
        return p402M123;
    }
    
    public void setP402M123(String p402M123) {
        this.p402M123 = p402M123;
    }

    @XmlElement(name="P402_A123")
    public String getP402A123() {
        return p402A123;
    }
    
    public void setP402A123(String p402A123) {
        this.p402A123 = p402A123;
    }

    @XmlElement(name="P404_123")
    public String getP404123() {
        return p404123;
    }
    
    public void setP404123(String p404123) {
        this.p404123 = p404123;
    }

    @XmlElement(name="P405_D123")
    public String getP405D123() {
        return p405D123;
    }
    
    public void setP405D123(String p405D123) {
        this.p405D123 = p405D123;
    }

    @XmlElement(name="P405_M123")
    public String getP405M123() {
        return p405M123;
    }
    
    public void setP405M123(String p405M123) {
        this.p405M123 = p405M123;
    }

    @XmlElement(name="P405_A123")
    public String getP405A123() {
        return p405A123;
    }
    
    public void setP405A123(String p405A123) {
        this.p405A123 = p405A123;
    }

    @XmlElement(name="P408_C3")
    public String getP408C3() {
        return p408C3;
    }
    
    public void setP408C3(String p408C3) {
        this.p408C3 = p408C3;
    }

    @XmlElement(name="P409_C3_D")
    public String getP409C3D() {
        return p409C3D;
    }    
    
    public void setP409C3D(String p409C3D) {
        this.p409C3D = p409C3D;
    }

    @XmlElement(name="P409_C3_M")
    public String getP409C3M() {
        return p409C3M;
    }
    
    public void setP409C3M(String p409C3M) {
        this.p409C3M = p409C3M;
    }

    @XmlElement(name="P409_C3_A")
    public String getP409C3A() {
        return p409C3A;
    }
    
    public void setP409C3A(String p409C3A) {
        this.p409C3A = p409C3A;
    }

    @XmlElement(name="P401_C4_1")
    public String getP401C41() {
        return p401C41;
    }

    public void setP401C41(String p401C41) {
        this.p401C41 = p401C41;
    }

    @XmlElement(name="P401_C4_2")
    public String getP401C42() {
        return p401C42;
    }

    public void setP401C42(String p401C42) {
        this.p401C42 = p401C42;
    }

    @XmlElement(name="P401_C4_3")
    public String getP401C43() {
        return p401C43;
    }

    public void setP401C43(String p401C43) {
        this.p401C43 = p401C43;
    }

    @XmlElement(name="P401_C4_N")
    public String getP401C4N() {
        return p401C4N;
    }

    public void setP401C4N(String p401C4N) {
        this.p401C4N = p401C4N;
    }
    
    @XmlElement(name="P402_C4")
    public String getP402C4() {
        return p402C4;
    }

    public void setP402C4(String p402C4) {
        this.p402C4 = p402C4;
    }

    @XmlElement(name="P406_C4")
    public String getP406C4() {
        return p406C4;
    }

    public void setP406C4(String p406C4) {
        this.p406C4 = p406C4;
    }

    @XmlElement(name="P407_C4_1")
    public String getP407C41() {
        return p407C41;
    }

    public void setP407C41(String p407C41) {
        this.p407C41 = p407C41;
    }

    @XmlElement(name="P407_C4_2")
    public String getP407C42() {
        return p407C42;
    }

    public void setP407C42(String p407C42) {
        this.p407C42 = p407C42;
    }

    @XmlElement(name="P407_C4_3")
    public String getP407C43() {
        return p407C43;
    }

    public void setP407C43(String p407C43) {
        this.p407C43 = p407C43;
    }

    @XmlElement(name="P407_C4_4")
    public String getP407C44() {
        return p407C44;
    }

    public void setP407C44(String p407C44) {
        this.p407C44 = p407C44;
    }

    @XmlElement(name="P407_C4_5")
    public String getP407C45() {
        return p407C45;
    }

    public void setP407C45(String p407C45) {
        this.p407C45 = p407C45;
    }

    @XmlElement(name="P407_C4_6")
    public String getP407C46() {
        return p407C46;
    }

    public void setP407C46(String p407C46) {
        this.p407C46 = p407C46;
    }

    @XmlElement(name="P407_C4_7")
    public String getP407C47() {
        return p407C47;
    }

    public void setP407C47(String p407C47) {
        this.p407C47 = p407C47;
    }

    @XmlElement(name="P407_C4_8")
    public String getP407C48() {
        return p407C48;
    }

    public void setP407C48(String p407C48) {
        this.p407C48 = p407C48;
    }

    @XmlElement(name="P407_C4_9")
    public String getP407C49() {
        return p407C49;
    }

    public void setP407C49(String p407C49) {
        this.p407C49 = p407C49;
    }
    
    @XmlElement(name="P407_C4_9E")
    public String getP407C49e() {
        return p407C49e;
    }

    public void setP407C49e(String p407C49e) {
        this.p407C49e = p407C49e;
    }

    @XmlElement(name="P407_C4_10")
    public String getP407C410() {
        return p407C410;
    }

    public void setP407C410(String p407C410) {
        this.p407C410 = p407C410;
    }
    
    @XmlElement(name="P407_C4_10E")
    public String getP407C410e() {
        return p407C410e;
    }

    public void setP407C410e(String p407C410e) {
        this.p407C410e = p407C410e;
    }

    @XmlElement(name="P407_C4_11N")
    public String getP407C411n() {
        return p407C411n;
    }

    public void setP407C411n(String p407C411n) {
        this.p407C411n = p407C411n;
    }

    @XmlElement(name="P502_13")
    public String getP50213() {
        return p50213;
    }

    public void setP50213(String p50213) {
        this.p50213 = p50213;
    }

    @XmlElement(name="P507_C3")
    public String getP507C3() {
        return p507C3;
    }

    public void setP507C3(String p507C3) {
        this.p507C3 = p507C3;
    }

    @XmlElement(name="P508_C3_AP")
    public String getP508C3Ap() {
        return p508C3Ap;
    }

    public void setP508C3Ap(String p508C3Ap) {
        this.p508C3Ap = p508C3Ap;
    }

    @XmlElement(name="P508_C3_NO")
    public String getP508C3No() {
        return p508C3No;
    }

    public void setP508C3No(String p508C3No) {
        this.p508C3No = p508C3No;
    }

    @XmlElement(name="P508_C3_DNI")
    public String getP508C3Dni() {
        return p508C3Dni;
    }

    public void setP508C3Dni(String p508C3Dni) {
        this.p508C3Dni = p508C3Dni;
    }

    @XmlElement(name="P510_C3_1")
    public String getP510C31() {
        return p510C31;
    }

    public void setP510C31(String p510C31) {
        this.p510C31 = p510C31;
    }
    
    @XmlElement(name="P510_C3_2")
    public String getP510C32() {
        return p510C32;
    }

    public void setP510C32(String p510C32) {
        this.p510C32 = p510C32;
    }

    @XmlElement(name="P510_C3_3")
    public String getP510C33() {
        return p510C33;
    }

    public void setP510C33(String p510C33) {
        this.p510C33 = p510C33;
    }

    @XmlElement(name="P510_C3_4")
    public String getP510C34() {
        return p510C34;
    }

    public void setP510C34(String p510C34) {
        this.p510C34 = p510C34;
    }

    @XmlElement(name="P510_C3_5")
    public String getP510C35() {
        return p510C35;
    }

    public void setP510C35(String p510C35) {
        this.p510C35 = p510C35;
    }

    @XmlElement(name="P510_C3_6")
    public String getP510C36() {
        return p510C36;
    }

    public void setP510C36(String p510C36) {
        this.p510C36 = p510C36;
    }
    
    @XmlElement(name="P510_C3_6E")
    public String getP510C36e() {
        return p510C36e;
    }

    public void setP510C36e(String p510C36e) {
        this.p510C36e = p510C36e;
    }
    
    @XmlElement(name="TIPOENVIO")
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }
    
    
    @XmlElement(name="ACTIV_IE_1")
    public String getActivIe1() {
        return activIe1;
    }

    public void setActivIe1(String activIe1) {
        this.activIe1 = activIe1;
    }
    
    @XmlElement(name="ACTIV_IE_2")
    public String getActivIe2() {
        return activIe2;
    }

    public void setActivIe2(String activIe2) {
        this.activIe2 = activIe2;
    }
    @XmlElement(name="ACTIV_IE_3")
    public String getActivIe3() {
        return activIe3;
    }

    public void setActivIe3(String activIe3) {
        this.activIe3 = activIe3;
    }
    
    @XmlElement(name="ACTIV_IE_4")
    public String getActivIe4() {
        return activIe4;
    }

    public void setActivIe4(String activIe4) {
        this.activIe4 = activIe4;
    }
    
    @XmlElement(name="ACTIV_IE_5")
    public String getActivIe5() {
        return activIe5;
    }

    public void setActivIe5(String activIe5) {
        this.activIe5 = activIe5;
    }
    
    @XmlElement(name="ACTIV_IE_6")
    public String getActivIe6() {
        return activIe6;
    }

    public void setActivIe6(String activIe6) {
        this.activIe6 = activIe6;
    }
    
    @XmlElement(name="ACTIV_IE_7")
    public String getActivIe7() {
        return activIe7;
    }

    public void setActivIe7(String activIe7) {
        this.activIe7 = activIe7;
    }    
    

    @XmlElement(name="SERV_IE_1")
    public String getServIe1() {
        return servIe1;
    }

    public void setServIe1(String servIe1) {
        this.servIe1 = servIe1;
    }

    @XmlElement(name="SERV_IE_2")
    public String getServIe2() {
        return servIe2;
    }

    public void setServIe2(String servIe2) {
        this.servIe2 = servIe2;
    }

    @XmlElement(name="SERV_IE_3")
    public String getServIe3() {
        return servIe3;
    }

    public void setServIe3(String servIe3) {
        this.servIe3 = servIe3;
    }

    @XmlElement(name="SERV_IE_4")
    public String getServIe4() {
        return servIe4;
    }

    public void setServIe4(String servIe4) {
        this.servIe4 = servIe4;
    }   

    @XmlElement(name="BBTK_AULA_0")
    public String getBbtkAula0() {
        return bbtkAula0;
    }

    public void setBbtkAula0(String bbtkAula0) {
        this.bbtkAula0 = bbtkAula0;
    }

    @XmlElement(name="BBTK_AULA_1")
    public String getBbtkAula1() {
        return bbtkAula1;
    }

    public void setBbtkAula1(String bbtkAula1) {
        this.bbtkAula1 = bbtkAula1;
    }

    @XmlElement(name="BBTK_AULA_2")
    public String getBbtkAula2() {
        return bbtkAula2;
    }

    public void setBbtkAula2(String bbtkAula2) {
        this.bbtkAula2 = bbtkAula2;
    }

    @XmlElement(name="BBTK_AULA_3")
    public String getBbtkAula3() {
        return bbtkAula3;
    }

    public void setBbtkAula3(String bbtkAula3) {
        this.bbtkAula3 = bbtkAula3;
    }

    @XmlElement(name="BBTK_AULA_4")
    public String getBbtkAula4() {
        return bbtkAula4;
    }

    public void setBbtkAula4(String bbtkAula4) {
        this.bbtkAula4 = bbtkAula4;
    }

    @XmlElement(name="BBTK_AULA_5")
    public String getBbtkAula5() {
        return bbtkAula5;
    }

    public void setBbtkAula5(String bbtkAula5) {
        this.bbtkAula5 = bbtkAula5;
    }    

    @XmlElement(name="BBTK_AULA_N")
    public String getBbtkAulaN() {
        return bbtkAulaN;
    }

    public void setBbtkAulaN(String bbtkAulaN) {
        this.bbtkAulaN = bbtkAulaN;
    }   

    @XmlElement(name="ANOTACIONES")
    public String getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }    
    
    @XmlElement(name="DIR_APE")
    public String getDirApe() {
        return dirApe;
    }

    public void setDirApe(String dirApe) {
        this.dirApe = dirApe;
    }

    @XmlElement(name="DIR_NOM")
    public String getDirNom() {
        return dirNom;
    }

    public void setDirNom(String dirNom) {
        this.dirNom = dirNom;
    }

    @XmlElement(name="DIR_SITUA")
    public String getDirSitua() {
        return dirSitua;
    }

    public void setDirSitua(String dirSitua) {
        this.dirSitua = dirSitua;
    }

    @XmlElement(name="DIR_DNI")
    public String getDirDni() {
        return dirDni;
    }

    public void setDirDni(String dirDni) {
        this.dirDni = dirDni;
    }
    
    @XmlElement(name="DIR_FONO")
    public String getDirFono() {
        return dirFono;
    }

    public void setDirFono(String dirFono) {
        this.dirFono = dirFono;
    }

    @XmlElement(name="DIR_EMAIL")
    public String getDirEmail() {
        return dirEmail;
    }

    public void setDirEmail(String dirEmail) {
        this.dirEmail = dirEmail;
    }

    @XmlElement(name="SDIR_APE")
    public String getSdirApe() {
        return sdirApe;
    }

    public void setSdirApe(String sdirApe) {
        this.sdirApe = sdirApe;
    }

    @XmlElement(name="SDIR_NOM")
    public String getSdirNom() {
        return sdirNom;
    }

    public void setSdirNom(String sdirNom) {
        this.sdirNom = sdirNom;
    }

    @XmlElement(name="SDIR_SITUA")
    public String getSdirSitua() {
        return sdirSitua;
    }

    public void setSdirSitua(String sdirSitua) {
        this.sdirSitua = sdirSitua;
    }

    @XmlElement(name="SDIR_DNI")
    public String getSdirDni() {
        return sdirDni;
    }

    public void setSdirDni(String sdirDni) {
        this.sdirDni = sdirDni;
    }
    
    @XmlElement(name="SDIR_FONO")
    public String getSdirFono() {
        return sdirFono;
    }

    public void setSdirFono(String sdirFono) {
        this.sdirFono = sdirFono;
    }

    @XmlElement(name="SDIR_EMAIL")
    public String getSdirEmail() {
        return sdirEmail;
    }

    public void setSdirEmail(String sdirEmail) {
        this.sdirEmail = sdirEmail;
    }

    @XmlElement(name="REC_CED_DIA")
    public String getRecCedDia() {
        return recCedDia;
    }

    public void setRecCedDia(String recCedDia) {
        this.recCedDia = recCedDia;
    }

    @XmlElement(name="REC_CED_MES")
    public String getRecCedMes() {
        return recCedMes;
    }

    public void setRecCedMes(String recCedMes) {
        this.recCedMes = recCedMes;
    }

    @XmlElement(name="CUL_CED_DIA")
    public String getCulCedDia() {
        return culCedDia;
    }

    public void setCulCedDia(String culCedDia) {
        this.culCedDia = culCedDia;
    }

    @XmlElement(name="CUL_CED_MES")
    public String getCulCedMes() {
        return culCedMes;
    }

    public void setCulCedMes(String culCedMes) {
        this.culCedMes = culCedMes;
    }

    @XmlElement(name="ULTIMO")
    public Boolean getUltimo() {
        return ultimo;
    }

    public void setUltimo(Boolean ultimo) {
        this.ultimo = ultimo;
    }

    @XmlElement(name="VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @XmlElement(name="FUENTE")
    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    @XmlElement(name="FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
/*
    @XmlElement(name="PERSONAL")
    public List<Matricula2014Personal> getMatricula2014PersonalList() {
        return matricula2014PersonalList;
    }

    public void setMatricula2014PersonalList(List<Matricula2014Personal> matricula2014PersonalList) {
        this.matricula2014PersonalList = matricula2014PersonalList;
    }
    
    @XmlElement(name="PERIFERICOS")
    public List<Matricula2014Perifericos> getMatricula2014PerifericosList() {
        return matricula2014PerifericosList;
    }

    public void setMatricula2014PerifericosList(List<Matricula2014Perifericos> matricula2014PerifericosList) {
        this.matricula2014PerifericosList = matricula2014PerifericosList;
    }

    @XmlElement(name="LOCAL_PRONOEI")
    public List<Matricula2014Localpronoei> getMatricula2014LocalpronoeiList() {
        return matricula2014LocalpronoeiList;
    }

    public void setMatricula2014LocalpronoeiList(List<Matricula2014Localpronoei> matricula2014LocalpronoeiList) {
        this.matricula2014LocalpronoeiList = matricula2014LocalpronoeiList;
    }

    @XmlElement(name="COORD_PRONOEI")
    public List<Matricula2014Coordpronoei> getMatricula2014CoordpronoeiList() {
        return matricula2014CoordpronoeiList;
    }

    public void setMatricula2014CoordpronoeiList(List<Matricula2014Coordpronoei> matricula2014CoordpronoeiList) {
        this.matricula2014CoordpronoeiList = matricula2014CoordpronoeiList;
    }

    @XmlElement(name="RECURSOS")
    @XmlJavaTypeAdapter(Matricula2014RecursosMapAdapter.class)
    public Map<String, Matricula2014Recursos> getDetalleRecursos() {
        return detalleRecursos;
    }

    
    public void setDetalleRecursos(Map<String, Matricula2014Recursos> detalleRecursos) {
        this.detalleRecursos = detalleRecursos;
    }

    @XmlElement(name="MATRICULA")
    @XmlJavaTypeAdapter(Matricula2014MatriculaMapAdapter.class)
    public Map<String, Matricula2014Matricula> getDetalleMatricula() {
        return detalleMatricula;
    }


    public void setDetalleMatricula(Map<String, Matricula2014Matricula> detalleMatricula) {
        this.detalleMatricula = detalleMatricula;
    }
    
    @XmlElement(name="DOCENTE")
    @XmlJavaTypeAdapter(Matricula2014DocenteMapAdapter.class)
    public Map<String, Matricula2014Docente> getDetalleDocente() {
        return detalleDocente;
    }


    public void setDetalleDocente(Map<String, Matricula2014Docente> detalleDocente) {
        this.detalleDocente = detalleDocente;
    }

    @XmlElement(name="SECCION")
    @XmlJavaTypeAdapter(Matricula2014SeccionMapAdapter.class)
    public Map<String, Matricula2014Seccion> getDetalleSeccion() {
        return detalleSeccion;
    }
 
    public void setDetalleSeccion(Map<String, Matricula2014Seccion> detalleSeccion) {
        this.detalleSeccion = detalleSeccion;
    }

    @XmlElement(name="SAANEES")    
    public List<Matricula2014Saanee> getMatricula2014SaaneeList() {
        return matricula2014SaaneeList;
    }

    public void setMatricula2014SaaneeList(List<Matricula2014Saanee> matricula2014SaaneeList) {
        this.matricula2014SaaneeList = matricula2014SaaneeList;
    }

*/
    @XmlElement(name="NROCED")
    public String getNroCed() {
        return nroCed;
    }

    public void setNroCed(String nroCed) {
        this.nroCed = nroCed;
    }

    @XmlAttribute
    public long getToken() {
        return token;
    }

    public void setToken(long token) {
        this.token = token;
    }

    @XmlAttribute(name="MSG")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    @XmlAttribute(name="ESTADO_RPT")
    public String getEstadoRpt() {
        return estadoRpt;
    }

    public void setEstadoRpt(String estadoRpt) {
        this.estadoRpt = estadoRpt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2014Cabecera)) {
            return false;
        }
        Matricula2014Cabecera other = (Matricula2014Cabecera) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }
/*
    @XmlElement(name="CURSOS_CETPRO")
    public List<Matricula2014CetproCurso> getMatricula2014CetproCursoList() {
        return matricula2014CetproCursoList;
    }

    
    public void setMatricula2014CetproCursoList(List<Matricula2014CetproCurso> matricula2014CetproCursoList) {
        this.matricula2014CetproCursoList = matricula2014CetproCursoList;
    }*/

    @XmlElement(name="MODATEN")
    public String getModaten() {
        return modaten;
    }


    public void setModaten(String modaten) {
        this.modaten = modaten;
    }

    @XmlElement(name="EGESTORA")
    public String getEgestora() {
        return egestora;
    }

    public void setEgestora(String egestora) {
        this.egestora = egestora;
    }

    @XmlElement(name="TIPODFINA_1")    
    public String getTipodfina1() {
        return tipodfina1;
    }

    public void setTipodfina1(String tipodfina1) {
        this.tipodfina1 = tipodfina1;
    }

    @XmlElement(name="TIPODFINA_2")    
    public String getTipodfina2() {
        return tipodfina2;
    }

    public void setTipodfina2(String tipodfina2) {
        this.tipodfina2 = tipodfina2;
    }

    @XmlElement(name="TIPODFINA_3")    
    public String getTipodfina3() {
        return tipodfina3;
    }

    public void setTipodfina3(String tipodfina3) {
        this.tipodfina3 = tipodfina3;
    }

    @XmlElement(name="TIPODFINA_4")    
    public String getTipodfina4() {
        return tipodfina4;
    }

    public void setTipodfina4(String tipodfina4) {
        this.tipodfina4 = tipodfina4;
    }

    @XmlElement(name="TIPODFINA_5")    
    public String getTipodfina5() {
        return tipodfina5;
    }

    public void setTipodfina5(String tipodfina5) {
        this.tipodfina5 = tipodfina5;
    }

    @XmlElement(name="TIPODFINA_6")    
    public String getTipodfina6() {
        return tipodfina6;
    }

    public void setTipodfina6(String tipodfina6) {
        this.tipodfina6 = tipodfina6;
    }

    @XmlElement(name="NUMRES")
    public String getNumres() {
        return numres;
    }

    public void setNumres(String numres) {
        this.numres = numres;
    }


    @XmlElement(name="FECHARES_DD")
    public String getFecharesDD() {
        return fecharesDD;
    }

    public void setFecharesDD(String fecharesDD) {
        this.fecharesDD = fecharesDD;
    }


    @XmlElement(name="FECHARES_MM")
    public String getFecharesMM() {
        return fecharesMM;
    }

    public void setFecharesMM(String fecharesMM) {
        this.fecharesMM = fecharesMM;
    }


    @XmlElement(name="FECHARES_AA")
   public String getFecharesAA() {
        return fecharesAA;
    }

    public void setFecharesAA(String fecharesAA) {
        this.fecharesAA = fecharesAA;
    }  


}
