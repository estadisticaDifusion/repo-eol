/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.dto;

import java.io.Serializable;

/**
 *
 * @author omateo
 */
public class InstitucionDTO extends BaseDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoModular;
    private String anexo;
    private String nivelModalidad;
    private String codigoLocal;
    private String codigoInstitucion;
    private String mcenso;
    private String sienvio;
    private String estado;

    public String getCodigoModular() {
        return codigoModular;
    }

    public void setCodigoModular(String codigoModular) {
        this.codigoModular = codigoModular;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getNivelModalidad() {
        return nivelModalidad;
    }

    public void setNivelModalidad(String nivelModalidad) {
        this.nivelModalidad = nivelModalidad;
    }

    public String getCodigoLocal() {
        return codigoLocal;
    }

    public void setCodigoLocal(String codigoLocal) {
        this.codigoLocal = codigoLocal;
    }

    public String getCodigoInstitucion() {
        return codigoInstitucion;
    }

    public void setCodigoInstitucion(String codigoInstitucion) {
        this.codigoInstitucion = codigoInstitucion;
    }

    public String getMcenso() {
        return mcenso;
    }

    public void setMcenso(String mcenso) {
        this.mcenso = mcenso;
    }

    

    public String getSienvio() {
		return sienvio;
	}

	public void setSienvio(String sienvio) {
		this.sienvio = sienvio;
	}

	public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

	@Override
	public String toString() {
		return "InstitucionDTO [codigoModular=" + codigoModular + ", anexo=" + anexo + ", nivelModalidad="
				+ nivelModalidad + ", codigoLocal=" + codigoLocal + ", codigoInstitucion=" + codigoInstitucion
				+ ", mcenso=" + mcenso + ", sienvio=" + sienvio + ", estado=" + estado + "]";
	}
    
    

}
