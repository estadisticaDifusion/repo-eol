package pe.gob.minedu.escale.eol.estadistica.ejb.censo2017;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Ie2017Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class CedulaID2017Facade extends AbstractFacade<Ie2017Cabecera> {

	static final Logger LOGGER = Logger.getLogger(CedulaID2017Facade.class.getName());
	public static final String CEDULA_ID = "CENSO-ID";

	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public CedulaID2017Facade() {
		super(Ie2017Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Ie2017Cabecera entity) {

		String sql = "UPDATE Ie2017Cabecera a SET a.ultimo=false WHERE  a.codIed=:codied";
		Query query = em.createQuery(sql);
		query.setParameter("codied", entity.getCodIed());
		query.executeUpdate();
		/*
		 * OM if (entity.getIe2017EstablecimientosList() != null) for
		 * (Ie2017Establecimientos est : entity.getIe2017EstablecimientosList()) {
		 * est.setIe2017Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();

	}

	public int findByCedulaID(String codIed) {
		Query q = em.createQuery(
				"SELECT COUNT(ie.idEnvio) FROM Ie2017Cabecera ie WHERE ie.codIed=:codIed AND ie.ultimo=true");
		q.setParameter("codIed", codIed);
		try {
			return Integer.parseInt(q.getSingleResult().toString());
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public Long findByCedulaIDYNivel(String codIed, String query) {

		Long idenvio = 0L;
		Query q = em.createNativeQuery(query);
		q.setParameter("1", codIed);
		try {

			List<Long> lres = q.getResultList();
			if (lres.size() > 0) {
				idenvio = lres.get(0);
			}

			return idenvio;

		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}
}
