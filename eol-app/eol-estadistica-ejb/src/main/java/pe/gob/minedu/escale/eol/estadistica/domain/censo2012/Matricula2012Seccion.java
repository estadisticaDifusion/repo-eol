/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JMATAMOROS
 */
@Entity
@Table(name = "matricula2012_seccion")
public class Matricula2012Seccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENVIO")
    private Long idEnvio;
    //@Column(name = "CUADRO")
    @Transient
    private String cuadro;
/*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matricula2012Seccion")
    private List<Matricula2012SeccionFila> matricula2012SeccionFilaList;
*/

    @JoinColumn(name = "CABECERA_IDENVIO", referencedColumnName = "ID_ENVIO")
    @ManyToOne
    private Matricula2012Cabecera matricula2012Cabecera;

    public Matricula2012Seccion() {
    }

    public Matricula2012Seccion(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    @XmlElement(name="CUADRO")
    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

/*
    @XmlElement(name="SECCION_FILAS")
    public List<Matricula2012SeccionFila> getMatricula2012SeccionFilaList() {
        return matricula2012SeccionFilaList;
    }

    public void setMatricula2012SeccionFilaList(List<Matricula2012SeccionFila> matricula2012SeccionFilaList) {
        this.matricula2012SeccionFilaList = matricula2012SeccionFilaList;
    }
*/
    @XmlTransient
    public Matricula2012Cabecera getMatricula2012Cabecera() {
        return matricula2012Cabecera;
    }

    public void setMatricula2012Cabecera(Matricula2012Cabecera matricula2012Cabecera) {
        this.matricula2012Cabecera = matricula2012Cabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula2012Seccion)) {
            return false;
        }
        Matricula2012Seccion other = (Matricula2012Seccion) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    

}
