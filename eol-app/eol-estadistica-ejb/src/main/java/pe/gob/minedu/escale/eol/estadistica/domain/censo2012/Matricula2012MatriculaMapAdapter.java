/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2012;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2012.Matricula2012MatriculaMapAdapter.Matricula2012MatriculaList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2012MatriculaMapAdapter extends XmlAdapter<Matricula2012MatriculaList, Map<String, Matricula2012Matricula>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2012MatriculaMapAdapter.class.getName());

    static class Matricula2012MatriculaList {

        private List<Matricula2012Matricula> detalle;

        private Matricula2012MatriculaList(ArrayList<Matricula2012Matricula> lista) {
            detalle = lista;
        }

        public Matricula2012MatriculaList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2012Matricula> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2012Matricula> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2012Matricula> unmarshal(Matricula2012MatriculaList v) throws Exception {

        Map<String, Matricula2012Matricula> map = new HashMap<String, Matricula2012Matricula>();
        for (Matricula2012Matricula detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2012MatriculaList marshal(Map<String, Matricula2012Matricula> v) throws Exception {
        if(v==null)return null;

        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2012Matricula> lista = new ArrayList<Matricula2012Matricula>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2012Matricula $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2012MatriculaList list = new Matricula2012MatriculaList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
