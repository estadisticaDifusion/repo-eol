package pe.gob.minedu.escale.eol.estadistica.ejb.censo2016;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2016.Ie2016Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.AbstractFacade;

@Singleton
public class CedulaID2016Facade extends AbstractFacade<Ie2016Cabecera> {

	static final Logger LOGGER = Logger.getLogger(Local2016Facade.class.getName());
	@PersistenceContext(unitName = "eol-PU")
	private EntityManager em;

	public CedulaID2016Facade() {
		super(Ie2016Cabecera.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void create(Ie2016Cabecera entity) {

		String sql = "UPDATE Ie2016Cabecera a SET a.ultimo=false WHERE  a.codIed=:codied";
		Query query = em.createQuery(sql);
		query.setParameter("codied", entity.getCodIed());
		query.executeUpdate();
		/*
		 * OM if (entity.getIe2016EstablecimientosList() != null) for
		 * (Ie2016Establecimientos est : entity.getIe2016EstablecimientosList()) {
		 * est.setIe2016Cabecera(entity); }
		 */
		em.persist(entity);
		em.flush();

	}

	public int findByCedulaID(String codIed) {
		Query q = em.createQuery(
				"SELECT COUNT(ie.idEnvio) FROM Ie2016Cabecera ie WHERE ie.codIed=:codIed AND ie.ultimo=true");
		q.setParameter("codIed", codIed);
		try {
			return Integer.parseInt(q.getSingleResult().toString());
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public Long findByCedulaIDYNivel(String codIed, String query) {

		Long idenvio = 0L;
		Query q = em.createNativeQuery(query);
		q.setParameter("1", codIed);
		try {

			List<Long> lres = q.getResultList();
			if (lres.size() > 0) {
				idenvio = lres.get(0);
			}

			return idenvio;

		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}
}
