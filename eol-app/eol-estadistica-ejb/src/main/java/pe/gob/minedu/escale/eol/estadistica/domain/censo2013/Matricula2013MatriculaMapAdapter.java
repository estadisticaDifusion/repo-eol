/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.estadistica.domain.censo2013;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2013.Matricula2013MatriculaMapAdapter.Matricula2013MatriculaList;

/**
 * Clase para convertir un MAP a List y visceversa
 * @author JMATAMOROS
 */
public class Matricula2013MatriculaMapAdapter extends XmlAdapter<Matricula2013MatriculaList, Map<String, Matricula2013Matricula>> {

    private static final Logger LOGGER = Logger.getLogger(Matricula2013MatriculaMapAdapter.class.getName());

    static class Matricula2013MatriculaList {

        private List<Matricula2013Matricula> detalle;

        private Matricula2013MatriculaList(ArrayList<Matricula2013Matricula> lista) {
            detalle = lista;
        }

        public Matricula2013MatriculaList() {
        }

        @XmlElement(name = "CUADROS")
        public List<Matricula2013Matricula> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<Matricula2013Matricula> detalle) {
            this.detalle = detalle;
        }
    }

    @Override
    public Map<String, Matricula2013Matricula> unmarshal(Matricula2013MatriculaList v) throws Exception {

        Map<String, Matricula2013Matricula> map = new HashMap<String, Matricula2013Matricula>();
        for (Matricula2013Matricula detalle : v.getDetalle()) {
            map.put(detalle.getCuadro(), detalle);
        }
        LOGGER.log(Level.FINE, "unmarshal:de {0} a {1}", new Object[]{v, map});
        return map;
    }

    @Override
    public Matricula2013MatriculaList marshal(Map<String, Matricula2013Matricula> v) throws Exception {
        if(v==null)return null;

        Set<String> keySet = v.keySet();
        if (keySet.isEmpty()) {
            LOGGER.fine("no hay claves");
            return null;

        }
        ArrayList<Matricula2013Matricula> lista = new ArrayList<Matricula2013Matricula>();
        for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
            String key = it.next();
            Matricula2013Matricula $detalle = v.get(key);
            $detalle.setCuadro(key);
            lista.add($detalle);
        }
        Matricula2013MatriculaList list = new Matricula2013MatriculaList(lista);
        LOGGER.log(Level.FINE, "marshal:de {0} a {1}", new Object[]{v, lista});
        return list;
    }
}
