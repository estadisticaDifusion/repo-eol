package pe.gob.minedu.escale.eol.auth.application.controller;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.auth.application.utils.ConstantsApiPadron;
import pe.gob.minedu.escale.eol.auth.application.utils.FunctionsApiPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.ProvinciaDTO;
import pe.gob.minedu.escale.eol.padron.ejb.ProvinciaLocal;
import pe.gob.minedu.escale.eol.payload.padron.EncryptDTO;
import pe.gob.minedu.escale.eol.payload.padron.ProvinciaListEncrypt;
import pe.gob.minedu.escale.eol.payload.padron.ProvinciaResponse;
import pe.gob.minedu.escale.eol.payload.padron.ProvinciaResponsePayload;
import pe.gob.minedu.escale.eol.payload.padron.UbicacionRequest;
import pe.gob.minedu.escale.eol.utils.Funciones;

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestProvinciaServices {

	private Logger logger = Logger.getLogger(RestProvinciaServices.class);

//	private String param,salt,iv;

//	private int keySize = 128, iterationCount = 1000;

//	private String passphrase= "0015975320171234";

//	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);	

	Gson gson = new Gson();

	@EJB(mappedName = "java:global/eol-padron-ws/ProvinciaFacade!pe.gob.minedu.escale.eol.padron.ejb.ProvinciaLocal")
	private ProvinciaLocal provinciaFacade;

	@RequestMapping(value = "/post/provinciaList", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ProvinciaResponse> getListProvinciaDTO(@RequestBody UbicacionRequest request) {

		logger.info(":: RestProvinciaServices.getListProvinciaDTO :: Starting execution...");
		validateRegionRequest(request);

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "provinciaList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		ProvinciaResponse provinciaResponse = new ProvinciaResponse();
		ProvinciaResponsePayload responsePayload = new ProvinciaResponsePayload();

		provinciaResponse.setId(id);
		provinciaResponse.setDateTime(dateTime);
		provinciaResponse.setResourceName(resourceName);
		provinciaResponse.setHttpMethod(httpMethod);

		List<ProvinciaDTO> listProvinciaDTO = provinciaFacade.getListProvinciaDTO(request.getCodregion());

		if (listProvinciaDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setDataDetail(listProvinciaDTO);
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}

		provinciaResponse.setStatus(status);
		provinciaResponse.setCode(code);
		provinciaResponse.setMessage(message);
		provinciaResponse.setResponsePayload(responsePayload);
		logger.info(":: RestRegionServices.getListProvinciaDTO :: Execution finish.");

		return new ResponseEntity<ProvinciaResponse>(provinciaResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/post/provinciaListUpd", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ProvinciaResponse> getListProvinciaDTOUpd(@RequestBody EncryptDTO request) {

		logger.info(":: RestProvinciaServices.getListProvinciaDTO :: Starting execution...");
//		validateRegionRequest(request);

		ProvinciaListEncrypt aut = new ProvinciaListEncrypt();

		aut = gson.fromJson(FunctionsApiPadron.decryptData(request.getCipher(), request.getSalt(), request.getIv()),
				ProvinciaListEncrypt.class); // desencriptar

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "provinciaList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		ProvinciaResponse provinciaResponse = new ProvinciaResponse();
		ProvinciaResponsePayload responsePayload = new ProvinciaResponsePayload();

		provinciaResponse.setId(id);
		provinciaResponse.setDateTime(dateTime);
		provinciaResponse.setResourceName(resourceName);
		provinciaResponse.setHttpMethod(httpMethod);

		List<ProvinciaDTO> listProvinciaDTO = provinciaFacade.getListProvinciaDTO(aut.getCodregion());

		if (listProvinciaDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);

			responsePayload.setCipher(
					FunctionsApiPadron.encryptData(request.getSalt(), request.getIv(), gson.toJson(listProvinciaDTO))); // encriptar

		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}

		provinciaResponse.setStatus(status);
		provinciaResponse.setCode(code);
		provinciaResponse.setMessage(message);
		provinciaResponse.setSalt(request.getSalt());
		provinciaResponse.setIv(request.getIv());
		provinciaResponse.setPassphrace(ConstantsApiPadron.PASS_PHRASE); // passphrase
		provinciaResponse.setResponsePayload(responsePayload);
		logger.info(":: RestRegionServices.getListProvinciaDTO :: Execution finish.");

		return new ResponseEntity<ProvinciaResponse>(provinciaResponse, HttpStatus.OK);

	}

	private void validateRegionRequest(UbicacionRequest request) {
		if (request == null) {
			throw new RuntimeException("Request es incorrecto");
		}
	}
	/*
	 * private String desencriptar(String param,String salt, String iv){ String
	 * objJsonString = aesUtil.decrypt(salt, iv, passphrase, param); return
	 * objJsonString; }
	 * 
	 * private String encriptar(String salt, String iv, String param ){ String
	 * cipher = aesUtil.encrypt(salt, iv, passphrase, param); return cipher; }
	 */
}
