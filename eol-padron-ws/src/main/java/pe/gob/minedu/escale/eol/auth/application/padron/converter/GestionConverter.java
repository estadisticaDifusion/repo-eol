/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.Gestion;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "gestion")
public class GestionConverter extends CodigoConverter<Gestion> {

    public GestionConverter(Gestion entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public GestionConverter(Gestion entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public GestionConverter() {
        entity = new Gestion();
    }
}
