package pe.gob.minedu.escale.eol.auth.application.service.security.dao;

import java.util.List;

import pe.gob.minedu.escale.eol.auth.application.model.Role;
 
public interface RoleServicesDao {
 
    public List<Role> getAllRoles();
 
    public Role getInfoRole();
 
    public void deleteRole();
 
    public void updateRole();
 
}