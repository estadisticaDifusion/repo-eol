/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.Genero;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "genero")
public class GeneroConverter extends CodigoConverter<Genero> {

    public GeneroConverter(Genero entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public GeneroConverter(Genero entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public GeneroConverter() {
        entity = new Genero();
    }
}
