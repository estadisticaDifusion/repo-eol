/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.Area;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "area")
public class AreaConverter extends CodigoConverter<Area> {

    public AreaConverter(Area entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public AreaConverter(Area entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public AreaConverter() {
        entity = new Area();
    }
}
