package pe.gob.minedu.escale.eol.auth.application.service.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.minedu.escale.eol.auth.application.model.Role;
import pe.gob.minedu.escale.eol.auth.application.repository.dao.UserDao;
import pe.gob.minedu.escale.eol.auth.application.service.security.dao.UserServicesDao;

@Service("customUserDetailsService")
@Transactional
public class CustomUserDetailServices implements UserDetailsService, UserServicesDao {

	private Logger logger = Logger.getLogger(CustomUserDetailServices.class);
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		pe.gob.minedu.escale.eol.auth.application.model.User user = userDao.loadUserByUsername(username);

		if (user == null)
			throw new UsernameNotFoundException("No se encontro informacion del usuario!");

		return new User(user.getLogin(), user.getPassword(), Boolean.valueOf(user.getEnable()),
				Boolean.valueOf(user.getAccountNonExpired()), Boolean.valueOf(user.getCredentialNonExpired()),
				Boolean.valueOf(user.getAccountNonLocked()), getAuthorities(user.getRoleList()));
	}

	public Collection<? extends GrantedAuthority> getAuthorities(List<Role> role) {
		logger.info(":: CustomUserDetailServices.getAuthorities :: Starting execution...");
		List<GrantedAuthority> authoritiesList = new ArrayList<GrantedAuthority>(2);

		Iterator<Role> iterRole = role.iterator();

		while (iterRole.hasNext()) {
			Role rol = iterRole.next();
			authoritiesList.add(new SimpleGrantedAuthority(rol.getRole()));
			logger.info("ROLES --> " + rol.getRole());
		}
		logger.info(":: CustomUserDetailServices.getAuthorities :: Execution finish.");
		return authoritiesList;
	}

	@Override
	public boolean isUserAvailable(String username) {

		boolean available = userDao.isUserAvailable(username);

		return available;
	}

	@Override
	public pe.gob.minedu.escale.eol.auth.application.model.User getUserByLogin(String login) {
		return userDao.loadUserByUsername(login);
	}

	@Override
	public pe.gob.minedu.escale.eol.auth.application.model.User actualizar(pe.gob.minedu.escale.eol.auth.application.model.User u) {
		userDao.update(u);
		return u;
	}

	@Override
	public void eliminar(pe.gob.minedu.escale.eol.auth.application.model.User u) {
		userDao.delete(u.getIdUser().longValue());
	}

	@Override
	public void createUser(pe.gob.minedu.escale.eol.auth.application.model.User u, List<Role> role) {
		u.setPassword(passwordEncoder.encode(u.getPassword()));
		u.setRoleList(role);
		userDao.save(u);
	}

}