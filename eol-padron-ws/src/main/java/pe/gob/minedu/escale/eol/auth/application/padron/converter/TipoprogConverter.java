/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.Tipoprog;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "tipoprog")
public class TipoprogConverter extends CodigoConverter<Tipoprog> {

    public TipoprogConverter(Tipoprog entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public TipoprogConverter(Tipoprog entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public TipoprogConverter() {
        entity = new Tipoprog();
    }
}
