package pe.gob.minedu.escale.eol.auth.application.repository.dao;

import java.util.List;

import pe.gob.minedu.escale.eol.auth.application.model.Role;
import pe.gob.minedu.escale.eol.auth.application.repository.generics.GenericDAO;

public interface RoleDao extends GenericDAO<Role, Long> {

	public List<Role> getRoleListNq();

}