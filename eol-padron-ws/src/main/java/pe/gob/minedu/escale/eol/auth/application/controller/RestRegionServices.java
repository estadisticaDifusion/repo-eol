package pe.gob.minedu.escale.eol.auth.application.controller;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.auth.application.utils.ConstantsApiPadron;
import pe.gob.minedu.escale.eol.auth.application.utils.FunctionsApiPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.RegionDTO;
import pe.gob.minedu.escale.eol.padron.ejb.RegionLocal;
import pe.gob.minedu.escale.eol.payload.padron.EncryptDTO;
import pe.gob.minedu.escale.eol.payload.padron.RegionListEncrypt;
import pe.gob.minedu.escale.eol.payload.padron.RegionResponse;
import pe.gob.minedu.escale.eol.payload.padron.RegionResponsePayload;
import pe.gob.minedu.escale.eol.payload.padron.UbicacionRequest;
import pe.gob.minedu.escale.eol.utils.Funciones;

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestRegionServices {

	private Logger logger = Logger.getLogger(RestRegionServices.class);

	// private String param,salt,iv;

	// private int keySize = 128, iterationCount = 1000;

	// private String passphrase= "0015975320171234";

	// private AesUtil aesUtil = new AesUtil(keySize, iterationCount);

	Gson gson = new Gson();

	@EJB(mappedName = "java:global/eol-padron-ws/RegionFacade!pe.gob.minedu.escale.eol.padron.ejb.RegionLocal")
	private RegionLocal regionFacade;

	@RequestMapping(value = "/post/regionList", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<RegionResponse> getListRegionDTO(@RequestBody UbicacionRequest request) {

		logger.info(":: RestRegionServices.getListRegionDTO :: Starting execution...");
		validateRegionRequest(request);

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "regionList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		RegionResponse regionResponse = new RegionResponse();
		RegionResponsePayload responsePayload = new RegionResponsePayload();

		regionResponse.setId(id);
		regionResponse.setDateTime(dateTime);
		regionResponse.setResourceName(resourceName);
		regionResponse.setHttpMethod(httpMethod);

		List<RegionDTO> listRegionDTO = regionFacade.getListRegionDTO();

		if (listRegionDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setDataDetail(listRegionDTO);
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		regionResponse.setStatus(status);
		regionResponse.setCode(code);
		regionResponse.setMessage(message);
		regionResponse.setResponsePayload(responsePayload);
		logger.info(":: RestRegionServices.getListRegionDTO :: Execution finish.");

		return new ResponseEntity<RegionResponse>(regionResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/post/regionListUpd", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<RegionResponse> getListRegionDTOUpd(@RequestBody EncryptDTO request) {

		logger.info(":: RestRegionServices.getListRegionDTO :: Starting execution...");
//		validateRegionRequest(request);

		RegionListEncrypt aut = new RegionListEncrypt();

		aut = gson.fromJson(FunctionsApiPadron.decryptData(request.getCipher(), request.getSalt(), request.getIv()),
				RegionListEncrypt.class); // desencriptar

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "regionList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		RegionResponse regionResponse = new RegionResponse();
		RegionResponsePayload responsePayload = new RegionResponsePayload();

		regionResponse.setId(id);
		regionResponse.setDateTime(dateTime);
		regionResponse.setResourceName(resourceName);
		regionResponse.setHttpMethod(httpMethod);

		List<RegionDTO> listRegionDTO = regionFacade.getListRegionDTO();

		if (listRegionDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);

			responsePayload.setCipher(
					FunctionsApiPadron.encryptData(request.getSalt(), request.getIv(), gson.toJson(listRegionDTO))); // encriptar

		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		regionResponse.setStatus(status);
		regionResponse.setCode(code);
		regionResponse.setMessage(message);
		regionResponse.setSalt(request.getSalt());
		regionResponse.setIv(request.getIv());
		regionResponse.setPassphrace(ConstantsApiPadron.PASS_PHRASE); // passphrase
		regionResponse.setResponsePayload(responsePayload);
		logger.info(":: RestRegionServices.getListRegionDTO :: Execution finish.");

		return new ResponseEntity<RegionResponse>(regionResponse, HttpStatus.OK);

	}

	private void validateRegionRequest(UbicacionRequest request) {
		if (request == null) {
			throw new RuntimeException("Request es incorrecto");
		}
	}
	/*
	 * private String desencriptar(String param,String salt, String iv){ String
	 * objJsonString = aesUtil.decrypt(salt, iv, passphrase, param); return
	 * objJsonString; }
	 * 
	 * private String encriptar(String salt, String iv, String param ){ String
	 * cipher = aesUtil.encrypt(salt, iv, passphrase, param); return cipher; }
	 */

}
