/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.padron.domain.Institucion;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "instituciones")
public class InstitucionesConverter {

    private Collection<Institucion> entities;
    private Collection<InstitucionConverter> items;
    private URI uri;
    private int expandLevel;

    public InstitucionesConverter() {
    }

    public InstitucionesConverter(Collection<Institucion> entities, URI uri, int expandLevel) {
        this.entities = entities;
        this.uri = uri;
        this.expandLevel = expandLevel;
        getInstitucion();
    }

    @XmlElement(name = "items")
    public Collection<InstitucionConverter> getInstitucion() {
        if (items == null) {
            items = new ArrayList<InstitucionConverter>();
        }
        if (entities != null) {
            items.clear();
            for (Institucion entity : entities) {
                items.add(new InstitucionConverter(entity, uri, expandLevel, true));
            }
        }
        return items;
    }

    public void setInstitucion(Collection<InstitucionConverter> items) {
        this.items = items;
    }

    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    @XmlTransient
    public Collection<Institucion> getEntities() {
        entities = new ArrayList<Institucion>();
        if (items != null) {
            for (InstitucionConverter item : items) {
                entities.add(item.getEntity());
            }
        }
        return entities;
    }
}
