/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.persistence.EntityManager;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.padron.domain.Codigo;

/**
 *
 * @author DSILVA
 */
public class CodigoConverter<T extends Codigo> {

    protected T entity;
    protected URI uri;
    protected int expandLevel;

    public CodigoConverter() {
    }

    public CodigoConverter(T entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);

    }

    public CodigoConverter(T entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getIdCodigo() + "/").build() : uri;
        this.expandLevel = expandLevel;

    }

    public void setValor(String valor) {
        entity.setValor(valor);
    }

    public void setIdCodigo(String idCodigo) {
        entity.setIdCodigo(idCodigo);
    }

    @XmlElement
    public String getValor() {
        return entity == null ? null : entity.getValor();
    }

    @XmlElement
    public String getIdCodigo() {
        return ((expandLevel > 0) && (entity != null)) ? entity.getIdCodigo() : null;
    }

    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the URI for this reference converter.
     *
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@XmlTransient
    public T getEntity() {
        if (entity.getIdCodigo() == null) {
            CodigoConverter converter = UriResolver.getInstance().resolve(CodigoConverter.class, uri);
            if (converter != null) {
                entity = (T) converter.getEntity();
            }
        }
        return entity;
    }

    public T resolveEntity(EntityManager em) {
        return entity;
    }
}
