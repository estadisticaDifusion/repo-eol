/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.persistence.EntityManager;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.padron.domain.Igel;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "igel")
public class IgelConverter {

    private Igel entity;
    private URI uri;
    private int expandLevel;

    public IgelConverter() {
        entity = new Igel();
    }

    public IgelConverter(Igel entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getIdIgel() + "/").build() : uri;
        this.expandLevel = expandLevel;
        getDireccionRegional();
    }

    public IgelConverter(Igel entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    @XmlElement
    public DireccionRegionalConverter getDireccionRegional() {
        if (expandLevel > 0) {
            if (entity.getDireccionRegional() != null) {
                return new DireccionRegionalConverter(entity.getDireccionRegional(), uri.resolve("direccionRegional/"), expandLevel - 1, false);
            }
        }
        return null;
    }

    public void setDireccionRegional(DireccionRegionalConverter value) {
        entity.setDireccionRegional((value != null) ? value.getEntity() : null);
    }

    @XmlTransient
    public Igel getEntity() {
        if (entity.getIdIgel() == null) {
            IgelConverter converter = UriResolver.getInstance().resolve(IgelConverter.class, uri);
            if (converter != null) {
                entity = converter.getEntity();
            }
        }
        return entity;
    }

    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public void setNombreUgel(String nombreUgel) {
        entity.setNombreUgel(nombreUgel);
    }

    public void setIdIgel(String idIgel) {
        entity.setIdIgel(idIgel);
    }

    @XmlElement
    @XmlSchemaType(name = "String")
    public String getNombreUgel() {
        return entity.getNombreUgel();
    }

    @XmlElement
    @XmlSchemaType(name = "String")
    public String getIdIgel() {
        return (expandLevel > 0) ? entity.getIdIgel() : null;
    }

    public Igel resolveEntity(EntityManager em) {
        return entity;
    }
}
