package pe.gob.minedu.escale.eol.auth.application.service.security.dao;

import pe.gob.minedu.escale.eol.auth.application.model.OauthClientDetails;

public interface OauthClientDetailsServicesDao {

    public void createClient(OauthClientDetails client);

    public boolean isUserAvailable(String clientId);

    public OauthClientDetails getClientById(String clientId);

    public OauthClientDetails actualizar(OauthClientDetails client);

    public void eliminar(OauthClientDetails client);
}
