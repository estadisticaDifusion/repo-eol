/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.Turno;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "turno")
public class TurnoConverter extends CodigoConverter<Turno> {

    public TurnoConverter(Turno entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public TurnoConverter(Turno entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public TurnoConverter() {
        entity = new Turno();
    }
}
