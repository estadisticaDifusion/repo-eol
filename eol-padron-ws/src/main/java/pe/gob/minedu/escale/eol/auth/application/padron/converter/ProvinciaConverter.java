/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.persistence.EntityManager;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
//import pe.gob.minedu.escale.rest.server.domain.Provincia;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.padron.domain.Provincia;
import pe.gob.minedu.escale.eol.padron.domain.Region;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "provincia")
public class ProvinciaConverter {

    private Provincia entity;
    private URI uri;
    private int expandLevel;

    /** Creates a new instance of ProvinciaConverter */
    public ProvinciaConverter() {
        entity = new Provincia();
    }

    /**
     * Creates a new instance of ProvinciaConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should be expanded@param isUriExtendable indicates whether the uri can be extended
     */
    public ProvinciaConverter(Provincia entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getIdProvincia() + "/").build() : uri;
        this.expandLevel = expandLevel;
        getRegion();
    }

    /**
     * Creates a new instance of ProvinciaConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should be expanded
     */
    public ProvinciaConverter(Provincia entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    /**
     * Getter for idProvincia.
     *
     * @return value for idProvincia
     */
    @XmlElement
    public String getIdProvincia() {
        return (expandLevel > 0) ? entity.getIdProvincia() : null;
    }

    /**
     * Setter for idProvincia.
     *
     * @param value the value to set
     */
    public void setIdProvincia(String value) {
        entity.setIdProvincia(value);
    }

    /**
     * Getter for nombreProvincia.
     *
     * @return value for nombreProvincia
     */
    @XmlElement
    public String getNombreProvincia() {
        return (expandLevel > 0) ? entity.getNombreProvincia() : null;
    }

    /**
     * Setter for nombreProvincia.
     *
     * @param value the value to set
     */
    public void setNombreProvincia(String value) {
        entity.setNombreProvincia(value);
    }

    /**
     * Getter for region.
     *
     * @return value for region
     */
    @XmlElement
    public RegionConverter getRegion() {
        if (expandLevel > 0) {
            if (entity.getRegion() != null) {
                return new RegionConverter(entity.getRegion(), uri.resolve("region/"), expandLevel - 1, false);
            }
        }
        return null;
    }

    /**
     * Setter for region.
     *
     * @param value the value to set
     */
    public void setRegion(RegionConverter value) {
        entity.setRegion((value != null) ? value.getEntity() : null);
    }

    /**
     * Returns the URI associated with this converter.
     *
     * @return the uri
     */
    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the URI for this reference converter.
     *
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the Provincia entity.
     *
     * @return an entity
     */
    @XmlTransient
    public Provincia getEntity() {
        if (entity.getIdProvincia() == null) {
            ProvinciaConverter converter = UriResolver.getInstance().resolve(ProvinciaConverter.class, uri);
            if (converter != null) {
                entity = converter.getEntity();
            }
        }
        return entity;
    }

    /**
     * Returns the resolved Provincia entity.
     *
     * @return an resolved entity
     */
    public Provincia resolveEntity(EntityManager em) {
        Region region = entity.getRegion();
        if (region != null) {
            entity.setRegion(em.getReference(Region.class, region.getIdRegion()));
        }
        return entity;
    }

    public void setZoom(int zoom) {
        entity.setZoom(zoom);
    }

    public void setPointY(double pointY) {
        entity.setPointY(pointY);
    }

    public void setPointX(double pointX) {
        entity.setPointX(pointX);
    }

    @XmlElement
    public int getZoom() {
        return entity.getZoom();
    }

    @XmlElement
    public double getPointY() {
        return entity.getPointY();
    }

    @XmlElement
    public double getPointX() {
        return entity.getPointX();
    }
}
