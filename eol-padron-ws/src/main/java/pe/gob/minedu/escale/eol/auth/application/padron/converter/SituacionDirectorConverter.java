/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.SituacionDirector;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "situacionDirector")
public class SituacionDirectorConverter extends CodigoConverter<SituacionDirector> {

    public SituacionDirectorConverter(SituacionDirector entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public SituacionDirectorConverter(SituacionDirector entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public SituacionDirectorConverter() {
        entity = new SituacionDirector();
    }
}
