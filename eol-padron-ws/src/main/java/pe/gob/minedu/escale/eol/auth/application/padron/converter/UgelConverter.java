/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.persistence.EntityManager;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
//import pe.gob.minedu.escale.rest.server.domain.Ugel;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.padron.domain.Ugel;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "ugel")
public class UgelConverter {

    private Ugel entity;
    private URI uri;
    private int expandLevel;

    /**
     * Creates a new instance of UgelConverter
     */
    public UgelConverter() {
        entity = new Ugel();
    }

    /**
     * Creates a new instance of UgelConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded@param isUriExtendable indicates whether the uri can be
     * extended
     */
    public UgelConverter(Ugel entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getIdUgel() + "/").build() : uri;
        this.expandLevel = expandLevel;
        getDireccionRegional();
    }

    /**
     * Creates a new instance of UgelConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded
     */
    public UgelConverter(Ugel entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    /**
     * Getter for idUgel.
     *
     * @return value for idUgel
     */
    @XmlElement
    public String getIdUgel() {
        return (expandLevel > 0) ? entity.getIdUgel() : null;
    }

    /**
     * Setter for idUgel.
     *
     * @param value the value to set
     */
    public void setIdUgel(String value) {
        entity.setIdUgel(value);
    }

    /**
     * Getter for ugel.
     *
     * @return value for ugel
     */
    @XmlElement
    public String getNombreUgel() {
        return (expandLevel > 0) ? entity.getNombreUgel() : null;
    }

    /**
     * Setter for ugel.
     *
     * @param value the value to set
     */
    public void setNombreUgel(String value) {
        entity.setNombreUgel(value);
    }

    /**
     * Returns the URI associated with this converter.
     *
     * @return the uri
     */
    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the URI for this reference converter.
     *
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the Ugel entity.
     *
     * @return an entity
     */
    @XmlTransient
    public Ugel getEntity() {
        if (entity.getIdUgel() == null) {
            UgelConverter converter = UriResolver.getInstance().resolve(UgelConverter.class, uri);
            if (converter != null) {
                entity = converter.getEntity();
            }
        }
        return entity;
    }

    /**
     * Returns the resolved Ugel entity.
     *
     * @return an resolved entity
     */
    public Ugel resolveEntity(EntityManager em) {
        return entity;
    }

    public void setZoom(int zoom) {
        entity.setZoom(zoom);
    }

    public void setPointY(double pointY) {
        entity.setPointY(pointY);
    }

    public void setPointX(double pointX) {
        entity.setPointX(pointX);
    }

    @XmlElement
    public int getZoom() {
        return entity.getZoom();
    }

    @XmlElement
    public double getPointY() {
        return entity.getPointY();
    }

    @XmlElement
    public double getPointX() {
        return entity.getPointX();
    }

    @XmlElement
    public DireccionRegionalConverter getDireccionRegional() {
        if (expandLevel > 0) {
            if (entity.getDireccionRegional() != null) {
                return new DireccionRegionalConverter(entity.getDireccionRegional(), uri.resolve("direccionRegional/"), expandLevel - 1, false);
            }
        }
        return null;
    }

    public void setDireccionRegional(DireccionRegionalConverter value) {
        entity.setDireccionRegional((value != null) ? value.getEntity() : null);
    }
}
