/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.padron.dto.InstitucionEducativaDTO;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "iiee")
public class InstitucionesEducativasConverter {

    private Collection<InstitucionEducativaDTO> entities;
    private Collection<InstitucionEducativaConverter> items;
    private URI uri;
    private int expandLevel;

    public InstitucionesEducativasConverter() {
    }

    public InstitucionesEducativasConverter(Collection<InstitucionEducativaDTO> entities, URI uri) {
        this(entities, uri, 1);
    }

    public InstitucionesEducativasConverter(Collection<InstitucionEducativaDTO> entities, URI uri, int expandLevel) {
        this.entities = entities;
        this.uri = uri;
        this.expandLevel = expandLevel;        
        getInstitucionEducativa();
    }

    
    @XmlElement(name="items")
    public Collection<InstitucionEducativaConverter> getInstitucionEducativa() {
        if (items == null) {
            items = new ArrayList<InstitucionEducativaConverter>();
        }
        if (entities != null) {
            items.clear();
            for (InstitucionEducativaDTO institucionEducativa : entities) {
                items.add(new InstitucionEducativaConverter(institucionEducativa, uri, expandLevel, true));
            }
        }
        return items;
    }

    public void setInstitucionEducativa(Collection<InstitucionEducativaConverter> items) {
        this.items = items;
    }

    /*@XmlAttribute
    public URI getUri() {
        return uri;
    }*/

    @XmlTransient
    public Collection<InstitucionEducativaDTO> getEntities() {
        entities = new ArrayList<InstitucionEducativaDTO>();
        if (items != null) {
            for (InstitucionEducativaConverter item : items) {
                entities.add(item.getEntity());
            }
        }
        return entities;
    }
}
