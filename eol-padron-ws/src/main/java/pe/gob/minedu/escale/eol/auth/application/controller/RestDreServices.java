package pe.gob.minedu.escale.eol.auth.application.controller;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.auth.application.utils.ConstantsApiPadron;
import pe.gob.minedu.escale.eol.auth.application.utils.FunctionsApiPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.DireccionRegionalDTO;
import pe.gob.minedu.escale.eol.padron.ejb.DreLocal;
import pe.gob.minedu.escale.eol.payload.padron.DreListEncrypt;
import pe.gob.minedu.escale.eol.payload.padron.DreResponse;
import pe.gob.minedu.escale.eol.payload.padron.DreResponsePayload;
import pe.gob.minedu.escale.eol.payload.padron.EncryptDTO;
import pe.gob.minedu.escale.eol.payload.padron.UgelRequest;
import pe.gob.minedu.escale.eol.utils.Funciones;

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestDreServices {

	private Logger logger = Logger.getLogger(RestDreServices.class);

//	private String param,salt,iv;

//	private int keySize = 128, iterationCount = 1000;

//	private String passphrase= "0015975320171234";

//	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);	

	Gson gson = new Gson();

	@EJB(mappedName = "java:global/eol-padron-ws/DreFacade!pe.gob.minedu.escale.eol.padron.ejb.DreLocal")
	private DreLocal dreFacade;

	@RequestMapping(value = "/post/dreList", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<DreResponse> getListDreDTO(@RequestBody UgelRequest request) {

		logger.info(":: RestDreServices.getListDreDTO :: Starting execution...");
		validateRegionRequest(request);

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "dreList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		DreResponse dreResponse = new DreResponse();
		DreResponsePayload dreResponsePayload = new DreResponsePayload();

		dreResponse.setId(id);
		dreResponse.setDateTime(dateTime);
		dreResponse.setResourceName(resourceName);
		dreResponse.setHttpMethod(httpMethod);

		List<DireccionRegionalDTO> listDreDTO = dreFacade.gestListDre();

		if (listDreDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			dreResponsePayload.setDataResult(CoreConstant.DATA_FOUND);
			dreResponsePayload.setDataDetail(listDreDTO);
		} else {
			dreResponsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		dreResponse.setStatus(status);
		dreResponse.setCode(code);
		dreResponse.setMessage(message);
		dreResponse.setResponsePayload(dreResponsePayload);
		logger.info(":: RestDreServices.getListDreDTO :: Execution finish.");

		return new ResponseEntity<DreResponse>(dreResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/post/dreListUpd", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<DreResponse> getListDreDTOUpd(@RequestBody EncryptDTO request) {

		logger.info(":: RestDreServices.getListDreDTO :: Starting execution...");
//		validateRegionRequest(request);

		DreListEncrypt aut = new DreListEncrypt();

		aut = gson.fromJson(FunctionsApiPadron.decryptData(request.getCipher(), request.getSalt(), request.getIv()),
				DreListEncrypt.class); // desencriptar

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "dreList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		DreResponse dreResponse = new DreResponse();
		DreResponsePayload dreResponsePayload = new DreResponsePayload();

		dreResponse.setId(id);
		dreResponse.setDateTime(dateTime);
		dreResponse.setResourceName(resourceName);
		dreResponse.setHttpMethod(httpMethod);

		List<DireccionRegionalDTO> listDreDTO = dreFacade.gestListDre();

		if (listDreDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			dreResponsePayload.setDataResult(CoreConstant.DATA_FOUND);

			dreResponsePayload.setCipher(
					FunctionsApiPadron.encryptData(request.getSalt(), request.getIv(), gson.toJson(listDreDTO))); // encriptar

//			dreResponsePayload.setDataDetail(listDreDTO);
		} else {
			dreResponsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		dreResponse.setStatus(status);
		dreResponse.setCode(code);
		dreResponse.setMessage(message);
		dreResponse.setSalt(request.getSalt());
		dreResponse.setIv(request.getIv());
		dreResponse.setPassphrace(ConstantsApiPadron.PASS_PHRASE); // passphrase
		dreResponse.setResponsePayload(dreResponsePayload);
		logger.info(":: RestDreServices.getListDreDTO :: Execution finish.");

		return new ResponseEntity<DreResponse>(dreResponse, HttpStatus.OK);

	}

	private void validateRegionRequest(UgelRequest request) {
		if (request == null) {
			throw new RuntimeException("Request es incorrecto");
		}
	}
	/*
	 * private String desencriptar(String param,String salt, String iv){ String
	 * objJsonString = aesUtil.decrypt(salt, iv, passphrase, param); return
	 * objJsonString; }
	 * 
	 * private String encriptar(String salt, String iv, String param ){ String
	 * cipher = aesUtil.encrypt(salt, iv, passphrase, param); return cipher; }
	 */

}
