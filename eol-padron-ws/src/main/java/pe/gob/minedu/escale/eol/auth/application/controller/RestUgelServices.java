package pe.gob.minedu.escale.eol.auth.application.controller;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.auth.application.utils.ConstantsApiPadron;
import pe.gob.minedu.escale.eol.auth.application.utils.FunctionsApiPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.UgelDTO;
import pe.gob.minedu.escale.eol.padron.ejb.UgelLocal;
import pe.gob.minedu.escale.eol.payload.padron.EncryptDTO;
import pe.gob.minedu.escale.eol.payload.padron.UgelListEncrypt;
import pe.gob.minedu.escale.eol.payload.padron.UgelRequest;
import pe.gob.minedu.escale.eol.payload.padron.UgelResponse;
import pe.gob.minedu.escale.eol.payload.padron.UgelResponsePayload;
import pe.gob.minedu.escale.eol.utils.Funciones;

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestUgelServices {

	private Logger logger = Logger.getLogger(RestUgelServices.class);
	
//	private String param,salt,iv;
	
//	private int keySize = 128, iterationCount = 1000;
	
//	private String passphrase= "0015975320171234";
	
//	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);	
	
	Gson gson = new Gson();

	@EJB(mappedName = "java:global/eol-padron-ws/UgelFacade!pe.gob.minedu.escale.eol.padron.ejb.UgelLocal")
	private UgelLocal ugelFacade;

	@RequestMapping(value = "/post/ugelList", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<UgelResponse> getListUgelDTO(@RequestBody UgelRequest request){
		logger.info(":: RestUgelServices.getListUgelDTO :: Starting execution...");
		validateUgelRequest(request);
		
		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "ugelList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";
		
		UgelResponse ugelResponse = new UgelResponse();
		UgelResponsePayload responsePayload = new UgelResponsePayload();
		
		ugelResponse.setId(id);
		ugelResponse.setDateTime(dateTime);
		ugelResponse.setResourceName(resourceName);
		ugelResponse.setHttpMethod(httpMethod);
		
		List<UgelDTO> listUgelDTO=ugelFacade.gestListUgel(request.getCodigodre());
		
		if(listUgelDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setDataListDetail(listUgelDTO);
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		
		ugelResponse.setStatus(status);
		ugelResponse.setCode(code);
		ugelResponse.setMessage(message);
		ugelResponse.setResponsePayload(responsePayload);
		logger.info(":: RestUgelServices.getListUgelDTO :: Execution finish.");
		
		return new ResponseEntity<UgelResponse>(ugelResponse, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/post/ugelListUpd", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<UgelResponse> getListUgelDTOUpd(@RequestBody EncryptDTO request){
		logger.info(":: RestUgelServices.getListUgelDTO :: Starting execution...");
//		validateUgelRequest(request);
		
		UgelListEncrypt aut=new UgelListEncrypt();
		
		aut = gson.fromJson(FunctionsApiPadron.decryptData(request.getCipher(),request.getSalt(), request.getIv()) , UgelListEncrypt.class); // desencriptar

				
		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}
		
		
		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "ugelList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";
		
		UgelResponse ugelResponse = new UgelResponse();
		UgelResponsePayload responsePayload = new UgelResponsePayload();
		
		ugelResponse.setId(id);
		ugelResponse.setDateTime(dateTime);
		ugelResponse.setResourceName(resourceName);
		ugelResponse.setHttpMethod(httpMethod);
		
		List<UgelDTO> listUgelDTO=ugelFacade.gestListUgel(aut.getCodigodre());
		
		if(listUgelDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			
			responsePayload.setCipher(FunctionsApiPadron.encryptData(request.getSalt(), request.getIv(),gson.toJson(listUgelDTO))); // encriptar

		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		
		ugelResponse.setStatus(status);
		ugelResponse.setCode(code);
		ugelResponse.setMessage(message);
		ugelResponse.setSalt(request.getSalt());
		ugelResponse.setIv(request.getIv());
		ugelResponse.setPassphrace(ConstantsApiPadron.PASS_PHRASE); // passphrase
		ugelResponse.setResponsePayload(responsePayload);
		logger.info(":: RestUgelServices.getListUgelDTO :: Execution finish.");
		
		return new ResponseEntity<UgelResponse>(ugelResponse, HttpStatus.OK);
		
	}
	
	
	
	@RequestMapping(value = "/post/ugel", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<UgelResponse> getUgelDTO(@RequestBody UgelRequest request) {
		logger.info(":: RestUgelServices.getUgelDTO :: Starting execution...");
		validateUgelRequest(request);
		
		String codigoUgel = request.getId();

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "ugel";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		UgelResponse ugelResponse = new UgelResponse();
		UgelResponsePayload responsePayload = new UgelResponsePayload();

		ugelResponse.setId(id);
		ugelResponse.setDateTime(dateTime);
		ugelResponse.setResourceName(resourceName);
		ugelResponse.setHttpMethod(httpMethod);

		UgelDTO ugelDTO = ugelFacade.getUgel(codigoUgel);

		if (ugelDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult("DATA_FOUND");
			responsePayload.setDataDetail(ugelDTO);
		} else {
			responsePayload.setDataResult("DATA_NOT_FOUND");
		}
		ugelResponse.setStatus(status);
		ugelResponse.setCode(code);
		ugelResponse.setMessage(message);
		ugelResponse.setResponsePayload(responsePayload);

		logger.info(":: RestUgelServices.getUgelDTO :: Execution finish.");
		return new ResponseEntity<UgelResponse>(ugelResponse, HttpStatus.OK);
	}

	private void validateUgelRequest(UgelRequest request) {
		if (request == null) {
			throw new RuntimeException("Request es incorrecto");
		}
	}
	/*
    private String desencriptar(String param,String salt, String iv){
		String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
    	return objJsonString;
    }

    private String encriptar(String salt, String iv, String param ){
		String cipher = aesUtil.encrypt(salt, iv, passphrase, param);
    	return cipher;
    }
	*/
	
}
