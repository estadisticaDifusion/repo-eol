/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.GestionDependencia;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "gestionDependencia")
public class GestionDependenciaConverter extends CodigoConverter<GestionDependencia> {

    public GestionDependenciaConverter(GestionDependencia entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public GestionDependenciaConverter(GestionDependencia entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public GestionDependenciaConverter() {
        entity = new GestionDependencia();
    }
}
