package pe.gob.minedu.escale.eol.auth.application.controller;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.auth.application.utils.ConstantsApiPadron;
import pe.gob.minedu.escale.eol.auth.application.utils.FunctionsApiPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.DistritoDTO;
import pe.gob.minedu.escale.eol.padron.ejb.DistritoLocal;
import pe.gob.minedu.escale.eol.payload.padron.DistritoListEncrypt;
import pe.gob.minedu.escale.eol.payload.padron.DistritoResponse;
import pe.gob.minedu.escale.eol.payload.padron.DistritoResponsePayload;
import pe.gob.minedu.escale.eol.payload.padron.EncryptDTO;
import pe.gob.minedu.escale.eol.payload.padron.UbicacionRequest;
import pe.gob.minedu.escale.eol.utils.Funciones;

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestDistritoServices {

	private Logger logger = Logger.getLogger(RestDistritoServices.class);

//	private int keySize = 128, iterationCount = 1000;

//	private String passphrase= "0015975320171234";

//	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);

	Gson gson = new Gson();

	@EJB(mappedName = "java:global/eol-padron-ws/DistritoFacade!pe.gob.minedu.escale.eol.padron.ejb.DistritoLocal")
	private DistritoLocal distritoFacade;

	@RequestMapping(value = "/post/distritoList", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<DistritoResponse> getListDistritoDTO(@RequestBody UbicacionRequest request) {
		logger.info(":: RestProvinciaServices.getListDistritoDTO :: Starting execution...");
		validateRegionRequest(request);

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "distritoList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		DistritoResponse distritoResponse = new DistritoResponse();
		DistritoResponsePayload responsePayload = new DistritoResponsePayload();

		distritoResponse.setId(id);
		distritoResponse.setDateTime(dateTime);
		distritoResponse.setResourceName(resourceName);
		distritoResponse.setHttpMethod(httpMethod);

		List<DistritoDTO> listDistritoDTO = distritoFacade.getListDistritoDTO(request.getCodprovincia());

		if (listDistritoDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setDataDetail(listDistritoDTO);
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}

		distritoResponse.setStatus(status);
		distritoResponse.setCode(code);
		distritoResponse.setMessage(message);
		distritoResponse.setResponsePayload(responsePayload);
		logger.info(":: RestRegionServices.getListDistritoDTO :: Execution finish.");

		return new ResponseEntity<DistritoResponse>(distritoResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/post/distritoListUpd", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<DistritoResponse> getListDistritoDTOUpd(@RequestBody EncryptDTO request) {
		logger.info(":: RestProvinciaServices.getListDistritoDTO :: Starting execution...");
//		validateRegionRequest(request);

		DistritoListEncrypt aut = new DistritoListEncrypt();
		// desencriptar
		aut = gson.fromJson(FunctionsApiPadron.decryptData(request.getCipher(), request.getSalt(), request.getIv()),
				DistritoListEncrypt.class);

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "distritoList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		DistritoResponse distritoResponse = new DistritoResponse();
		DistritoResponsePayload responsePayload = new DistritoResponsePayload();

		distritoResponse.setId(id);
		distritoResponse.setDateTime(dateTime);
		distritoResponse.setResourceName(resourceName);
		distritoResponse.setHttpMethod(httpMethod);

		List<DistritoDTO> listDistritoDTO = distritoFacade.getListDistritoDTO(aut.getCodprovincia());

		if (listDistritoDTO != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);

			responsePayload.setCipher(
					FunctionsApiPadron.encryptData(request.getSalt(), request.getIv(), gson.toJson(listDistritoDTO))); // encriptar

//			responsePayload.setDataDetail(listDistritoDTO);

		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}

		distritoResponse.setStatus(status);
		distritoResponse.setCode(code);
		distritoResponse.setMessage(message);
		distritoResponse.setSalt(request.getSalt());
		distritoResponse.setIv(request.getIv());
		distritoResponse.setPassphrace(ConstantsApiPadron.PASS_PHRASE); // passphrase
		distritoResponse.setResponsePayload(responsePayload);
		logger.info(":: RestRegionServices.getListDistritoDTO :: Execution finish.");

		return new ResponseEntity<DistritoResponse>(distritoResponse, HttpStatus.OK);

	}

	private void validateRegionRequest(UbicacionRequest request) {
		if (request == null) {
			throw new RuntimeException("Request es incorrecto");
		}
	}
	/*
	 * private String desencriptar(String param,String salt, String iv){ String
	 * objJsonString = aesUtil.decrypt(salt, iv, passphrase, param); return
	 * objJsonString; }
	 * 
	 * private String encriptar(String salt, String iv, String param ){ String
	 * cipher = aesUtil.encrypt(salt, iv, passphrase, param); return cipher; }
	 */
}
