/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.padron.domain.Estado;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "estado")
public class EstadoConverter extends CodigoConverter<Estado> {

    public EstadoConverter(Estado entity, URI uri, int expandLevel, boolean isUriExtendable) {
        super(entity, uri, expandLevel, isUriExtendable);
    }

    public EstadoConverter(Estado entity, URI uri, int expandLevel) {
        super(entity, uri, expandLevel);
    }

    public EstadoConverter() {
        entity = new Estado();
    }
}
