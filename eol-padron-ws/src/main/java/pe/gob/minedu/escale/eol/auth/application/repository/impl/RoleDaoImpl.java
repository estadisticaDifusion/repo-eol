package pe.gob.minedu.escale.eol.auth.application.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.minedu.escale.eol.auth.application.model.Role;
import pe.gob.minedu.escale.eol.auth.application.repository.dao.RoleDao;
import pe.gob.minedu.escale.eol.auth.application.repository.generics.GenericDAOImpl;

@Repository("roleDao")
@Transactional
public class RoleDaoImpl extends GenericDAOImpl<Role, Long> implements RoleDao {

	public RoleDaoImpl() {
		super(Role.class);
	}

	//@SuppressWarnings({ "unchecked" })
	@Override
	public List<Role> getRoleListNq() {

		StringBuilder sQuery = new StringBuilder();

		sQuery.append("SELECT ID_ROLE, \n");
		sQuery.append(" ROLE \n");
		sQuery.append("FROM ROLES");

		Query query = (Query) getSessionFactory().getCurrentSession().createSQLQuery(sQuery.toString());

		List<Role> roles = new ArrayList<Role>();

		for (Object object : query.getResultList()) {
			if (object instanceof Role) {
				roles.add((Role) object);
			}
		}

		return roles;
	}

}