package pe.gob.minedu.escale.eol.auth.application.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import pe.gob.minedu.escale.common.velocity.VelocityUtil;
import pe.gob.minedu.escale.eol.auth.application.utils.ConstantsApiPadron;
import pe.gob.minedu.escale.eol.auth.application.utils.FunctionsApiPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.padron.dto.CensoieDTO;
import pe.gob.minedu.escale.eol.padron.ejb.InstitucionLocal;
import pe.gob.minedu.escale.eol.payload.padron.CantidadResponse;
import pe.gob.minedu.escale.eol.payload.padron.CantidadResponsePayload;
import pe.gob.minedu.escale.eol.payload.padron.CensoResponse;
import pe.gob.minedu.escale.eol.payload.padron.CensoResponsePayload;
import pe.gob.minedu.escale.eol.payload.padron.EncryptDTO;
import pe.gob.minedu.escale.eol.payload.padron.InsticucionEncrypt;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionRequest;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionRequestEncrypt;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionResponse;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionResponsePayload;
import pe.gob.minedu.escale.eol.utils.AesUtil;
import pe.gob.minedu.escale.eol.utils.Funciones;

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestInstitucionServices {

	private Logger logger = Logger.getLogger(RestInstitucionServices.class);

	// private String param, salt, iv;

	private int keySize = 128, iterationCount = 1000;

	private String passphrase = "0015975320171234";

	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);

	Gson gson = new Gson();

	@EJB(mappedName = "java:global/eol-padron-ws/InstitucionFacade!pe.gob.minedu.escale.eol.padron.ejb.InstitucionLocal")
	private InstitucionLocal institucionFacade;

	@RequestMapping(value = "/post/institucionList", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<InstitucionResponse> getListInstitucionDTO(@RequestBody InstitucionRequest request) {
		logger.info(":: RestInstitucionServices.getListInstitucionDTO :: Starting execution...");
		validateInstitucionRequest(request);

		String estado = request.getEstado();
		Integer start = request.getStart();
		Integer max = request.getMax();
		String orderBy = request.getOrderBy();
		if (estado == null) {
			estado = "1";
		}
		if (start == null) {
			start = 0;
		}
		if (max == null) {
			max = 50;
		}
		if (orderBy == null) {
			orderBy = "cenEdu";
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "institucionList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		InstitucionResponse institucionResponse = new InstitucionResponse();
		InstitucionResponsePayload responsePayload = new InstitucionResponsePayload();

		institucionResponse.setId(id);
		institucionResponse.setDateTime(dateTime);
		institucionResponse.setResourceName(resourceName);
		institucionResponse.setHttpMethod(httpMethod);

		List<InstitucionDTO> institucionList = institucionFacade.getInstitucionList(request.getCodmod(),
				request.getCodlocal(), request.getUbigeo(), request.getCodooii(), request.getNombreCP(),
				request.getNombreIE(), request.getDisVrae(), request.getDisJuntos(), request.getDisCrecer(),
				request.getDisNinguno(), request.getMatIndigena(), request.getNiveles(), request.getGestiones(),
				request.getAreas(), estado, request.getTipoices(), request.getFormas(), request.getEstados(),
				request.getProgarti(), request.getProgise(), start, max, orderBy);

		if (institucionList != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setDataDetail(institucionList);
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		institucionResponse.setStatus(status);
		institucionResponse.setCode(code);
		institucionResponse.setMessage(message);
		institucionResponse.setResponsePayload(responsePayload);

		logger.info(":: RestInstitucionServices.getListInstitucionDTO :: Execution finish.");
		return new ResponseEntity<InstitucionResponse>(institucionResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/post/institucionListInfoBase", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<InstitucionResponse> getListInstitucionBase(@RequestBody EncryptDTO request) {
		logger.info(":: RestInstitucionServices.getListInstitucionBase :: Starting execution...");
		String resourceName = "institucionListInfoBase";
		InstitucionResponse institucionResponse = generateResponseList(request, false, resourceName);
		logger.info(":: RestInstitucionServices.getListInstitucionBase :: Execution finish.");
		return new ResponseEntity<InstitucionResponse>(institucionResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/post/institucionListInfoComplete", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<InstitucionResponse> getListInstitucionComplete(@RequestBody EncryptDTO request) {
		logger.info(":: RestInstitucionServices.getListInstitucionComplete :: Starting execution...");
		String resourceName = "institucionListInfoComplete";
		InstitucionResponse institucionResponse = generateResponseList(request, true, resourceName);
		logger.info(":: RestInstitucionServices.getListInstitucionComplete :: Execution finish.");
		return new ResponseEntity<InstitucionResponse>(institucionResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/post/institucion", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CensoResponse> getCensoInstitucionDTO(@RequestBody InstitucionRequest request) {

		logger.info(":: RestInstitucionServices.getCensoIE :: Starting execution...");
		validateInstitucionRequest(request);

		String estado = request.getEstado();
		if (estado == null) {
			estado = "1";
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "censoIE";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		CensoResponse censoResponse = new CensoResponse();
		CensoResponsePayload cresponsePayload = new CensoResponsePayload();

		censoResponse.setId(id);
		censoResponse.setDateTime(dateTime);
		censoResponse.setResourceName(resourceName);
		censoResponse.setHttpMethod(httpMethod);

		CensoieDTO censoiddto = institucionFacade.getDataCensoIE(request.getCodmod(), request.getAnexo(),
				request.getAnio(), request.getNivel());

		if (censoiddto != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			cresponsePayload.setDataResult(CoreConstant.DATA_FOUND);
			cresponsePayload.setDataDetail(censoiddto);
		} else {
			cresponsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		censoResponse.setStatus(status);
		censoResponse.setCode(code);
		censoResponse.setMessage(message);
		censoResponse.setCresponsePayload(cresponsePayload);
		;

		return new ResponseEntity<CensoResponse>(censoResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/post/institucionUpd", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CensoResponse> getCensoInstitucionDTOUpd(@RequestBody EncryptDTO request) {

		logger.info(":: RestInstitucionServices.getCensoIE :: Starting execution...");

		InsticucionEncrypt aut = new InsticucionEncrypt();

		aut = gson.fromJson(FunctionsApiPadron.decryptData(request.getCipher(), request.getSalt(), request.getIv()),
				InsticucionEncrypt.class);

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		String estado = request.getEstado();
		if (estado == null) {
			estado = "1";
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "censoIE";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		CensoResponse censoResponse = new CensoResponse();
		CensoResponsePayload cresponsePayload = new CensoResponsePayload();

		censoResponse.setId(id);
		censoResponse.setDateTime(dateTime);
		censoResponse.setResourceName(resourceName);
		censoResponse.setHttpMethod(httpMethod);

		CensoieDTO censoiddto = institucionFacade.getDataCensoIE(aut.getCodmod(), aut.getAnexo(), aut.getAnio(),
				aut.getNivel());

		if (censoiddto != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			cresponsePayload.setDataResult(CoreConstant.DATA_FOUND);

			cresponsePayload.setCipher(
					FunctionsApiPadron.encryptData(request.getSalt(), request.getIv(), gson.toJson(censoiddto)));

		} else {
			cresponsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		censoResponse.setStatus(status);
		censoResponse.setCode(code);
		censoResponse.setMessage(message);
		censoResponse.setSalt(request.getSalt());
		censoResponse.setIv(request.getIv());
		censoResponse.setPassphrace(ConstantsApiPadron.PASS_PHRASE);
		censoResponse.setCresponsePayload(cresponsePayload);

		return new ResponseEntity<CensoResponse>(censoResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/post/institucionListUpd", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<InstitucionResponse> getListInstitucionDTOUpd(@RequestBody EncryptDTO request) {
		logger.info(":: Ingresando a getListInstitucionDTOUpd...");
		InstitucionRequestEncrypt aut = new InstitucionRequestEncrypt();

		aut = gson.fromJson(desencriptar(request.getCipher(), request.getSalt(), request.getIv()),
				InstitucionRequestEncrypt.class);

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		// String[] estados = aut.getEstados();
		String estado = "";
		Integer start = Integer.parseInt(aut.getStart());
		Integer max = null;
		String orderBy = null;
		// for(int i=0;i<estados.length;i++){
		//// if (estados == null) {
		//// estado = "1";
		//// }
		// }

		if (max == null) {
			max = 50;
		}
		if (orderBy == null) {
			orderBy = "cenEdu";
		}
		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "institucionListUpd";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		InstitucionResponse institucionResponse = new InstitucionResponse();
		InstitucionResponsePayload responsePayload = new InstitucionResponsePayload();

		institucionResponse.setId(id);
		institucionResponse.setDateTime(dateTime);
		institucionResponse.setResourceName(resourceName);
		institucionResponse.setHttpMethod(httpMethod);

		List<String> nivelesLis = new ArrayList<String>();
		// String niveles="";
		// nivelesLis.add(aut.getNiveles());
		if (aut.getNiveles() != null) {
			for (int i = 0; i < aut.getNiveles().length; i++) {
				nivelesLis.add(aut.getNiveles()[i]);
				// niveles=nivelesLis.get(i).toString();
			}
		}

		List<String> gestionesLis = new ArrayList<String>();
		// String gestiones="";
		if (aut.getGestiones() != null) {
			for (int i = 0; i < aut.getGestiones().length; i++) {
				gestionesLis.add(aut.getGestiones()[i]);
				// gestiones=gestionesLis.get(i).toString();
			}
		}

		List<String> areasLis = new ArrayList<String>();
		List<String> tipoIcesLis = new ArrayList<String>();
		List<String> formasLis = new ArrayList<String>();
		List<String> estadosLis = new ArrayList<String>();
		// String estados="";
		if (aut.getEstados() != null) {
			for (int i = 0; i < aut.getEstados().length; i++) {
				estadosLis.add(aut.getEstados()[i]);
				// estados=estadosLis.get(i).toString();
			}
		}
		String disVrae = "";
		String disJuntos = "";
		String disCrecer = "";
		String disNinguno = "";
		String matIndigena = "";
		String progarTi = "";
		String proGise = "";

		List<InstitucionDTO> institucionList = institucionFacade.getInstitucionList(aut.getCodmod(), aut.getCodlocal(),
				aut.getUbigeo(), aut.getCodooii(), aut.getNombreCP(), aut.getNombreIE(), disVrae, disJuntos, disCrecer,
				disNinguno, matIndigena, nivelesLis, gestionesLis, areasLis, estado, tipoIcesLis, formasLis, estadosLis,
				progarTi, proGise, start, max, orderBy);

		// List<Object> lista = new ArrayList<Object>();

		if (institucionList != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setCipher(encriptar(request.getSalt(), request.getIv(), gson.toJson(institucionList)));
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		institucionResponse.setStatus(status);
		institucionResponse.setCode(code);
		institucionResponse.setMessage(message);

		institucionResponse.setResponsePayload(responsePayload);
		institucionResponse.setSalt(request.getSalt());
		institucionResponse.setIv(request.getIv());
		institucionResponse.setPassphrace(passphrase);
		logger.info(":: RestInstitucionServices.getListInstitucionDTO :: Execution finish.");
		return new ResponseEntity<InstitucionResponse>(institucionResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/post/institucionCount", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CantidadResponse> getCountInstitucion(@RequestBody InstitucionRequest request) {
		logger.info(":: RestInstitucionServices.getCountInstitucion :: Starting execution...");
		validateInstitucionRequest(request);

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "institucionCount";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		CantidadResponse cantidadResponse = new CantidadResponse();
		CantidadResponsePayload responsePayload = new CantidadResponsePayload();

		cantidadResponse.setId(id);
		cantidadResponse.setDateTime(dateTime);
		cantidadResponse.setResourceName(resourceName);
		cantidadResponse.setHttpMethod(httpMethod);

		Long count = institucionFacade.getRecordCount(request.getCodmod(), request.getCodlocal(), request.getUbigeo(),
				request.getCodooii(), request.getNombreCP(), request.getNombreIE(), request.getDisVrae(),
				request.getDisJuntos(), request.getDisCrecer(), request.getDisNinguno(), request.getMatIndigena(),
				request.getNiveles(), request.getGestiones(), request.getAreas(), request.getEstado(),
				request.getTipoices(), request.getFormas(), request.getEstados(), request.getProgarti(),
				request.getProgise());
		logger.info("count institucion=" + count);
		logger.info(":: RestInstitucionServices.getCountInstitucion :: Execution finish.");

		if (count != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult("DATA_FOUND");
			responsePayload.setDataDetail(count);
		} else {
			responsePayload.setDataResult("DATA_NOT_FOUND");
		}
		cantidadResponse.setStatus(status);
		cantidadResponse.setCode(code);
		cantidadResponse.setMessage(message);
		cantidadResponse.setResponsePayload(responsePayload);

		return new ResponseEntity<CantidadResponse>(cantidadResponse, HttpStatus.OK);
	}

	private InstitucionResponse generateResponseList(EncryptDTO request, boolean allAtribute, String resourceName) {
		InstitucionRequestEncrypt aut = new InstitucionRequestEncrypt();

		aut = gson.fromJson(FunctionsApiPadron.decryptData(request.getCipher(), request.getSalt(), request.getIv()),
				InstitucionRequestEncrypt.class);

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		String estado = "";
		Integer start = Integer.parseInt(aut.getStart());
		Integer max = null;
		String orderBy = null;

		if (max == null) {
			max = 50;
		}
		if (orderBy == null) {
			orderBy = "cenEdu";
		}
		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		InstitucionResponse institucionResponse = new InstitucionResponse();
		InstitucionResponsePayload responsePayload = new InstitucionResponsePayload();

		institucionResponse.setId(id);
		institucionResponse.setDateTime(dateTime);
		institucionResponse.setResourceName(resourceName);
		institucionResponse.setHttpMethod(httpMethod);

		List<String> nivelesLis = new ArrayList<String>();

		if (aut.getNiveles() != null) {
			for (int i = 0; i < aut.getNiveles().length; i++) {
				nivelesLis.add(aut.getNiveles()[i]);
			}
		}

		List<String> gestionesLis = new ArrayList<String>();

		if (aut.getGestiones() != null) {
			for (int i = 0; i < aut.getGestiones().length; i++) {
				gestionesLis.add(aut.getGestiones()[i]);
			}
		}

		List<String> areasLis = new ArrayList<String>();
		List<String> tipoIcesLis = new ArrayList<String>();
		List<String> formasLis = new ArrayList<String>();
		List<String> estadosLis = new ArrayList<String>();

		if (aut.getEstados() != null) {
			for (int i = 0; i < aut.getEstados().length; i++) {
				estadosLis.add(aut.getEstados()[i]);
			}
		}
		String disVrae = "";
		String disJuntos = "";
		String disCrecer = "";
		String disNinguno = "";
		String matIndigena = "";
		String progarTi = "";
		String proGise = "";
		
		logger.info("ubigeo = " + aut.getUbigeo());

		List<InstitucionDTO> institucionList = institucionFacade.getInstitucionList(aut.getCodmod(), aut.getCodlocal(),
				aut.getUbigeo(), aut.getCodooii(), aut.getNombreCP(), aut.getNombreIE(), disVrae, disJuntos, disCrecer,
				disNinguno, matIndigena, nivelesLis, gestionesLis, areasLis, estado, tipoIcesLis, formasLis, estadosLis,
				progarTi, proGise, start, max, orderBy);

		if (institucionList != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";

			String responseJson = generateJSon(allAtribute, institucionList);

			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setCipher(FunctionsApiPadron.encryptData(request.getSalt(), request.getIv(), responseJson));
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		institucionResponse.setStatus(status);
		institucionResponse.setCode(code);
		institucionResponse.setMessage(message);

		institucionResponse.setResponsePayload(responsePayload);
		institucionResponse.setSalt(request.getSalt());
		institucionResponse.setIv(request.getIv());
		institucionResponse.setPassphrace(ConstantsApiPadron.PASS_PHRASE);
		return institucionResponse;
	}

	private void validateInstitucionRequest(InstitucionRequest request) {
		if (request == null) {
			throw new RuntimeException("Request es incorrecto");
		}
	}

	private String createResponseJsonAsString(Map<String, Object> dataResult) {
		String responseJson = null;
		String path = "template-files/out/response/";
		String filename = "apiResponseSuccessDataBase.txt";

		String responseJsonTemplate = getFileAsString(path, filename);
		responseJson = VelocityUtil.velocityTransform(dataResult, responseJsonTemplate);
		return responseJson;
	}

	private String getFileAsString(String path, String filename) {
		InputStream is = getClass().getClassLoader().getResourceAsStream(path + filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		return reader.lines().collect(Collectors.joining("\n"));
	}

	@SuppressWarnings("rawtypes")
	private String generateJSon(boolean allAtribute, List<InstitucionDTO> institucionList) {

		Map<String, Object> dataResult = new HashMap<String, Object>();

		List<Map> listMap = Funciones.convertListToMap(institucionList);

		dataResult.put("allAtribute", allAtribute);
		dataResult.put("dataDetail", listMap);

		String responseJson = createResponseJsonAsString(dataResult);

		return responseJson;
	}

	private String desencriptar(String param, String salt, String iv) {
		String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
		return objJsonString;
	}

	private String encriptar(String salt, String iv, String param) {
		String cipher = aesUtil.encrypt(salt, iv, passphrase, param);
		return cipher;
	}

}
