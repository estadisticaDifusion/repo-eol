/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.auth.application.padron.converter;

import java.math.BigDecimal;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.padron.dto.InstitucionEducativaDTO;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "ie")
public class InstitucionEducativaConverter {

	private InstitucionEducativaDTO entity;
	private URI uri;
	private int expandLevel;

	public InstitucionEducativaConverter() {
		entity = new InstitucionEducativaDTO();
	}

	public InstitucionEducativaConverter(InstitucionEducativaDTO entity, URI uri, int expandLevel) {
		this(entity, uri, expandLevel, false);

	}

	public InstitucionEducativaConverter(InstitucionEducativaDTO entity, URI uri, int expandLevel,
			boolean isUriExtendable) {
		this.entity = entity;
		this.uri = (isUriExtendable)
				? UriBuilder.fromUri(uri).path(entity.getCodigoModular() + "/" + entity.getAnexo()).build()
				: uri;
		this.expandLevel = expandLevel;

	}

	public void setUbigeo(String ubigeo) {

		entity.setUbigeo(ubigeo);
	}

	public void setTipoSexo(String tipoSexo) {
		entity.setTipoSexo(tipoSexo);
	}

	public void setTelefono(String telefono) {
		entity.setTelefono(telefono);
	}

	public void setProvincia(String provincia) {
		entity.setProvincia(provincia);
	}

	public void setPaginaWeb(String paginaWeb) {
		entity.setPaginaWeb(paginaWeb);
	}

	public void setNombreIE(String nombreIE) {
		entity.setNombreIE(nombreIE);
	}

	public void setNivelModalidad(String nivelModalidad) {
		entity.setNivelModalidad(nivelModalidad);
	}

	public void setForma(String forma) {
		entity.setForma(forma);
	}

	public void setEstado(String estado) {
		entity.setEstado(estado);
	}

	public void setDistrito(String distrito) {
		entity.setDistrito(distrito);
	}

	public void setDirector(String director) {
		entity.setDirector(director);
	}

	public void setDireccion(String direccion) {
		entity.setDireccion(direccion);
	}

	public void setDepartamento(String departamento) {
		entity.setDepartamento(departamento);
	}

	public void setCorreoElectronico(String correoElectronico) {
		entity.setCorreoElectronico(correoElectronico);
	}

	public void setCoordY(BigDecimal coordY) {
		entity.setCoordY(coordY);
	}

	public void setCoordX(BigDecimal coordX) {
		entity.setCoordX(coordX);
	}

	public void setCodigoModular(String codigoModular) {
		entity.setCodigoModular(codigoModular);
	}

	public void setCodigoLocal(String codigoLocal) {
		entity.setCodigoLocal(codigoLocal);
	}

	public void setCodTurno(String codTurno) {
		entity.setCodTurno(codTurno);
	}

	public void setCodNivelModalidad(String codNivelModalidad) {
		entity.setCodNivelModalidad(codNivelModalidad);
	}

	public void setCodGestion(String codGestion) {
		entity.setCodGestion(codGestion);
	}

	public void setCodDreUgel(String codDreUgel) {
		entity.setCodDreUgel(codDreUgel);
	}

	public void setCodCentroPoblado(String codCentroPoblado) {
		entity.setCodCentroPoblado(codCentroPoblado);
	}

	public void setCodArea(String codArea) {
		entity.setCodArea(codArea);
	}

	public void setCentroPoblado(String centroPoblado) {
		entity.setCentroPoblado(centroPoblado);
	}

	public void setAnexo(String anexo) {
		entity.setAnexo(anexo);
	}

	public void setDocentes(int docentes) {
		entity.setDocentes(docentes);
	}

	public void setAlumnos(int alumnos) {
		entity.setAlumnos(alumnos);
	}

	@XmlElement
	public String getUbigeo() {
		return entity.getUbigeo();
	}

	@XmlElement
	public String getTipoSexo() {
		return entity.getTipoSexo();
	}

	@XmlElement
	public String getDescTipoSexo() {
		return entity.getDescTipoSexo();
	}

	@XmlElement
	public String getTelefono() {
		return entity.getTelefono();
	}

	@XmlElement
	public String getProvincia() {
		return entity.getProvincia();
	}

	@XmlElement
	public String getPaginaWeb() {
		return entity.getPaginaWeb();
	}

	@XmlElement
	public String getNombreIE() {
		return entity.getNombreIE();
	}

	@XmlElement
	public String getNivelModalidad() {
		return entity.getNivelModalidad();
	}

	@XmlElement
	public String getForma() {
		return entity.getForma();
	}

	@XmlElement
	public String getDescForma() {
		return entity.getDescForma();
	}

	@XmlElement
	public String getEstado() {
		return entity.getEstado();
	}

	@XmlElement
	public String getDescEstado() {
		return entity.getDescEstado();
	}

	@XmlElement
	public String getDistrito() {
		return entity.getDistrito();
	}

	@XmlElement
	public String getDirector() {
		return entity.getDirector();
	}

	@XmlElement
	public String getDireccion() {
		return entity.getDireccion();
	}

	@XmlElement
	public String getDepartamento() {
		return entity.getDepartamento();
	}

	@XmlElement
	public String getCorreoElectronico() {
		return entity.getCorreoElectronico();
	}

	@XmlElement
	public BigDecimal getCoordY() {
		return entity.getCoordY();
	}

	@XmlElement
	public BigDecimal getCoordX() {
		return entity.getCoordX();
	}

	@XmlElement
	public String getCodigoModular() {
		return (expandLevel > 0) ? entity.getCodigoModular() : null;
	}

	@XmlElement
	public String getCodigoLocal() {
		return entity.getCodigoLocal();
	}

	@XmlElement
	public String getCodTurno() {
		return entity.getCodTurno();
	}

	@XmlElement
	public String getDescTurno() {
		return entity.getDescTurno();
	}

	@XmlElement
	public String getCodNivelModalidad() {
		return entity.getCodNivelModalidad();
	}

	@XmlElement
	public String getCodGestion() {
		return entity.getCodGestion();
	}

	@XmlElement
	public String getDescGestion() {
		return entity.getDescGestion();
	}

	@XmlElement
	public String getCodDependencia() {
		return entity.getCodDependencia();
	}

	@XmlElement
	public String getDescDependencia() {
		return entity.getDescDependencia();
	}

	@XmlElement
	public String getCodDreUgel() {
		return entity.getCodDreUgel();
	}

	@XmlElement
	public String getNomDreUgel() {
		return entity.getNomDreUgel();
	}

	@XmlElement
	public String getCodCentroPoblado() {
		return entity.getCodCentroPoblado();
	}

	@XmlElement
	public String getCodArea() {
		return entity.getCodArea();
	}

	@XmlElement
	public String getDescArea() {
		return entity.getDescArea();
	}

	@XmlElement
	public String getCodCaracteristica() {
		return entity.getCodCaracteristica();
	}

	@XmlElement
	public String getDescCaracteristica() {
		return entity.getDescCaracteristica();
	}

	@XmlElement
	public String getCentroPoblado() {
		return entity.getCentroPoblado();
	}

	@XmlElement
	public String getAnexo() {
		return (expandLevel > 0) ? entity.getAnexo() : null;
	}

	@XmlElement
	public Integer getAlumnos() {
		return entity.getAlumnos();
	}

	@XmlElement
	public Integer getDocentes() {
		return entity.getDocentes();
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	@XmlAttribute
	public URI getUri() {
		return uri;
	}

	@XmlTransient
	public InstitucionEducativaDTO getEntity() {
		if (entity.getCodigoModular() == null || entity.getAnexo() == null) {
			InstitucionEducativaConverter converter = UriResolver.getInstance()
					.resolve(InstitucionEducativaConverter.class, uri);
			if (converter != null) {
				entity = converter.getEntity();
			}
		}
		return entity;
	}
}
