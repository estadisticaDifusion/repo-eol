package pe.gob.minedu.escale.eol.auth.application.utils;

public class ConstantsApiPadron {

	public static final String MSG_ERROR_CLIENTE_NO_REGISTRADO = "Cliente %s no registrado";

	public static final String PASS_PHRASE = "0015975320171234";

	public static final Integer KEY_SIZE = 128;
	
	public static final Integer ITERATION_COUNT = 1000;

	public static final String TYPE_DATA_BASE = "TYPE_DATA_BASE";
	
	public static final String TYPE_DATA_COMPLETE = "TYPE_DATA_COMPLETE";
	
	public static final String RESOURCE_ID = "microservice-padron";
}
