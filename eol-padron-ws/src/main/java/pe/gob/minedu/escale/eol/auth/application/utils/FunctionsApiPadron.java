package pe.gob.minedu.escale.eol.auth.application.utils;

import pe.gob.minedu.escale.eol.utils.AesUtil;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright (C) 2019
 * Ministerio de Educacion 
 * Unidad de Estadistica Educativa
 * (Lima - Peru)
 *  
 * PROYECTO EOL
 *
 * All rights reserved. Used by permission This software is the confidential and
 * proprietary information of MIMP ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with MIMP.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */
/**
 * @objetivo: Se implementa los metodos necesarios para la funcionalidad de capa vista.
 * @autor: Ing. Oscar Mateo
 * @fecha: 10/08/2019
 *
 *         ------------------------------------------------------------------------
 *         Modificaciones Fecha Nombre Descripción
 *         ------------------------------------------------------------------------
 *
 */

public class FunctionsApiPadron {

	public static String decryptData(String param, String salt, String iv) {
		AesUtil aesUtil = new AesUtil(ConstantsApiPadron.KEY_SIZE, ConstantsApiPadron.ITERATION_COUNT);
		String objJsonString = aesUtil.decrypt(salt, iv, ConstantsApiPadron.PASS_PHRASE, param);
		return objJsonString;
	}

	public static String encryptData(String salt, String iv, String param) {
		AesUtil aesUtil = new AesUtil(ConstantsApiPadron.KEY_SIZE, ConstantsApiPadron.ITERATION_COUNT);
		String cipher = aesUtil.encrypt(salt, iv, ConstantsApiPadron.PASS_PHRASE, param);
		return cipher;
	}
	
}
