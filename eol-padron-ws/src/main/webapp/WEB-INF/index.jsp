<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>RestFul Services - Buenas Practicas</title>
<style type="text/css">
#table_7_1 {
	width: 900px;
	margin: 0 auto;
}

table {
	width: 900px;
	display: table;
	border-color: gray;
	border: 1px solid #CAD5CF;
	border-spacing: 0;
	border-collapse: collapse;
	cellspacing: 0;
}

table.property th {
	font-size: 14px;
	text-align: left;
	vertical-align: text-center;
	width: 110px;
	border: 1px solid #CAD5CF;
	background: #F6F6F9;
	height: 50px;
	margin: 10px;
}

table.property td {
	font-size: 14px;
	text-align: left;
	vertical-align: text-center;
	/*width: 110px;*/
	border: 1px solid #CAD5CF;
}

table, table td, table th .table, .table td, .table th {
	line-height: 1.5;
}

th {
	text-align: left;
}

td, th {
	display: table-cell;
	vertical-align: inherit;
	margin: 5px;
	padding: 5px;
}

th {
	font-weight: bold;
}

body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 14px;
	line-height: 1.42857143;
	color: #333;
	background-color: #fff;
}
</style>
</head>

<body>
	<div class="table" id="table_7_1">

		<p align="center">
			<strong>RestFul Services Padr&oacute;n - Buenas Pr&aacute;cticas</strong>
		</p>

		<!-- Tabla de informacion -->
		<table class="property">
			<thead>
				<tr>
					<th scope="row">Proyecto</th>
					<td><strong class="title">Proyecto de
							Integraci&oacute;n de Informaci&oacute;n de Padr&oacute;n Escolar</strong></td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>

</body>
</html>