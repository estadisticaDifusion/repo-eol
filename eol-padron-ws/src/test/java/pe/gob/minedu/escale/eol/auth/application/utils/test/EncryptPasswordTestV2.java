package pe.gob.minedu.escale.eol.auth.application.utils.test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class EncryptPasswordTestV2 {

	private Logger logger = Logger.getLogger(EncryptPasswordTestV2.class);

	@Test
	public void testEncryptPassword() {

		SecureRandom random = null;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		PasswordEncoder encoder = new BCryptPasswordEncoder(16, random);

		/*
		 * String pass = "123456"; PasswordEncoder encoder = new
		 * BCryptPasswordEncoder(16, random);
		 * 
		 * for (int i = 0; i < 3; i++) { logger.info("Hash {" + i + "} --> " +
		 * encoder.encode(pass)); }
		 * 
		 * pass = "1234567"; for (int i = 0; i < 4; i++) { logger.info("New Hash {" + i
		 * + "} --> " + encoder.encode(pass)); }
		 * 
		 * /*pass = "ACERVERA1";
		 * 
		 * logger.info("ACERVERA1 = " + encoder.encode(pass));
		 */

		/*
		 * String pass = "12345678";
		 * 
		 * logger.info("12345678 = " + encoder.encode(pass));
		 */

		List<UsuarioDTO> listausuario = getDataFromDB();

		logger.info("CANTIDAD = " + listausuario.size());

		// cifrado de contraseñas
		String ccifrado = "";
		for (UsuarioDTO ousuario : listausuario) {
			logger.info("USUARIO : " + ousuario.getContrasena() );
			if (!ousuario.getUsuario().isEmpty()) {
				ccifrado = encoder.encode(ousuario.getContrasena());
				ousuario.setCifrado(ccifrado);
			}
		}

		/*
		 * for (UsuarioDTO ousuario : listausuario) {
		 * 
		 * if(!ousuario.getUsuario().isEmpty()) { logger.info("CF " +
		 * ousuario.getContrasena() +":" + ousuario.getCifrado()); } }
		 */

		logger.info("INICIANDO ACTUAIZACION : " + listausuario );

		updateDataFromDB(listausuario);

		// String plainClientCredentials = "eol-padron-ws:987654321";
		// logger.info("Credentials 1: " + new
		// String(Base64.encodeBase64(plainClientCredentials.getBytes())));
	}

	private List<UsuarioDTO> getDataFromDB() {

		List<UsuarioDTO> listUser = new ArrayList<>();
		PreparedStatement crunchifyPrepareStat = null;

		try {

			// Instancias la clase que hemos creado anteriormente
			ConexionMySQL sqlcon = new ConexionMySQL();
			// Llamas al método que tiene la clase y te devuelve una conexión
			Connection conn = sqlcon.conectarMySQL();
			// Query que usarás para hacer lo que necesites
			// String sSQL = "SELECT * FROM auth.auth_usuario WHERE usuario = '200001'";
			String sSQL = " SELECT a.* FROM auth.auth_usuario a INNER JOIN padron.padron p "
					+ " ON a.usuario = p.cod_mod WHERE p.estado = 1 AND a.passwcifrado IS NULL ";
			// String sSQL = "SELECT * FROM auth.auth_usuario LIMIT 10";

			// MySQL Select Query Tutorial
			// String getQueryStatement = "SELECT * FROM employee";

			crunchifyPrepareStat = conn.prepareStatement(sSQL);

			// Execute the Query, and get a java ResultSet
			ResultSet rs = crunchifyPrepareStat.executeQuery();

			// Let's iterate through the java ResultSet
			UsuarioDTO user;
			while (rs.next()) {
				user = new UsuarioDTO();
				user.setUsuario(rs.getString("usuario"));
				user.setContrasena(rs.getString("contrasenia"));

				listUser.add(user);
				// System.out.format("DATOS : "+usuario + " : " + nombre);
			}

			// System.out.format("DATOS : "+listUser.size());

		} catch (

		SQLException e) {
			e.printStackTrace();
		}

		return listUser;

	}

	@SuppressWarnings("unused")
	private void updateDataFromDB(List<UsuarioDTO> listUseru) {

		List<UsuarioDTO> listUser = new ArrayList<>();
		CallableStatement stmt = null;

		try {

			// Instancias la clase que hemos creado anteriormente
			ConexionMySQL sqlcon = new ConexionMySQL();
			// Llamas al método que tiene la clase y te devuelve una conexión
			Connection conn = sqlcon.conectarMySQL();
			// Query que usarás para hacer lo que necesites
			// String sSQL = "SELECT * FROM auth.auth_usuario WHERE usuario = '200001'";
			// String sSQL = "SELECT * FROM auth.auth_usuario";
			String sSQL = "{CALL grabarcifrado(?,?)}";

			for (UsuarioDTO ousuariou : listUseru) {

				if (!ousuariou.getUsuario().isEmpty()) {
					stmt = conn.prepareCall(sSQL);
					stmt.setString(1, ousuariou.getUsuario());
					stmt.setString(2, ousuariou.getCifrado());
					stmt.execute();
				}
			}

			System.out.format("FINALIZANDO ACTUALIZACION");

		} catch (

		SQLException e) {
			e.printStackTrace();
		}

	}

}
