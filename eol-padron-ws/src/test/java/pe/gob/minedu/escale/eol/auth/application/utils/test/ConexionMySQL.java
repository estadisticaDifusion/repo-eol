package pe.gob.minedu.escale.eol.auth.application.utils.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionMySQL {

	public Connection conectarMySQL() {
		// Librería de MySQL
	    String driver = "com.mysql.jdbc.Driver";

	    // Nombre de la base de datos
	    String database = "auth";

	    // Host
	    String hostname = "localhost";

	    // Puerto
	    String port = "3306";

	    // Ruta de nuestra base de datos (desactivamos el uso de SSL con "?useSSL=false")
	    String url = "jdbc:mysql://" + hostname + ":" + port + "/" + database + "?useSSL=false";

	    // Nombre de usuario
	    String username = "root";

	    // Clave de usuario
	    String password = "root";
        Connection conn = null;

        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }
    
}
