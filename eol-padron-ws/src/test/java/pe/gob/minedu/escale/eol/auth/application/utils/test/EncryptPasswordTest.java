package pe.gob.minedu.escale.eol.auth.application.utils.test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class EncryptPasswordTest {

	private Logger logger = Logger.getLogger(EncryptPasswordTest.class);

	@Test
	public void testEncryptPassword10() {
		SecureRandom random = null;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		PasswordEncoder encoder = new BCryptPasswordEncoder(10, random);
		String pass = "2504";
		
		Assert.assertTrue(encoder != null);
		
		logger.info("pass = " + pass + "; encrypt = " + encoder.encode(pass));
		

		//String plainClientCredentials = "eol-padron-ws:987654321";
		//logger.info("Credentials 1: " + new String(Base64.encodeBase64(plainClientCredentials.getBytes())));
	}
	
	@Test
	public void testEncryptPassword() {
		SecureRandom random = null;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		PasswordEncoder encoder = new BCryptPasswordEncoder(16, random);
		
		/*String pass = "123456";
		PasswordEncoder encoder = new BCryptPasswordEncoder(16, random);

		for (int i = 0; i < 3; i++) {
			logger.info("Hash {" + i + "} --> " + encoder.encode(pass));
		}

		pass = "1234567";
		for (int i = 0; i < 4; i++) {
			logger.info("New Hash {" + i + "} --> " + encoder.encode(pass));
		}
		
		/*pass = "ACERVERA1";
		
			logger.info("ACERVERA1 = " + encoder.encode(pass));*/
		String pass = "12345678";
		
		logger.info("12345678 = " + encoder.encode(pass));
		

		//String plainClientCredentials = "eol-padron-ws:987654321";
		//logger.info("Credentials 1: " + new String(Base64.encodeBase64(plainClientCredentials.getBytes())));
	}

}
