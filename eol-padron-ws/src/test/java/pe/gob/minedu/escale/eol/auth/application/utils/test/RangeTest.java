package pe.gob.minedu.escale.eol.auth.application.utils.test;

import java.util.Arrays;

public class RangeTest {
	
public static void main(String[] args) {
    int[] a = new int[] {0,1,2,3,4,5,6,7,8,9};
    int[] b = Arrays.copyOfRange(a, Integer.MIN_VALUE, 10);
    // If we'll use Integer.MIN_VALUE+100 instead Integer.MIN_VALUE,
    // OutOfMemoryError will be thrown
    for (int k = 0; k < b.length; k++)
        System.out.print(b[k] + " ");
   }

}