/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.test;

import static junit.framework.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.junit.Test;
import pe.gob.minedu.escale.rest.client.EolActividadesClient;
import pe.gob.minedu.escale.rest.client.domain.EolActividadesConverter;

/**
 *
 * @author cym
 */
public class ActividadesClientTest {

    private Logger logger = Logger.getLogger(ActividadesClientTest.class);

    @Test
    public void testGetXml() {
        logger.info(":: ActividadesClientTest.testGetXml :: Starting execution");

        EolActividadesClient client = new EolActividadesClient();
        String nombre = "";
        String periodo = "";
        EolActividadesConverter actividades = client.getActividadesConverter(nombre, periodo);

        assertTrue(!(actividades == null));
        logger.info(" value actividades: " + actividades);
        logger.info(":: ActividadesClientTest.testGetXml :: Execution finish.");
    }
}
