package pe.gob.minedu.escale.rest.client.test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;

import pe.gob.minedu.escale.auth.client.OAuth2Client;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.AuthTokenInfoDTO;
import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionRequest;
import pe.gob.minedu.escale.rest.client.InstitucionEducativaClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionesEducativas;

public class InstitucionEducativaClientTest {

    private Logger logger = Logger.getLogger(InstitucionEducativaClientTest.class);

    private InstitucionEducativaClient client = new InstitucionEducativaClient();

    @Test
    public void testConsumeGetIieeXml() {
        logger.info(":: InstitucionEducativaClientTest.testConsumeGetIieeXml :: Starting execution...");
        String dreugel = "200001";
        String[] niveles = new String[]{"B0", "F0"};
        
        InstitucionesEducativas ies = client.getInstitucionesByDreUgel(dreugel, niveles);

        assertEquals(false, ies.getItems().isEmpty());
        
        logger.info(" educational institution name = " + ies.getItems().size());
        
        logger.info(":: InstitucionEducativaClientTest.testConsumeGetIieeXml :: Execution finish.");
    }

	@SuppressWarnings("unchecked")
	@Test
	public void testListInstitucionByParams() {
		logger.info(":: InstitucionEducativaClientTest.testListInstitucionByParams :: Starting execution");
		String ugelRestUrl = "http://localhost:8080/eol-padron-ws/api/v1.0/post/institucionList";
		String accessToken = "776edc79-bdb9-42d1-9121-0fbdb87b4ab8";
		String refreshToken = "4fcecc3e-41fc-45f3-8c73-36e2778192d2";

		List<String> niveles = new ArrayList<String>();
		niveles.add("B0");
		niveles.add("F0");
		
		InstitucionRequest institucionRequest = new InstitucionRequest();
		institucionRequest.setCodooii("200001");
		institucionRequest.setNiveles(niveles);

		InstitucionEducativaClient clientRest = new InstitucionEducativaClient();
		Map<String, Object> result = clientRest.getClientRestInstituciones(ugelRestUrl, accessToken, institucionRequest);
		String status = (String) result.get("status");
		
		if (CoreConstant.RESULT_FAILURE.equals(status)) {
			String message = (String) result.get("message");
			logger.info("return message: "+message);
			if (message != null && "401 Unauthorized".equals(message)) {
				String newToken = getRefresToken(refreshToken);
				result = clientRest.getClientRestInstituciones(ugelRestUrl, newToken, institucionRequest);
				status = (String) result.get("status");
			}
		}
		
		assertTrue(CoreConstant.RESULT_SUCCESS.equals(status));

		List<InstitucionDTO> listInstitucionDTO = (List<InstitucionDTO>) result.get("resultData");

		logger.info("listInstitucionDTO.size = " + listInstitucionDTO.size());
		for (InstitucionDTO institucionDTO : listInstitucionDTO) {
			logger.info("Institucion Educativa = " + institucionDTO.getCenEdu() + " (" + institucionDTO.getCodMod() + ")");
		}
		
		logger.info(":: InstitucionEducativaClientTest.testListInstitucionByParams :: Execution finish.");
	}

	private String getRefresToken(String refreshToken) {
		String newToken = "";
		
		String tokenUri = "http://localhost:8080/eol-auth-ws/oauth/token";
		String grantType = "password";
		String clientId = "eol-auth-ws";
		String clientSecret = "987654321";
		String username = "root";
		String password = "123456";
		
		String authorization = "ZW9sLWF1dGgtd3M6OTg3NjU0MzIx";
		
		OAuth2Client oauth2Client = new OAuth2Client(tokenUri, grantType, clientId, clientSecret, username, password);

		AuthTokenInfoDTO authTokenInfo = oauth2Client.getRefreshTokenInfoEol(refreshToken, authorization);
		
		if (authTokenInfo != null) {
			newToken = authTokenInfo.getAccessToken();
		}
		
		return newToken;
		
	}
}
