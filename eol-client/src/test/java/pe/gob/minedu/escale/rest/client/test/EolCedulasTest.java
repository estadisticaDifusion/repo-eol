package pe.gob.minedu.escale.rest.client.test;

import java.util.List;
import static junit.framework.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.junit.Test;

import pe.gob.minedu.escale.eol.converter.EolCedulaConverter;
import pe.gob.minedu.escale.rest.client.EolCedulasClient;

public class EolCedulasTest {

    private Logger logger = Logger.getLogger(EolCedulasTest.class);

    private EolCedulasClient client = new EolCedulasClient();

    @Test
    public void testGetCedulasConverter() {
        logger.info(":: EolCedulasTest.testGetCedulasConverter :: Starting execution");

        String nivel = "B0";
        String anio = "2018";
        String nombreActividad = null;

        List<EolCedulaConverter> cedulas = client.getCedulasConverter(nivel, anio, nombreActividad).getItems();

        assertTrue(!cedulas.isEmpty());
        logger.info(" values cedulas: " + cedulas);
        logger.info(":: EolCedulasTest.testGetCedulasConverter :: Execution finish.");
    }
}
