package pe.gob.minedu.escale.rest.client.test;

import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.Test;

import pe.gob.minedu.escale.auth.client.OAuth2Client;
import pe.gob.minedu.escale.eol.dto.AuthTokenInfoDTO;

public class OAuth2ClientTest {

	private Logger logger = Logger.getLogger(OAuth2ClientTest.class);

	private String tokenUri = "http://localhost:8080/eol-auth-ws/oauth/token";
	private String grantType = "password";
	private String clientId = "eol-auth-ws";
	private String clientSecret = "987654321";
	private String username = "root";
	private String password = "123456";
	
	@Test
	public void testTokenOAuth2() {
		logger.info(":: OAuth2ClientTest.testTokenOAuth2 :: Starting execution");

		username = "150102";
		password = "2504";
		
		OAuth2Client oauth2Client = new OAuth2Client(tokenUri, grantType, clientId, clientSecret, username, password);
		
		AuthTokenInfoDTO authTokenInfo = oauth2Client.getTokenInfoEol();
		
		assertNotNull(authTokenInfo);
		
		logger.info("(3) AccessToken ->" + authTokenInfo.getAccessToken());
		logger.info("(3) RefreshToken ->" + authTokenInfo.getRefreshToken());
		logger.info("(3) Scope ->" + authTokenInfo.getScope());
		logger.info("(3) TokenType ->" + authTokenInfo.getTokenType());
		logger.info("(3) ExpiresIn ->" + authTokenInfo.getExpiresIn());
		
		logger.info(":: OAuth2ClientTest.testTokenOAuth2 :: Execution finish.");
	}
	
	@Test
	public void testRefreshTokenOAuth2() {
		logger.info(":: OAuth2ClientTest.testRefreshTokenOAuth2 :: Starting execution");
		grantType = "refresh_token";
		String token = "bc2f7c4a-d50a-4082-8405-710e8f51c54a";
		String authorization = "";
		
		OAuth2Client oauth2Client = new OAuth2Client(tokenUri, grantType, clientId, clientSecret, username, password);

		AuthTokenInfoDTO authTokenInfo = oauth2Client.getRefreshTokenInfoEol(token, authorization);
		
		assertNotNull(authTokenInfo);
		
		logger.info("(3) AccessToken ->" + authTokenInfo.getAccessToken());
		logger.info("(3) RefreshToken ->" + authTokenInfo.getRefreshToken());
		logger.info("(3) Scope ->" + authTokenInfo.getScope());
		logger.info("(3) TokenType ->" + authTokenInfo.getTokenType());
		logger.info("(3) ExpiresIn ->" + authTokenInfo.getExpiresIn());

		logger.info(":: OAuth2ClientTest.testRefreshTokenOAuth2 :: Execution finish.");
	}
}
