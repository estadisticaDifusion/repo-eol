/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.test;

import static junit.framework.Assert.assertTrue;

import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;

import pe.gob.minedu.escale.auth.client.OAuth2Client;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.AuthTokenInfoDTO;
import pe.gob.minedu.escale.eol.dto.entities.UgelDTO;
import pe.gob.minedu.escale.eol.payload.padron.UgelRequest;
import pe.gob.minedu.escale.rest.client.UgelClient;
import pe.gob.minedu.escale.rest.client.domain.Ugel;

/**
 *
 * @author cym
 */
public class UgelClientTest {

	private Logger logger = Logger.getLogger(UgelClientTest.class);

	@Test
	public void testGetXml() {
		logger.info(":: UgelClientTest.testGetXml :: Starting execution");

		String codigoUgel = "150102";

		UgelClient client = new UgelClient(codigoUgel);
		Ugel ugel = client.get_XML(Ugel.class);

		assertTrue(!(ugel == null));
		logger.info(" value ugel: " + ugel);
		logger.info(":: UgelClientTest.testGetXml :: Execution finish.");
	}

	@Test
	public void testRestUgelByCodigo() {
		logger.info(":: UgelClientTest.testRestUgelByCodigo :: Starting execution");
		String ugelRestUrl = "http://localhost:8080/eol-padron-ws/api/v1.0/post/ugel";
		String accessToken = "be1db47c-cd59-443b-b40d-1f466b729ffe";
		String refreshToken = "4fcecc3e-41fc-45f3-8c73-36e2778192d2";

		UgelRequest ugelRequest = new UgelRequest();
		ugelRequest.setId("200001");

		UgelClient clientRest = new UgelClient();
		Map<String, Object> result = clientRest.getUgel(ugelRestUrl, accessToken, ugelRequest);
		String status = (String) result.get("status");
		
		if (CoreConstant.RESULT_FAILURE.equals(status)) {
			String message = (String) result.get("message");
			logger.info("return message: "+message);
			if (message != null && "401 Unauthorized".equals(message)) {
				String newToken = getRefresToken(refreshToken);
				result = clientRest.getUgel(ugelRestUrl, newToken, ugelRequest);
				status = (String) result.get("status");
			}
		}
		
		assertTrue(CoreConstant.RESULT_SUCCESS.equals(status));

		UgelDTO ugelDTO = (UgelDTO) result.get("resultData");

		logger.info(" value ugel: " + ugelDTO.getNombreUgel());
		logger.info(":: UgelClientTest.testRestUgelByCodigo :: Execution finish.");
	}
	
	private String getRefresToken(String refreshToken) {
		String newToken = "";
		
		String tokenUri = "http://localhost:8080/eol-auth-ws/oauth/token";
		String grantType = "password";
		String clientId = "eol-auth-ws";
		String clientSecret = "987654321";
		String username = "root";
		String password = "123456";
		
		String authorization = "ZW9sLWF1dGgtd3M6OTg3NjU0MzIx";
		
		OAuth2Client oauth2Client = new OAuth2Client(tokenUri, grantType, clientId, clientSecret, username, password);

		AuthTokenInfoDTO authTokenInfo = oauth2Client.getRefreshTokenInfoEol(refreshToken, authorization);
		
		if (authTokenInfo != null) {
			newToken = authTokenInfo.getAccessToken();
		}
		
		return newToken;
		
	}
}
