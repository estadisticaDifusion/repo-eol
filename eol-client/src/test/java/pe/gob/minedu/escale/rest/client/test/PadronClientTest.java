package pe.gob.minedu.escale.rest.client.test;

import java.util.HashMap;
import java.util.Map;
import static junit.framework.Assert.*;
import org.apache.log4j.Logger;
import org.junit.Test;
import pe.gob.minedu.escale.rest.client.PadronClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.rest.client.domain.Instituciones;

public class PadronClientTest {

    private Logger logger = Logger.getLogger(PadronClientTest.class);

    private PadronClient client = new PadronClient();

    @Test
    public void testConsumeGetResourceXml() {
        logger.info(":: PadronClientTest.testConsumeGetResourceXml :: Starting execution");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("codmod", "1238179");
        parameters.put("orderBy", "anexo");

        Instituciones ies = client.consumeGetResourceXml(Instituciones.class, "post/getInstituciones", parameters);

        assertEquals(false, ies.getInstitucion().isEmpty());
        
        for (InstitucionEducativaIE ie : ies.getInstitucion()) {
            logger.info(" educational institution name = " + ie.getCentroEducativo());
        }
        
        logger.info(":: PadronClientTest.testConsumeGetResourceXml :: Execution finish.");
    }
    
    @Test
    public void testConsumeGetResourceTextPlain() {
        logger.info(":: PadronClientTest.testConsumeGetResourceTextPlain :: Starting execution");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("estados", "1");
        parameters.put("ugel", "150102");
        parameters.put("start", "0");
        parameters.put("niveles", "B0");

        String count = client.consumeGetResourceTextPlain(String.class, "post/getCuentaInstitucion", parameters);

        assertNotNull(count);
        
        logger.info(" educational institution count = " + count);
        
        logger.info(":: PadronClientTest.testConsumeGetResourceTextPlain :: Execution finish.");
    }
}
