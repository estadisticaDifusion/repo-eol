/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionRequest;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionResponse;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativa;
import pe.gob.minedu.escale.rest.client.domain.InstitucionesEducativas;
import pe.gob.minedu.escale.rest.constant.Constantes;

/**
 *
 * @author OMATEO
 */
public class InstitucionEducativaClient {

	private Logger logger = Logger.getLogger(InstitucionEducativaClient.class);

	private Client client;

	public InstitucionEducativaClient() {
		client = ClientBuilder.newClient();
	}

	public InstitucionEducativaClient(String codMod, String anexo) {
		//String resourcePath = java.text.MessageFormat.format("iiee/{0}/{1}", new Object[] { codMod, anexo });
		client = ClientBuilder.newClient();
		// WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
	}

	public InstitucionesEducativas getInstitucionesByUbigeo(String ubigeo) {
		WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS);
		target = target.queryParam("ubigeo", ubigeo);
		return target.request(MediaType.APPLICATION_JSON).get(new GenericType<InstitucionesEducativas>() {
		});
	}

	public InstitucionesEducativas getInstitucionesByDreUgel(String dreugel, String[] niveles) {
		WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path("get/iiee");

		target = target.queryParam("codDreUgel", dreugel);
		if (niveles != null && niveles.length > 0) {
			for (String nivel : niveles) {
				target = target.queryParam("niveles", nivel);
			}
		}
		logger.info(" getInstitucionesByDreUgel.URL: " + target.getUri().toString());
		return target.request(MediaType.APPLICATION_XML).get(new GenericType<InstitucionesEducativas>() {
		});
	}

	public InstitucionesEducativas getInstitucionesProgArti(String dreugel, String progarti) {
		WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path("get/iiee");
		target.queryParam("codDreUgel", dreugel);
		target = target.queryParam("progarti", progarti);
		return target.request(MediaType.APPLICATION_XML).get(new GenericType<InstitucionesEducativas>() {
		});
	}

	public InstitucionesEducativas getInstitucionesProgIse(String dreugel, String progise) {
		WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path("get/iiee");
		target.queryParam("codDreUgel", dreugel);
		target = target.queryParam("progise", progise);

		return target.request(MediaType.APPLICATION_XML).get(new GenericType<InstitucionesEducativas>() {
		});
	}

	public InstitucionEducativa getInstitucion() {
		WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS);
		return target.request(MediaType.APPLICATION_JSON).get(new GenericType<InstitucionEducativa>() {
		});
	}

	public Map<String, Object> getClientRestInstituciones(String restUrl, String accessToken, InstitucionRequest institucionRequest) {
		logger.info(":: InstitucionEducativaClient.getClientRestInstituciones :: Starting execution...");
		Map<String, Object> dataResult = new HashMap<String, Object>();

		Gson gson = new Gson();

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + accessToken);

		String jsonString = gson.toJson(institucionRequest);

		logger.info("jsonString: " + jsonString);
		
		dataResult.put("status", CoreConstant.RESULT_FAILURE);

		ResponseEntity<InstitucionResponse> responseEntity = null;
		HttpEntity<String> request = new HttpEntity<String>(jsonString, headers);
		try {
			responseEntity = restTemplate.exchange(restUrl, HttpMethod.POST, request, InstitucionResponse.class);

			InstitucionResponse institucionResponse = responseEntity.getBody();

			if (CoreConstant.RESULT_SUCCESS.equals(institucionResponse.getStatus())
					&& CoreConstant.DATA_FOUND.equals(institucionResponse.getResponsePayload().getDataResult())) {
				List<InstitucionDTO> listInstitucionDTO= institucionResponse.getResponsePayload().getDataDetail();
				dataResult.put("status", CoreConstant.RESULT_SUCCESS);
				dataResult.put("resultData", listInstitucionDTO);
			}

		} catch (Exception e) {
			dataResult.put("message", e.getMessage());
			logger.info("ERROR invoke REST InstitucionEducativaClient.getClientRestInstituciones: "+e.getMessage());
		}
		logger.info("responseEntity: " + responseEntity);

		logger.info(":: InstitucionEducativaClient.getClientRestInstituciones :: Starting execution...");
		return dataResult;
	}
	
}
