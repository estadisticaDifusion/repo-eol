/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="gestiones")
public class Gestiones  extends Codigos{



@XmlElement   
public Collection<Gestion> getGestion(){
    return super.items;
}
    
public void setGestion(Collection<Gestion> items){
super.setItems(items);
}


}
