/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.entities.UgelDTO;
import pe.gob.minedu.escale.eol.payload.padron.UgelRequest;
import pe.gob.minedu.escale.eol.payload.padron.UgelResponse;
import pe.gob.minedu.escale.rest.client.domain.Ugel;
import pe.gob.minedu.escale.rest.constant.Constantes;

/**
 * Jersey REST client generated for REST resource:codigos-rest
 * [ugels/{idUgel}/]<br>
 * USAGE: UgelClient client = new UgelClient(); Object response =
 * client.XXX(...); // do whatever with response client.close();
 *
 * @author DSILVA update
 */
public class UgelClient {

	private Logger logger = Logger.getLogger(UgelClient.class);
	private WebTarget target;
	private Client client;

	public UgelClient() {

	}

	public UgelClient(String idUgel) {
		client = ClientBuilder.newClient();
		String resourcePath = java.text.MessageFormat.format("get/ugel/{0}", new Object[] { idUgel });
		// target = client.target(Constantes.BASE_URI).path(resourcePath);
		target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
	}

	/*
	 * public void setResourcePath(String idUgel) { String resourcePath =
	 * java.text.MessageFormat.format("ugels/{0}", new Object[]{idUgel});
	 * webResource = client.resource(BASE_URI).path(resourcePath); }
	 */
	/**
	 * @param responseType        Class representing the response
	 * @param optionalQueryParams List of optional query parameters in the form of
	 *                            "param_name=param_value",...<br>
	 *                            List of optional query parameters:
	 *                            <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "1"]
	 * @return response object (instance of responseType class)
	 */
	public <T> T get_XML(Class<T> responseType, String... optionalQueryParams) { // throws UniformInterfaceException
//        return webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
		logger.info(" get_XML: getUri: " + target.getUri());
		return target.request(MediaType.APPLICATION_XML).get(responseType);
	}

	/**
	 * @param responseType        Class representing the response
	 * @param optionalQueryParams List of optional query parameters in the form of
	 *                            "param_name=param_value",...<br>
	 *                            List of optional query parameters:
	 *                            <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "1"]
	 * @return response object (instance of responseType class)
	 */
	public <T> T get_JSON(Class<T> responseType, String... optionalQueryParams) { // throws UniformInterfaceException
		// return
		// webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
		logger.info(" get_JSON: getUri: " + target.getUri());
		return target.request(MediaType.APPLICATION_JSON).get(responseType);
	}

	public Ugel getUgel(String... optionalQueryParams) {
		/*
		 * return
		 * webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.
		 * core.MediaType.APPLICATION_JSON).get(new GenericType<Ugel>() { });
		 */
		return target.request(MediaType.APPLICATION_JSON).get(new GenericType<Ugel>() {
		});
	}

	public Map<String, Object> getUgel(String ugelRestUrl, String accessToken, UgelRequest ugelRequest) {
		logger.info(":: UgelClient.getUgel :: Starting execution...");
		Map<String, Object> dataResult = new HashMap<String, Object>();

		Gson gson = new Gson();

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + accessToken);

		String jsonString = gson.toJson(ugelRequest);

		logger.info("jsonString: " + jsonString);
		
		dataResult.put("status", CoreConstant.RESULT_FAILURE);

		ResponseEntity<UgelResponse> responseEntity = null;
		HttpEntity<String> request = new HttpEntity<String>(jsonString, headers);
		try {
			responseEntity = restTemplate.exchange(ugelRestUrl, HttpMethod.POST, request, UgelResponse.class);

			UgelResponse ugelResponse = responseEntity.getBody();

			if (CoreConstant.RESULT_SUCCESS.equals(ugelResponse.getStatus())
					&& CoreConstant.DATA_FOUND.equals(ugelResponse.getResponsePayload().getDataResult())) {
				UgelDTO ugelDTO = ugelResponse.getResponsePayload().getDataDetail();
				dataResult.put("status", CoreConstant.RESULT_SUCCESS);
				dataResult.put("resultData", ugelDTO);
			}

		} catch (Exception e) {
			dataResult.put("message", e.getMessage());
			logger.info("ERROR invoke REST: "+e.getMessage());
		}
		logger.info("responseEntity: " + responseEntity);

		logger.info(":: UgelClient.getUgel :: Starting execution...");
		return dataResult;
	}
}
