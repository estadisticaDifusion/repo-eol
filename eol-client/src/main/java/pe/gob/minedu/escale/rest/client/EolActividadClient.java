/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.rest.constant.Constantes;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class EolActividadClient {

    private WebTarget target;
    private Client client;

    public EolActividadClient(Integer idActividad) {
        client = ClientBuilder.newClient();
        String resourcePath = java.text.MessageFormat.format("get/actividades/{0}", idActividad.toString());
        target = client.target(Constantes.BASE_URI_EOL_ADMIN).path(resourcePath);
    }

    public EolActividadConverter getActividad() {
        return target.request(MediaType.APPLICATION_JSON).get(new javax.ws.rs.core.GenericType<EolActividadConverter>() {
        });
    }
}
