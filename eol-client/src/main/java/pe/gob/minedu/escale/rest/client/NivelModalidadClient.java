/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import pe.gob.minedu.escale.rest.client.domain.NivelModalidad;
import pe.gob.minedu.escale.rest.constant.Constantes;

/**
 *
 * @author jmatamoros
 */
public class NivelModalidadClient {

    //private WebResource webResource;
    private Client client;

    public NivelModalidadClient() {
        //com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = ClientBuilder.newClient(); //Client.create(config);

        //webResource = client.resource(BASE_URI).path(resourcePath);
    }

    public NivelModalidad getNivelModalidad(String idCodigo) {
        String resourcePath = java.text.MessageFormat.format("codigos/nivelesModalidades/{0}", new Object[]{idCodigo});
//        WebTarget target = client.target(Constantes.BASE_URI).path(resourcePath);
        WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
        return target.request(MediaType.APPLICATION_JSON).get(new GenericType<NivelModalidad>() {
        });
        /*
        return webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<NivelModalidad>() {
        });*/
    }

}
