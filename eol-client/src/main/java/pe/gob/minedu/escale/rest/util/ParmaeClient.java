/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import pe.gob.minedu.escale.rest.client.domain.Codigo;
import pe.gob.minedu.escale.rest.client.domain.Codigos;
import pe.gob.minedu.escale.rest.constant.Constantes;

/**
 *
 * @author jmatamoros
 */
public class ParmaeClient {

    //  private static WebResource webResource;
    private static Client client;
    public static Map<String, List<Codigo>> mapParmae = new HashMap<String, List<Codigo>>();
    public static Map<String, Codigos> mapCodigos = new HashMap<String, Codigos>();
//    public static com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();

    public static Codigo getCodigoMap(String nombreClase, String tipoCodigo, String idCodigo) {
        if (mapParmae.get(nombreClase) == null) {
            Codigos<Codigo> codigos = getCodigos(nombreClase, tipoCodigo);
            for (Codigo cod : codigos.getItems()) {
                if (idCodigo.equals(cod.getIdCodigo())) {
                    return cod;
                }
            }
        }
        return null;

        //return webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<Codigo>(){});
    }

    public static Codigo getCodigo(String nombreClase, String tipoCodigo, String idCodigo) {
        if (mapParmae.get(nombreClase) == null) {
            try {
                Class clase = Class.forName(nombreClase);
                client = ClientBuilder.newClient();
                String resourcePath = java.text.MessageFormat.format("codigos/{0}/{1}", new Object[]{tipoCodigo, idCodigo});
                //WebTarget target = client.target(Constantes.BASE_URI);
                WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS);
                target.path(resourcePath);
                return (Codigo) target.request(MediaType.APPLICATION_JSON).get(clase);
            } catch (ClassNotFoundException ex) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static Codigos getCodigos(String nombreClase, String tipCodigo) {
        if (!(mapCodigos.get(nombreClase) != null)) {
            try {
                Class clase = Class.forName(nombreClase);
                //com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
                client = ClientBuilder.newClient();  // Client.create(config); 
                String resourcePath = java.text.MessageFormat.format("codigos/{0}", tipCodigo);
                //webResource = client.resource(BASE_URI).path(resourcePath);
                WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS);
                target.path(resourcePath);
                mapCodigos.put(nombreClase, null);
                //Codigos retCodigos = (Codigos) webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(clase);
                Codigos retCodigos = (Codigos) target.request(MediaType.APPLICATION_JSON).get(clase);
                mapCodigos.put(nombreClase, retCodigos);
                return retCodigos;
            } catch (ClassNotFoundException ex) {
                return null;
            }
        } else {
            return mapCodigos.get(nombreClase);
        }
        //return webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<Codigo>(){});
    }

    public static <T> T getCodigo(Class<T> responseType, String codigo, String idCodigo) {//com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = ClientBuilder.newClient(); // Client.create(config);
        String resourcePath = java.text.MessageFormat.format("codigos/{0}/{1}", new Object[]{codigo, idCodigo});
        WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS);
        target.path(resourcePath);
        return target.request(MediaType.APPLICATION_XML).get(responseType);
    }

    public static <T> T getCodigos(Class<T> responseType, String codigo) {
        client = ClientBuilder.newClient();
        String resourcePath = java.text.MessageFormat.format("codigos/{0}", codigo);
        WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS);
        target.path(resourcePath);
        return target.request(MediaType.APPLICATION_XML).get(responseType);
    }

    public static Codigos getCodigo(String tipCampo) {
        if (!mapCodigos.containsKey(tipCampo)) {
            client = ClientBuilder.newClient();
            String resourcePath = java.text.MessageFormat.format("codigos/{0}", tipCampo);
            WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS);
            target.path(resourcePath);
            return target.request(MediaType.APPLICATION_XML).get(Codigos.class);
        } else {
            return mapCodigos.get(tipCampo);
        }
    }
}
