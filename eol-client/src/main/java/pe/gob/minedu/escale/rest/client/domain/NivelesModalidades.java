/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="nivelesModalidades")
public class NivelesModalidades  extends Codigos{



@XmlElement   
public Collection<NivelModalidad> getNivelModalidad(){
    return super.items;
}
    
public void setNivelModalidad(Collection<NivelModalidad> items){
super.setItems(items);
}


}
