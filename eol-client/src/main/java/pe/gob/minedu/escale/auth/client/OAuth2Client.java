package pe.gob.minedu.escale.auth.client;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.client.RestTemplate;

import pe.gob.minedu.escale.eol.dto.AuthTokenInfoDTO;
import pe.gob.minedu.escale.eol.payload.oauth2.OAuth2Response;

public class OAuth2Client {

	private Logger logger = Logger.getLogger(OAuth2Client.class);

	private String tokenUri;
	private String grantType;
	private String clientId;
	private String clientSecret;
	private String username;
	private String password;

	public OAuth2Client() {

	}

	public OAuth2Client(String tokenUri) {
		this.tokenUri = tokenUri;
	}

	public OAuth2Client(String tokenUri, String grantType, String clientId, String clientSecret, String username,
			String password) {
		this.tokenUri = tokenUri;
		this.grantType = grantType;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.username = username;
		this.password = password;
	}
	
	public OAuth2Client(String tokenUri, String clientId, String clientSecret) {
		this.tokenUri = tokenUri;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}

	public OAuth2Response removeTokenEol(String tokenId) {
		logger.info(":: OAuth2Client.removeTokenEol :: Starting execution...");

		RestTemplate restTemplate = new RestTemplate();
		
		String endpointAuth2 = tokenUri;

		logger.info("endpointAuth2 = " + endpointAuth2);
		
		HttpHeaders headers = getHeadersWithClientCredentials();
		headers.add("tokenId", tokenId);
		
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<OAuth2Response> responsePayload = restTemplate.exchange(endpointAuth2, HttpMethod.DELETE, request,
				OAuth2Response.class);
		OAuth2Response response = responsePayload.getBody();
		logger.info(":: OAuth2Client.removeTokenEol :: Execution finish.");
		return response;
	}

	public OAuth2RestTemplate restTemplate() {
		logger.info(":: OAuth2Client.restTemplate :: Starting execution...");

		logger.info("tokenUri: " + tokenUri + "; grantType: " + grantType);

		ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
		resourceDetails.setGrantType(grantType);
		resourceDetails.setAccessTokenUri(tokenUri);

		// -- set the clients info
		resourceDetails.setClientId(clientId);
		resourceDetails.setClientSecret(clientSecret);

		// set scopes
		/*
		 * List<String> scopes = new ArrayList<String>(); scopes.add("read");
		 * scopes.add("write"); scopes.add("trust"); resourceDetails.setScope(scopes);
		 */

		// -- set Resource Owner info
		resourceDetails.setUsername(username);
		resourceDetails.setPassword(password);

		logger.info(":: OAuth2Client.restTemplate :: Execution finish.");

		return new OAuth2RestTemplate(resourceDetails);
	}

	public OAuth2AccessToken generateToken() {
		logger.info(":: OAuth2Client.generateToken :: Starting execution...");

		ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
		resource.setGrantType(grantType);
		resource.setAccessTokenUri(tokenUri);
		resource.setClientId(clientId);
		resource.setClientSecret(clientSecret);

		// resource.setScope(Arrays.asList("trust"));

		ClientCredentialsAccessTokenProvider provider = new ClientCredentialsAccessTokenProvider();
		OAuth2AccessToken accessToken = provider.obtainAccessToken(resource, new DefaultAccessTokenRequest());

		logger.info(":: OAuth2Client.generateToken :: Execution finish.");

		return accessToken;
	}

	@SuppressWarnings("unchecked")
	public AuthTokenInfoDTO getTokenInfoEol() {
		logger.info(":: OAuth2Client.getTokenInfoEol :: Starting execution...");
		AuthTokenInfoDTO tokenInfo = null;
		try {
		String QPM_PASSWORD_GRANT = "?grant_type=" + grantType + "&username=" + username + "&password=" + password;
		RestTemplate restTemplate = new RestTemplate();
		
		String endpointAuth2 = tokenUri + QPM_PASSWORD_GRANT;

		logger.info("endpointAuth2 = " + endpointAuth2);
		
		HttpEntity<String> request = new HttpEntity<String>(getHeadersWithClientCredentials());
		logger.info("AUTH2 : "  + request  );
		ResponseEntity<Object> response = restTemplate.exchange(endpointAuth2, HttpMethod.POST, request,
				Object.class);
		logger.info("AUTH2 11111 : "  + response  );
		LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) response.getBody();
		logger.info("AUTH2 2222 : "  + map  );
//		AuthTokenInfoDTO tokenInfo = null;

		if (map != null) {
			logger.info("AUTH2 33333: "  + map  );
			tokenInfo = new AuthTokenInfoDTO();
			tokenInfo.setAccessToken((String) map.get("access_token"));
			tokenInfo.setTokenType((String) map.get("token_type"));
			tokenInfo.setRefreshToken((String) map.get("refresh_token"));
			tokenInfo.setExpiresIn((Integer) map.get("expires_in"));
			tokenInfo.setScope((String) map.get("scope"));
			
			logger.info("AUTH2 44444  "+ tokenInfo);
			
			String base64ClientCredentials = encryptCredentials();
			tokenInfo.setAuthorization(base64ClientCredentials);
		} else {
			logger.info("AUTH2 : 55555 "  + request  );
			logger.info("No user exist----------");
			throw new Exception();
		}
		logger.info("AUTH2 66666 : "  + tokenInfo  );
		logger.info(":: OAuth2Client.getTokenInfoEol :: Execution finish.");
		
		
		}catch(Exception e) {
			
		}
		return tokenInfo;
	}

	@SuppressWarnings("unchecked")
	public AuthTokenInfoDTO getRefreshTokenInfoEol(String token, String autorizathion) {
		logger.info(":: OAuth2Client.getTokenInfoEol :: Starting execution...");

		String QPM_PASSWORD_GRANT = "?grant_type=refresh_token&refresh_token=" + token;
		RestTemplate restTemplate = new RestTemplate();

		String endpointAuth2 = tokenUri + QPM_PASSWORD_GRANT;
		logger.info("endpointAuth2 = " + endpointAuth2);
		
		HttpEntity<String> request = new HttpEntity<String>(getHeadersRefreshToken(autorizathion));
		LinkedHashMap<String, Object> map = null;
		try {
			ResponseEntity<Object> response = restTemplate.exchange(endpointAuth2, HttpMethod.POST, request, Object.class);
			map = (LinkedHashMap<String, Object>) response.getBody();
		} catch (Exception e) {
			logger.error("ERROR getRefreshTokenInfoEol = " + e.getMessage());
			e.printStackTrace();
		}

		AuthTokenInfoDTO tokenInfo = null;
		if (map != null) {
			tokenInfo = new AuthTokenInfoDTO();
			tokenInfo.setAccessToken((String) map.get("access_token"));
			tokenInfo.setTokenType((String) map.get("token_type"));
			tokenInfo.setRefreshToken((String) map.get("refresh_token"));
			tokenInfo.setExpiresIn((Integer) map.get("expires_in"));
			tokenInfo.setScope((String) map.get("scope"));
			tokenInfo.setAuthorization(autorizathion);
		} else {
			logger.info("No user exist----------");

		}
		logger.info(":: OAuth2Client.getTokenInfoEol :: Execution finish.");
		return tokenInfo;
	}

	private static HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		return headers;
	}

	private HttpHeaders getHeadersWithClientCredentials() {
		String base64ClientCredentials = encryptCredentials();
		HttpHeaders headers = getHeaders();
		headers.add("Authorization", "Basic " + base64ClientCredentials);
		return headers;
	}

	private HttpHeaders getHeadersRefreshToken(String autorizathion) {
		HttpHeaders headers = getHeaders();
		headers.add("Authorization", "Basic " + autorizathion);
		return headers;
	}

	private String encryptCredentials() {
		String plainClientCredentials = clientId + ":" + clientSecret;
		return new String(Base64.encodeBase64(plainClientCredentials.getBytes()));
	}

}
