/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name="direccionRegional")
public class DireccionRegional   {

    private static final long serialVersionUID = 1L;
    private String id;
    private String nombreDre;
  

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreDre() {
        return nombreDre;
    }

    public void setNombreDre(String nombreDre) {
        this.nombreDre = nombreDre;
    }

    @Override
    public String toString() {
        return nombreDre;
    }

  

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DireccionRegional other = (DireccionRegional) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    

}
