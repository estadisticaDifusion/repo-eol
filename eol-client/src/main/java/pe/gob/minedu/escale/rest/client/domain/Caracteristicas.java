/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="caracteristicas")
public class Caracteristicas  extends Codigos{



@XmlElement
public Collection<Caracteristica> getCaracteristica(){
    return super.items;
}

public void setCaracteristica(Collection<Caracteristica> items){
super.setItems(items);
}


}
