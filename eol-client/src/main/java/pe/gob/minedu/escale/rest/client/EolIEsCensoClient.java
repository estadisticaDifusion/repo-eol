/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import javax.ws.rs.client.Client;
import pe.gob.minedu.escale.rest.client.domain.EolIEsCensoConverter;
import pe.gob.minedu.escale.rest.constant.Constantes;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * Jersey REST client generated for REST resource:codigos-rest
 * [distritos/{idDistrito}/]<br>
 * USAGE:
 * <pre>
 *        DistritoClient client = new DistritoClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author DSILVA
 */
public class EolIEsCensoClient {

    private WebTarget target;
    //private WebResource webResource;
    private Client client;

    public EolIEsCensoClient() {
        //com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = ClientBuilder.newClient(); //Client.create(config);
        //webResource = client.resource(BASE_URI_EOL_ADMIN).path("/ies_censo");
        target = client.target(Constantes.BASE_URI_EOL_ADMIN).path("/ies_censo");
    }

    public EolIEsCensoConverter getEnvios(String nivel, String codMod, String anexo, String codLocal) {
        /*return webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<List<InstitucionEducativa>>() {
        });
        String[] queryParamNames = new String[]{"niveles", "codmod", "anexo", "codlocal"};
        String[] queryParamValues = new String[]{nivel, codMod, anexo, codLocal};
*/
        target = target.queryParam("niveles", nivel);
        target = target.queryParam("codmod", codMod);
        target = target.queryParam("anexo", anexo);
        target = target.queryParam("codlocal", codLocal);

        /*        return webResource.queryParams(getQueryOrFormParams(queryParamNames, queryParamValues)).accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<EolIEsCensoConverter>() {
        });*/
        return target.request(MediaType.APPLICATION_JSON).get(new javax.ws.rs.core.GenericType<EolIEsCensoConverter>() {
        });
    }
    /*
    private MultivaluedMap getQueryOrFormParams(String[] paramNames, String[] paramValues) {
        MultivaluedMap<String, String> qParams = new com.sun.jersey.api.representation.Form();
        for (int i = 0; i < paramNames.length; i++) {
            if (paramValues[i] != null) {
                qParams.add(paramNames[i], paramValues[i]);
            }
        }
        return qParams;
    }

    public void close() {
        client.destroy();
    }*/

}
