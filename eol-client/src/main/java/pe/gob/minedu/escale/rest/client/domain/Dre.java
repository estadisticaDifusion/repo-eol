/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "direccionRegional")
public class Dre {
private String id;
private String nombreDre;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the nombreDre
     */
    public String getNombreDre() {
        return nombreDre;
    }

    /**
     * @param nombreDre the nombreDre to set
     */
    public void setNombreDre(String nombreDre) {
        this.nombreDre = nombreDre;
    }

}
