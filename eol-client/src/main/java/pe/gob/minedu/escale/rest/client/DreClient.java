/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import pe.gob.minedu.escale.rest.client.domain.DireccionRegional;
import pe.gob.minedu.escale.rest.constant.Constantes;

/**
 * @author DSILVA
 * Update: Ing. Oscar Mateo
 */
public class DreClient {

    private WebTarget target;
    private Client client;

    public DreClient(String id) {
        client = ClientBuilder.newClient();
        String resourcePath = java.text.MessageFormat.format("direccionesRegionales/{0}", new Object[]{id});
//        target = client.target(Constantes.BASE_URI).path(resourcePath);
        target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
    }

    public void setResourcePath(String id) {
        String resourcePath = java.text.MessageFormat.format("direccionesRegionales/{0}", new Object[]{id});
        target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
    }

    /**
     * @param responseType Class representing the response
     * @param optionalQueryParams List of optional query parameters in the form
     * of "param_name=param_value",...<br>
     * List of optional query parameters:
     * <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "1"]
     * @return response object (instance of responseType class)
     */
    public <T> T get_XML(Class<T> responseType, String... optionalQueryParams) { // throws UniformInterfaceException
//        return webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
        return target.request(MediaType.APPLICATION_XML).get(responseType);
    }

    /**
     * @param responseType Class representing the response
     * @param optionalQueryParams List of optional query parameters in the form
     * of "param_name=param_value",...<br>
     * List of optional query parameters:
     * <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "1"]
     * @return response object (instance of responseType class)
     */
    public <T> T get_JSON(Class<T> responseType, String... optionalQueryParams) { // throws UniformInterfaceException
//        return webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
        return target.request(MediaType.APPLICATION_JSON).get(responseType);
    }

    public DireccionRegional getDre(String... optionalQueryParams) {
        /*        return webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<DireccionRegional>() {
        });*/
        return target.request(MediaType.APPLICATION_JSON).get(new GenericType<DireccionRegional>() {
        });
    }
/*
    private MultivaluedMap getQParams(String... optionalParams) {
        MultivaluedMap<String, String> qParams = new com.sun.jersey.api.representation.Form();
        for (String qParam : optionalParams) {
            String[] qPar = qParam.split("=");
            if (qPar.length > 1) {
                qParams.add(qPar[0], qPar[1]);
            }
        }
        return qParams;
    }

    public void close() {
        client.destroy();
    }*/
}
