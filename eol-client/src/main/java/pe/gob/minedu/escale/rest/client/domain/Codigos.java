/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.domain;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;


/**
 *
 * @author DSILVA
 */
public abstract class Codigos<S extends Codigo> {
    protected Collection<S> items;
    
    protected int expandLevel;

    public Codigos() {
    }

    public Collection<S> getItems() {
        if (items == null) {
            items = new ArrayList<S>();
        }
        
        return items;
    }

    protected void setItems(Collection<S> items) {
        this.items = items;
    }

   
}
