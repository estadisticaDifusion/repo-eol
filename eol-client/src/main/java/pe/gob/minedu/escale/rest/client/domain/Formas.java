/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="formas")
public class Formas  extends Codigos{



@XmlElement   
public Collection<Forma> getForma(){
    return super.items;
}
    
public void setForma(Collection<Forma> items){
super.setItems(items);
}


}
