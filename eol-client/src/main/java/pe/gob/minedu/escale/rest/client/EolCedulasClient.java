/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import pe.gob.minedu.escale.rest.client.domain.EolCedulasConverter;
import pe.gob.minedu.escale.rest.constant.Constantes;

/**
 * Jersey REST client generated for REST resource:codigos-rest
 * [distritos/{idDistrito}/]<br>
 * USAGE:
 * 
 * <pre>
 *        DistritoClient client = new DistritoClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author DSILVA
 */
public class EolCedulasClient {

	private Logger logger = Logger.getLogger(EolCedulasClient.class);
	private WebTarget target;
	private Client client;

	public EolCedulasClient() {
		client = ClientBuilder.newClient();
	}

	public EolCedulasConverter getCedulasConverter(String nivel, String periodo, String nombreActividad) {
		logger.info(":: EolCedulasClient.getCedulasConverter :: Starting execution...");
		/*
		 * return
		 * webResource.queryParams(getQParams(optionalQueryParams)).accept(javax.ws.rs.
		 * core.MediaType.APPLICATION_JSON).get(new
		 * GenericType<List<InstitucionEducativa>>() { });
		 */
		target = client.target(Constantes.BASE_URI_EOL_ADMIN).path("get/getCedulas");
		/*
		 * String[] queryParamNames = new String[]{"nivel", "periodo",
		 * "nombreActividad"}; String[] queryParamValues = new String[]{nivel, periodo,
		 * nombreActividad};
		 */
		target = target.queryParam("nivel", nivel);
		target = target.queryParam("periodo", periodo);
		target = target.queryParam("nombreActividad", nombreActividad);

		logger.info(" getUri: " + target.getUri());
		logger.info(":: EolCedulasClient.getCedulasConverter :: Execution finish.");
		return target.request(MediaType.APPLICATION_XML).get(new GenericType<EolCedulasConverter>() {
		});

//        return webResource.queryParams(getQueryOrFormParams(queryParamNames, queryParamValues)).accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<EolCedulasConverter>() {});
	}
	/*
	 * private MultivaluedMap getQueryOrFormParams(String[] paramNames, String[]
	 * paramValues) { MultivaluedMap<String, String> qParams = new
	 * com.sun.jersey.api.representation.Form(); for (int i = 0; i <
	 * paramNames.length; i++) { if (paramValues[i] != null) {
	 * qParams.add(paramNames[i], paramValues[i]); } } return qParams; }
	 * 
	 * public void close() { client.destroy(); }
	 */

}
