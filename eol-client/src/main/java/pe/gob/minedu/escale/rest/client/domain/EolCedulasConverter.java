/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.converter.EolCedulaConverter;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="cedulas")
public class EolCedulasConverter {

    private List<EolCedulaConverter> items;

    @XmlElement(name="items")
    public List<EolCedulaConverter> getItems() {
        return items;
    }

    public void setItems(List<EolCedulaConverter> items) {
        this.items = items;
    }

    }
