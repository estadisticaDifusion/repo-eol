/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "institucion")
public class InstitucionEducativaIE {

    public static final String NIVEL_INICIAL_NO_ESCOLARIZADO = "A5";
    public static final String NIVEL_INICIAL = "A";
    public static final String NIVEL_INICIAL_ARTICULACION = "A4";
    public static final String NIVEL_PRIMARIA = "B0";
    public static final String NIVEL_PRIMARIA_ADULTOS = "C0";
    public static final String NIVEL_BASICA_ALTERNATIVA = "D0";
    public static final String NIVEL_SECUNDARIA = "F0";
    public static final String NIVEL_SECUNDARIA_ADULTOS = "G0";
    public static final String NIVEL_PEDAGOGICA = "K0";
    public static final String NIVEL_TECNOLOGICA = "T0";
    public static final String NIVEL_ARTISTICA = "M0";
    public static final String NIVEL_ESPECIAL = "E0";
    public static final String NIVEL_OCUPACIONAL = "L";
    public static final String SI = "1";
    public static final String ARTICULACION_SI = "1";
    public static final String ISPCONIST_SI = "1";
    private String anexo;
    private String codigoModular;
    private String centroEducativo;
    private Igel igel;
    private NivelModalidad nivelModalidad;
    private String progarti;
    private String codlocal;
    private GestionDependencia gestionDependencia;
    private EstadoPadron estado;
    private String tipoprog;
    private Ugel ugel;
    private String progise;
    private String codinst;

    private String mcenso;

    private String sienvio;
    private String gestion;
    private Gestion gestionie;//imendoza 20170323
    private Distrito distrito;//jbedrillana 20180510

    public String getProgarti() {
        return progarti;
    }

    public void setProgarti(String progarti) {
        this.progarti = progarti;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    @XmlElement(name = "cenEdu")
    public String getCentroEducativo() {
        return centroEducativo;
    }

    public void setCentroEducativo(String centroEducativo) {
        this.centroEducativo = centroEducativo;
    }

    @XmlElement(name = "codMod")
    public String getCodigoModular() {
        return codigoModular;
    }

    public void setCodigoModular(String codigoModular) {
        this.codigoModular = codigoModular;
    }

    @XmlElement(name = "igel")
    public Igel getIgel() {
        return igel;
    }

    public void setIgel(Igel igel) {
        this.igel = igel;
    }

    @XmlElement(name = "nivelModalidad")
    public NivelModalidad getNivelModalidad() {
        return nivelModalidad;
    }

    public void setNivelModalidad(NivelModalidad nivelModalidad) {
        this.nivelModalidad = nivelModalidad;
    }

    @XmlElement(name="codlocal")
    public String getCodlocal() {
        return codlocal;
    }
    public void setCodlocal(String codlocal) {
        this.codlocal = codlocal;
    }
    @XmlElement(name = "gestionDependencia")
    public GestionDependencia getGestionDependencia() {
        return gestionDependencia;
    }
    public void setGestionDependencia(GestionDependencia gestionDependencia) {
        this.gestionDependencia = gestionDependencia;
    }

    @XmlElement(name = "estado")
    public EstadoPadron getEstado() {
        return estado;
    }

    public void setEstado(EstadoPadron estado) {
        this.estado = estado;
    }

    @XmlElement(name = "tipoprog")
    public String getTipoprog() {
        return tipoprog;
    }

    /**
     * @param tipoprog the tipoprog to set
     */
    public void setTipoprog(String tipoprog) {
        this.tipoprog = tipoprog;
    }

    @XmlElement(name = "ugel")
    public Ugel getUgel() {
        return ugel;
    }


    public void setUgel(Ugel ugel) {
        this.ugel = ugel;
    }

    @XmlElement(name = "distrito")
    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    /**
     * @return the progise
     */
    public String getProgise() {
        return progise;
    }

    /**
     * @param progise the progise to set
     */
    public void setProgise(String progise) {
        this.progise = progise;
    }

    @XmlElement(name = "codinst")
    public String getCodinst() {
        return codinst;
    }

    public void setCodinst(String codinst) {
        this.codinst = codinst;
    }

    //imendoza 20170306 inicio
    @XmlElement(name = "mcenso")
    public String getMcenso() {
        return mcenso;
    }

    public void setMcenso(String mcenso) {
        this.mcenso = mcenso;
    }

    @XmlElement(name = "sienvio")
    public String getSienvio() {
        return sienvio;
    }

    public void setSienvio(String sienvio) {
        this.sienvio = sienvio;
    }
  

    //imendoza 20170306 fin
    //imendoza 20170323 inicio
    @XmlElement(name = "gestionie")
    public Gestion getGestionIe() {
        return gestionie;
    }
    public void setGestionIe(Gestion gestionie) {
        this.gestionie = gestionie;
    }
    //imendoza 20170323 inicio
    @XmlElement(name = "gestion")
    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

	@Override
	public String toString() {
		return "InstitucionEducativaIE [anexo=" + anexo + ", codigoModular=" + codigoModular + ", centroEducativo="
				+ centroEducativo + ", igel=" + igel + ", nivelModalidad=" + nivelModalidad + ", progarti=" + progarti
				+ ", codlocal=" + codlocal + ", gestionDependencia=" + gestionDependencia + ", estado=" + estado
				+ ", tipoprog=" + tipoprog + ", ugel=" + ugel + ", progise=" + progise + ", codinst=" + codinst
				+ ", mcenso=" + mcenso + ", sienvio=" + sienvio + ", gestion=" + gestion + ", gestionie=" + gestionie
				+ ", distrito=" + distrito + "]";
	}
    
    
    
}
