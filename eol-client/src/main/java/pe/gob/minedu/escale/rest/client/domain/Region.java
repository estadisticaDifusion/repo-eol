package pe.gob.minedu.escale.rest.client.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "region")
public class Region {

    private String idRegion;
    private String nombreRegion;

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getNombreRegion() {
        return nombreRegion;
    }

    public void setNombreRegion(String nombreRegion) {
        this.nombreRegion = nombreRegion;
    }

    @Override
    public String toString() {
        return nombreRegion;
    }
}
