/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="areas")
public class Areas  extends Codigos{



@XmlElement   
public Collection<Area> getArea(){
    return super.items;
}
    
public void setArea(Collection<Area> items){
super.setItems(items);
}


}
