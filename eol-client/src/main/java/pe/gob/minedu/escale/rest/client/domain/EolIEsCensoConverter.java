/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.converter.EolIECensoConverter;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "censo")
public class EolIEsCensoConverter {

	private List<EolIECensoConverter> institucion;

	@XmlElement(name = "institucion")
	public List<EolIECensoConverter> getInstitucion() {
		return institucion;
	}

	public void setInstitucion(List<EolIECensoConverter> institucion) {
		this.institucion = institucion;
	}

}
