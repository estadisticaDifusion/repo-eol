/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="ie")
public class InstitucionEducativa {

    private String codDreUgel;
    private String nomDreUgel;
    private String departamento;
    private String provincia;
    private String distrito;
    private String nombreIE;
    private String codigoModular;
    private String anexo;
    private String nivelModalidad;
    private String descGestion;
    private String descDependencia;
    private String descEstado;
    private String ubigeo;
    private String direccion;
    private String codigoLocal;
    private String forma;
    private String descForma;
    private String telefono;
    private String correoElectronico;
    private String centroPoblado;
    private String descArea;
    private String descTipoSexo;
    private String descTurno;
    private String descCaracteristica;
    private String director;

    public String getCodDreUgel() {
        return codDreUgel;
    }

    public void setCodDreUgel(String codDreUgel) {
        this.codDreUgel = codDreUgel;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getNombreIE() {
        return nombreIE;
    }

    public void setNombreIE(String nombreIE) {
        this.nombreIE = nombreIE;
    }

    public String getCodigoModular() {
        return codigoModular;
    }

    public void setCodigoModular(String codigoModular) {
        this.codigoModular = codigoModular;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getNivelModalidad() {
        return nivelModalidad;
    }

    public void setNivelModalidad(String nivelModalidad) {
        this.nivelModalidad = nivelModalidad;
    }

    public String getDescGestion() {
        return descGestion;
    }

    public void setDescGestion(String descGestion) {
        this.descGestion = descGestion;
    }

    public String getDescEstado() {
        return descEstado;
    }

    public void setDescEstado(String descEstado) {
        this.descEstado = descEstado;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoLocal() {
        return codigoLocal;
    }

    public void setCodigoLocal(String codigoLocal) {
        this.codigoLocal = codigoLocal;
    }

    public String getNomDreUgel() {
        return nomDreUgel;
    }

    public void setNomDreUgel(String nomDreUgel) {
        this.nomDreUgel = nomDreUgel;
    }

    public String getDescDependencia() {
        return descDependencia;
    }

    public void setDescDependencia(String descDependencia) {
        this.descDependencia = descDependencia;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getCentroPoblado() {
        return centroPoblado;
    }

    public void setCentroPoblado(String centroPoblado) {
        this.centroPoblado = centroPoblado;
    }

    public String getDescArea() {
        return descArea;
    }

    public void setDescArea(String descArea) {
        this.descArea = descArea;
    }

    public String getDescTipoSexo() {
        return descTipoSexo;
    }

    public void setDescTipoSexo(String descTipoSexo) {
        this.descTipoSexo = descTipoSexo;
    }

    public String getDescTurno() {
        return descTurno;
    }

    public void setDescTurno(String descTurno) {
        this.descTurno = descTurno;
    }

    /**
     * @return the descCaracteristica
     */
    public String getDescCaracteristica() {
        return descCaracteristica;
    }

    /**
     * @param descCaracteristica the descCaracteristica to set
     */
    public void setDescCaracteristica(String descCaracteristica) {
        this.descCaracteristica = descCaracteristica;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDescForma() {
        return descForma;
    }

    public void setDescForma(String descForma) {
        this.descForma = descForma;
    }



}
