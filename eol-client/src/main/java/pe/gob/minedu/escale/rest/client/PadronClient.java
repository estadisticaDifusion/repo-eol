/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client;

import java.util.Map;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.rest.constant.Constantes;

/**
 * Jersey REST client generated for REST resource:application
 * [instituciones/{cod_mod}/{anexo}/]<br>
 * USAGE:
 *
 * <pre>
 *        PadronClient client = new PadronClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author DSILVA
 */
public class PadronClient {

    private Logger logger = Logger.getLogger(PadronClient.class);
    
    private Client client;

    public PadronClient() {
        logger.info(":: PadronClient.constructor :: Starting execution...");
        client = ClientBuilder.newClient();
        String resourcePath = "getInstituciones";

        logger.info("  Constantes.BASE_URI=" + Constantes.BASE_URI_PADRON_WS + "; resourcePath: " + resourcePath);

        logger.info(":: PadronClient.constructor :: Execution finish.");
    }

    public PadronClient(String cod_mod, String anexo) {
        client = ClientBuilder.newClient();
        //com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
/*        client = Client.create();
        //config
        String resourcePath = java.text.MessageFormat.format("instituciones/{0}/{1}", new Object[]{cod_mod, anexo});
        webResource = client.resource(Constantes.BASE_URI).path(resourcePath);*/
    }

    public void setResourcePath(String cod_mod, String anexo) {
        /*        String resourcePath = java.text.MessageFormat.format("instituciones/{0}/{1}", new Object[]{cod_mod, anexo});
        webResource = client.resource(Constantes.BASE_URI).path(resourcePath);*/
    }

    /**
     * @param responseType Class representing the response
     * @param optionalQueryParams List of optional query parameters in the form
     * of "param_name=param_value",...<br>
     * List of optional query parameters:
     * <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "3"]
     * @return response object (instance of responseType class)
     */
    public <T> T get_XML(Class<T> responseType, String... optionalQueryParams) throws Exception { // UniformInterfaceException
        return null;
        /*        return webResource.queryParams(getQParams(optionalQueryParams))
                .accept(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);*/
    }

    /**
     * @param responseType Class representing the response
     * @param optionalQueryParams List of optional query parameters in the form
     * of "param_name=param_value",...<br>
     * List of optional query parameters:
     * <LI>expandLevel [OPTIONAL, DEFAULT VALUE: "3"]
     * @return response object (instance of responseType class)
     */
    public <T> T get_JSON(Class<T> responseType, String... optionalQueryParams) {
        String resourcePath = "getInstituciones";
        return client
                .target(Constantes.BASE_URI_PADRON_WS)
                .path(resourcePath)
                .request(MediaType.APPLICATION_JSON)
                .get(responseType);
    }
    
    public <T> T get_JSON(Class<T> responseType, Map<String, String> parameters) {
        String resourcePath = "getInstituciones";
        logger.info("invoke service = "+Constantes.BASE_URI_PADRON_WS+resourcePath);
        WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
        addParameter(target, parameters);
        return target.request(MediaType.APPLICATION_JSON).get(responseType);
    }
    
    public <T> T consumeGetResourceXml(Class<T> responseType, String resourcePath, Map<String, String> parameters) {
        logger.info(":: PadronClient.consumeGetResourceXml :: Starting execution...");

        WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
        for (String key : parameters.keySet()) {
            String value = parameters.get(key);
            target = target.queryParam(key, value);  //It is important to know queryParam method won't update current WebTarget object, but return a new one. 
        }
        logger.info(" consumeGetResourceXml.URL: " + target.getUri().toString());
        logger.info(":: PadronClient.consumeGetResourceXml :: Execution finish.");
        return target.request(MediaType.APPLICATION_XML).get(responseType);
    }
    
    public <T> T consumeGetResourceTextPlain(Class<T> responseType, String resourcePath, Map<String, String> parameters) { // UniformInterfaceException
        logger.info(":: PadronClient.consumeGetResourceXml :: Starting execution...");

        WebTarget target = client.target(Constantes.BASE_URI_PADRON_WS).path(resourcePath);
        for (String key : parameters.keySet()) {
            String value = parameters.get(key);
            target = target.queryParam(key, value);  //It is important to know queryParam method won't update current WebTarget object, but return a new one. 
        }
        logger.info(" URL: " + target.getUri().toString());
        logger.info(":: PadronClient.consumeGetResourceXml :: Execution finish.");
        return target.request(MediaType.TEXT_PLAIN).get(responseType);
    }
    
    private void addParameter(WebTarget target, Map<String, String> parameters) {
        for (String key : parameters.keySet()) {
            String value = parameters.get(key);
            target = target.queryParam(key, value); 
        }
    }
}
