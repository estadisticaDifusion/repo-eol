/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.domain;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "instituciones")
public class Instituciones {

    private Collection<InstitucionEducativaIE> entities;

    @XmlElement(name = "items")
    public Collection<InstitucionEducativaIE> getInstitucion() {
        return entities;
    }

    public void setInstitucion(Collection<InstitucionEducativaIE> items) {
        this.entities = items;
    }
}
