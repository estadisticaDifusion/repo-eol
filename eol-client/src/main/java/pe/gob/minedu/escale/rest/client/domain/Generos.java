/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="generos")
public class Generos  extends Codigos{



@XmlElement   
public Collection<Genero> getGenero(){
    return super.items;
}
    
public void setGenero(Collection<Genero> items){
super.setItems(items);
}


}
