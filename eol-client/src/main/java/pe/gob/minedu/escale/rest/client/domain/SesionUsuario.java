/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.domain;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "sesion")
public class SesionUsuario {

    private String usuario;
    private Long idSesion=-1L;

    public SesionUsuario() {
    
    }

    public String getUsuario() {
        return usuario;
    }


    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    @XmlElement(name = "token")
    public Long getIdSesion() {
        return idSesion;
    }
    /**
     * @param idSesion the idSesion to set
     */
    public void setIdSesion(Long idSesion) {
        this.idSesion = idSesion;
    }

   
}
