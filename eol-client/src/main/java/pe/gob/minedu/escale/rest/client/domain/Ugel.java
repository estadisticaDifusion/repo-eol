/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "ugel")
public class Ugel {

    private String idUgel;
    private String nombreUgel;

    public Ugel() {
    }

    public String getIdUgel() {
        return idUgel;
    }

    public void setIdUgel(String idUgel) {
        this.idUgel = idUgel;
    }

    public String getNombreUgel() {
        return nombreUgel;
    }

    public void setNombreUgel(String nombreUgel) {
        this.nombreUgel = nombreUgel;
    }

    @Override
    public String toString() {
        return nombreUgel;
    }
}
