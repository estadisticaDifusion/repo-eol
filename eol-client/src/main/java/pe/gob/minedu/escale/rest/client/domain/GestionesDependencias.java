/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.rest.client.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "gestionesDependencias")
public class GestionesDependencias extends Codigos<GestionDependencia> {

    //private Collection<GestionDependencia> items;

    public GestionesDependencias() {
    }

    //@XmlElement(name = "gestionDepedencia")
    public Collection<GestionDependencia> getGestionDependencia() {
        return super.getItems();
    }

    public void setGestionDependencia(Collection<GestionDependencia> items){
        super.setItems(items);

    }
}
