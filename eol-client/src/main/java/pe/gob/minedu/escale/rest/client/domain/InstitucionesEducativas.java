/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name="iiee")
public class InstitucionesEducativas {

    private List<InstitucionEducativa> items;

    @XmlElement(name="items")
    public List<InstitucionEducativa> getItems() {
        return items;
    }

    public void setItems(List<InstitucionEducativa> items) {
        this.items = items;
    }

    }
