/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.rest.client.domain;

import java.io.Serializable;
import java.util.List;

import pe.gob.minedu.escale.eol.converter.EolCedulaConverter;


/**
 *
 * @author JMATAMOROS
 */

public class EolPeriodoConverter implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
    private String anio;
    
    private String descripcion;
    
    private List<EolCedulaConverter> actividades;
    
    public EolPeriodoConverter() {
    }

    

    

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<EolCedulaConverter> getActividades() {
        return actividades;
    }

    public void setActividades(List<EolCedulaConverter> actividades) {
        this.actividades = actividades;
    }

    
}
