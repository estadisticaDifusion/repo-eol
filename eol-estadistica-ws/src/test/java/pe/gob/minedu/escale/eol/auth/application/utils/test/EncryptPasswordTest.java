package pe.gob.minedu.escale.eol.auth.application.utils.test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class EncryptPasswordTest {

	private Logger logger = Logger.getLogger(EncryptPasswordTest.class);

	@Test
	public void testEncryptPassword() {
		SecureRandom random = null;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String pass = "123456";
		PasswordEncoder encoder = new BCryptPasswordEncoder(16, random);

		for (int i = 0; i < 3; i++) {
			logger.info("Hash {" + i + "} --> " + encoder.encode(pass));
		}

		pass = "1234567";
		for (int i = 0; i < 4; i++) {
			logger.info("New Hash {" + i + "} --> " + encoder.encode(pass));
		}

		String plainClientCredentials = "eol-padron-ws:987654321";
		logger.info("Credentials 1: " + new String(Base64.encodeBase64(plainClientCredentials.getBytes())));
	}

}
