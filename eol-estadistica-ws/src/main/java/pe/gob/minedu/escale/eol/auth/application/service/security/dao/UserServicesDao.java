package pe.gob.minedu.escale.eol.auth.application.service.security.dao;

import java.util.List;

import pe.gob.minedu.escale.eol.auth.application.model.Role;
import pe.gob.minedu.escale.eol.auth.application.model.User;

public interface UserServicesDao {

	public void createUser(User u, List<Role> role);

	public boolean isUserAvailable(String username);

	public User getUserByLogin(String login);

	public User actualizar(User u);

	public void eliminar(User u);

}