package pe.gob.minedu.escale.eol.auth.application.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.minedu.escale.eol.admin.ejb.CedulaLocal;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.estadistica.EolActividadDTO;
import pe.gob.minedu.escale.eol.dto.estadistica.EolCedulaDTO;
import pe.gob.minedu.escale.eol.dto.estadistica.EolEnvioActividadDTO;
import pe.gob.minedu.escale.eol.dto.estadistica.EolEnvioCedulaDTO;
import pe.gob.minedu.escale.eol.dto.estadistica.EolEnvioIECensoDTO;
import pe.gob.minedu.escale.eol.dto.estadistica.EolEnvioPeriodoDTO;
import pe.gob.minedu.escale.eol.enums.admin.EstadoActividadEnum;
import pe.gob.minedu.escale.eol.payload.estadistica.CedulaCensalResponse;
import pe.gob.minedu.escale.eol.payload.estadistica.CedulaCensalResponsePayload;
import pe.gob.minedu.escale.eol.payload.estadistica.EstadisticaRequest;
import pe.gob.minedu.escale.eol.portlets.ejb.CensoLocal;
import pe.gob.minedu.escale.eol.utils.Funciones;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestEstadisticaServices {

	private Logger logger = Logger.getLogger(RestEstadisticaServices.class);

	@EJB(mappedName = "java:global/eol-estadistica-ws/CensoFacade!pe.gob.minedu.escale.eol.portlets.ejb.CensoLocal")
	private CensoLocal censoFacade;

	@EJB(mappedName = "java:global/eol-estadistica-ws/CedulaFacade!pe.gob.minedu.escale.eol.admin.ejb.CedulaLocal")
	private CedulaLocal cedulaFacade;

	@RequestMapping(value = "/post/cedulaCensalList", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CedulaCensalResponse> getListCedulaCensalDTO(@RequestBody EstadisticaRequest request) {
		logger.info(":: RestEstadisticaServices.getListCedulaCensalDTO :: Starting execution...");
		validateRequest(request);

		String nivel = request.getNivel();
		String anexo = request.getAnexo();
		String codmod = request.getCodmod();
		String codlocal = request.getCodlocal();
		String codinst = request.getCodinst();
		String tipo = request.getTipo();
		Integer anioInicio = request.getAnioInicio();
		Integer anioFinal = request.getAnioFinal();

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "cedulaCensalList";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		CedulaCensalResponse response = new CedulaCensalResponse();
		CedulaCensalResponsePayload responsePayload = new CedulaCensalResponsePayload();

		response.setId(id);
		response.setDateTime(dateTime);
		response.setResourceName(resourceName);
		response.setHttpMethod(httpMethod);

		List<EolEnvioPeriodoDTO> envioPeriodList = searchPeriodCedula(nivel, anexo, codmod, codlocal, codinst, tipo,
				anioInicio, anioFinal);

		if (envioPeriodList != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setDataResult(CoreConstant.DATA_FOUND);
			responsePayload.setDataDetail(envioPeriodList);
		} else {
			responsePayload.setDataResult(CoreConstant.DATA_NOT_FOUND);
		}
		response.setStatus(status);
		response.setCode(code);
		response.setMessage(message);
		response.setResponsePayload(responsePayload);

		logger.info(":: RestEstadisticaServices.getListCedulaCensalDTO :: Execution finish.");
		return new ResponseEntity<CedulaCensalResponse>(response, HttpStatus.OK);
	}

	private void validateRequest(EstadisticaRequest request) {
		if (request == null) {
			throw new RuntimeException("Request es incorrecto");
		}
		List<String> reqParams = new ArrayList<String>();

		reqParams.add(request.getNivel());
		reqParams.add(request.getAnexo());
		reqParams.add(request.getCodmod());
		reqParams.add(request.getCodlocal());
		reqParams.add(request.getCodinst());

		boolean existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);
		if (existChar) {
			throw new RuntimeException("Request contiene codigo SQL ");
		}

	}

	private List<EolEnvioPeriodoDTO> searchPeriodCedula(String nivel, String anexo, String codmod, String codlocal,
			String codinst, String tipo, Integer anioInicio, Integer anioFinal) {
		logger.info(":: RestEstadisticaServices.searchPeriodCedula :: Starting execution...");
		List<EolEnvioPeriodoDTO> periodos = new ArrayList<EolEnvioPeriodoDTO>();

		List<EolEnvioIECensoDTO> envios = censoFacade.censoTotalListByCodigo(nivel, codmod, anexo, codlocal, codinst);

		if (envios == null) {
			envios = new ArrayList<EolEnvioIECensoDTO>();
		}

		if (envios != null) {
			Collections.sort(envios, buscarEnvio);
		}

		EolEnvioIECensoDTO act = new EolEnvioIECensoDTO();
		for (int i = anioFinal; i >= anioInicio; i--) {
			EolEnvioPeriodoDTO periodo = new EolEnvioPeriodoDTO();
			String anio = new Integer(i).toString();

			List<EolEnvioCedulaDTO> cedulas = setListEnvioCedula(nivel, anio);

			for (EolEnvioCedulaDTO ced : cedulas) {
				act.setIdActividad(ced.getActividad().getId());
				ced.getActividad()
						.setStrFechaInicio(Funciones.fechaAcadena(ced.getActividad().getFechaInicio(), "MMM yyyy"));
				ced.getActividad()
						.setStrFechaTermino(Funciones.fechaAcadena(ced.getActividad().getFechaTermino(), "MMM yyyy"));
				ced.getActividad().setStrFechaTerminosigied(
						Funciones.fechaAcadena(ced.getActividad().getFechaTerminosigied(), "MMM yyyy"));
				ced.getActividad().setStrFechaTerminohuelga(
						Funciones.fechaAcadena(ced.getActividad().getFechaTerminohuelga(), "MMM yyyy"));

				ced.getActividad()
						.setUrlFormato(ced.getActividad().getUrlFormato().concat("?codmod=").concat(codmod)
								.concat("&anexo=").concat(anexo).concat("&nivel=").concat(nivel).concat("&codlocal=")
								.concat(codlocal).concat("&codinst=").concat(codinst).concat("&nomactiv=")
								.concat(ced.getActividad().getNombre()));

				int pos = Collections.binarySearch(envios, act, buscarEnvio);

				if (pos >= 0) {
					EolEnvioIECensoDTO envio = envios.get(pos);
					ced.setEnvio(true);
					ced.setFechaEnvio(envio.getFechaEnvio());
					ced.setStrFechaEnvio(Funciones.fechaAcadena(envio.getFechaEnvio(), "dd/MM/yyyy HH:mm"));
					ced.setNroEnvio(envio.getNroEnvio());
					ced.getActividad().setUrlConstancia(ced.getActividad().getUrlConstancia().concat("?nivel=")
							.concat(nivel).concat("&idEnvio=").concat(envio.getNroEnvio().toString()));

				}

			}
			periodo.setAnio(new Integer(i).toString());
			periodo.setActividades(cedulas);
			periodos.add(periodo);
		}
		logger.info(":: RestEstadisticaServices.searchPeriodCedula :: Execution finish.");
		return periodos;
	}

	private List<EolEnvioCedulaDTO> setListEnvioCedula(String nivel, String anio) {
		List<EolEnvioCedulaDTO> envioCedulaDtoList = new ArrayList<EolEnvioCedulaDTO>();

		List<EolCedulaDTO> cedulas = cedulaFacade.findCedulaList(null, null, nivel, null, anio);
		if (cedulas != null) {
			EolEnvioCedulaDTO eolEnvioCedulaDTO = null;
			for (EolCedulaDTO cedula : cedulas) {
				eolEnvioCedulaDTO = new EolEnvioCedulaDTO();
				eolEnvioCedulaDTO.setId(cedula.getId());
				eolEnvioCedulaDTO.setNombre(cedula.getNombre());
				eolEnvioCedulaDTO.setNivel(cedula.getNivel());
				eolEnvioCedulaDTO.setEstado(cedula.getEstado());
				eolEnvioCedulaDTO.setVersion(cedula.getVersion());
				eolEnvioCedulaDTO.setActividad(setEnvioActividad(cedula.getActividad()));
				envioCedulaDtoList.add(eolEnvioCedulaDTO);
			}
		}

		return envioCedulaDtoList;

	}

	private EolEnvioActividadDTO setEnvioActividad(EolActividadDTO eolActividadDTO) {
		EolEnvioActividadDTO eolEnvioActividadDTO = new EolEnvioActividadDTO();
		if (eolActividadDTO != null) {
			eolEnvioActividadDTO.setId(eolActividadDTO.getId());
			eolEnvioActividadDTO.setNombre(eolActividadDTO.getNombre());
			eolEnvioActividadDTO.setDescripcion(eolActividadDTO.getDescripcion());
			eolEnvioActividadDTO.setFechaInicio(eolActividadDTO.getFechaInicio());
			eolEnvioActividadDTO.setFechaTermino(eolActividadDTO.getFechaTermino());
			eolEnvioActividadDTO.setFechaTerminosigied(eolActividadDTO.getFechaTerminosigied());
			eolEnvioActividadDTO.setFechaTerminohuelga(eolActividadDTO.getFechaTerminohuelga());
			eolEnvioActividadDTO.setFechaLimite(eolActividadDTO.getFechaLimite());
			eolEnvioActividadDTO.setFechaLimitesigied(eolActividadDTO.getFechaLimitesigied());
			eolEnvioActividadDTO.setFechaLimitehuelga(eolActividadDTO.getFechaLimitehuelga());
			eolEnvioActividadDTO.setEstadoFormato(eolActividadDTO.getEstadoFormato());
			eolEnvioActividadDTO.setEstadoConstancia(eolActividadDTO.getEstadoConstancia());
			eolEnvioActividadDTO.setEstadoCobertura(eolActividadDTO.getEstadoCobertura());
			eolEnvioActividadDTO.setEstadoSituacion(eolActividadDTO.getEstadoSituacion());
			eolEnvioActividadDTO.setEstadoOmisos(eolActividadDTO.getEstadoOmisos());
			eolEnvioActividadDTO.setEstadoSie(eolActividadDTO.getEstadoSie());
			eolEnvioActividadDTO.setUrlFormato(eolActividadDTO.getUrlFormato());
			eolEnvioActividadDTO.setUrlOmisos(eolActividadDTO.getUrlOmisos());
			eolEnvioActividadDTO.setUrlCobertura(eolActividadDTO.getUrlCobertura());
			eolEnvioActividadDTO.setUrlConstancia(eolActividadDTO.getUrlConstancia());
			eolEnvioActividadDTO.setUrlSituacion(eolActividadDTO.getUrlSituacion());

			Date ahora = new Date();
			if (ahora.before(eolActividadDTO.getFechaInicio())) {
				eolEnvioActividadDTO.setEstadoActividad(EstadoActividadEnum.SIN_INICIAR.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaLimite())) {
				eolEnvioActividadDTO.setEstadoActividad(EstadoActividadEnum.FINALIZADO.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaTermino()) && (ahora.before(eolActividadDTO.getFechaLimite())
					|| ahora.equals(eolActividadDTO.getFechaLimite()))) {
				eolEnvioActividadDTO.setEstadoActividad(EstadoActividadEnum.EXTENPORANEO.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaInicio()) && ahora.before(eolActividadDTO.getFechaTermino())) {
				eolEnvioActividadDTO.setEstadoActividad(EstadoActividadEnum.INICIADO.getCod());
			}

			if (ahora.before(eolActividadDTO.getFechaInicio())) {
				eolEnvioActividadDTO.setEstadoActividadsigied(EstadoActividadEnum.SIN_INICIAR.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaLimitesigied())) {
				eolEnvioActividadDTO.setEstadoActividadsigied(EstadoActividadEnum.FINALIZADO.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaTerminosigied())
					&& (ahora.before(eolActividadDTO.getFechaLimitesigied())
							|| ahora.equals(eolActividadDTO.getFechaLimitesigied()))) {
				eolEnvioActividadDTO.setEstadoActividadsigied(EstadoActividadEnum.EXTENPORANEO.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaInicio())
					&& ahora.before(eolActividadDTO.getFechaTerminosigied())) {
				eolEnvioActividadDTO.setEstadoActividadsigied(EstadoActividadEnum.INICIADO.getCod());
			}

			if (ahora.before(eolActividadDTO.getFechaInicio())) {
				eolEnvioActividadDTO.setEstadoActividadhuelga(EstadoActividadEnum.SIN_INICIAR.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaLimitehuelga())) {
				eolEnvioActividadDTO.setEstadoActividadhuelga(EstadoActividadEnum.FINALIZADO.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaTerminohuelga())
					&& (ahora.before(eolActividadDTO.getFechaLimitehuelga())
							|| ahora.equals(eolActividadDTO.getFechaLimitehuelga()))) {
				eolEnvioActividadDTO.setEstadoActividadhuelga(EstadoActividadEnum.EXTENPORANEO.getCod());
			}
			if (ahora.after(eolActividadDTO.getFechaInicio())
					&& ahora.before(eolActividadDTO.getFechaTerminohuelga())) {
				eolEnvioActividadDTO.setEstadoActividadhuelga(EstadoActividadEnum.INICIADO.getCod());
			}

			eolEnvioActividadDTO.setNombrePeriodo(
					(eolActividadDTO.getPeriodo() != null ? eolActividadDTO.getPeriodo().getDescripcion() : ""));
		}
		return eolEnvioActividadDTO;
	}

	static Comparator<EolEnvioIECensoDTO> buscarEnvio = new Comparator<EolEnvioIECensoDTO>() {

		public int compare(EolEnvioIECensoDTO o1, EolEnvioIECensoDTO o2) {

			return (o1.getIdActividad()).compareTo(o2.getIdActividad());
		}
	};

}
