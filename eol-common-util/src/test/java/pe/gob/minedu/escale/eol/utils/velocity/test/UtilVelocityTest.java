package pe.gob.minedu.escale.eol.utils.velocity.test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.junit.Test;

import junit.framework.Assert;
import pe.gob.minedu.escale.common.velocity.VelocityUtil;

public class UtilVelocityTest {

	private Logger logger = Logger.getLogger(UtilVelocityTest.class);

	@Test
	public void velocityTransformTest01() {

		logger.info("Start velocityTransformTest01");

		String path = "test-file/";
		String filename = "apiResponseTemplate.json";
		String responseJsonTemplate = getFileAsString(path, filename);

		Map<String, Object> dataMap = getDataMap();

		String responseJson = VelocityUtil.velocityTransform(dataMap, responseJsonTemplate);

		Assert.assertNotNull(responseJson);

		logger.info("responseJson = " + responseJson);

		logger.info("End velocityTransformTest01");
	}

	private Map<String, Object> getDataMap() {

		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("endpointUrl", "DummyUrl");
		dataMap.put("subscription-key", "DummyEndpointKey");
		dataMap.put("endpointRegion", "westus");

		return dataMap;
	}

	private String getFileAsString(String path, String filename) {
		InputStream is = getClass().getClassLoader().getResourceAsStream(path + filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		return reader.lines().collect(Collectors.joining("\n"));
	}

}
