package pe.gob.minedu.escale.eol.utils.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.utils.Funciones;

public class UtilsTest {

    private Logger logger = Logger.getLogger(UtilsTest.class);

    @Test
    public void stringTest() {
        logger.info(":: UtilsTest.stringTest :: Starting execution..");
        String name = "InstitucionFacade";
        logger.info("name="+name+"; "+name.replace("Local", "Facade"));
        logger.info(":: UtilsTest.stringTest :: Execution finish.");
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
    public void convertHashMapTest() {
        logger.info(":: UtilsTest.convertHashMapTest :: Starting execution..");
        List<InstitucionDTO> institucionList = new ArrayList<InstitucionDTO>();
        InstitucionDTO ie = null;
        for (int x = 0; x<10; x++) {
        	ie = new InstitucionDTO();
        	ie.setCodMod("029338"+x);
        	ie.setCodlocal(x+"52413");
        	ie.setCenEdu("Julio Cesar Escobar");
        	institucionList.add(ie);
        }
        ObjectMapper oMapper = new ObjectMapper();
        List<Map> listMap = new ArrayList();
        
        Map<String, Object> map = null;
        for (InstitucionDTO centro : institucionList) {
        	map = oMapper.convertValue(centro, Map.class);
        	//modelMap.put("data", map);
        	listMap.add(map);
        }
        logger.info("listMap = " + listMap);
        
        logger.info(":: UtilsTest.convertHashMapTest :: Execution finish.");
    }
    
	@SuppressWarnings("rawtypes")
	@Test
    public void convertListToMapTest() {
        logger.info(":: UtilsTest.convertListToMapTest :: Starting execution..");
        List<InstitucionDTO> institucionList = new ArrayList<InstitucionDTO>();
        InstitucionDTO ie = null;
        for (int x = 0; x<10; x++) {
        	ie = new InstitucionDTO();
        	ie.setCodMod("029338"+x);
        	ie.setCodlocal(x+"52413");
        	ie.setCenEdu("Julio Cesar Escobar");
        	institucionList.add(ie);
        }
        List<Map> listMap = Funciones.convertListToMap(institucionList);
        logger.info("listMap = " + listMap);
        
        logger.info(":: UtilsTest.convertListToMapTest :: Execution finish.");
	}
}
