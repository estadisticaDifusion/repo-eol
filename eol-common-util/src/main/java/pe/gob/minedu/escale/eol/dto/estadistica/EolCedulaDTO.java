package pe.gob.minedu.escale.eol.dto.estadistica;

import java.io.Serializable;

public class EolCedulaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6216218123928721548L;
	
	private Long id;
	private String nombre;
	private String arcXls;
	private String arcPdf;
	private String sqlBase;
	private String nivel;
	private Boolean estado;
	private String version;
	private EolActividadDTO actividad;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getArcXls() {
		return arcXls;
	}

	public void setArcXls(String arcXls) {
		this.arcXls = arcXls;
	}

	public String getArcPdf() {
		return arcPdf;
	}

	public void setArcPdf(String arcPdf) {
		this.arcPdf = arcPdf;
	}

	public String getSqlBase() {
		return sqlBase;
	}

	public void setSqlBase(String sqlBase) {
		this.sqlBase = sqlBase;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public EolActividadDTO getActividad() {
		return actividad;
	}

	public void setActividad(EolActividadDTO actividad) {
		this.actividad = actividad;
	}

}
