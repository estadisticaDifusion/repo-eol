package pe.gob.minedu.escale.eol.dto.estadistica;

import java.util.Date;

public class EolEnvioIECensoDTO {
	private Integer idActividad;
	private Integer nroEnvio;
	private Date fechaEnvio;
	private String codooii;
	private String situacion;
	private String codMod;
	private String anexo;
	private String nivMod;
	private String codLocal;
	private String cenEdu;
	private String nivelModalidad;
	private String codId;

	public Integer getIdActividad() {
		return idActividad;
	}

	public void setIdActividad(Integer idActividad) {
		this.idActividad = idActividad;
	}

	public Integer getNroEnvio() {
		return nroEnvio;
	}

	public void setNroEnvio(Integer nroEnvio) {
		this.nroEnvio = nroEnvio;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getCodooii() {
		return codooii;
	}

	public void setCodooii(String codooii) {
		this.codooii = codooii;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getNivMod() {
		return nivMod;
	}

	public void setNivMod(String nivMod) {
		this.nivMod = nivMod;
	}

	public String getCodLocal() {
		return codLocal;
	}

	public void setCodLocal(String codLocal) {
		this.codLocal = codLocal;
	}

	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	public String getNivelModalidad() {
		return nivelModalidad;
	}

	public void setNivelModalidad(String nivelModalidad) {
		this.nivelModalidad = nivelModalidad;
	}

	public String getCodId() {
		return codId;
	}

	public void setCodId(String codId) {
		this.codId = codId;
	}

}
