/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.estadistica;

/**
 *
 * @author Owner
 */
public enum TipGradoEstudio3BEnum {
   
    PRIMERO("1","1","PRIMERO"),
    SEGUNDO("2","2","SEGUNDO"),
    TERCERO("3","3","TERCERO"),
    CUARTO("4","4","CUARTO"),
    QUINTO("5","5","QUINTO"),
    SEXTO("6","6","SEXTO");

    private String cod;
    private String codTipo;
    private String descri;
    
    TipGradoEstudio3BEnum(String cod,String codTipo,String descri)
    {    this.cod=cod;
         this.codTipo=codTipo;
         this.descri=descri;
    }

    /**
     * @return the cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @return the codTipo
     */
    public String getCodTipo() {
        return codTipo;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

}
