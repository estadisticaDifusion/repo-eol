package pe.gob.minedu.escale.eol.dto.entities;

public class IgelDTO {
	private String idIgel;
	private String nombreUgel;
	private DireccionRegionalDTO direccionRegional;

	public IgelDTO() {

	}

	public String getIdIgel() {
		return idIgel;
	}

	public void setIdIgel(String idIgel) {
		this.idIgel = idIgel;
	}

	public String getNombreUgel() {
		return nombreUgel;
	}

	public void setNombreUgel(String nombreUgel) {
		this.nombreUgel = nombreUgel;
	}

	public DireccionRegionalDTO getDireccionRegional() {
		return direccionRegional;
	}

	public void setDireccionRegional(DireccionRegionalDTO direccionRegional) {
		this.direccionRegional = direccionRegional;
	}

}
