package pe.gob.minedu.escale.eol.dto.entities;

public class UgelDTO {
	private String idUgel;
	private String nombreUgel;
	private Double pointX;
	private Double pointY;
	private Integer zoom;
	private DireccionRegionalDTO direccionRegional;

	public UgelDTO() {

	}

	public String getIdUgel() {
		return idUgel;
	}

	public void setIdUgel(String idUgel) {
		this.idUgel = idUgel;
	}

	public String getNombreUgel() {
		return nombreUgel;
	}

	public void setNombreUgel(String nombreUgel) {
		this.nombreUgel = nombreUgel;
	}

	public Double getPointX() {
		return pointX;
	}

	public void setPointX(Double pointX) {
		this.pointX = pointX;
	}

	public Double getPointY() {
		return pointY;
	}

	public void setPointY(Double pointY) {
		this.pointY = pointY;
	}

	public Integer getZoom() {
		return zoom;
	}

	public void setZoom(Integer zoom) {
		this.zoom = zoom;
	}

	public DireccionRegionalDTO getDireccionRegional() {
		return direccionRegional;
	}

	public void setDireccionRegional(DireccionRegionalDTO direccionRegional) {
		this.direccionRegional = direccionRegional;
	}

}
