package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class InstitucionResponse extends ResponseGeneric {

	private InstitucionResponsePayload responsePayload;

	public InstitucionResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(InstitucionResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}

}
