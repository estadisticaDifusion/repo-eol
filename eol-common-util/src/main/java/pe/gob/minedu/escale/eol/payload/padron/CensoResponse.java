package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class CensoResponse extends ResponseGeneric {

	private CensoResponsePayload cresponsePayload;

	public CensoResponsePayload getCresponsePayload() {
		return cresponsePayload;
	}

	public void setCresponsePayload(CensoResponsePayload cresponsePayload) {
		this.cresponsePayload = cresponsePayload;
	}
	
}
