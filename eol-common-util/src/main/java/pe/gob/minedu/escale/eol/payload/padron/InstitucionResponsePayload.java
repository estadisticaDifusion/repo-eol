package pe.gob.minedu.escale.eol.payload.padron;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;

public class InstitucionResponsePayload {
	private String dataResult;
	private List<InstitucionDTO> dataDetail;

	private String cipher;
	
	public String getDataResult() {
		return dataResult;
	}

	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}

	public List<InstitucionDTO> getDataDetail() {
		return dataDetail;
	}

	public void setDataDetail(List<InstitucionDTO> dataDetail) {
		this.dataDetail = dataDetail;
	}
	
	@Override
	public String toString() {
		return "InstitucionResponsePayload [dataResult=" + dataResult + ", dataDetail=" + dataDetail + "]";
	}

	public String getCipher() {
		return cipher;
	}

	public void setCipher(String cipher) {
		this.cipher = cipher;
	}


}
