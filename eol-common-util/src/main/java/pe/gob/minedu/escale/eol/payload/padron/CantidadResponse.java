package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class CantidadResponse extends ResponseGeneric {

	private CantidadResponsePayload responsePayload;

	public CantidadResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(CantidadResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}

}
