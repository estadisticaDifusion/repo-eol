package pe.gob.minedu.escale.eol.utils;

import java.util.List;


public class VerifSQLInject {

    static final String[] listaQ = {"--", ";", "*", "/", "@@", "@","char", "nchar", "varchar", "nvarchar",
                          "alter", "begin", "cast", "create", "cursor","declare", "delete", "drop", "end", "exec",
                          "execute", "fetch", "insert", "kill", "open","select", "sys", "sysobjects", "syscolumns",
                          "table", "update",".","(",")","'","<",">",",","="," ","|",":","%","\""};


    public static boolean verificacionSqlInjectList(List<String> reqParams){
        boolean verifChar = false;

        for (String param : reqParams) {
            verifChar = verificacionSqlparameter(param!=null?param:"");
            if(verifChar)
                break;
        }

        return verifChar;
    }

    private static boolean verificacionSqlparameter(String texteval) {

       boolean evalattr = false;

       for (String queryIn : listaQ) {
           if(texteval.indexOf(queryIn)>-1){
               evalattr = true;
               if(evalattr)
                   break;
           }
       }

       return evalattr;
    }


}
