package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class DreResponse extends ResponseGeneric{
	
	private DreResponsePayload responsePayload;

	public DreResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(DreResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}

}
