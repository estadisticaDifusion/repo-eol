package pe.gob.minedu.escale.eol.payload.padron;


public class CensoResponsePayload {

	private String dataResult;
	private Object dataDetail;
	
	private String Cipher;
	
	public String getDataResult() {
		return dataResult;
	}
	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}
	public Object getDataDetail() {
		return dataDetail;
	}
	public void setDataDetail(Object dataDetail) {
		this.dataDetail = dataDetail;
	}
	public String getCipher() {
		return Cipher;
	}
	public void setCipher(String cipher) {
		Cipher = cipher;
	}
	
	
	
}
