/*
 * 
 */
package pe.gob.minedu.escale.commons.util.velocity.plugin.tool;

import java.math.BigDecimal;

import org.apache.velocity.tools.config.DefaultKey;

/**
 * The Class MathOperationsTool.
 * 
 * @author axfin
 * @version 2.0.1.0
 */
@DefaultKey("mathOperation")
public final class MathOperationsTool {

	/**
	 * Sum numbers.
	 * 
	 * @param parameter1
	 *            the parameter1
	 * @param parameter2
	 *            the parameter2
	 * @return a encoded string
	 */
	public static String sumNumbers(String parameter1, String parameter2) {
		BigDecimal resultSum;
		resultSum = new BigDecimal(parameter1).add(new BigDecimal(parameter2));
		return resultSum.toString();
	}

	/**
	 * Div numbers.
	 * 
	 * @param parameter1
	 *            the parameter1
	 * @param parameter2
	 *            the parameter2
	 * @return the string
	 */
	public static String divNumbers(String parameter1, String parameter2) {

		BigDecimal num1 = new BigDecimal(parameter1);
		BigDecimal num2 = new BigDecimal(parameter2);
		float result = num1.floatValue() / num2.floatValue();

		return String.valueOf(result);
	}

	/**
	 * Subtract numbers.
	 * 
	 * @param parameter1
	 *            the parameter1
	 * @param parameter2
	 *            the parameter2
	 * @return the string
	 */
	public static String subtractNumbers(String parameter1, String parameter2) {
		BigDecimal resultSum;
		resultSum = new BigDecimal(parameter1).subtract(new BigDecimal(
				parameter2));
		return resultSum.toString();
	}

	/**
	 * Multiply numbers.
	 * 
	 * @param parameter1
	 *            the parameter1
	 * @param parameter2
	 *            the parameter2
	 * @return the string
	 */
	public static String multiplyNumbers(String parameter1, String parameter2) {
		BigDecimal resultSum;
		resultSum = new BigDecimal(parameter1).multiply(new BigDecimal(
				parameter2));
		return resultSum.toString();
	}

	/**
	 * Instantiates a new math operations tool.
	 */
	public MathOperationsTool() {

	}

}
