package pe.gob.minedu.escale.eol.utils;



import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;


//import org.apache.commons.codec.DecoderException;
//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.codec.binary.Hex;

//TODO: Implement 256-bit version like: http://securejava.wordpress.com/2012/10/25/aes-256/
public class AesUtil {
	private Logger logger = Logger.getLogger(AesUtil.class);
	
	private final int keySize;
	private final int iterationCount;
	private final Cipher cipher;

	public AesUtil(int keySize, int iterationCount) {
		this.keySize = keySize;
		this.iterationCount = iterationCount;
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (NoSuchAlgorithmException e) {
			throw fail(e);
		} catch (NoSuchPaddingException e){
			throw fail(e);
		}
	}

	public String encrypt(String salt, String iv, String passphrase, String plaintext) {
		try {
			logger.info("AEASUTIL :" + salt +"///" + iv +"///"+ passphrase + "///" + plaintext );
			SecretKey key = generateKey(salt, passphrase);
			logger.info("KEY GENERADO : " + key );
			byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, iv, plaintext.getBytes("UTF-8"));
			logger.info("ENCRIPTADO AES : " + encrypted );
			return base64(encrypted);
		} catch (UnsupportedEncodingException e) {
			logger.info("ERROR EN AES : "  +  e);
			throw fail(e);
		}
	}

	public String decrypt(String salt, String iv, String passphrase, String ciphertext) {
		try {
			SecretKey key = generateKey(salt, passphrase);
			byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv, base64(ciphertext));
			return new String(decrypted, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw fail(e);
		}
	}

	private byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
		try {
			cipher.init(encryptMode, key, new IvParameterSpec(hex(iv)));
			return cipher.doFinal(bytes);
		} catch (InvalidKeyException e) {
			throw fail(e);
		}catch (InvalidAlgorithmParameterException e) {
			throw fail(e);
		}catch (IllegalBlockSizeException e) {
			throw fail(e);
		}catch (BadPaddingException e) {
			throw fail(e);
		}
	}

	private SecretKey generateKey(String salt, String passphrase) {
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), hex(salt), iterationCount, keySize);
			SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
			return key;
		} catch (InvalidKeySpecException e) {
			throw fail(e);
		}catch (NoSuchAlgorithmException e) {
			throw fail(e);
		}
	}

	public static String random(int length) {
		byte[] salt = new byte[length];
		new SecureRandom().nextBytes(salt);
		return hex(salt);
	}

	public static String base64(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
		//		return Base64.encodeBase64String(bytes);
	}

	public static byte[] base64(String str) {
		byte[] encryptedBytes2 = Base64.getDecoder().decode(str);
		return encryptedBytes2;
//		return Base64.decodeBase64(str);
	}

	public static String hex(byte[] bytes) {
		//return Hex.encodeHexString(bytes);
		return encodeUsingDataTypeConverter(bytes);
	}
	
	public static String encodeUsingDataTypeConverter(byte[] bytes) {
	    return DatatypeConverter.printHexBinary(bytes);
	}
	 
	public static byte[] hex(String str) {
		try {
			//return Hex.decodeHex(str.toCharArray());
			return decodeUsingDataTypeConverter(str);
		} catch (IllegalStateException e) {
			throw new IllegalStateException(e);
		}
	}
	
	public  static byte[] decodeUsingDataTypeConverter(String hexString) {
	    return DatatypeConverter.parseHexBinary(hexString);
	}

	private IllegalStateException fail(Exception e) {
		return new IllegalStateException(e);
	}
	
	public String cifrarBase64(String a){
        Base64.Encoder encoder = Base64.getEncoder();
        String b = encoder.encodeToString(a.getBytes(StandardCharsets.UTF_8) );        
        return b;
    }
 
    public String descifrarBase64(String a){
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedByteArray = decoder.decode(a);
 
        String b = new String(decodedByteArray);        
        return b;
    }
}