package pe.gob.minedu.escale.eol.dto.entities;

public class DistritoDTO {
	private String idDistrito;
	private String nombreDistrito;
	private ProvinciaDTO provincia;
	private Double pointX;
	private Double pointY;
	private Integer zoom;
	private String dptoProvDist;

	public DistritoDTO() {

	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getNombreDistrito() {
		return nombreDistrito;
	}

	public void setNombreDistrito(String nombreDistrito) {
		this.nombreDistrito = nombreDistrito;
	}

	public ProvinciaDTO getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaDTO provincia) {
		this.provincia = provincia;
	}

	public Double getPointX() {
		return pointX;
	}

	public void setPointX(Double pointX) {
		this.pointX = pointX;
	}

	public Double getPointY() {
		return pointY;
	}

	public void setPointY(Double pointY) {
		this.pointY = pointY;
	}

	public Integer getZoom() {
		return zoom;
	}

	public void setZoom(Integer zoom) {
		this.zoom = zoom;
	}

	public String getDptoProvDist() {
		dptoProvDist = "";
		if (this.getProvincia() != null && this.getProvincia().getRegion() != null) {
			dptoProvDist = this.getProvincia().getRegion().getNombreRegion() + " / "
					+ this.getProvincia().getNombreProvincia() + " / " + nombreDistrito;
		}
		return dptoProvDist;
	}

	public void setDptoProvDist(String dptoProvDist) {
		this.dptoProvDist = dptoProvDist;
	}

}
