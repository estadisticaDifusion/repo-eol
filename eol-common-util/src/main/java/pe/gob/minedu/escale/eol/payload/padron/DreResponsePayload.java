package pe.gob.minedu.escale.eol.payload.padron;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.entities.DireccionRegionalDTO;

public class DreResponsePayload {
	
	private String dataResult;
	private List<DireccionRegionalDTO> dataDetail;
	
	private String cipher;
	
	public String getDataResult() {
		return dataResult;
	}
	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}
	public List<DireccionRegionalDTO> getDataDetail() {
		return dataDetail;
	}
	public void setDataDetail(List<DireccionRegionalDTO> dataDetail) {
		this.dataDetail = dataDetail;
	}
	public String getCipher() {
		return cipher;
	}
	public void setCipher(String cipher) {
		this.cipher = cipher;
	}
	
	

}
