package pe.gob.minedu.escale.common.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.XML;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import pe.gob.minedu.escale.common.validator.ValidatorUtil;

public class JsonUtil {

	private static final String BLANCK_CHARACTER = "";
	private static final String UNICODE_CHARACTER_EXPRESSION = "\\r|\\t|\\n";
	private static final String UNCHECKED = "unchecked";
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	private JsonUtil() {
		throw new UnsupportedOperationException();
	}

	public static String xmlToJson(String xml) {
		String json = XML.toJSONObject(xml).toString();
		return json;
	}

	@SuppressWarnings(UNCHECKED)
	public static <T> T fromJson(String json, Class<?> clazz) {
		return (T) gson.fromJson(json, clazz);
	}

	public static String toJson(Object obj) {
		return gson.toJson(obj);
	}

	@SuppressWarnings(UNCHECKED)
	public static Map<String, Object> jsonToMap(String json) throws IOException {
		Map<String, Object> input = new ObjectMapper()
				.readValue(json.replaceAll(UNICODE_CHARACTER_EXPRESSION, BLANCK_CHARACTER), Map.class);
		return input;
	}

	@SuppressWarnings(UNCHECKED)
	public static List<Object> jsonToList(String json) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<Object> input = mapper.readValue(json.replaceAll(UNICODE_CHARACTER_EXPRESSION, BLANCK_CHARACTER),
				List.class);
		
		return input;
	}

	@SuppressWarnings(UNCHECKED)
	public static <T> T jsonToJavaObject(String json, Class<?> obj) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		T output = null;
		try {
			output = (T) mapper.readValue(json.replaceAll(UNICODE_CHARACTER_EXPRESSION, BLANCK_CHARACTER), obj);
		} catch (Exception e) {
		}
		return output;
	}

	@SuppressWarnings("deprecation")
	public static String javaObjectToJson(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String output = null;
		try {
			output = mapper.defaultPrettyPrintingWriter().writeValueAsString(obj);
		} catch (Exception e) {
		}
		return output;
	}

	public static String formatterJson(String json) {
		String newJson = null;
		Object object = null;

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			object = mapper.readValue(json.replaceAll(UNICODE_CHARACTER_EXPRESSION, BLANCK_CHARACTER), Object.class);
			newJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
			return newJson;
		} catch (Exception e) {
			return json;
		}
	}

	public static String formatterJsonWithoutNull(String json) {
		String newJson = null;
		Object object = null;

		com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
		mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
		try {
			object = mapper.readValue(json.replaceAll(UNICODE_CHARACTER_EXPRESSION, BLANCK_CHARACTER), Object.class);
			newJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (Exception e) {
		}
		return newJson;
	}

	public static Object extractPath(String json, String path) throws IOException {
		if (ValidatorUtil.isEmpty(path)) {
			return json;
		}

		Object o = null;
		try {
			o = JsonPath.read(json, path);
		} catch (PathNotFoundException e) {
			return o;
		}
		String extractedJson = javaObjectToJson(o);
		if (o instanceof JSONArray) {
			return jsonToList(extractedJson);
		} else if (o instanceof JSONObject) {
			return jsonToMap(extractedJson);
		} else {
			return o;
		}
	}

	public static List<Object> extractPathToList(String json, String path) throws IOException {

		Object o = JsonPath.read(json, path);
		String extractedJson = javaObjectToJson(o);
		if (o instanceof JSONArray) {
			return jsonToList(extractedJson);
		}
		List<Object> list = new ArrayList<Object>();
		list.add(jsonToMap(extractedJson));
		return list;
	}

	public static <T> List<T> extractPathToList(String json, String path, Class<?> clazz) throws IOException {

		Object extractedObject = JsonPath.read(json, path);
		if (ValidatorUtil.isEmpty(extractedObject)) {
			return null;
		}
		List<T> newList = new ArrayList<T>();
		String extractedJson = javaObjectToJson(extractedObject);
		if (extractedObject instanceof JSONArray) {
			List<Object> list = jsonToJavaObject(extractedJson, List.class);
			for (Object object : list) {
				T t = jsonToJavaObject(javaObjectToJson(object), clazz);
				newList.add(t);
			}
			return newList;
		}
		T t = jsonToJavaObject(extractedJson, clazz);
		newList.add(t);
		return newList;
	}

	public static <T> T extractPathToObject(String json, String path, Class<?> clazz) throws IOException {
		List<T> list = extractPathToList(json, path, clazz);
		if (ValidatorUtil.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	public static Object extractPathToObject(String json, String path) throws IOException {
		List<Object> list = extractPathToList(json, path);
		if (ValidatorUtil.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	public static Map<String, Object> transformToValidMap(String json) {
		Map<String, Object> map = null;
		if (ValidatorUtil.isNotEmpty(json)) {
			map = jsonToJavaObject(json, Map.class);
		}
		return map;
	}

}
