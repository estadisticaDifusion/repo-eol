package pe.gob.minedu.escale.eol.dto.estadistica;

import java.util.List;

public class EolEnvioPeriodoDTO {
	private String anio;
	private String descripcion;
	private List<EolEnvioCedulaDTO> actividades;

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<EolEnvioCedulaDTO> getActividades() {
		return actividades;
	}

	public void setActividades(List<EolEnvioCedulaDTO> actividades) {
		this.actividades = actividades;
	}

}
