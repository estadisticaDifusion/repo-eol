package pe.gob.minedu.escale.eol.dto;

public class OAuthResponseDTO {
	private String status;
	private String code;
	private String message;
	private String newToken;
	private String newRefreshToken;
	private Integer expired;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNewToken() {
		return newToken;
	}

	public void setNewToken(String newToken) {
		this.newToken = newToken;
	}

	public String getNewRefreshToken() {
		return newRefreshToken;
	}

	public void setNewRefreshToken(String newRefreshToken) {
		this.newRefreshToken = newRefreshToken;
	}

	public Integer getExpired() {
		return expired;
	}

	public void setExpired(Integer expired) {
		this.expired = expired;
	}

}
