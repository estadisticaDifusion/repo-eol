package pe.gob.minedu.escale.eol.dto.estadistica;

import java.io.Serializable;
import java.util.Date;

public class EolActividadDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2168044762522243963L;

	private Integer id;
	private String nombre;
	private String descripcion;
	private Date fechaInicio;
	private Date fechaTermino;
	private Date fechaLimite;
	private Date fechaTerminosigied;
	private Date fechaLimitesigied;
	private Date fechaTerminohuelga;
	private Date fechaLimitehuelga;
	private String urlFormato;
	private String urlConstancia;
	private String urlCobertura;
	private String urlSituacion;
	private String urlOmisos;
	private String sqlCenso;
	private String estado;
	private String situacion;
	private String nombreCedula;
	private String tipo;
	private Boolean estadoSie;
	private Boolean estadoConstancia;
	private Boolean estadoFormato;
	private Boolean estadoCobertura;
	private Boolean estadoSituacion;
	private Boolean estadoOmisos;
	private String orden;
	private EolPeriodoDTO periodo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public Date getFechaLimite() {
		return fechaLimite;
	}

	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public Date getFechaTerminosigied() {
		return fechaTerminosigied;
	}

	public void setFechaTerminosigied(Date fechaTerminosigied) {
		this.fechaTerminosigied = fechaTerminosigied;
	}

	public Date getFechaLimitesigied() {
		return fechaLimitesigied;
	}

	public void setFechaLimitesigied(Date fechaLimitesigied) {
		this.fechaLimitesigied = fechaLimitesigied;
	}

	public Date getFechaTerminohuelga() {
		return fechaTerminohuelga;
	}

	public void setFechaTerminohuelga(Date fechaTerminohuelga) {
		this.fechaTerminohuelga = fechaTerminohuelga;
	}

	public Date getFechaLimitehuelga() {
		return fechaLimitehuelga;
	}

	public void setFechaLimitehuelga(Date fechaLimitehuelga) {
		this.fechaLimitehuelga = fechaLimitehuelga;
	}

	public String getUrlFormato() {
		return urlFormato;
	}

	public void setUrlFormato(String urlFormato) {
		this.urlFormato = urlFormato;
	}

	public String getUrlConstancia() {
		return urlConstancia;
	}

	public void setUrlConstancia(String urlConstancia) {
		this.urlConstancia = urlConstancia;
	}

	public String getUrlCobertura() {
		return urlCobertura;
	}

	public void setUrlCobertura(String urlCobertura) {
		this.urlCobertura = urlCobertura;
	}

	public String getUrlSituacion() {
		return urlSituacion;
	}

	public void setUrlSituacion(String urlSituacion) {
		this.urlSituacion = urlSituacion;
	}

	public String getUrlOmisos() {
		return urlOmisos;
	}

	public void setUrlOmisos(String urlOmisos) {
		this.urlOmisos = urlOmisos;
	}

	public String getSqlCenso() {
		return sqlCenso;
	}

	public void setSqlCenso(String sqlCenso) {
		this.sqlCenso = sqlCenso;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getNombreCedula() {
		return nombreCedula;
	}

	public void setNombreCedula(String nombreCedula) {
		this.nombreCedula = nombreCedula;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Boolean getEstadoSie() {
		return estadoSie;
	}

	public void setEstadoSie(Boolean estadoSie) {
		this.estadoSie = estadoSie;
	}

	public Boolean getEstadoConstancia() {
		return estadoConstancia;
	}

	public void setEstadoConstancia(Boolean estadoConstancia) {
		this.estadoConstancia = estadoConstancia;
	}

	public Boolean getEstadoFormato() {
		return estadoFormato;
	}

	public void setEstadoFormato(Boolean estadoFormato) {
		this.estadoFormato = estadoFormato;
	}

	public Boolean getEstadoCobertura() {
		return estadoCobertura;
	}

	public void setEstadoCobertura(Boolean estadoCobertura) {
		this.estadoCobertura = estadoCobertura;
	}

	public Boolean getEstadoSituacion() {
		return estadoSituacion;
	}

	public void setEstadoSituacion(Boolean estadoSituacion) {
		this.estadoSituacion = estadoSituacion;
	}

	public Boolean getEstadoOmisos() {
		return estadoOmisos;
	}

	public void setEstadoOmisos(Boolean estadoOmisos) {
		this.estadoOmisos = estadoOmisos;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public EolPeriodoDTO getPeriodo() {
		return periodo;
	}

	public void setPeriodo(EolPeriodoDTO periodo) {
		this.periodo = periodo;
	}

}
