package pe.gob.minedu.escale.eol.payload.padron;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.entities.UgelDTO;

public class UgelResponsePayload {
	private String dataResult;
	private UgelDTO dataDetail;
	private List<UgelDTO> dataListDetail;

	private String cipher;
	
	public String getDataResult() {
		return dataResult;
	}

	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}

	public UgelDTO getDataDetail() {
		return dataDetail;
	}

	public void setDataDetail(UgelDTO dataDetail) {
		this.dataDetail = dataDetail;
	}

	public List<UgelDTO> getDataListDetail() {
		return dataListDetail;
	}

	public void setDataListDetail(List<UgelDTO> dataListDetail) {
		this.dataListDetail = dataListDetail;
	}

	public String getCipher() {
		return cipher;
	}

	public void setCipher(String cipher) {
		this.cipher = cipher;
	}
	
	

}
