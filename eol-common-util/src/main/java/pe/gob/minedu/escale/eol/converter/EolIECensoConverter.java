/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author jmatamoros
 */
@XmlRootElement(name = "institucion")
public class EolIECensoConverter {

    private Integer idActividad;
    private Integer nroEnvio;
    private Date fechaEnvio;
    private String codooii;
    private String situacion;
    private String codMod;
    private String anexo;
    private String nivMod;
    private String codLocal;
    private String cenEdu;
    private String nivelModalidad;
    private String codId;

    public EolIECensoConverter() {
    }



    @XmlElement(name = "idActividad")
    public Integer getIdActividad() {
    return idActividad;
    }


    @XmlElement(name = "nroEnvio")
    public Integer getNroEnvio() {
        return nroEnvio;
    }

    @XmlElement(name = "fechaEnvio")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    @XmlElement(name = "codooii")
    public String getCodooii() {
        return codooii;
    }

    @XmlElement(name = "situacion")
    public String getSituacion() {
        return situacion;
    }

    @XmlElement(name = "codMod")
    public String getCodMod() {
        return codMod;
    }

    @XmlElement(name = "anexo")
    public String getAnexo() {
        return anexo;
    }

    @XmlElement(name = "nivMod")
    public String getNivMod() {
        return nivMod;
    }

    @XmlElement(name = "codlocal")
    public String getCodLocal() {
        return codLocal;
    }

    @XmlElement(name = "cenEdu")
    public String getCenEdu() {
        return cenEdu;
    }

    @XmlElement(name = "nivelModalidad")
    public String getNivelModalidad() {
        return nivelModalidad;
    }

    @XmlElement(name = "codId")
    public String getCodId() {
        return codId;
    }




    public void setIdActividad(Integer idActividad) {
        this.idActividad = idActividad;
    }

    public void setNroEnvio(Integer nroEnvio) {
        this.nroEnvio = nroEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public void setCodooii(String codooii) {
        this.codooii = codooii;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public void setNivMod(String nivMod) {
        this.nivMod = nivMod;
    }

    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    public void setCenEdu(String cenEdu) {
        this.cenEdu = cenEdu;
    }

    public void setNivelModalidad(String nivelModalidad) {
        this.nivelModalidad = nivelModalidad;
    }

    public void setCodId(String codId) {
        this.codId = codId;
    }

}
