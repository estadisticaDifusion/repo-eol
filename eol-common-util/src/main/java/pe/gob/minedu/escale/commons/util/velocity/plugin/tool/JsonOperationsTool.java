/*
 * 
 */
package pe.gob.minedu.escale.commons.util.velocity.plugin.tool;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.velocity.tools.config.DefaultKey;
import org.joda.time.DateTime;
import org.joda.time.Days;

import pe.gob.minedu.escale.common.json.JsonUtil;
import pe.gob.minedu.escale.common.validator.ValidatorUtil;

@DefaultKey("jsonOperation")
public final class JsonOperationsTool {

	private static final String NULL = "null";
	private static final String UNCHECKED = "unchecked";

	public JsonOperationsTool() {

	}

	public static String toJson(Object parameter) {
		if (parameter == null) {
			return null;
		}
		return JsonUtil.javaObjectToJson(parameter);
	}

	public static Map<String, Object> toMap(Object parameter) {
		if (ValidatorUtil.isEmpty(parameter)) {
			return null;
		}
		try {
			return JsonUtil.jsonToMap(String.valueOf(parameter));
		} catch (IOException e) {
			return null;
		}
	}

	@SuppressWarnings("rawtypes")
	public static List toList(Object parameter) {
		if (ValidatorUtil.isEmpty(parameter)) {
			return null;
		}
		try {
			return JsonUtil.jsonToList(String.valueOf(parameter));
		} catch (IOException e) {
			return null;
		}
	}

	@SuppressWarnings(UNCHECKED)
	public static String extractJsonFromMap(Object parameter, String keys) {
		Map<String, Object> extracted = new HashMap<String, Object>();
		Object newParameter = parameter;

		if (ValidatorUtil.isEmpty(newParameter)) {
			return null;
		}

		if (newParameter instanceof String) {
			newParameter = JsonUtil.jsonToJavaObject(String.valueOf(newParameter), Map.class);
		}

		if (newParameter instanceof Map) {
			Map<String, Object> json = (Map<String, Object>) newParameter;
			String[] key = keys.split(",");
			for (int i = 0; i < key.length; i++) {
				if (ValidatorUtil.isNotEmpty(json.get(key[i]))) {
					extracted.put(key[i], json.get(key[i]));
				}
			}
			if (ValidatorUtil.isNotEmpty(extracted)) {
				return JsonUtil.javaObjectToJson(extracted);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@SuppressWarnings(UNCHECKED)
	public static String extractJsonValueFromMap(Object parameter, String key) {
		Object newParameter = parameter;

		if (ValidatorUtil.isEmpty(newParameter)) {
			return null;
		}

		if (newParameter instanceof String) {
			newParameter = JsonUtil.jsonToJavaObject(String.valueOf(newParameter), Map.class);
		}

		if (newParameter instanceof Map) {
			Map<String, Object> json = (Map<String, Object>) newParameter;
			if (ValidatorUtil.isNotEmpty(json.get(key))) {
				return JsonUtil.javaObjectToJson(json.get(key));
			} else {
				return NULL;
			}
		} else {
			return null;
		}
	}

	@SuppressWarnings(UNCHECKED)
	public static String getJsonWithValuesStringFromListMap(Object parameter) {
		List<Map<String, Object>> listJson = null;

		if (ValidatorUtil.isEmpty(parameter)) {
			return null;
		}

		if (parameter instanceof List) {
			listJson = (List<Map<String, Object>>) parameter;
			for (Map<String, Object> json : listJson) {

				for (Entry<String, Object> e : json.entrySet()) {
					if (ValidatorUtil.isNotEmpty(e.getValue())) {
						json.put(e.getKey(), e.getValue().toString());
					}
				}

			}

		} else {
			return null;
		}

		if (ValidatorUtil.isNotEmpty(listJson)) {
			return JsonUtil.javaObjectToJson(listJson);
		} else {
			return null;
		}
	}

	public static String extractListJsonFromListMap(Object parameter, String keys) {
		return extractListJsonFromListMap(parameter, keys, -1);
	}

	@SuppressWarnings(UNCHECKED)
	public static String extractListJsonFromListMap(Object parameter, String keys, int index) {
		List<Map<String, Object>> listJson = null;
		List<Map<String, Object>> extracted = new ArrayList<Map<String, Object>>();
		Object newParameter = parameter;
		Map<String, Object> detail = new HashMap<String, Object>();

		if (ValidatorUtil.isEmpty(newParameter)) {
			return null;
		}

		if (newParameter instanceof String) {
			newParameter = JsonUtil.jsonToJavaObject(String.valueOf(newParameter), List.class);
		}

		if (newParameter instanceof List) {
			listJson = (List<Map<String, Object>>) newParameter;
			for (Map<String, Object> json : listJson) {

				String[] key = keys.split(",");
				for (int i = 0; i < key.length; i++) {
					if (ValidatorUtil.isNotEmpty(json.get(key[i]))) {
						detail.put(key[i], json.get(key[i]));
					}
				}
				if (ValidatorUtil.isNotEmpty(detail)) {
					extracted.add(detail);
				}
			}

		} else {
			return null;
		}

		if (ValidatorUtil.isNotEmpty(extracted)) {
			if (index > -1) {
				return JsonUtil.javaObjectToJson(extracted.get(index));
			} else {
				return JsonUtil.javaObjectToJson(extracted);
			}
		} else {
			return null;
		}
	}

	@SuppressWarnings(UNCHECKED)
	public static String extractListJsonFromListMap(Object parameter, String keys, String values) {
		List<Map<String, Object>> extracted = new ArrayList<Map<String, Object>>();
		if (ValidatorUtil.isEmpty(parameter)) {
			return null;
		}

		if (parameter instanceof List) {
			List<Map<String, Object>> listJson = (List<Map<String, Object>>) parameter;
			for (Map<String, Object> json : listJson) {

				String[] key = keys.split(",");
				String[] value = values.split(",");
				for (int i = 0; i < key.length; i++) {
					if (ValidatorUtil.isNotEmpty(json.get(key[i]))
							&& String.valueOf(json.get(key[i])).equals(value[i])) {
						extracted.add(json);
						break;
					}

				}
			}

		} else {
			return null;
		}

		if (ValidatorUtil.isNotEmpty(extracted)) {
			return JsonUtil.javaObjectToJson(extracted);
		} else {
			return null;
		}
	}

	public static String extractCraKeyList(Object parameter, String description) {
		String result = extractJsonValueFromMap(extractListJsonFromListMap(
				extractListJsonFromListMap(parameter, "creditRiskAgentVariableName", description),
				"creditRiskAgentKeyFactors", 0), "creditRiskAgentKeyFactors");
		if (ValidatorUtil.isEmpty(result)) {
			return NULL;
		} else {
			return result;
		}
	}

	public static String getDaysBetween(String date) {

		if (ValidatorUtil.isEmpty(date)) {
			return NULL;
		}
		DateTime myDateTime;
		try {
			myDateTime = new DateTime();
			myDateTime = DateTime.parse(date);
			Days days = Days.daysBetween(myDateTime, new DateTime(System.currentTimeMillis()));

			return String.valueOf(days.getDays());
		} catch (Exception e) {
			return NULL;
		}

	}

	public static Object encodeDate(Object parameter, String format) {
		if (parameter instanceof Date) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
			return simpleDateFormat.format((Date) parameter);
		} else {
			return parameter;
		}

	}

	public static String formatterJson(String json) {
		return JsonUtil.formatterJson(json);
	}

	public static String formatterJsonWithoutNull(String json) {
		return JsonUtil.formatterJsonWithoutNull(json);
	}

	public List<Map<String, Object>> customSortList(List<Map<String, Object>> list, String splitList,
			final String propertyName) {
		final List<String> definedOrder = Arrays.asList(splitList.split(","));
		Comparator<Map<String, Object>> comparator = new Comparator<Map<String, Object>>() {
			public int compare(final Map<String, Object> o1, final Map<String, Object> o2) {
				if (definedOrder.indexOf(o1.get(propertyName)) == -1) {
					return 1;
				}
				if (definedOrder.indexOf(o2.get(propertyName)) == -1) {
					return -1;
				}
				return Integer.valueOf(definedOrder.indexOf(o1.get(propertyName)))
						.compareTo(Integer.valueOf(definedOrder.indexOf(o2.get(propertyName))));
			}
		};
		Collections.sort(list, comparator);
		return list;
	}

	public List<Map<String, Object>> customSortListGetElementsRange(List<Map<String, Object>> list, String splitList,
			final String propertyName, int indexFromRange, int indexToRange) {
		return customSortList(list, splitList, propertyName).subList(indexFromRange, indexToRange);
	}

	public Map<String, Object> customSortListGetElement(List<Map<String, Object>> list, String splitList,
			final String propertyName, int index) {
		return customSortList(list, splitList, propertyName).get(index);
	}

}
