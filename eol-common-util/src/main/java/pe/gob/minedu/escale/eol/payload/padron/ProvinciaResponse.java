package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class ProvinciaResponse extends ResponseGeneric {
	
	private ProvinciaResponsePayload responsePayload;

	public ProvinciaResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(ProvinciaResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}
	
	

}
