package pe.gob.minedu.escale.eol.payload.padron;

public class InsticucionEncrypt {
	private String codmod;
	private String anexo;
	private String anio;
	private String nivel;

	public InsticucionEncrypt() {
		super();
	}

	public String getCodmod() {
		return codmod;
	}

	public void setCodmod(String codmod) {
		this.codmod = codmod;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

}
