package pe.gob.minedu.escale.eol.dto.estadistica;

import java.util.Date;

public class EolEnvioCedulaDTO {
	private Long id;
	private String nombre;
	private Boolean estado;
	private String nivel;
	private String version;
	private Boolean envio = false;
	private Date fechaEnvio;
	private String strFechaEnvio;
	private Integer nroEnvio;
	private EolEnvioActividadDTO actividad;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getEnvio() {
		return envio;
	}

	public void setEnvio(Boolean envio) {
		this.envio = envio;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getStrFechaEnvio() {
		return strFechaEnvio;
	}

	public void setStrFechaEnvio(String strFechaEnvio) {
		this.strFechaEnvio = strFechaEnvio;
	}

	public Integer getNroEnvio() {
		return nroEnvio;
	}

	public void setNroEnvio(Integer nroEnvio) {
		this.nroEnvio = nroEnvio;
	}

	public EolEnvioActividadDTO getActividad() {
		return actividad;
	}

	public void setActividad(EolEnvioActividadDTO actividad) {
		this.actividad = actividad;
	}

}
