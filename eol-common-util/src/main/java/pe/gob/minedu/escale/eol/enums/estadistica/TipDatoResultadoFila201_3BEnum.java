/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.estadistica;

/**
 *
 * @author Owner
 */
public enum TipDatoResultadoFila201_3BEnum {

    TOTAL("1","","TOTAL"),
    APROBADOS("2","A","APROBADOS"),
    DESAPROBADOS("3","D","DESAPROBADOS");
    private String cod;
    private String codTipo;
    private String descri;

    TipDatoResultadoFila201_3BEnum(String cod,String codTipo,String descri)
    { this.cod=cod;
      this.codTipo=codTipo;
      this.descri=descri;

    }

    /**
     * @return the cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @return the codTipo
     */
    public String getCodTipo() {
        return codTipo;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }




}
