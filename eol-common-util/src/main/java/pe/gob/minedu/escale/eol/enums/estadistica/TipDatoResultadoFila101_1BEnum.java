/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.estadistica;

/**
 *
 * @author Owner
 */
public enum TipDatoResultadoFila101_1BEnum {

    TOTAL("1","","TOTAL MATRICULA"),
    CONCLUYERON("2","C","CONCLUYERON"),
    RETIRADOS("5","R","RETIRADOS"),
    TRASLADADOSA_OTROS_CE("6","T","TRASLADADOSA_OTROS_CE"),
    FALLECIDOS("7","F","FALLECIDOS");
    private String cod;
    private String codTipo;
    private String descri;

    TipDatoResultadoFila101_1BEnum(String cod,String codTipo,String descri)
    { this.cod=cod;
      this.codTipo=codTipo;
      this.descri=descri;

    }

    /**
     * @return the cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @return the codTipo
     */
    public String getCodTipo() {
        return codTipo;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }




}
