package pe.gob.minedu.escale.eol.dto.entities;

public class InstitucionBaseDTO {
	private String codMod;
	private String anexo;
	private String codlocal;
	private String cenEdu;
	private NivelModalidadDTO nivelModalidad;
	private String dirCen;
	private String telefono;
	private String director;
	private GestionDependenciaDTO gestionDependencia;
	private DistritoDTO distrito;
	private String cenPob;

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getCenEdu() {
		return cenEdu;
	}

	public void setCenEdu(String cenEdu) {
		this.cenEdu = cenEdu;
	}

	public NivelModalidadDTO getNivelModalidad() {
		return nivelModalidad;
	}

	public void setNivelModalidad(NivelModalidadDTO nivelModalidad) {
		this.nivelModalidad = nivelModalidad;
	}

	public String getDirCen() {
		return dirCen;
	}

	public void setDirCen(String dirCen) {
		this.dirCen = dirCen;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public GestionDependenciaDTO getGestionDependencia() {
		return gestionDependencia;
	}

	public void setGestionDependencia(GestionDependenciaDTO gestionDependencia) {
		this.gestionDependencia = gestionDependencia;
	}

	public DistritoDTO getDistrito() {
		return distrito;
	}

	public void setDistrito(DistritoDTO distrito) {
		this.distrito = distrito;
	}

	public String getCenPob() {
		return cenPob;
	}

	public void setCenPob(String cenPob) {
		this.cenPob = cenPob;
	}

}
