package pe.gob.minedu.escale.eol.constant;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright (C) 2018 Ministerio de Educacion Unidad de Estadistica Educativa
 * (Lima - Peru)
 *
 * PROYECTO EOL
 *
 * All rights reserved. Used by permission This software is the confidential and
 * proprietary information of MIMP ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with MIMP.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */
/**
 * @objetivo: Se implementa las constantes necesarios para los diferentes
 * componentes
 * @autor: Ing. Oscar Mateo
 * @fecha: 01/11/2018
 *
 * ------------------------------------------------------------------------
 * Modificaciones Fecha Nombre Descripción
 * ------------------------------------------------------------------------
 *
 */
public final class CoreConstant {

    public static final String RESULT_FAILURE = "FAILURE";
    public static final String RESULT_SUCCESS = "SUCCESS";
    
    public static final String DATA_FOUND = "DATA_FOUND";
    public static final String DATA_NOT_FOUND = "DATA_NOT_FOUND";
    
    public static final String DATA_PROCESSED = "DATA_PROCESSED";
    public static final String DATA_NOT_PROCESSED = "DATA_NOT_PROCESSED";

    public static final String LOGGED_USER = "LOGGED_USER";

    public static final Integer STATUS_ACTIVE = 1;
    public static final Integer STATUS_INACTIVE = 0;

    public static final String STATUS_ACTIVE_STR = "1";
    public static final String STATUS_INACTIVE_STR = "0";
    
    public static final String DATA_ALL = "TODOS";

    public static final String ACTIVE_YES = "S";
    public static final String ACTIVE_NOT = "N";

    public static final String S = "S";
    public static final String N = "N";

    public static final String WS_NOT_CALLED = "0";
    public static final String WS_PROBLEM = "1";

    public static final String BLANCO = " ";
    public static final String VACIO = "";

    public static final String OPERATION_NEW = "OPERATION_NEW";
    public static final String OPERATION_EDIT = "OPERATION_EDIT";
    public static final String OPERATION_VIEW = "OPERATION_VIEW";
    public static final String OPERATION_DELETE = "OPERATION_DELETE";

    public static final String FMT_FECHA_DDMMYYYY = "dd/MM/yyyy";

    public static final String CONDITION_OR = "or";
    public static final String CONDITION_AND = "and";
    public static final String SEPARATOR_COMA = ",";
    public static final String SEPARATOR_GUION = "-";
    public static final String SEPARATOR_FOLDER = "/";

    public static final String CODIGO_TODOS = "-1"; // "100";

    public static final String SALTO_MENSAJE = "\n";

    public static final Integer MAX_SIZE_FILE_UPLOAD = 10485760;

    public static final Long LEVEL_ZERO = 0L;

    public static final String URL_FORWARD = "pe.gob.minedu.escale.eol.constant.CoreConstant.URL_FORWARD";

    public static final Long ID_NEGATIVE = -1L;
    public static final Long NUMBER_ZERO = 0L;
    public static final Long CARGO_PADRE_ID = 7L;

    public static final Integer ROL_COORDINADOR_DIBP = 5;
    public static final Integer ROL_ADMINISTRADOR_SISTEMA = 6;

    public static final String IS_ASSOCIATE_ENTITY = "IS_ASSOCIATE_ENTITY";
    public static final String IS_ADMINISTRATOR = "IS_ADMINISTRATOR";
    public static final String ENTITY_ASSOCIATE_ID = "ENTITY_ASSOCIATE_ID";
    public static final String AMBITO_ENTITY_ID = "AMBITO_ENTITY_ID";

    public static final String CWS_CONNECT_TIMEOUT = "CWS_CONNECT_TIMEOUT";
    public static final String CWS_REQUEST_TIMEOUT = "CWS_REQUEST_TIMEOUT";

    public static final String LOCATE_ES_PE = "es_PE";
    public static final String LOCATE_ES_ES = "es_ES";

    public static final String FORMAT_PDF = "PDF";
    public static final String FORMAT_XLS = "XLS";

    public static final String EMPTY = "";

    /* Constant Project eol-admin-ws*/
    public static String SITUACION_PENDIENTE = "1";
    public static String SITUACION_REPORTADO = "2";
    public static String SITUACION_NO_APLICA = "3";

    public static String PADRON_CONST_ESTADO_ACTIVO = "1";
    public static String PADRON_CONST_ESTADO_INACTIVO = "2";
    public static String PADRON_CONST_ESTADO_ACTIVO_DESC = "Activo";
    public static String PADRON_CONST_ESTADO_INACTIVO_DESC = "Inactivo";

    public static String TIPO_ACTIVIDAD_IIEE = "1";
    public static String TIPO_ACTIVIDAD_LOCAL = "2";

    protected CoreConstant() {
        throw new UnsupportedOperationException();
    }
}
