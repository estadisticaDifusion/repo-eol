package pe.gob.minedu.escale.eol.payload.padron;

public class UbicacionRequest {

	private String codregion;
	private String codprovincia;
	private String coddistrito;
	
	public String getCodregion() {
		return codregion;
	}
	public void setCodregion(String codregion) {
		this.codregion = codregion;
	}
	public String getCodprovincia() {
		return codprovincia;
	}
	public void setCodprovincia(String codprovincia) {
		this.codprovincia = codprovincia;
	}
	public String getCoddistrito() {
		return coddistrito;
	}
	public void setCoddistrito(String coddistrito) {
		this.coddistrito = coddistrito;
	}	
}
