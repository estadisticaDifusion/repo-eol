package pe.gob.minedu.escale.eol.payload.estadistica;

public class Censo2017CeduladRequest {
	private Long idEnvio;
	private String nroced;
	private String codMod;
	private String codlocal;
	private String codugel;
	private String p01sw;
	private String p02aini;
	private String p02bpri;
	private String p02csec;
	private String p03ainiD;
	private String p03ainiM;
	private String p03bpriD;
	private String p03bpriM;
	private String p03csecD;
	private String p03csecM;
	private String p04aini;
	private String p04bpri;
	private String p04csec;
	private String p05augel;
	private String p05bdre;
	private String p05cmun;
	private String p05dotr;
	private String p05dotrE;
	private String p05eno;
	private String p06;
	private String p07aini;
	private String p07bpri;
	private String p07csec;
	private String p08a11;
	private String p08a12;
	private String p08a13;
	private String p08b21;
	private String p08b22;
	private String p08b23;
	private String p08c31;
	private String p08c32;
	private String p08c33;
	private String p08d41;
	private String p08d42;
	private String p08d43;
	private String p08e51;
	private String p08e52;
	private String p08e53;
	private String p09a11;
	private String p09a12;
	private String p09a13;
	private String p09b21;
	private String p09b22;
	private String p09b23;
	private String p09c31;
	private String p09c32;
	private String p09c33;
	private String p09d41;
	private String p09d42;
	private String p09d43;
	private String p09e51;
	private String p09e52;
	private String p09e53;
	private String p09f61;
	private String p09f62;
	private String p09f63;
	private Integer p10aini;
	private Integer p10bpri;
	private Integer p10csec;
	private Integer p11aini;
	private Integer p11bpri;
	private Integer p11csec;
	private Integer p12aini;
	private Integer p12bpri;
	private Integer p12csec;
	private String p13;
	private String p13Esp;
	private String p14;
	private String p15aini;
	private String p15bpri;
	private String p15csec;
	private String p16aie;
	private String p16bugel;
	private String p16cdre;
	private String p16dmun;
	private String apaterno;
	private String amaterno;
	private String nombres;
	private String fuente;
	private String fechaEnvio;
	private Boolean ultimo;
	private String version;
	private String situacion;
	private String estadoRpt;
	private String msg;
	private Long token;
	private String anexo;

	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	public String getNroced() {
		return nroced;
	}

	public void setNroced(String nroced) {
		this.nroced = nroced;
	}

	public String getCodMod() {
		return codMod;
	}

	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}

	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getCodugel() {
		return codugel;
	}

	public void setCodugel(String codugel) {
		this.codugel = codugel;
	}

	public String getP01sw() {
		return p01sw;
	}

	public void setP01sw(String p01sw) {
		this.p01sw = p01sw;
	}

	public String getP02aini() {
		return p02aini;
	}

	public void setP02aini(String p02aini) {
		this.p02aini = p02aini;
	}

	public String getP02bpri() {
		return p02bpri;
	}

	public void setP02bpri(String p02bpri) {
		this.p02bpri = p02bpri;
	}

	public String getP02csec() {
		return p02csec;
	}

	public void setP02csec(String p02csec) {
		this.p02csec = p02csec;
	}

	public String getP03ainiD() {
		return p03ainiD;
	}

	public void setP03ainiD(String p03ainiD) {
		this.p03ainiD = p03ainiD;
	}

	public String getP03ainiM() {
		return p03ainiM;
	}

	public void setP03ainiM(String p03ainiM) {
		this.p03ainiM = p03ainiM;
	}

	public String getP03bpriD() {
		return p03bpriD;
	}

	public void setP03bpriD(String p03bpriD) {
		this.p03bpriD = p03bpriD;
	}

	public String getP03bpriM() {
		return p03bpriM;
	}

	public void setP03bpriM(String p03bpriM) {
		this.p03bpriM = p03bpriM;
	}

	public String getP03csecD() {
		return p03csecD;
	}

	public void setP03csecD(String p03csecD) {
		this.p03csecD = p03csecD;
	}

	public String getP03csecM() {
		return p03csecM;
	}

	public void setP03csecM(String p03csecM) {
		this.p03csecM = p03csecM;
	}

	public String getP04aini() {
		return p04aini;
	}

	public void setP04aini(String p04aini) {
		this.p04aini = p04aini;
	}

	public String getP04bpri() {
		return p04bpri;
	}

	public void setP04bpri(String p04bpri) {
		this.p04bpri = p04bpri;
	}

	public String getP04csec() {
		return p04csec;
	}

	public void setP04csec(String p04csec) {
		this.p04csec = p04csec;
	}

	public String getP05augel() {
		return p05augel;
	}

	public void setP05augel(String p05augel) {
		this.p05augel = p05augel;
	}

	public String getP05bdre() {
		return p05bdre;
	}

	public void setP05bdre(String p05bdre) {
		this.p05bdre = p05bdre;
	}

	public String getP05cmun() {
		return p05cmun;
	}

	public void setP05cmun(String p05cmun) {
		this.p05cmun = p05cmun;
	}

	public String getP05dotr() {
		return p05dotr;
	}

	public void setP05dotr(String p05dotr) {
		this.p05dotr = p05dotr;
	}

	public String getP05dotrE() {
		return p05dotrE;
	}

	public void setP05dotrE(String p05dotrE) {
		this.p05dotrE = p05dotrE;
	}

	public String getP05eno() {
		return p05eno;
	}

	public void setP05eno(String p05eno) {
		this.p05eno = p05eno;
	}

	public String getP06() {
		return p06;
	}

	public void setP06(String p06) {
		this.p06 = p06;
	}

	public String getP07aini() {
		return p07aini;
	}

	public void setP07aini(String p07aini) {
		this.p07aini = p07aini;
	}

	public String getP07bpri() {
		return p07bpri;
	}

	public void setP07bpri(String p07bpri) {
		this.p07bpri = p07bpri;
	}

	public String getP07csec() {
		return p07csec;
	}

	public void setP07csec(String p07csec) {
		this.p07csec = p07csec;
	}

	public String getP08a11() {
		return p08a11;
	}

	public void setP08a11(String p08a11) {
		this.p08a11 = p08a11;
	}

	public String getP08a12() {
		return p08a12;
	}

	public void setP08a12(String p08a12) {
		this.p08a12 = p08a12;
	}

	public String getP08a13() {
		return p08a13;
	}

	public void setP08a13(String p08a13) {
		this.p08a13 = p08a13;
	}

	public String getP08b21() {
		return p08b21;
	}

	public void setP08b21(String p08b21) {
		this.p08b21 = p08b21;
	}

	public String getP08b22() {
		return p08b22;
	}

	public void setP08b22(String p08b22) {
		this.p08b22 = p08b22;
	}

	public String getP08b23() {
		return p08b23;
	}

	public void setP08b23(String p08b23) {
		this.p08b23 = p08b23;
	}

	public String getP08c31() {
		return p08c31;
	}

	public void setP08c31(String p08c31) {
		this.p08c31 = p08c31;
	}

	public String getP08c32() {
		return p08c32;
	}

	public void setP08c32(String p08c32) {
		this.p08c32 = p08c32;
	}

	public String getP08c33() {
		return p08c33;
	}

	public void setP08c33(String p08c33) {
		this.p08c33 = p08c33;
	}

	public String getP08d41() {
		return p08d41;
	}

	public void setP08d41(String p08d41) {
		this.p08d41 = p08d41;
	}

	public String getP08d42() {
		return p08d42;
	}

	public void setP08d42(String p08d42) {
		this.p08d42 = p08d42;
	}

	public String getP08d43() {
		return p08d43;
	}

	public void setP08d43(String p08d43) {
		this.p08d43 = p08d43;
	}

	public String getP08e51() {
		return p08e51;
	}

	public void setP08e51(String p08e51) {
		this.p08e51 = p08e51;
	}

	public String getP08e52() {
		return p08e52;
	}

	public void setP08e52(String p08e52) {
		this.p08e52 = p08e52;
	}

	public String getP08e53() {
		return p08e53;
	}

	public void setP08e53(String p08e53) {
		this.p08e53 = p08e53;
	}

	public String getP09a11() {
		return p09a11;
	}

	public void setP09a11(String p09a11) {
		this.p09a11 = p09a11;
	}

	public String getP09a12() {
		return p09a12;
	}

	public void setP09a12(String p09a12) {
		this.p09a12 = p09a12;
	}

	public String getP09a13() {
		return p09a13;
	}

	public void setP09a13(String p09a13) {
		this.p09a13 = p09a13;
	}

	public String getP09b21() {
		return p09b21;
	}

	public void setP09b21(String p09b21) {
		this.p09b21 = p09b21;
	}

	public String getP09b22() {
		return p09b22;
	}

	public void setP09b22(String p09b22) {
		this.p09b22 = p09b22;
	}

	public String getP09b23() {
		return p09b23;
	}

	public void setP09b23(String p09b23) {
		this.p09b23 = p09b23;
	}

	public String getP09c31() {
		return p09c31;
	}

	public void setP09c31(String p09c31) {
		this.p09c31 = p09c31;
	}

	public String getP09c32() {
		return p09c32;
	}

	public void setP09c32(String p09c32) {
		this.p09c32 = p09c32;
	}

	public String getP09c33() {
		return p09c33;
	}

	public void setP09c33(String p09c33) {
		this.p09c33 = p09c33;
	}

	public String getP09d41() {
		return p09d41;
	}

	public void setP09d41(String p09d41) {
		this.p09d41 = p09d41;
	}

	public String getP09d42() {
		return p09d42;
	}

	public void setP09d42(String p09d42) {
		this.p09d42 = p09d42;
	}

	public String getP09d43() {
		return p09d43;
	}

	public void setP09d43(String p09d43) {
		this.p09d43 = p09d43;
	}

	public String getP09e51() {
		return p09e51;
	}

	public void setP09e51(String p09e51) {
		this.p09e51 = p09e51;
	}

	public String getP09e52() {
		return p09e52;
	}

	public void setP09e52(String p09e52) {
		this.p09e52 = p09e52;
	}

	public String getP09e53() {
		return p09e53;
	}

	public void setP09e53(String p09e53) {
		this.p09e53 = p09e53;
	}

	public String getP09f61() {
		return p09f61;
	}

	public void setP09f61(String p09f61) {
		this.p09f61 = p09f61;
	}

	public String getP09f62() {
		return p09f62;
	}

	public void setP09f62(String p09f62) {
		this.p09f62 = p09f62;
	}

	public String getP09f63() {
		return p09f63;
	}

	public void setP09f63(String p09f63) {
		this.p09f63 = p09f63;
	}

	public Integer getP10aini() {
		return p10aini;
	}

	public void setP10aini(Integer p10aini) {
		this.p10aini = p10aini;
	}

	public Integer getP10bpri() {
		return p10bpri;
	}

	public void setP10bpri(Integer p10bpri) {
		this.p10bpri = p10bpri;
	}

	public Integer getP10csec() {
		return p10csec;
	}

	public void setP10csec(Integer p10csec) {
		this.p10csec = p10csec;
	}

	public Integer getP11aini() {
		return p11aini;
	}

	public void setP11aini(Integer p11aini) {
		this.p11aini = p11aini;
	}

	public Integer getP11bpri() {
		return p11bpri;
	}

	public void setP11bpri(Integer p11bpri) {
		this.p11bpri = p11bpri;
	}

	public Integer getP11csec() {
		return p11csec;
	}

	public void setP11csec(Integer p11csec) {
		this.p11csec = p11csec;
	}

	public Integer getP12aini() {
		return p12aini;
	}

	public void setP12aini(Integer p12aini) {
		this.p12aini = p12aini;
	}

	public Integer getP12bpri() {
		return p12bpri;
	}

	public void setP12bpri(Integer p12bpri) {
		this.p12bpri = p12bpri;
	}

	public Integer getP12csec() {
		return p12csec;
	}

	public void setP12csec(Integer p12csec) {
		this.p12csec = p12csec;
	}

	public String getP13() {
		return p13;
	}

	public void setP13(String p13) {
		this.p13 = p13;
	}

	public String getP13Esp() {
		return p13Esp;
	}

	public void setP13Esp(String p13Esp) {
		this.p13Esp = p13Esp;
	}

	public String getP14() {
		return p14;
	}

	public void setP14(String p14) {
		this.p14 = p14;
	}

	public String getP15aini() {
		return p15aini;
	}

	public void setP15aini(String p15aini) {
		this.p15aini = p15aini;
	}

	public String getP15bpri() {
		return p15bpri;
	}

	public void setP15bpri(String p15bpri) {
		this.p15bpri = p15bpri;
	}

	public String getP15csec() {
		return p15csec;
	}

	public void setP15csec(String p15csec) {
		this.p15csec = p15csec;
	}

	public String getP16aie() {
		return p16aie;
	}

	public void setP16aie(String p16aie) {
		this.p16aie = p16aie;
	}

	public String getP16bugel() {
		return p16bugel;
	}

	public void setP16bugel(String p16bugel) {
		this.p16bugel = p16bugel;
	}

	public String getP16cdre() {
		return p16cdre;
	}

	public void setP16cdre(String p16cdre) {
		this.p16cdre = p16cdre;
	}

	public String getP16dmun() {
		return p16dmun;
	}

	public void setP16dmun(String p16dmun) {
		this.p16dmun = p16dmun;
	}

	public String getApaterno() {
		return apaterno;
	}

	public void setApaterno(String apaterno) {
		this.apaterno = apaterno;
	}

	public String getAmaterno() {
		return amaterno;
	}

	public void setAmaterno(String amaterno) {
		this.amaterno = amaterno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	public String getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getEstadoRpt() {
		return estadoRpt;
	}

	public void setEstadoRpt(String estadoRpt) {
		this.estadoRpt = estadoRpt;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Long getToken() {
		return token;
	}

	public void setToken(Long token) {
		this.token = token;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

}
