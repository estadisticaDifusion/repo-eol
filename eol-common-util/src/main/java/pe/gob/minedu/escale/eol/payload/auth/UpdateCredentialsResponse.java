package pe.gob.minedu.escale.eol.payload.auth;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class UpdateCredentialsResponse extends ResponseGeneric {

	private UpdateCredentialsResponsePayload responsePayload;

	public UpdateCredentialsResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(UpdateCredentialsResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}

}
