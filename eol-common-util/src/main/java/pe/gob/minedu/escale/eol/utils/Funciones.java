package pe.gob.minedu.escale.eol.utils;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.gob.minedu.escale.eol.constant.CoreConstant;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright (C) 2018
 * Ministerio de Educacion 
 * Unidad de Estadistica Educativa
 * (Lima - Peru)
 *  
 * PROYECTO EOL
 *
 * All rights reserved. Used by permission This software is the confidential and
 * proprietary information of MIMP ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with MIMP.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */
/**
 * @objetivo: Se implementa los metodos necesarios para los Usuarios Auth
 * @autor: Ing. Oscar Mateo
 * @fecha: 01/11/2018
 *
 *         ------------------------------------------------------------------------
 *         Modificaciones Fecha Nombre Descripción
 *         ------------------------------------------------------------------------
 *
 */
public class Funciones {

	private static String[] formatoArchivo = { "image/png", "image/jpeg", "image/pjpeg", "image/x-png" };

	public static Long uniqueID() {
		Long current = System.currentTimeMillis();
		return current++;
	}

	public static String getISODateNowAsString() {
		SimpleDateFormat dateToIsoDateString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		TimeZone tz = TimeZone.getTimeZone("UTC");
		dateToIsoDateString.setTimeZone(tz);
		return dateToIsoDateString.format(new Date());
	}

	public static boolean ftmArchivo(String formato) {
		for (int i = 0; i < formatoArchivo.length; i++) {
			if (formatoArchivo[i].equals(formato)) {
				return true;
			}
		}
		return false;
	}

	public static Date fecha(Integer dia, Integer mes, Integer anio) {
		Calendar cal = Calendar.getInstance();
		cal.set(anio, mes, dia);
		return cal.getTime();
	}

	public static boolean CheckDate(int day, int month, int year, int hora, int minuto, int segundo) {
		java.util.GregorianCalendar gcDate = new java.util.GregorianCalendar();
		gcDate.clear();
		gcDate.setLenient(false);
		try {
			gcDate.set(year, month - 1, day, hora, minuto, segundo);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean CheckDate(int day, int month, int year) {
		return CheckDate(day, month, year, 0, 0, 0);
	}

	public static boolean verificarFechaCadena(String fecha) {
		boolean ret = false;
		int day = 0, month = 0, year = 0, hora = 0, minu = 0, segu = 0;
		String mask = "\\d{2}\\/\\d{2}\\/\\d{4}";
		try {
			ret = fecha.matches(mask);
			if (ret) {
				day = Integer.parseInt(fecha.substring(0, 2));
				month = Integer.parseInt(fecha.substring(3, 5));
				year = Integer.parseInt(fecha.substring(6, 10));
				if (month < 0 || month > 12) {
					return false;
				}
				if (day < 0 || day > numDiasMes(month, year)) {
					return false;
				}
				ret = CheckDate(day, month, year, hora, minu, segu);
			}
		} catch (Exception e) {
			return false;
		}
		return ret;
	}

	public static boolean verificarFechaHoraCadena(String fecha) {
		boolean ret = false;
		int day = 0, month = 0, year = 0, hora = 0, minu = 0, segu = 0;
		String mask = "\\d{2}\\/\\d{2}\\/\\d{4} \\d{2}:\\d{2}:\\d{2}";
		try {
			ret = fecha.matches(mask);
			if (ret) {
				day = Integer.parseInt(fecha.substring(0, 2));
				month = Integer.parseInt(fecha.substring(3, 5));
				year = Integer.parseInt(fecha.substring(6, 10));
				hora = Integer.parseInt(fecha.substring(11, 13));
				minu = Integer.parseInt(fecha.substring(14, 16));
				segu = Integer.parseInt(fecha.substring(17, 19));
				System.out.println("numDiasMes: " + numDiasMes(month, year));
				if (month < 0 || month > 12) {
					return false;
				}
				if (day < 0 || day > numDiasMes(month, year)) {
					return false;
				}
				if (hora < 0 || hora > 24) {
					return false;
				}
				if (minu < 0 || minu > 60) {
					return false;
				}
				if (segu < 0 || segu > 60) {
					return false;
				}
				ret = CheckDate(day, month, year, hora, minu, segu);

			}
		} catch (Exception e) {
			return false;
		}
		return ret;
	}

	@SuppressWarnings("rawtypes")
	public static boolean esVacio(Object value) {
		if (value == null) {
			return true;
		}
		if (value instanceof String) {
			return ((String) value).trim().equals("") || ((String) value).trim().equals("-")
					|| ((String) value).trim().equals("false");
		}
		if (value instanceof Object[]) {
			return ((Object[]) value).length == 0;
		}
		if (value instanceof Collection) {
			return ((Collection) value).size() == 0;
		}
		if (value instanceof Integer) {
			return ((Integer) value) == 0;
		}
		if (value instanceof Double) {
			return ((Double) value) == 0;
		}
		return false;
	}

	@SuppressWarnings("unused")
	public static boolean esNumero(String value) {
		boolean ret = false;
		if (value == null) {
			return ret;
		}
		try {
			int n = Integer.parseInt(value);
			ret = true;
		} catch (Exception e) {
			ret = false;
		}
		return ret;
	}

	public static boolean esNumeroEntero0(String value) {
		boolean ret = false;
		if (esNumero(value)) {
			int n = Integer.parseInt(value);
			if (n >= 0) {
				ret = true;
			}
		}
		return ret;
	}

	@SuppressWarnings("unused")
	public static boolean esDecimal(String value) {
		boolean ret = false;
		if (value == null) {
			return ret;
		}
		try {
			double n = Double.parseDouble(value);
			ret = true;
		} catch (Exception e) {
			ret = false;
		}
		return ret;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void copyProperties(Map a, Map de) {
		Set<String> keySet = de.keySet();
		Object value = null;
		for (String key : keySet) {
			// String key = keyi.next();
			value = de.get(key);
			// log.debug(key + "=" + value);
			if (!Funciones.esVacio(value)) {
				Object v2 = a.get(key);
				// log.debug("v2:" + v2);
				if (Funciones.esVacio(v2) || !v2.equals(value)) {
					// log.debug("v2." + key + "=" + value);
					a.put(key, value);
				}
			}
		}
	}

	public static Date calcularFechaMin(Integer anio, Integer mes, Integer dia) {
		if (esVacio(anio) && esVacio(mes) && esVacio(dia)) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Funciones.esVacio(anio) ? 1900 : anio, Funciones.esVacio(mes) ? 0 : mes - 1,
				Funciones.esVacio(dia) ? 1 : dia);
		return cal.getTime();
	}

	public static Date getTipoFecha(int dia, int mes, int anio) {
		return getTipoFecha(dia, mes, anio, 0, 0, 0);
	}

	public static Date getTipoFecha(int dia, int mes, int anio, int hora, int minuto, int segundo) {
		Calendar cal = Calendar.getInstance();
		cal.set(anio, mes - 1, dia, hora, minuto, segundo);
		return cal.getTime();
	}

	public static Date getTipoFecha(String fecha, boolean fechaHora) {
		fecha = fecha.trim();
		Date ret;
		int day = 0, month = 0, year = 0, hora = 0, minu = 0, segu = 0;
		day = Integer.parseInt(fecha.substring(0, 2));
		month = Integer.parseInt(fecha.substring(3, 5));
		year = Integer.parseInt(fecha.substring(6, 10));
		if (fechaHora) {
			hora = Integer.parseInt(fecha.substring(11, 13));
			minu = Integer.parseInt(fecha.substring(14, 16));
			if (!esVacio(fecha.substring(17, 19))) {
				segu = Integer.parseInt(fecha.substring(17, 19));
			}
		}
		if (fechaHora) {
			ret = getTipoFecha(day, month, year, hora, minu, segu);
		} else {
			ret = getTipoFecha(day, month, year);
		}
		return ret;
	}

	public static Date calcularFechaMax(Integer anio, Integer mes, Integer dia) {
		if (esVacio(anio) && esVacio(mes) && esVacio(dia)) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		int vAnio = Funciones.esVacio(anio) ? 3000 : anio;
		int vMes = Funciones.esVacio(mes) ? 12 : mes;
		int dd = Funciones.esVacio(dia) ? numDiasMes(vMes, vAnio) : dia;
		cal.set(vAnio, vMes - 1, dd);
		return cal.getTime();
	}

	public static int numDiasMes(int vMes, int vAnio) {
		switch (vMes) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		case 2:
			return vAnio % 4 == 0 ? 29 : 28;
		}
		return -1;
	}

	public static String espacios(int n) {
		String cad = "";
		while (cad.length() < n) {
			cad += " ";

		}
		return cad;
	}

	public static boolean existe(String busca, String[] lista) {
		for (String item : lista) {
			if (item.equals(busca)) {
				return true;
			}
		}
		return false;
	}

	public static boolean enLista(Object object, Object[] lista) {
		Arrays.sort(lista);
		int pos = Arrays.binarySearch(lista, object);
		return pos >= 0 && pos < lista.length;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List array2list(Object[] value) {
		List ret = new ArrayList();
		for (Object item : value) {
			ret.add(item);
		}
		return ret;
	}

	public static Long contar(Map<String, Map<String, Long>> cuentas) {
		long cuenta = 0;
		for (String key : cuentas.keySet()) {
			// String key = iter.next();
			Map<String, Long> fila = cuentas.get(key);
			cuenta += fila.get("cuenta");
		}
		return cuenta;
	}

	public static boolean esVacio(String tipoReg, boolean noConsiderarGuiones) {
		if (noConsiderarGuiones) {
			return tipoReg == null || tipoReg.trim().equals("");
		}
		return esVacio(tipoReg);
	}

	public static void pausa(int i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException ex) {
			// log.warn(ex);
		}
	}

	public static Date formatoFecha(Integer dia, Integer mes, Integer anio) {
		if (esVacio(anio) && esVacio(mes) && esVacio(dia)) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.set(anio, mes - 1, dia);
		return cal.getTime();
	}

	public static Date fechaActual() {
		Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}

	public static Date fechaHoraActualconFormato() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		java.sql.Time sqlTime = new java.sql.Time(cal.getTimeInMillis());
		String fecha = sdf.format(cal.getTime()) + " " + sqlTime;
		// log.debug("fecha: "+fecha);
		return getTipoFecha(fecha, true);
	}

	public static Date fechaActualconFormato() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String fecha = sdf.format(cal.getTime());
		return getTipoFecha(fecha, false);
	}

	public static int mesActual() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String mes = sdf.format(cal.getTime());
		// log.debug("mes: "+mes);
		return Integer.parseInt(mes);
	}

	public static int anioActual() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		String anio = sdf.format(cal.getTime());
		return Integer.parseInt(anio);
	}

	public static String fechaFormatoDMY() {
		return fechaAcadena(fechaActualconFormato(), "dd/MM/yyyy");
	}

	public static String fechaAcadena(Date fecha, String formato) {
		// "dd/MM/yyyy"
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(fecha);
	}

	public static String[] stringToArray(String wordString, String separador) {
		String[] result;
		int i = 0; // index into the next empty array element

		// --- Declare and create a StringTokenizer
		StringTokenizer st = new StringTokenizer(wordString, separador);

		// --- Create an array which will hold all the tokens.
		result = new String[st.countTokens()];

		// --- Loop, getting each of the tokens
		while (st.hasMoreTokens()) {
			result[i++] = st.nextToken();
		}

		return result;
	}

	public static Date getTipoFecha(String formato) {
		return getTipoFecha(fechaAcadena(fechaActual(), formato), formato);
	}

	@SuppressWarnings("unused")
	public static Date getTipoFecha(String date, String formato) {
		Date fechaRetorno = null;
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		// we will now try to parse the string into date form
		try {
			fechaRetorno = sdf.parse(date);
		} // if the format of the string provided doesn't match the format we
			// declared in SimpleDateFormat() we will get an exception
		catch (ParseException e) {
			String errorMessage = "the date you provided is in an invalid date" + " format.";
		}
		return fechaRetorno;
	}

	@SuppressWarnings("unused")
	public static boolean esFecha(String date, String formato) {

		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		// declare and initialize testDate variable, this is what will hold
		// our converted string

		Date testDate = null;

		// we will now try to parse the string into date form
		try {
			testDate = sdf.parse(date);
		} // if the format of the string provided doesn't match the format we
			// declared in SimpleDateFormat() we will get an exception
		catch (ParseException e) {
			String errorMessage = "the date you provided is in an invalid date" + " format.";
			return false;
		}

		if (!sdf.format(testDate).equals(date)) {
			String errorMessage = "The date that you provided is invalid.";
			return false;
		}

		return true;

	}

	public static String frmCodigoPedido(int id) {
		return String.format("%1$08d-" + anioActual(), id);
	}

	// Codigo de Tramite 2010-031-AGBGEXC
	public static String frmCodigoDenuncia() {
		return randomTextUpper(7); // +id+"-"+anioActual();
	}

	public static String randomTextUpper(int size) {
		String elegibleChars = "ABCDEFGHJKLMPQRSTUVWXY23456789";
		char[] chars = elegibleChars.toCharArray();
		StringBuffer finalString = new StringBuffer();
		if (size == 0) {
			size = 6;
		}
		for (int i = 0; i < size; i++) {
			double randomValue = Math.random();
			int randomIndex = (int) Math.round(randomValue * (chars.length - 1));
			char characterToShow = chars[randomIndex];
			finalString.append(characterToShow);
		}
		return finalString.toString();
	}

	public static String randomText(int size) {
		String elegibleChars = "ABCDEFGHJKLMPQRSTUVWXYabcdefhjkmnpqrstuvwxy23456789";
		char[] chars = elegibleChars.toCharArray();
		StringBuffer finalString = new StringBuffer();
		if (size == 0) {
			size = 6;
		}
		for (int i = 0; i < size; i++) {
			double randomValue = Math.random();
			int randomIndex = (int) Math.round(randomValue * (chars.length - 1));
			char characterToShow = chars[randomIndex];
			finalString.append(characterToShow);
		}
		return finalString.toString();
	}

	public static boolean esCorreoElectronico(String email) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static String fileComponent(String filename) {
		int i = filename.lastIndexOf(File.separator);
		return (i > -1) ? filename.substring(i + 1) : filename;
	}

	public static String dateToString(Date date, String format) {
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}

	public static String fmtLlenaCeroCorrelativo(Integer length, Long value) {
		return String.format("%1$0" + length + "d", value);
	}

	// VRamos
	public static String formatoDecimal(Double numero, int decimales) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		String dec = "";
		for (int i = 0; i < decimales; i++) {
			dec = dec + "0";
		}
		String strDec = "############0." + dec;
		DecimalFormat format = new DecimalFormat(strDec, symbols);

		return format.format(numero);
	}

	public static String recortarCadena(String texto, Integer nuevoTamanio) {
		String nuevoTexto = "";
		texto = texto.trim();
		if (texto.length() <= nuevoTamanio) {
			nuevoTexto = texto;
		} else {
			nuevoTexto = texto.substring(0, nuevoTamanio) + "...";
		}
		return nuevoTexto;
	}

	private static Date operacionesConFechas(Date fecha, int parteFecha, int cantidad) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(parteFecha, cantidad);
		return calendar.getTime();
	}

	public static Date restarDiasFecha(Date fecha, int cantidad) {
		return operacionesConFechas(fecha, Calendar.DAY_OF_YEAR, cantidad * -1);
	}

	public static Date ObtenerDiaHabileFecha(Date fecha) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fecha);
		int dia = cal.get(Calendar.DAY_OF_WEEK);

		if (dia > 1 && dia < 7) {
			return fecha;
		} else {
			return ObtenerDiaHabileFecha(restarDiasFecha(fecha, 1));
		}
	}

	public static String diaSemanaFecha(Date fecha) {
		String diaSemana = "";
		int dia;

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fecha);
		dia = cal.get(Calendar.DAY_OF_WEEK);

		switch (dia) {
		case 1:
			diaSemana = "Domingo";
			break;
		case 2:
			diaSemana = "Lunes";
			break;
		case 3:
			diaSemana = "Martes";
			break;
		case 4:
			diaSemana = "Miercoles";
			break;
		case 5:
			diaSemana = "Jueves";
			break;
		case 6:
			diaSemana = "Viernes";
			break;
		case 7:
			diaSemana = "Sabado";
			break;
		}

		return diaSemana;
	}

	public static String getPathSubdirectory(String tramaData) {
		if (Funciones.esVacio(tramaData)) {
			return null;
		}
		String[] tramaByArray = tramaData.split(CoreConstant.SEPARATOR_GUION);
		String ruta = tramaByArray[1] + CoreConstant.SEPARATOR_FOLDER + tramaByArray[0] + CoreConstant.SEPARATOR_GUION
				+ tramaByArray[2];
		return ruta;
	}

	public static void reflactUtils(String propiedad) {
		if (propiedad == null) {
			return;
		}
		String[] propertyNameArray = propiedad.split("\\.");
		System.out.println(propertyNameArray + " size: " + propertyNameArray.length);
		if (propertyNameArray.length > 1) {

		} else {

		}
	}

	public static HashMap<String, String> getElapsedTimeInMap(Long startTime) {

		HashMap<String, String> mapResult = new HashMap<String, String>();

		Long endTime = System.currentTimeMillis();
		Long elapsedTimeMillis = endTime - startTime;

		Float elapsedTimeSec = elapsedTimeMillis / 1000F;
		Float elapsedTimeMin = elapsedTimeMillis / (60 * 1000F);

		String printMessage = "Elapsed time of the execution in milliseconds " + elapsedTimeMillis + " | in seconds "
				+ elapsedTimeSec + " | in minutes " + elapsedTimeMin;

		mapResult.put("elapsedTimeMillis", elapsedTimeMillis.toString());
		mapResult.put("elapsedTimeSec", elapsedTimeSec.toString());
		mapResult.put("elapsedTimeMin", elapsedTimeMin.toString());
		mapResult.put("printMessage", printMessage);
		return mapResult;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <T> List<Map> convertListToMap(List<T> list) {
        ObjectMapper oMapper = new ObjectMapper();
        List<Map> listMap = new ArrayList();
        
        Map<String, Object> map = null;
        for (T element : list) {
        	map = oMapper.convertValue(element, Map.class);
        	listMap.add(map);
        }
        return listMap;
	}
}
