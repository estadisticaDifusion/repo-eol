package pe.gob.minedu.escale.common.velocity;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.ToolContext;
import org.apache.velocity.tools.ToolManager;

public class VelocityUtil {

	private static Logger logger = Logger.getLogger(VelocityUtil.class);

	private VelocityUtil() {

	}

	public static final String VELOCITY_TOOLBOX = "/META-INF/velocity-toolbox.xml";
	private static ToolContext toolContext = executeToolManager(VELOCITY_TOOLBOX);
	private static VelocityEngine velocityEngine = newInstanceVelocityEngine();

	public static String velocityTransform(Map<String, Object> inputDatamodel, String template) {

		Map<String, Map<String, Object>> modelMap = new HashMap<String, Map<String, Object>>();
		modelMap.put("context", inputDatamodel);

		VelocityContext velocityContext = new VelocityContext(modelMap, toolContext);

		StringWriter writer = new StringWriter();

		velocityEngine.evaluate(velocityContext, writer, "", template);

		return writer.toString();
	}

	private static VelocityEngine newInstanceVelocityEngine() {
		return new VelocityEngine();
	}

	public static ToolContext executeToolManager(String toolContextPath) {
		ToolManager toolManager = new ToolManager();
		File file;
		try {
			file = new File(VelocityUtil.class.getResource(toolContextPath).toURI());
			toolManager.configure(file.getAbsolutePath());
		} catch (Exception e) {
			logger.error("Error found in this method executeToolManager : " + e.getMessage());
		}

		return toolManager.createContext();
	}
}
