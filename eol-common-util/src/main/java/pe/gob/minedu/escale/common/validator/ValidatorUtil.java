package pe.gob.minedu.escale.common.validator;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

public final class ValidatorUtil {

	private ValidatorUtil() {

	}

	public static boolean isEmpty(Object o) {
		return isObjectEmpty(o);
	}

	public static boolean isNotEmpty(Object o) {
		return !isObjectEmpty(o);
	}

	public static boolean isEmpty(String s) {
		return ((s == null) || (s.trim().length() == 0));
	}

	public static <E> boolean isEmpty(Collection<E> c) {
		return ((c == null) || (c.size() == 0));
	}

	public static <K, E> boolean isEmpty(Map<K, E> m) {
		return ((m == null) || (m.size() == 0));
	}

	public static <E> boolean isEmpty(CharSequence c) {
		return ((c == null) || (c.length() == 0));
	}

	public static boolean isNotEmpty(String s) {
		return ((s != null) && (s.length() > 0));
	}

	public static <E> boolean isNotEmpty(Collection<E> c) {
		return ((c != null) && (c.size() > 0));
	}

	public static <E> boolean isNotEmpty(CharSequence c) {
		return ((c != null) && (c.length() > 0));
	}

	public static boolean isObjectEmpty(Object value) {
		if (value == null) {
			return true;
		} else if (value instanceof String) {
			return ValidatorUtil.isEmpty((String) value);
		} else if (value instanceof CharSequence) {
			return ValidatorUtil.isEmpty((CharSequence) value);
		} else if (value instanceof Collection || value instanceof Map) {
			return isCollectionEmpty(value);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private static boolean isCollectionEmpty(Object value) {
		if (value instanceof Collection) {
			return ValidatorUtil.isEmpty((Collection<? extends Object>) value);
		} else {
			return ValidatorUtil.isEmpty((Map<? extends Object, ? extends Object>) value);
		}

	}

	public static boolean isBigDecimal(String value) {
		try {
			new BigDecimal(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isInteger(String value) {
		try {
			Integer.valueOf(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isBoolean(String value) {
		if ("true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value)) {
			return true;
		}
		return false;
	}

	public static Boolean parseBoolean(String value) {
		return Boolean.valueOf(value);
	}

}
