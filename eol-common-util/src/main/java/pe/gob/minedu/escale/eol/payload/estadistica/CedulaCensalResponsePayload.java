package pe.gob.minedu.escale.eol.payload.estadistica;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.estadistica.EolEnvioPeriodoDTO;

public class CedulaCensalResponsePayload {
	private String dataResult;
	private List<EolEnvioPeriodoDTO> dataDetail;

	public String getDataResult() {
		return dataResult;
	}

	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}

	public List<EolEnvioPeriodoDTO> getDataDetail() {
		return dataDetail;
	}

	public void setDataDetail(List<EolEnvioPeriodoDTO> dataDetail) {
		this.dataDetail = dataDetail;
	}

}
