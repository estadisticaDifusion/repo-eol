package pe.gob.minedu.escale.eol.dto.entities;

public class TotalEstadisticaDTO {
	private Integer totAlumno;
	private Integer totDocente;
	private Integer totSeccion;

	public TotalEstadisticaDTO() {
		super();
	}

	public Integer getTotAlumno() {
		return totAlumno;
	}

	public void setTotAlumno(Integer totAlumno) {
		this.totAlumno = totAlumno;
	}

	public Integer getTotDocente() {
		return totDocente;
	}

	public void setTotDocente(Integer totDocente) {
		this.totDocente = totDocente;
	}

	public Integer getTotSeccion() {
		return totSeccion;
	}

	public void setTotSeccion(Integer totSeccion) {
		this.totSeccion = totSeccion;
	}

}
