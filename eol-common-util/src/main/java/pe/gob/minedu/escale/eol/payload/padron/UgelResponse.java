package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class UgelResponse extends ResponseGeneric {
	private UgelResponsePayload responsePayload;

	public UgelResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(UgelResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}

}
