package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class RegionResponse extends ResponseGeneric {
	
	private RegionResponsePayload responsePayload;

	public RegionResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(RegionResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}

}
