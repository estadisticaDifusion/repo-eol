/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.estadistica;

/**
 *
 * @author Owner
 */
public enum TipEvaluacionEnum {

    FINAL("F","F","FINAL"),
    RECUPERACION("R","R","RECUPERACION");
    

    private String cod;
    private String codTipo;
    private String descri;

    private TipEvaluacionEnum(String cod,String codTipo,String descri)
    { this.cod=cod;
      this.codTipo=codTipo;
      this.descri=descri;
    }

    public String getCod() {
        return cod;
    }


    public void setCod(String cod) {
        this.cod = cod;
    }

 
    public String getCodTipo() {
        return codTipo;
    }

    /**
     * @param codTipo the codTipo to set
     */
    public void setCodTipo(String codTipo) {
        this.codTipo = codTipo;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

    /**
     * @param descri the descri to set
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }

}
