package pe.gob.minedu.escale.eol.payload.padron;

public class InstitucionRequestEncrypt {

	private String nombreIE;
	private String codooii;
	private String codmod;
	private String codlocal;
	private String nombreCP;
	private String[] niveles;
	private String[] gestiones;
	private String ubigeo;
	private String[] estados;
	private String start;

	public InstitucionRequestEncrypt() {
		super();
	}

	public String getNombreIE() {
		return nombreIE;
	}

	public void setNombreIE(String nombreIE) {
		this.nombreIE = nombreIE;
	}

	public String getCodooii() {
		return codooii;
	}

	public void setCodooii(String codooii) {
		this.codooii = codooii;
	}

	public String getCodmod() {
		return codmod;
	}

	public void setCodmod(String codmod) {
		this.codmod = codmod;
	}

	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getNombreCP() {
		return nombreCP;
	}

	public void setNombreCP(String nombreCP) {
		this.nombreCP = nombreCP;
	}
//	public String getNiveles() {
//		return niveles;
//	}
//	public void setNiveles(String niveles) {
//		this.niveles = niveles;
//	}

	public String[] getNiveles() {
		return niveles;
	}

	public void setNiveles(String[] niveles) {
		this.niveles = niveles;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String[] getGestiones() {
		return gestiones;
	}

	public void setGestiones(String[] gestiones) {
		this.gestiones = gestiones;
	}

	public String[] getEstados() {
		return estados;
	}

	public void setEstados(String[] estados) {
		this.estados = estados;
	}

	@Override
	public String toString() {
		return "InstitucionRequestEncrypt [nombreIE=" + nombreIE + ", codooii=" + codooii + ", codmod=" + codmod
				+ ", codlocal=" + codlocal + ", nombreCP=" + nombreCP + ", niveles=" + niveles + ", gestiones="
				+ gestiones + ", ubigeo=" + ubigeo + ", estados=" + estados + ", start=" + start + "]";
	}

}
