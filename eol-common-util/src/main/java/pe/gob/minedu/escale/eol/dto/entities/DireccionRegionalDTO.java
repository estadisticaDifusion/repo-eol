package pe.gob.minedu.escale.eol.dto.entities;

public class DireccionRegionalDTO {
	private String id;
	private String nombreDre;
	private Double pointX;
	private Double pointY;
	private Integer zoom;

	public DireccionRegionalDTO() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombreDre() {
		return nombreDre;
	}

	public void setNombreDre(String nombreDre) {
		this.nombreDre = nombreDre;
	}

	public Double getPointX() {
		return pointX;
	}

	public void setPointX(Double pointX) {
		this.pointX = pointX;
	}

	public Double getPointY() {
		return pointY;
	}

	public void setPointY(Double pointY) {
		this.pointY = pointY;
	}

	public Integer getZoom() {
		return zoom;
	}

	public void setZoom(Integer zoom) {
		this.zoom = zoom;
	}

}
