package pe.gob.minedu.escale.eol.payload.padron;

public class UgelRequest {

	private String id;
	private Integer expandLevel;
	private String codigodre;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getExpandLevel() {
		return expandLevel;
	}

	public void setExpandLevel(Integer expandLevel) {
		this.expandLevel = expandLevel;
	}

	public String getCodigodre() {
		return codigodre;
	}

	public void setCodigodre(String codigodre) {
		this.codigodre = codigodre;
	}

}
