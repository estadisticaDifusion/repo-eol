package pe.gob.minedu.escale.eol.payload.padron;

public class CantidadResponsePayload {
	private String dataResult;
	private Long dataDetail;

	public String getDataResult() {
		return dataResult;
	}

	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}

	public Long getDataDetail() {
		return dataDetail;
	}

	public void setDataDetail(Long dataDetail) {
		this.dataDetail = dataDetail;
	}

}
