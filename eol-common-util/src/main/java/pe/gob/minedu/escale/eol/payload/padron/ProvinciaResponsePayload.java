package pe.gob.minedu.escale.eol.payload.padron;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.entities.ProvinciaDTO;;

public class ProvinciaResponsePayload {

	private String dataResult;
	private List<ProvinciaDTO> dataDetail;
	
	private String cipher;
	
	public String getDataResult() {
		return dataResult;
	}
	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}
	public List<ProvinciaDTO> getDataDetail() {
		return dataDetail;
	}
	public void setDataDetail(List<ProvinciaDTO> dataDetail) {
		this.dataDetail = dataDetail;
	}
	public String getCipher() {
		return cipher;
	}
	public void setCipher(String cipher) {
		this.cipher = cipher;
	}
	
	
	
	
}
