package pe.gob.minedu.escale.eol.payload.padron;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.entities.RegionDTO;
import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class RegionResponsePayload extends ResponseGeneric{

	private String dataResult;
	private List<RegionDTO> dataDetail;
	
	private String cipher;
	

	
	public String getDataResult() {
		return dataResult;
	}
	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}
	
	public List<RegionDTO> getDataDetail() {
		return dataDetail;
	}
	public void setDataDetail(List<RegionDTO> dataDetail) {
		this.dataDetail = dataDetail;
	}
	public String getCipher() {
		return cipher;
	}
	public void setCipher(String cipher) {
		this.cipher = cipher;
	}

	
	
	
}
