package pe.gob.minedu.escale.eol.payload.auth;

public class UpdateCredentialsResponsePayload {

	private String result;
	private String stateUpdate;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getStateUpdate() {
		return stateUpdate;
	}

	public void setStateUpdate(String stateUpdate) {
		this.stateUpdate = stateUpdate;
	}

}
