package pe.gob.minedu.escale.eol.dto.estadistica;

import java.util.Date;

public class EolEnvioActividadDTO {
	private Integer id;
	private String nombre;
	private String descripcion;
	private Date fechaInicio;
	private Date fechaTermino;
	private Date fechaTerminosigied;
	private Date fechaTerminohuelga;
	private Date fechaLimite;
	private Date fechaLimitesigied;
	private Date fechaLimitehuelga;
	private Integer estadoActividad;
	private Integer estadoActividadsigied;
	private Integer estadoActividadhuelga;
	private Boolean estadoFormato;

	private Boolean estadoConstancia;
	private Boolean estadoCobertura;
	private Boolean estadoSituacion;
	private Boolean estadoOmisos;
	private Boolean estadoSie;

	private String urlFormato;
	private String urlOmisos;
	private String urlCobertura;
	private String urlConstancia;
	private String urlSituacion;

	private String strFechaEnvio;
	private String strFechaInicio;
	private String strFechaTermino;
	private String strFechaTerminosigied;
	private String strFechaTerminohuelga;
	private String strFechaLimite;
	private String strFechaLimitesigied;
	private String strFechaLimitehuelga;

	private String nombrePeriodo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public Date getFechaTerminosigied() {
		return fechaTerminosigied;
	}

	public void setFechaTerminosigied(Date fechaTerminosigied) {
		this.fechaTerminosigied = fechaTerminosigied;
	}

	public Date getFechaTerminohuelga() {
		return fechaTerminohuelga;
	}

	public void setFechaTerminohuelga(Date fechaTerminohuelga) {
		this.fechaTerminohuelga = fechaTerminohuelga;
	}

	public Date getFechaLimite() {
		return fechaLimite;
	}

	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public Date getFechaLimitesigied() {
		return fechaLimitesigied;
	}

	public void setFechaLimitesigied(Date fechaLimitesigied) {
		this.fechaLimitesigied = fechaLimitesigied;
	}

	public Date getFechaLimitehuelga() {
		return fechaLimitehuelga;
	}

	public void setFechaLimitehuelga(Date fechaLimitehuelga) {
		this.fechaLimitehuelga = fechaLimitehuelga;
	}

	public Integer getEstadoActividad() {
		return estadoActividad;
	}

	public void setEstadoActividad(Integer estadoActividad) {
		this.estadoActividad = estadoActividad;
	}

	public Integer getEstadoActividadsigied() {
		return estadoActividadsigied;
	}

	public void setEstadoActividadsigied(Integer estadoActividadsigied) {
		this.estadoActividadsigied = estadoActividadsigied;
	}

	public Integer getEstadoActividadhuelga() {
		return estadoActividadhuelga;
	}

	public void setEstadoActividadhuelga(Integer estadoActividadhuelga) {
		this.estadoActividadhuelga = estadoActividadhuelga;
	}

	public Boolean getEstadoFormato() {
		return estadoFormato;
	}

	public void setEstadoFormato(Boolean estadoFormato) {
		this.estadoFormato = estadoFormato;
	}

	public Boolean getEstadoConstancia() {
		return estadoConstancia;
	}

	public void setEstadoConstancia(Boolean estadoConstancia) {
		this.estadoConstancia = estadoConstancia;
	}

	public Boolean getEstadoCobertura() {
		return estadoCobertura;
	}

	public void setEstadoCobertura(Boolean estadoCobertura) {
		this.estadoCobertura = estadoCobertura;
	}

	public Boolean getEstadoSituacion() {
		return estadoSituacion;
	}

	public void setEstadoSituacion(Boolean estadoSituacion) {
		this.estadoSituacion = estadoSituacion;
	}

	public Boolean getEstadoOmisos() {
		return estadoOmisos;
	}

	public void setEstadoOmisos(Boolean estadoOmisos) {
		this.estadoOmisos = estadoOmisos;
	}

	public Boolean getEstadoSie() {
		return estadoSie;
	}

	public void setEstadoSie(Boolean estadoSie) {
		this.estadoSie = estadoSie;
	}

	public String getUrlFormato() {
		return urlFormato;
	}

	public void setUrlFormato(String urlFormato) {
		this.urlFormato = urlFormato;
	}

	public String getUrlOmisos() {
		return urlOmisos;
	}

	public void setUrlOmisos(String urlOmisos) {
		this.urlOmisos = urlOmisos;
	}

	public String getUrlCobertura() {
		return urlCobertura;
	}

	public void setUrlCobertura(String urlCobertura) {
		this.urlCobertura = urlCobertura;
	}

	public String getUrlConstancia() {
		return urlConstancia;
	}

	public void setUrlConstancia(String urlConstancia) {
		this.urlConstancia = urlConstancia;
	}

	public String getUrlSituacion() {
		return urlSituacion;
	}

	public void setUrlSituacion(String urlSituacion) {
		this.urlSituacion = urlSituacion;
	}

	public String getStrFechaEnvio() {
		return strFechaEnvio;
	}

	public void setStrFechaEnvio(String strFechaEnvio) {
		this.strFechaEnvio = strFechaEnvio;
	}

	public String getStrFechaInicio() {
		return strFechaInicio;
	}

	public void setStrFechaInicio(String strFechaInicio) {
		this.strFechaInicio = strFechaInicio;
	}

	public String getStrFechaTermino() {
		return strFechaTermino;
	}

	public void setStrFechaTermino(String strFechaTermino) {
		this.strFechaTermino = strFechaTermino;
	}

	public String getStrFechaTerminosigied() {
		return strFechaTerminosigied;
	}

	public void setStrFechaTerminosigied(String strFechaTerminosigied) {
		this.strFechaTerminosigied = strFechaTerminosigied;
	}

	public String getStrFechaTerminohuelga() {
		return strFechaTerminohuelga;
	}

	public void setStrFechaTerminohuelga(String strFechaTerminohuelga) {
		this.strFechaTerminohuelga = strFechaTerminohuelga;
	}

	public String getStrFechaLimite() {
		return strFechaLimite;
	}

	public void setStrFechaLimite(String strFechaLimite) {
		this.strFechaLimite = strFechaLimite;
	}

	public String getStrFechaLimitesigied() {
		return strFechaLimitesigied;
	}

	public void setStrFechaLimitesigied(String strFechaLimitesigied) {
		this.strFechaLimitesigied = strFechaLimitesigied;
	}

	public String getStrFechaLimitehuelga() {
		return strFechaLimitehuelga;
	}

	public void setStrFechaLimitehuelga(String strFechaLimitehuelga) {
		this.strFechaLimitehuelga = strFechaLimitehuelga;
	}

	public String getNombrePeriodo() {
		return nombrePeriodo;
	}

	public void setNombrePeriodo(String nombrePeriodo) {
		this.nombrePeriodo = nombrePeriodo;
	}

}
