/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.admin;

/**
 *
 * @author JMATAMOROS  
 */
public enum EstadoActividadEnum {
    SIN_INICIAR(1),
    INICIADO(2),
    SUSPENDIDO(3),
    EXTENPORANEO(4),
    FINALIZADO(5);

    private int cod;

    EstadoActividadEnum(int cod)
    {   this.cod=cod;                
    }

    /**
     * @return the cod
     */
    public int getCod() {
        return cod;
    }

    /**
     * @param cod the cod to set
     */
    public void setCod(int cod) {
        this.cod = cod;
    }

    @Override
    public String toString(){
        return new Integer(cod).toString();
    }

}
