package pe.gob.minedu.escale.eol.payload.padron;

import java.util.List;

import pe.gob.minedu.escale.eol.dto.entities.DistritoDTO;

public class DistritoResponsePayload {

	private String dataResult;
	private List<DistritoDTO> dataDetail;
	
	private String cipher;
	
	public String getDataResult() {
		return dataResult;
	}
	public void setDataResult(String dataResult) {
		this.dataResult = dataResult;
	}
	public List<DistritoDTO> getDataDetail() {
		return dataDetail;
	}
	public void setDataDetail(List<DistritoDTO> dataDetail) {
		this.dataDetail = dataDetail;
	}
	public String getCipher() {
		return cipher;
	}
	public void setCipher(String cipher) {
		this.cipher = cipher;
	}
	
	
	
}
