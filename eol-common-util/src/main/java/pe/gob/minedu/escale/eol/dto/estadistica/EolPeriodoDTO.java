package pe.gob.minedu.escale.eol.dto.estadistica;

import java.io.Serializable;

public class EolPeriodoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9129139685179532194L;
	
	private Integer id;
	private String anio;
	private String descripcion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
