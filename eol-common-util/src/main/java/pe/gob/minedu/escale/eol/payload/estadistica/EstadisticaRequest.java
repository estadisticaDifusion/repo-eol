package pe.gob.minedu.escale.eol.payload.estadistica;

public class EstadisticaRequest {
	private String nivel;
	private String anexo;
	private String codmod;
	private String codlocal;
	private String codinst;
	private String tipo;
	private Integer anioInicio;
	private Integer anioFinal;

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getCodmod() {
		return codmod;
	}

	public void setCodmod(String codmod) {
		this.codmod = codmod;
	}

	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getCodinst() {
		return codinst;
	}

	public void setCodinst(String codinst) {
		this.codinst = codinst;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getAnioInicio() {
		return anioInicio;
	}

	public void setAnioInicio(Integer anioInicio) {
		this.anioInicio = anioInicio;
	}

	public Integer getAnioFinal() {
		return anioFinal;
	}

	public void setAnioFinal(Integer anioFinal) {
		this.anioFinal = anioFinal;
	}

}
