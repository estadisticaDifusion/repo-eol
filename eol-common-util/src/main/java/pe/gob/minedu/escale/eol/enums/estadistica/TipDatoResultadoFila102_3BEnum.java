/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.estadistica;

/**
 *
 * @author Owner
 */
public enum TipDatoResultadoFila102_3BEnum {

    TOTAL("1","","TOTAL"),
    SITUACION_ECONOMICA("2","EC","SITUACION ECONOMICA"),
    APOYO_EN_LAB_AGROPECUARIAS("3","AG","APOYO EN LAB AGROPECUARIAS"),
    TRABAJO_INFANTIL("4","TR","TRABAJO_INFANTIL"),
    VIOLENCIA("5","VI","VIOLENCIA"),
    ENFERMEDAD("6","EN","ENFERMEDAD"),
    ADICCION("7","AD","ADICCION"),
    OTROS1("8","OT","OTROS"),
    OTROS2("9","","OTROS"),
    OTROS3("10","","OTROS");
    
    private String cod;
    private String codTipo;
    private String descri;

    TipDatoResultadoFila102_3BEnum(String cod,String codTipo,String descri)
    { this.cod=cod;
      this.codTipo=codTipo;
      this.descri=descri;

    }

    /**
     * @return the cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @return the codTipo
     */
    public String getCodTipo() {
        return codTipo;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }




}
