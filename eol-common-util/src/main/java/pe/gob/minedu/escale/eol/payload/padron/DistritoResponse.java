package pe.gob.minedu.escale.eol.payload.padron;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class DistritoResponse extends ResponseGeneric {

	private DistritoResponsePayload responsePayload;

	public DistritoResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(DistritoResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}
}
