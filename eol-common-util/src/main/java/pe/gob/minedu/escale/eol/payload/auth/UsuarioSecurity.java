package pe.gob.minedu.escale.eol.payload.auth;

public class UsuarioSecurity {
	
	private String filterOne;
	private String filterTwo;
	
	public UsuarioSecurity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFilterOne() {
		return filterOne;
	}

	public void setFilterOne(String filterOne) {
		this.filterOne = filterOne;
	}

	public String getFilterTwo() {
		return filterTwo;
	}

	public void setFilterTwo(String filterTwo) {
		this.filterTwo = filterTwo;
	}
	
	
	
}
