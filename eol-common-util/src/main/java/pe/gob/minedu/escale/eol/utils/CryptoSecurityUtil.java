package pe.gob.minedu.escale.eol.utils;

import java.nio.charset.Charset;
import java.security.DigestException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

public final class CryptoSecurityUtil {
	
	private static Logger logger = Logger.getLogger(CryptoSecurityUtil.class);
	
	public static final Charset UTF_8 = Charset.forName("UTF-8");
	
	public CryptoSecurityUtil () {}
	
	public static String getDecryptedSecurity(final String parameter, final String secret) {
//		String secret = "eol2018";

		MessageDigest md5;
		Cipher aesCBCpass;
		final byte[][] keyAndIVpass;
		byte[] decryptedDatapass;

		byte[] cipherDatapass = Base64Util.decode(parameter);
		byte[] saltDatapass = Arrays.copyOfRange(cipherDatapass, 8, 16);

		try {
			md5 = MessageDigest.getInstance("MD5");

			keyAndIVpass = generateKeyAndIV(32, 16, 1, saltDatapass, secret.getBytes(UTF_8), md5);
			SecretKeySpec keypass = new SecretKeySpec(keyAndIVpass[0], "AES");
			IvParameterSpec ivpass = new IvParameterSpec(keyAndIVpass[1]);

			byte[] encryptedpass = Arrays.copyOfRange(cipherDatapass, 16, cipherDatapass.length);

			aesCBCpass = Cipher.getInstance("AES/CBC/PKCS5Padding");
			aesCBCpass.init(Cipher.DECRYPT_MODE, keypass, ivpass);

			decryptedDatapass = aesCBCpass.doFinal(encryptedpass);
			String decryptedParameter = new String(decryptedDatapass, UTF_8);
			logger.info("Decrypted Parameter :" + decryptedParameter);
			
			return decryptedParameter;
		} catch (NoSuchAlgorithmException ex) {
			logger.error("Error NoSuchAlgorithmException ");
		} catch (NoSuchPaddingException ex) {
			logger.error("Error NoSuchPaddingException ");
		} catch (InvalidKeyException ex) {
			logger.error("Error InvalidKeyException ");
		} catch (InvalidAlgorithmParameterException ex) {
			logger.error("Error InvalidAlgorithmParameterException ");
		} catch (IllegalBlockSizeException ex) {
			logger.error("Error IllegalBlockSizeException ");
		} catch (BadPaddingException ex) {
			logger.error("Error BadPaddingException ");
		} catch (Exception ex) {
			logger.error("Error Exception ");
		}
		
		return null;
	}
	
	private static byte[][] generateKeyAndIV(int keyLength, int ivLength, int iterations, byte[] salt, byte[] password,
			MessageDigest md) {

		int digestLength = md.getDigestLength();
		int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
		byte[] generatedData = new byte[requiredLength];
		int generatedLength = 0;

		try {
			md.reset();
			// Repeat process until sufficient data has been generated
			while (generatedLength < keyLength + ivLength) {

				// Digest data (last digest if available, password data, salt if available)
				if (generatedLength > 0) {
					md.update(generatedData, generatedLength - digestLength, digestLength);
				}
				md.update(password);
				if (salt != null) {
					md.update(salt, 0, 8);
				}
				md.digest(generatedData, generatedLength, digestLength);

				// additional rounds
				for (int i = 1; i < iterations; i++) {
					md.update(generatedData, generatedLength, digestLength);
					md.digest(generatedData, generatedLength, digestLength);
				}

				generatedLength += digestLength;
			}
			// Copy keyuser and IV into separate byte arrays
			byte[][] result = new byte[2][];
			result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
			if (ivLength > 0) {
				result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);
			}

			return result;

		} catch (DigestException e) {
			throw new RuntimeException(e);

		} finally {
			// Clean out temporary data
			Arrays.fill(generatedData, (byte) 0);
		}
	}
	
}
