package pe.gob.minedu.escale.eol.payload.padron;

public class EncryptDTO {

	private String salt;
	private String iv;
	private String cipher;
	private String estado;
	
	public EncryptDTO() {
		super();
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getCipher() {
		return cipher;
	}

	public void setCipher(String cipher) {
		this.cipher = cipher;
	}

	
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "EncryptDTO [salt=" + salt + ", iv=" + iv + ", cipher=" + cipher + "]";
	}
	
	
	
	
}
