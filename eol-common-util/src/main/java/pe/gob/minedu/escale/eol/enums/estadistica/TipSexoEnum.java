/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.estadistica;

/**
 *
 * @author Owner
 */
public enum TipSexoEnum {

    HOBRES("1","H","HOMBRES"),
    MUJERES("2","M","MUJERES"),
    MIXTO("3","","MIXTO");

    private String cod;
    private String codTipo;
    private String descri;

    private TipSexoEnum(String cod,String codTipo,String descri)
    { this.cod=cod;
      this.codTipo=codTipo;
      this.descri=descri;
    }

    /**
     * @return the cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @param cod the cod to set
     */
    public void setCod(String cod) {
        this.cod = cod;
    }

    /**
     * @return the codTipo
     */
    public String getCodTipo() {
        return codTipo;
    }

    /**
     * @param codTipo the codTipo to set
     */
    public void setCodTipo(String codTipo) {
        this.codTipo = codTipo;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

    /**
     * @param descri the descri to set
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }

}
