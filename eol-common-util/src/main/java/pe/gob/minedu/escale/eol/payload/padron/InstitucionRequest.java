package pe.gob.minedu.escale.eol.payload.padron;

import java.util.List;

public class InstitucionRequest {

	private String codmod;
	private String codlocal;
	private String ubigeo;
	private String codooii;
	private String nombreCP;
	private String nombreIE;
	private String disVrae;
	private String disJuntos;
	private String disCrecer;
	private String disNinguno;
	private String matIndigena;
	private List<String> niveles;
	private List<String> gestiones;
	private List<String> areas;
	private String estado = "1";
	private List<String> tipoices;
	private List<String> formas;
	private List<String> estados;
	private String progarti;
	private String progise;
	private Integer start;
	private Integer max;
	private String orderBy;
	private Integer expandLevel;
	private String anexo;
	private String anio;
	private String nivel;

	public String getCodmod() {
		return codmod;
	}

	public void setCodmod(String codmod) {
		this.codmod = codmod;
	}

	public String getCodlocal() {
		return codlocal;
	}

	public void setCodlocal(String codlocal) {
		this.codlocal = codlocal;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getCodooii() {
		return codooii;
	}

	public void setCodooii(String codooii) {
		this.codooii = codooii;
	}

	public String getNombreCP() {
		return nombreCP;
	}

	public void setNombreCP(String nombreCP) {
		this.nombreCP = nombreCP;
	}

	public String getNombreIE() {
		return nombreIE;
	}

	public void setNombreIE(String nombreIE) {
		this.nombreIE = nombreIE;
	}

	public String getDisVrae() {
		return disVrae;
	}

	public void setDisVrae(String disVrae) {
		this.disVrae = disVrae;
	}

	public String getDisJuntos() {
		return disJuntos;
	}

	public void setDisJuntos(String disJuntos) {
		this.disJuntos = disJuntos;
	}

	public String getDisCrecer() {
		return disCrecer;
	}

	public void setDisCrecer(String disCrecer) {
		this.disCrecer = disCrecer;
	}

	public String getDisNinguno() {
		return disNinguno;
	}

	public void setDisNinguno(String disNinguno) {
		this.disNinguno = disNinguno;
	}

	public String getMatIndigena() {
		return matIndigena;
	}

	public void setMatIndigena(String matIndigena) {
		this.matIndigena = matIndigena;
	}

	public List<String> getNiveles() {
		return niveles;
	}

	public void setNiveles(List<String> niveles) {
		this.niveles = niveles;
	}

	public List<String> getGestiones() {
		return gestiones;
	}

	public void setGestiones(List<String> gestiones) {
		this.gestiones = gestiones;
	}

	public List<String> getAreas() {
		return areas;
	}

	public void setAreas(List<String> areas) {
		this.areas = areas;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<String> getTipoices() {
		return tipoices;
	}

	public void setTipoices(List<String> tipoices) {
		this.tipoices = tipoices;
	}

	public List<String> getFormas() {
		return formas;
	}

	public void setFormas(List<String> formas) {
		this.formas = formas;
	}

	public List<String> getEstados() {
		return estados;
	}

	public void setEstados(List<String> estados) {
		this.estados = estados;
	}

	public String getProgarti() {
		return progarti;
	}

	public void setProgarti(String progarti) {
		this.progarti = progarti;
	}

	public String getProgise() {
		return progise;
	}

	public void setProgise(String progise) {
		this.progise = progise;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Integer getExpandLevel() {
		return expandLevel;
	}

	public void setExpandLevel(Integer expandLevel) {
		this.expandLevel = expandLevel;
	}
	
	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	@Override
	public String toString() {
		return "InstitucionRequest [codmod=" + codmod + ", codlocal=" + codlocal + ", ubigeo=" + ubigeo + ", codooii="
				+ codooii + ", nombreCP=" + nombreCP + ", nombreIE=" + nombreIE + ", disVrae=" + disVrae
				+ ", disJuntos=" + disJuntos + ", disCrecer=" + disCrecer + ", disNinguno=" + disNinguno
				+ ", matIndigena=" + matIndigena + ", niveles=" + niveles + ", gestiones=" + gestiones + ", areas="
				+ areas + ", estado=" + estado + ", tipoices=" + tipoices + ", formas=" + formas + ", estados="
				+ estados + ", progarti=" + progarti + ", progise=" + progise + ", start=" + start + ", max=" + max
				+ ", orderBy=" + orderBy + ", expandLevel=" + expandLevel + "]";
	}

}
