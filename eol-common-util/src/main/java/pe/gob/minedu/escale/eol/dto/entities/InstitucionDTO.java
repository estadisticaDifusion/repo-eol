package pe.gob.minedu.escale.eol.dto.entities;

import java.util.List;

public class InstitucionDTO extends InstitucionBaseDTO {
	private String email;
	private String pagweb;
	private UgelDTO ugel;
	private IgelDTO igel;
	private String tipoigel;
	private String codccpp;
	private FormaDTO forma;
	private AreaDTO area;
	private SituacionDirectorDTO situacionDirector;
	private EstadoDTO estado;
	private String mcenso;
	private TurnoDTO turno;
	private String tipoprog;
	private GeneroDTO genero;
	private String progdist;
	private String progarti;
	private String comenta;
	private String fechareg;
	private String fecharet;
	private AreaDTO areaSig;
	private String registro;
	private String metodo;
	private String contjesc;
	private String fecharea;
	private String codcp_inei;
	private Integer alt_cp;
	private String fte_cp;
	private String tipoIce;
	private String codCord;
	private String clongIE;
	private String clatIE;
	private String referencia;
	private String fteArea;
	private String clongCp;
	private String clatCp;
	private String cpEtnico;
	private String lenEtnica;
	private Float nlatCp;
	private Float nlongCp;
	private Float nlatIE;
	private Float nlongIE;
	private Integer nzoom;
	private String localidad;
	private CaracteristicaDTO caracteristica;
	private GestionDTO gestion;
	private DisVraeDTO disVrae;
	private DisCrecerDTO disCrecer;
	private DisJuntosDTO disJuntos;
	private String dre;
	private String progise;
	private String codinst;
	private String sienvio;
	private TotalEstadisticaDTO totalEstadistica;
	private List<EstadisticaDTO> estadistica;
	private String imputado;

	public InstitucionDTO() {

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPagweb() {
		return pagweb;
	}

	public void setPagweb(String pagweb) {
		this.pagweb = pagweb;
	}

	public UgelDTO getUgel() {
		return ugel;
	}

	public void setUgel(UgelDTO ugel) {
		this.ugel = ugel;
	}

	public IgelDTO getIgel() {
		return igel;
	}

	public void setIgel(IgelDTO igel) {
		this.igel = igel;
	}

	public String getTipoigel() {
		return tipoigel;
	}

	public void setTipoigel(String tipoigel) {
		this.tipoigel = tipoigel;
	}

	public String getCodccpp() {
		return codccpp;
	}

	public void setCodccpp(String codccpp) {
		this.codccpp = codccpp;
	}

	public FormaDTO getForma() {
		return forma;
	}

	public void setForma(FormaDTO forma) {
		this.forma = forma;
	}

	public AreaDTO getArea() {
		return area;
	}

	public void setArea(AreaDTO area) {
		this.area = area;
	}

	public SituacionDirectorDTO getSituacionDirector() {
		return situacionDirector;
	}

	public void setSituacionDirector(SituacionDirectorDTO situacionDirector) {
		this.situacionDirector = situacionDirector;
	}

	public EstadoDTO getEstado() {
		return estado;
	}

	public void setEstado(EstadoDTO estado) {
		this.estado = estado;
	}

	public String getMcenso() {
		return mcenso;
	}

	public void setMcenso(String mcenso) {
		this.mcenso = mcenso;
	}

	public TurnoDTO getTurno() {
		return turno;
	}

	public void setTurno(TurnoDTO turno) {
		this.turno = turno;
	}

	public String getTipoprog() {
		return tipoprog;
	}

	public void setTipoprog(String tipoprog) {
		this.tipoprog = tipoprog;
	}

	public GeneroDTO getGenero() {
		return genero;
	}

	public void setGenero(GeneroDTO genero) {
		this.genero = genero;
	}

	public String getProgdist() {
		return progdist;
	}

	public void setProgdist(String progdist) {
		this.progdist = progdist;
	}

	public String getProgarti() {
		return progarti;
	}

	public void setProgarti(String progarti) {
		this.progarti = progarti;
	}

	public String getComenta() {
		return comenta;
	}

	public void setComenta(String comenta) {
		this.comenta = comenta;
	}

	public String getFechareg() {
		return fechareg;
	}

	public void setFechareg(String fechareg) {
		this.fechareg = fechareg;
	}

	public String getFecharet() {
		return fecharet;
	}

	public void setFecharet(String fecharet) {
		this.fecharet = fecharet;
	}

	public AreaDTO getAreaSig() {
		return areaSig;
	}

	public void setAreaSig(AreaDTO areaSig) {
		this.areaSig = areaSig;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getContjesc() {
		return contjesc;
	}

	public void setContjesc(String contjesc) {
		this.contjesc = contjesc;
	}

	public String getFecharea() {
		return fecharea;
	}

	public void setFecharea(String fecharea) {
		this.fecharea = fecharea;
	}

	public String getCodcp_inei() {
		return codcp_inei;
	}

	public void setCodcp_inei(String codcp_inei) {
		this.codcp_inei = codcp_inei;
	}

	public Integer getAlt_cp() {
		return alt_cp;
	}

	public void setAlt_cp(Integer alt_cp) {
		this.alt_cp = alt_cp;
	}

	public String getFte_cp() {
		return fte_cp;
	}

	public void setFte_cp(String fte_cp) {
		this.fte_cp = fte_cp;
	}

	public String getTipoIce() {
		return tipoIce;
	}

	public void setTipoIce(String tipoIce) {
		this.tipoIce = tipoIce;
	}

	public String getCodCord() {
		return codCord;
	}

	public void setCodCord(String codCord) {
		this.codCord = codCord;
	}

	public String getClongIE() {
		return clongIE;
	}

	public void setClongIE(String clongIE) {
		this.clongIE = clongIE;
	}

	public String getClatIE() {
		return clatIE;
	}

	public void setClatIE(String clatIE) {
		this.clatIE = clatIE;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFteArea() {
		return fteArea;
	}

	public void setFteArea(String fteArea) {
		this.fteArea = fteArea;
	}

	public String getClongCp() {
		return clongCp;
	}

	public void setClongCp(String clongCp) {
		this.clongCp = clongCp;
	}

	public String getClatCp() {
		return clatCp;
	}

	public void setClatCp(String clatCp) {
		this.clatCp = clatCp;
	}

	public String getCpEtnico() {
		return cpEtnico;
	}

	public void setCpEtnico(String cpEtnico) {
		this.cpEtnico = cpEtnico;
	}

	public String getLenEtnica() {
		return lenEtnica;
	}

	public void setLenEtnica(String lenEtnica) {
		this.lenEtnica = lenEtnica;
	}

	public Float getNlatCp() {
		return nlatCp;
	}

	public void setNlatCp(Float nlatCp) {
		this.nlatCp = nlatCp;
	}

	public Float getNlongCp() {
		return nlongCp;
	}

	public void setNlongCp(Float nlongCp) {
		this.nlongCp = nlongCp;
	}

	public Float getNlatIE() {
		return nlatIE;
	}

	public void setNlatIE(Float nlatIE) {
		this.nlatIE = nlatIE;
	}

	public Float getNlongIE() {
		return nlongIE;
	}

	public void setNlongIE(Float nlongIE) {
		this.nlongIE = nlongIE;
	}

	public Integer getNzoom() {
		return nzoom;
	}

	public void setNzoom(Integer nzoom) {
		this.nzoom = nzoom;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public CaracteristicaDTO getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(CaracteristicaDTO caracteristica) {
		this.caracteristica = caracteristica;
	}

	public GestionDTO getGestion() {
		return gestion;
	}

	public void setGestion(GestionDTO gestion) {
		this.gestion = gestion;
	}

	public DisVraeDTO getDisVrae() {
		return disVrae;
	}

	public void setDisVrae(DisVraeDTO disVrae) {
		this.disVrae = disVrae;
	}

	public DisCrecerDTO getDisCrecer() {
		return disCrecer;
	}

	public void setDisCrecer(DisCrecerDTO disCrecer) {
		this.disCrecer = disCrecer;
	}

	public DisJuntosDTO getDisJuntos() {
		return disJuntos;
	}

	public void setDisJuntos(DisJuntosDTO disJuntos) {
		this.disJuntos = disJuntos;
	}

	public String getDre() {
		return dre;
	}

	public void setDre(String dre) {
		this.dre = dre;
	}

	public String getProgise() {
		return progise;
	}

	public void setProgise(String progise) {
		this.progise = progise;
	}

	public String getCodinst() {
		return codinst;
	}

	public void setCodinst(String codinst) {
		this.codinst = codinst;
	}

	public String getSienvio() {
		return sienvio;
	}

	public void setSienvio(String sienvio) {
		this.sienvio = sienvio;
	}

	public TotalEstadisticaDTO getTotalEstadistica() {
		return totalEstadistica;
	}

	public void setTotalEstadistica(TotalEstadisticaDTO totalEstadistica) {
		this.totalEstadistica = totalEstadistica;
	}

	public List<EstadisticaDTO> getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(List<EstadisticaDTO> estadistica) {
		this.estadistica = estadistica;
	}

	public String getImputado() {
		return imputado;
	}

	public void setImputado(String imputado) {
		this.imputado = imputado;
	}

	@Override
	public String toString() {
		return "InstitucionDTO [email=" + email + ", pagweb=" + pagweb + ", ugel=" + ugel + ", igel=" + igel
				+ ", tipoigel=" + tipoigel + ", codccpp=" + codccpp + ", forma=" + forma + ", area=" + area
				+ ", situacionDirector=" + situacionDirector + ", estado=" + estado + ", mcenso=" + mcenso + ", turno="
				+ turno + ", tipoprog=" + tipoprog + ", genero=" + genero + ", progdist=" + progdist + ", progarti="
				+ progarti + ", comenta=" + comenta + ", fechareg=" + fechareg + ", fecharet=" + fecharet + ", areaSig="
				+ areaSig + ", registro=" + registro + ", metodo=" + metodo + ", contjesc=" + contjesc + ", fecharea="
				+ fecharea + ", codcp_inei=" + codcp_inei + ", alt_cp=" + alt_cp + ", fte_cp=" + fte_cp + ", tipoIce="
				+ tipoIce + ", codCord=" + codCord + ", clongIE=" + clongIE + ", clatIE=" + clatIE + ", referencia="
				+ referencia + ", fteArea=" + fteArea + ", clongCp=" + clongCp + ", clatCp=" + clatCp + ", cpEtnico="
				+ cpEtnico + ", lenEtnica=" + lenEtnica + ", nlatCp=" + nlatCp + ", nlongCp=" + nlongCp + ", nlatIE="
				+ nlatIE + ", nlongIE=" + nlongIE + ", nzoom=" + nzoom + ", localidad=" + localidad
				+ ", caracteristica=" + caracteristica + ", gestion=" + gestion + ", disVrae=" + disVrae
				+ ", disCrecer=" + disCrecer + ", disJuntos=" + disJuntos + ", dre=" + dre + ", progise=" + progise
				+ ", codinst=" + codinst + ", sienvio=" + sienvio + ", totalEstadistica=" + totalEstadistica
				+ ", estadistica=" + estadistica + ", imputado=" + imputado + "]";
	}
	
	

}
