package pe.gob.minedu.escale.eol.dto.entities;


public class EstadisticaDTO {
	
	private String codMod;
    private String anexo;
    private String tipoReg;
    private int talumno;
    private int tseccion;
    private int tdocente;
    private NivelModalidadDTO nivelModalidad;
    private String imputado;
    private int totalh;
    private int totalm;
    private int matr1h;
    private int matr2h;
    private int matr3h;
    private int matr4h;
    private int matr5h;
    private int matr6h;
    private int matr7h;
    private int matr8h;
    private int matr9h;
    private int matr10h;
    private int matr1m;
    private int matr2m;
    private int matr3m;
    private int matr4m;
    private int matr5m;
    private int matr6m;
    private int matr7m;
    private int matr8m;
    private int matr9m;
    private int matr10m;
    
	public String getCodMod() {
		return codMod;
	}
	public void setCodMod(String codMod) {
		this.codMod = codMod;
	}
	public String getAnexo() {
		return anexo;
	}
	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}
	public String getTipoReg() {
		return tipoReg;
	}
	public void setTipoReg(String tipoReg) {
		this.tipoReg = tipoReg;
	}
	public int getTalumno() {
		return talumno;
	}
	public void setTalumno(int talumno) {
		this.talumno = talumno;
	}
	public int getTseccion() {
		return tseccion;
	}
	public void setTseccion(int tseccion) {
		this.tseccion = tseccion;
	}
	public int getTdocente() {
		return tdocente;
	}
	public void setTdocente(int tdocente) {
		this.tdocente = tdocente;
	}
	public NivelModalidadDTO getNivelModalidad() {
		return nivelModalidad;
	}
	public void setNivelModalidad(NivelModalidadDTO nivelModalidad) {
		this.nivelModalidad = nivelModalidad;
	}
	public String getImputado() {
		return imputado;
	}
	public void setImputado(String imputado) {
		this.imputado = imputado;
	}
	public int getTotalh() {
		return totalh;
	}
	public void setTotalh(int totalh) {
		this.totalh = totalh;
	}
	public int getTotalm() {
		return totalm;
	}
	public void setTotalm(int totalm) {
		this.totalm = totalm;
	}
	public int getMatr1h() {
		return matr1h;
	}
	public void setMatr1h(int matr1h) {
		this.matr1h = matr1h;
	}
	public int getMatr2h() {
		return matr2h;
	}
	public void setMatr2h(int matr2h) {
		this.matr2h = matr2h;
	}
	public int getMatr3h() {
		return matr3h;
	}
	public void setMatr3h(int matr3h) {
		this.matr3h = matr3h;
	}
	public int getMatr4h() {
		return matr4h;
	}
	public void setMatr4h(int matr4h) {
		this.matr4h = matr4h;
	}
	public int getMatr5h() {
		return matr5h;
	}
	public void setMatr5h(int matr5h) {
		this.matr5h = matr5h;
	}
	public int getMatr6h() {
		return matr6h;
	}
	public void setMatr6h(int matr6h) {
		this.matr6h = matr6h;
	}
	public int getMatr7h() {
		return matr7h;
	}
	public void setMatr7h(int matr7h) {
		this.matr7h = matr7h;
	}
	public int getMatr8h() {
		return matr8h;
	}
	public void setMatr8h(int matr8h) {
		this.matr8h = matr8h;
	}
	public int getMatr9h() {
		return matr9h;
	}
	public void setMatr9h(int matr9h) {
		this.matr9h = matr9h;
	}
	public int getMatr10h() {
		return matr10h;
	}
	public void setMatr10h(int matr10h) {
		this.matr10h = matr10h;
	}
	public int getMatr1m() {
		return matr1m;
	}
	public void setMatr1m(int matr1m) {
		this.matr1m = matr1m;
	}
	public int getMatr2m() {
		return matr2m;
	}
	public void setMatr2m(int matr2m) {
		this.matr2m = matr2m;
	}
	public int getMatr3m() {
		return matr3m;
	}
	public void setMatr3m(int matr3m) {
		this.matr3m = matr3m;
	}
	public int getMatr4m() {
		return matr4m;
	}
	public void setMatr4m(int matr4m) {
		this.matr4m = matr4m;
	}
	public int getMatr5m() {
		return matr5m;
	}
	public void setMatr5m(int matr5m) {
		this.matr5m = matr5m;
	}
	public int getMatr6m() {
		return matr6m;
	}
	public void setMatr6m(int matr6m) {
		this.matr6m = matr6m;
	}
	public int getMatr7m() {
		return matr7m;
	}
	public void setMatr7m(int matr7m) {
		this.matr7m = matr7m;
	}
	public int getMatr8m() {
		return matr8m;
	}
	public void setMatr8m(int matr8m) {
		this.matr8m = matr8m;
	}
	public int getMatr9m() {
		return matr9m;
	}
	public void setMatr9m(int matr9m) {
		this.matr9m = matr9m;
	}
	public int getMatr10m() {
		return matr10m;
	}
	public void setMatr10m(int matr10m) {
		this.matr10m = matr10m;
	}
    
    

}
