/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.enums.estadistica;

/**
 *
 * @author Owner
 */
public enum TipNivelModalidadEnum {
    INICIAL_CUNA("A1","","INICIAL CUNA"),
    INICIAL_JARDIN("A2","INI","INICIAL JARDIN"),
    INICIAL_CUNA_Y_JARDIN("A3","","INICIAL CUNA Y JARDIN"),
    ARTICULACION("A4","","ARTICULACION"),
    PRIMARIA_DE_MENORES("B0","PRI","PRIMARIA DE MENORES"),
    PRIMARIA_DE_ADULTOS("C0","","PRIMARIA DE ADULTOS"),
    SECUNDARIA_DE_MENORES("F0","SEC","SECUNDARIA DE MENORES"),
    SECUNDARIA_DE_ADULTOS("G0","","SECUNDARIA DE ADULTOS"),
    FORMACION_MAGISTERIAL_ISP("K0","","FORMACION MAGISTERIAL ISP"),
    SUPERIOR_TECNOLOGICA_IST("T0","","SUPERIOR TECNOLOGICA IST"),
    ESCUELAS_DE_FORMACION_ARTISTICA("M0","","ESCUELAS DE FORMACION ARTISTICA"),
    EDUCACION_ESPECIAL("E0","","EDUCACION ESPECIAL"),
    CETPRO("L0","","CETPRO"),
    INICIAL_NO_ESCOLARIZADO("A5","","INICIAL_NO_ESCOLARIZADO"),
    BASICA_ALTERNATIVA("DO","","BASICA ALTERNATIVA");

    private String cod;
    private String codTipo;
    private String descri;






    TipNivelModalidadEnum(String cod,String codTipo,String descri)
    {   this.cod=cod;
        this.codTipo=codTipo;
        this.descri=descri;
    }

    /**
     * @return the cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @return the codTipo
     */
    public String getCodTipo() {
        return codTipo;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

}
