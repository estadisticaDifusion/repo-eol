package pe.gob.minedu.escale.eol.payload.estadistica;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class CedulaCensalResponse extends ResponseGeneric {

	private CedulaCensalResponsePayload responsePayload;

	public CedulaCensalResponsePayload getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(CedulaCensalResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}

}
