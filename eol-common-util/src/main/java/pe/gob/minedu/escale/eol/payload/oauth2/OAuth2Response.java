package pe.gob.minedu.escale.eol.payload.oauth2;

import pe.gob.minedu.escale.eol.payload.ResponseGeneric;

public class OAuth2Response extends ResponseGeneric {
	private String payload;

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

}
