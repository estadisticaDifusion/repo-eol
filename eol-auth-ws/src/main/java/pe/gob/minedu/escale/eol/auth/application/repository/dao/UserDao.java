package pe.gob.minedu.escale.eol.auth.application.repository.dao;

import pe.gob.minedu.escale.eol.auth.application.model.User;
import pe.gob.minedu.escale.eol.auth.application.repository.generics.GenericDAO;

public interface UserDao extends GenericDAO<User, Long> {

	public boolean isUserAvailable(String login);

	public User loadUserByUsername(String username);

}