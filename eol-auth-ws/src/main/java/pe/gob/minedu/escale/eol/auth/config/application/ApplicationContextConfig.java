package pe.gob.minedu.escale.eol.auth.config.application;

import java.util.Properties;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import pe.gob.minedu.escale.eol.auth.application.aspect.LogginAspect;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "pe.gob.minedu.escale.eol.auth.application.*" })
@PropertySource(value = { "classpath:eol-hibernate.properties" })
public class ApplicationContextConfig {

	private Logger logger = Logger.getLogger(ApplicationContextConfig.class);

	@Autowired
	private Environment env;

	@Bean
	public DataSource oauth2DataSource() {
		logger.info(":: ApplicationContextConfig.oauth2DataSource :: Starting execution...");

		final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
        dsLookup.setResourceRef(true);
        DataSource oauth2DataSource = dsLookup.getDataSource("java:jboss/jdbc/applicationDS");
        
        logger.info("oauth2DataSource = " + oauth2DataSource);
		
		logger.info(":: ApplicationContextConfig.oauth2DataSource :: Execution finish.");
		
		return oauth2DataSource;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

		sessionFactory.setDataSource(oauth2DataSource());
		sessionFactory.setPackagesToScan(new String[] { "pe.gob.minedu.escale.eol.auth.application.model" });
		sessionFactory.setHibernateProperties(getHibernateProperties());

		return sessionFactory;
	}

	private Properties getHibernateProperties() {
		Properties prop = new Properties();

		prop.put("hibernate.dialect", env.getProperty("mysql.dialect"));
		prop.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		
//		prop.put("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
//		prop.put("hibernate.query.substitutions", env.getProperty("hibernate.query.substitutions"));
//		prop.put("hibernate.connection.release_mode", env.getProperty("hibernate.connection.release_mode"));
//		prop.put("hibernate.generate_statistics", env.getProperty("hibernate.generate_statistics"));

		return prop;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager txtManager = new HibernateTransactionManager();
		txtManager.setSessionFactory(sessionFactory);
		return txtManager;
	}

	@Bean
	public LogginAspect myAspect() {
		return new LogginAspect();
	}
	
    @Bean
    public PasswordEncoder passwordEncoder() {
        SecureRandom random = null;
        try {
             random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
                 e.printStackTrace();
        }
        PasswordEncoder encoder = new BCryptPasswordEncoder(16, random);
        return encoder;
   }
    
}