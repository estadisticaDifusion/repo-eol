package pe.gob.minedu.escale.eol.auth.application.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.payload.oauth2.OAuth2Response;
import pe.gob.minedu.escale.eol.utils.Funciones;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright (C) 2019 Ministerio de Educacion (MINEDU) - Unidad de Estadistica Educativa (UEE)
 * (Lima - Peru)
 *
 * PROYECTO EOL
 *
 * All rights reserved. Used by permission This software is the confidential and
 * proprietary information of MINEDU ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with MINEDU.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */
/**
 * @objetivo: API que permite eliminar un token del OAuth2, cuando se desea forzar el acceso
 * en APIs de informacion del EOL
 * @autor: Ing. Oscar Mateo
 * @fecha: 11/05/2019
 *
 * ---------------------------------------------------------------------------------
 * Modificaciones Fecha Nombre Descripción
 * ---------------------------------------------------------------------------------
 *
 */

@FrameworkEndpoint
public class RevokeTokenEndpoint {

	private Logger logger = Logger.getLogger(RevokeTokenEndpoint.class);

	@Autowired
	@Qualifier("tokenServices")
	private ConsumerTokenServices tokenServices;

	@RequestMapping(method = RequestMethod.DELETE, value = "/oauth/token")
	@ResponseBody
	public ResponseEntity<OAuth2Response> revokeToken(HttpServletRequest request) {
		logger.info(":: RevokeTokenEndpoint.revokeToken :: Starting execution...");
		
		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "revokeToken";
		String httpMethod = "DELETE";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";
		
		OAuth2Response response = new OAuth2Response();

		response.setId(id);
		response.setDateTime(dateTime);
		response.setResourceName(resourceName);
		response.setHttpMethod(httpMethod);
		
		String authorization = request.getHeader("Authorization");
		String tokenId = request.getHeader("tokenId");
		logger.info("authorization = " + authorization);
		if (authorization != null && authorization.contains("Basic") && tokenId != null) {
			logger.info("tokenId = " + tokenId);
			logger.info("tokenServices = " + tokenServices);
			if (tokenServices.revokeToken(tokenId)) {
				status = "SUCCESS";
				code = "S0001";
				message = "The transaction has been successfully executed";
				response.setPayload(CoreConstant.DATA_PROCESSED);
			} else {
				response.setPayload(CoreConstant.DATA_NOT_PROCESSED);
			}
		}
		response.setStatus(status);
		response.setCode(code);
		response.setMessage(message);		
		logger.info(":: RevokeTokenEndpoint.revokeToken :: Execution finish.");
		return new ResponseEntity<OAuth2Response>(response, HttpStatus.OK);
	}
}
