package pe.gob.minedu.escale.eol.auth.application.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.auth.application.model.User;
import pe.gob.minedu.escale.eol.auth.application.service.security.dao.UserServicesDao;
import pe.gob.minedu.escale.eol.auth.application.utils.ConstantsApiOAuth2;
import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.auth.domain.AuthUsuarioHistorial;
import pe.gob.minedu.escale.eol.auth.ejb.AuthUsuarioHistorialLocal;
import pe.gob.minedu.escale.eol.estadistica.ejb.SesionUsuarioFacade;
import pe.gob.minedu.escale.eol.payload.auth.UpdateCredentialsRequest;
import pe.gob.minedu.escale.eol.payload.auth.UpdateCredentialsResponse;
import pe.gob.minedu.escale.eol.payload.auth.UpdateCredentialsResponsePayload;
import pe.gob.minedu.escale.eol.payload.padron.EncryptDTO;
import pe.gob.minedu.escale.eol.utils.AesUtil;
//import pe.gob.minedu.escale.eol.utils.CryptoSecurityUtil;
import pe.gob.minedu.escale.eol.utils.Funciones;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

/**
 * 
 * 
 * @author Martin Huamani Mendoza
 * @version 1.0
 */

@RestController
@RequestMapping(value = "/api/v1.0")
public class RestCredentialsUserServices {

	private Logger logger = Logger.getLogger(RestCredentialsUserServices.class);

	private AesUtil aesUtil = new AesUtil(ConstantsApiOAuth2.KEY_SIZE, ConstantsApiOAuth2.ITERATION_COUNT);

	Gson gson = new Gson();

	@EJB(mappedName = "java:global/eol-auth-ws/SesionUsuarioFacade!pe.gob.minedu.escale.eol.estadistica.ejb.SesionUsuarioFacade")
	private SesionUsuarioFacade sesionUsuarioFacade;

	@EJB(mappedName = "java:global/eol-auth-ws/AuthUsuarioHistorialFacade!pe.gob.minedu.escale.eol.auth.ejb.AuthUsuarioHistorialLocal")
	private AuthUsuarioHistorialLocal authUsuarioHistorialLocal;

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UserServicesDao customUserDetailServices;

	@RequestMapping(value = "/post/updateCredentials", method = RequestMethod.POST, headers = "Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<UpdateCredentialsResponse> updatePasswordByUser(@RequestBody EncryptDTO request) {
		logger.info(":: RestCredentialsUserServices.updatePasswordByUser :: Starting execution...");

		UpdateCredentialsRequest aut = new UpdateCredentialsRequest();

		aut = gson.fromJson(desencriptar(request.getCipher(), request.getSalt(), request.getIv()),
				UpdateCredentialsRequest.class);

		if (aut == null) {
			throw new RuntimeException("Request es incorrecto");
		}

		String id = Funciones.uniqueID().toString();
		String dateTime = Funciones.getISODateNowAsString();
		String resourceName = "ugel";
		String httpMethod = "POST";
		String status = "ERROR";
		String code = "E0001";
		String message = "Generic Error Found";

		UpdateCredentialsResponse updateCredentialsResponse = new UpdateCredentialsResponse();
		updateCredentialsResponse.setResponsePayload(new UpdateCredentialsResponsePayload());
		UpdateCredentialsResponsePayload responsePayload = updateCredentialsResponse.getResponsePayload();

		updateCredentialsResponse.setId(id);
		updateCredentialsResponse.setDateTime(dateTime);
		updateCredentialsResponse.setResourceName(resourceName);
		updateCredentialsResponse.setHttpMethod(httpMethod);

		String state = getCredentialsUpdateState(aut);

		if (state != null) {
			status = "SUCCESS";
			code = "S0001";
			message = "The transaction has been successfully executed";
			responsePayload.setResult("DATA_FOUND");
			responsePayload.setStateUpdate(state);
		} else {
			logger.info("CREDENCIALES NULAS : "  + state  );
			responsePayload.setResult("DATA_NOT_FOUND");
		}
		updateCredentialsResponse.setStatus(status);
		updateCredentialsResponse.setCode(code);
		updateCredentialsResponse.setMessage(message);
		updateCredentialsResponse
				.setCipher(encriptar(request.getSalt(), request.getIv(), gson.toJson(updateCredentialsResponse)));
		updateCredentialsResponse.setSalt(request.getSalt());
		updateCredentialsResponse.setIv(request.getIv());
		updateCredentialsResponse.setPassphrace(ConstantsApiOAuth2.PASS_PHRASE);

		logger.info(":: RestCredentialsUserServices.updatePasswordByUser :: Execution finish.");
		return new ResponseEntity<UpdateCredentialsResponse>(updateCredentialsResponse, HttpStatus.OK);
	}

	private String getCredentialsUpdateState(final UpdateCredentialsRequest updateCredentialsRequest) {
		
		String user = updateCredentialsRequest.getUsuario();
		
		String password = updateCredentialsRequest.getContrasenia();
		
		int respuesta = 0;

		String newPassword = updateCredentialsRequest.getNuevaContrasenia();
		List<String> reqParams = new ArrayList<String>();
		reqParams.add(user);
		reqParams.add(password);
		reqParams.add(newPassword);
		boolean existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);
		if (existChar) {
			respuesta = 4;
		} else {

			AuthUsuario authUsuario = this.sesionUsuarioFacade.userAuthValid(user, password);
			
			if (authUsuario != null) {

				
				if (!authUsuario.getContrasenia().equals(newPassword)) {
					
					AuthUsuarioHistorial authUsuarioHistorial = new AuthUsuarioHistorial();
					authUsuarioHistorial.setUsuario(user);
					authUsuarioHistorial.setContraseniaText(authUsuario.getContrasenia());
					authUsuarioHistorial.setContrasenia(newPassword);
					authUsuarioHistorial.setUsuarioRegistro(user);
					int validation = this.authUsuarioHistorialLocal.createUserHistory(this.passwordEncoder,
							authUsuarioHistorial);
					if (validation == 0) {
						// No existe contraseña en historial
						authUsuario = new AuthUsuario();
						authUsuario.setUsuario(user);
						authUsuario.setContrasenia(newPassword);
						this.sesionUsuarioFacade.update(authUsuario);
						User userApplication = this.customUserDetailServices.getUserByLogin(user);
						if (userApplication != null) {
							userApplication.setPassword(newPassword);
							this.customUserDetailServices.actualizar(userApplication);

						}
					} else if (validation == 1) {
						// Existe contraseña en historial
						respuesta = 3;
					}
				} else {
					// La contraseña no debe ser igual a actual
					respuesta = 2;
				}
			} else {
				// La contraseña actual no es correcta
				respuesta = 1;
			}
		}
		return String.valueOf(respuesta);
	}

	private String desencriptar(String param, String salt, String iv) {
		String objJsonString = aesUtil.decrypt(salt, iv, ConstantsApiOAuth2.PASS_PHRASE, param);
		return objJsonString;
	}

	private String encriptar(String salt, String iv, String param) {
		String cipher = aesUtil.encrypt(salt, iv, ConstantsApiOAuth2.PASS_PHRASE, param);
		return cipher;
	}

}
