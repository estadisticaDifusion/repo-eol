package pe.gob.minedu.escale.eol.auth.application.repository.dao;

import pe.gob.minedu.escale.eol.auth.application.model.OauthClientDetails;
import pe.gob.minedu.escale.eol.auth.application.repository.generics.GenericDAO;

public interface OAuthClientDAO extends GenericDAO<OauthClientDetails, String> {

	public boolean isClientAvailable(String clientId);

	public OauthClientDetails loadClientById(String clientId);

}