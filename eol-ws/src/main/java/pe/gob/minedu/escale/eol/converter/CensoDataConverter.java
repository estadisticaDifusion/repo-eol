/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.converter;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author JMATAMOROS
 */

public abstract class CensoDataConverter {

    @XmlElement(name="nroEnvio")
    public abstract int getNroEnvio();

    @XmlElement(name="cod_mod")
    public abstract String getCodMod() ;

    @XmlElement(name="anexo")
    public abstract String getAnexo();

    @XmlElement(name="niv_mod")
    public abstract String getNivMod();

    @XmlElement(name="codlocal")
    public abstract String getCodLocal();

    //@XmlElement(name="codgeo")
    //public abstract String getCodgeo();

    @XmlElement(name="codugel")
    public abstract String getCodugel();

    @XmlElement(name="fechaEnvio")
    public abstract Date getFechaEnvio();

    @XmlElement(name="situacion")
    public abstract String getSituacion();


}
