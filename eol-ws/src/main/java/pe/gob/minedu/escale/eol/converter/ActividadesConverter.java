/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.domain.EolActividad;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "actividades")
public class ActividadesConverter {

    private URI uri;
    private int expandLevel;
    private Collection<EolActividad> entities;
    private Collection<ActividadConverter> items = new ArrayList<ActividadConverter>();

    public ActividadesConverter() {
    }

    public ActividadesConverter(Collection<EolActividad> entities, URI uri, int expandLevel) {
        this.entities = entities;
        this.expandLevel = expandLevel;
        this.uri = uri;
    }

    @XmlTransient
    public Collection<EolActividad> getEntities() {
        return entities;
    }

    public void setEntities(Collection<EolActividad> entities) {
        this.entities = entities;
    }

    @XmlElement(name = "items")
    public Collection<ActividadConverter> getItems() {
        if (entities != null && !entities.isEmpty()) {
            for (EolActividad act : entities) {
                items.add(new ActividadConverter(act, getUri(), expandLevel));
            }
        }
        return items;
    }

    public void setItems(Collection<ActividadConverter> items) {
        this.items = items;
    }

    @XmlAttribute(name = "url")
    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

}
