package pe.gob.minedu.escale.eol.rest.censo2019;

import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ARTISTICA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA_AVANZADA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA_INICIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ESPECIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ESPECIAL_INICIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ESPECIAL_PRIMARIA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_INICIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_INICIAL_NO_ESCOLARIZADO;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_OCUPACIONAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_PEDAGOGICA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_PRIMARIA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_SECUNDARIA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_TECNOLOGICA;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_01B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_02B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_03BP;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_03BS;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_04BA;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_04BI;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_05B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_06B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_07B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_08BI;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_08BP;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_09B;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import pe.gob.minedu.escale.eol.constants.EolWsConstant;
import pe.gob.minedu.escale.eol.converter.RespuestaConverter;
import pe.gob.minedu.escale.eol.domain.EolActividadesClient;
import pe.gob.minedu.escale.eol.domain.EolActividadesConverter;
import pe.gob.minedu.escale.eol.domain.EolCedulaConverter;
import pe.gob.minedu.escale.eol.domain.EolCedulasClient;
import pe.gob.minedu.escale.eol.domain.EolCedulasConverter;
import pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Detalle;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Fila;
import pe.gob.minedu.escale.eol.domain.domainPadron.CentroEducativo;
import pe.gob.minedu.escale.eol.ejb.censo2019.Resultado2019Local;
import pe.gob.minedu.escale.padron.ejb.facade.PadronLocal;

@Path("/censo2019/resultado")
@Stateless
public class CedulaResultado2019Service {

	private Logger logger = Logger.getLogger(CedulaResultado2019Service.class.getName());

	@EJB
	private Resultado2019Local facade = lookupResultado(Resultado2019Local.class);

	@EJB
	private PadronLocal padronFacade = lookupPadron(PadronLocal.class);

//	@EJB
//	private CedulaResultado2019ValidResources facadeValid;

	
	private ResourceBundle bundle = ResourceBundle
			.getBundle("etiquetas_resultado");
	//.getBundle("/pe/gob/minedu/escale/eol/rest/censo2019/valid/resource/etiquetas_resultado");
	//.getBundle("pe.gob.minedu.escale.eol.rest.censo2019.valid.resource.etiquetas_resultado");
	private static final String TIPDATO_TIPOPROG = "c02b.tipoprog";
	private static String warning_str = "\n- No se ha definido el valor '%1$s'  para el tipo de dato '%2$s'";

	private static String warning_str_formato = "Hay errores en los datos que esta enviando.\n Por favor, envienos su cédula por medio de correo electrónico a la siguiente dirección: censoescolar@minedu.gob.pe ";
	final static DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);
	private static String PERIODO = "2019";
	private static String NOMBRE_ACTIVIDAD = "CENSO-RESULTADO";
	
	/**
	 * Registra una cédula de resultado vía REST
	 * 
	 * @param cedula La cédula de resultado
	 * @return un estado de respuesta según sea la situación de la recepción
	 */
	@POST
	@Path("/registrar")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	public Response registrar(Resultado2019Cabecera cedula) {
		logger.info(":: CedulaResultado2019Service.registrar :: Starting execution...");
		RespuestaConverter rpta = new RespuestaConverter();
		String msg = "";

		if (cedula.getToken() == 0 || cedula.getToken() == -1L) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		CentroEducativo ce = null;
		ce = padronFacade.obtainByCodMod(cedula.getCodMod(), cedula.getAnexo());

		try {
			if (ce == null) {
				msg = String.format(
						"No se encontró ninguna Institución Educativa con código modular %1s y anexo %2s  .",
						cedula.getCodMod(), cedula.getAnexo());
				rpta.setMensaje(msg);
				rpta.setStatus("ERROR");
				return Response.ok(rpta).build();// verificar_consistencia_2019
			}
			if (!ce.getMcenso().equals("1")) {
				msg = "La Institución Educativa no se encuentra incluida en el marco censal, para mayor información consulte con el estadístico de su UGEL.";
				rpta.setMensaje(msg);
				rpta.setStatus("ERROR");
				return Response.ok(rpta).build();
			}

		} catch (Exception e) {
			msg = "Esta cédula presenta un error interno en el servicio de busqueda del padron de IIEE."
					+ "\nPor favor vuelva a intentar más tarde." + "\nDisculpe las molestias ocasionadas. ";
			rpta.setMensaje(msg);
			rpta.setStatus("ERROR");
			return Response.ok(rpta).build();
		}

		cedula.setValido(false);
		
//		rpta = facadeValid.validar(cedula, ce);
		rpta = validar(cedula, ce);
		cedula.setCodooii(ce.getCodigel());
		cedula.setCodgeo(ce.getCodgeo());

		/* VALIDACION DE LA CONSISTENCIA MATRICULA */
		String msgobs = "";
		List<Object[]> listObj = null;
		if (!cedula.getVersion().equals("9")
				&& (cedula.getNroced().equals("c03bp") || cedula.getNroced().equals("c03bs"))) {

			List<Resultado2019Fila> filas = cedula.getResultado2019ResultadoList().get(0).getFilas();
			for (Resultado2019Fila rsfil : filas) {
				if (rsfil.getTipdato().equals("00")) { /////////////  VERIFICACION ////////////////////////////////////

					listObj = facade.verificarObservacionCedula(cedula.getCodMod(), cedula.getAnexo(),
							cedula.getNivMod(), rsfil.getTotalH(), rsfil.getTotalM(), rsfil.getDato01h(),
							rsfil.getDato01m(), rsfil.getDato02h(), rsfil.getDato02m(), rsfil.getDato03h(),
							rsfil.getDato03m(), rsfil.getDato04h(), rsfil.getDato04m(), rsfil.getDato05h(),
							rsfil.getDato05m(), rsfil.getDato06h(), rsfil.getDato06m(), rsfil.getDato07h(),
							rsfil.getDato07m(), rsfil.getDato08h(), rsfil.getDato08m(), rsfil.getDato09h(),
							rsfil.getDato09m(), rsfil.getDato10h(), rsfil.getDato10m());
					break;
				}
			}

			if (listObj != null) {

				for (int i = 0; i < listObj.size(); i++) {
					Integer flag = Integer.parseInt(listObj.get(i)[0].toString());
					if (flag.intValue() == 1) {
						cedula.setValido(true);

						msgobs = "¡Los datos se grabaron con éxito!\n\n"
								+ "Existe una variación significativa entre los datos reportados al inicio del año y los resultados del ejercicio educativo.\n\n"
								+ "Imprima su constancia de envío y verifique si la información declarada es correcta.";

					}
				}
			}
		}

		// validacion de variable
		cedula.setVarrec("0");
		if (cedula.getNroced().equals("c03bp") || cedula.getNroced().equals("c03bs")
				|| cedula.getNroced().equals("c4bi") || cedula.getNroced().equals("c4ba")) {

			logger.info("elements cedula.getResultado2019ResultadoList() size : "
					+ (cedula.getResultado2019ResultadoList() != null ? cedula.getResultado2019ResultadoList().size()
							: " is null"));

			List<Resultado2019Detalle> detalleList = cedula.getResultado2019ResultadoList().stream()
					.filter(element -> EolWsConstant.Resultado2019DetalleCuadroC101.equals(element.getCuadro())
							|| EolWsConstant.Resultado2019DetalleCuadroP101.equals(element.getCuadro()))
					.collect(Collectors.toList());
			logger.info("elements detalleList size : " + (detalleList != null ? detalleList.size() : " is null"));

			for (Resultado2019Detalle detalle : detalleList) {
				List<Resultado2019Fila> filas_ = detalle.getFilas();

				for (Resultado2019Fila f : filas_) {
					if (f.getTipdato().equals("02") && (f.getTotalH() > 0 || f.getTotalM() > 0)) {
						cedula.setVarrec("1");
					}
				}
			}

			/*
			for (Resultado2019Fila filas : cedula.getResultado2019ResultadoList().get(0).getFilas()) {
				logger.info("filas.getDetalle() = " + filas.getDetalle());
				if (filas.getDetalle().getCuadro().equals("C101") || filas.getDetalle().getCuadro().equals("P101")) {
					List<Resultado2019Fila> filas_ = cedula.getResultado2019ResultadoList().get(0).getFilas();

					for (Resultado2019Fila f : filas_) {
						if (f.getTipdato().equals("02") && (f.getTotalH() > 0 || f.getTotalM() > 0)) {
							cedula.setVarrec("1");
						}
					}
				}
			}*/
		}

		if (rpta.getMensaje().isEmpty()) {

			facade.create(cedula);
			if (cedula.getValido()) {

				rpta.setMensaje(msgobs);
				rpta.setStatus("WARNING");
			} else {

				rpta.setMensaje(
						"Su mensaje se envio correctamente, por favor verifique sus datos imprimiento su constancia desde la pagina http://escale.minedu.gob.pe");
				rpta.setStatus("OK");
			}

		} else {

			rpta.setStatus("ERROR");
		}

		logger.info(":: CedulaResultado2019Service.registrar :: Execution finish.");
		return Response.ok(rpta).build();
	}

	/*
	 * private Integer IntN(Integer num) { LOGGER.info("INT N  :  " + num); return
	 * num != null ? num : 0; }
	 */
	@GET
	@Produces({ "application/json" })
	public Resultado2019Cabecera getCedula(@QueryParam("idEnvio") String idEnvio) {

		Resultado2019Cabecera cedula = facade.findCedNivel(Long.parseLong(idEnvio));
		return cedula;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupPadron(Class<T> clase) {

		String nameContextEjb = "java:global/eol-ws/PadronFacade!pe.gob.minedu.escale.padron.ejb.facade.PadronLocal";

		try {

			javax.naming.Context c = new InitialContext();

			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {

			throw new RuntimeException(ne);
		}
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupResultado(Class<T> clase) {

		String nameContextEjb = "java:global/eol-ws/Resultado2019Facade!pe.gob.minedu.escale.eol.ejb.censo2019.Resultado2019Local";

		try {

			javax.naming.Context c = new InitialContext();

			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {

			throw new RuntimeException(ne);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public RespuestaConverter validar(Resultado2019Cabecera cedula, CentroEducativo ie) {
		logger.info(":: CedulaResultado2019ValidResources.validar :: Starting execution...");
		
		RespuestaConverter rpta = new RespuestaConverter();
		StringBuffer msg = new StringBuffer();
		String msgVPlazo = verificarPlazoEntrega(NOMBRE_ACTIVIDAD, PERIODO);
		String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO, cedula.getNivMod(), "1");
		String msgVCedula = verificarCedula(cedula, ie);
		boolean esPronoei = false;

		if (msgVPlazo.length() > 0) {
			
			msg.append(msgVPlazo);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (msgVVersion.length() > 0) {
			
			msg.append(msgVVersion);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (msgVCedula.length() > 0) {
			
			msg.append(msgVCedula);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (cedula.getAnexo() == null || cedula.getAnexo().isEmpty()) {
			
			msg.append("No está declarado el anexo.");
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (!ie.getMcenso().equals("1")) {
			
			msg.append(
					"El estado de la Institución Educativo en el registro de padrón es inactivo, regularice su situación con el estadístico de su UGEL.");
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		esPronoei = ie.getNivMod().equals(NIVEL_INICIAL_NO_ESCOLARIZADO);
		if (!esPronoei && cedula.getCodlocal() != null && !cedula.getCodlocal().isEmpty()
				&& !cedula.getCodlocal().equals(ie.getCodlocal())) {
			
			msg.append(String.format("\nEl código de local de la I.E. %1s es  %2s", ie.getCenEdu(), ie.getCodlocal()));
		}
		
//		if (cedula.getDetalle() == null || cedula.getDetalle().isEmpty()) {
		if (cedula.getResultado2019ResultadoList() == null || cedula.getResultado2019ResultadoList().isEmpty()) {
			
			msg.append(warning_str_formato);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (cedula.getNroced().equals(CEDULA_01B)) {
		} else if (cedula.getNroced().equals(CEDULA_02B)) {
			
			if (!bundle.containsKey(TIPDATO_TIPOPROG.concat(".").concat(cedula.getTipprog()))) {
				msg.append(String.format(warning_str, cedula.getTipprog(), "Tipo de Programa"));
			}
		} else if (cedula.getNroced().equals(CEDULA_03BS)) {
			

			if (cedula.getNivMod().equals(NIVEL_SECUNDARIA)
					&& !bundle.containsKey("c03b.F0.formaten.".concat(cedula.getFormaten()))) {
				
				msg.append(String.format(warning_str, cedula.getFormaten(), "Forma de atención"));
			}
		}

		//boolean esCed6B = cedula.getNroced().equals(CEDULA_06B);

		for (Resultado2019Detalle detalle : cedula.getResultado2019ResultadoList()) {

			if (detalle.getFilas() == null || detalle.getFilas().isEmpty() || detalle == null) {
				msg.append(warning_str_formato);
				rpta.setMensaje(msg.toString());
				rpta.setStatus("ERROR");
				return rpta;
			}
		}

		msg.append(validarTotales(cedula));
		if (msg.length() > 0) {
			
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
		}

		rpta.setMensaje(msg.toString());
		
		logger.info(":: CedulaResultado2019ValidResources.validar :: Execution finish.");
		return rpta;
	}

    public RespuestaConverter validarRecupera(Resultado2019Cabecera cedula, InstitucionEducativaIE ie) {
        RespuestaConverter rpta = new RespuestaConverter();
        StringBuffer msg = new StringBuffer();
        String msgVPlazo = verificarPlazoEntrega(NOMBRE_ACTIVIDAD, PERIODO);
        //String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO, cedula.getNivMod(), cedula.getVersion());
        String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO, cedula.getNivMod(), "1");
        String msgVCedula = verificarCedulaRecup(cedula, ie);
        boolean esPronoei = false;


        if (msgVPlazo.length() > 0) {
            msg.append(msgVPlazo);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (msgVVersion.length() > 0) {
            msg.append(msgVVersion);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (msgVCedula.length() > 0) {
            msg.append(msgVCedula);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (cedula.getAnexo() == null || cedula.getAnexo().isEmpty()) {
            msg.append("No está declarado el anexo.");
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (!ie.getMcenso().equals("1")) {
            msg.append("El estado de la Institución Educativo en el registro de padrón es inactivo, regularice su situación con el estadístico de su UGEL.");
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        esPronoei = ie.getNivelModalidad().getIdCodigo().equals(NIVEL_INICIAL_NO_ESCOLARIZADO);
        if (!esPronoei && cedula.getCodlocal() != null && !cedula.getCodlocal().isEmpty() && !cedula.getCodlocal().equals(ie.getCodlocal())) {
            msg.append(String.format("\nEl código de local de la I.E. %1s es  %2s", ie.getCentroEducativo(), ie.getCodlocal()));
        }

        if (cedula.getResultado2019ResultadoList() == null || cedula.getResultado2019ResultadoList().isEmpty()) {
            msg.append(warning_str_formato);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

//        if (!bundle.containsKey(TIPDATO_SITUACION.concat(".").concat(cedula.getSituacion()))) {
//            msg.append(String.format(warning_str, cedula.getSituacion(), "Situación del Director"));
//        }

        if (cedula.getNroced().equals(CEDULA_01B)) {
        } else if (cedula.getNroced().equals(CEDULA_02B)) {
            if (!bundle.containsKey(TIPDATO_TIPOPROG.concat(".").concat(cedula.getTipprog()))) {
                msg.append(String.format(warning_str, cedula.getTipprog(), "Tipo de Programa"));
            }
        } else if (cedula.getNroced().equals(CEDULA_03BS)) {


            if (cedula.getNivMod().equals(NIVEL_SECUNDARIA) && !bundle.containsKey("c03b.F0.formaten.".concat(cedula.getFormaten()))) {
                msg.append(String.format(warning_str, cedula.getFormaten(), "Forma de atención"));
            }
        }


        boolean esCed6B = cedula.getNroced().equals(CEDULA_06B);

            Resultado2019Detalle detalle = cedula.getResultado2019ResultadoList().get(0);


              if (detalle.getFilas() == null || detalle.getFilas().isEmpty()) {
                    msg.append(warning_str_formato);
                    rpta.setMensaje(msg.toString());
                    rpta.setStatus("ERROR");
                    return rpta;
                }



        msg.append(validarTotales(cedula));
        if (msg.length() > 0) {
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
        }

        rpta.setMensaje(msg.toString());
        return rpta;
    }
	
	
	public String validarTotales(Resultado2019Cabecera cedula) {
		String msgTot = "Los totales del cuadro %1s no son iguales a la suma de los detalles. "
				+ "Es posible que la cédula presente un error interno.\nPor favor, descargue nuevamente la cédula desde su tablero de control en la página ( http://escale.minedu.gob.pe/estadistica/ce/ ) .\nDisculpe las molestias ocasionadas.";


		if (cedula.getResultado2019ResultadoList() == null) {
			return warning_str_formato;
		}


		for (Resultado2019Detalle detalle : cedula.getResultado2019ResultadoList()) {

			// System.out.println("CUADRO:"+detalle.getCuadro());
			detalle.setCedula(cedula);
			List<Resultado2019Fila> filas = detalle.getFilas();
			Integer total = 0;
			Integer dTotal = 0;
			Integer valTipDato = -1;

			boolean isTot = false;
			for (Resultado2019Fila fila : filas) {
				fila.setDetalle(detalle);
				try {
					valTipDato = Integer.parseInt(fila.getTipdato());
				} catch (NumberFormatException e) {
					valTipDato = -1;
				}

				if (valTipDato == 0 && !isTot) {
					total = total + IntN(fila.getDato01h()) + IntN(fila.getDato01m());
					total = total + IntN(fila.getDato02h()) + IntN(fila.getDato02m());
					total = total + IntN(fila.getDato03h()) + IntN(fila.getDato03m());
					total = total + IntN(fila.getDato04h()) + IntN(fila.getDato04m());
					total = total + IntN(fila.getDato05h()) + IntN(fila.getDato05m());
					total = total + IntN(fila.getDato06h()) + IntN(fila.getDato06m());
					total = total + IntN(fila.getDato07h()) + IntN(fila.getDato07m());
					total = total + IntN(fila.getDato08h()) + IntN(fila.getDato08m());
					total = total + IntN(fila.getDato09h()) + IntN(fila.getDato09m());
					total = total + IntN(fila.getDato10h()) + IntN(fila.getDato10m());
					isTot = true;
				} else {
					if (!((cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_03BP)
							|| cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_03BS))
							&& detalle.getCuadro().equals("C101") && valTipDato.equals(new Integer(2)))) {// Se
																											// excluye
																											// el ítem 2
																											// del
																											// cuadro
																											// C101
						dTotal = dTotal + IntN(fila.getDato01h()) + IntN(fila.getDato01m());
						dTotal = dTotal + IntN(fila.getDato02h()) + IntN(fila.getDato02m());
						dTotal = dTotal + IntN(fila.getDato03h()) + IntN(fila.getDato03m());
						dTotal = dTotal + IntN(fila.getDato04h()) + IntN(fila.getDato04m());
						dTotal = dTotal + IntN(fila.getDato05h()) + IntN(fila.getDato05m());
						dTotal = dTotal + IntN(fila.getDato06h()) + IntN(fila.getDato06m());
						dTotal = dTotal + IntN(fila.getDato07h()) + IntN(fila.getDato07m());
						dTotal = dTotal + IntN(fila.getDato08h()) + IntN(fila.getDato08m());
						dTotal = dTotal + IntN(fila.getDato09h()) + IntN(fila.getDato09m());
						dTotal = dTotal + IntN(fila.getDato10h()) + IntN(fila.getDato10m());
					}
				}
			}
			if (isTot && filas.size() > 0 && !(total.intValue() == dTotal.intValue())) {
				return String.format(msgTot, detalle);
			}
			if (!isTot && (((!cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_08BI)
					|| !cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_08BP))
					&& !detalle.getCuadro().equals("C101"))
					&& (!cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_06B)
							&& !detalle.getCuadro().equals("C106")))) {
				return "No se está enviando el total en el cuadro ".concat(detalle.getFilas().toString()).concat(
						", verifique su cédula o envienos por medio de correo electrónico a la siguiente dirección: censoescolar@minedu.gob.pe, indicando el problema ");
			}

		}

		return "";
	}

	public Integer IntN(Integer num) {
		return num != null ? num : 0;
	}

	public String verificarPlazoEntrega(String tipoDocumento, String anio) {
		EolActividadesClient actClient = new EolActividadesClient();
		EolActividadesConverter actConv = actClient.getActividadesConverter(tipoDocumento, anio);
		// EolActividadConverter act=null;

		if (actConv != null && actConv.getItems() != null && !actConv.getItems().isEmpty()) {
			// act = actConv.getItems().get(0);
			Date ahora = new Date();
			Date fechTe = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(actConv.getItems().get(0).getFechaTermino());
			c.add(Calendar.DATE, 1);
			fechTe = c.getTime();
			if (!((ahora.equals(actConv.getItems().get(0).getFechaInicio())
					|| ahora.after(actConv.getItems().get(0).getFechaInicio()))
					&& (ahora.equals(fechTe) || ahora.before(fechTe)))) {
				return ("El plazo de entrega electrónica para este tipo documento es desde el día "
						+ DATE_FORMAT.format(actConv.getItems().get(0).getFechaInicio()) + " hasta el día "
						+ DATE_FORMAT.format(actConv.getItems().get(0).getFechaTermino()));
			}
		}

		if (actConv.getItems().get(0) == null) {
			return "Aún no se ha establecido una fecha de entrega electrónica para este tipo de documento";
		}

		return "";
	}

	public String verificarVersion(String tipoDocumento, String anio, String nivel, String versionEnvio) {
		EolCedulasClient cedClient = new EolCedulasClient();
		
		EolCedulasConverter cedConv = cedClient.getCedulasConverter(nivel, anio, tipoDocumento);
		EolCedulaConverter ced = null;
		
		if (cedConv != null && cedConv.getItems() != null && !cedConv.getItems().isEmpty()) {
			
			ced = cedConv.getItems().get(0);
		}
		if (ced == null) {
			
			return "Aún no se ha establecido la descarga de la cédula";
		}
		System.out.println("VERSION:" + ced.getVersion());
		if (ced.getVersion() == null || ced.getVersion().isEmpty()) {
			
			return "Aún no se ha establecido una versión para la cédula del nivel '".concat(nivel).concat("'");
		}

		if (!ced.getVersion().equals(versionEnvio)) {
			logger.info("CEDULARESULTADO2019VALIDA verificarVersion() cedConv : 4444");
			return "La versión de este formato está desactualizada, para actualizarlo por favor ingrese a su tablero "
					+ "( http://escale.minedu.gob.pe/estadistica/ce/ )  y descargue el último formato.";
		}

		return "";
	}


	public String verificarCedula(Resultado2019Cabecera cedula, CentroEducativo ie) {
		String mensaje = "";
		boolean ok = false;
		if (CEDULA_01B.equals(cedula.getNroced())) {
			ok = (cedula.getCodMod().equals(ie.getCodMod())) && (((ie.getNivMod().startsWith(NIVEL_INICIAL))
					&& (ie.getNivMod().charAt(0) == cedula.getNivMod().charAt(0)))
					|| (NIVEL_PRIMARIA.equals(ie.getNivMod()) && (InstitucionEducativaIE.SI.equals(ie.getProgarti()))));
			mensaje = "La Cédula 1B solamente corresponde al nivel inicial";
			if (ok && !InstitucionEducativaIE.SI.equals(ie.getProgarti())
					&& !(ie.getNivMod().equals(cedula.getNivMod()))) {
				ok = false;
				mensaje = "Error en el nivel seleccionado, el nivel de la I.E es:".concat(ie.getNivMod());
			}
		} else if (CEDULA_02B.equals(cedula.getNroced())) {
			ok = (cedula.getCodMod().equals(ie.getCodMod()))
					&& (ie.getNivMod().startsWith(NIVEL_INICIAL_NO_ESCOLARIZADO));
			mensaje = "La Cédula 2B solamente corresponde al nivel inicial no escolarizado";
		} else if (CEDULA_03BP.equals(cedula.getNroced())) {
			ok = (NIVEL_PRIMARIA.equals(ie.getNivMod())) && ie.getCodMod().equals(cedula.getCodMod())
					&& ie.getAnexo().equals(cedula.getAnexo());
			mensaje = "La Cédula 3BP solamente corresponde al nivel primario";
		} else if (CEDULA_03BS.equals(cedula.getNroced())) {
			ok = (NIVEL_SECUNDARIA.equals(ie.getNivMod())) && ie.getCodMod().equals(cedula.getCodMod())
					&& ie.getAnexo().equals(cedula.getAnexo());
			mensaje = "La Cédula 3BS solamente corresponde al nivel secundario";
		} else if (CEDULA_04BI.equals(cedula.getNroced()) || "c4bi".equals(cedula.getNroced())) {
			ok = (NIVEL_BASICA_ALTERNATIVA.equals(ie.getNivMod())
					|| NIVEL_BASICA_ALTERNATIVA_INICIAL.equals(ie.getNivMod()))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 4BI solamente corresponde al nivel Básico Alternativo - Inicial";
		} else if (CEDULA_04BA.equals(cedula.getNroced()) || "c4ba".equals(cedula.getNroced())) {
			ok = (NIVEL_BASICA_ALTERNATIVA_AVANZADA.equals(ie.getNivMod()))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 4BA solamente corresponde al nivel Básico Alternativo - Avanzada";
		} else if (CEDULA_05B.equals(cedula.getNroced())) {
			ok = ((NIVEL_PEDAGOGICA.equals(ie.getNivMod()))) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 5B solamente corresponde al nivel Superior Pedagógico";
		} else if (CEDULA_06B.equals(cedula.getNroced())) {
			ok = ((NIVEL_TECNOLOGICA.equals(ie.getNivMod())) || (NIVEL_PEDAGOGICA.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 6B solamente corresponde al nivel Superior Tecnológico";
		} else if (CEDULA_07B.equals(cedula.getNroced())) {
			ok = ((NIVEL_ARTISTICA.equals(ie.getNivMod()))) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 7B solamente corresponde al nivel Superior Artistico";
		} else if (CEDULA_08BI.equals(cedula.getNroced()) || "c08b".equals(cedula.getNroced())) {
			ok = ((NIVEL_ESPECIAL.equals(ie.getNivMod())) || (NIVEL_ESPECIAL_INICIAL.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 8BI solamente corresponde al nivel Educación Especial Inicial";
		} else if (CEDULA_08BP.equals(cedula.getNroced())) {
			ok = (NIVEL_ESPECIAL_PRIMARIA.equals(ie.getNivMod())) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 8BP solamente corresponde al nivel Educación Especial Primaria";
		} else if (CEDULA_09B.equals(cedula.getNroced())) {
			ok = ((ie.getNivMod().startsWith(NIVEL_OCUPACIONAL))) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 9B solamente corresponde al nivel CETPRO";
		}

		if (!ok) {
			return mensaje;
		} else {
			return "";
		}
	}
	
    public String verificarCedulaRecup(Resultado2019Cabecera cedula, InstitucionEducativaIE ie) {
        String mensaje = "";
        boolean ok = false;
        if (CEDULA_01B.equals(cedula.getNroced())) {
            ok = (cedula.getCodMod().equals(ie.getCodigoModular()))
                    && (((ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_INICIAL))
                    && (ie.getNivelModalidad().getIdCodigo().charAt(0) == cedula.getNivMod().charAt(0)))
                    || (NIVEL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo())
                    && (InstitucionEducativaIE.SI.equals(ie.getProgarti()))));
                mensaje = "La Cédula 1B solamente corresponde al nivel inicial";
                   if(ok && !InstitucionEducativaIE.SI.equals(ie.getProgarti())
                   && !(ie.getNivelModalidad().getIdCodigo().equals(cedula.getNivMod()))){
                        ok=false;
                        mensaje = "Error en el nivel seleccionado, el nivel de la I.E es:".concat(ie.getNivelModalidad().getValor());
                    }
        } else if (CEDULA_02B.equals(cedula.getNroced())) {
            ok = (cedula.getCodMod().equals(ie.getCodigoModular()))
                    && (ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_INICIAL_NO_ESCOLARIZADO));
            mensaje = "La Cédula 2B solamente corresponde al nivel inicial no escolarizado";
        } else if (CEDULA_03BP.equals(cedula.getNroced())) {
            ok = (NIVEL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo()))
                    && ie.getCodigoModular().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
            mensaje = "La Cédula 3BP solamente corresponde al nivel primario";
        } else if (CEDULA_03BS.equals(cedula.getNroced())) {
            ok = (NIVEL_SECUNDARIA.equals(ie.getNivelModalidad().getIdCodigo()))
                    && ie.getCodigoModular().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
            mensaje = "La Cédula 3BS solamente corresponde al nivel secundario";
        } else if (CEDULA_04BI.equals(cedula.getNroced()) || "c4bi".equals(cedula.getNroced())) {
            ok = (NIVEL_BASICA_ALTERNATIVA.equals(ie.getNivelModalidad().getIdCodigo()) || NIVEL_BASICA_ALTERNATIVA_INICIAL.equals(ie.getNivelModalidad().getIdCodigo()))
                    && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 4BI solamente corresponde al nivel Básico Alternativo - Inicial";
        } else if (CEDULA_04BA.equals(cedula.getNroced()) || "c4ba".equals(cedula.getNroced())) {
            ok = (NIVEL_BASICA_ALTERNATIVA_AVANZADA.equals(ie.getNivelModalidad().getIdCodigo()))
                    && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 4BA solamente corresponde al nivel Básico Alternativo - Avanzada";
        } else if (CEDULA_05B.equals(cedula.getNroced())) {
            ok = ((NIVEL_PEDAGOGICA.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 5B solamente corresponde al nivel Superior Pedagógico";
        } else if (CEDULA_06B.equals(cedula.getNroced())) {
            ok = ((NIVEL_TECNOLOGICA.equals(ie.getNivelModalidad().getIdCodigo())) || (NIVEL_PEDAGOGICA.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 6B solamente corresponde al nivel Superior Tecnológico";
        } else if (CEDULA_07B.equals(cedula.getNroced())) {
            ok = ((NIVEL_ARTISTICA.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 7B solamente corresponde al nivel Superior Artistico";
        } else if (CEDULA_08BI.equals(cedula.getNroced()) || "c08b".equals(cedula.getNroced())) {
            ok = ((NIVEL_ESPECIAL.equals(ie.getNivelModalidad().getIdCodigo())) || (NIVEL_ESPECIAL_INICIAL.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 8BI solamente corresponde al nivel Educación Especial Inicial";
        } else if (CEDULA_08BP.equals(cedula.getNroced())) {
            ok = (NIVEL_ESPECIAL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo())) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 8BP solamente corresponde al nivel Educación Especial Primaria";
        } else if (CEDULA_09B.equals(cedula.getNroced())) {
            ok = ((ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_OCUPACIONAL))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 9B solamente corresponde al nivel CETPRO";
        }

        if (!ok) {
            return mensaje;
        } else {
            return "";
        }


    }
	

}
