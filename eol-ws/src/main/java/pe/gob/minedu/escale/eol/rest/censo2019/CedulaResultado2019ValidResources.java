package pe.gob.minedu.escale.eol.rest.censo2019;

import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ARTISTICA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA_AVANZADA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA_INICIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ESPECIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ESPECIAL_INICIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_ESPECIAL_PRIMARIA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_INICIAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_INICIAL_NO_ESCOLARIZADO;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_OCUPACIONAL;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_PEDAGOGICA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_PRIMARIA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_SECUNDARIA;
import static pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE.NIVEL_TECNOLOGICA;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_01B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_02B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_03BP;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_03BS;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_04BA;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_04BI;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_05B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_06B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_07B;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_08BI;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_08BP;
import static pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera.CEDULA_09B;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

import pe.gob.minedu.escale.eol.converter.RespuestaConverter;
import pe.gob.minedu.escale.eol.domain.Constantes;
import pe.gob.minedu.escale.eol.domain.EolActividadesClient;
import pe.gob.minedu.escale.eol.domain.EolActividadesConverter;
import pe.gob.minedu.escale.eol.domain.EolCedulaConverter;
import pe.gob.minedu.escale.eol.domain.EolCedulasClient;
import pe.gob.minedu.escale.eol.domain.EolCedulasConverter;
import pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Detalle;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Fila;
import pe.gob.minedu.escale.eol.domain.domainPadron.CentroEducativo;

/**
 * 
 * @author WARODRIGUEZ
 * 
 */
@Path("/censo2019/resultado/valid")
@Stateless
public class CedulaResultado2019ValidResources {

//	 private static final Logger LOGGER = Logger.getLogger(CedulaResultado2019ValidResources.class.getName());
	private Logger logger = Logger.getLogger(CedulaResultado2019ValidResources.class.getName());

//	private ResourceBundle bundle = ResourceBundle
//			.getBundle("/pe/gob/minedu/escale/eol/rest/censo2019/valid/resource/etiquetas_resultado");

	private ResourceBundle bundle = ResourceBundle
			.getBundle("etiquetas_resultado");
	private static final String TIPDATO_TIPOPROG = "c02b.tipoprog";
	private static String warning_str = "\n- No se ha definido el valor '%1$s'  para el tipo de dato '%2$s'";

	private static String warning_str_formato = "Hay errores en los datos que esta enviando.\n Por favor, envienos su cédula por medio de correo electrónico a la siguiente dirección: censoescolar@minedu.gob.pe ";
	final static DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);
	private static String PERIODO = "2019";
	private static String NOMBRE_ACTIVIDAD = "CENSO-RESULTADO";

	Constantes consVERSION = null;

//	public RespuestaConverter validar(Resultado2019Cabecera cedula, InstitucionEducativaIE ie) {
//		LOGGER.info("VALIDAR 000 : " + cedula +"///"+ ie);
//		RespuestaConverter rpta = new RespuestaConverter();
//		StringBuffer msg = new StringBuffer();
//		String msgVPlazo = verificarPlazoEntrega(NOMBRE_ACTIVIDAD, PERIODO);
//		// String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO,
//		// cedula.getNivMod(), cedula.getVersion());
//		String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO, cedula.getNivMod(), "1");
//		String msgVCedula = verificarCedula(cedula, ie);
//		boolean esPronoei = false;
//
//		if (msgVPlazo.length() > 0) {
//			LOGGER.info("VALIDAR 1111111111 : " );
//			msg.append(msgVPlazo);
//			rpta.setMensaje(msg.toString());
//			rpta.setStatus("ERROR");
//			return rpta;
//		}
//
//		if (msgVVersion.length() > 0) {
//			LOGGER.info("VALIDAR 2222222222222 : " );
//			msg.append(msgVVersion);
//			rpta.setMensaje(msg.toString());
//			rpta.setStatus("ERROR");
//			return rpta;
//		}
//
//		if (msgVCedula.length() > 0) {
//			LOGGER.info("VALIDAR 333333333 : " );
//			msg.append(msgVCedula);
//			rpta.setMensaje(msg.toString());
//			rpta.setStatus("ERROR");
//			return rpta;
//		}
//
//		if (cedula.getAnexo() == null || cedula.getAnexo().isEmpty()) {
//			LOGGER.info("VALIDAR 4444444444 : " );
//			msg.append("No está declarado el anexo.");
//			rpta.setMensaje(msg.toString());
//			rpta.setStatus("ERROR");
//			return rpta;
//		}
//
//		if (!ie.getMcenso().equals("1")) {
//			LOGGER.info("VALIDAR 555555555555 : " );
//			msg.append(
//					"El estado de la Institución Educativo en el registro de padrón es inactivo, regularice su situación con el estadístico de su UGEL.");
//			rpta.setMensaje(msg.toString());
//			rpta.setStatus("ERROR");
//			return rpta;
//		}
//
//		esPronoei = ie.getNivelModalidad().getIdCodigo().equals(NIVEL_INICIAL_NO_ESCOLARIZADO);
//		if (!esPronoei && cedula.getCodlocal() != null && !cedula.getCodlocal().isEmpty()
//				&& !cedula.getCodlocal().equals(ie.getCodlocal())) {
//			LOGGER.info("VALIDAR 6666666666 : " );
//			msg.append(String.format("\nEl código de local de la I.E. %1s es  %2s", ie.getCentroEducativo(),
//					ie.getCodlocal()));
//		}
//
////		if (cedula.getDetalle() == null || cedula.getDetalle().isEmpty()) {
//		if (cedula.getResultado2019ResultadoList() == null || cedula.getResultado2019ResultadoList().isEmpty()) {
//			LOGGER.info("VALIDAR 7777777777 : " );
//			msg.append(warning_str_formato);
//			rpta.setMensaje(msg.toString());
//			rpta.setStatus("ERROR");
//			return rpta;
//		}
//
//		// if
//		// (!bundle.containsKey(TIPDATO_SITUACION.concat(".").concat(cedula.getSituacion())))
//		// {
//		// msg.append(String.format(warning_str, cedula.getSituacion(),
//		// "Situación del Director"));
//		// }
//
//		if (cedula.getNroced().equals(CEDULA_01B)) {
//		} else if (cedula.getNroced().equals(CEDULA_02B)) {
//			LOGGER.info("VALIDAR 8888888888 : " );
//			if (!bundle.containsKey(TIPDATO_TIPOPROG.concat(".").concat(cedula.getTipprog()))) {
//				msg.append(String.format(warning_str, cedula.getTipprog(), "Tipo de Programa"));
//			}
//		} else if (cedula.getNroced().equals(CEDULA_03BS)) {
//			LOGGER.info("VALIDAR 99999999999999 : " );
//
//			if (cedula.getNivMod().equals(NIVEL_SECUNDARIA)
//					&& !bundle.containsKey("c03b.F0.formaten.".concat(cedula.getFormaten()))) {
//				LOGGER.info("VALIDAR AAAAAAAAAAAAA : " );
//				msg.append(String.format(warning_str, cedula.getFormaten(), "Forma de atención"));
//			}
//		}
//
//		boolean esCed6B = cedula.getNroced().equals(CEDULA_06B);
//
//
//		
//		for (Resultado2019Detalle detalle : cedula.getResultado2019ResultadoList()) {
//			
//			if (detalle.getFilas() == null || detalle.getFilas().isEmpty() || detalle == null) {
//				msg.append(warning_str_formato);
//				rpta.setMensaje(msg.toString());
//				rpta.setStatus("ERROR");
//				return rpta;
//			}
//		}
//		
//		msg.append(validarTotales(cedula));
//		if (msg.length() > 0) {
//			LOGGER.info("VALIDAR BBBBBBBBBBBBB : " );
//			rpta.setMensaje(msg.toString());
//			rpta.setStatus("ERROR");
//		}
//
//		rpta.setMensaje(msg.toString());
//		return rpta;
//	}
//////////////////////////////********************************////////////////////////////////////
	public RespuestaConverter validar(Resultado2019Cabecera cedula, CentroEducativo ie) {
		logger.info(":: CedulaResultado2019ValidResources.validar :: Starting execution...");
		logger.info("VALIDAR 000 : " + cedula + "///" + ie);
		RespuestaConverter rpta = new RespuestaConverter();
		StringBuffer msg = new StringBuffer();
		String msgVPlazo = verificarPlazoEntrega(NOMBRE_ACTIVIDAD, PERIODO);
		String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO, cedula.getNivMod(), "1");
		String msgVCedula = verificarCedula(cedula, ie);
		boolean esPronoei = false;

		if (msgVPlazo.length() > 0) {
			logger.info("VALIDAR 1111111111 : ");
			msg.append(msgVPlazo);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (msgVVersion.length() > 0) {
			logger.info("VALIDAR 2222222222222 : ");
			msg.append(msgVVersion);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (msgVCedula.length() > 0) {
			logger.info("VALIDAR 333333333 : ");
			msg.append(msgVCedula);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (cedula.getAnexo() == null || cedula.getAnexo().isEmpty()) {
			logger.info("VALIDAR 4444444444 : ");
			msg.append("No está declarado el anexo.");
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (!ie.getMcenso().equals("1")) {
			logger.info("VALIDAR 555555555555 : ");
			msg.append(
					"El estado de la Institución Educativo en el registro de padrón es inactivo, regularice su situación con el estadístico de su UGEL.");
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		esPronoei = ie.getNivMod().equals(NIVEL_INICIAL_NO_ESCOLARIZADO);
		if (!esPronoei && cedula.getCodlocal() != null && !cedula.getCodlocal().isEmpty()
				&& !cedula.getCodlocal().equals(ie.getCodlocal())) {
			logger.info("VALIDAR 6666666666 : ");
			msg.append(String.format("\nEl código de local de la I.E. %1s es  %2s", ie.getCenEdu(), ie.getCodlocal()));
		}
		
//		if (cedula.getDetalle() == null || cedula.getDetalle().isEmpty()) {
		if (cedula.getResultado2019ResultadoList() == null || cedula.getResultado2019ResultadoList().isEmpty()) {
			logger.info("VALIDAR 7777777777 : ");
			msg.append(warning_str_formato);
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
			return rpta;
		}

		if (cedula.getNroced().equals(CEDULA_01B)) {
		} else if (cedula.getNroced().equals(CEDULA_02B)) {
			logger.info("VALIDAR 8888888888 : ");
			if (!bundle.containsKey(TIPDATO_TIPOPROG.concat(".").concat(cedula.getTipprog()))) {
				msg.append(String.format(warning_str, cedula.getTipprog(), "Tipo de Programa"));
			}
		} else if (cedula.getNroced().equals(CEDULA_03BS)) {
			logger.info("VALIDAR 99999999999999 : ");

			if (cedula.getNivMod().equals(NIVEL_SECUNDARIA)
					&& !bundle.containsKey("c03b.F0.formaten.".concat(cedula.getFormaten()))) {
				logger.info("VALIDAR AAAAAAAAAAAAA : ");
				msg.append(String.format(warning_str, cedula.getFormaten(), "Forma de atención"));
			}
		}

		//boolean esCed6B = cedula.getNroced().equals(CEDULA_06B);

		for (Resultado2019Detalle detalle : cedula.getResultado2019ResultadoList()) {

			if (detalle.getFilas() == null || detalle.getFilas().isEmpty() || detalle == null) {
				msg.append(warning_str_formato);
				rpta.setMensaje(msg.toString());
				rpta.setStatus("ERROR");
				return rpta;
			}
		}

		msg.append(validarTotales(cedula));
		if (msg.length() > 0) {
			logger.info("VALIDAR BBBBBBBBBBBBB : ");
			rpta.setMensaje(msg.toString());
			rpta.setStatus("ERROR");
		}

		rpta.setMensaje(msg.toString());
		
		logger.info(":: CedulaResultado2019ValidResources.validar :: Execution finish.");
		return rpta;
	}

    public RespuestaConverter validarRecupera(Resultado2019Cabecera cedula, InstitucionEducativaIE ie) {
        RespuestaConverter rpta = new RespuestaConverter();
        StringBuffer msg = new StringBuffer();
        String msgVPlazo = verificarPlazoEntrega(NOMBRE_ACTIVIDAD, PERIODO);
        //String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO, cedula.getNivMod(), cedula.getVersion());
        String msgVVersion = verificarVersion(NOMBRE_ACTIVIDAD, PERIODO, cedula.getNivMod(), "1");
        String msgVCedula = verificarCedulaRecup(cedula, ie);
        boolean esPronoei = false;


        if (msgVPlazo.length() > 0) {
            msg.append(msgVPlazo);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (msgVVersion.length() > 0) {
            msg.append(msgVVersion);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (msgVCedula.length() > 0) {
            msg.append(msgVCedula);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (cedula.getAnexo() == null || cedula.getAnexo().isEmpty()) {
            msg.append("No está declarado el anexo.");
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        if (!ie.getMcenso().equals("1")) {
            msg.append("El estado de la Institución Educativo en el registro de padrón es inactivo, regularice su situación con el estadístico de su UGEL.");
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

        esPronoei = ie.getNivelModalidad().getIdCodigo().equals(NIVEL_INICIAL_NO_ESCOLARIZADO);
        if (!esPronoei && cedula.getCodlocal() != null && !cedula.getCodlocal().isEmpty() && !cedula.getCodlocal().equals(ie.getCodlocal())) {
            msg.append(String.format("\nEl código de local de la I.E. %1s es  %2s", ie.getCentroEducativo(), ie.getCodlocal()));
        }

        if (cedula.getResultado2019ResultadoList() == null || cedula.getResultado2019ResultadoList().isEmpty()) {
            msg.append(warning_str_formato);
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
            return rpta;
        }

//        if (!bundle.containsKey(TIPDATO_SITUACION.concat(".").concat(cedula.getSituacion()))) {
//            msg.append(String.format(warning_str, cedula.getSituacion(), "Situación del Director"));
//        }

        if (cedula.getNroced().equals(CEDULA_01B)) {
        } else if (cedula.getNroced().equals(CEDULA_02B)) {
            if (!bundle.containsKey(TIPDATO_TIPOPROG.concat(".").concat(cedula.getTipprog()))) {
                msg.append(String.format(warning_str, cedula.getTipprog(), "Tipo de Programa"));
            }
        } else if (cedula.getNroced().equals(CEDULA_03BS)) {
            /*if (cedula.getNivMod().equals(NIVEL_PRIMARIA) && cedula.getSeanexo() == null) {
                msg.append("\nError interno, el campo 'SEANEXO' deberia tener un valor");
            }*/
            //no funciona Articulación 21-11-13
          //  if (cedula.getNivMod().equals(NIVEL_PRIMARIA) && cedula.getSearti() == null) {
             //   msg.append("\nError interno, el campo 'SEARTI' deberia tener un valor");
           // }

            if (cedula.getNivMod().equals(NIVEL_SECUNDARIA) && !bundle.containsKey("c03b.F0.formaten.".concat(cedula.getFormaten()))) {
                msg.append(String.format(warning_str, cedula.getFormaten(), "Forma de atención"));
            }
        }


        boolean esCed6B = cedula.getNroced().equals(CEDULA_06B);

//        Set<String> keys = cedula.getDetalle().keySet();

//        for (Iterator<String> it = keys.iterator(); it.hasNext();) {
//            String key = it.next();
            Resultado2019Detalle detalle = cedula.getResultado2019ResultadoList().get(0);


              if (detalle.getFilas() == null || detalle.getFilas().isEmpty()) {
                    msg.append(warning_str_formato);
                    rpta.setMensaje(msg.toString());
                    rpta.setStatus("ERROR");
                    return rpta;
                }

            /*
            List<Resultado2018Fila> filas = detalle.getFilas();
            for (Resultado2018Fila fila : filas) {
                String nomCuadro = key.toUpperCase().concat(".") ;
                if(esCed6B){
                    if(key.equals("C104")||key.equals("C105"))
                    nomCuadro = "" ;
                }

                String tipdato = cedula.getNroced().concat(".").concat(nomCuadro).concat("tipdato").concat(".").concat(fila.getTipdato());


                LOGGER.log(Level.INFO, tipdato);
                if (!bundle.containsKey(tipdato)) {
                    msg.append(String.format(warning_str_cuadro, fila.getTipdato(), key.toUpperCase()));
                }
            }*/
//        }

        msg.append(validarTotales(cedula));
        if (msg.length() > 0) {
            rpta.setMensaje(msg.toString());
            rpta.setStatus("ERROR");
        }

        rpta.setMensaje(msg.toString());
        return rpta;
    }
	
	
	public String validarTotales(Resultado2019Cabecera cedula) {
		String msgTot = "Los totales del cuadro %1s no son iguales a la suma de los detalles. "
				+ "Es posible que la cédula presente un error interno.\nPor favor, descargue nuevamente la cédula desde su tablero de control en la página ( http://escale.minedu.gob.pe/estadistica/ce/ ) .\nDisculpe las molestias ocasionadas.";

//		if (cedula.getDetalle() == null) {
		if (cedula.getResultado2019ResultadoList() == null) {
			return warning_str_formato;
		}
		//boolean hayTotal = false;
//		Set<String> keys = cedula.getDetalle().keySet();
//		Set<String> keys = cedula.getResultado2019ResultadoList().keySet();
//		for (Iterator<String> it = keys.iterator(); it.hasNext();) {
//			String key = it.next();
////			Resultado2019Detalle detalle = cedula.getDetalle().get(key);
//			Resultado2019Detalle detalle = cedula.getResultado2019ResultadoList().get(key);
//			// System.out.println("CUADRO:"+detalle.getCuadro());
//			detalle.setCedula(cedula);
//			List<Resultado2019Fila> filas = detalle.getFilas();
//			Integer total = 0;
//			Integer dTotal = 0;
//			Integer valTipDato = -1;
//
//			boolean isTot = false;
//			for (Resultado2019Fila fila : filas) {
//				fila.setDetalle(detalle);
//				try {
//					valTipDato = Integer.parseInt(fila.getTipdato());
//				} catch (NumberFormatException e) {
//					valTipDato = -1;
//				}
//
//				if (valTipDato == 0 && !isTot) {
//					total = total + IntN(fila.getDato01h()) + IntN(fila.getDato01m());
//					total = total + IntN(fila.getDato02h()) + IntN(fila.getDato02m());
//					total = total + IntN(fila.getDato03h()) + IntN(fila.getDato03m());
//					total = total + IntN(fila.getDato04h()) + IntN(fila.getDato04m());
//					total = total + IntN(fila.getDato05h()) + IntN(fila.getDato05m());
//					total = total + IntN(fila.getDato06h()) + IntN(fila.getDato06m());
//					total = total + IntN(fila.getDato07h()) + IntN(fila.getDato07m());
//					total = total + IntN(fila.getDato08h()) + IntN(fila.getDato08m());
//					total = total + IntN(fila.getDato09h()) + IntN(fila.getDato09m());
//					total = total + IntN(fila.getDato10h()) + IntN(fila.getDato10m());
//					isTot = true;
//				} else {
//					if (!((cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_03BP)
//							|| cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_03BS)) && key.equals("C101")
//							&& valTipDato.equals(new Integer(2)))) {// Se
//																	// excluye
//																	// el ítem 2
//																	// del
//																	// cuadro
//																	// C101
//						dTotal = dTotal + IntN(fila.getDato01h()) + IntN(fila.getDato01m());
//						dTotal = dTotal + IntN(fila.getDato02h()) + IntN(fila.getDato02m());
//						dTotal = dTotal + IntN(fila.getDato03h()) + IntN(fila.getDato03m());
//						dTotal = dTotal + IntN(fila.getDato04h()) + IntN(fila.getDato04m());
//						dTotal = dTotal + IntN(fila.getDato05h()) + IntN(fila.getDato05m());
//						dTotal = dTotal + IntN(fila.getDato06h()) + IntN(fila.getDato06m());
//						dTotal = dTotal + IntN(fila.getDato07h()) + IntN(fila.getDato07m());
//						dTotal = dTotal + IntN(fila.getDato08h()) + IntN(fila.getDato08m());
//						dTotal = dTotal + IntN(fila.getDato09h()) + IntN(fila.getDato09m());
//						dTotal = dTotal + IntN(fila.getDato10h()) + IntN(fila.getDato10m());
//					}
//				}
//			}
//			if (isTot && filas.size() > 0 && !(total.intValue() == dTotal.intValue())) {
//				return String.format(msgTot, key);
//			}
//			if (!isTot && (((!cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_08BI)
//					|| !cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_08BP)) && !key.equals("C101"))
//					&& (!cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_06B) && !key.equals("C106")))) {
//				return "No se está enviando el total en el cuadro ".concat(key).concat(
//						", verifique su cédula o envienos por medio de correo electrónico a la siguiente dirección: censoescolar@minedu.gob.pe, indicando el problema ");
//			}
//
//		}

		for (Resultado2019Detalle detalle : cedula.getResultado2019ResultadoList()) {

			// System.out.println("CUADRO:"+detalle.getCuadro());
			detalle.setCedula(cedula);
			List<Resultado2019Fila> filas = detalle.getFilas();
			Integer total = 0;
			Integer dTotal = 0;
			Integer valTipDato = -1;

			boolean isTot = false;
			for (Resultado2019Fila fila : filas) {
				fila.setDetalle(detalle);
				try {
					valTipDato = Integer.parseInt(fila.getTipdato());
				} catch (NumberFormatException e) {
					valTipDato = -1;
				}

				if (valTipDato == 0 && !isTot) {
					total = total + IntN(fila.getDato01h()) + IntN(fila.getDato01m());
					total = total + IntN(fila.getDato02h()) + IntN(fila.getDato02m());
					total = total + IntN(fila.getDato03h()) + IntN(fila.getDato03m());
					total = total + IntN(fila.getDato04h()) + IntN(fila.getDato04m());
					total = total + IntN(fila.getDato05h()) + IntN(fila.getDato05m());
					total = total + IntN(fila.getDato06h()) + IntN(fila.getDato06m());
					total = total + IntN(fila.getDato07h()) + IntN(fila.getDato07m());
					total = total + IntN(fila.getDato08h()) + IntN(fila.getDato08m());
					total = total + IntN(fila.getDato09h()) + IntN(fila.getDato09m());
					total = total + IntN(fila.getDato10h()) + IntN(fila.getDato10m());
					isTot = true;
				} else {
					if (!((cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_03BP)
							|| cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_03BS))
							&& detalle.getCuadro().equals("C101") && valTipDato.equals(new Integer(2)))) {// Se
																											// excluye
																											// el ítem 2
																											// del
																											// cuadro
																											// C101
						dTotal = dTotal + IntN(fila.getDato01h()) + IntN(fila.getDato01m());
						dTotal = dTotal + IntN(fila.getDato02h()) + IntN(fila.getDato02m());
						dTotal = dTotal + IntN(fila.getDato03h()) + IntN(fila.getDato03m());
						dTotal = dTotal + IntN(fila.getDato04h()) + IntN(fila.getDato04m());
						dTotal = dTotal + IntN(fila.getDato05h()) + IntN(fila.getDato05m());
						dTotal = dTotal + IntN(fila.getDato06h()) + IntN(fila.getDato06m());
						dTotal = dTotal + IntN(fila.getDato07h()) + IntN(fila.getDato07m());
						dTotal = dTotal + IntN(fila.getDato08h()) + IntN(fila.getDato08m());
						dTotal = dTotal + IntN(fila.getDato09h()) + IntN(fila.getDato09m());
						dTotal = dTotal + IntN(fila.getDato10h()) + IntN(fila.getDato10m());
					}
				}
			}
			if (isTot && filas.size() > 0 && !(total.intValue() == dTotal.intValue())) {
				return String.format(msgTot, detalle);
			}
			if (!isTot && (((!cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_08BI)
					|| !cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_08BP))
					&& !detalle.getCuadro().equals("C101"))
					&& (!cedula.getNroced().equals(Resultado2019Cabecera.CEDULA_06B)
							&& !detalle.getCuadro().equals("C106")))) {
				return "No se está enviando el total en el cuadro ".concat(detalle.getFilas().toString()).concat(
						", verifique su cédula o envienos por medio de correo electrónico a la siguiente dirección: censoescolar@minedu.gob.pe, indicando el problema ");
			}

		}

		return "";
	}

	public Integer IntN(Integer num) {
		return num != null ? num : 0;
	}

	public String verificarPlazoEntrega(String tipoDocumento, String anio) {
		EolActividadesClient actClient = new EolActividadesClient();
		EolActividadesConverter actConv = actClient.getActividadesConverter(tipoDocumento, anio);
		// EolActividadConverter act=null;

		if (actConv != null && actConv.getItems() != null && !actConv.getItems().isEmpty()) {
			// act = actConv.getItems().get(0);
			Date ahora = new Date();
			Date fechTe = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(actConv.getItems().get(0).getFechaTermino());
			c.add(Calendar.DATE, 1);
			fechTe = c.getTime();
			if (!((ahora.equals(actConv.getItems().get(0).getFechaInicio())
					|| ahora.after(actConv.getItems().get(0).getFechaInicio()))
					&& (ahora.equals(fechTe) || ahora.before(fechTe)))) {
				return ("El plazo de entrega electrónica para este tipo documento es desde el día "
						+ DATE_FORMAT.format(actConv.getItems().get(0).getFechaInicio()) + " hasta el día "
						+ DATE_FORMAT.format(actConv.getItems().get(0).getFechaTermino()));
			}
		}

		if (actConv.getItems().get(0) == null) {
			return "Aún no se ha establecido una fecha de entrega electrónica para este tipo de documento";
		}

		return "";
	}

	public String verificarVersion(String tipoDocumento, String anio, String nivel, String versionEnvio) {
		EolCedulasClient cedClient = new EolCedulasClient();
		logger.info("CEDULARESULTADO2019VALIDA verificarVersion() : " + nivel + "///" + anio + "///" + tipoDocumento);
		EolCedulasConverter cedConv = cedClient.getCedulasConverter(nivel, anio, tipoDocumento);
		EolCedulaConverter ced = null;
		logger.info("CEDULARESULTADO2019VALIDA verificarVersion() cedConv : " + cedConv);
		if (cedConv != null && cedConv.getItems() != null && !cedConv.getItems().isEmpty()) {
			logger.info("CEDULARESULTADO2019VALIDA verificarVersion() cedConv : 1111");
			ced = cedConv.getItems().get(0);
		}
		if (ced == null) {
			logger.info("CEDULARESULTADO2019VALIDA verificarVersion() cedConv : 2222");
			return "Aún no se ha establecido la descarga de la cédula";
		}
		System.out.println("VERSION:" + ced.getVersion());
		if (ced.getVersion() == null || ced.getVersion().isEmpty()) {
			logger.info("CEDULARESULTADO2019VALIDA verificarVersion() cedConv : 33333");
			return "Aún no se ha establecido una versión para la cédula del nivel '".concat(nivel).concat("'");
		}

		if (!ced.getVersion().equals(versionEnvio)) {
			logger.info("CEDULARESULTADO2019VALIDA verificarVersion() cedConv : 4444");
			return "La versión de este formato está desactualizada, para actualizarlo por favor ingrese a su tablero "
					+ "( http://escale.minedu.gob.pe/estadistica/ce/ )  y descargue el último formato.";
		}

		return "";
	}

//	public String verificarCedula(Resultado2019Cabecera cedula, InstitucionEducativaIE ie) {
//		String mensaje = "";
//		boolean ok = false;
//		if (CEDULA_01B.equals(cedula.getNroced())) {
//			ok = (cedula.getCodMod().equals(ie.getCodigoModular()))
//					&& (((ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_INICIAL))
//							&& (ie.getNivelModalidad().getIdCodigo().charAt(0) == cedula.getNivMod().charAt(0)))
//							|| (NIVEL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo())
//									&& (InstitucionEducativaIE.SI.equals(ie.getProgarti()))));
//			mensaje = "La Cédula 1B solamente corresponde al nivel inicial";
//			if (ok && !InstitucionEducativaIE.SI.equals(ie.getProgarti())
//					&& !(ie.getNivelModalidad().getIdCodigo().equals(cedula.getNivMod()))) {
//				ok = false;
//				mensaje = "Error en el nivel seleccionado, el nivel de la I.E es:"
//						.concat(ie.getNivelModalidad().getValor());
//			}
//		} else if (CEDULA_02B.equals(cedula.getNroced())) {
//			ok = (cedula.getCodMod().equals(ie.getCodigoModular()))
//					&& (ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_INICIAL_NO_ESCOLARIZADO));
//			mensaje = "La Cédula 2B solamente corresponde al nivel inicial no escolarizado";
//		} else if (CEDULA_03BP.equals(cedula.getNroced())) {
//			ok = (NIVEL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo()))
//					&& ie.getCodigoModular().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
//			mensaje = "La Cédula 3BP solamente corresponde al nivel primario";
//		} else if (CEDULA_03BS.equals(cedula.getNroced())) {
//			ok = (NIVEL_SECUNDARIA.equals(ie.getNivelModalidad().getIdCodigo()))
//					&& ie.getCodigoModular().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
//			mensaje = "La Cédula 3BS solamente corresponde al nivel secundario";
//		} else if (CEDULA_04BI.equals(cedula.getNroced()) || "c4bi".equals(cedula.getNroced())) {
//			ok = (NIVEL_BASICA_ALTERNATIVA.equals(ie.getNivelModalidad().getIdCodigo())
//					|| NIVEL_BASICA_ALTERNATIVA_INICIAL.equals(ie.getNivelModalidad().getIdCodigo()))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 4BI solamente corresponde al nivel Básico Alternativo - Inicial";
//		} else if (CEDULA_04BA.equals(cedula.getNroced()) || "c4ba".equals(cedula.getNroced())) {
//			ok = (NIVEL_BASICA_ALTERNATIVA_AVANZADA.equals(ie.getNivelModalidad().getIdCodigo()))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 4BA solamente corresponde al nivel Básico Alternativo - Avanzada";
//		} else if (CEDULA_05B.equals(cedula.getNroced())) {
//			ok = ((NIVEL_PEDAGOGICA.equals(ie.getNivelModalidad().getIdCodigo())))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 5B solamente corresponde al nivel Superior Pedagógico";
//		} else if (CEDULA_06B.equals(cedula.getNroced())) {
//			ok = ((NIVEL_TECNOLOGICA.equals(ie.getNivelModalidad().getIdCodigo()))
//					|| (NIVEL_PEDAGOGICA.equals(ie.getNivelModalidad().getIdCodigo())))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 6B solamente corresponde al nivel Superior Tecnológico";
//		} else if (CEDULA_07B.equals(cedula.getNroced())) {
//			ok = ((NIVEL_ARTISTICA.equals(ie.getNivelModalidad().getIdCodigo())))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 7B solamente corresponde al nivel Superior Artistico";
//		} else if (CEDULA_08BI.equals(cedula.getNroced()) || "c08b".equals(cedula.getNroced())) {
//			ok = ((NIVEL_ESPECIAL.equals(ie.getNivelModalidad().getIdCodigo()))
//					|| (NIVEL_ESPECIAL_INICIAL.equals(ie.getNivelModalidad().getIdCodigo())))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 8BI solamente corresponde al nivel Educación Especial Inicial";
//		} else if (CEDULA_08BP.equals(cedula.getNroced())) {
//			ok = (NIVEL_ESPECIAL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo()))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 8BP solamente corresponde al nivel Educación Especial Primaria";
//		} else if (CEDULA_09B.equals(cedula.getNroced())) {
//			ok = ((ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_OCUPACIONAL)))
//					&& (ie.getCodigoModular().equals(cedula.getCodMod()));
//			mensaje = "La Cédula 9B solamente corresponde al nivel CETPRO";
//		}
//
//		if (!ok) {
//			return mensaje;
//		} else {
//			return "";
//		}
//
//	}

	public String verificarCedula(Resultado2019Cabecera cedula, CentroEducativo ie) {
		String mensaje = "";
		boolean ok = false;
		if (CEDULA_01B.equals(cedula.getNroced())) {
			ok = (cedula.getCodMod().equals(ie.getCodMod())) && (((ie.getNivMod().startsWith(NIVEL_INICIAL))
					&& (ie.getNivMod().charAt(0) == cedula.getNivMod().charAt(0)))
					|| (NIVEL_PRIMARIA.equals(ie.getNivMod()) && (InstitucionEducativaIE.SI.equals(ie.getProgarti()))));
			mensaje = "La Cédula 1B solamente corresponde al nivel inicial";
			if (ok && !InstitucionEducativaIE.SI.equals(ie.getProgarti())
					&& !(ie.getNivMod().equals(cedula.getNivMod()))) {
				ok = false;
				mensaje = "Error en el nivel seleccionado, el nivel de la I.E es:".concat(ie.getNivMod());
			}
		} else if (CEDULA_02B.equals(cedula.getNroced())) {
			ok = (cedula.getCodMod().equals(ie.getCodMod()))
					&& (ie.getNivMod().startsWith(NIVEL_INICIAL_NO_ESCOLARIZADO));
			mensaje = "La Cédula 2B solamente corresponde al nivel inicial no escolarizado";
		} else if (CEDULA_03BP.equals(cedula.getNroced())) {
			ok = (NIVEL_PRIMARIA.equals(ie.getNivMod())) && ie.getCodMod().equals(cedula.getCodMod())
					&& ie.getAnexo().equals(cedula.getAnexo());
			mensaje = "La Cédula 3BP solamente corresponde al nivel primario";
		} else if (CEDULA_03BS.equals(cedula.getNroced())) {
			ok = (NIVEL_SECUNDARIA.equals(ie.getNivMod())) && ie.getCodMod().equals(cedula.getCodMod())
					&& ie.getAnexo().equals(cedula.getAnexo());
			mensaje = "La Cédula 3BS solamente corresponde al nivel secundario";
		} else if (CEDULA_04BI.equals(cedula.getNroced()) || "c4bi".equals(cedula.getNroced())) {
			ok = (NIVEL_BASICA_ALTERNATIVA.equals(ie.getNivMod())
					|| NIVEL_BASICA_ALTERNATIVA_INICIAL.equals(ie.getNivMod()))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 4BI solamente corresponde al nivel Básico Alternativo - Inicial";
		} else if (CEDULA_04BA.equals(cedula.getNroced()) || "c4ba".equals(cedula.getNroced())) {
			ok = (NIVEL_BASICA_ALTERNATIVA_AVANZADA.equals(ie.getNivMod()))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 4BA solamente corresponde al nivel Básico Alternativo - Avanzada";
		} else if (CEDULA_05B.equals(cedula.getNroced())) {
			ok = ((NIVEL_PEDAGOGICA.equals(ie.getNivMod()))) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 5B solamente corresponde al nivel Superior Pedagógico";
		} else if (CEDULA_06B.equals(cedula.getNroced())) {
			ok = ((NIVEL_TECNOLOGICA.equals(ie.getNivMod())) || (NIVEL_PEDAGOGICA.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 6B solamente corresponde al nivel Superior Tecnológico";
		} else if (CEDULA_07B.equals(cedula.getNroced())) {
			ok = ((NIVEL_ARTISTICA.equals(ie.getNivMod()))) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 7B solamente corresponde al nivel Superior Artistico";
		} else if (CEDULA_08BI.equals(cedula.getNroced()) || "c08b".equals(cedula.getNroced())) {
			ok = ((NIVEL_ESPECIAL.equals(ie.getNivMod())) || (NIVEL_ESPECIAL_INICIAL.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 8BI solamente corresponde al nivel Educación Especial Inicial";
		} else if (CEDULA_08BP.equals(cedula.getNroced())) {
			ok = (NIVEL_ESPECIAL_PRIMARIA.equals(ie.getNivMod())) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 8BP solamente corresponde al nivel Educación Especial Primaria";
		} else if (CEDULA_09B.equals(cedula.getNroced())) {
			ok = ((ie.getNivMod().startsWith(NIVEL_OCUPACIONAL))) && (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 9B solamente corresponde al nivel CETPRO";
		}

		if (!ok) {
			return mensaje;
		} else {
			return "";
		}
	}
	
    public String verificarCedulaRecup(Resultado2019Cabecera cedula, InstitucionEducativaIE ie) {
        String mensaje = "";
        boolean ok = false;
        if (CEDULA_01B.equals(cedula.getNroced())) {
            ok = (cedula.getCodMod().equals(ie.getCodigoModular()))
                    && (((ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_INICIAL))
                    && (ie.getNivelModalidad().getIdCodigo().charAt(0) == cedula.getNivMod().charAt(0)))
                    || (NIVEL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo())
                    && (InstitucionEducativaIE.SI.equals(ie.getProgarti()))));
                mensaje = "La Cédula 1B solamente corresponde al nivel inicial";
                   if(ok && !InstitucionEducativaIE.SI.equals(ie.getProgarti())
                   && !(ie.getNivelModalidad().getIdCodigo().equals(cedula.getNivMod()))){
                        ok=false;
                        mensaje = "Error en el nivel seleccionado, el nivel de la I.E es:".concat(ie.getNivelModalidad().getValor());
                    }
        } else if (CEDULA_02B.equals(cedula.getNroced())) {
            ok = (cedula.getCodMod().equals(ie.getCodigoModular()))
                    && (ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_INICIAL_NO_ESCOLARIZADO));
            mensaje = "La Cédula 2B solamente corresponde al nivel inicial no escolarizado";
        } else if (CEDULA_03BP.equals(cedula.getNroced())) {
            ok = (NIVEL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo()))
                    && ie.getCodigoModular().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
            mensaje = "La Cédula 3BP solamente corresponde al nivel primario";
        } else if (CEDULA_03BS.equals(cedula.getNroced())) {
            ok = (NIVEL_SECUNDARIA.equals(ie.getNivelModalidad().getIdCodigo()))
                    && ie.getCodigoModular().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
            mensaje = "La Cédula 3BS solamente corresponde al nivel secundario";
        } else if (CEDULA_04BI.equals(cedula.getNroced()) || "c4bi".equals(cedula.getNroced())) {
            ok = (NIVEL_BASICA_ALTERNATIVA.equals(ie.getNivelModalidad().getIdCodigo()) || NIVEL_BASICA_ALTERNATIVA_INICIAL.equals(ie.getNivelModalidad().getIdCodigo()))
                    && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 4BI solamente corresponde al nivel Básico Alternativo - Inicial";
        } else if (CEDULA_04BA.equals(cedula.getNroced()) || "c4ba".equals(cedula.getNroced())) {
            ok = (NIVEL_BASICA_ALTERNATIVA_AVANZADA.equals(ie.getNivelModalidad().getIdCodigo()))
                    && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 4BA solamente corresponde al nivel Básico Alternativo - Avanzada";
        } else if (CEDULA_05B.equals(cedula.getNroced())) {
            ok = ((NIVEL_PEDAGOGICA.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 5B solamente corresponde al nivel Superior Pedagógico";
        } else if (CEDULA_06B.equals(cedula.getNroced())) {
            ok = ((NIVEL_TECNOLOGICA.equals(ie.getNivelModalidad().getIdCodigo())) || (NIVEL_PEDAGOGICA.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 6B solamente corresponde al nivel Superior Tecnológico";
        } else if (CEDULA_07B.equals(cedula.getNroced())) {
            ok = ((NIVEL_ARTISTICA.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 7B solamente corresponde al nivel Superior Artistico";
        } else if (CEDULA_08BI.equals(cedula.getNroced()) || "c08b".equals(cedula.getNroced())) {
            ok = ((NIVEL_ESPECIAL.equals(ie.getNivelModalidad().getIdCodigo())) || (NIVEL_ESPECIAL_INICIAL.equals(ie.getNivelModalidad().getIdCodigo()))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 8BI solamente corresponde al nivel Educación Especial Inicial";
        } else if (CEDULA_08BP.equals(cedula.getNroced())) {
            ok = (NIVEL_ESPECIAL_PRIMARIA.equals(ie.getNivelModalidad().getIdCodigo())) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 8BP solamente corresponde al nivel Educación Especial Primaria";
        } else if (CEDULA_09B.equals(cedula.getNroced())) {
            ok = ((ie.getNivelModalidad().getIdCodigo().startsWith(NIVEL_OCUPACIONAL))) && (ie.getCodigoModular().equals(cedula.getCodMod()));
            mensaje = "La Cédula 9B solamente corresponde al nivel CETPRO";
        }

        if (!ok) {
            return mensaje;
        } else {
            return "";
        }


    }

}
