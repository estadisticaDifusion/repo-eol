package pe.gob.minedu.escale.eol.rest.censo2019;

import static pe.gob.minedu.escale.eol.enums.estadistica.ConstanteEnum.VERSION2019;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.eol.domain.Constantes;
import pe.gob.minedu.escale.eol.domain.EolActividadesClient;
import pe.gob.minedu.escale.eol.domain.EolActividadesConverter;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Local2019Sec112;
import pe.gob.minedu.escale.eol.domain.domainPadron.CentroEducativo;
import pe.gob.minedu.escale.eol.ejb.ConstanteLocal;
import pe.gob.minedu.escale.eol.ejb.censo2019.Local2019Local;
import pe.gob.minedu.escale.padron.ejb.facade.PadronLocal;

@Path("/censo2019/local")
public class CedulaLocal2019Service {

	private static final Logger logger = Logger.getLogger(CedulaLocal2019Service.class.getName());
	final static DateFormat DATE_FORMAT_SIGIED = new SimpleDateFormat("yyyy-MM-dd");

	@EJB
	Local2019Local localFacade = lookupLocal(Local2019Local.class);

	@EJB
	private ConstanteLocal constanteFacade = lookupConstante(ConstanteLocal.class);

	@EJB
	private PadronLocal padronFacade = lookupPadron(PadronLocal.class);

	private static final String CED2019 = "2019";
	private static final String NOMB_PROCESO_MATRICULA = "CENSO-LOCAL";
	private static final String ESTADO_ACTIVO_IE = "1";
	private static final String NROCED = "c11";
	private static final StringBuffer MSG_BF = new StringBuffer();
	final static DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);

	@POST
	@Path("/registrar")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	public Response registrar(Local2019Cabecera cedula) {

//        LOGGER.log(Level.INFO, String.format("**************INCIO DE REG. CED. LOCAL 2018: %1s ***********", cedula.getCodlocal()));
		String msg = verificarPlazoEntrega(NOMB_PROCESO_MATRICULA, CED2019);
		Constantes consVERSION = null;

		if (msg != null) {
			return Response.status(Response.Status.GONE).build(); // 410 -- Actividad finalizada
		}

		if (cedula.getToken() == 0 || cedula.getToken() == -1L) {
			return Response.status(Response.Status.UNAUTHORIZED).build(); // 401 -- No tiene autorización
		}
		if (cedula.getCodlocal() == null || cedula.getCodlocal().isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build(); // 400 -- No esta declarado el codigo de local
		}
		consVERSION = constanteFacade.devConstante(VERSION2019, NROCED);

		if (consVERSION == null) {
			String mensaje = "No se ha definido versión de este formato,\nPor favor vuelva a intentar más tarde."
					+ "\nDisculpe las molestias ocasionadas. ";
			cedula.setMsg(String.format(mensaje));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}
		if (!cedula.getVersion().equals(consVERSION.getDescConst())) {
			String mensaje = "La versión de este formato está desactualizada, para actualizarlo por favor ingrese a su tablero ( http://escale.minedu.gob.pe/estadistica/ce/ )  y descargue el último formato.";
			cedula.setMsg(String.format(mensaje));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		CentroEducativo ce = null;
		Local2019Sec112 secIE = null;
		String cmEnvio = "";
		String anxEnvio = "";

		if (cedula.getLocal2019Sec112List() != null) {
			secIE = cedula.getLocal2019Sec112List().get(0);
			cmEnvio = secIE.getP112Cm();
			anxEnvio = secIE.getP112Anx();
			// ce = null;
			ce = padronFacade.obtainByCodMod(cmEnvio, anxEnvio);
		} else {
			cedula.setMsg("Necesita enviar la sección 104!!");
			logger.log(Level.INFO, "********************MSG:".concat(cedula.getMsg()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		List<CentroEducativo> centros = padronFacade.getCodigosModularesByCodLocal(cedula.getCodlocal());

		if (ce == null) {
			cedula.setMsg(String.format(StatusValid.VALID_IE_NUL.reason, cmEnvio, anxEnvio));
			logger.log(Level.INFO, "********************MSG:".concat(cedula.getMsg()));
			cedula.setEstadoRpt(String.valueOf(StatusValid.VALID_IE_NUL.code));
			return Response.ok(cedula).build();
		}
		if (!ce.getEstado().equals(ESTADO_ACTIVO_IE)) {
			cedula.setMsg(String.format(StatusValid.VALID_IE_INACT.reason, cmEnvio));
			logger.log(Level.INFO, "********************MSG:".concat(cedula.getMsg()));
			cedula.setEstadoRpt(String.valueOf(StatusValid.VALID_IE_INACT.code));
			return Response.ok(cedula).build();
		}

		if (centros == null || centros.isEmpty()) {
			cedula.setMsg(String.format(StatusValid.VALID_IE_CODLOC_NUL.reason, cedula.getCodlocal()));
			logger.log(Level.INFO, "********************MSG:".concat(cedula.getMsg()));
			cedula.setEstadoRpt(String.valueOf(StatusValid.VALID_IE_CODLOC_NUL.code));
			return Response.ok(cedula).build();
		}

		List<Local2019Sec112> listSec104 = cedula.getLocal2019Sec112List();
		if (listSec104 == null) {
			cedula.setMsg(StatusValid.VALID_SEC104_NUL.reason);
			logger.log(Level.INFO, "********************MSG:".concat(cedula.getMsg()));
			cedula.setEstadoRpt(String.valueOf(StatusValid.VALID_SEC104_NUL.code));
			return Response.ok(cedula).build();
		}

		CentroEducativo $ce = new CentroEducativo();
		CentroEducativo $$ce;
		MSG_BF.delete(0, MSG_BF.length());
		for (int i = 0; i < listSec104.size(); i++) {
			Local2019Sec112 item = listSec104.get(i);
			String codmod = item.getP112Cm();
			String nivmod = item.getP112Nm();
			String anexo = item.getP112Anx();
			$ce.setCodMod(codmod);
			$ce.setAnexo(anexo);
			if (nivmod != null && !nivmod.equals("A5")) {// Excluye a los PRONOEI en la validación
				int $pos = centros.indexOf($ce);
				if ($pos >= 0) {
					$$ce = centros.get($pos);
					if ("1".equals($$ce.getEstado())) {
						if ($$ce.getNivMod().charAt(0) != nivmod.charAt(0)) {
							MSG_BF.append("\nEl nivel de la fila ").append(item.getP112Nro()).append(
									" de la sección 104 no coincide con el Padrón de Instituciones Educativas.");
						}
					} else {
						MSG_BF.append("\nUsted envió datos del código modular '").append(item.getP112Cm())
								.append("' con nivel '").append(item.getP112Nm())
								.append("' y este se encuentra cerrado según el Padrón de Instituciones Educativas.");
					}
				} else {
					MSG_BF.append("\nEl código modular de la fila ").append(item.getP112Nro())
							.append(" no está registrado en el Padrón de Instituciones Educativas.");
				}
			}
		}

		if (MSG_BF.length() > 0) {
			cedula.setMsg(MSG_BF.toString());
			logger.log(Level.INFO, "********************MSG:".concat(cedula.getMsg()));
			cedula.setEstadoRpt(String.valueOf(StatusValid.VALID_SEC104_DAT_ERROR.code));
			return Response.ok(cedula).build();
		}
		MSG_BF.delete(0, MSG_BF.length());
		List<Local2019Sec112> $seccion104 = new ArrayList<Local2019Sec112>(listSec104);
		Collections.sort($seccion104, comparator104);
		Local2019Sec112 $key104 = new Local2019Sec112();
		for (CentroEducativo ceValid : centros) {
			$key104.setP112Cm(ceValid.getCodMod());
			$key104.setP112Anx(ceValid.getAnexo());
			int pos = Collections.binarySearch($seccion104, $key104, comparator104);
			if ("1".equals(ce.getEstado())) {
				if (pos < 0) {
					MSG_BF.append("\nEn esta cédula no se ha declarado información de la Institución Educativa '")
							.append(ceValid.getCenEdu()).append("' (Código modular:").append(ceValid.getCodMod())
							.append(", Nivel:").append(ceValid.getNivMod())
							.append(") que se encuentra en el mismo local");
				}
			}
		}

		cedula.setFechaEnvio(new Date());

		if (MSG_BF.length() > 0) {
			cedula.setMsg(MSG_BF.toString());
			logger.log(Level.INFO, "********************MSG:".concat(cedula.getMsg()));
			cedula.setEstadoRpt(String.valueOf(StatusValid.VALID_SEC104_DAT_ERROR_CONS.code));
			return Response.ok(cedula).build();
		}
		cedula.setCodgeo(ce.getCodgeo());
		cedula.setCodugel(ce.getCodooii());
		cedula.setUltimo(Boolean.TRUE);
		localFacade.create(cedula);
		cedula.setEstadoRpt("1");
		logger.log(Level.INFO,
				String.format("**************FIN DE REG. CED. LOCAL 2019: %1s ***********", cedula.getCodlocal()));
		return Response.ok(cedula).build();
	}

	public enum StatusValid {
		VALID_IE_NUL(2, "No se encontró ninguna Institución Educativa con código modular %1s y anexo %2s  ."),
		VALID_IE_INACT(3,
				"El estado la Institución Educativo con código modular %1s en el registro de padrón es inactivo, regularice su situación con el estadístico de su UGEL."),
		VALID_IE_CODLOC_NUL(4,
				"El código de local %1s no está registrado en el Padrón de Instituciones Educativas. Por favor, coordine con el estadístico de su UGEL."),
		VALID_SEC104_NUL(5, "No se está enviando datos de la sección 104."), VALID_SEC104_DAT_ERROR(6, ""),
		VALID_SEC104_DAT_ERROR_CONS(7, ""), VALID_VERSION(8,
				"Esta cédula presenta un error interno.\nPor favor, descargue nuevamente la cédula desde su tablero de control en lapágina ( http://escale2.minedu.gob.pe/estadistica/ce/ ) .\nDisculpe las molestias ocasionadas.");
		private final int code;
		private final String reason;

		private StatusValid(int code, String reason) {
			this.code = code;
			this.reason = reason;
		}
	}

	static Comparator<Local2019Sec112> comparator104 = new Comparator<Local2019Sec112>() {

		public int compare(Local2019Sec112 o1, Local2019Sec112 o2) {
			return (o1.getP112Cm() + "-" + o1.getP112Anx()).compareTo(o2.getP112Cm() + "-" + o2.getP112Anx());
		}
	};

	@GET
	@Produces({ "application/json" })
	public Local2019Cabecera getMatricula(@QueryParam("codlocal") String codlocal) {
		return localFacade.findByCodLocal(codlocal);
	}

	public String verificarPlazoEntrega(String tipoDocumento, String anio) {
		EolActividadesClient actClient = new EolActividadesClient();
		EolActividadesConverter actConv = actClient.getActividadesConverter(tipoDocumento, anio);
		EolActividadConverter act = null;

		if (actConv != null && actConv.getItems() != null && !actConv.getItems().isEmpty()) {
			act = actConv.getItems().get(0);
		}

		Date ahora = new Date();
		Date fechLimite = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(act.getFechaLimite());
		c.add(Calendar.DATE, 1);
		// c.add(Calendar.HOUR_OF_DAY, 5); UTILIZADO SOLO PARA EL SERVIDOR EXTERNO
		fechLimite = c.getTime();

		if (act == null) {
			return "Aún no se ha establecido una fecha de entrega electrónica para este tipo de documento";
		}
		/*
		 * if (!((ahora.equals(act.getFechaInicio()) ||
		 * ahora.after(act.getFechaInicio())) && (ahora.equals(fechTe) ||
		 * ahora.before(fechTe)))) { return
		 * ("El plazo de entrega electrónica para este tipo documento es desde el día "
		 * + DATE_FORMAT.format(act.getFechaInicio()) + " hasta el día " +
		 * DATE_FORMAT.format(act.getFechaTermino())); }
		 */

		if (ahora.before(act.getFechaInicio()) || ahora.after(fechLimite)) {
			return ("El plazo de entrega electrónica para este tipo documento es desde el día "
					+ DATE_FORMAT.format(act.getFechaInicio()) + " hasta el día "
					+ DATE_FORMAT.format(act.getFechaTermino()));
		}
		return null;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupConstante(Class<T> clase) {

		String nameContextEjb = "java:global/eol-ws/ConstanteFacade!pe.gob.minedu.escale.eol.ejb.ConstanteLocal";

		try {

			javax.naming.Context c = new InitialContext();

			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {

			throw new RuntimeException(ne);
		}
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupLocal(Class<T> clase) {

		String nameContextEjb = "java:global/eol-ws/Local2019Facade!pe.gob.minedu.escale.eol.ejb.censo2019.Local2019Local";

		try {

			javax.naming.Context c = new InitialContext();

			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {

			throw new RuntimeException(ne);
		}
	}

// 	@SuppressWarnings({ "unchecked" })
// 	private <T> T lookupPlazo(Class<T> clase) {
// 	
// 																	
// 		String nameContextEjb = "java:global/eol-ws/PlazoFacade!pe.gob.minedu.escale.eol.ejb.PlazoLocal";
//
//
// 		try {
//
// 			javax.naming.Context c = new InitialContext();
// 		
// 			return (T) c.lookup(nameContextEjb);
// 		} catch (NamingException ne) {
// 	
// 			throw new RuntimeException(ne);
// 		}
// 	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupPadron(Class<T> clase) {

		String nameContextEjb = "java:global/eol-ws/PadronFacade!pe.gob.minedu.escale.padron.ejb.facade.PadronLocal";

		try {

			javax.naming.Context c = new InitialContext();

			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {

			throw new RuntimeException(ne);
		}
	}

}
