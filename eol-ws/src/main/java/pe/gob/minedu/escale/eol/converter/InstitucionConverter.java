/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;
import java.util.List;

import javax.persistence.EntityManager;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.domain.EolPadron;
//import pe.gob.minedu.escale.eol.padron.domain.Area;
//import pe.gob.minedu.escale.eol.padron.domain.Estado;
//import pe.gob.minedu.escale.eol.padron.domain.Forma;
//import pe.gob.minedu.escale.eol.padron.domain.Genero;
//import pe.gob.minedu.escale.eol.padron.domain.Gestion;
//import pe.gob.minedu.escale.eol.padron.domain.GestionDependencia;
//import pe.gob.minedu.escale.eol.padron.domain.InfoA;
//import pe.gob.minedu.escale.eol.padron.domain.InfoB;
//import pe.gob.minedu.escale.eol.padron.domain.InfoC;
//import pe.gob.minedu.escale.eol.padron.domain.InfoD;
//import pe.gob.minedu.escale.eol.padron.domain.InfoE;
//import pe.gob.minedu.escale.eol.padron.domain.InfoF;
//import pe.gob.minedu.escale.eol.padron.domain.InfoG;
//import pe.gob.minedu.escale.eol.padron.domain.InfoK;
//import pe.gob.minedu.escale.eol.padron.domain.InfoL;
//import pe.gob.minedu.escale.eol.padron.domain.InfoM;
//import pe.gob.minedu.escale.eol.padron.domain.InfoMEdad;
//import pe.gob.minedu.escale.eol.padron.domain.InfoMGrPeriodo;
//import pe.gob.minedu.escale.eol.padron.domain.InfoT;
//import pe.gob.minedu.escale.eol.padron.domain.Institucion;
//import pe.gob.minedu.escale.eol.padron.domain.NivelModalidad;
//import pe.gob.minedu.escale.eol.padron.domain.SituacionDirector;
//import pe.gob.minedu.escale.eol.padron.domain.Turno;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "institucion")
public class InstitucionConverter {

	private final Logger logger = Logger.getLogger(InstitucionConverter.class);

	private EolPadron entity;
//	private URI uri;
//	private int expandLevel;
//	private List<InfoA> infoInicial;
//	private List<InfoB> infoPrimaria;
//	private List<InfoF> infoSecundaria;
//	private List<InfoC> infoPrimariaAdultos;
//	private List<InfoD> infoBasicaAlternativa;
//	private List<InfoG> infoSecundariaAdultos;
//	private List<InfoK> infoISP;
//	private List<InfoT> infoIST;
//	private List<InfoM> infoSuperiorArtistica;
//	private List<InfoE> infoEducacionEspecial;
//	private List<InfoL> infoCETPRO;
//	private List<InfoMEdad> infoMatriculaEdad;
//	private List<InfoMGrPeriodo> infoMgrPeriodoList;
//
	public InstitucionConverter() {
		entity = new EolPadron();
	}
//
//	public InstitucionConverter(EolPadron entity, URI uri, int expandLevel, boolean isUriExtendable) {
//		logger.info(":: InstitucionConverter.constructor :: Starting execution...");
//		this.entity = entity;
//		this.uri = (isUriExtendable)
//				? UriBuilder.fromUri(uri).path(entity.getCodMod() + "/" + entity.getAnexo() + "/").build()
//				: uri;
//		this.expandLevel = expandLevel;
//		getGestionDependencia();
//		getDistrito();
//		getIgel();
//		getUgel();
//		getArea();
//		getAreaSig();
//		getTurno();
//		//getTipoprog();
//		getGestionIe();
//		logger.info(":: InstitucionConverter.constructor :: Execution finish.");
//	}
//
//	public InstitucionConverter(EolPadron entity, URI uri, int expandLevel) {
//		this(entity, uri, expandLevel, false);
//	}
//
//	/**
//	 * Returns the URI associated with this converter.
//	 *
//	 * @return the uri
//	 */
//	@XmlAttribute
//	public URI getUri() {
//		return uri;
//	}
//
//	/**
//	 * Sets the URI for this reference converter.
//	 *
//	 */
//	public void setUri(URI uri) {
//		this.uri = uri;
//	}
//
//	/**
//	 * Returns the DireccionRegional entity.
//	 *
//	 * @return an entity
//	 */
//	@XmlTransient
//	public EolPadron getEntity() {
//		if (entity.getCodMod() == null || entity.getAnexo() == null) {
//			InstitucionConverter converter = UriResolver.getInstance().resolve(InstitucionConverter.class, uri);
//			if (converter != null) {
//				entity = converter.getEntity();
//			}
//		}
//		return entity;
//	}
//
//	/**
//	 * Returns the resolved DireccionRegional entity.
//	 *
//	 * @return an resolved entity
//	 */
//	public EolPadron resolveEntity(EntityManager em) {
//		return entity;
//	}
//
//	public void setTipoigel(String tipoigel) {
//		entity.setTipoigel(tipoigel);
//	}
//
//	public void setTelefono(String telefono) {
//		entity.setTelefono(telefono);
//	}
//
//	public void setRegistro(String registro) {
//		entity.setRegistro(registro);
//	}
//
//	public void setProgdist(String progdist) {
//		entity.setProgdist(progdist);
//	}
//
//	public void setProgarti(String progarti) {
//		entity.setProgarti(progarti);
//	}
//
//	public void setMcenso(String mcenso) {
//		entity.setMcenso(mcenso);
//	}
//
//	public void setPagweb(String pagweb) {
//		entity.setPagweb(pagweb);
//	}
//
//	public void setGestionDependencia(GestionDependenciaConverter gestionDependencia) {
//		entity.setGestionDependencia(gestionDependencia != null ? gestionDependencia.getEntity() : null);
//	}
//
//	public void setFecharet(String fecharet) {
//		entity.setFecharet(fecharet);
//	}
//
//	public void setFechareg(String fechareg) {
//		entity.setFechareg(fechareg);
//	}
//
//	public void setEmail(String email) {
//		entity.setEmail(email);
//	}
//
//	public void setDirector(String director) {
//		entity.setDirector(director);
//	}
//
//	public void setDirCen(String dirCen) {
//		entity.setDirCen(dirCen);
//	}
//
//	public void setComenta(String comenta) {
//		entity.setComenta(comenta);
//	}
//
//	public void setCodlocal(String codlocal) {
//		entity.setCodlocal(codlocal);
//	}
//
//	public void setCodinst(String codinst) {
//		entity.setCodinst(codinst);
//	}
//
//	public void setDistrito(DistritoConverter distrito) {
//		entity.setDistrito(distrito != null ? distrito.getEntity() : null);
//	}
//
//	public void setCodccpp(String codccpp) {
//		entity.setCodccpp(codccpp);
//	}
//
//	public void setCodMod(String codMod) {
//		entity.setCodMod(codMod);
//	}
//
//	public void setCenPob(String cenPob) {
//		entity.setCenPob(cenPob);
//	}
//
//	public void setCenEdu(String cenEdu) {
//		entity.setCenEdu(cenEdu);
//	}
//
//	public void setAnexo(String anexo) {
//		entity.setAnexo(anexo);
//	}
//
//	@XmlElement
//	public String getTipoigel() {
//		return entity.getTipoigel();
//	}
//
//	@XmlElement
//	public String getTelefono() {
//		return entity.getTelefono();
//	}
//
//	@XmlElement
//	public String getRegistro() {
//		return entity.getRegistro();
//	}
//
//	@XmlElement
//	public String getProgdist() {
//		return entity.getProgdist();
//	}
//
//	@XmlElement
//	public String getProgarti() {
//		return entity.getProgarti();
//	}
//
//	@XmlElement
//	public String getMcenso() {
//		return entity.getMcenso();
//	}
//
//	@XmlElement
//	public String getProgise() {
//		return entity.getProgise();
//	}
//
//	@XmlElement
//	public String getPagweb() {
//		return entity.getPagweb();
//	}
//
//	@XmlElement
//	public NivelModalidad getNivelModalidad() {
//		if (expandLevel > 0) {
//			if (entity.getNivelModalidad() != null) {
//				return entity.getNivelModalidad();
//			}
//		}
//		return null;
//	}
//
//	public void setNivelModalidad(NivelModalidadConverter valor) {
//		entity.setNivelModalidad(valor != null ? valor.getEntity() : null);
//	}
//
//	@XmlElement
//	public GestionDependencia getGestionDependencia() {
//		if (expandLevel > 0) {
//			if (entity.getGestionDependencia() != null) {
//				return entity.getGestionDependencia();
//			}
//		}
//		return null;
//	}
//
//	@XmlElement
//	public DistritoConverter getDistrito() {
//		if (expandLevel > 0) {
//			if (entity.getDistrito() != null) {
//				return new DistritoConverter(entity.getDistrito(), uri.resolve("distrito/"), expandLevel - 1, false);
//			}
//		}
//		return null;
//	}
//
//	@XmlElement
//	public UgelConverter getUgel() {
//		if (expandLevel > 0) {
//			if (entity.getUgel() != null) {
//				return new UgelConverter(entity.getUgel(), uri.resolve("ugel/"), expandLevel - 1, false);
//			}
//		}
//		return null;
//	}
//
//	public void setUgel(UgelConverter ugel) {
//		entity.setUgel(ugel != null ? ugel.getEntity() : null);
//	}
//
//	@XmlElement
//	public IgelConverter getIgel() {
//		if (expandLevel > 0) {
//			if (entity.getIgel() != null) {
//				return new IgelConverter(entity.getIgel(), uri.resolve("igel/"), expandLevel - 1, false);
//			}
//		}
//		return null;
//	}
//
//	public void setIgel(IgelConverter ugel) {
//		entity.setIgel(ugel != null ? ugel.getEntity() : null);
//	}
//
//	@XmlElement
//	public Area getArea() {
//		if (expandLevel > 0) {
//			if (entity.getArea() != null) {
//				return entity.getArea();
//			}
//		}
//		return null;
//	}
//
//	public void setArea(AreaConverter ugel) {
//		entity.setArea(ugel != null ? ugel.getEntity() : null);
//	}
//
//	@XmlElement
//	public Turno getTurno() {
//		if (expandLevel > 0) {
//			if (entity.getTurno() != null) {
//				return entity.getTurno();
//			}
//		}
//		return null;
//	}
//
//	public void setTurno(TurnoConverter ugel) {
//		entity.setTurno(ugel != null ? ugel.getEntity() : null);
//	}
//
//	@XmlElement
//	public Area getAreaSig() {
//		if (expandLevel > 0) {
//			if (entity.getAreaSig() != null) {
//				return entity.getAreaSig();
//			}
//		}
//		return null;
//	}
//
//	public void setAreaSig(AreaConverter ugel) {
//		entity.setAreaSig(ugel != null ? ugel.getEntity() : null);
//	}
//
//	@XmlElement
//	public Estado getEstado() {
//		if (expandLevel > 0) {
//			if (entity.getEstado() != null) {
//				return entity.getEstado();
//			}
//		}
//		return null;
//	}
//
//	public void setEstado(EstadoConverter estado) {
//		entity.setEstado(estado != null ? estado.getEntity() : null);
//	}
//
//	@XmlElement
//	public Genero getGenero() {
//		if (expandLevel > 0) {
//			if (entity.getGenero() != null) {
//				return entity.getGenero();
//			}
//		}
//		return null;
//	}
//
//	public void setGenero(GeneroConverter item) {
//		entity.setGenero(item != null ? item.getEntity() : null);
//	}
//
//	@XmlTransient
//	public SituacionDirector getSituacionDirector() {
//		if (expandLevel > 0) {
//			if (entity.getSituacionDirector() != null) {
//				return entity.getSituacionDirector();
//			}
//		}
//		return null;
//	}
//
//	public void setSituacionDirector(SituacionDirectorConverter situacionDirector) {
//		entity.setSituacionDirector(situacionDirector != null ? situacionDirector.getEntity() : null);
//	}
//
//	@XmlElement
//	public String getFecharet() {
//		return entity.getFecharet();
//	}
//
//	@XmlElement
//	public String getFechareg() {
//		return entity.getFechareg();
//	}
//
//	@XmlElement
//	public String getEmail() {
//		return entity.getEmail();
//	}
//
//	@XmlElement
//	public String getDirector() {
//		return entity.getDirector();
//	}
//
//	@XmlElement
//	public String getDirCen() {
//		return entity.getDirCen();
//	}
//
//	@XmlElement
//	public String getComenta() {
//		return entity.getComenta();
//	}
//
//	@XmlElement
//	public String getCodlocal() {
//		return entity.getCodlocal();
//	}
//
//	@XmlElement
//	public String getCodinst() {
//		return entity.getCodinst();
//	}
//
//	@XmlElement
//	public String getCodccpp() {
//		return entity.getCodccpp();
//	}
//
//	@XmlElement
//	public String getCodMod() {
//		return entity.getCodMod();
//	}
//
//	@XmlElement
//	public String getCenPob() {
//		return entity.getCenPob();
//	}
//
//	@XmlElement
//	public String getCenEdu() {
//		return entity.getCenEdu();
//	}
//
//	@XmlElement
//	public String getAnexo() {
//		return entity.getAnexo();
//	}
//
//	@XmlElement
//	public String getLenEtnica() {
//		return entity.getLenEtnica();
//	}
//
//	@XmlElement
//	public Float getNlatCp() {
//		return entity.getNlatCp();
//	}
//
//	@XmlElement
//	public Float getNlongCp() {
//		return entity.getNlongCp();
//	}
//
//	@XmlElement
//	public Float getNlatIE() {
//		return entity.getNlatIE();
//	}
//
//	@XmlElement
//	public Float getNlongIE() {
//		return entity.getNlongIE();
//	}
//
//	@XmlElement
//	public Integer getNzoom() {
//		return entity.getNzoom();
//	}
//
//	@XmlElement
//	public Integer getAlt_cp() {
//		return entity.getAlt_cp();
//	}
//
//	public void setInfoInicial(List<InfoA> lista) {
//		infoInicial = lista;
//	}
//
//	@XmlElement(name = "inicial")
//	public List<InfoA> getInfoInicial() {
//		if (infoInicial != null && infoInicial.isEmpty()) {
//			return null;
//		}
//		return infoInicial;
//	}
//
//	@XmlElement(name = "primaria")
//	public List<InfoB> getInfoPrimaria() {
//		if (infoPrimaria != null && infoPrimaria.isEmpty()) {
//			return null;
//		}
//		return infoPrimaria;
//	}
//
//	public void setInfoPrimaria(List<InfoB> infoPrimaria) {
//		this.infoPrimaria = infoPrimaria;
//	}
//
//	@XmlElement(name = "secundaria")
//	public List<InfoF> getInfoSecundaria() {
//		if (infoSecundaria != null && infoSecundaria.isEmpty()) {
//			return null;
//		}
//		return infoSecundaria;
//	}
//
//	public void setInfoSecundaria(List<InfoF> infoSecundaria) {
//		this.infoSecundaria = infoSecundaria;
//	}
//
//	public void setForma(Forma forma) {
//		entity.setForma(forma);
//	}
//
//	@XmlElement
//	public Forma getForma() {
//		if (expandLevel > 0) {
//			if (entity.getForma() != null) {
//				return entity.getForma();
//			}
//		}
//		return null;
//	}
//
//	@XmlElement(name = "primaria_adultos")
//	// @XmlTransient
//	public List<InfoC> getInfoPrimariaAdultos() {
//		if (infoPrimariaAdultos != null && infoPrimariaAdultos.isEmpty()) {
//			return null;
//		}
//		return infoPrimariaAdultos;
//	}
//
//	public void setInfoPrimariaAdultos(List<InfoC> infoPrimariaAdultos) {
//		this.infoPrimariaAdultos = infoPrimariaAdultos;
//	}
//
//	@XmlElement(name = "basica_alternativa")
//	// @XmlTransient
//	public List<InfoD> getInfoBasicaAlternativa() {
//		if (infoBasicaAlternativa != null && infoBasicaAlternativa.isEmpty()) {
//			return null;
//		}
//		return infoBasicaAlternativa;
//	}
//
//	public void setInfoBasicaAlternativa(List<InfoD> infoBasicaAlternativa) {
//		this.infoBasicaAlternativa = infoBasicaAlternativa;
//	}
//
//	@XmlElement(name = "secundaria_adultos")
//	// @XmlTransient
//	public List<InfoG> getInfoSecundariaAdultos() {
//		if (infoSecundariaAdultos != null && infoSecundariaAdultos.isEmpty()) {
//			return null;
//		}
//		return infoSecundariaAdultos;
//	}
//
//	public void setInfoSecundariaAdultos(List<InfoG> infoSecundariaAdultos) {
//		this.infoSecundariaAdultos = infoSecundariaAdultos;
//	}
//
//	@XmlElement(name = "isp")
//	public List<InfoK> getInfoISP() {
//		if (infoISP != null && infoISP.isEmpty()) {
//			return null;
//		}
//		return infoISP;
//	}
//
//	public void setInfoISP(List<InfoK> infoISP) {
//		this.infoISP = infoISP;
//	}
//
//	@XmlElement(name = "ist")
//	public List<InfoT> getInfoIST() {
//		if (infoIST != null && infoIST.isEmpty()) {
//			return null;
//		}
//
//		return infoIST;
//	}
//
//	public void setInfoIST(List<InfoT> infoIST) {
//		this.infoIST = infoIST;
//	}
//
//	@XmlElement(name = "superior_artistica")
//	public List<InfoM> getInfoSuperiorArtistica() {
//		if (infoSuperiorArtistica != null && infoSuperiorArtistica.isEmpty()) {
//			return null;
//		}
//		return infoSuperiorArtistica;
//	}
//
//	public void setInfoSuperiorArtistica(List<InfoM> infoSuperiorArtistica) {
//		this.infoSuperiorArtistica = infoSuperiorArtistica;
//	}
//
//	@XmlElement(name = "educacion_especial")
//	public List<InfoE> getInfoEducacionEspecial() {
//		if (infoEducacionEspecial != null && infoEducacionEspecial.isEmpty()) {
//			return null;
//		}
//		return infoEducacionEspecial;
//	}
//
//	public void setInfoEducacionEspecial(List<InfoE> infoEducacionEspecial) {
//		this.infoEducacionEspecial = infoEducacionEspecial;
//	}
//
//	@XmlElement(name = "cetpro")
//	public List<InfoL> getInfoCETPRO() {
//		if (infoCETPRO != null && infoCETPRO.isEmpty()) {
//			return null;
//		}
//		return infoCETPRO;
//	}
//
//	public void setInfoCETPRO(List<InfoL> infoCETPRO) {
//		this.infoCETPRO = infoCETPRO;
//	}
//
//	@XmlElement(name = "localidad")
//	public String getLocalidad() {
//		return entity.getLocalidad();
//	}
//
//	@XmlElement(name = "fte_cp")
//	public String getFte_cp() {
//		return entity.getFte_cp();
//	}
//
//	@XmlElement(name = "caracteristica")
//	public String getCaracteristica() {
//		return entity.getCaracteristica() != null ? entity.getCaracteristica().getValor() : "";
//	}
//
//	@XmlElement(name = "gestion")
//	public String getGestion() {
//		return entity.getGestion() != null ? entity.getGestion().getValor() : "";
//	}
//
//	@XmlElement(name = "dis_vrae")
//	public String getDisVrae() {
//		return entity.getDisVrae() != null ? entity.getDisVrae().getValor() : "";
//	}
//
//	@XmlElement(name = "dis_juntos")
//	public String getDisJuntos() {
//		return entity.getDisJuntos() != null ? entity.getDisJuntos().getValor() : "";
//	}
//
//	@XmlElement(name = "dis_crecer")
//	public String getDisCrecer() {
//		return entity.getDisCrecer() != null ? entity.getDisCrecer().getValor() : "";
//	}
//
//	@XmlElement(name = "medad")
//	public List<InfoMEdad> getInfoMatriculaEdad() {
//		if (infoMatriculaEdad != null && infoMatriculaEdad.isEmpty()) {
//			return null;
//		}
//		return infoMatriculaEdad;
//	}
//
//	public void setInfoMatriculaEdad(List<InfoMEdad> infoMatriculaEdad) {
//		this.infoMatriculaEdad = infoMatriculaEdad;
//	}
//
//	@XmlElement(name = "mgperiodo")
//	public List<InfoMGrPeriodo> getInfoMgrPeriodoList() {
//		if (infoMgrPeriodoList != null && infoMgrPeriodoList.isEmpty()) {
//			return null;
//		}
//		return infoMgrPeriodoList;
//	}
//
//	public void setInfoMgrPeriodoList(List<InfoMGrPeriodo> infoMgrPeriodoList) {
//		this.infoMgrPeriodoList = infoMgrPeriodoList;
//	}
//
//	@XmlElement
//	public String getSienvio() {
//		return entity.getSienvio();
//	}
//
//	public void seSienvio(String sienvio) {
//		entity.setSienvio(sienvio);
//	}
//
//	@XmlElement(name = "gestionie")
//	public Gestion getGestionIe() {
//		if (expandLevel > 0) {
//			if (entity.getGestion() != null) {
//				return entity.getGestion();
//			}
//		}
//		return null;
//	}
//
//	public void setGestionIe(GestionConverter gestion) {
//		entity.setGestion(gestion != null ? gestion.getEntity() : null);
//	}
//	
}
