/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.domain.EolPeriodo;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "periodo")
public class PeriodoConverter {

    private EolPeriodo entity;
    private URI uri;
    private int expandLevel;

    public PeriodoConverter() {
        entity = new EolPeriodo();
    }

    public PeriodoConverter(EolPeriodo entity, URI uri, int expandLevel, boolean isUriExtendible) {
        this.entity = entity;
        this.uri = uri;//(isUriExtendible) ? UriBuilder.fromUri(uri).path(entity.getId() + "/").build() : uri;
        this.expandLevel = expandLevel;
    }

    public PeriodoConverter(EolPeriodo entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    @XmlElement
    public Integer getId() {
        return entity.getId();
    }

    @XmlElement
    public String getAnio() {
        return entity.getAnio();

    }

    @XmlElement
    public String getDescripcion() {
        return "decripcion";//entity.getDescripcion();
    }

    @XmlTransient
    public EolPeriodo getEntity() {
        return entity;
    }

    public void setEntity(EolPeriodo entity) {
        this.entity = entity;
    }

}
