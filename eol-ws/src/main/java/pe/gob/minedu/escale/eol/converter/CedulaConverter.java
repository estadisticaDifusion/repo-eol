/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.domain.EolCedula;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedula")
public class CedulaConverter {

    private EolCedula entity;
    private URI uri;
    private int expandLevel;

    public CedulaConverter() {
    }

    public CedulaConverter(EolCedula entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getId() + "/").build() : uri;
        this.expandLevel = expandLevel;
    }

    public CedulaConverter(EolCedula entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    @XmlElement
    public Long getId() {
        return entity.getId();
    }

    @XmlElement
    public String getNombre() {
        return entity.getNombre();
    }

    @XmlElement
    public boolean getEstado() {
        return entity.getEstado();
    }

    @XmlElement
    public String getNivel() {
        return entity.getNivel();
    }

    @XmlElement
    public String getVersion() {
        return entity.getVersion();
    }

    @XmlElement
    public ActividadConverter getActividad() {
        return new ActividadConverter(entity.getEolActividad(), uri, expandLevel);
    }

}
