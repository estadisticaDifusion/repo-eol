package pe.gob.minedu.escale.eol.rest.censo2019;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.eol.domain.Constantes;
import pe.gob.minedu.escale.eol.domain.EolActividadesClient;
import pe.gob.minedu.escale.eol.domain.EolActividadesConverter;
import pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Cabecera;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019MatriculaFila;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019Muledad;
import pe.gob.minedu.escale.eol.domain.censo2019.Matricula2019SeccionFila;
import pe.gob.minedu.escale.eol.domain.domainPadron.CentroEducativo;
import pe.gob.minedu.escale.eol.ejb.ConstanteLocal;
import pe.gob.minedu.escale.eol.ejb.censo2019.Matricula2019Facade;
import pe.gob.minedu.escale.eol.ejb.censo2019.Matricula2019Local;
import pe.gob.minedu.escale.eol.enums.estadistica.ConstanteEnum;
import pe.gob.minedu.escale.padron.ejb.facade.PadronLocal;

@Path("/censo2019/matricula")
public class CedulaMatricula2019Service {

	private Logger logger = Logger.getLogger(CedulaMatricula2019Service.class);
	private static final String CED2019 = "2019";
	private static final String tipDatoTot = "00";

	final static DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);
	final static DateFormat DATE_FORMAT_SIGIED = new SimpleDateFormat("yyyy-MM-dd");

	@EJB
	private Matricula2019Local matricula2019Facade = lookupMatricula(Matricula2019Local.class);

	@EJB
	private ConstanteLocal constanteFacade = lookup(ConstanteLocal.class);

	@EJB
	private PadronLocal padronFacade = lookupPadron(PadronLocal.class);

	/**
	 * Registra una cédula de resultado vía REST
	 * 
	 * @param cedula La cédula de resultado
	 * @return un estado de respuesta según sea la situación de la recepción
	 */
	@POST
	@Path("/registrar")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	public Response registrar(Matricula2019Cabecera cedula) {

		System.out.println("Registrando CENSO 2019");
		String msg = verificarPlazoEntrega(Matricula2019Facade.CEDULA_MATRICULA, CED2019);
		String mensaje = "";

//        System.out.println("MSG :: "+msg);
		StringBuilder msgValid = new StringBuilder();
		Constantes constanteVersion = null;
		boolean esPronoei = false;
		System.out.println("#############INICIO MATRICULA2019##################");
		if (msg != null) {
			return Response.status(Response.Status.GONE).build(); // 410 -- Actividad finalizada
		}

		if (cedula.getToken() == 0 || cedula.getToken() == -1L) {
			return Response.status(Response.Status.UNAUTHORIZED).build(); // 401 -- No tiene autorización
		}
		if (cedula.getAnexo() == null || cedula.getAnexo().isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build(); // 400 -- No esta declarado el anexo
		}

		CentroEducativo ie = null;
		try {
			ie = padronFacade.obtainByCodMod(cedula.getCodMod(), cedula.getAnexo());
			if (ie == null) {
				mensaje = "No se encontró ninguna Institución Educativa con código modular %1s y anexo %2s  .";
				cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
				cedula.setEstadoRpt(String.valueOf(-1L));
				return Response.ok(cedula).build();
			}
		} catch (Exception e) {
			mensaje = "Esta cédula presenta un error interno en el servicio de busqueda del padron de IIEE."
					+ "\nPor favor vuelva a intentar más tarde." + "\nDisculpe las molestias ocasionadas. ";
			cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		cedula.setCodugel(ie.getCodooii());
		// Validacion de Institucion obligada a enviar por EOL - Inicio
		if (!ie.getSienvio().equals("2")) {
			mensaje = "Esta institución educativa no esta habilitada para reportar sus datos mediante los formatos electrónicos - excel.";
			cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}
		// Validacion de Institucion obligada a enviar por EOL - Fin
		if (!ie.getMcenso().equals("1")) {
			mensaje = "La institución no se encuentra dentro del marco censal, regularice su situación con el estadístico de su UGEL.";
			cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		if (!ie.getGestion().equals(cedula.getGestion())) {
			mensaje = "Verfique la gestión de la Institución para su adecuado registro.";
			cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		if (ie.getTipoice().startsWith("2")) {
			mensaje = "Los centros de apoyo no remites información del censo.!!";
			cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		if (cedula.getNivMod() == null || cedula.getNivMod().isEmpty()) {
			mensaje = "Esta cédula presenta un error interno en el envio del nivel educativo, \n por favor ingrese a su tablero ( http://escale.minedu.gob.pe/estadistica/ce/ )  y descargue el último formato.!!";

			cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

//        esPronoei = ie.getNivMod().equals(NIVEL_INICIAL_NO_ESCOLARIZADO);
		esPronoei = ie.getNivMod().equals(InstitucionEducativaIE.NIVEL_INICIAL_NO_ESCOLARIZADO);

		if (!esPronoei && (cedula.getCodlocal() != null && !cedula.getCodlocal().isEmpty()
				&& !cedula.getCodlocal().equals(ie.getCodlocal()))) {
			mensaje = String.format("El código de local de la I.E. %1s es  %2s", ie.getCenEdu(), ie.getCodlocal());
			cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		constanteVersion = constanteFacade.devConstante(ConstanteEnum.VERSION2019, cedula.getNroced());

		logger.info(" DESPUES DE DEVCONSTANTE : " + constanteVersion);

		if (constanteVersion == null) {
			mensaje = "No se ha definido versión de este formato,\nPor favor vuelva a intentar más tarde."
					+ "\nDisculpe las molestias ocasionadas. ";
			cedula.setMsg(String.format(mensaje));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		if (!cedula.getVersion().equals(constanteVersion.getDescConst())) {
			mensaje = "La versión de este formato está desactualizada, para actualizarlo por favor ingrese a su tablero ( http://escale.minedu.gob.pe/estadistica/ce/ )  y descargue el último formato.";
			cedula.setMsg(String.format(mensaje));
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		boolean ok = false;
		if (Matricula2019Cabecera.CEDULA_01A.equals(cedula.getNroced())) {
			ok = cedula.getCodMod().equals(ie.getCodMod()) && (ie.getNivMod().equals(cedula.getNivMod())
					&& !(InstitucionEducativaIE.SI.equals(ie.getProgarti())));

			if (!(InstitucionEducativaIE.SI.equals(ie.getProgarti())) && cedula.getCodMod().equals(ie.getCodMod())
					&& !ie.getNivMod().equals(cedula.getNivMod())) {
				Constantes cNivMod1 = constanteFacade.devConstante(ConstanteEnum.NIVEL_Y_O_MODALIDAD,
						cedula.getNivMod());
				Constantes cNivMod2 = constanteFacade.devConstante(ConstanteEnum.NIVEL_Y_O_MODALIDAD, ie.getNivMod());
				mensaje = String.format(String.format(
						"Ud. selecciono %s debe seleccionar %s. Para modificar su nivel debe coordinar con el estadístico de su UGEL!!",
						cNivMod1.getDescConst().concat("(" + cNivMod1.getCodConst() + ")"),
						cNivMod2.getDescConst().concat("(" + cNivMod2.getCodConst() + ")")));
				cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
				cedula.setEstadoRpt(String.valueOf(-1L));
				return Response.ok(cedula).build();
			}
			if (InstitucionEducativaIE.SI.equals(ie.getProgarti()) && cedula.getCodMod().equals(ie.getCodMod())
					&& !(InstitucionEducativaIE.NIVEL_INICIAL_ARTICULACION.equals(cedula.getNivMod()))) {
				Constantes cNivMod = constanteFacade.devConstante(ConstanteEnum.NIVEL_Y_O_MODALIDAD,
						InstitucionEducativaIE.NIVEL_INICIAL_ARTICULACION);
				mensaje = String.format(String.format("Seleccionar en nivel %s!!",
						cNivMod.getDescConst().concat("(" + cNivMod.getCodConst() + ")")));
				cedula.setMsg(String.format(mensaje, cedula.getCodMod(), cedula.getAnexo()));
				cedula.setEstadoRpt(String.valueOf(-1L));
				return Response.ok(cedula).build();
			}
			ok = (cedula.getCodMod().equals(ie.getCodMod()))
					&& (((ie.getNivMod().startsWith(InstitucionEducativaIE.NIVEL_INICIAL))
							&& (ie.getNivMod().charAt(0) == cedula.getNivMod().charAt(0)))
							|| (InstitucionEducativaIE.NIVEL_PRIMARIA.equals(ie.getNivMod())
									&& (InstitucionEducativaIE.SI.equals(ie.getProgarti()))));

			mensaje = "La Cédula 1A solamente corresponde al nivel inicial";
		}

		else if (Matricula2019Cabecera.CEDULA_02A.equals(cedula.getNroced())) {
			ok = (cedula.getCodMod().equals(ie.getCodMod()))
					&& (ie.getNivMod().startsWith(InstitucionEducativaIE.NIVEL_INICIAL_NO_ESCOLARIZADO));
			mensaje = "La Cédula 2A solamente corresponde al nivel inicial no escolarizado";
		} else if (Matricula2019Cabecera.CEDULA_03AP.equals(cedula.getNroced())) {
			ok = (InstitucionEducativaIE.NIVEL_PRIMARIA.equals(ie.getNivMod()))
					&& ie.getCodMod().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
			mensaje = "La Cédula 3AP solamente corresponde al nivel primario";
			// INICIO MODIFICACIÓN INCIDENCIA 853
			if (ok && !ie.getNivMod().equals(cedula.getNivMod())) {
				ok = false;
				mensaje = "El nivel educativo seleccionado no corresponde a esta institución educativa, por favor corrija!!";
			}
			// FINAL MODIFICACIÓN INCIDENCIA 853
		} else if (Matricula2019Cabecera.CEDULA_03AS.equals(cedula.getNroced())) {
			ok = (InstitucionEducativaIE.NIVEL_SECUNDARIA.equals(ie.getNivMod()))
					&& ie.getCodMod().equals(cedula.getCodMod()) && ie.getAnexo().equals(cedula.getAnexo());
			mensaje = "La Cédula 3AP solamente corresponde al nivel secundario";
			if (ok && !ie.getNivMod().equals(cedula.getNivMod())) {
				ok = false;
				mensaje = "El nivel educativo seleccionado no corresponde a esta institución educativa, por favor corrija!!";
			}
		} else if (Matricula2019Cabecera.CEDULA_04A.equals(cedula.getNroced())) {
			ok = (InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA.equals(ie.getNivMod()))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 4A solamente corresponde al nivel Básico Alternativo";
		} else if (Matricula2019Cabecera.CEDULA_05A.equals(cedula.getNroced())) {
			ok = ((InstitucionEducativaIE.NIVEL_PEDAGOGICA.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 5A solamente corresponde al nivel Superior Pedagógico";
		} else if (Matricula2019Cabecera.CEDULA_06A.equals(cedula.getNroced())) {
			ok = ((InstitucionEducativaIE.NIVEL_TECNOLOGICA.equals(ie.getNivMod()))
					|| (InstitucionEducativaIE.NIVEL_PEDAGOGICA.equals(ie.getNivMod())
							&& (InstitucionEducativaIE.SI.equals(ie.getProgise()))))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 6A solamente corresponde al nivel Superior Tecnológico";
		} else if (Matricula2019Cabecera.CEDULA_07A.equals(cedula.getNroced())) {
			ok = ((InstitucionEducativaIE.NIVEL_ARTISTICA.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 7A solamente corresponde al nivel Superior Artistico";
		} else if (Matricula2019Cabecera.CEDULA_08AI.equals(cedula.getNroced())) {
			ok = ((InstitucionEducativaIE.NIVEL_ESPECIAL_INICIAL.equals(ie.getNivMod())
					|| InstitucionEducativaIE.NIVEL_ESPECIAL.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 8AI solamente corresponde al nivel Educación Especial - Inicial";
		} else if (Matricula2019Cabecera.CEDULA_08AP.equals(cedula.getNroced())) {
			ok = ((InstitucionEducativaIE.NIVEL_ESPECIAL_PRIMARIA.equals(ie.getNivMod())))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 8AP solamente corresponde al nivel Educación Especial - Primaria";
		} else if (Matricula2019Cabecera.CEDULA_09A.equals(cedula.getNroced())) {
			ok = ((ie.getNivMod().startsWith(InstitucionEducativaIE.NIVEL_OCUPACIONAL)))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 9A solamente corresponde al nivel CETPRO";
		} else if (Matricula2019Cabecera.CEDULA_04AI.equals(cedula.getNroced())) {
			ok = (InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA_INICIAL.equals(ie.getNivMod()))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 4AI solamente corresponde al nivel Básico Alternativo Inicial - Intermedio";
		} else if (Matricula2019Cabecera.CEDULA_04AA.equals(cedula.getNroced())) {
			ok = (InstitucionEducativaIE.NIVEL_BASICA_ALTERNATIVA_AVANZADA.equals(ie.getNivMod()))
					&& (ie.getCodMod().equals(cedula.getCodMod()));
			mensaje = "La Cédula 4AA solamente corresponde al nivel Básico Alternativo Avanzado";
		}

		if (!ok) {

			cedula.setMsg(mensaje);
			cedula.setEstadoRpt(String.valueOf(-1L));
			return Response.ok(cedula).build();
		}

		if (Matricula2019Cabecera.CEDULA_01A.equals(cedula.getNroced())) {

			long totalAlum = 0L;
			long totalSecc = 0L;
			long s0 = 0L, s1 = 0L, s2 = 0L, s3 = 0L, s4 = 0L, s5 = 0L, s6 = 0L; //, s7 = 0L
			long m0 = 0L, m1 = 0L, m2 = 0L, m3 = 0L, m4 = 0L, m5 = 0L, m6 = 0L, m7 = 0L;

//            try{
			List<Matricula2019MatriculaFila> mFilas = cedula.getDetalleMatricula().stream()
					.filter(element -> element.getCuadro().equals("C201")).findFirst().get()
					.getMatricula2019MatriculaFilaList(); // cambio
//            }catch (Exception e) {
			// TODO: handle exception
//			};

			// List<Matricula2018MatriculaFila> mFilas =
			// cedula.getDetalleMatricula().get("C201").getMatricula2018MatriculaFilaList();
			// // cambio

			List<Matricula2019SeccionFila> sFilas = cedula.getDetalleSeccion().stream()
					.filter(element -> element.getCuadro().equals("C204")).findFirst().get()
					.getMatricula2019SeccionFilaList();
			// List<Matricula2018SeccionFila> sFilas =
			// cedula.getDetalleSeccion().get("C204").getMatricula2018SeccionFilaList();

			// cambio

			for (Matricula2019MatriculaFila f : mFilas) {
				m0 = (f.getDato01() != null ? f.getDato01() : 0) + (f.getDato02() != null ? f.getDato02() : 0);
				m1 = (f.getDato03() != null ? f.getDato03() : 0) + (f.getDato04() != null ? f.getDato04() : 0);
				m2 = (f.getDato05() != null ? f.getDato05() : 0) + (f.getDato06() != null ? f.getDato06() : 0);
				m3 = (f.getDato07() != null ? f.getDato07() : 0) + (f.getDato08() != null ? f.getDato08() : 0);
				m4 = (f.getDato09() != null ? f.getDato09() : 0) + (f.getDato10() != null ? f.getDato10() : 0);
				m5 = (f.getDato11() != null ? f.getDato11() : 0) + (f.getDato12() != null ? f.getDato12() : 0);
				m6 = (f.getDato13() != null ? f.getDato13() : 0) + (f.getDato14() != null ? f.getDato14() : 0);
				// m7 = f.getDato15() + f.getDato16();
				totalAlum = m0 + m1 + m2 + m3 + m4 + m5 + m6;// + m7
				break;
			}

			for (Matricula2019SeccionFila s : sFilas) {
				if (s.getTipdato().equals(tipDatoTot)) {
					s0 = (s.getDato01() != null ? s.getDato01() : 0);
					s1 = (s.getDato02() != null ? s.getDato02() : 0);
					s2 = (s.getDato03() != null ? s.getDato03() : 0);
					s3 = (s.getDato04() != null ? s.getDato04() : 0);
					s4 = (s.getDato05() != null ? s.getDato05() : 0);
					s5 = (s.getDato06() != null ? s.getDato06() : 0);
					// s6 = s.getDato07();
					// s7 = s.getDato07();
					totalSecc = s0 + s1 + s2 + s3 + s4 + s5 + s6;// +s7;
					break;
				}
			}
			// System.out.println("NIVEL:"+ie.getNivMod());

			List<Matricula2019Muledad> muledad = cedula.getMatricula2019MuledadList();
			;

			if (!(muledad != null && muledad.size() > 0)) {
				if (ie.getNivMod().equals(InstitucionEducativaIE.NIVEL_INICIAL_CUNA)) {
					if ((m0 > 0) && (s0 > 0)) {
						if (!(s0 <= (m0 / 10) + 1)) {
							msgValid.append("- Verifique el número de secciones en (0 años) Tabla 204 !! \n");
						}
					} else if (m0 > 0 || s0 > 0) {
						msgValid.append("- Verifique el número de secciones en (0 años) Tabla 204 !! \n");
					}

					if (m1 > 0 && (s1 > 0)) {
						if (!(s1 <= (m1 / 10) + 1)) {
							msgValid.append("- Verifique el número de secciones en (1 año) Tabla 204 !! \n");
						}
					} else if (m1 > 0 || s1 > 0) {
						msgValid.append("- Verifique el número de secciones en (1 año) Tabla 204 !! \n");
					}

					if ((m2 + m3 + m4 + m5 + m6 + m7 > 0) && s2 > 0) {
						if (!(s2 <= ((m2 + m3 + m4 + m5 + m6 + m7) / 10) + 1)) {
							msgValid.append("- Verifique el número de secciones en (2 años y más) Tabla 204 !! \n");
						}
					} else if ((m2 + m3 + m4 + m5 + m6 + m7 > 0) || s2 > 0) {
						msgValid.append("- Verifique el número de secciones en (2 años y más) Tabla 204 !! \n");
					}

					if (s3 > 0 || s4 > 0 || s5 > 0) {
						msgValid.append(
								"- El nivel Inicial - Cuna no tiene secciones en 3, 4 y 5 años, verifique la Tabla 204 !! \n");
					}

				} else if (ie.getNivMod().equals(InstitucionEducativaIE.NIVEL_INICIAL_JARDIN)) {

					if (s0 > 0 || s1 > 0 || s2 > 0) {
						msgValid.append(
								"- El nivel Inicial - Jardin no tiene secciones en 0, 1 y 2 años, verifique la Tabla 204 !! \n");
					}

					if ((m0 + m1 + m2 + m3) > 0 && s3 > 0) {
						if (!(s3 <= ((m0 + m1 + m2 + m3) / 10 + 1))) {
							msgValid.append("- Verifique el número de secciones en (3 años y menos) Tabla 204 !! \n");
						}
					} else if ((m0 + m1 + m2 + m3) > 0 || s3 > 0) {
						msgValid.append("- Verifique el número de secciones en (3 años y menos) Tabla 204 !! \n");
					}
					if (m4 > 0 && s4 > 0) {
						if (!(s4 <= m4 / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (4 años) Tabla 204 !! \n");
						}
					} else if (m4 > 0 || s4 > 0) {
						msgValid.append("- Verifique el número de secciones en (4 años) Tabla 204 !! \n");
					}

					if ((m5 + m6 + m7) > 0 && s5 > 0) {
						if (!(s5 <= (m5 + m6 + m7) / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (5 años y más) Tabla 204 !! \n");
						}
					} else if ((m5 + m6 + m7) > 0 || s5 > 0) {
						msgValid.append("- Verifique el número de secciones en (5 años y más) Tabla 204 !! \n");
					}
				} else if (ie.getNivMod().equals(InstitucionEducativaIE.NIVEL_INICIAL_CUNA_JARDIN)) {
					if (m0 > 0 && s0 > 0) {
						if (!(s0 <= m0 / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (0 años) Tabla 204 !! \n");
						}
					} else if (m0 > 0 || s0 > 0) {
						msgValid.append("- Verifique el número de secciones en (0 años) Tabla 204 !! \n");
					}
					if (m1 > 0 && s1 > 0) {
						if (!(s1 <= m1 / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (1 año) Tabla 204 !! \n");
						}
					} else if (m1 > 0 || s1 > 0) {
						msgValid.append("- Verifique el número de secciones en (1 año) Tabla 204 !! \n");
					}
					if (m2 > 0 && s2 > 0) {
						if (!(s2 <= m2 / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (2 años) Tabla 204 !! \n");
						}
					} else if (m2 > 0 || s2 > 0) {
						msgValid.append("- Verifique el número de secciones en (2 años) Tabla 204 !! \n");
					}
					if (m3 > 0 && s3 > 0) {
						if (!(s3 <= m3 / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (3 años) Tabla 204 !! \n");
						}
					} else if (m3 > 0 || s3 > 0) {
						msgValid.append("- Verifique el número de secciones en (3 años) Tabla 204 !! \n");
					}

					if (m4 > 0 && s4 > 0) {
						if (!(s4 <= m4 / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (4 años) Tabla 204 !! \n");
						}
					} else if (m4 > 0 || s4 > 0) {
						msgValid.append("- Verifique el número de secciones en (4 años) Tabla 204 !! \n");
					}
					if ((m5 + m6 + m7) > 0 && s5 > 0) {
						if (!(s5 <= (m5 + m6 + m7) / 10 + 1)) {
							msgValid.append("- Verifique el número de secciones en (5 años y más) Tabla 204 !! \n");
						}
					} else if ((m5 + m6 + m7) > 0 || s5 > 0) {
						msgValid.append("- Verifique el número de secciones en (5 años y más) Tabla 204 !! \n");
					}
				}
			} else {
				int lnSec = (int) totalAlum / 9 + 3;

				if (totalSecc > lnSec)
					msgValid.append("- Verifique el número de secciones de la tabla 204  !! \n");

				Long lnAlum = (totalSecc * 50);
				if (totalAlum > lnAlum)
					msgValid.append("- Verifique el número de alumnos por seccion de la tabla 201  !! \n");
			}

			if (!msgValid.toString().isEmpty()) {
				System.out.println("MSG : " + msgValid.toString());
				cedula.setMsg(msgValid.toString());
				cedula.setEstadoRpt(String.valueOf(-1L));
				return Response.ok(cedula).build();
			}

			/*
			 * if (totalAlum != 0L && totalSecc != 0L && totalSecc >= totalAlum) { mensaje =
			 * String.
			 * format("El total de secciones del cuadro 304 no puede mayor o igual al total de alumnos del cuadro 301!!"
			 * ); cedula.setMsg(String.format(mensaje, cedula.getCodMod(),
			 * cedula.getAnexo())); cedula.setEstadoRpt(String.valueOf(-1L)); return
			 * Response.ok(cedula).build();
			 * 
			 * }
			 */
		}

		/*
		 * try { if(cedula.getFechasigied()!=null &&
		 * !cedula.getFechasigied().equals(""))
		 * cedula.setFechaEnvio(DATE_FORMAT_SIGIED.parse(cedula.getFechasigied())); }
		 * catch (ParseException ex) {
		 * Logger.getLogger(CedulaMatriculaCabecera2018Resources.class.getName()).log(
		 * Level.SEVERE, null, ex); }
		 */
		cedula.setEstadoRpt(String.valueOf(1L));
		matricula2019Facade.create(cedula);// ...registra...

		System.out.println("#############FIN MATRICULA2019##################");
//        }

		return Response.ok(cedula).build();// .. y termina
	}

	@SuppressWarnings("unused")
	public String verificarPlazoEntrega(String tipoDocumento, String anio) {
		EolActividadesClient actClient = new EolActividadesClient();
		EolActividadesConverter actConv = actClient.getActividadesConverter(tipoDocumento, anio);
		EolActividadConverter act = null;

		if (actConv != null && actConv.getItems() != null && !actConv.getItems().isEmpty()) {
			act = actConv.getItems().get(0);
		}

		System.out.println("LIMITE GENERAL : " + DATE_FORMAT_SIGIED.format(act.getFechaLimite()));
		// System.out.println("LIMITE 01 : "+
		// DATE_FORMAT_SIGIED.format(act.getFechaLimitesigied()) );
		// System.out.println("LIMITE 02 : "+
		// DATE_FORMAT_SIGIED.format(act.getFechaLimitehuelga()) );
		Date ahora = new Date();
		Date fechLimite = new Date();
		Calendar c = Calendar.getInstance();
//        c.setTime(act.getFechaTermino());
		c.setTime(act.getFechaLimite());
		// c.setTime(ahora);
		c.add(Calendar.DATE, 1);
		// c.add(Calendar.HOUR_OF_DAY, 5); UTILIZADO SOLO PARA EL SERVIDOR EXTERNO
		fechLimite = c.getTime();
		if (act == null) {
			return "Aún no se ha establecido una fecha de entrega electrónica para este tipo de documento";
		}

		/*
		 * if (!((ahora.equals(act.getFechaInicio()) ||
		 * ahora.after(act.getFechaInicio())) && (ahora.equals(fechLimite) ||
		 * ahora.before(fechLimite)))) { return
		 * ("El plazo de entrega electrónica para este tipo documento es desde el día "
		 * + DATE_FORMAT.format(act.getFechaInicio()) + " hasta el día " +
		 * DATE_FORMAT.format(act.getFechaTermino())); }
		 */

		/*
		 * if ( ahora.before(act.getFechaInicio()) || ahora.after(act.getFechaLimite())
		 * ) { return
		 * ("El plazo de entrega electrónica para este tipo documento es desde el día "
		 * + DATE_FORMAT.format(act.getFechaInicio()) + " hasta el día " +
		 * DATE_FORMAT.format(act.getFechaTermino())); }
		 */

		if (ahora.before(act.getFechaInicio()) || ahora.after(fechLimite)) {
			return ("El plazo de entrega electrónica para este tipo documento es desde el día "
					+ DATE_FORMAT.format(act.getFechaInicio()) + " hasta el día "
					+ DATE_FORMAT.format(act.getFechaTermino()));
		}

		return null;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		logger.info("LLEGAMOS A LOOKUP " + clase);

		String nameContextEjb = "java:global/eol-ws/ConstanteFacade!pe.gob.minedu.escale.eol.ejb.ConstanteLocal";

		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			logger.info("NEMMING : " + c);
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.info("LLEGAMOS A LOOKUP ERROR" + ne);
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupMatricula(Class<T> clase) {
		logger.info("LLEGAMOS A LOOKUP " + clase);

		String nameContextEjb = "java:global/eol-ws/Matricula2019Facade!pe.gob.minedu.escale.eol.ejb.censo2019.Matricula2019Local";

		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			logger.info("NEMMING : " + c);
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.info("LLEGAMOS A LOOKUP ERROR" + ne);
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupPadron(Class<T> clase) {
		logger.info("LLEGAMOS A LOOKUP " + clase);

		String nameContextEjb = "java:global/eol-ws/PadronFacade!pe.gob.minedu.escale.padron.ejb.facade.PadronLocal";

		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			logger.info("NEMMING : " + c);
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.info("LLEGAMOS A LOOKUP ERROR" + ne);
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

}