/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.domain.DireccionRegional;

import javax.xml.bind.annotation.XmlAttribute;
import javax.ws.rs.core.UriBuilder;
import javax.persistence.EntityManager;

import pe.gob.minedu.escale.eol.converter.UriResolver;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "direccionRegional")
public class DireccionRegionalConverter {

    private DireccionRegional entity;
    private URI uri;
    private int expandLevel;

    /**
     * Creates a new instance of DireccionRegionalConverter
     */
    public DireccionRegionalConverter() {
        entity = new DireccionRegional();
    }

    /**
     * Creates a new instance of DireccionRegionalConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded@param isUriExtendable indicates whether the uri can be
     * extended
     */
    public DireccionRegionalConverter(DireccionRegional entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getId() + "/").build() : uri;
        this.expandLevel = expandLevel;
    }

    /**
     * Creates a new instance of DireccionRegionalConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded
     */
    public DireccionRegionalConverter(DireccionRegional entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    /**
     * Getter for id.
     *
     * @return value for id
     */
    @XmlElement
    public String getId() {
        return (expandLevel > 0) ? entity.getId() : null;
    }

    /**
     * Setter for id.
     *
     * @param value the value to set
     */
    public void setId(String value) {
        entity.setId(value);
    }

    /**
     * Getter for nombreDre.
     *
     * @return value for nombreDre
     */
    @XmlElement
    public String getNombreDre() {
        return (expandLevel > 0) ? entity.getNombreDre() : null;
    }

    /**
     * Setter for nombreDre.
     *
     * @param value the value to set
     */
    public void setNombreDre(String value) {
        entity.setNombreDre(value);
    }

    /**
     * Getter for pointX.
     *
     * @return value for pointX
     */
    @XmlElement
    public Double getPointX() {
        return (expandLevel > 0) ? entity.getPointX() : null;
    }

    /**
     * Setter for pointX.
     *
     * @param value the value to set
     */
    public void setPointX(Double value) {
        entity.setPointX(value);
    }

    /**
     * Getter for pointY.
     *
     * @return value for pointY
     */
    @XmlElement
    public Double getPointY() {
        return (expandLevel > 0) ? entity.getPointY() : null;
    }

    /**
     * Setter for pointY.
     *
     * @param value the value to set
     */
    public void setPointY(Double value) {
        entity.setPointY(value);
    }

    /**
     * Getter for zoom.
     *
     * @return value for zoom
     */
    @XmlElement
    public Integer getZoom() {
        return (expandLevel > 0) ? entity.getZoom() : null;
    }

    /**
     * Setter for zoom.
     *
     * @param value the value to set
     */
    public void setZoom(Integer value) {
        entity.setZoom(value);
    }

    /**
     * Returns the URI associated with this converter.
     *
     * @return the uri
     */
    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the URI for this reference converter.
     *
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the DireccionRegional entity.
     *
     * @return an entity
     */
    @XmlTransient
    public DireccionRegional getEntity() {
        if (entity.getId() == null) {
            DireccionRegionalConverter converter = UriResolver.getInstance().resolve(DireccionRegionalConverter.class, uri);
            if (converter != null) {
                entity = converter.getEntity();
            }
        }
        return entity;
    }

    /**
     * Returns the resolved DireccionRegional entity.
     *
     * @return an resolved entity
     */
    public DireccionRegional resolveEntity(EntityManager em) {
        return entity;
    }
}
