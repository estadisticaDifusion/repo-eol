/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.eol.domain.EolCedula;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "cedulas")
public class CedulasConverter {

    private URI uri;
    private int expandLevel;
    private Collection<EolCedula> entities;
    private Collection<CedulaConverter> items = new ArrayList<CedulaConverter>();

    public CedulasConverter() {
    }

    public CedulasConverter(Collection<EolCedula> entities, URI uri, int expandLevel) {
        this.entities = entities;
        this.expandLevel = expandLevel;
        this.uri = uri;
    }

    @XmlTransient
    public Collection<EolCedula> getEntities() {
        return entities;
    }

    public void setEntities(Collection<EolCedula> entities) {
        this.entities = entities;
    }

    @XmlElement(name = "items")
    public Collection<CedulaConverter> getItems() {
        if (entities != null && !entities.isEmpty()) {
            for (EolCedula ced : entities) {
                items.add(new CedulaConverter(ced, getUri(), expandLevel));
            }
        }
        return items;
    }

    public void setItems(Collection<CedulaConverter> items) {
        this.items = items;
    }

    @XmlAttribute(name = "url")
    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

}
