package pe.gob.minedu.escale.eol.constants;

public final class EolWsConstant {
	
	public static final String Resultado2019DetalleCuadroC101 = "C101";
	public static final String Resultado2019DetalleCuadroP101 = "P101";
	
    protected EolWsConstant() {
        throw new UnsupportedOperationException();
    }
}
