package pe.gob.minedu.escale.eol.rest.censo2019;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import pe.gob.minedu.escale.eol.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.eol.domain.PadronClient;
import pe.gob.minedu.escale.eol.domain.censo2019.Resultado2019Cabecera;
import pe.gob.minedu.escale.eol.domain.domainPadron.CentroEducativo;
import pe.gob.minedu.escale.eol.ejb.censo2019.Resultado2019Local;
import pe.gob.minedu.escale.padron.ejb.facade.PadronLocal;
import pe.gob.minedu.escale.eol.converter.RespuestaConverter;

import java.util.logging.Logger;

/**
 * 
 * @author WARODRIGUEZ
 *
 */

@Path("/censo2019/resultadorecuperacion")
@Stateless
public class CedulaResultadoRecuperacion2019Service {
	private Logger logger = Logger.getLogger(CedulaResultadoRecuperacion2019Service.class.getName());

	@EJB
	private Resultado2019Local facade = lookupResultado(Resultado2019Local.class);

	@EJB
	private CedulaResultado2019ValidResources facadeValid;

	@EJB
	private PadronLocal padronFacade = lookupPadron(PadronLocal.class);

	@POST
	@Path("/actualizar")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	public Response actualizar(Resultado2019Cabecera cedula) {

		RespuestaConverter rpta = new RespuestaConverter();
		String msg = "";

		if (cedula.getToken() == 0 || cedula.getToken() == -1L) {
			return Response.status(Response.Status.UNAUTHORIZED).build(); // 401
																			// --
																			// No
																			// tiene
																			// autorización
		}
//		PadronClient padronClient = new PadronClient(cedula.getCodMod(), cedula.getAnexo());
//		logger.info("SERVICIO RECUPERACION [] :: " + padronClient); 
		CentroEducativo ce = null;
		ce =padronFacade.obtainByCodMod(cedula.getCodMod(), cedula.getAnexo());
//		InstitucionEducativaIE ie =new InstitucionEducativaIE();
		try {
//			logger.info("00000000000000000000000000");
//			ie = padronClient.get_JSON(InstitucionEducativaIE.class);
//			logger.info("SERVICIO RECUPERACION [] :: 000000" + ie);
//			if (ie == null) {
			if (ce == null) {
					msg = String.format(
						"No se encontró ninguna Institución Educativa con código modular %1s y anexo %2s  .",
						cedula.getCodMod(), cedula.getAnexo());
				rpta.setMensaje(msg);
				rpta.setStatus("ERROR");
				return Response.ok(rpta).build();
			}

//			if (!ie.getMcenso().equals("1")) {
			if (!ce.getMcenso().equals("1")) {
				msg = "La Institución Educativa no se encuentra incluida en el marco censal, para mayor información consulte con el estadístico de su UGEL.";
				rpta.setMensaje(msg);
				rpta.setStatus("ERROR");
				return Response.ok(rpta).build();
			}

		} catch (Exception e) {
			logger.info("ERROR : "  + e.getMessage()  );
			
			msg = "Esta cédula presenta un error interno en el servicio de busqueda del padron de IIEE."
					+ "\nPor favor vuelva a intentar más tarde." + "\nDisculpe las molestias ocasionadas. ";
			rpta.setMensaje(msg);
			rpta.setStatus("ERROR");
			return Response.ok(rpta).build();
		}

		cedula.setValido(false);
//		rpta = facadeValid.validarRecupera(cedula, ie);
		rpta = facadeValid.validar(cedula, ce);
//		cedula.setCodooii(ie.getIgel().getIdIgel());
		cedula.setCodooii(ce.getCodigel());
//		cedula.setCodgeo(ie.getDistrito().getIdDistrito());
		cedula.setCodgeo(ce.getCodgeo());
		if (rpta.getMensaje().isEmpty()) {
			logger.info("SERVICIO RECUPERACION [] :: 111111111");
			Resultado2019Cabecera resltdiciembre = facade.findByCodModAnNiv(cedula.getCodMod(), cedula.getAnexo(),
					cedula.getNivMod());

			if (resltdiciembre != null) {
				facade.updatedetail(resltdiciembre, cedula);
				rpta.setMensaje(
						"Su mensaje se envio correctamente, por favor verifique sus datos imprimiento su constancia desde la pagina http://escale.minedu.gob.pe");
				rpta.setStatus("OK");
			} else {
				rpta.setStatus("ERROR");
			}
			// resltdiciembre.setDetalle(cedula.getDetalle());

		} else {
			logger.info("SERVICIO RECUPERACION [] :: 22222222");
			rpta.setStatus("ERROR");
		}

		// ...registra...
		return Response.ok(rpta).build();// .. y termina
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookupResultado(Class<T> clase) {

		String nameContextEjb = "java:global/eol-ws/Resultado2019Facade!pe.gob.minedu.escale.eol.ejb.censo2019.Resultado2019Local";

		try {

			javax.naming.Context c = new InitialContext();

			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {

			throw new RuntimeException(ne);
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	private <T> T lookupPadron(Class<T> clase) {

		String nameContextEjb = "java:global/eol-ws/PadronFacade!pe.gob.minedu.escale.padron.ejb.facade.PadronLocal";

		try {

			javax.naming.Context c = new InitialContext();

			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {

			throw new RuntimeException(ne);
		}
	}

}
