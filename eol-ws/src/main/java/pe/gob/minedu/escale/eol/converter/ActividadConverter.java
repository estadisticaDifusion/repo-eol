/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.converter;

import java.net.URI;
import java.util.Date;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.domain.EolActividad;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "actividad")
public class ActividadConverter {

	private static String ESTADO_ACTIVO = "1";
	private static String ESTADO_INACTIVO = "0";
	private EolActividad entity;
	private URI uri;
	private int expandLevel;

	public ActividadConverter() {
	}

	public ActividadConverter(EolActividad entity, URI uri, int expandLevel, boolean isUriExtendable) {
		this.entity = entity;
		this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getId() + "/").build() : uri;
		this.expandLevel = expandLevel;
		// getPeriodo();
	}

	public ActividadConverter(EolActividad entity, URI uri, int expandLevel) {
		this(entity, uri, expandLevel, false);

	}

	@XmlAttribute
	public URI getUri() {
		return uri;
	}

	@XmlElement
	public Integer getId() {
		return entity.getId();
	}

	@XmlElement
	public String getNombre() {
		return entity.getNombre();
	}

	@XmlElement
	public String getDescripcion() {
		return entity.getDescripcion();
	}

	@XmlElement
	public Date getFechaInicio() {
		return entity.getFechaInicio();
	}

	@XmlElement
	public Date getFechaTermino() {
		return entity.getFechaTermino();
	}

	@XmlElement
	public Date getFechaTerminosigied() {
		return entity.getFechaTerminosigied();
	}

	@XmlElement
	public Date getFechaTerminohuelga() {
		return entity.getFechaTerminohuelga();
	}

	@XmlElement
	public Date getFechaLimite() {
		return entity.getFechaLimite();
	}

	@XmlElement
	public Date getFechaLimitesigied() {
		return entity.getFechaLimitesigied();
	}

	@XmlElement
	public Date getFechaLimitehuelga() {
		return entity.getFechaLimitehuelga();
	}

	@XmlElement
	public String getUrlFormato() {
		return entity.getUrlFormato();
	}

	@XmlElement
	public String getUrlConstancia() {
		return entity.getUrlConstancia();
	}

	@XmlElement
	public String getUrlCobertura() {
		return entity.getUrlCobertura();
	}

	@XmlElement
	public String getUrlSituacion() {
		return entity.getUrlSituacion();
	}

	@XmlElement
	public String getUrlOmisos() {
		return entity.getUrlOmisos();
	}

	@XmlElement
	public boolean getEstado() {
		return entity.getEstado().equals(ESTADO_ACTIVO);
	}

	@XmlElement
	public boolean getEstadoFormato() {
		return entity.getEstadoFormato();
	}

	@XmlElement
	public boolean getEstadoConstancia() {
		return entity.getEstadoConstancia();
	}

	@XmlElement
	public boolean getEstadoCobertura() {
		return entity.getEstadoCobertura();
	}

	@XmlElement
	public boolean getEstadoSituacion() {
		return entity.getEstadoSituacion();
	}

	@XmlElement
	public boolean getEstadoOmisos() {
		return entity.getEstadoOmisos();
	}

	@XmlElement
	public boolean getEstadoSie() {
		return entity.getEstadoSie();
	}

	@XmlElement(name = "tipo")
	public String getTipo() {
		return entity.getTipo();
	}

	@XmlElement(name = "periodo")
	public PeriodoConverter getPeriodo() {
		if (entity.getEolPeriodo() != null) {
			return new PeriodoConverter(entity.getEolPeriodo(), uri.resolve("/periodo"), expandLevel - 1);
		} else {
			return null;
		}
	}

	@XmlElement(name = "nombrePeriodo")
	public String getNombrePeriodo() {
		return entity.getEolPeriodo().getAnio();
	}

	public void setPeriodo(PeriodoConverter periodo) {
		entity.setEolPeriodo(periodo.getEntity());
	}
}
