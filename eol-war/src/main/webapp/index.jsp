<%@page contentType="text/html; charset=UTF-8"%>
<%
    /*Cookie cookie = new Cookie("JSESSIONID", null);
    cookie.setPath("/estadistica");
    cookie.setHttpOnly(true);
    cookie.setMaxAge(0);
    response.addCookie(cookie);*/

/*      String sessionid = request.getSession().getId();
     response.addHeader( "Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly"); */
    
    //System.out.println("JSP Session: "+request.getSession(false));
//     System.out.println("JSP LOGGED_USER: "+request.getSession().getAttribute("LOGGED_USER"));
    /*
    if(request.getSession().getAttribute("LOGGED_USER") == null){
    	System.out.println("REDIRECCIONANDO AL LOGIN");
    	request.getRequestDispatcher("/index.jsp");
    }else{
    	System.out.println("CONTINUA LA NAVEGACION");
    }*/ 

%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<logic:notPresent name="LOGGED_USER">
    <logic:redirect action="/inicio" />
</logic:notPresent>
<logic:present name="LOGGED_USER">
    <logic:equal value="UGEL" name="LOGGED_USER" property="tipo">
    <%System.out.println("JSP Invoca redirect: ");%>
        <logic:redirect page="/ugel" />
    </logic:equal>
    <logic:equal value="CE" name="LOGGED_USER" property="tipo">
        <logic:redirect page="/ce" />
    </logic:equal>
    <logic:equal value="ADMIN" name="LOGGED_USER" property="tipo">
        <logic:redirect page="/admin" />
    </logic:equal>
</logic:present>
