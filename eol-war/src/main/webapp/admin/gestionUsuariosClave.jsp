<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<fmt:setBundle basename="ApplicationResources" var="aplmsg" />
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link type="text/css" href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/popup.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/padron.css'/>" rel="stylesheet" />


<%-- SECCION REUBICADA AL DEFAULT

<script type="text/javascript" src="<c:url value='/recursos/js/popup_lib.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/js/datepicker-es.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/js/jquery.validate.js'/>"></script>
<script type="text/javascript">var ugel='${LOGGED_USER.usuario}'</script>
<c:if test="${fn:contains(header['user-agent'],'MSIE')}">
    <script type="text/javascript" src="<c:url value='/recursos/js/json2.js'/>"></script>
</c:if>
<script type="text/javascript" src="<c:url value='/recursos/js/form_estadistico.js'/>"></script>

<script type="text/javascript" >
    // Funciones para uso de REST
            var rootURL = '/eol-ws/rest/loginAuth';
            var actividad;



            function updateUsuario() {

                    $.ajax({
                            type: 'PUT',
                            url: rootURL + '/' + $('#usuario').val(),
                            contentType: "application/json",
                            dataType: "json",
                            crossDomain: false,
                            async: false,
                            cache: false,
                            data: formToJSON(),
                            
                            success: function(data, textStatus, jqXHR){
                                    console.log("response datossss:" + data);
                                    if(data=="1"){
                                        alert('El usuario fue actualizado.' );
                                    }else{
                                        alert('El usuario no existe');
                                    }
                                    
                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                    alert('Error: Comunicarse con el administrador del sistema.');
                            }
                    });
            }


           
            function formToJSON() {
                    var usuario = $('#usuario').val();
                    var clave = $('#clave').val();
                    return JSON.stringify({
                    "usuario": usuario == "" ? alert('Ingresar el usuario.').stop() : usuario,
                    "contrasenia": clave =="" ? alert('Ingresar la contraseña.').stop() : clave
                    });
            }

            function validar() {
                if(validarSuma()) {
                        updateUsuario();
                }else{
                    alert('Ingresar el valor de la suma.').stop();
                }
            }          
</script>
<script type="text/javascript">
    var a = Math.ceil(Math.random() * 10);
    var b = Math.ceil(Math.random() * 10);
    var c = a + b
    function pintaSuma()
    {
        document.write( a + " + " + b +" ? ");
        document.write("<input id='idNumeros' type='text' maxlength='2' size='2'/>");
    }
    function validarSuma(){
        var d = document.getElementById('idNumeros').value;
        if (d == c) return true;        
        return false;

    }
    </script>
    --%>
    
<%--<link type="text/css" href="<c:url value='/recursos/jquery/css/jquery-ui-1.8.8.custom.css'/>" rel="stylesheet" />--%>




    <div class="titulo">
        <div style="font:10pt Arial,Helvetica,Verdana,sans-serif;margin: 0 0 5px 17px;border-bottom:0px dashed #055FCD;font-size:17px;text-transform: uppercase; color:  #055FCD ;font-weight: bold;">
            Bienvenido administrador: ${LOGGED_USER.usuario}
        </div>
        <span style="margin: 0 0 0 17px;color: #333333;font-size: 12px; font-family: Arial,Helvetica,Verdana,sans-serif; font-weight: bold">Gestion de claves de usuarios&nbsp;</span>
    </div>

<!--<form name="usuariosForm" id="usuariosForm">
    <div class="mainArea">
        <label>Usuario</label>
        <input id="usuario" name="usuario" type="text" />

        <label>Clave</label>
        <input type="password" id="clave" name="clave" >

        <label>Cuanto es: </label>
        <script type="text/javascript">pintaSuma()</script>
    </div>
    <br>
    <div id="botones" class="mainArea">
        <button id="btnSave" name="btnSave" onclick="validar()">Grabar</button>                 
    </div>
</form>-->
<form class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-2" for="usuario">Usuario:</label>
        <div class="col-sm-10">
          <input class="form-control" id="usuario" name="usuario" type="text" >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="clave">Password</label>
        <div class="col-sm-10">
          <input class="form-control" id="clave" name="clave" type="password" >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="clave">Cuanto es:</label>
        <div class="col-sm-10">
          <script type="text/javascript">pintaSuma()</script>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button id="btnSave" class="btn btn-default" onclick="validar()">Guardar</button>
        </div>
    </div>
</form>


