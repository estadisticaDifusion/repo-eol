<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<fmt:setBundle basename="ApplicationResources" var="aplmsg" />
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link type="text/css" href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/popup.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/padron.css'/>" rel="stylesheet" />

<%-- SECCION REUBICADA AL DEFAULT

<script type="text/javascript" >
    var urlR='<c:url value="/"/>';
    var urlMsj='<c:url value="/MensajeServlet"/>';
    var urlEol='<c:url value="/EolCensoServlet"/>';
    var urlCEol='<c:url value="/ClaveEolServlet"/>';
</script>

<script type="text/javascript" src="<c:url value='/recursos/js/popup_lib.js'/>"></script>
<!--<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-1.4.4.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui-1.8.8.custom.min.js'/>"></script>-->
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/js/datepicker-es.js'/>"></script>
<script type="text/javascript">var ugel='${LOGGED_USER.usuario}'</script>
<c:if test="${fn:contains(header['user-agent'],'MSIE')}">
    <script type="text/javascript" src="<c:url value='/recursos/js/json2.js'/>"></script>
</c:if>
<script type="text/javascript" src="<c:url value='/recursos/js/form_estadistico.js'/>"></script>

--%>


<%--<link type="text/css" href="<c:url value='/recursos/jquery/css/jquery-ui-1.8.8.custom.css'/>" rel="stylesheet" />--%>
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.structure.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.theme.min.css'/>" rel="stylesheet" />

    <div class="titulo">
        <div style="font:10pt Arial,Helvetica,Verdana,sans-serif;margin: 0 0 5px 17px;border-bottom:0px dashed #055FCD;font-size:17px;text-transform: uppercase; color:  #055FCD ;font-weight: bold;">
            Bienvenido administrador: ${LOGGED_USER.usuario}
        </div>
    </div>

                      