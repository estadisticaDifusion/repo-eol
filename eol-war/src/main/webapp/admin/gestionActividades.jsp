<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<fmt:setBundle basename="ApplicationResources" var="aplmsg" />
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link type="text/css" href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/popup.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/padron.css'/>" rel="stylesheet" />

<%-- SECCION REUBICADA AL DEFAULT
<script type="text/javascript" >
    var urlR='<c:url value="/"/>';
    var urlMsj='<c:url value="/MensajeServlet"/>';
    var urlEol='<c:url value="/EolCensoServlet"/>';
    var urlCEol='<c:url value="/ClaveEolServlet"/>';
</script>

<script type="text/javascript" src="<c:url value='/recursos/js/popup_lib.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/js/datepicker-es.js'/>"></script>
<script type="text/javascript">var ugel='${LOGGED_USER.usuario}'; </script>
<c:if test="${fn:contains(header['user-agent'],'MSIE')}">
    <script type="text/javascript" src="<c:url value='/recursos/js/json2.js'/>"></script>
</c:if>
<script type="text/javascript" src="<c:url value='/recursos/js/form_gestion.js'/>"></script>
--%>


<%--<link type="text/css" href="<c:url value='/recursos/jquery/css/jquery-ui-1.8.8.custom.css'/>" rel="stylesheet" />--%>
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.structure.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.theme.min.css'/>" rel="stylesheet" />

    <div class="titulo">
        <div style="font:10pt Arial,Helvetica,Verdana,sans-serif;margin: 0 0 5px 17px;border-bottom:0px dashed #055FCD;font-size:17px;text-transform: uppercase; color:  #055FCD ;font-weight: bold;">
            Bienvenido administrador: ${LOGGED_USER.usuario}
        </div> 
        <span style="margin: 0 0 0 17px;color: #333333;font-size: 12px; font-family: Arial,Helvetica,Verdana,sans-serif; font-weight: bold">Gestion de actividades&nbsp;</span>
        <br/>
        
    </div>

    <div class="leftArea" style="text-align: center;">
        <ul id="actividadLista"></ul>
    </div>

<form class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-2" for="actId">Id:</label>
        <div class="col-sm-10">
          <input class="form-control" id="actId" name="actId" type="text" disabled >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="nombre">Nombre:</label>
        <div class="col-sm-10">
          <input class="form-control" id="nombre" name="nombre" type="text" disabled >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="descripcion">Descripcion:</label>
        <div class="col-sm-10">
          <input class="form-control" id="descripcion" name="descripcion" type="text" disabled >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="fechaInicio">Fecha Inicio</label>
        <div class="col-sm-10">
          <input class="form-control" id="fechaInicio" name="fechaInicio" type="text" placeholder="Fecha inicio actividad" >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="fechaTermino">Fecha Termino</label>
        <div class="col-sm-10">
          <input class="form-control" id="fechaTermino" name="fechaTermino" type="text" placeholder="Fecha termino actividad" >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="fechaLimite">Fecha limite</label>
        <div class="col-sm-10">
          <input class="form-control" id="fechaLimite" name="fechaLimite" type="text" placeholder="Fecha limite actividad" >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="fechaTerminoSigied">Fecha Termino Sigied</label>
        <div class="col-sm-10">
          <input class="form-control" id="fechaTerminoSigied" name="fechaTerminoSigied" type="text" placeholder="Fecha termino actividad sigied" >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="fechaLimiteSigied">Fecha Limite Sigied</label>
        <div class="col-sm-10">
          <input class="form-control" id="fechaLimiteSigied" name="fechaLimiteSigied" type="text" placeholder="Fecha limite actividad sigied" >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="fechaTerminoHuelga">Fecha Termino Huelga</label>
        <div class="col-sm-10">
          <input class="form-control" id="fechaTerminoHuelga" name="fechaTerminoHuelga" type="text" placeholder="Fecha termino actividad huelga" >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="fechaLimiteHuelga">Fecha Limite Huelga</label>
        <div class="col-sm-10">
          <input class="form-control" id="fechaLimiteHuelga" name="fechaLimiteHuelga" type="text" placeholder="Fecha limite actividad huelga" >
        </div>
     </div>
    
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">          
          <button id="btnSave" class="btn btn-default" >Guardar</button>
        </div>
     </div>    
</form>
                               
