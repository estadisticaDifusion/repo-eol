<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<fmt:setBundle basename="ApplicationResources" var="aplmsg" />
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link type="text/css" href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/popup.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/padron.css'/>" rel="stylesheet" />

<%--<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-1.4.4.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui-1.8.8.custom.min.js'/>"></script>--%>
<%--<script type="text/javascript" src="<c:url value='/recursos/js/datepicker-es.js'/>"></script>--%>
<%--<script type="text/javascript" src="<c:url value='/recursos/js/form_estadistico.js'/>"></script>--%>
<%--<link type="text/css" href="<c:url value='/recursos/jquery/css/jquery-ui-1.8.8.custom.css'/>" rel="stylesheet" />--%>

    <div class="titulo">
        <div style="font:10pt Arial,Helvetica,Verdana,sans-serif;margin: 0 0 5px 17px;border-bottom:0px dashed #055FCD;font-size:17px;text-transform: uppercase; color:  #055FCD ;font-weight: bold;">
            Bienvenido administrador: ${LOGGED_USER.usuario}
        </div> 
        <span style="margin: 0 0 0 17px;color: #333333;font-size: 12px; font-family: Arial,Helvetica,Verdana,sans-serif; font-weight: bold">Gestión de archivos&nbsp;</span>
        <br/>
        
    </div>

    <div class="leftArea" style="text-align: center;">
       	<ul id="listaFile">
            <li><a href="#" id="cedula11" >CEDULA 11</a></li>
            <li><a href="#" id="matricula" >MATRICULA</a></li>
            <li><a href="#" id="resultado" >RESULTADO</a></li>
        </ul>
    </div>

<!--    <form id="formulario" action="SubidaArchivosCensos" method="POST" enctype="multipart/form-data">
        <div class="mainArea">
            <label>Carpeta:</label>
            <input type="text" id="idCarpeta" name="idCarpeta"  disabled />
            <input type="hidden" id="carpeta" name="carpeta"  />
            <label>Nombre:</label>
            <input type="text" id="nombreArchivo" name="nombreArchivo" >

            <label>Archivo: </label>
            <input type="file" id="file" name="file" >

        </div>
        <br>
        <div id="botones" class="mainArea">
            <input type="submit" value="Subir archivo" />
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="well well-sm">
            <ul class="list-group">
              <li class="list-group-item list-group-item-warning">Nombre de los formatos electrónicos del Censo Escolar:</li>
              <li class="list-group-item list-group-item-warning"> 1.Cédula de Matrícula: <br>CensoMatriculaDocentesRecursosCed + (Nro. Cédula Mayús.) + _2018.xls</li>
              <li class="list-group-item list-group-item-warning">2. Cédula de Local: <br>CensoEscolarCed11_2018.xls</li>
              <li class="list-group-item list-group-item-warning">3. Cédula ID: <br>IdentificacionIECedID_2018.xls</li>
            </ul>                          
        </div>

    </form>-->

<form class="form-horizontal" id="formulario" action="SubidaArchivosCensos" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label col-sm-2" for="idCarpeta">Carpeta:</label>
        <div class="col-sm-10">
          <input class="form-control" type="text" id="idCarpeta" name="idCarpeta"  disabled  >
          <input type="hidden" id="carpeta" name="carpeta"  />
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="nombreArchivo">Nombre archivo:</label>
        <div class="col-sm-10">
          <input class="form-control"  type="text" id="nombreArchivo" name="nombreArchivo" >
        </div>
     </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="file">Archivo:</label>
        <div class="col-sm-10">
          <input class="form-control" type="file" id="file" name="file" >
        </div>
     </div>
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Subir archivo</button>
        </div>
     </div>
</form>
<br>
<br>
    <c:if test="${requestScope.state eq 1}">
        <div class="alert alert-success">
          <strong>Exito!</strong> ${requestScope.message}
        </div>
    </c:if>
    <c:if test="${requestScope.state eq 0}">
        <div class="alert alert-danger">
          <strong>Error!</strong> ${requestScope.message}
        </div>
    </c:if>
<br>
<br>
<br>
<div class="well well-sm">
    <ul class="list-group">
      <li class="list-group-item list-group-item-warning">Nombre de los formatos electrónicos del Censo Escolar:</li>
      <li class="list-group-item list-group-item-warning"> 1.Cédula de Matrícula: <br>CensoMatriculaDocentesRecursosCed + (Nro. Cédula Mayús.) + _2019.xls</li>
      <li class="list-group-item list-group-item-warning">2. Cédula de Local: <br>CensoEscolarCed11_2019.xls</li>
<!--       <li class="list-group-item list-group-item-warning">3. Cédula ID: <br>IdentificacionIECedID_2019.xls</li> -->
    </ul>
</div>

                               
