<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-1.4.4.min.js'/>"></script>--%>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-3.4.1.min.js'/>"></script>

<c:if test="${param.css ne false}">
    <%--<script src="<c:url value='/recursos/jquery/jquery-ui-1.8.8.custom.min.js'/>" type="text/javascript"></script>
    <link type="text/css" href="<c:url value='/recursos/jquery/css/jquery-ui-1.8.8.custom.css' />" rel="stylesheet" />--%>
    <script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"></script>
    
    <link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.css'/>" rel="stylesheet" />
    <link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.structure.min.css'/>" rel="stylesheet" />
    <link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.theme.min.css'/>" rel="stylesheet" />

</c:if>
