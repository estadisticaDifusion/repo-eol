'use strict';
angular.module('eolApp')
    .service('messagesService', function ($modal) {
        var messages = null;
        this.loadMessages = function(scp){
            messages = {
                    title: '',
                    listMsgs: [],
                    prompt: '',
                    confirmMsg: {
                        modal: $modal({
                            scope: scp,
                            templateUrl: '/recursos/views/templates/rp-modal-confirm-tpl.html',
                            show: false
                        }),
                        okFunc: null
                    },
                    showAlert: function(msgs, tit, func){
                        var ms = msgs || [];
                        var selfie = this;
                        if(ms.length > 0){
                            selfie.title = tit || '';
                            selfie.listMsgs = ms;
                            var fn = func || function(){
                                    console.log('No hide event');
                            };
                            var modal = $modal({
                                        scope: scp,
                                        templateUrl: '/recursos/views/templates/rp-modal-alert-tpl.html',
                                        show: false,
                                        onHide: fn
                                    });
                            modal.$promise.then(modal.show);
                        }
                    },
                    showOkMessage: function(msgs, tit, func){
                        var ms = msgs || [];
                        var selfie = this;
                        if(ms.length > 0){
                            selfie.title = tit || '';
                            selfie.listMsgs = ms;
                            var fn = func || function(){
                                    console.log('No hide event');
                            };
                            var modal = $modal({
                                        scope: scp,
                                        templateU: 'views/templates/rp-modal-accept-tpl.html',
                                        show: false,
                                        onHide: fn
                                    });
                            modal.$promise.then(modal.show);
                        }
                    },
                    showConfirmMessage: function(prompt, tit, func){
                        var pt = prompt || '';
                        var fn = func || function(){
                            console.log('Funcion no encontrada.');
                        };
                        var selfie = this;
                        if(pt && func){
                            selfie.confirmMsg.okFunc = function(){
                                fn();
                                selfie.confirmMsg.modal.hide();
                            };
                            selfie.title = tit || '';
                            selfie.prompt = pt;
                            selfie.confirmMsg.modal.$promise.then(selfie.confirmMsg.modal.show);
                        }
                    }
                };
                if(!scp.formModel){
                    scp.formModel = {};
                }
            scp.formModel.messages = messages;
        };
    });

