'use strict';
angular.module('eolApp')
  .service('padronIeService', function ($http, $q, URL) {
        this.fromModularCode = function(codMod, codIi){
            var deferredObj = $q.defer();
            var baseUrl = URL.BASE_PADRON+URL.SERV_PADRON;
            var queryString = '&estados=1&mcenso=1&codmod=' + codMod;
            var sUrl = baseUrl + '?' + queryString;
            $http.get(sUrl,{
                    headers: {
                            'Accept':'application/json'
                    }
            }).then(function(res){
                if(res.data.items.ugel.idUgel === codIi){
                    deferredObj.resolve(res);
                }else{
                    deferredObj.reject(res);
                }
            }).catch(function(err){
                    deferredObj.reject(err);
            });
            return deferredObj.promise;
        };
    });
