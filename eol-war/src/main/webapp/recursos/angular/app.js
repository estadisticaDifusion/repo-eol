angular.module("eolApp",['mgcrea.ngStrap'])
    .constant('REGEXP',{
        'TEXTO': /^[a-zA-Z0-9\sáéíóúÁÉÍÓÚüÜñÑ%\-@\/&'\.]*$/,
        'NUMEROS': /^[0-9]*$/,
        'NUMERO_GUION': /^[0-9\-]*$/,
        'EMAIL': /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
        'TEXT_EXT': /^[a-zA-Z0-9\sáéíóúÁÉÍÓÚüÜñÑ%\-@\/_&'\.]*$/
    })
    .constant('URL',{
        'HOST_APP':'',
        'BASE_PADRON':'http://escale.minedu.gob.pe/padron-ws',
        //'BASE_PADRON_LOC':'http://10.36.131.178:8080/padron-ws',
        'BASE_PADRON_LOC':'/padron-ws',
        'SERV_PADRON': '/rest/instituciones',
        'SERV_PADRON_ACT_ENV': '/actualizarPadronEnvio'
    });
    

