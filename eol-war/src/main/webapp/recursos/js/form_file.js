
            var actividad = [
                    { id: 'c11',nombre: 'cedula11'},
                    { id: 'mat',nombre: 'matricula'},
                    { id: 'res',nombre: 'resultado'}
                ];

           // window.onload = findAll();
            //$.ajaxSetup({ cache:false });
            $(document).ready(function(){

                /*
                $.datepicker.setDefaults($.datepicker.regional['es']);
                $("#fechaInicio").datepicker({ dateFormat: 'dd/mm/yy' });
                $("#fechaTermino").datepicker({ dateFormat: 'dd/mm/yy' });
                $("#fechaLimite").datepicker({ dateFormat: 'dd/mm/yy' });
                */
            	
            	jQuery('#listaFile').on('click','a', function() {
                	//findByIdCarpeta(jQuery(this));
            		findByIdCarpeta(this);
    	        });
            	
            });

            $(document).on("submit", "#formulario", function() {
                var $form = $(this);
                $.post($form.attr("action"), $form.serialize(), function(response) {
                    //alert('hola mundo');
                });
               
            });
            
            
            
            /*$('#"listaFile" a').live('click', function() {
                    findById($(this).data('identity'));
            });*/
            
            function findAll() {
                
                actividad = [
                    { id: 'cedula11',nombre: 'CEDULA 11'},
                    { id: 'matricula',nombre: 'MATRICULA'},
                    { id: 'resultado',nombre: 'RESULTADO'}
                ];
                renderizarLista(actividad);
                
                /*
                    $.ajax({
                            type: 'GET',
                            url: rootURL + "/buscarActividad",
                            dataType: "json",
                            success: renderizarLista
                    });
                */
            }

            
            
            function findByIdCarpeta(e) {
                id = e.id;
                renderizarFileDetalles(e);
                    /*
                    $.ajax({
                            type: 'GET',
                            url: rootURL + '/buscarActividad/' + id,
                            dataType: "json",
                            crossDomain: false,
                            async: false,
                            cache: false,
                            success: function(data){
                                    actividad = data;
                                    renderizarFileDetalles(actividad);
                            }
                    });
                    */
            }

            function renderizarLista(data) {
                    var list = data == null ? [] : (data instanceof Array ? data : [data]);

                    $('#actividadLista li').remove();
                    $.each(list, function(key, actividad) {
                        $('#actividadLista').append('<li><a href="#" data-identity="' + actividad.id + '">'+actividad.nombre+'</a></li>');                            
                    });
                    /*
                    $.each(list, function(key, actividad) {
                        $.each(actividad, function (key1, actividad) {
                            $.each(actividad, function (index, actividad) {
                                $('#actividadLista').append('<li><a href="#" data-identity="' + actividad.id + '">'+actividad.nombre+'</a></li>');
                            })
                        })
                    });
                    */
            }

            function renderizarFileDetalles(actividad) {
                    $('#idCarpeta').val(actividad.id);
                    $('#carpeta').val(actividad.id);
                    /*
                    $('#descripcion').val(actividad.descripcion);
                    $('#fechaInicio').val(fechaFormatoJavaJson(actividad.fechaInicio));
                    $('#fechaTermino').val(fechaFormatoJavaJson(actividad.fechaTermino));
                    $('#fechaLimite').val(fechaFormatoJavaJson(actividad.fechaLimite));
                    */


            }

            function fechaFormatoJavaJson(fechaPrimitiva){
                var d = fechaPrimitiva.slice(0, 10).split('-');
                return d[2] +'/'+ d[1] +'/'+ d[0];
            }
            function fechaFormatoJsonJava(fechaEvolutiva){
                var d = fechaEvolutiva.slice(0, 10).split('/');
                var d1 = new Date(d[2], d[1]-1, d[0]);
                return d1;
            }

            function formFILEToJSON() {
                    var actId = $('#actId').val();
                    return JSON.stringify({
                            "id": actId == "" ? null : actId,
                            "nombre": $('#nombre').val(),
                            "descripcion": $('#descripcion').val(),
                            "fechaInicio": fechaFormatoJsonJava($('#fechaInicio').val()),
                            "fechaTermino": fechaFormatoJsonJava($('#fechaTermino').val()),
                            "fechaLimite": fechaFormatoJsonJava($('#fechaLimite').val())
                            });
            }
