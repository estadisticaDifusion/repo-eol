jQuery(document).ready(init)
//var urlMsj="MensajeServlet";
var inicio=0;
var cantTotal=0;

var inicioEnv=0;
var cantTotalEnv=0;

var ugel="";
//codMod="";
function init(){


//ugel=jQuery.cookie("COD_UGEL");
//codMod=jQuery("#usuario").val();//jQuery.cookie("COD_MOD");
//  ugel=leerCookie("COD_UGEL");
//  codMod=leerCookie("COD_MOD");

//  ugel= jQuery.trim(jQuery("#COD_UGEL").text());
//  codMod= jQuery.trim(jQuery("#COD_MOD").text());
  jQuery("#EnviarMSJ").click(enviarMSJ)
  buscarMsj(codMod)
  buscarMsjEnv(codMod)

  jQuery("#msjDlg").dialog({
        autoOpen:false,
        width:"620px",        
        modal:true,
        title:"Mensaje",
        buttons:{
            "Cerrar":function(){
                cerrarMsjDlg()
            }
        }
    })
jQuery("#msjRefresh").click(function(){
    inicio=0;
    cantTotal=0;
    resetFormEnvio()
    buscarMsj(codMod)
})
jQuery.ajaxSetup({
  contentType: "application/x-www-form-urlencoded; charset=UTF-8"});
}
function cerrarMsjDlg(){
    resetFormEnvio()
    buscarMsj(codMod)
    buscarMsjEnv(codMod)
    jQuery("#msjDlg").dialog("close")
}
function enviarMSJ(){
var txtAsunto=jQuery("#txtasunto").val()
var txtMensaje=jQuery("#txtmensaje").val()

if(txtAsunto==undefined ||txtAsunto=="" )
 {alert("Ingrese el asunto del mensaje!")
     return;
 }

if(txtMensaje==undefined ||txtMensaje=="" )
 {alert("Ingrese el mensaje!")
    return;
 }


//var userEnvia=jQuery("#txtmensaje")

//alert(txtMensaje)
var mensaje={
    asunto:txtAsunto,
    mensaje:txtMensaje
}

//alert(JSON.stringify(mensaje))
var datos={
    OPC:"I",
    TIPO:"IE",
    COD_UGEL:ugel,
    COD_MOD:codMod,
    mensaje:JSON.stringify(mensaje)
}
//alert(JSON.stringify(datos))

//jQuery.getJSON(urlMsj, datos, devEnvio,"json");
//jQuery.post(urlMsj, JSON.stringify(datos), devEnvio,"json");
//jQuery.post(urlMsj, datos, devEnvio,"json");

//jQuery.ajax({url:urlMsj,type:"POST",data:datos,dataType:"json",contentType:"application/json; charset=utf-8",success:devEnvio})
jQuery.ajax({url:urlMsj,type:"POST",data:datos,dataType:"json", beforeSend: function(x) {
            if (x && x.overrideMimeType) {
              x.overrideMimeType("json;charset=UTF-8");
            }
          },success:devEnvio})

}

function devEnvio(data)
{
    if(data!=undefined && data.status=="ok")
   {alert("Su mensaje se envió correctamente")
        resetFormEnvio()
        buscarMsjEnv(codMod);
        
   }else
       {alert("Ocurrio un error al enviar su mensaje")
       }
}
function resetFormEnvio()
{
    document.getElementById("formMsj").reset();
    //jQuery("#msjlist tbody").empty()
    //jQuery(".paginador").empty()
}
function buscarMsj(usuario)
{
    var filtroBq={start:inicio};
    /*if(cantTotal==0)
    {*/
    jQuery.get(urlMsj+"?OPC=C&TIPO=IE&COD_UGEL="+ugel+"&COD_MOD="+codMod,filtroBq, cargarCuenta, "text")
    /*}else{
        cargarCuenta(cantTotal)
    }*/
    jQuery.getJSON(urlMsj+"?OPC=M&TIPO=IE&COD_UGEL="+ugel+"&COD_MOD="+codMod, filtroBq,cargarMsj)

}
function cargarMsj(data)
{jQuery("#msjlist td").empty()
    jQuery("#listMensajeria tbody").empty()
    var tbody=jQuery("#listMensajeria tbody")
    if(data.data !=undefined){
          if(jQuery.isArray(data.data))
            {jQuery.each(data.data, function(i,msj){
                mostrarMsj(tbody,i,msj)
            })
            }else{
                mostrarMsj(tbody,0,data.data)
            }
        }
}
function cargarCuenta(cuenta){
    //alert(cuenta)
    jQuery(".paginador").empty()
    cantTotal=cuenta;
    var max=10;
    var $cuenta=parseInt(cuenta)
    if (isNaN($cuenta))return;

    if (inicio>0){
        jQuery("<a/>",{
            innerHTML:"Anterior",
            href:"#"
        }).appendTo(".paginador").click(function(){
            inicio-=max
            resetFormEnvio()
            buscarMsj(codMod)
            return false
        })
    }

    if ($cuenta>(inicio+max)){
        jQuery("<a/>",{
            innerHTML:"Siguiente",
            href:"#"
        }).appendTo(".paginador").click(function(){
            inicio+=max
            resetFormEnvio()
            buscarMsj(codMod)
            return false
        })
    }
}
function mostrarMsj(tbody,i,msj)
{
    var tr=jQuery("<tr/>").appendTo(tbody).addClass(msj.leido?"row1":"row0")
    

    var lkIcon=jQuery("<a/>",{innerHTML:"",id:msj.id,href:"#",
            click:function(){
                    leerMsj(this.id)
                    return false
                }}).addClass(msj.leido?"leerMsj1":"leerMsj");    
    jQuery("<td/>").appendTo(tr).append(lkIcon)

    if(msj.leido)
    {var lkQuit=jQuery("<a/>",{innerHTML:"",id:msj.id,href:"#",
            click:function(){
                    quitMsj(this.id)
                    return false
                }}).addClass("quitMsj");
    jQuery("<td/>").appendTo(tr).append(lkQuit)
    }else{
        jQuery("<td/>").appendTo(tr).append("-")
    }

    var lkUOrigen=jQuery("<a/>",{innerHTML:msj.usuarioOrigen,id:msj.id,href:"#",
            click:function(){
                    leerMsj(this.id);
                    return false
                }})
    lkUOrigen.attr("style", "color:#333")
    jQuery("<td/>").appendTo(tr).append(lkUOrigen)
    //jQuery("<td/>",{innerHTML:msj.usuarioOrigen}).appendTo(tr)

    var msjAsunto=msj.asunto;
    if(msj.asunto.length>17){
        msjAsunto=msj.asunto.substring(0,15)+"..";
    }

    var lkAsunto=jQuery("<a/>",{
            innerHTML:msjAsunto,
            id:msj.id,
            href:"#",
            click:function(){
                    leerMsj(this.id)
                    return false
                }
            })
    
    lkAsunto.attr("style", "color:#333")
    jQuery("<td/>").appendTo(tr).append(lkAsunto)



}
function leerMsj(idMsj){    
    var parm={
        idMsj:idMsj
    };
    
    jQuery.getJSON(urlMsj+"?OPC=L&TIPO=IE&COD_UGEL="+ugel+"&COD_MOD="+codMod, parm, function(msj){
        var tDE='<div id="tDE" class="outputLine" > <span class="labelField">Enviado por:</span><span class="valueField">'+msj.data.usuarioOrigen+'</span></div>'
    var tASUNTO='<div id="tASUNTO" class="outputLine" > <span class="labelField">Asunto:</span><span class="valueField">'+msj.data.asunto+'</span></div>'
    var tFECHA='<div id="tFECHA" class="outputLine" > <span class="labelField">Fecha:</span><span class="valueField">'+msj.data.fechaCrea+'</span></div>'
    var lMENSAJE='<div id="lMENSAJE" class="outputLine" > <span class="labelField">Mensaje:</span></div>'
    var tMENSAJE='<div id="tMENSAJE" class="outputLineMsg" ><span class="labelField"></span> <span class="valueField">'+msj.data.mensaje+'</span></div>'

    var popup = new Popup("Mensaje recibido",
	tDE+tASUNTO+tFECHA+lMENSAJE+tMENSAJE,
	"Cerrar",
	buscarMsj(codMod),
	null ,
        null);
    popup.show()
    /*
    jQuery("#tDE .valueField").text(msj.data.usuarioOrigen)
    jQuery("#tASUNTO .valueField").text(msj.data.asunto)
    jQuery("#tFECHA .valueField").text(msj.data.fechaCrea)
    jQuery("#tMENSAJE .glosaField").text(msj.data.mensaje)
    
    jQuery("#msjDlg").dialog("open")*/

    })
}
function quitMsj(idMsj){
    
    var opcQuit=confirm("Esta seguro que desea eliminar?")
    if(opcQuit){
        var parm={idMsj:idMsj}
        jQuery.getJSON(urlMsj+"?OPC=Q&TIPO=IE&COD_MOD="+codMod, parm, function(msj){
        //resetFormEnvio();
        inicio=0;
        buscarMsj(codMod);
        })
    }
}

// INICIO - LECTURA DE MENSAJES ENVIADOS
function buscarMsjEnv(usuario)
{var filtro={start:inicioEnv}
    codMod=usuario;
    jQuery.get(urlMsj+"?OPC=C_ENV&TIPO=IE&COD_MOD="+codMod,filtro, cargarCuentaEnv, "text")
    
    jQuery.getJSON(urlMsj+"?OPC=M_ENV&TIPO=IE&COD_MOD="+codMod,filtro, cargarMsjEnv)
}
function cargarCuentaEnv(cuenta){
    jQuery(".paginadorEnv").empty()
    cantTotalEnv=cuenta;
    var max=10;
    var $cuenta=parseInt(cuenta)
    if (isNaN($cuenta))return;
    if (inicioEnv>0){
        jQuery("<a/>",{
            innerHTML:"Anterior",
            href:"#"
        }).appendTo(".paginadorEnv").click(function(){
            inicioEnv-=max
            buscarMsjEnv(codMod)
            return false
        })
    }

    if ($cuenta>(inicioEnv+max)){
        jQuery("<a/>",{
            innerHTML:"Siguiente",
            href:"#"
        }).appendTo(".paginadorEnv").click(function(){
            inicioEnv+=max
            buscarMsjEnv(codMod)
            return false
        })
    }
}
function cargarMsjEnv(data)
{   //jQuery("#msjlist td").empty()
    jQuery("#msjlistEnv tbody").empty()
    var tbody=jQuery("#listMensajeriaEnv tbody")
    if(data.data !=undefined){
          if(jQuery.isArray(data.data))
            {jQuery.each(data.data, function(i,msj){
                mostrarMsjEnv(tbody,i,msj)
            })
            }else{
                mostrarMsjEnv(tbody,0,data.data)
            }
        }
}
function mostrarMsjEnv(tbody,i,msj)
{var tr=jQuery("<tr/>").appendTo(tbody).addClass("row0")
    //jQuery("<td/>",{innerHTML:msj.usuarioOrigen}).appendTo(tr)
    var msjAsunto=msj.asunto;
    if(msj.asunto.length>25){
        msjAsunto=msj.asunto.substring(0,20)+"....";
    }
    var lkAsunto=jQuery("<a/>",{
            innerHTML:msjAsunto,
            id:msj.id,
            href:"#",
            click:function(){
                    leerMsjEnv(this.id)
                    return false
                }
            })
    lkAsunto.attr("style", "color:#333")
    jQuery("<td/>").appendTo(tr).append(lkAsunto)
}
function leerMsjEnv(idMsj){
    var parm={
        idMsj:idMsj
    };
    
    jQuery.getJSON(urlMsj+"?OPC=L_ENV&TIPO=IE&COD_MOD="+codMod, parm, function(msj){
    /*jQuery("#tDE .valueField").text(msj.data.usuarioOrigen)
    jQuery("#tASUNTO .valueField").text(msj.data.asunto)
    jQuery("#tFECHA .valueField").text(msj.data.fechaCrea)
    jQuery("#tMENSAJE .glosaField").text(msj.data.mensaje)
    jQuery("#msjDlg").dialog("open")*/
     var tASUNTO='<div id="tASUNTO" class="outputLine" > <span class="labelField">Asunto:</span><span class="valueField">'+msj.data.asunto+'</span></div>'
    var tFECHA='<div id="tFECHA" class="outputLine" > <span class="labelField">Fecha:</span><span class="valueField">'+msj.data.fechaCrea+'</span></div>'
    
    var lMENSAJE='<div id="lMENSAJE" class="outputLine" > <span class="labelField">Mensaje:</span></div>'
    var tMENSAJE='<div id="tMENSAJE" class="outputLineMsg" ><span class="labelField"></span> <span class="valueField">'+msj.data.mensaje+'</span></div>'

    var popup = new Popup("MENSAJE ENVIADO",
	tASUNTO+tFECHA+lMENSAJE+tMENSAJE,
	"OK",
	null,
	null,
        null);
    popup.show()

    })


}
// FINAL  - LECTURA DE MENSAJES ENVIADOS

