// by (qhapaq) moises torres romero - 09/2010
ECO = window.ECO || {};
(function () {
	if (ECO && ECO.Widget) {
		//throw ("ya existe ECO.Widget");
		return;
	}
	function slide(obj){
		timeToSlide = 35;
		obj.style.visibility = "hidden";
		obj.style.display = "block";
		var height = obj.offsetHeight;
		obj.style.height="0px";
		obj.style.visibility = "visible";
		var pxPerLoop = height/timeToSlide;
		slideDown(obj,0,height,pxPerLoop);
	}
	function slideDown(obj,offset,full,px){
		if(offset < full){
			obj.style.height = offset+"px";
			offset=offset+px;
			setTimeout((function(){slideDown(obj,offset,full,px);}),1);
		} else {
			obj.style.height = "auto";
		}
	}
	var ie = navigator.userAgent.match(/MSIE\s([^;]*)/);
	var formatDate = function ( str, top ) {
		var a = str.split(/[-:\s]/), m = a[1] -1;
		var update = new Date(a[0],m,a[2],a[3],a[4],a[5]), now = new Date();
		var ms = ["","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
		/*si es mas de 24 horas*/
		//return ((now-update)>(24*60*60*1000)) ? (a[2] + "." + a[1] + "." + a[0] + " " + a[3] + ":" + a[4] + ":" + a[5]) : (a[3] + ":" + a[4]+ ":" + a[5]);
		if ((now-update)>(12*60*60*1000)){
			return (top?"Actualizado el ":"")+ a[2] + " de " + ms[a[1]*1] + " a las " + a[3] + ":" + a[4] + ":" + a[5];
		}else{
			return (top?"Actualizado el ":"")+ a[3] + ":" + a[4]+ ":" + a[5];
		}
	}
	var loadCssOk = function () {
		if (document.defaultView && document.defaultView.getComputedStyle) {
			return function (obj, estilo) {
				var i = null;
				var h = document.defaultView.getComputedStyle(obj, "");
				if (h) {
					i = h[estilo]
				}
				var f = obj.style[estilo] || i;
				return f
			}
		} else {
			if (document.documentElement.currentStyle && ie) {
				return function (obj, estilo) {
					var g = obj.currentStyle ? obj.currentStyle[estilo] : null;
					return (obj.style[estilo] || g)
				}
			}
		}
	}();
	var get = function (id) {  return (typeof id == "string") ? document.getElementById(id) : id };
	ECO.Widget = function (par) {
		this.init(par)
	};
	(function () {
		ECO.Widget.eschemaMain = '<div class="ecowg-dc"><div class="ecowg-hd"><h1 class="ecowg-hd-tt"><span class="ecowg-hd-ev"></span> | <span class="ecowg-hd-ds"></span></h1><p class="ecowg-hd-up">Actualizado a las <span id="ecowg-hd-tm">...</span></p></div><div class="ecowg-bd"></div></div><div class="ecowg-sh"><a href="http://elcomercio.pe/widget/elecciones-2010/" target="_blank" id="ecowg-sh-btn">Lleva este contenido a tu sitio web</a></div>';
		ECO.Widget.eschemaBody = '<div class="ecowg-bd-time">[time]</div>[elem]<div class="ecowg-bd-text">[text]</div>';
		ECO.Widget.pathSrc = "http://w.ecodigital.pe/";
		//ECO.Widget.pathSrc = "http://localhost/f/widget/";
		ECO.Widget.itemView = 10;
		ECO.Widget.objs = {};
		ECO.Widget.data = function(js){
			var t = ECO.Widget.objs["ecowg-" + js.dominio + "-" + js.id], T = ECO.Widget, nodos = js.flujo.length;
			if (typeof t == "undefined") return false;
					clearTimeout(t.timer);
					if (t.request==0){
						t.objChildren[0].children[0].children[0].innerHTML = js.evento;
						t.objChildren[0].children[0].children[1].innerHTML = js.descripcion;
						t.objChildren[0].children[1].innerHTML = formatDate(js.lastupdate, true);
						t.idShowLast = ((nodos > T.itemView ) ? T.itemView : nodos );
						t.idShowFirst = parseInt(js.flujo[0].id);
						t.drawItems(js.flujo,0, t.idShowLast, "init");
						t.data = js.flujo;
						t.timer = setTimeout(function(){
							t.loadJs(ECO.Widget.pathSrc + "data/" + t.filter[2] + "/last10.js");
						}, t.interval);

					}else{
						if (nodos>0){
							t.objChildren[0].children[1].innerHTML = formatDate(js.lastupdate, true);
							var fin = -1;
							for (var i = 0; i < nodos; i++){
									if (t.idShowFirst < parseInt(js.flujo[i].id) ){
										fin = i;
									}else{
										i = nodos;
									}
							}
							if (fin>-1){
								t.drawItems(js.flujo,0, fin, "before");
								t.idShowFirst = parseInt(js.flujo[0].id);
							}
						}
						var sc = document.getElementById("ecowg-js-live"+t.id); if (sc) sc.parentNode.removeChild(sc);
						t.timer = setTimeout(function(){
							t.loadJs(ECO.Widget.pathSrc + "data/" + t.filter[2] + "/last10.js");
						}, t.interval);
					}
					t.request++;
					return this;
		};
		ECO.Widget.prototype = function () {
			return {
				init: function (par) {
					var t = this;
					t.width = 400;
					t.height = 350;
					t.interval = 5000;
					for (var p in par ) t[p] = par[p];
					t.filter = "qhapaq";
					t.idShowFirst = 0;
					t.request = 0;
					t.id = Math.ceil(Math.random()*100000);
					t.loadCssStatus = 0;
					t.obj = {};
					t.objChildren = {};
					t.timer = 0;
					//ECO.Widget.objs["ecowg-" + t.id] = t;
					return this;
				},
				draw: function (filter){
					var t = this, T = ECO.Widget;
					t.filter = filter.split("\/");
					document.write('<div class="ecowg ecowg-'+t.filter[0]+'-'+t.filter[1]+'" id="ecowg-' + t.id + '" style="width:' + t.width + 'px;height:' + t.height + 'px"></div>');
					t.obj = get("ecowg-"+t.id);
					t.loadCss(T.pathSrc  + t.filter[0] + "/" + t.filter[1] + "/estilo.css", t.obj);
					ECO.Widget.objs["ecowg-" + t.filter[0] + "-" + t.filter[2]] = t;
					return this;
				},
				loadCss: function (url, obj){
					var t = this, T = ECO.Widget;

					if (t.loadCssStatus==0) {
						t.loadCssStatus = 1;
						var lnk = document.createElement("link");
						lnk.href = url;
						lnk.rel = "stylesheet";
						lnk.type = "text/css";
						document.getElementsByTagName("head")[0].appendChild(lnk);
						var i = setInterval(function () {
							var j = loadCssOk(obj, "position");
							if (j == "relative") {
								clearInterval(i);
								t.loadCssStatus = 2;
								t.obj.innerHTML = T.eschemaMain;
								t.objChildren = t.obj.children[0].children;
								t.obj.children[1].children[0].href = "http://"+t.filter[0]+".pe/widget/"+t.filter[1]+"/";
								t.objChildren[1].style.height = (t.height - (t.objChildren[0].clientHeight + t.obj.children[1].clientHeight)) + "px";
								t.loadJs(ECO.Widget.pathSrc + "data/" + t.filter[2] + "/last50.js");
							}
						}, 50)
					}
				},
				loadJs: function (url){
					var js = document.createElement("script");
					js.type = "text/javascript";
					js.id = ((url.indexOf("last10.js")>0) ? "ecowg-js-live" : "ecowg-js") + this.id;
					//js.src = url + "?idcallback="+this.id+"&noc="+Math.ceil(Math.random()*100000);
					js.src = url + "?noc="+Math.ceil(Math.random()*100000);
					document.getElementsByTagName("head")[0].appendChild(js);
					return this;
				},
				drawItems: function (items, ini, fin, position){
					var p = (typeof position) == "undefined" ? "after" : position;
					var t = this, T = ECO.Widget, html = "";
					var i	= (p=="before")?fin:ini;
					while (i!="fin"){
						var div = document.createElement("div"); div.className = "ecowg-bd-box";
						var item = items[i];
						var schema = T.eschemaBody;
						var elem = "";
						if (item.elemento){
							if ((item.elemento.indexOf(".jpg")+4) == (item.elemento.length)){ /* si es jpg */
								elem =  '<div class="ecowg-bd-elem ' + ((typeof item.tp == "string")?"ecowg-bd-elem-"+item.tp:"" ) + '" >';
								if (typeof item.elementolink == "string") elem += '<a href="'+item.elementolink+'" target="_blank">';
								elem += '<img class="ecowg-bd-img" src="' + item.elemento + '" />';
								if (typeof item.elementolink == "string") elem += '</a>';
								elem += '</div>';
							}else{
								elem = '<div class="ecowg-bd-elem video"><object width="273" height="227"><param name="movie" value="http://www.youtube.com/v/' + item.elemento + '?fs=1&amp;hl=es_ES"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' + item.elemento + '?fs=1&amp;hl=es_ES" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="273" height="227"></embed></object></div>';
							}
						}
						div.innerHTML = schema.replace("[time]",formatDate(item.modtime)).replace("[elem]",elem).replace("[text]",item.text);
						if (p=="before"){
							div.style.display = "none";
							t.objChildren[1].children[0].parentNode.insertBefore(div, t.objChildren[1].children[0]);
							slide(div);
						}else{
							var last = t.objChildren[1].children.length;
							if (last && p!="init"){
								last = t.objChildren[1].children[last-1];
								last.parentNode.insertBefore(div,last);
							}else{
								t.objChildren[1].appendChild(div);
							}
						}
						if (p=="before"){
							i--;
							if (i<ini) i = "fin";
						}else{
							i++;
							if (i>=fin) i = "fin";
						}
					}
					if (p=="init"){
						var div = document.createElement("div"); div.className = "ecowg-ft";
						var lnk = document.createElement('a')
						lnk.appendChild(document.createTextNode("Ver mas"));
						lnk.setAttribute('href','#mas');
						lnk.onclick  = function(){
							if (typeof t.data == "undefined") return false;
							if (t.idShowLast>=t.data.length) return false;
							var next = t.idShowLast + T.itemView;
							next = (next>t.data.length)?t.data.length:next;
							t.drawItems(t.data,t.idShowLast, next);
							t.idShowLast = next;
							return false;
						}
						div.appendChild(lnk);
						t.objChildren[1].appendChild(div);
					}
				}
			}
		}()
	})()
})();