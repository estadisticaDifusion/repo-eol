/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function Popup(popupTitle,popupHtml, okButtonText, okButtonHandler, cancelButtonText, cancelButtonHandler)
{

        this.popupTitle=popupTitle;
        /* HTML content to be shown inside the popup */
	this.popupHtml = popupHtml;

	/* text for the ok button */
	this.okButtonText = okButtonText;

	/* text for the cancel button */
	this.cancelButtonText = cancelButtonText;

	/* handler function for the ok button */
	this.okButtonHandler = okButtonHandler;

	/* handler function for the cancel button */
	this.cancelButtonHandler = cancelButtonHandler;

	/* page cover DOM element */
	this.coverElement = null;

	/* popup DOM element */
	this.popupElement = null;
}

Popup.MAIN_CSS_CLASS = 'popup';

Popup.TITLE_CSS_CLASS = 'popup_title';

/* CSS class applied to popup content */
Popup.TEXT_CSS_CLASS = 'popup_text';

/* CSS class applied to popup buttons' container */
Popup.BUTTONS_CSS_CLASS = 'popup_buttons';

/* path of the image used to opacize the widget */
Popup.COVER_IMAGE_SRC = 'images/page_cover.png';

/* element ID of the OK button */
Popup.OK_BUTTON_ID = 'popup_button_ok';

/* element ID of the CANCEL button */
Popup.CANCEL_BUTTON_ID = 'popup_button_cancel';

/* minimum margin of the popup from screen boundaries */
Popup.MARGIN = 450;

/* z-index to be used for the popup DOM element */
Popup.zIndex = 10000;

Popup.prototype.show = function(beforeShowHandler)
{
	/* calculate the width and height to be covered */
        var winHeight=window.innerHeight!=undefined?window.innerHeight:document.body.clientHeight;
	var bodyHeight = Math.max(document.body.offsetHeight, winHeight);
	var bodyWidth = document.body.offsetWidth;

	/* build the cover that will opacize the widget's content */
	/*var coverElement = document.createElement('img');
	coverElement.src = Popup.COVER_IMAGE_SRC;
	coverElement.style.position = 'absolute';
	coverElement.style.width = '100%';
	coverElement.style.height = bodyHeight + 'px';
	coverElement.style.top = '0px';
	coverElement.style.left = '0px';
	coverElement.style.zIndex = Popup.zIndex - 1;

	this.coverElement = coverElement;

	
	document.body.appendChild(coverElement);*/

	/* now, build the popup DOM element */
	var popupElement = document.createElement('div');
	popupElement.style.position = 'absolute';
        
	popupElement.style.width = (bodyWidth - 2 * Popup.MARGIN) + 'px';//500px
	popupElement.style.left = (document.body.scrollLeft + Popup.MARGIN) + 'px';//500px
	popupElement.style.zIndex = Popup.zIndex;
	popupElement.className = Popup.MAIN_CSS_CLASS;

	this.popupElement = popupElement;

        /*poput titulo*/
        var txtTitleElement=document.createElement('div');
        txtTitleElement.className = Popup.TITLE_CSS_CLASS;
        //txtTitleElement.style.width = (bodyWidth - 2 * Popup.MARGIN) + 'px';//500px
	txtTitleElement.innerHTML = this.popupTitle;
	popupElement.appendChild(txtTitleElement);



	/* build the DOM element that contains the popup content */
	var textElement = document.createElement('div');
	textElement.className = Popup.TEXT_CSS_CLASS;
	textElement.innerHTML = this.popupHtml;
	popupElement.appendChild(textElement);

	/* add the ok and cancel buttons */
	var buttonsContainer = document.createElement('div');
	buttonsContainer.className = Popup.BUTTONS_CSS_CLASS;
	popupElement.appendChild(buttonsContainer);

	
	if(this.cancelButtonText != null)
	{
		var cancelButton = document.createElement('div');
		cancelButton.id = Popup.CANCEL_BUTTON_ID;
		cancelButton.innerHTML = this.cancelButtonText;
		buttonsContainer.appendChild(cancelButton);

		cancelButton.onclick = function()
		{
			self.cancelButtonPressed();
		}
	}

    //var okButton = document.createElement('div');
      var okButton = document.createElement('button');
	okButton.id = Popup.OK_BUTTON_ID;
	okButton.innerHTML = this.okButtonText;
        //okButton.type=button;
	buttonsContainer.appendChild(okButton);

	var self = this;

	okButton.onclick = function()
	{
		self.okButtonPressed();
	}


	if(beforeShowHandler)
		beforeShowHandler(popupElement);

	/* finally, append the popup element to the document's body */
	document.body.appendChild(popupElement);

	/* let's center the popup */
	var popupTop = Math.max(
		(document.body.scrollTop + (winHeight - popupElement.offsetHeight) / 2),
		document.body.scrollTop
	);

	popupElement.style.top = popupTop + 'px';
}

/**
 * Function called when the popup CANCEL button is pressed
 */
Popup.prototype.cancelButtonPressed = function()
{
	this.hide();

	if(this.cancelButtonHandler != null)
		this.cancelButtonHandler();
}
/**
 * Function called when the popup OK button is pressed
 */
Popup.prototype.okButtonPressed = function()
{
	this.hide();

	if(this.okButtonHandler != null)
		this.okButtonHandler();
}
Popup.prototype.hide = function()
{
	this.popupElement.parentNode.removeChild(this.popupElement);
	//this.coverElement.parentNode.removeChild(this.coverElement);

	this.popupElement = null;
	this.coverElement = null;
}

/* Contenidos */


function FieldPopup(lField,tField){


}