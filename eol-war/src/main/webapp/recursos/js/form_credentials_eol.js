var AUTH_URL = '/eol-auth-ws';
var AUTH_UPDATE_CREDENTIALS_API_URL = AUTH_URL + '/api/v1.0/post';

function getUpdateRefreshToken(filterQuery, callbackCount, callbackViewCount, callbackList, callbackViewList) {
	console.info(":: getUpdateRefreshToken : Start");
	$.ajax({
		url : "/estadistica/ugel/accessOAuth2",
		type: "POST",
	    dataType: "json",
	    contentType: "application/json", 
	    mimeType: "application/json",
		success : function(data) {
			if (data.status == "SUCCESS") {
				var newToken = data.newToken;
    			
				
				
				jQuery("#eolinfo").val(newToken);
				console.info(":: getUpdateRefreshToken : End");
    			callbackCount(filterQuery, newToken, null, callbackList, null, callbackViewCount, callbackViewList);
			}
		},
	    error:function(data,status,er) {
	        console.info("getUpdateRefreshToken error: "+data+" status: "+status+" er:"+er);
	    }
	});
	console.info(":: getUpdateRefreshToken : End");
}

/*
function getUpdateRefreshToken(callbackCount) {
	console.info(":: getUpdateRefreshToken : Start");
	$.ajax({
		url : "/estadistica/ugel/accessOAuth2",
		type: "POST",
	    dataType: "json",
	    contentType: "application/json",
	    mimeType: "application/json",
		success : function(data) {
			if (data.status == "SUCCESS") {
				var newToken = data.newToken;
    			jQuery("#token").val(newToken);
				console.info(":: getUpdateRefreshToken : End");
    			callbackCount(newToken, null, null);
			}
		},
	    error:function(data,status,er) {
	        console.info("getUpdateRefreshToken error: "+data+" status: "+status+" er:"+er);
	    }
	});
	console.info(":: getUpdateRefreshToken : End");
};
*/
function getCredentialObject() {
	var data={
			"usuario" 			: $('#soldier').val(),
			"contrasenia" 		: $('#today').val(),
			"nuevaContrasenia" 	: $('#tomorrow').val()
	};
	
	return cipher(JSON.stringify(data));
};

function getEncryptByParam(param) {
	
	return CryptoJS.AES.encrypt(param, "eol2018");
};

function resetSystem(){
	
	document.forms['formCredentials'].action = '/estadistica/logout.uee'; 
    document.forms['formCredentials'].submit();
    
    getUpdateRefreshToken(callbackCount); 
};

function validarActualizarContrasenia(id,usu,newusu){
    var tema =  $('#today').val();
    var control = $('#tomorrow').val();
    var newcontrol = $('#tomorrowtwo').val();
    
    var jtema=JSON.parse(id);
    var jcontrol=JSON.parse(usu);
    var jnewcontrol=JSON.parse(newusu);
    var initUser=intbolt();
    var othertema=decipher(jtema.salt, jtema.iv, initUser.oneger , jtema.cipher) ;
    var othercontrol=decipher(jcontrol.salt, jcontrol.iv, initUser.oneger , jcontrol.cipher) ;
    var othernewcontrol=decipher(jnewcontrol.salt, jnewcontrol.iv, initUser.oneger , jnewcontrol.cipher) ;
    
    
    if(othertema != "" && othercontrol != "" && othernewcontrol != "") {
     	if(!/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{7,15}$/.test(othercontrol)) {
     		  $("#mensaje").html('La contraseña debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.').css({'background-color':'#FAD9D7','color' : '#910B03'});
     		return false;
     	} 
     	if(othercontrol == othernewcontrol) {
    		$("#msgActualizacion").html('Espere por favor. La actualización de las credenciales podría demorar algunos segundos...').css({'background-color':'#FAD9D7','color' : '#910B03'});
    		$("#mensaje").html('');
			var thogeter = jQuery("#thogeter").val();
			
    	    var urlRest = AUTH_UPDATE_CREDENTIALS_API_URL + "/updatecredentials";
    	    var objectUpdate = getCredentialObject();   
    		var dataEncrypt={
    				"salt"		:	objectUpdate.salt,
    				"iv" 		: 	objectUpdate.iv,
    				"cipher" 	: 	objectUpdate.cipher
    				};
    		
    	    	$.ajax({
    	            type: 'POST',
    	            contentType: 'application/json',
    	            url: urlRest,
    	            dataType: 'json',
    	            timeout: "60000",
    	            data: JSON.stringify(dataEncrypt),
    	    		beforeSend: function(request) {
    	    			request.setRequestHeader("Authorization", "Bearer "+thogeter);
    	    			request.setRequestHeader("Content-Type", "application/json");
    	    		},
    	            success: function(data) {
    	                if (data.status == "SUCCESS") {
    	                	
    	                	var stateUpdate = data.responsePayload.stateUpdate;
    	                    if (stateUpdate == "0") {
    	                    	alert('La contraseña fue actualizada correctamente. Vuelva a acceder al Sistema.');
    	                    	$('#today').val(cipher(JSON.stringify($('#today').val())));
    	                        $('#tomorrow').val(cipher(JSON.stringify($('#tomorrow').val())));
    	                        $('#tomorrowtwo').val(cipher(JSON.stringify($('#tomorrowtwo').val())));
    	                        resetSystem();
    	                        
    	                    } else if (stateUpdate == "1") {
    	                    	$('#today').val(cipher(JSON.stringify($('#today').val())));
    	                        $('#tomorrow').val(cipher(JSON.stringify($('#tomorrow').val())));
    	                        $('#tomorrowtwo').val(cipher(JSON.stringify($('#tomorrowtwo').val()))); 
    	                    	$("#mensaje").html('La contraseña actual no es correcta, verifique y vuelva intentar.').css({'background-color':'#FAD9D7','color' : '#910B03'});
    	                    	 $("#msgActualizacion").html('');
    	                    	    
    	                    } else if (stateUpdate == "2") {
    	                    	$('#today').val(cipher(JSON.stringify($('#today').val())));
    	                        $('#tomorrow').val(cipher(JSON.stringify($('#tomorrow').val())));
    	                        $('#tomorrowtwo').val(cipher(JSON.stringify($('#tomorrowtwo').val())));
    	                    	$("#mensaje").html('La nueva contraseña no debe ser igual a la actual, verifique y vuelva intentar.').css({'background-color':'#FAD9D7','color' : '#910B03'});
    	                        $("#msgActualizacion").html('');
    	                    } else if (stateUpdate == "3") {
    	                    	$('#today').val(cipher(JSON.stringify($('#today').val())));
    	                        $('#tomorrow').val(cipher(JSON.stringify($('#tomorrow').val())));
    	                        $('#tomorrowtwo').val(cipher(JSON.stringify($('#tomorrowtwo').val())));
    	                    	$("#mensaje").html('No puede usar una contraseña antigua, verifique y vuelva intentar.').css({'background-color':'#FAD9D7','color' : '#910B03'});
    	                        $("#msgActualizacion").html('');
    	                    } else if (stateUpdate == "4") {
    	                    	$('#today').val(cipher(JSON.stringify($('#today').val())));
    	                        $('#tomorrow').val(cipher(JSON.stringify($('#tomorrow').val())));
    	                        $('#tomorrowtwo').val(cipher(JSON.stringify($('#tomorrowtwo').val())));
    	                    	$("#mensaje").html('La nueva constraseña contiene caracteres extraños, verifique y vuelva intentar.').css({'background-color':'#FAD9D7','color' : '#910B03'});
    	                        $("#msgActualizacion").html('');
    	                    }
    	                }
    	            },
    	            error: function(XMLHttpRequest, textStatus, errorThrown) {
    	    			if (XMLHttpRequest.responseText.indexOf("Access token expired:")!=-1) {
    	    				console.info("El token de acceso expiro!");
    	    				callbackNewToken(callbackCount);
    	    			} else if (XMLHttpRequest.status==401) {
    	    				console.info("Se intento acceder a la seguridad externamente!");
    	    				callbackNewToken(callbackCount);
    	    			} else if (XMLHttpRequest.status==403) {
    	    				console.info("problem 403!");
    	    				alert('Servicio no disponible, por favor inténtelo en unos momentos.');
    	    			} else { 
    	    				alert('Error: Comunicarse con el administrador del sistema.');
    	    			}
    	    		}
    	    });

    	} else {
   		  $("#mensaje").html('La nueva contraseña y en repetir nueva constraseña no coinciden.').css({'background-color':'#FAD9D7','color' : '#910B03'});
    		return false;
    	}
    } else {
        if(othertema == "") {
     		  $("#mensaje").html('Ingresar la contraseña actual.').css({'background-color':'#FAD9D7','color' : '#910B03'});
        	return false;
        }
        if(othercontrol == "") {
   		  $("#mensaje").html('Ingresar la nueva constraseña.').css({'background-color':'#FAD9D7','color' : '#910B03'});
        	return false;
        }
        if(othernewcontrol == "") {
     		  $("#mensaje").html('Ingresar en repetir nueva constraseña.').css({'background-color':'#FAD9D7','color' : '#910B03'});
        	return false;
        }
    }
};

function cleanBox(){
	
	cipher(JSON.stringify($('#today').val()));
	cipher(JSON.stringify($('#tomorrow').val()));
	cipher(JSON.stringify($('#tomorrowtwo').val()));
	cipher(JSON.stringify($('#soldier').val()));
	
	$('#today').val("")
	$('#tomorrow').val("")
	$('#tomorrowtwo').val("")
	$('#soldier').val("")
}

/**************************************************INICIO jquery *******************************************/

jQuery(document).ready(function() {
	
	  var type = jQuery('#valueType').val();
      jQuery("#institucion").val(type != 'CE' ? type : 'Servicio Educativo');
      jQuery("#nomInstitucion").val(jQuery('#valueName').val());
      jQuery("#soldier").val(jQuery('#valueUser').val());
	
	
    jQuery('#btnSaveCU').on('click',function() {
    	validarActualizarContrasenia(JSON.stringify(cipher($('#today').val())),JSON.stringify(cipher($('#tomorrow').val())),JSON.stringify(cipher($('#tomorrowtwo').val())));
    	return false;
    	
    });
    
    jQuery('#btnCancelCU').on('click',function() {
  	  	console.info("boton cancelar");
    	regresarInicio();
    });
    
    jQuery('#btnCleanCU').on('click',function() {
    	cleanBox();
    });
	
})
