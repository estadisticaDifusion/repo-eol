	var iterCount = 1000;
	var ks = 128;
	var pass = '0015975320171234';
	
	function generateKey(salt, pass){
		var key = CryptoJS.PBKDF2(pass, CryptoJS.enc.Hex.parse(salt),{
			keySize: ks / 32,
			iterations: iterCount 
		});
		return key;
	};

	function cipher(str){
		var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		var key = generateKey(salt, pass);
		
		var encripted = CryptoJS.AES.encrypt(str, key, {iv: CryptoJS.enc.Hex.parse(iv)});
		return {
			salt: salt,
			iv: iv,
			cipher: encripted.ciphertext.toString(CryptoJS.enc.Base64)
		};
	};

	function intbolt(){
		return {
			oneger : pass
		};
	};
	
	 function decipher(salt, iv, pass, cipherText){
		var key = generateKey(salt, pass);
		var cipherParams = CryptoJS.lib.CipherParams.create({
			ciphertext: CryptoJS.enc.Base64.parse(cipherText)
		});
		var decrypted = CryptoJS.AES.decrypt(
      		cipherParams,
      		key,
      		{ iv: CryptoJS.enc.Hex.parse(iv) }
      	);
		return decrypted.toString(CryptoJS.enc.Utf8);
	};

	 function toBase64(s){
		var words = CryptoJS.enc.Utf8.parse(s);
		return CryptoJS.enc.Base64.stringify(words); 
	};

	function  fromBase64(ss){
		var words = CryptoJS.enc.Base64.parse(ss);
		return CryptoJS.enc.Utf8.stringify(words);
	};

	