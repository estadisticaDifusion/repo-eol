jQuery(document).ready(init)

var PADRON_URL = "/eol-padron-ws";
var PADRON_API_URL = PADRON_URL + "/api/v1.0/post";

var ESBAS_URL = "/eol-estadistica-ws";
var ESBAS_API_URL = ESBAS_URL + "/api/v1.0/post";

var inicio=0;
var cantTotal=0;

var inicioIE=0;
var cantTotalIE=0;

var inicioEnv=0;
var cantTotalEnv=0;

var ugel = "";
var esregSigied = "";
var esregHuelga = "";

var urlR = "";
var urlRSubstring = "";
var urlRAdding = "";
var urlMsj = "";
var urlEol = "";
var urlCEol = "";

function init() {

  ugel = jQuery("#valueUGEL").val();
  esregSigied = jQuery("#esregSigied").val();
  esregHuelga = jQuery("#esregHuelga").val();

  urlR = jQuery("#urlR").val();
  if (urlR != undefined) {
	  urlRSubstring = (urlR.split(";")[0]!=null?urlR.split(";")[0]:urlR);
	  urlRAdding = (urlR.split(";")[1]!=null?urlR.split(";")[1]:"");  
  }
  urlMsj = jQuery("#urlMsj").val();
  urlEol = jQuery("#urlEol").val();
  urlCEol = jQuery("#urlCEol").val();

  jQuery("#tabs").tabs();

  jQuery("#cbTodos").change(function() {
     if(jQuery("input[name='cbTodos']").is(":checked")) {
    	 jQuery("#txtdestino").val("");
         jQuery("#txtdestino").attr("disabled", true);
     } else {
         jQuery("#txtdestino").attr("disabled", false);
     }
  });

  jQuery("#msjRefresh").click(function() {
     inicioEnv=0;
     inicio=0;
     cantTotal=0;
  });
  
  jQuery("#buscar").click(function() {
     inicioIE = 0;
     cantTotalIE = 0;
     searchInstitucionEducativa();
  });

  jQuery("#limpiar").click(function() {
     jQuery("#listadoIIEE thead tr,#listadoIIEE tbody,.paginador_ie,#cuenta").empty();
     document.getElementById("buscarIIEEForm").reset();
  });

  jQuery("#capaBqIE").show();
  jQuery("#capaTabIE").hide();
  
  jQuery("#volverBq").click(function() {
     jQuery("#capaBqIE").show("slow");
     jQuery("#capaTabIE").hide("slow");
  });

  jQuery.ajaxSetup({
     contentType: "application/x-www-form-urlencoded; charset=UTF-8"}
  );

  jQuery("#cod_mod, #codlocal, #codUbigeo").on("keypress keyup blur",function (event) {    
      $(this).val($(this).val().replace(/[^\d].+/, ""));
       if ((event.which < 48 || event.which > 57)) {
           event.preventDefault();
       }
  });

}

function cerrarMsjDlg() {
  resetFormEnvio();
  buscarMsj();
  buscarMsjEnv();
  jQuery("#msjDlg").dialog("close");
}

function enviarMSJ() {
  var txtAsunto=jQuery("#txtasunto").val();
  var txtMensaje=jQuery("#txtmensaje").val();
  var txtDestino="";

  if (txtAsunto==undefined ||txtAsunto=="" ) {
     alert("Ingrese el asunto del mensaje!");
     return;
  }

  if (txtMensaje==undefined ||txtMensaje=="" ) {
	  alert("Ingrese el mensaje!");
      return;
  }

  if(!jQuery("input[name='cbTodos']").is(":checked")){
      txtDestino=jQuery("#txtdestino").val();
      if (txtDestino==undefined ||txtDestino=="" ) {
    	  alert("Ingrese el destino!")
          return;
      }
  }

  var mensaje={
    asunto:txtAsunto,
    mensaje:txtMensaje,
    usuarioDestino:txtDestino
  }

  var datos={
    OPC:"I",
    TIPO:"UGEL",
    COD_UGEL:ugel,
    mensaje:JSON.stringify(mensaje)
  }

  jQuery.ajax({url:urlMsj,type:"POST",data:datos,dataType:"json", beforeSend: function(x) {
      if (x && x.overrideMimeType) {
        x.overrideMimeType("application/json;charset=UTF-8");
      }
    },success:devEnvio})

}

function devEnvio(data) {
    if (data!= undefined && data.status=="ok") {
        alert("Su mensaje se envió correctamente");
        inicioEnv=0;
        resetFormEnvio();
        buscarMsjEnv();
   } else if (data!= undefined && data.status=="error") {
       alert("Ocurrio un error al enviar su mensaje."+data.mensaje);
   } else {
	   alert("Ocurrio un error al enviar su mensaje");
   }
}

function resetFormEnvio() {
	document.getElementById("formMsj").reset();
    jQuery("#txtdestino").attr("disabled", true);    
}

var campos;
var columnas={"codMod":"Código modular",
    "anexo":"Anexo",    
    "codlocal":"Código de Local",
    "cenEdu":"Nombre",
    "nivMod":"Nivel y/o Modalidad",
    "dirCen":"Dirección",
    "telefono":"Teléfono",
    "director":"Director",
    "gestion":"Gestión",
    "distrito":"Distrito",
    "cenPob":"Centro Poblado"
 };
//"EOL":"--",

function searchInstitucionEducativa() {
	jQuery(".paginador_ie").empty();
    var theadTr=jQuery("#listadoIIEE thead tr").empty();
    var codUgel=ugel;
    jQuery("#listadoIIEE tbody").empty();
    
    var codmod=jQuery("#cod_mod").val();
    var codlocal=jQuery("#codlocal").val();
    var nombreIE=jQuery("#nombreIE").val();
    var nombreCP=jQuery("#nombreCP").val();
    var nivelI=jQuery("#nivelI").val();
    var gestionI=jQuery("#gestionI").val();
    var codUbigeo=jQuery("#codUbigeo").val();

    if(nivelI == '00')
        nivelI = '';
    if(gestionI == '00')
        gestionI = '';

//    if (ugel.substring(4,2) =='00'|| ugel =='150101' ||ugel=='150200'||ugel=='070101') {
//        codUgel=ugel.substring(0,4);
//    } // CAMBIO ECHO POR MATEO. FILTRO EN EL BUSCADOR EOL
    if (ugel.substring(4,6) =='00'|| ugel =='150101' ||ugel=='070101') {
        codUgel=ugel.substring(0,4);
    }
	
    var filterQuery = {
    	"nombreIE":nombreIE,
    	"codooii": codUgel,
    	"codmod":codmod,
    	"codlocal":codlocal,
    	"nombreCP":nombreCP,
    	"niveles": [nivelI],
    	"gestiones":[gestionI],
    	"ubigeo":codUbigeo,
    	"estados": ["1"],
    	"start": inicioIE
    };

    campos=new Array();
    jQuery("<th/>").text("#").appendTo(theadTr);
    var pos=0;

    jQuery.each(columnas,function(cName,cVal){
        campos[pos]=cName
        jQuery("<th/>").text(cVal).appendTo(theadTr)
        pos=pos+1
    });

    var infoeol = jQuery("#eolinfo").val();

    findInstitucionCount(filterQuery, infoeol, findInstitucionCount, findInstitucionList, getUpdateEolRefreshToken, viewInstitucionCount, recordViewInfo);

}

function getInsttucioInit(filterQuery){
	var nivel;
	var gestion;
	var estado;
	
	var data={
			"nombreIE"	:	filterQuery.nombreIE,
			"codooii" 	: 	filterQuery.codooii,
			"codmod" 	: 	filterQuery.codmod,
			"codlocal" 	: 	filterQuery.codlocal,
			"nombreCP" 	: 	filterQuery.nombreCP,
			"niveles" 	: 	filterQuery.niveles,
			"gestiones" : 	filterQuery.gestiones,
			"ubigeo" 	: 	filterQuery.ubigeo,
			"estados" 	: 	filterQuery.estados,
			"start" 	: 	filterQuery.start 
			};
	
	return cipher(JSON.stringify(data));
}

function findInstitucionList(filterQuery, token, callbackViewList) {
	var dataInp=getInsttucioInit(filterQuery);
	
	var dataEncrypt={
			"salt"		:	dataInp.salt,
			"iv" 		: 	dataInp.iv,
			"cipher" 	: 	dataInp.cipher
			};

	var urlRest = PADRON_API_URL + "/institucionListInfoBase";

	$.ajax({
		type: "POST",
		contentType: 'application/json',
		url: urlRest,
		data: JSON.stringify(dataEncrypt),
		beforeSend: function(request) {
			request.setRequestHeader("Authorization", "Bearer "+token);
			request.setRequestHeader("Content-Type", "application/json");
		},
		success: function(data) {
			callbackViewList(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.responseText.indexOf("Access token expired:")!=-1) {
			} else if (XMLHttpRequest.status==403) {
				viewMessageAlert("Servicio no disponible, por favor inténtelo en unos momentos");
			} else { 

			}
		}
	});
}

function findInstitucionCount(filterQuery, token, callbackCount, callbackList, callbackNewToken, callbackViewCount, callbackViewList) {
	var urlRest = PADRON_API_URL + "/institucionCount";

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: urlRest,
		data: JSON.stringify(filterQuery),
		beforeSend: function(request) {
			request.setRequestHeader("Authorization", "Bearer "+token);
			request.setRequestHeader("Content-Type", "application/json");
		},
		success: function(data) {
			if (data.status == 'SUCCESS' && data.responsePayload.dataResult == 'DATA_FOUND') {
				callbackViewCount(data.responsePayload.dataDetail);
				callbackList(filterQuery, token, callbackViewList);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.responseText.indexOf("Access token expired:")!=-1) {
				callbackNewToken(filterQuery, callbackCount, callbackViewCount, callbackList, callbackViewList);
			} else if (XMLHttpRequest.status==401) {
				callbackNewToken(filterQuery, callbackCount, callbackViewCount, callbackList, callbackViewList);
			} else if (XMLHttpRequest.status==403) {
				viewMessageAlert("Servicio no disponible, por favor inténtelo en unos momentos");
			} else { 

			}
		}
	});

}

function getUpdateEolRefreshToken(filterQuery, callbackCount, callbackViewCount, callbackList, callbackViewList) {
	$.ajax({
		url : "/estadistica/ugel/retrieveInoo",
		type: "POST",
	    dataType: "json",
	    contentType: "application/json",
	    mimeType: "application/json",
		success : function(data) {
			if (data.status == "SUCCESS") {
				var newToken = data.newToken;
    			jQuery("#eolinfo").val(newToken);
    			callbackCount(filterQuery, newToken, null, callbackList, null, callbackViewCount, callbackViewList);
			}
			if (data.status == "FAILURE") {
				window.location.href = "/estadistica/sesionexpiro.uee";
			}

		},
	    error:function(data,status,er) {
	    	window.location.href = "/estadistica/sesionexpiro.uee";
	    }
	});
}

function getRefreshTokenEsbas(filterEsbas, callbackPeriodo, callbackViewPeriodo) {
	$.ajax({
		url : "/estadistica/ugel/retrieveInoo",
		type: "POST",
	    dataType: "json",
	    contentType: "application/json",
	    mimeType: "application/json",
		success : function(data) {
			if (data.status == "SUCCESS") {
				console.info(":: getRefreshTokenEsbas : End");
				var newToken = data.newToken;
    			jQuery("#eolinfo").val(newToken);   			
    			callbackPeriodo(filterEsbas, newToken, null, null, callbackViewPeriodo);
			}
			if (data.status == "FAILURE") {
				window.location.href = "/estadistica/sesionexpiro.uee";
			}

		},
	    error:function(data,status,er) {
//	        console.info("getRefreshTokenEsbas error: "+data+" status: "+status+" er:"+er);
	    	window.location.href = "/estadistica/sesionexpiro.uee";
	    }
	});
}

function viewInstitucionCount(cuenta) {
    cantTotalIE=cuenta;
    var max=50;
    var $cuenta=parseInt(cuenta)
    if (isNaN($cuenta))return;

    if (inicioIE>0){
        jQuery("<a/>",{
            html:"Anterior",
            href:"#"
        }).appendTo(".paginador_ie").click(function(){
            inicioIE-=max;
            searchInstitucionEducativa();
            return false;
        });
    }

    if ($cuenta>(inicioIE+max)){
        jQuery("<a/>",{
            html:"Siguiente",
            href:"#"
        }).appendTo(".paginador_ie").click(function(){
            inicioIE+=max;
            searchInstitucionEducativa();
            return false
        })

    }

    jQuery("#cuenta").text("Total: "+cuenta);

}

//***** function buscar IIEE *** start

function viewMessageAlert(message) {
    jQuery('#alert').html(message);
    jQuery('#alert').removeClass("hide");
    jQuery('#alert').addClass("show");
}

//***** read and view info *** start

function recordViewInfo(data) {

	var decifrado = [];
	decifrado=decipher(data.salt, data.iv, data.passphrace, data.responsePayload.cipher);
	var tbody=jQuery("#listadoIIEE tbody");
	   var i = 0;
	   if (data.status == 'SUCCESS' && data.responsePayload.dataResult == 'DATA_FOUND') { 
			$.each(JSON.parse(decifrado), function (index, ie) {
				buildRowInstitucion(tbody, i, ie);
	           i++;
	       });
	   }
	}

function buildRowInstitucion(tbody, i, ie) {
    var tr = jQuery("<tr/>").appendTo(tbody).attr("id", ie.codMod + ":" + ie.anexo + ":" + ie.codnivmod + ":" + ie.codlocal + ":" + ie.codinst).addClass(i % 2 === 0 ? "par" : "impar");
    
    jQuery("<td/>", {
        html: (i + inicioIE + 1)
    }).appendTo(tr);


    if (jQuery.inArray("codMod", campos) >= 0) {
        var codmod = jQuery("<a/>", {
            html: ie.codMod,
            id: "link:" + ie.codMod + ":" + ie.anexo + ":" + ie.codnivmod + ":" + ie.codlocal + ":" + ie.codinst,
            href: "#"
        }).click(function() {
            jQuery("#capaBqIE").hide("slow");
            jQuery("#capaTabIE").show("slow");
            var cods = this.id.split(":");
            jQuery("#tableAct tbody").empty();
            cargarTableroIE(cods[1], cods[2], cods[3], cods[4], cods[5]);
        });

        jQuery("<td/>").appendTo(tr).append(codmod);
    }

    if (jQuery.inArray("anexo", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.anexo
        }).appendTo(tr);
    }

    if (jQuery.inArray("codlocal", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.codlocal
        }).appendTo(tr);
    }
    if (jQuery.inArray("cenEdu", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.cenEdu
        }).appendTo(tr);
    }
    if (jQuery.inArray("nivMod", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.nivMod
        }).appendTo(tr);
    }
    if (jQuery.inArray("dirCen", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.dirCen
        }).appendTo(tr);
    }

    if (jQuery.inArray("telefono", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.telefono
        }).appendTo(tr);
    }

    if (jQuery.inArray("director", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.director
        }).appendTo(tr);
    }

    if (jQuery.inArray("gestion", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.gesDep
        }).appendTo(tr);
    }

    if (jQuery.inArray("distrito", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.distrito
        }).appendTo(tr);
    }

    if (jQuery.inArray("cenPob", campos) >= 0) {
        jQuery("<td/>", {
            html: ie.cenPob
        }).appendTo(tr);
    }
}
//***** read and view info *** end

function cargarTableroIE(codMod, anexo, nivMod, codlocal, codinst) {
	jQuery("#IE_CODMOD").text(codMod);
    jQuery("#IE_NIVMOD").text(nivMod);
    jQuery("#IE_CODLOCAL").text(codlocal);
    
    var anioIni = jQuery("#anioInicio").val();
    var anioFin = jQuery("#anioFinal").val();
    
    var filterEsbas = {
        	"codmod" : codMod,
        	"tipo": "UGEL",
        	"anexo" : "0",
        	"codlocal" : codlocal,
        	"codinst" : codinst,
        	"nivel": nivMod,
        	"anioInicio" : anioIni,
        	"anioFinal" : anioFin
        };
    
    var token = jQuery("#eolinfo").val();

    viewPeriodoEsbas(filterEsbas, token, viewPeriodoEsbas, getRefreshTokenEsbas, recordViewPeriodoInfo);

}

function viewPeriodoEsbas(filterEsbas, token, callbackPeriodo, callbackNewToken, callRecordViewPeriodoInfo) {
	var urlRest = ESBAS_API_URL + "/cedulaCensalList";

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: urlRest,
		data: JSON.stringify(filterEsbas),
		beforeSend: function(request) {
			request.setRequestHeader("Authorization", "Bearer "+token);
			request.setRequestHeader("Content-Type", "application/json");
		},
		success: function(data) {
			callRecordViewPeriodoInfo(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.responseText.indexOf("Access token expired:")!=-1) {
				console.info("El token de acceso expiro!");
				callbackNewToken(filterEsbas, callbackPeriodo, callRecordViewPeriodoInfo);
			} else if (XMLHttpRequest.status==401) {
				console.info("Se intento acceder a la seguridad externamente!");
				callbackNewToken(filterEsbas, callbackPeriodo, callRecordViewPeriodoInfo);
			} else if (XMLHttpRequest.status==403) {
				console.info("problem 403!");
				viewMessageAlert("Servicio no disponible, por favor inténtelo en unos momentos");
			} else { 

			}
		}
	});
}

function recordViewPeriodoInfo(data) {
   var tbody = jQuery("#tableAct tbody");
   var i = 0;
   if (data.status == "SUCCESS" && data.responsePayload.dataResult == "DATA_FOUND") {
       $.each(data.responsePayload.dataDetail, function (index, periodo) {
    	   agregarPeriodo(tbody, i, periodo);
           i++;
       });
   }
}

function agregarPeriodo(tbody, i, periodo) {
   var tr = jQuery("<tr/>").appendTo(tbody);
   jQuery("<td/>",{
            html:periodo.anio
        }).addClass("rowAnio").attr("colspan", campos.length).appendTo(tr);

   jQuery.each(periodo.actividades, function(i, act){
        agregarAct(tbody,i,act,periodo.anio);
    });
}

var estadoAct={1:"No Iniciado",2:"En Proceso",3:"Suspendido",4:"Periodo Extemporáneo",5:"Finalizado"};

function agregarAct(tbody, i, act,anioperiodo) {
	var tr=jQuery("<tr/>").appendTo(tbody);
            
    if (jQuery.inArray(act.actividad.estadoActividad, estadoAct) != undefined) {
        var strEstado=estadoAct[esregHuelga ? act.actividad.estadoActividadhuelga : (esregSigied ? act.actividad.estadoActividadsigied : act.actividad.estadoActividad)];
        
        var estSpan=jQuery("<span/>",{html:"  ("+strEstado+")"}).attr("style", "font-size: 8pt;font-family:Arial;color: #4682b4");
        
        jQuery("<td/>",{
            html:act.actividad.descripcion
        }).attr("height", "50").appendTo(tr).append(estSpan);
    } else {
        jQuery("<td/>",{
            html:act.actividad.descripcion
        }).attr("height", "50").appendTo(tr);
    }

    jQuery("<td/>",{
            html:act.actividad.strFechaInicio+" - "+(esregHuelga ? act.actividad.strFechaTerminohuelga : (esregSigied?act.actividad.strFechaTerminosigied:act.actividad.strFechaTermino))
        }).addClass("tituloPlazoAct").attr("align", "center").appendTo(tr);

    if (act.actividad.urlFormato != undefined && act.actividad.urlFormato!="" && act.actividad.estadoFormato) {
    	var enlacedescarga = urlRSubstring +act.actividad.urlFormato.substring(1).split("?")[0]+ ";" +urlRAdding +"?"+act.actividad.urlFormato.substring(1).split("?")[1]
    	var urlFt=jQuery("<a/>",{html:"Descargar",href:enlacedescarga}).addClass("xls");
        jQuery("<td/>").attr("align","center").appendTo(tr).append(urlFt);
    } else {
        jQuery("<td/>",{html:"-"}).attr("align","center").appendTo(tr);
    }

    var eSpan; //=jQuery("<span/>",{html:"-"})

    if (act.actividad.nombre == "CENSO-ID" && anioperiodo == "2015" ) {
       eSpan = jQuery("<span/>",{html:"-"});
    } else {
       eSpan = jQuery("<span/>",{html:"&nbsp;"}).addClass((act.envio)?"reportado":"pendiente");
    }

    jQuery("<td/>",{
            html:""
        }).appendTo(tr).append(eSpan).attr("align", "center");

    jQuery("<td/>",{
            html:act.strFechaEnvio!=undefined?act.strFechaEnvio:"-"
        }).appendTo(tr).attr("align", "center");

    var urlC = "";
    if (act.actividad.nombre == "CENSO-ID" && anioperiodo == "2015") {
        if (anioperiodo == "2015") {
            urlC = jQuery("<a/>",{html:"Obtener",href: "/estadistica/ce/id/2015.pdf?codMod="+jQuery("#IE_CODMOD").text()+"&nivel="+jQuery("#IE_NIVMOD").text(),target:"_blank"}).addClass("pdf");
            jQuery("<td/>").appendTo(tr).append(urlC).attr("align", "center");
        } else {
            jQuery("<td/>",{
                html:"-"
            }).appendTo(tr).attr("align", "center");
        }
    } else {
        if (act.envio && act.actividad.estadoConstancia) {
           var enlaceconstancia = urlRSubstring +act.actividad.urlConstancia.substring(1).split("?")[0]+ ";" +urlRAdding +"?"+act.actividad.urlConstancia.substring(1).split("?")[1]
           urlC=jQuery("<a/>",{html:"Obtener",href:enlaceconstancia,target:"_blank"}).addClass("pdf");
           jQuery("<td/>").appendTo(tr).append(urlC).attr("align", "center");
        } else {
           jQuery("<td/>",{
                html:"-"
            }).appendTo(tr).attr("align", "center");
        }
    }
    
    if (act.actividad.urFormato != undefined) {
        var urlXC=jQuery("<a/>",{html:"Descargar",href:act.actividad.urFormato,target:"_blank"}).addClass("xls");
        jQuery("<td/>").appendTo(tr).append(urlXC).attr("align", "center");
    } else {
        jQuery("<td/>",{
            html:"-"
        }).appendTo(tr).attr("align", "center");
    }

}

// INICIO - LECTURA DE MENSAJE RECIBIDO

function buscarMsj() {
  var filtro={start:inicio}
  jQuery.get(urlMsj+"?OPC=C&TIPO=UGEL&COD_UGEL="+ugel,filtro, cargarCuenta, "text");
  jQuery.getJSON(urlMsj+"?OPC=M&TIPO=UGEL&COD_UGEL="+ugel,filtro, cargarMsj);
}

function cargarMsj(data) {
  jQuery("#msjlist tbody").empty();
  var tbody=jQuery("#listMensajeria tbody");
    
  if (data.data !=undefined) {
    if (jQuery.isArray(data.data)) {
       jQuery.each(data.data, function(i,msj){
             mostrarMsj(tbody,i,msj)
        });
    } else {
       mostrarMsj(tbody,0,data.data);
    }
  }
}

function cargarCuenta(cuenta) {
    jQuery(".paginador").empty();
    cantTotal=cuenta;
    var max=10;
    var $cuenta=parseInt(cuenta);
    if (isNaN($cuenta)) return;

    if (inicio>0) {
        jQuery("<a/>",{
            innerHTML:"Anterior",
            href:"#"
        }).appendTo(".paginador").click(function() {
            inicio-= max;
            resetFormEnvio();
            buscarMsj();
            return false;
        })
    }

    if ($cuenta>(inicio+max)) {
        jQuery("<a/>",{
            innerHTML:"Siguiente",
            href:"#"
        }).appendTo(".paginador").click(function() {
            inicio+=max;
            resetFormEnvio();
            buscarMsj();
            return false;
        })
    }
}

function mostrarMsj(tbody,i,msj) {
    var tr=jQuery("<tr/>").appendTo(tbody).addClass(msj.leido?"row1":"row0");

    var lkIcon=jQuery("<a/>",{innerHTML:"",id:msj.id,href:"#",
            click:function(){
                    leerMsj(this.id);
                    return false;
                }}).addClass(msj.leido?"leerMsj1":"leerMsj");    
    jQuery("<td/>").appendTo(tr).append(lkIcon);

    if(msj.leido){
    var lkQuit=jQuery("<a/>",{innerHTML:"",id:msj.id,href:"#",
            click:function(){
                    quitMsj(this.id)
                    return false
                }}).addClass("quitMsj");
    jQuery("<td/>").appendTo(tr).append(lkQuit);
    } else {
        jQuery("<td/>").appendTo(tr).append("-");
    }


    var lkUOrigen=jQuery("<a/>",{innerHTML:msj.usuarioOrigen,id:msj.id,href:"#",
            click:function(){
                    leerMsj(this.id);                    
                    return false
                }})
    lkUOrigen.attr("style", "color:#333")
    jQuery("<td/>").appendTo(tr).append(lkUOrigen)


    var msjAsunto=msj.asunto;
    if (msj.asunto.length>17) {
        msjAsunto=msj.asunto.substring(0,15)+"..";
    }

    var lkAsunto=jQuery("<a/>",{
            innerHTML:msjAsunto,
            id:msj.id,
            href:"#",
            click:function(){
                    leerMsj(this.id)
                    return false
                }
            });
    lkAsunto.attr("style", "color:#333");
    jQuery("<td/>").appendTo(tr).append(lkAsunto);
}

function leerMsj(idMsj) {
    
    var parm={
        idMsj:idMsj
    };
    jQuery.getJSON(urlMsj+"?OPC=L&TIPO=UGEL&COD_UGEL="+ugel, parm, function(msj){

    var tDE='<div id="tDE" class="outputLine" > <span class="labelField">Enviado por:</span><span class="valueField">'+msj.data.usuarioOrigen+'</span></div>'
    var tASUNTO='<div id="tASUNTO" class="outputLine" > <span class="labelField">Asunto:</span><span class="valueField">'+msj.data.asunto+'</span></div>'
    var tFECHA='<div id="tFECHA" class="outputLine" > <span class="labelField">Fecha:</span><span class="valueField">'+msj.data.fechaCrea+'</span></div>'
    var lMENSAJE='<div id="lMENSAJE" class="outputLine" > <span class="labelField">Mensaje:</span></div>'
    var tMENSAJE='<div id="tMENSAJE" class="outputLineMsg" ><span class="labelField"></span> <span class="valueField">'+msj.data.mensaje+'</span></div>'

    var popup = new Popup("Mensaje recibido",
	tDE+tASUNTO+tFECHA+lMENSAJE+tMENSAJE,
	"Cerrar",
	buscarMsj(),
	null,
        null);
    popup.show()
    })

}

function quitMsj(idMsj) {
    
    var opcQuit=confirm("Esta seguro que desea eliminar?")
    if(opcQuit){
        var parm={idMsj:idMsj}
        jQuery.getJSON(urlMsj+"?OPC=Q&TIPO=UGEL&COD_UGEL="+ugel, parm, function(msj){
        inicio=0;
        resetFormEnvio();
        buscarMsj();
        })
    }
}
// FINAL  - LECTURA DE MENSAJE RECIBIDO

// INICIO - LECTURA DE MENSAJES ENVIADOS
function buscarMsjEnv() {
	jQuery("#msjlistEnv tbody").empty();
        
    var filtro={start:inicioEnv}
    jQuery.get(urlMsj+"?OPC=C_ENV&TIPO=UGEL&COD_UGEL="+ugel,filtro, cargarCuentaEnv, "text")
    jQuery.getJSON(urlMsj+"?OPC=M_ENV&TIPO=UGEL&COD_UGEL="+ugel,filtro, cargarMsjEnv)
}

function cargarCuentaEnv(cuenta) {
    jQuery(".paginadorEnv").empty();
    cantTotalEnv=cuenta;
    var max=10;
    var $cuenta=parseInt(cuenta);

    if (isNaN($cuenta))return;
    if (inicioEnv>0){
        jQuery("<a/>",{
            innerHTML:"Anterior",
            href:"#"
        }).appendTo(".paginadorEnv").click(function(){
            inicioEnv-=max
            buscarMsjEnv()
            return false
        })
    }

    if ($cuenta>(inicioEnv+max)){
        jQuery("<a/>",{
            innerHTML:"Siguiente",
            href:"#"
        }).appendTo(".paginadorEnv").click(function(){
            inicioEnv+=max
            buscarMsjEnv()
            return false
        })
    }
}

function cargarMsjEnv(data) {
  var tbody=jQuery("#listMensajeriaEnv tbody");
    
  if (data.data !=undefined) {
    if (jQuery.isArray(data.data)) {
    	jQuery.each(data.data, function(i,msj){
                mostrarMsjEnv(tbody,i,msj)
            });
    } else {
       mostrarMsjEnv(tbody,0,data.data);
    }
  }
}

function mostrarMsjEnv(tbody,i,msj) {
	var tr=jQuery("<tr/>").appendTo(tbody).addClass("row0");
    var msjAsunto=msj.asunto;
    if (msj.asunto.length>35) {
        msjAsunto=msj.asunto.substring(0,30)+"....";
    }
    var lkAsunto=jQuery("<a/>",{
            innerHTML:msjAsunto,
            id:msj.id,
            href:"#",
            click:function(){
                    leerMsjEnv(this.id)
                    return false
                }
            })
    lkAsunto.attr("style", "color:#333");
    jQuery("<td/>").appendTo(tr).append(lkAsunto);
}

function leerMsjEnv(idMsj) {
    var parm = {
        idMsj:idMsj
    };
    
    jQuery.getJSON(urlMsj+"?OPC=L_ENV&TIPO=UGEL&COD_UGEL="+ugel, parm, function(msj) {
    
    var tASUNTO='<div id="tASUNTO" class="outputLine" > <span class="labelField">Asunto:</span><span class="valueField">'+msj.data.asunto+'</span></div>'
    
    var tFECHA='<div id="tFECHA" class="outputLine" > <span class="labelField">Fecha:</span><span class="valueField">'+msj.data.fechaCrea+'</span></div>'
    var lMENSAJE='<div id="lMENSAJE" class="outputLine" > <span class="labelField">Mensaje:</span></div>'
    var tMENSAJE='<div id="tMENSAJE" class="outputLineMsg" ><span class="labelField"></span> <span class="valueField">'+msj.data.mensaje+'</span></div>'
    
    var popup = new Popup("Mensaje enviado",
	tASUNTO+tFECHA+lMENSAJE+tMENSAJE,
	"Cerrar",
	null,
	null,
        null);
    popup.show()

    })

}

function mostrarCEOL(codMod) {
     var parm={
        codMod:codMod
    };
    jQuery.getJSON(urlCEol+"?TIPO=UGEL", parm, function(msj){
    var tDATA='<div id="tASUNTO" class="outputLine" > <span class="labelField">La clave EOL es:</span><span class="valueField">'+msj.data+'</span></div>'


    var popup = new Popup("EOL",
	tDATA,
	"OK",
	null,
	null,
        null);
    popup.show()

    })
}

function imprimirconstancia() {
    console.log("Imprimiendo constancia");
}

// FINAL  - LECTURA DE MENSAJES ENVIADOS