<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="pe.gob.minedu.escale.eol.enums.admin.NivelModalidadEnum"%>
<%@page import="pe.gob.minedu.escale.eol.enums.admin.EstadoActividadEnum"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!-- INICIO MODIF INICIO C.E - JCMATAMOROS : 21-03-2011 -->
<!--logic:redirect action="/ce/inicio" /-->
<!--logic:redirect action="/ce/inicio" /-->

<!--h2>Seleccione la institución</h2-->
<h1>Seleccione el tablero de control</h1>
<p> La institución educativa cuenta con un servicio educativo asociado(Programa de articulación). Para acceder al tablero de control de la I.E principal como al tablero del programa de articulación tiene los siguientes enlaces:</p>
RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
<ul>
<li><p>
    <html:link action="/ce/tablero?codmod=${IIEE.codigoModular}&anexo=${IIEE.anexo}&nivel=${IIEE.nivelModalidad.idCodigo}&codlocal=${IIEE.codlocal}&codinst=${IIEE.codinst}&mcenso=${IIEE.mcenso}&sienvio=${IIEE.sienvio}&estado=${IIEE.estado.idCodigo}">
        Tablero de control la Institución Educativa principal
    </html:link>
    </p>
</li>

<li><p>
<html:link action="/ce/tablero?codmod=${IIEE.codigoModular}&anexo=${IIEE.anexo}&nivel=A4&codlocal=${IIEE.codlocal}&codinst=${IIEE.codinst}&mcenso=${IIEE.mcenso}&sienvio=${IIEE.sienvio}&estado=${IIEE.estado.idCodigo}">
    Tablero de control del Programa de Articulación
</html:link>
    </p>
</li>
</ul>

<!-- FIN MODIF INICIO C.E - JCMATAMOROS : 21-03-2011 -->
