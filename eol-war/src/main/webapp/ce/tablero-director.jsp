<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="pe.gob.minedu.escale.eol.enums.admin.NivelModalidadEnum"%>
<%@page import="pe.gob.minedu.escale.eol.enums.admin.EstadoActividadEnum"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.enumerations.TipoActividadEnum"%>
<%@page import="pe.gob.minedu.escale.eol.auth.domain.AuthUsuario"%>
<%@page import="pe.gob.minedu.escale.eol.estadistica.domain.EolIndicadorVariable"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<% java.util.Date hoy = new java.util.Date();
            AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
            pageContext.setAttribute("usuario", usuario.getUsuario());
            pageContext.setAttribute("nombre", usuario.getNombre());
            
            pageContext.setAttribute("hoy", hoy);
            pageContext.setAttribute("ENUM_INICIADO", EstadoActividadEnum.INICIADO.toString());
            pageContext.setAttribute("ENUM_EXTENPORANEO", EstadoActividadEnum.EXTENPORANEO.toString());
            pageContext.setAttribute("ENUM_SININICIAR", EstadoActividadEnum.SIN_INICIAR.toString());

            pageContext.setAttribute("NIV_A5", NivelModalidadEnum.INICIAL_NO_ESCOLARIZADO_A5.getCod().toString());
            pageContext.setAttribute("TIP_ACT_LOCAL", TipoActividadEnum.CENSO_LOCAL.getDescri().toString());
            pageContext.setAttribute("TIP_ACT_ID", TipoActividadEnum.CENSO_ID.getDescri().toString());

%>
<!--
<head>
    <meta name="viewport" content="width=device-width" />
    
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script src="../ce/EOL-HTML/scripts/arcgislink-min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../ce/EOL-HTML/css/redmond/jquery-ui-1.10.4.custom.css" />

</head>
-->
<link type="text/css" rel="stylesheet" href="<c:url value='/ce/EOL-HTML/css/styles.css'/>" />
<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />
<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/popup.css'/>" />
<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />
<fmt:setBundle basename="messages" var="msg" />

<%-- SECCION REUBICADA AL DEFAULT
<script type="text/javascript" src="<c:url value='/recursos/js/popup_lib.js'/>"></script>

<!--<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-1.4.4.min.js'/>"></script>-->
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<!--<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui-1.8.8.custom.min.js'/>"></script>-->
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"></script>

<!--<script type="text/javascript">var ugel='${LOGGED_USER.usuario}'</script>-->

<script type="text/javascript" src="<c:url value='/recursos/js/form_estadistico.js'/>"></script>
--%>


<%--<link type="text/css" href="<c:url value='/recursos/jquery/css/jquery-ui-1.8.8.custom.css'/>" rel="stylesheet" />--%>
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.structure.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.theme.min.css'/>" rel="stylesheet" />

<!--
<script src="../ce/EOL-HTML/scripts/jquery-2.1.0.js"></script>
<script src="../ce/EOL-HTML/js/jquery-ui-1.10.4.custom.js"></script>
<script src="../ce/EOL-HTML/scripts/mapa.js"></script> 
<script type="text/javascript">
    function mensajerecuperacion(){
        alert("recuperacion");
    }

</script> 
-->

<logic:notPresent name="IIEESession">
    <p style="padding: 0px 10px;font-weight: bold;">
        <html:link action="/ce/inicio" >
            «Volver
        </html:link>
    </p>
</logic:notPresent>

<c:set var="IDRESULTADO" value="0" />
<c:set var="IDMATRICULA" value="0" />
<div style=" width: 100%" >
    <input type="hidden" id="valueUGEL" name="valueUGEL" value="${LOGGED_USER.usuario}">
    <div class="panel panel-primary">
        <div class="panel-heading">Tablero de Control del Director de Institución Educativa</div>
        <div class="panel-body">
            <%--
            <c:if test="${param.sienvio != 2}">
                <ul class="list-group">
                  <li class="list-group-item list-group-item-danger" style="color: black;">
                      Estimado Director(a): <br>Su institución educativa forma parte de las Regiones seleccionadas para remitir su información del Censo Educativo a través del nuevo “Sistema de Información de Gestión Educativa” – SIGIED, el mismo que a través de una plataforma Web, conectada a internet, recibe y valida los datos que usted registra para el Censo Educativo 2017. Ingrese al siguiente link: <a href="https://sigied.minedu.gob.pe" target="_blank">https://sigied.minedu.gob.pe</a> y acceda al sistema haciendo uso de su usuario y contraseña. No olvides que si ya estás registrado en algún Sistema del MINEDU que utilice como seguridad de acceso el “Passport”, debes hacer uso de esa clave, caso contrario utiliza tu clave SIAGIE. Los directores de EBA, SNU y ETP serán registrados en este sistema de seguridad hasta el 10 de junio. Para mayor información puedes realizar una consulta en el Link “Formulario” dentro de la Pestaña “Censos” de la Plataforma de Autoayuda: <a href="http://estadisticasoporte.minedu.gob.pe" target="_blank">http://estadisticasoporte.minedu.gob.pe</a>
                  </li>
                </ul>               
            </c:if>
            --%>
            <h6>Ud. está visualizando el tablero de control de la I.E con código modular:${LOGGED_INSTITUCION.codigoModular} anexo:${LOGGED_INSTITUCION.anexo} y nivel: <fmt:message bundle="${msg}" key="nivel.${LOGGED_INSTITUCION.nivelModalidad}"/></h6>
            <html:link action="/ce/tablero">
                <span class="label label-primary" style="font-size: 8pt;">Actualizar tablero</span>
            </html:link>          
            <br>
            
            <div>
           
			<c:if test="${(EstadoPadron eq 2) }">                                                                                    
                   <span style="font-size: 10pt;font-family:Arial;color: #FF0000; ">
                   El código modular se encuentra inactivo, consulte con el especialista estadístico de su respectiva UGEL.
                   </span>
            </c:if>
			
			</div>																		               
            
            <c:if test="${ExistFile == null }">
                <span style="font-size: 8pt;font-family:Arial;color: #FF0000;font-weight: bold;background-color: yellow; display:none;">Aún no se encuentra disponible la cedula electrónica de matrícula.</span>
            </c:if>
            <br>
            <br>
            <table class="table table-striped"  border="0">
                <tr>
                    <td class="tablero">
                        <div id="contenido" >
                            <%int a = 1;%>
                            <div id="tabs" >
                                <ul>
                                    <li id="t1"><a href="#tabs-1">Tablero</a></li>
                                    <li id="t2" <c:if test='${LOGGED_INSTITUCION.nivelModalidad != "A1" && LOGGED_INSTITUCION.nivelModalidad != "A2" && LOGGED_INSTITUCION.nivelModalidad != "A3" && LOGGED_INSTITUCION.nivelModalidad != "B0" && LOGGED_INSTITUCION.nivelModalidad != "F0"}'>style="display: none;"</c:if> ><a href="#tabs-2">Indicadores de Gestión</a></li>
                                    <li id="t3"><a href="#tabs-3">Cifras</a></li>
                                </ul>
                                <div id="tabs-1" style="margin: -5px -15px 0 -15px;">

                                    <table class="table table-striped"  border=0>
                                        <tr>
                                            <td class="tablero">
                                                <div id="cuadros">
                                                    <!--
                                                    <ul>-->
<!--                                                    <li class="pendiente_ley">Pendiente</li>
                                                    <li class="reportado_ley">Reportado</li>-->
                                                    <li class="pendiente_ley"><span class="label label-danger">Pendiente</span></li>
                                                    <li class="reportado_ley"><span class="label label-success">Reportado</span></li>
                                                    <!--</ul>-->
                                                </div>
                                                <div id="contenido">

                                                    <c:forEach items="${mapAct}" var="mp" >
                                                        <h4 style="padding: 0px;margin: 0 0 4px 0;">${mp.key}</h4>
<!--                                                        <strong style="font:150% Arial,Helvetica,Verdana,sans-serif;color:black; font-size: 20px;">${mp.key}</strong>-->
                                                        <div id="tableContainer" style="font-size: 1em" >

                                                            <table cellpadding="1" cellspacing="1" class="tabla"  width="100%" border="1" bordercolor="#4297D7">
                                                                <thead>
                                                                    <tr>
                                                                        <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.actividad"/></th>
                                                                        <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.plazo"/></th>
                                                                        <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.formato"/></th>
                                                                        <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.situacion"/></th>
                                                                        <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.fecha"/></th>
                                                                        <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.constancia"/></th>
                                                                        <%--
                                                                        <c:if test="${mp.key == 2017}">
                                                                        <th>Formato enviado</th>
                                                                        </c:if>
                                                                        --%>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <c:forEach items="${mp.value}" var="cedula">

                                                                        <tr <c:if test="${LOGGED_INSTITUCION.nivelModalidad == NIV_A5 && (cedula.actividad.nombre == TIP_ACT_LOCAL || cedula.actividad.nombre == TIP_ACT_ID)}">style="display: none;"</c:if> >
                                                                            <td width="300" height="30">
<!--                                                                                <span >${cedula.actividad.nombre}</span>
                                                                                <span style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;">
                                                                                    <br/> (<fmt:message bundle="${msg}" key="actividad.estado.${cedula.actividad.estadoActividad}"/>)
                                                                                </span>-->

                                                                                <h6>${cedula.actividad.nombre}<br><div class="label-estado">
																				       
																				        <%--<span><fmt:message bundle="${msg}" key="actividad.estado.${EsregHuelga ? cedula.actividad.estadoActividadhuelga : 
																						(EsregSigied ? cedula.actividad.estadoActividadsigied : cedula.actividad.estadoActividad)}"/>
																						</span></div></h6>
																						--%>
																				       <span><fmt:message bundle="${msg}" key="actividad.estado.${EsregHuelga ? cedula.actividad.estadoActividadhuelga : 
																					   (EsregSigied ? (cedula.actividad.estadoActividadsigied eq 4?5:(cedula.actividad.estadoActividadsigied)) : 
																					   (cedula.actividad.estadoActividad eq 4?5:(cedula.actividad.estadoActividad)))}"/>
																					   </span>
																					   
																					    
																					   
																					   </div>
																				</h6>
                                                                               
                                                                               
                                                                                <c:if test="${(cedula.actividad.nombre == 'CENSO-ID') && (mp.key == 2017) }">                                                                                    
                                                                                    <span style="font-size: 8pt;font-family:Arial;color: #FF0000;font-weight: bold;">Solo para ser llenado por el Director General</span>
                                                                                </c:if>
                                                                            </td>
                                                                            <td width="200" align="center" >
                                                                                <h6>
                                                                                    <mark>
                                                                                        <%--
                                                                                        <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                                                                            <fmt:formatDate pattern="dd MMM yyyy" value="${cedula.actividad.fechaInicio}"/>
                                                                                        </font>
                                                                                        -
                                                                                        --%>
                                                                                        <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                                                                        
	                                                                                        <c:if test="${cedula.actividad.estadoActividad eq 2 }">                                                                                    
			                                                                                    <span>Según DRE/UGEL</span>
			                                                                                </c:if>
			                                                                                <c:if test="${!(cedula.actividad.estadoActividad eq 2) }">                                                                                    
			                                                                                    <span>Según DRE/UGEL</span>
			                                                                                </c:if>
                                                                                        	<%--<fmt:formatDate pattern="dd MMM yyyy" value="${EsregHuelga ? cedula.actividad.fechaTerminohuelga : (EsregSigied ? cedula.actividad.fechaTerminosigied : cedula.actividad.fechaTermino)}"/> --%>
                                                                                        </font>
                                                                                    </mark>
                                                                                </h6>
                                                                            </td>
                                                                            <td width="90" align="center" valign="center">
                                                                               
                                                                               
                                                                               <c:choose>
                                                                                    <c:when test="${(cedula.actividad.estadoActividad==ENUM_INICIADO ||
                                                                                                    cedula.actividad.estadoActividad==ENUM_EXTENPORANEO)
                                                                                                    &&  cedula.actividad.estadoFormato && (LOGGED_INSTITUCION.mcenso=='1')
                                                                                                    && (LOGGED_INSTITUCION.estado=='1') && (LOGGED_INSTITUCION.sienvio=='2')}">

                                                                                            
                                                                                                <span>
                                                                                                <c:if test="${(not empty cedula.actividad.urlFormato) && cedula.actividad.nombre != 'CENSO-RESULTADO-RECUPERACION' && cedula.actividad.nombre != 'IDENTIFICACION-DE-RESIDENCIAS'}">
                                                                                                    <c:url var="url" value="${cedula.actividad.urlFormato}">
                                                                                                        <c:param name="codmod" value="${LOGGED_INSTITUCION.codigoModular}" />
                                                                                                        <c:param name="anexo" value="${LOGGED_INSTITUCION.anexo}" />
                                                                                                        <c:param name="nivel" value="${LOGGED_INSTITUCION.nivelModalidad}" />
                                                                                                        <c:param name="codlocal" value="${LOGGED_INSTITUCION.codigoLocal}" />
                                                                                                        <c:if test="${not empty cedula.nroEnvio}">
                                                                                                            <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                                                        </c:if>
                                                                                                        <c:param name="codinst" value="${LOGGED_INSTITUCION.codigoInstitucion}" />
                                                                                                        <c:param name="nomactiv" value="${cedula.actividad.nombre}" />

                                                                                                    </c:url>
                                                                                                   <!--   <a class="xls" target="_blank" href="${url}" data-toggle="tooltip" title="Descargar formato excel"> Descargar</a> -->

                                                                                                    <c:if test="${(cedula.actividad.nombre == 'CENSO-RESULTADO') && (mp.key == 2019) }">
                                                                                                        <a class="xls" target="_blank" href="${url}" data-toggle="tooltip" title="Descargar formato excel"> Descargar</a>
                                                                                                        <c:set var="IDRESULTADO" value="${cedula.nroEnvio}" />
                                                                                                    </c:if>
                                                                                                    <c:if test="${(cedula.actividad.nombre == 'CENSO-MATRICULA') && (mp.key == 2019) }">
                                                                                                        <c:set var="IDMATRICULA" value="${cedula.nroEnvio}" />
                                                                                                    </c:if>
                                                                                                    <%--
                                                                                                    <label>${IDRESULTADO}</label>
                                                                                                    <c:if test="${(cedula.actividad.nombre == 'CENSO-RESULTADO-RECUPERACION') && (mp.key == 2019) }">
                                                                                                        <label>${IDRESULTADO}</label>
                                                                                                    </c:if>
                                                                                                     --%>

                                                                                                
                                                                                                </c:if>
                                                                                                <c:if test="${(not empty cedula.actividad.urlFormato) && (((cedula.actividad.nombre == 'CENSO-RESULTADO-RECUPERACION') && (IDRESULTADO > 0)) or ((cedula.actividad.nombre == 'IDENTIFICACION-DE-RESIDENCIAS') && (IDMATRICULA > 0)))}">
                                                                                                    <c:url var="url" value="${cedula.actividad.urlFormato}">
                                                                                                        <c:param name="codmod" value="${LOGGED_INSTITUCION.codigoModular}" />
                                                                                                        <c:param name="anexo" value="${LOGGED_INSTITUCION.anexo}" />
                                                                                                        <c:param name="nivel" value="${LOGGED_INSTITUCION.nivelModalidad}" />
                                                                                                        <c:param name="codlocal" value="${LOGGED_INSTITUCION.codigoLocal}" />
                                                                                                        <c:if test="${not empty cedula.nroEnvio}">
                                                                                                            <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                                                        </c:if>
                                                                                                        <c:param name="codinst" value="${LOGGED_INSTITUCION.codigoInstitucion}" />
                                                                                                        <c:param name="nomactiv" value="${cedula.actividad.nombre}" />

                                                                                                    </c:url>
                                                                                                    <a class="xls" target="_blank" href="${url}" data-toggle="tooltip" title="Descargar formato excel"> Descargar</a>

                                                                                                </c:if>

                                                                                                </span>

                                                                                    </c:when>
                                                                                    <c:otherwise>

                                                                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;color: Gray;vertical-align:middle;" >
                                                                                           <!--  <strong class="xls_finish">Descargar </strong>  -->

                                                                                        </span>
                                                                                    </c:otherwise>

                                                                                </c:choose>
                                                                               

                                                                            </td>
                                                                            <td align="center">
                                                                                <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR}">
                                                                                    <c:if test="${!cedula.envio}">
                                                                                        <%-- <c:if test='${cedula.actividad.nombre != "CENSO-ID"}'> --%>
                                                                                        <c:choose>
                                                                                            <c:when test="${((cedula.actividad.nombre == 'CENSO-ID') && (mp.key == 2015)) || ( (cedula.actividad.nombre == 'CENSO-RESULTADO-RECUPERACION') && !(IDRESULTADO > 0) ) || ( (cedula.actividad.nombre == 'IDENTIFICACION-DE-RESIDENCIAS') && !(IDMATRICULA > 0) ) }">
                                                                                            </c:when>
                                                                                            <c:otherwise>
                                                                                                <strong class="pendiente" data-toggle="tooltip" title="Pendiente de envio">&nbsp;</strong>
                                                                                            </c:otherwise>
                                                                                        </c:choose>
                                                                                        <%-- </c:if> --%>
                                                                                    </c:if>
                                                                                    <c:if test="${cedula.envio}">
                                                                                        <strong class="reportado" data-toggle="tooltip" title="Envio reportado">&nbsp;</strong>
                                                                                    </c:if>
                                                                                </c:if>
                                                                            </td>
                                                                            <td align="center">
                                                                                <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR}">
                                                                                    <c:if test="${cedula.fechaEnvio ne null}">
                                                                                        <h6>
                                                                                            <mark>
                                                                                                <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;">
                                                                                                    <fmt:formatDate pattern="dd/MM/yyyy" value="${cedula.fechaEnvio}"/>
                                                                                                </span>
                                                                                            </mark>
                                                                                        </h6>
                                                                                    </c:if>
                                                                                    
                                                                                </c:if>
                                                                            </td>
                                                                            <td width="80">

                                                                                <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR }">

                                                                                    <c:choose>
                                                                                        <c:when test="${cedula.envio}">
                                                                                            <c:if test="${not empty cedula.actividad.urlConstancia and cedula.actividad.estadoConstancia and cedula.nroEnvio > 0}">
                                                                                                <span>
                                                                                                    <c:url var="url" value="${cedula.actividad.urlConstancia}">
                                                                                                        <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                                                        <c:param name="nivel" value="${LOGGED_INSTITUCION.nivelModalidad}" />
                                                                                                    </c:url>
                                                                                                    
                                                                                                    <a class="pdf" href="${url}" target="_blank">
                                                                                                        <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">Obtener </span>
                                                                                                        </a>
                                                                                                    <!--<a class="pdf" href="#" onclick="javascript:imprimirconstancia(); return false;"></a>-->
                                                                                                    <!--<a class="pdf" href="#" onclick="" ></a>-->

                                                                                                </span>
                                                                                            </c:if>
                                                                                        </c:when>
                                                                                        <c:otherwise>
                                                                                            <c:if test='${cedula.actividad.nombre == "CENSO-ID" && mp.key == 2015}'>
                                                                                                <a target="_blank" href="/estadistica/ce/id/2015.pdf?codMod=${param.codmod}&nivel=${LOGGED_INSTITUCION.nivelModalidad}" class="pdf">
                                                                                                    <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">Obtener </span>
                                                                                                </a>
                                                                                            </c:if>
                                                                                        </c:otherwise>
                                                                                        <%--
                                                                                         <c:otherwise>
                                                                                             <c:if test='${cedula.actividad.nombre == "CENSO-ID"}'>
                                                                                                 <a target="_blank" href="/estadistica/ce/id/2015.pdf?codMod=${param.codmod}&nivel=${param.nivel}" class="pdf">
                                                                                                     <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">Obtener </span>
                                                                                                 </a>
                                                                                             </c:if>
                                                                                         </c:otherwise>
                                                                                        --%>
                                                                                    </c:choose>
                                                                                </c:if>
                                                                            </td>
                                                                            <%--<c:if test="${mp.key == 2017}">
                                                                            <td width="90" align="center" valign="center">


                                                                                    <c:if test="${cedula.envio} ">
                                                                                        <span>

                                                                                        <c:url var="url" value="/descarga">
                                                                                            <c:param name="codmod" value="${param.codmod}" />
                                                                                            <c:param name="anexo" value="${param.anexo}" />
                                                                                            <c:param name="nivel" value="${param.nivel}" />
                                                                                            <c:param name="codlocal" value="${param.codlocal}" />
                                                                                            <c:param name="actividad" value="${cedula.actividad.nombre}" />
                                                                                            <c:param name="idActividad" value="${cedula.actividad.id}" />
                                                                                            <c:param name="codinst" value="${param.codinst}" />
                                                                                            <c:param name="anio" value="${mp.key}" />


                                                                                            <c:if test="${not empty cedula.nroEnvio}">
                                                                                                <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                                            </c:if>

                                                                                        </c:url>

                                                                                        <a class="xls" href="${url}"> Descargar </a>
                                                                                    </span>
                                                                                </c:if>
                                                                                <c:if test="${!cedula.envio}">
                                                                                    <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;color: Gray;vertical-align:middle;" >
                                                                                        <strong class="xls_finish">Descargar </strong>

                                                                                    </span>
                                                                                </c:if>





                                                                        </td>
                                                                        </c:if>
                                                                            --%>
                                                                        </tr>

                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <br/>
                                                    </c:forEach>


                                                </div>
                                            <td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="tabs-2" style="margin: 0px 0px 0 0px;" >
                                    <table  cellpadding="1" cellspacing="1" class="tabla"  width="100%">
                                        <thead>
                                            <tr>
                                                <th>INDICADOR</th>
                                                <th>DESCRIPCION</th>
                                            </tr>
                                            <c:forEach items="${mapListaIndicador}" var="mpLI">
                                                <tr>
                                                    <td bgcolor="#FFFFFF"><span  style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;" >
		                                                    <c:url var="url" value="/ce/resultado/indicador2014.pdf">
																<c:param name="codMod" value="${LOGGED_INSTITUCION.codigoModular}" />
																<c:param name="variable" value="${mpLI.idVariable}" />
																<c:param name="nivel" value="${LOGGED_INSTITUCION.nivelModalidad}" />
															</c:url>
															<%--
															<a target="_blank" href="/estadistica/ce/resultado/indicador2014.pdf?codMod=${param.codmod}&variable=${mpLI.idVariable}&nivel=${LOGGED_INSTITUCION.nivelModalidad}" class="pdf">
                                                                <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">${mpLI.nombVar} </span>
                                                            </a>
															--%>
                                                            <a target="_blank" href="${url}" class="pdf">
                                                                <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">${mpLI.nombVar} </span>
                                                            </a>
                                                        </span>
                                                    </td>
                                                    <td bgcolor="#FFFFFF">
                                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;color: Gray;vertical-align:middle;" >
                                                            ${mpLI.descripcion}
                                                        </span>
                                                    </td>
                                                </tr>
                                            </c:forEach>

                                        </thead>

                                    </table>
                                </div>
                                <div id="tabs-3" style="margin: 0px 0px 0 0px;" >
                                    <table  cellpadding="1" cellspacing="1" class="tabla"  width="100%">
                                        <thead>
                                            <tr>
                                                <th>VARIABLE</th>
                                                <th>DESCRIPCION</th>
                                            </tr>
                                            <c:forEach items="${mapListaCifras}" var="mpLIC">
                                                <tr>
                                                    <td bgcolor="#FFFFFF"><span  style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;" >
                                                    		<c:url var="url" value="/ce/resultado/cifras2014.pdf">
																<c:param name="codMod" value="${LOGGED_INSTITUCION.codigoModular}" />
																<c:param name="variable" value="${mpLIC.idVariable}" />
																<c:param name="nivel" value="${LOGGED_INSTITUCION.nivelModalidad}" />
																<c:param name="anexo" value="${LOGGED_INSTITUCION.anexo}" />
															</c:url>
                                                            <a target="_blank" href="${url}" class="pdf">
                                                                <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">${mpLIC.nombVar} </span>
                                                            </a>
                                                        </span>
                                                    </td>
                                                    <td bgcolor="#FFFFFF">
                                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;color: Gray;vertical-align:middle;" >
                                                            ${mpLIC.descripcion}
                                                        </span>
                                                    </td>
                                                </tr>
                                            </c:forEach>

                                        </thead>

                                    </table>
                                </div>


                            </div>
                        </div>
            </table>

        </div>
    </div>
    <div class="clear"></div>
    
</div>

