//variables para el mapa y sus servicios...
var nomdominio = 'http://sigmed.minedu.gob.pe/';

var layer1 = nomdominio + 'medgis/rest/services/carto_base/cp/MapServer';
var layer2 = nomdominio + 'medgis/rest/services/carto_base/lim_pol/MapServer';
var layer3 = nomdominio + 'medgis/rest/services/carto_base/lim_ugel/MapServer';
var layer5 = nomdominio + 'medgis/rest/services/carto_base/ie/MapServer';

var initialLat = -9.318990;
var initialLon = -75.234375;
var initialLevelZoom = 5;

var dynamapIE;
var dynamapCP;
var dynamapLimitesAdm;
var dynamapUgel;

var setmapIE = false;
var setmapCP = false;
var setLimitesAdm = false;
var setLimitesUgel = false;

var tocIE = '';
var tocCP = '';
var tocLimitesAdm = '';
var tocLimitesUgel = '';

var map;

var infowindowCP;
var infowindowIE;

var markerpuntosCP = [];
var markerpuntosIE = [];

var layersDefinition = {
    'layers':
    [
    {
        'type': 'Dynamic',
        'service': layer1,
        'visible': true,
        'caption': 'Centros Poblados',
        'options': {
            opacity: 1
        }
    },
    {
        'type': 'Dynamic',
        'service': layer5,
        'visible': true,
        'caption': 'IIEE',
        'options': {
            opacity: 1
        }
    }
    ,
    {
        'type': 'Dynamic',
        'service': layer2,
        'visible': true,
        'caption': 'L\u00ED­mites Políticos',
        'options': {
            opacity: 1
        }
    }
    ,
    {
        'type': 'Dynamic',
        'service': layer3,
        'visible': false,
        'caption': 'L\u00ED­mites UGEL',
        'options': {
            opacity: 1
        }
    }
    
    ]
};
//variables para los servicios web del EOL
var _url_mid_service = "servicios/rest/service/restsig.svc/";
var _ws_listar_escuelas_x_local = nomdominio + _url_mid_service + "listarescuelasxlocal"; //Sin parámetros...
var _ws_listar_eol_ubic_local_tipo_obs = nomdominio + _url_mid_service + "listareolubiclocaltipoobs"; //Sin parámetros
var _ws_consultar_ubic_local = nomdominio + _url_mid_service + "consultarubiclocal"; //Datos por defecto...: Parámetros(codmod, anexo)
var _ws_consultar_eol_ubic_local = nomdominio + _url_mid_service + "consultareolubiclocal"; //Datos grabados, para modificación...: Parámetros(codmod, anexo)
var _ws_grabar_eol_ubic_local = nomdominio + _url_mid_service + "grabareolubiclocal"; //Parámetros(codmod, anexo, nivmod, codlocal, conforme, latitud, longitud, zoom, tipoobs, permitireditar)
//Variables para el EOL.
var marker_new_ubic = null;
var marker_old_ubic = null;
var ubiclocal = null;
var ubiceollocal = null;
//
var lati;
var longi;
var zoomi;

var xbound = new google.maps.LatLngBounds();
var cargopointrojo=0;
var cargopointverde=0;

function buildMap() {
    var myOptions = {
        zoom: initialLevelZoom,
        center: new google.maps.LatLng(initialLat, initialLon),
        mapTypeId: google.maps.MapTypeId.HYBRID,
        overviewMapControl: false,
        overviewMapControlOptions: {
            opened: false
        },
        scaleControl: false,
        zoomControl: true,
        minZoom: 5,
        maxZoom: 19,
        panControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_TOP
        }
    };
    map = new google.maps.Map(document.getElementById('divcontent'), myOptions);

    var homeControlDiv = document.createElement('div');
    var homeControl = new HomeControl(homeControlDiv, map);

    homeControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(homeControlDiv);
}

function getEscuelas_x_local(codmod, anexo) {
    var strparams = "codmod=" + codmod + "&anexo=" + anexo;
    $.ajax({
        type: "GET",
        url: _ws_listar_escuelas_x_local + "?" + strparams,
        dataType: "json",
        async: false,
        success: function (data) {
            var dat = JSON.parse(data);
            $("#t_ls_escuelas tbody tr").remove();
            $(dat.Rows).each(function (k, v) {
                $("#t_ls_escuelas tbody").append("<tr><td>" + v.COD_MOD + "</td><td>" + v.ANEXO + "</td><td>" + v.CODLOCAL + "</td><td>" + v.CEN_EDU + "</td><td>" + v.NIV_MOD + "</td></tr>");
            });
        }
    });
}
function getUbicLocal(codmod, anexo) {
    var strparams = "codmod=" + codmod + "&anexo=" + anexo;
    $.ajax({
        type: "GET",
        url: _ws_consultar_ubic_local + "?" + strparams,
        dataType: "json",
        async: false,
        success: function (data) {
            ubiclocal = JSON.parse(data).Rows[0];
        }
    });
}
function getEOLUbicLocal(codmod, anexo) {
    var strparams = "codmod=" + codmod + "&anexo=" + anexo;
    $.ajax({
        type: "GET",
        url: _ws_consultar_eol_ubic_local + "?" + strparams,
        dataType: "json",
        async: false,
        success: function (data) {
            ubiceollocal = JSON.parse(data).Rows[0];
        }
    });
}
function getEOLUbicLocalTipoObs() {
    $.ajax({
        type: "GET",
        url: _ws_listar_eol_ubic_local_tipo_obs,
        dataType: "json",
        async: false,
        success: function (data) {
            var dat = JSON.parse(data);
            $("#cont_tipoobs li").remove();
            $(dat.Rows).each(function (k, v) {
                //$("#cont_tipoobs ul").append("<li><input type='checkbox' class='chk_tipoobs' value='" + v.IDOBS + "'>" + v.NOMBREOBS + "</li>");
                $("#cont_tipoobs").append("<li><input type='checkbox' class='chk_tipoobs' value='" + v.IDOBS + "'>" + v.NOMBREOBS + "</li>");
            });
        }
    });
}
function grabarEOLUbicLocal(codmod, anexo, nivmod, codlocal, conforme, latitud, longitud, zoom, tipoobs, permitireditar) {
    var strparams = "codmod=" + codmod + "&anexo=" + anexo + "&nivmod=" + nivmod + "&codlocal=" + codlocal + "&conforme=" + conforme + "&latitud=" + latitud + "&longitud=" + longitud + "&zoom=" + zoom + "&tipoobs=" + tipoobs + "&permitireditar=" + permitireditar;
    $.ajax({
        type: "GET",
        url: _ws_grabar_eol_ubic_local + "?" + strparams,
        dataType: "json",
        async: false,
        success: function (data) {
            var dat = JSON.parse(data).Rows[0];
            if (dat.RESULT == '1') {
                alert('Datos grabados satisfactoriamente.');
            }
            if (dat.RESULT == '-1') {
                alert('Ha ocurrido un error al grabar los datos, consulte al administrador del sistema.');
            }
        }
    });
}

function createMarker(ind, lat, lng) {
    var myLatLng = new google.maps.LatLng(lat, lng);
	
    switch (ind) {
        case 0:
		
            xbound.extend(myLatLng);
            cargopointrojo=1;
		
            marker_old_ubic = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map,
                icon: '../ce/EOL-HTML/img/red-dot.png'
            });
            break;
        case 1:
		
            xbound.extend(myLatLng);
            cargopointverde=1;
/*
            if (ubiclocal.FTECOORDENADA==1)
            {
                lng	 = lng + 0.002;
            }
*/
            marker_new_ubic = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map,
                icon: '../ce/EOL-HTML/img/green-dot.png'
            });
            break;
    }
}

/*
function setDraggable(marker, ind)
{
    marker.setDraggable(ind);
}
        */
       
function setDraggable(marker, ind)
{
    marker.setDraggable(ind);
    google.maps.event.addListener(marker, 'dragend', function() { 
        //alert('marker dragged'); 
        if (ubiclocal.FTECOORDENADA==1)
        {
            $("#obs_dyn_map").text("OJO: El marcador rojo indica la localización del poblado donde funciona el local escolar. Ubique el local escolar en el mapa.");
        }
    } );
}



function deleteMarker(marker) {
    marker.setMap(null);
}



function getnombrenivel(nivmod)
{	
    var valor = nivmod.substring(0,1);
	
    switch(valor)
    {
        case "A":
            getnombrenivel = "Inicial";
            break;
        case "B":
            getnombrenivel = "Primaria";
            break;
        case "F":
            getnombrenivel = "Secundaria";
            break;
        case "D":
            getnombrenivel = "Básica Alternativa";
            break;
        case "K":
            getnombrenivel = "Superior Pedagógica";
            break;
        case "T":
            getnombrenivel = "Superior Tecnológica";
            break;   
        case "M":
            getnombrenivel = "Formación Artística (ESFA)";
            break;   
        case "E":
            getnombrenivel = "Básica Especial";
            break;   
        case "L":
            getnombrenivel = "Técnico Productiva";
            break;   
    }
    return getnombrenivel;
}



$(document).ready(function () {
	
    $("#tabs").tabs({
        active: 0
    });
	
    buildMap();
		
    //$("#btnbuscar").click(function () {
    //Cargar lista de tipos de observacion...
    getEOLUbicLocalTipoObs();
    //Fin
    //Cargar lista de escuelas con mismo local
    //getEscuelas_x_local($("#txtcodmod").val(), $("#txtanexo").val());
    //Fin
    //Cargar ubic local
    getUbicLocal($("#txtcodmod").val(), $("#txtanexo").val());
       
    
    switch (ubiclocal.FTECOORDENADA) {
        case 0:
            $("#obs_dyn_map").text("OJO: No se encontró ubicación del local escolar.");
            lati = ubiclocal.CODGEO_LATITUDCENTROIDE;
            longi = ubiclocal.CODGEO_LONGITUDCENTROIDE;
            zoomi = parseInt(ubiclocal.CODGEO_ZOOMCENTROIDE);
               
            map.setCenter(new google.maps.LatLng(ubiclocal.CODGEO_LATITUDCENTROIDE, ubiclocal.CODGEO_LONGITUDCENTROIDE));
            map.setZoom(parseInt(ubiclocal.CODGEO_ZOOMCENTROIDE));
				
				
            break;
        case 1:
            //$("#obs_dyn_map").text("OJO: El marcador rojo indica la localización del poblado donde funciona el local escolar. Ubique el local escolar.");
            $("#obs_dyn_map").text("OJO: El marcador rojo (está debajo del marcador verde) indica la localización del poblado donde funciona el local escolar. Ubique el local escolar en el mapa.");
            lati = ubiclocal.LATITUD;
            longi = ubiclocal.LONGITUD;
            zoomi = parseInt(ubiclocal.ZOOM);
				 
            map.setCenter(new google.maps.LatLng(ubiclocal.LATITUD, ubiclocal.LONGITUD));
            map.setZoom(parseInt(ubiclocal.ZOOM));
            createMarker(0, ubiclocal.LATITUD, ubiclocal.LONGITUD);
            break;
        case 2:
            $("#obs_dyn_map").text("OJO: El marcador rojo indica la localización del local escolar donde funciona la institución educativa");
            lati = ubiclocal.LATITUD;
            longi = ubiclocal.LONGITUD;
            zoomi = parseInt(ubiclocal.ZOOM);
				 
            map.setCenter(new google.maps.LatLng(ubiclocal.LATITUD, ubiclocal.LONGITUD));
            map.setZoom(parseInt(ubiclocal.ZOOM));
            createMarker(0, ubiclocal.LATITUD, ubiclocal.LONGITUD);
            break;
    }
    
    
    //Fin
    //Cargar ubic eol local
    getEOLUbicLocal($("#txtcodmod").val(), $("#txtanexo").val());
    if (ubiceollocal.TIPO_OBSERVACION != '') {
        var tiposobs = ubiceollocal.TIPO_OBSERVACION.split(',');
        $(tiposobs).each(function (k, v) {
            $(".chk_tipoobs[value='" + v + "']").prop("checked", true);
        });
    }
    if ((ubiceollocal.COD_MOD != '') && (ubiceollocal.ANEXO != '')) {
        if (ubiceollocal.CONFORME > 1) {
            $("#dconfirm_ubic").addClass("invisible");
            if (ubiceollocal.CONFORME == '3') {
                if ((ubiceollocal.COD_MOD_EDITO == '') && (ubiceollocal.ANEXO_EDITO == '')) {
                    $("#dmap_instruction ul").append(
                        '<li>Haga click-izquierdo en el mapa si desea ubicar el punto verde <img src="../ce/EOL-HTML/img/green-dot.png" /> y click-derecho si desea removerlo</li>'
                        );
                    //Logica del mapa.
                    google.maps.event.addListener(map, 'click', function (e) {
                        if (marker_new_ubic != null) {
                            marker_new_ubic.setPosition(e.latLng);
                        } else {
                            createMarker(1, e.latLng.lat(), e.latLng.lng());
                            setDraggable(marker_new_ubic, true);
                        }
                    });
                    google.maps.event.addListener(map, 'rightclick', function (e) {
                        if (marker_new_ubic != null) {
                            deleteMarker(marker_new_ubic);
                            marker_new_ubic = null;
                        }
                    });
                }
            }
        } else {
            switch (ubiceollocal.CONFORME) {
                case '0':
                    $("#r_u_correct_no").prop("checked", true);
                    break;
                case '1':
                    $("#r_u_correct_si").prop("checked", true);
                    break;
                case '':
                    //No hay, se obligará a grabar una opinión en caso de que los option aparescan...
                    break;
            }
        }
        if ((ubiceollocal.COD_MOD_EDITO != '') && (ubiceollocal.ANEXO_EDITO != '')) {
            lati = ubiceollocal.LATITUD_EDITO;
            longi = ubiceollocal.LONGITUD_EDITO;
            zoomi = parseInt(ubiceollocal.ZOOM_EDITO);
			
            createMarker(1, ubiceollocal.LATITUD_EDITO, ubiceollocal.LONGITUD_EDITO);
            map.setCenter(new google.maps.LatLng(ubiceollocal.LATITUD_EDITO, ubiceollocal.LONGITUD_EDITO));
            map.setZoom(parseInt(ubiceollocal.ZOOM_EDITO));
               
  
            /*
			   $("#obs_dyn_map2").show();
			   $("#obs_dyn_map2").text("OJO: La ubicación ha sido registrada por el director cuyo COD. MODULAR es: "
                    + ubiceollocal.COD_MOD_EDITO + ", el nivel es: "
                    + ubiceollocal.NIV_MOD_EDITO + " en la fecha: "
                    + ubiceollocal.FECHA_REGISTRO_EDITO + ". No puede modificar la ubicación");
				$("#obs_dyn_map2").css("font-weight","bold");
				$("#obs_dyn_map2").css("color","red");
                                
*/            

            $("#msgyaubicado").hide();
            $("#obs_dyn_map2").show();
            $("#obs_dyn_map2").text("OJO: La localización ha sido registrada por el director de la I.E. cuyo COD. MODULAR es: "
                + ubiceollocal.COD_MOD_EDITO + ", nivel "
                + getnombrenivel(ubiceollocal.NIV_MOD_EDITO) + ", en la fecha: "
                + ubiceollocal.FECHA_REGISTRO_EDITO + ". Por lo cual no es posible modificar la localización por este medio y puede retornar al tablero. Si no está de acuerdo o tiene alguna observación, comuníquela mediante el correo censoescolar@minedu.gob.pe");
            $("#obs_dyn_map2").css("font-weight","bold");
            $("#obs_dyn_map2").css("color","red");
                                
                                
        } else {
            if ((ubiceollocal.LATITUD != '') && (ubiceollocal.LONGITUD != '')) {
                lati = ubiceollocal.LATITUD;
                longi = ubiceollocal.LONGITUD;
                zoomi = parseInt(ubiceollocal.ZOOM);
				
                createMarker(1, ubiceollocal.LATITUD, ubiceollocal.LONGITUD);
                map.setCenter(new google.maps.LatLng(ubiceollocal.LATITUD, ubiceollocal.LONGITUD));
                map.setZoom(parseInt(ubiceollocal.ZOOM));
                setDraggable(marker_new_ubic, true);
            }
        }
    } else {
        if ((ubiceollocal.COD_MOD_EDITO != '') && (ubiceollocal.ANEXO_EDITO != '')) {
            lati = ubiceollocal.LATITUD_EDITO;
            longi = ubiceollocal.LONGITUD_EDITO;
            zoomi = parseInt(ubiceollocal.ZOOM_EDITO);
			
            createMarker(1, ubiceollocal.LATITUD_EDITO, ubiceollocal.LONGITUD_EDITO);
            map.setCenter(new google.maps.LatLng(ubiceollocal.LATITUD_EDITO, ubiceollocal.LONGITUD_EDITO));
            map.setZoom(parseInt(ubiceollocal.ZOOM_EDITO));
                
            /*
                $("#obs_dyn_map2").text("OJO: La ubicación ha sido registrada por el director cuyo COD_MOD es: "
                    + ubiceollocal.COD_MOD_EDITO + ", el nivel es: "
                    + ubiceollocal.NIV_MOD_EDITO + " en la fecha: "
                    + ubiceollocal.FECHA_REGISTRO_EDITO + ". No puede modificar la ubicación.");
				$("#obs_dyn_map2").css("font-weight","bold");
				$("#obs_dyn_map2").css("color","red");
*/

            $("#msgyaubicado").hide();
            $("#obs_dyn_map2").show();
            $("#obs_dyn_map2").text("OJO: La localización ha sido registrada por el director de la I.E. cuyo COD. MODULAR es: "
                + ubiceollocal.COD_MOD_EDITO + ", nivel "
                + getnombrenivel(ubiceollocal.NIV_MOD_EDITO) + ", en la fecha: "
                + ubiceollocal.FECHA_REGISTRO_EDITO + ". Por lo cual no es posible modificar la localización por este medio y puede retornar al tablero. Si no está de acuerdo o tiene alguna observación, comuníquela mediante el correo censoescolar@minedu.gob.pe");
            $("#obs_dyn_map2").css("font-weight","bold");
            $("#obs_dyn_map2").css("color","red");
                                

                                
            $("#dconfirm_ubic").addClass("invisible");
        }
    }
    //Enumerar los pasos
    var i = 1;
    $(".inst").each(function (k, v) {
        if (!$(v).hasClass('invisible')) {
            $(v).find(".npaso").text("Paso " + i + ":");
            i++;
        }
    });
    
    //}); //fin de buscar 
	
    $("#r_u_correct_si").click(function () {
        //Si hay marcador verde... lo quitamos...
        if (marker_new_ubic != null) {
            deleteMarker(marker_new_ubic);
            marker_new_ubic = null;
        }
    });
    $("#r_u_correct_no").click(function () {
        //Si no hay data...
        if (ubiceollocal.COD_MOD_EDITO == '') {
            //Creamos el marcador... la posición se decidirá según la fuente de la coordenada...
            if (marker_new_ubic == null) {
                createMarker(1, map.getCenter().lat(), map.getCenter().lng());
            }
            //Ponemos el draggable.
            setDraggable(marker_new_ubic, true);
        } else {
            //Si hay data, preguntamos si está permitido editar la ubicación al usuario (puede que no sea el mismo que grabó anteriormente.)
            if (ubiceollocal.PERMITIR_EDITAR == '1') {
                setDraggable(marker_new_ubic, true);
            }
        }
    });
	

    $("#tabs, #tabs-1").tabs({ 
        activate: function(event, ui) {
            //alert(ui.newTab[0].id);
            //console.log(ui.newTab);
            if (ui.newTab[0].id=="t2")
            {
                $("#dialog").dialog({
                    modal: true,
                    buttons: {
                        Ok: function(){
                            google.maps.event.trigger(map, 'resize');
							
                            map.setCenter(new google.maps.LatLng(lati, longi));
                            map.setZoom(parseInt(zoomi));
							
                            if ((ubiclocal.FTECOORDENADA!=0) && (cargopointrojo = 1) && (cargopointverde==1))
                            {
                                map.fitBounds(xbound);
                            }
																		
                            $(this).dialog("close");
							
                            if (ubiclocal.FTECOORDENADA==0)
                            {
                                google.maps.event.trigger(map, 'resize');
                                //alert("No contamos con coordenadas geográficas de su local escolar. En el mapa de la parte inferior de la ventana aparecerá enfocado en el distrito donde se encuentra declarada la escuela, a partir de esa posición aproxímese lo más posible a la localización del local y proceda con la ubicación.");
                                alert("El local escolar no cuenta con coordenadas geográficas identificadas. Por lo cual, el mapa de la parte inferior de la ventana se enfocará en el distrito donde se encuentra declarada la institución educativa. A partir de esa posición aproxímese lo más posible a la localización del local y proceda con la ubicación utilizando el marcador verde.");

                                $("#msg00").hide();
                                $("#msg01").hide();
                                $("#msg02").hide();
                                $("#msg03").hide();
                                $("#bold01").hide();
                                $("#bold02").hide();
                                $("#r_u_correct_no").click();	
							  
                                $("#r_u_correct_si").hide();
                                $("#r_u_correct_no").hide();
                            }
                            else
                            {
                                if (ubiclocal.FTECOORDENADA==1)
                                {
                                    $("#msg00").hide();
                                    $("#msg01").hide();
                                    $("#msg02").hide();
                                    $("#msg03").hide();
                                    $("#bold01").hide();
                                    $("#bold02").hide();
                                    $("#r_u_correct_no").click();	
									  
                                    $("#r_u_correct_si").hide();
                                    $("#r_u_correct_no").hide();
                                }
                            }
							
                        }
                    }
                });
            }
        }
    });

    /*        
     $("#tabs, #tabs-1").tabs({ 
	   activate: function(event, ui) {
			//alert(ui.newTab[0].id);
			//console.log(ui.newTab);
			if (ui.newTab[0].id=="t2")
			{
				$("#dialog").dialog({
					modal: true,
					buttons: {
						Ok: function(){
							google.maps.event.trigger(map, 'resize');
							
							map.setCenter(new google.maps.LatLng(lati, longi));
							map.setZoom(parseInt(zoomi));
							
							if ((ubiclocal.FTECOORDENADA!=0) && (cargopointrojo = 1) && (cargopointverde==1))
							{
								map.fitBounds(xbound);
							}
																		
							$(this).dialog("close");
						}
					}
				});
			}
	  }
	});
        */
        
		
    $("#btngrabar").click(function () {
        //Ubicación...
        var codmod = '', anexo = '', nivmod = '', codlocal = '', conforme = '', latitud = '', longitud = '', zoom = '', tipoobs = '', permitireditar = '';
        codmod = ubiclocal.COD_MOD;
        anexo = ubiclocal.ANEXO;
        nivmod = ubiclocal.NIV_MOD;
        codlocal = ubiclocal.CODLOCAL;
        //el parametro 'conforme'
        if (ubiclocal.FTECOORDENADA == '0') {
            conforme = '3';
        } else if ((ubiceollocal.COD_MOD_EDITO != '') && (ubiceollocal.ANEXO_EDITO != '')) {
            conforme = '2';
        } else {
            var ar = $("#r_u_correct_si").prop("checked") || $("#r_u_correct_no").prop("checked");
            if (!ar) {
                alert("Confirme si está de acuerdo con la ubicación presentada en el mapa.");
                return;
            }
            if ($("#r_u_correct_si").prop("checked")) {
                conforme = '1';
            } else {
                conforme = '0';
            }
        }
        //
        if (ubiceollocal.PERMITIR_EDITAR == 1) {
            if (marker_new_ubic != null) {
                latitud = marker_new_ubic.getPosition().lat();
                longitud = marker_new_ubic.getPosition().lng();
                zoom = map.getZoom();
								
                lati = latitud;
                longi = longitud;
                zoomi = zoom;
            } else if (marker_old_ubic != null) {
                if ($("#r_u_correct_si").prop("checked")) {
                    latitud = marker_old_ubic.getPosition().lat();
                    longitud = marker_old_ubic.getPosition().lng();
                    zoom = map.getZoom();
					
                    lati = latitud;
                    longi = longitud;
                    zoomi = zoom;
                }
            }
        }
        //Observaciones
        $(".chk_tipoobs").each(function (k, v) {
            if ($(v).prop("checked")) {
                tipoobs += $(v).val() + ',';
            }
        });
        if (tipoobs != '') {
            tipoobs = tipoobs.substring(0, tipoobs.length - 1);
        } else {
            alert("Seleccione por lo menos una observación.");
            return;
        }
        permitireditar = ubiceollocal.PERMITIR_EDITAR;
        grabarEOLUbicLocal(codmod, anexo, nivmod, codlocal, conforme, latitud, longitud, zoom, tipoobs, permitireditar);
    });
});

function showLayerbox() {
    if (window.timer) clearTimeout(timer);
    $("#toc").fadeIn();
}

function setClose() {
    timer = window.setTimeout(function () {
        $("#toc").fadeOut();
    }, 400);
}

function HomeControl(controlDiv, map) {

    controlDiv.style.padding = '5px';
 
    // CSS para el control contenedor 
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '1px';
    controlUI.style.borderColor = 'gray';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';

    controlDiv.appendChild(controlUI);

    // CSS para el control interior
    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';

    controlText.style.paddingTop = '1px';
    controlText.style.paddingBottom = '2px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.style.width = '103px';
    controlText.innerHTML = '<b>Servicios MED</b>';

    controlUI.appendChild(controlText);

    // CSS para el control Table of Content
    var controlTOC = document.getElementById("toc");
    controlTOC.style.fontFamily = 'Arial,sans-serif';
    controlTOC.style.backgroundColor = 'white';
    controlTOC.style.borderStyle = 'solid';
    controlTOC.style.borderWidth = '1px';
    controlTOC.style.borderColor = 'gray';
    controlTOC.style.fontSize = '10px';
    controlTOC.style.paddingTop = '0px';
    controlTOC.style.paddingBottom = '2px';
    controlTOC.style.paddingLeft = '5px';
    controlTOC.style.paddingRight = '5px';

    toc(controlTOC);

    controlDiv.appendChild(controlTOC);

    controlDiv.onmouseover = showLayerbox;
    controlDiv.onmouseout = setClose;

}

function setVisCP() {

    if (setmapCP == false) {
        dynamapCP.setMap(map);
        setmapCP = true;
    }

    var service = dynamapCP.getMapService();

    var el = document.getElementById('layerCP' + service.layers[2].id);

    switch (map.getZoom())
    {
        case 5:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = (el.checked === true);
            break;
        case 6:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = (el.checked === true);
            service.layers[37].visible = false;
            break;
        case 7:
            service.layers[1].visible = false;
            ;
            service.layers[2].visible = false;
            ;
            service.layers[4].visible = false;
            ;
            service.layers[5].visible = false;
            ;
            service.layers[7].visible = false;
            ;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = (el.checked === true);
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 8:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = (el.checked === true);
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 9:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = (el.checked === true);
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 10:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = (el.checked === true);
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 11:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = (el.checked === true);
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 12:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = (el.checked === true);
            service.layers[23].visible = (el.checked === true);
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 13:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = (el.checked === true);
            service.layers[20].visible = (el.checked === true);
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 14:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = (el.checked === true);
            service.layers[17].visible = (el.checked === true);
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 15:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = (el.checked === true);
            service.layers[14].visible = (el.checked === true);
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 16:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = (el.checked === true);
            service.layers[11].visible = (el.checked === true);
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 17:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = (el.checked === true);
            service.layers[8].visible = (el.checked === true);
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 18:
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[4].visible = (el.checked === true);
            service.layers[5].visible = (el.checked === true);
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
        case 19:
            service.layers[1].visible = (el.checked === true);
            service.layers[2].visible = (el.checked === true);
            service.layers[4].visible = false;
            service.layers[5].visible = false;
            service.layers[7].visible = false;
            service.layers[8].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[16].visible = false;
            service.layers[17].visible = false;
            service.layers[19].visible = false;
            service.layers[20].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[25].visible = false;
            service.layers[27].visible = false;
            service.layers[29].visible = false;
            service.layers[31].visible = false;
            service.layers[33].visible = false;
            service.layers[35].visible = false;
            service.layers[37].visible = false;
            break;
    }

    dynamapCP.refresh();
}

function setVisIE() {

    if (setmapIE == false) {
        dynamapIE.setMap(map);
        setmapIE = true;
    }

    var service = dynamapIE.getMapService();

    var el = document.getElementById('layerIE' + service.layers[1].id);

    switch (map.getZoom()) {
        case 15:
            service.layers[9].visible = (el.checked === true);
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 16:
            service.layers[9].visible = false;
            service.layers[7].visible = (el.checked === true);
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 17:
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = (el.checked === true);
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 18:
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = (el.checked === true);
            service.layers[1].visible = false;
            break;
        case 19:
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = (el.checked === true);
            break;
    }
        
    dynamapIE.refresh();
}



function setVisLimitesAdm() {

    if (setLimitesAdm == false) {
        dynamapLimitesAdm.setMap(map);
        setLimitesAdm = true;
    }

    var service = dynamapLimitesAdm.getMapService();

    var el = document.getElementById('layerLimitesAdm' + service.layers[3].id);

    switch (map.getZoom()) {
        case 5:
            service.layers[50].visible = (el.checked === true);
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 6:
            service.layers[50].visible = false;
            service.layers[48].visible = (el.checked === true);
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 7:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = (el.checked === true);
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 8:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = (el.checked === true);
            service.layers[44].visible = (el.checked === true);
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 9:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = (el.checked === true);
            service.layers[41].visible = (el.checked === true);
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 10:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = (el.checked === true);
            service.layers[38].visible = (el.checked === true);
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 11:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = (el.checked === true);
            service.layers[34].visible = (el.checked === true);
            service.layers[35].visible = (el.checked === true);
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 12:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = (el.checked === true);
            service.layers[30].visible = (el.checked === true);
            service.layers[31].visible = (el.checked === true);
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 13:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = (el.checked === true);
            service.layers[26].visible = (el.checked === true);
            service.layers[27].visible = (el.checked === true);
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 14:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = (el.checked === true);
            service.layers[22].visible = (el.checked === true);
            service.layers[23].visible = (el.checked === true);
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 15:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = (el.checked === true);
            service.layers[18].visible = (el.checked === true);
            service.layers[19].visible = (el.checked === true);
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 16:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = (el.checked === true);
            service.layers[14].visible = (el.checked === true);
            service.layers[15].visible = (el.checked === true);
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 17:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = (el.checked === true);
            service.layers[10].visible = (el.checked === true);
            service.layers[11].visible = (el.checked === true);
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 18:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = (el.checked === true);
            service.layers[6].visible = (el.checked === true);
            service.layers[7].visible = (el.checked === true);
            service.layers[1].visible = false;
            service.layers[2].visible = false;
            service.layers[3].visible = false;
            break;
        case 19:
            service.layers[50].visible = false;
            service.layers[48].visible = false;
            service.layers[46].visible = false;
            service.layers[43].visible = false;
            service.layers[44].visible = false;
            service.layers[40].visible = false;
            service.layers[41].visible = false;
            service.layers[37].visible = false;
            service.layers[38].visible = false;
            service.layers[33].visible = false;
            service.layers[34].visible = false;
            service.layers[35].visible = false;
            service.layers[29].visible = false;
            service.layers[30].visible = false;
            service.layers[31].visible = false;
            service.layers[25].visible = false;
            service.layers[26].visible = false;
            service.layers[27].visible = false;
            service.layers[21].visible = false;
            service.layers[22].visible = false;
            service.layers[23].visible = false;
            service.layers[17].visible = false;
            service.layers[18].visible = false;
            service.layers[19].visible = false;
            service.layers[13].visible = false;
            service.layers[14].visible = false;
            service.layers[15].visible = false;
            service.layers[9].visible = false;
            service.layers[10].visible = false;
            service.layers[11].visible = false;
            service.layers[5].visible = false;
            service.layers[6].visible = false;
            service.layers[7].visible = false;
            service.layers[1].visible = (el.checked === true);
            service.layers[2].visible = (el.checked === true);
            service.layers[3].visible = (el.checked === true);
            break;
    }
    
    var zoomini = map.getZoom();
    map.setZoom(zoomini - 1);
    map.setZoom(zoomini);
}

function setVisLimitesUgel() {

    if (setLimitesUgel == false) {
        dynamapUgel.setMap(map);
        setLimitesUgel = true;
    }

    var service = dynamapUgel.getMapService();

    var el = document.getElementById('layerLimitesUgel' + service.layers[1].id);

    switch (map.getZoom()) {
        case 7:
            service.layers[25].visible = (el.checked === true);
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 8:
            service.layers[25].visible = false;
            service.layers[23].visible = (el.checked === true);
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 9:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = (el.checked === true);
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 10:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = (el.checked === true);
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 11:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = (el.checked === true);
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 12:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = (el.checked === true);
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 13:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = (el.checked === true);
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 14:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = (el.checked === true);
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 15:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = (el.checked === true);
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 16:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = (el.checked === true);
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 17:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = (el.checked === true);
            service.layers[3].visible = false;
            service.layers[1].visible = false;
            break;
        case 18:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = (el.checked === true);
            service.layers[1].visible = false;
            break;
        case 19:
            service.layers[25].visible = false;
            service.layers[23].visible = false;
            service.layers[21].visible = false;
            service.layers[19].visible = false;
            service.layers[17].visible = false;
            service.layers[15].visible = false;
            service.layers[13].visible = false;
            service.layers[11].visible = false;
            service.layers[9].visible = false;
            service.layers[7].visible = false;
            service.layers[5].visible = false;
            service.layers[3].visible = false;
            service.layers[1].visible = (el.checked === true);
            break;
    }

    var zoomini = map.getZoom();
    map.setZoom(zoomini - 1);
    map.setZoom(zoomini);

}

function toc(controlTOC) {
    
    //svc = new gmaps.ags.MapService(layersDefinition.layers[0].service);

    //Centro Poblado
    dynamapCP = new gmaps.ags.MapOverlay(layersDefinition.layers[0].service, {
        opacity: 1
    });

    google.maps.event.addListenerOnce(dynamapCP.getMapService(), 'load', function () {

        if (layersDefinition.layers[0].visible) {
            dynamapCP.setMap(map);
            setmapCP = true;
        }

        var serviceCP = dynamapCP.getMapService();

        for (var i = 0; i < serviceCP.layers.length; i++) {

            if (serviceCP.layers[i].name == "CP") {
                tocCP += '<input type="checkbox" id="layerCP' + serviceCP.layers[i].id + '"';

               
                if (layersDefinition.layers[0].visible) {
                    tocCP += ' checked="checked"';
                }

                

                tocCP += ' onclick="setVisCP()">'+ layersDefinition.layers[0].caption +'<br/>';
                
                break;
            }
        }

        controlTOC.innerHTML = tocCP + tocIE + tocLimitesAdm + tocLimitesUgel;

    });
    
    //IE
    dynamapIE = new gmaps.ags.MapOverlay(layersDefinition.layers[1].service, {
        opacity: 1
    });

    google.maps.event.addListenerOnce(dynamapIE.getMapService(), 'load', function () {

        if (layersDefinition.layers[1].visible) {
            dynamapIE.setMap(map);
            setmapIE = true;
        }

        var serviceIE = dynamapIE.getMapService();

        for (var i = 0; i < serviceIE.layers.length; i++) {

            if (serviceIE.layers[i].name == "IE") {
                tocIE += '<input type="checkbox" id="layerIE' + serviceIE.layers[i].id + '"';

               
                if (layersDefinition.layers[1].visible) {
                    tocIE += ' checked="checked"';
                }


                tocIE += ' onclick="setVisIE()">' + layersDefinition.layers[1].caption + '<br/>';
               
                break;
            }
        }

        controlTOC.innerHTML = tocCP + tocIE + tocLimitesAdm + tocLimitesUgel;

    });


    //Limites
    var lmpol = new gmaps.ags.MapService(layersDefinition.layers[2].service);

    dynamapLimitesAdm = new gmaps.ags.MapOverlay(lmpol, {
        opacity: 1
    });

    google.maps.event.addListenerOnce(dynamapLimitesAdm.getMapService(), 'load', function () {

        if (layersDefinition.layers[2].visible) {
            dynamapLimitesAdm.setMap(map);
            setLimitesAdm = true;
        }

        var serviceLimitesAdm = dynamapLimitesAdm.getMapService();

        for (var i = 0; i < serviceLimitesAdm.layers.length; i++) {

            if (serviceLimitesAdm.layers[i].name == "Distritos") {
                tocLimitesAdm += '<input type="checkbox" id="layerLimitesAdm' + serviceLimitesAdm.layers[i].id + '"';

                
                if (layersDefinition.layers[2].visible) {
                    tocLimitesAdm += ' checked="checked"';
                }
                
                tocLimitesAdm += ' onclick="setVisLimitesAdm()">' + layersDefinition.layers[2].caption + '<br/>';
                
                break;
            }
        }

        controlTOC.innerHTML = tocIE + tocCP + tocLimitesAdm + tocLimitesUgel;

    });

    //Ugel
    var lmugl = new gmaps.ags.MapService(layersDefinition.layers[3].service);

    dynamapUgel = new gmaps.ags.MapOverlay(lmugl, {
        opacity: 1
    });

    google.maps.event.addListenerOnce(dynamapUgel.getMapService(), 'load', function () {

        if (layersDefinition.layers[3].visible) {
            dynamapUgel.setMap(map);
            setLimitesUgel = true;
        }

        var serviceLimitesUgel = dynamapUgel.getMapService();
        
        for (var i = 0; i < serviceLimitesUgel.layers.length; i++) {

            if (serviceLimitesUgel.layers[i].name == "ugel") {
                tocLimitesUgel += '<input type="checkbox" id="layerLimitesUgel' + serviceLimitesUgel.layers[i].id + '"';
                              
                if (layersDefinition.layers[3].visible) {
                    tocLimitesUgel += ' checked="checked"';
                }

                tocLimitesUgel += ' onclick="setVisLimitesUgel()">' + layersDefinition.layers[3].caption + '<br/>';
                
                break;
            }
        }

        controlTOC.innerHTML = tocIE + tocCP + tocLimitesAdm + tocLimitesUgel;

    });
}

function deleteOverlaysPuntos() {
    if (markerpuntosCP != null) {

        for (var i = 0; i <= markerpuntosCP.length - 1; i++) {
            markerpuntosCP[i].setMap(null);
        }

    }

    if (markerpuntosIE != null) {

        for (var i = 0; i <= markerpuntosIE.length - 1; i++) {
            markerpuntosIE[i].setMap(null);
        }

    }
}

function Ver()
{
    VerEnMapa($('#txtcodcp').val(), $('#txtniveles').val(), $('#txtgestion').val(), $('#txtcodmod').val());
}


function VerEnMapa(codigo_centro_poblado,nivmod,gesdep,codmod) {

    deleteOverlaysPuntos();

    //ejecutando webservices PADRON
    varType = "GET";
    varUrl = "";
    varData = "";
    varContentType = "text/xml;charset=utf-8";
    varDataType = "json";
    varProcessData = true;


    var codgeo = '';
    var codugel = '';
    var codmod = ''
    var anexo = '';
    var nombreie = '';
    var codlocal = '';
    var direccion = '';
    var cenpob = '';
    var localidad = '';
    var nivmod = '';
    var gesdep = '';
    var codcp = '';
    var codcp = codigo_centro_poblado;
    
    
    if (codigo_centro_poblado.lenght == 0 || codigo_centro_poblado.lenght==undefined)
        codcp = $('#txtcodcp').val();
    else
        codcp = codigo_centro_poblado;
    
    if (nivmod.lenght == 0 || nivmod.lenght == undefined)
        nivmod = $('#txtniveles').val();
    
    if (gesdep.lenght == 0 || gesdep.lenght == undefined)
        gesdep = $('#txtgestion').val();
    

    if (codmod.lenght == 0 || codmod.lenght == undefined)
        codmod = $('#txtcodmod').val();
    

    
    var estado = '1';
    var ubicados = '1';

    varUrl = nomdominio + "servicios/rest/service/restsig.svc/padronsac?codcp=" + codcp + '&nivmod=' + nivmod + '&gesdep=' + gesdep + '&codmod=' + codmod;

    $.ajax({
        type: varType, //GET or POST or PUT or DELETE verb
        url: varUrl, // Location of the service
        data: varData, //Data sent to server
        contentType: varContentType, // content type sent to server
        dataType: varDataType, //Expected data format from server
        processdata: varProcessData, //True or False
        success: function (msg) {//On Successfull service call
            ServiceSucceededIE(msg);
        },
        error: ServiceFailed// When Service call fails
    });

    
    var ubigeo = '';

    //var codcp = $('#txtcodcp').val();
    
    var nomcp = '';
    var ubicado = '';
    var area = '';
    var fuentecp = '';

    //ejecutando webservices CCPP
    varType = "GET";
    varUrl = nomdominio+"servicios/rest/service/restsig.svc/ccpp?ubigeo=" + ubigeo + "&codcp=" + codcp + "&nomcp=" + nomcp + "&ubicado=" + ubicado + "&area=" + area + "&fuentecp=" + fuentecp;
    varData = '';
    varContentType = "text/xml;charset=utf-8";
    varDataType = "json";
    varProcessData = true;

    $.ajax({
        type: varType, //GET or POST or PUT or DELETE verb
        url: varUrl, // Location of the service
        data: varData, //Data sent to server
        contentType: varContentType, // content type sent to server
        dataType: varDataType, //Expected data format from server
        processdata: varProcessData, //True or False
        success: function (msg) {//On Successfull service call
            ServiceSucceededCP(msg);
        },
        error: ServiceFailed// When Service call fails
    });

}

function ServiceSucceededCP(result) {//When service call is sucessful
    var k = 0;
    var obj = $.parseJSON(result);

    if (obj.Rows.length == 0) {
        alert("El Centro poblado no existe en la base de datos del área SIG");
        return;
    }

    $.each(obj.Rows, function (i, campo) {

        if (campo.LATITUD_DEC=="" || campo.LONGITUD_DEC=="")
        {
            alert("El Centro poblado no posee coordenadas de ubicación");
            return;
        }

        contentString = "<table border=0>";
                
        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>CENTRO POBLADO: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.DENOMINACION + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>VRAEM: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.VRAEM + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>JUNTOS: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.JUNTOS + "</td>";
        contentStrkng = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>GRANDES CIUDADES: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.GC80 + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>LATITUD: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.LATITUD_DEC + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>LONGITUD: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.LONGITUD_DEC + "</td>";
        contentString = contentString + "</tr>"

        contentString = contentString + "</table>";

        var myLatlng = new google.maps.LatLng(campo.LATITUD_DEC, campo.LONGITUD_DEC);
        map.setCenter(myLatlng);
        map.setZoom(14);

        k++;
        
        var marker = new google.maps.Marker({
            position: myLatlng,
            icon: nomdominio + "servicios/rest/images/ie_green.png",
            map: map,
            html: contentString
        });

        markerpuntosCP.push(marker);

        infowindowCP = new google.maps.InfoWindow();
        
        google.maps.event.addListener(marker, 'click', function () {

            infowindowCP.setContent(this.html);
            infowindowCP.open(map, this);
            
        });
        
        google.maps.event.addListener(map, 'click', function () {
            infowindowCP.close();
        });

    });

    varType = null;
    varUrl = null;
    varData = null;
    varContentType = null;
    varDataType = null;
    varProcessData = null;
}

function ServiceSucceededIE(result) {//When service call is sucessful
    var k = 0;
    var obj = $.parseJSON(result);

    if (obj.Rows.length == 0) return;

    $.each(obj.Rows, function (i, campo) {

        contentString = "<table border=0>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>CÓDIGO MODULAR: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.CODIGO_MODULAR + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>NOMBRE DE ESCUELA: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.NOMBRE_ESCUELA + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>DIRECCIÓN: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.DIRECCION_ESCUELA + "</td>";
        contentStrkng = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>NIVEL: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.NIVEL_MODALIDAD + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>TOTAL ALUMNOS: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.TOTAL_ALUMNOS + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>TOTAL DOCENTES: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.TOTAL_DOCENTES + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>LATITUD: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.LATITUD_DEC + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "<tr>";
        contentString = contentString + "<td style='font-weight: bold; color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'><b>LONGITUD: </b></td>";
        contentString = contentString + "<td style='color: Black; font-family: Arial, Helvetica, sans-serif;font-size: 8px;'>" + campo.LONGITUD_DEC + "</td>";
        contentString = contentString + "</tr>";

        contentString = contentString + "</table>";

        var myLatlng = new google.maps.LatLng(campo.LATITUD_DEC, campo.LONGITUD_DEC);
        
        k++;
       
        var marker = new google.maps.Marker({
            position: myLatlng,
            icon: nomdominio + "servicios/rest/images/ie_orange.png",
            map: map,
            html: contentString
        });

        markerpuntosIE.push(marker);

        infowindowIE = new google.maps.InfoWindow();

        google.maps.event.addListener(marker, 'click', function () {

            infowindowIE.setContent(this.html);
            infowindowIE.open(map, this);

        });

        google.maps.event.addListener(map, 'click', function () {

            infowindowIE.close();

        });

    });

    varType = null;
    varUrl = null;
    varData = null;
    varContentType = null;
    varDataType = null;
    varProcessData = null;
}

function ServiceFailed(result) {
    alert('La consulta del webservice SIG no devolvió resultados');
    varType = null;
    varUrl = null;
    varData = null;
    varContentType = null;
    varDataType = null;
    varProcessData = null;
}