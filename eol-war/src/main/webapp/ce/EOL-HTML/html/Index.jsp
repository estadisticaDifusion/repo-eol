<%-- 
    Document   : Index
    Created on : 03/03/2014, 05:08:02 PM
    Author     : CMOLINA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
﻿<!DOCTYPE html>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>


<html>
    <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" type="text/css" href="../css/styles.css" />
        <link rel="stylesheet" type="text/css" href="../css/redmond/jquery-ui-1.10.4.custom.css" />
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="../scripts/jquery-2.1.0.js"></script>
        <script src="../js/jquery-ui-1.10.4.custom.js"></script>
        <script src="../scripts/arcgislink-min.js" type="text/javascript"></script>
        <script src="../scripts/mapa.js"></script>
        <title>Estadística On Line</title>
    </head>

    <link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />
    <link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/popup.css'/>" />
    <link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />

    <body>
        <div style="width:1150px; margin-left: 370px; margin-bottom: 200px; margin-top: 20px">
            <div id="opciones_tablero">

                <div class="opciones" >
                    <center>
                        <span style="color:white;font-family:  Arial, Helvetica,sans-serif;font-size: 9pt;font-weight: Bold;">Menú del Sistema</span>
                    </center>
                </div>

                <div id="tablero">
                    <ul>
                        <logic:notPresent name="LOGGED_USER">
                            <li>
                                <html:link page="/">
                                <bean:message key="home" />
                            </html:link>
                            </li>
                            <li><html:link page="/ce">
                                <bean:message key="ce.login" />
                            </html:link>
                            </li>
                            <li><html:link page="/ugel">
                                <bean:message key="ugel.login" />
                            </html:link>
                            </li>

                        </logic:notPresent>
                        <logic:present name="LOGGED_USER">
                            <c:import url="/tiles/sidemenu-ce.jsp" />

                            <li><a target="_blank" href="http://escale.minedu.gob.pe/censo_instructivos" >Leer instrucciones</a></li>
                            <li><a target="_blank" href="http://escale.minedu.gob.pe/preguntas-frecuentes">Preguntas frecuentes</a></li>
                            <li><a target="_blank" href="http://escale.minedu.gob.pe/foros/-/message_boards?_19_mbCategoryId=812538">Foro</a></li>
                            <li><a target="_blank" href="http://escale.minedu.gob.pe/censo_instructivos">Ejemplos llenado</a></li>
                            <li><a target="_blank" href="http://escale.minedu.gob.pe/cedulas-borrador">Cédulas borrador (sólo para imprimir)</a></li>
                            <li><a target="_blank" href="EOL-HTML/html/Index.jsp?codmod=${param.codmod}&anexo=${param.anexo}">Ubicación Geográfica</a></li>
                            <li style="background-color: #C3D9FF">
                                <html:link  action="/logout">
                                <bean:message key="logout" />
                            </html:link>
                            </li>

                        </logic:present>
                    </ul>
                </div>
            </div>

            <div id="contenido" style="height: 850px; width:730px;overflow:Scroll; margin-left: 2px; margin-bottom: 20px; margin-top: 2px" >
                <div id="dialog" title="Ubique su local escolar">
                    <p align='justify'>
                        Esta opción le permitirá indicar la ubicación geográfica correcta de su local escolar. Para ello debe usar el mapa, acercarse lo máximo posible para marcar la posición del local escolar, los pasos para realizar esta acción se detallan a continuación.
                    </p>
                </div>
                <div id="container">
                    <div style="font-size: 12px;">
                        <li><a href="/estadistica/ce/">Volver</a></li>
                        <span>Lista de escuelas en el local escolar</span>
                        <div id="d_ls_escuelas">
                            <table id="t_ls_escuelas">
                                <thead>
                                    <tr style="font-size: 12px">
                                        <th>COD. MODULAR</th><th>ANEXO</th><th>COD. LOCAL</th><th>NOMBRE DE ESCUELA</th><th>NIVEL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div id="dconfirm_ubic" class="inst">
                            <table class="tpasos">
                                <tbody>
                                    <tr>
                                        <td class="npaso"></td>
                                        <td class="tpaso">
                                            <ul>
                                                <li>
                                                    El marcador rojo <img src="../img/red-dot.png" /> indica la sugerencia de la ubicación del local, de no aparecer, 
                                                    el mapa se centrará en el distrito en el cual se ubica la escuela.
                                                <li>Por favor, indique si está de acuerdo con la sugerencia de ubicación del local escolar en el mapa. </li></br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="r_u_correct" id="r_u_correct_si" /><b>Si estoy de acuerdo</b>&nbsp;
                                                <input type="radio" name="r_u_correct" id="r_u_correct_no" /><b>No estoy de acuerdo</b>.
                                                </li>
                                                <li id="obs_dyn_map"></li>
                                                <li>
                                                    Al hacer click en <b>No</b> aparecerá en el mapa un marcador verde <img src="../img/green-dot.png" />, 
                                                    el cual utilizará para ubicar correctamente el local escolar. Vea el siguiente paso para más detalles.
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="dmap_instruction" class="inst">
                            <table class="tpasos">
                                <tbody>
                                    <tr>
                                        <td class="npaso"></td>
                                        <td class="tpaso">
                                            <ul>
                                                <li>
                                                    Para aumentar el zoom del mapa, hacer doble click al lugar del mapa que desea ver con mas detalle. También puede hacer click en los íconos 
                                                    <img src="../img/aumenta-zoom.png" /> y <img src="../img/disminuye-zoom.png" /> ubicados en la parte izquierda del mapa.
                                                </li>
                                                <li>
                                                    El marcador verde <img src="../img/green-dot.png" /> indica la posición que Ud. elegirá para el local escolar. Si Ud. ha habilitado el marcador verde, 
                                                    puede arrastrarlo con el mouse al lugar que desee para ubicar correctamente el local escolar.
                                                </li>
                                                <li id="obs_dyn_map2">
                                                    Si otro director en el mismo local ha habilitado el marcador verde, Ud. no podrá modificarlo a menos que el mismo director lo deshabilite.
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div id="divcontent"></div>
                        </div>
                        <div id="toc"></div>
                    </div>
                    <div>
                        <div id="dtipoobs_instruccion" class="inst">
                            <table class="tpasos">
                                <tbody>
                                    <tr>
                                        <td class="npaso"></td>
                                        <td class="tpaso">
                                            <ul>
                                                <li>
                                                    Seleccione por lo menos una opción de la lista, también puede modificar sus observaciones después de guardarlas.
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="cont_tipoobs">
                            <ul></ul>
                        </div>
                    </div>
                    <div id="botones">
                        <input type="button" id="btngrabar" value="Grabar" /><input type="button" id="btncancelar" value="Cancelar" />
                    </div>
                    <div>             
                        <%
                            String codmod2 = request.getParameter("codmod");
                            String anexo2 = request.getParameter("anexo");
                        %>

                        <input type="hidden" id="txtcodmod" value="<%=codmod2%>" />
                        <input type="hidden" id="txtanexo" value="<%=anexo2%>" /> 
                        <%--<input type="button" id="btnbuscar" value="Buscar"/> --%>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>