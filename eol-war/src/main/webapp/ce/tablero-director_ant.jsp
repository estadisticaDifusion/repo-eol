<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="pe.gob.minedu.escale.eol.enums.admin.EstadoActividadEnum"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@page import="pe.gob.minedu.escale.eol.auth.domain.AuthUsuario"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<% java.util.Date hoy = new java.util.Date();
    AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
    pageContext.setAttribute("usuario",usuario.getUsuario() );
    pageContext.setAttribute("nombre",usuario.getNombre() );
    
    pageContext.setAttribute("ENUM_INICIADO",EstadoActividadEnum.INICIADO.toString() );
    pageContext.setAttribute("ENUM_EXTENPORANEO",EstadoActividadEnum.EXTENPORANEO.toString() );
    pageContext.setAttribute("ENUM_SININICIAR",EstadoActividadEnum.SIN_INICIAR.toString() );
%>

<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />
<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/popup.css'/>" />
<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />
<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />

<h1>Tablero de Control del Director de Institución Educativa</h1>
<p>

<div align="left" style="position: relative;left: 15px;font:12px Arial,Helvetica,Verdana,sans-serif;padding-bottom:7px;"  >
    <span style="color: #333333; font-weight: bold">Ud. está visualizando el tablero de control de la I.E con código modular:${param.codmod} anexo:${param.anexo} y nivel: <fmt:message bundle="${msg}" key="nivel.${param.nivel}"/> </span>
</div>

<p>

    
<html:link action="/ce/tablero?codmod=${param.codmod}&anexo=${param.anexo}&nivel=${param.nivel}&codlocal=${param.codlocal} ">
    Actualizar tablero
</html:link>

    <div  style=" width: 100%" >
        <!--div class="clear"></div-->
        <table class="body_content_tablero"  border=0>
            <tr>
                <td class="tablero">
                    <div id="cuadros">
                            <ul>
                                <li class="pendiente_ley">Pendiente</li>
                                <li class="reportado_ley">Reportado</li>
                            </ul>
                    </div>
                    <div id="contenido">

                        <c:forEach items="${mapAct}" var="mp">
                            <strong style="font:150% Arial,Helvetica,Verdana,sans-serif;color:black; font-size: 20px;">${mp.key}</strong>
                            <div id="tableContainer" >

                                <table  cellpadding="1" cellspacing="1" class="tabla"  width="100%">
                                    <thead>
                                        <tr>
                                            <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.actividad"/></th>
                                            <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.plazo"/></th>
                                            <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.formato"/></th>
                                            <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.situacion"/></th>
                                            <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.fecha"/></th>
                                            <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.constancia"/></th>
                                            <th>Archivo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${mp.value}" var="cedula">
                                            <tr>
                                                <td width="150" height="50">
                                                    <span >${cedula.actividad.nombre}</span>
                                                    <span style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;">
                                                                           <br/> (<fmt:message bundle="${msg}" key="actividad.estado.${cedula.actividad.estadoActividad}"/>)
                                                    </span>

                                                </td>
                                                <td width="120" align="center" >

                                                    <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                                        <fmt:formatDate pattern="MMM yyyy" value="${cedula.actividad.fechaInicio}"/>
                                                    </font>
                                                    -
                                                    <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                                        <fmt:formatDate pattern="MMM yyyy" value="${cedula.actividad.fechaTermino}"/>                                                        
                                                    </font>

                                                </td>
                                                <td width="90" align="center" valign="center">
                                                    <c:choose>
                                                        <c:when test="${(cedula.actividad.estadoActividad==ENUM_INICIADO ||
                                                                        cedula.actividad.estadoActividad==ENUM_EXTENPORANEO)
                                                                        &&  cedula.actividad.estadoFormato}">
                                                            <c:if test="${not empty cedula.actividad.urlFormato}">
                                                            <span>

                                                                <c:url var="url" value="${cedula.actividad.urlFormato}">
                                                                    <c:param name="codmod" value="${param.codmod}" />
                                                                    <c:param name="anexo" value="${param.anexo}" />
                                                                    <c:param name="nivel" value="${param.nivel}" />
                                                                    <c:param name="codlocal" value="${param.codlocal}" />
                                                                    <c:if test="${not empty cedula.nroEnvio}">
                                                                        <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                    </c:if>

                                                                </c:url>

                                                                <a class="xls" href="${url}"> Descargar </a>
                                                            </span>
                                                            </c:if>

                                                        </c:when>
                                                        <c:otherwise>

                                                            <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;color: Gray;vertical-align:middle;" >
                                                                <strong class="xls_finish">Descargar </strong>
                                                                
                                                            </span>
                                                        </c:otherwise>

                                                    </c:choose>

                                                </td>
                                                <td align="center">
                                                    <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR}">
                                                        <c:if test="${!cedula.envio}">
                                                            <strong class="pendiente">&nbsp;</strong>
                                                        </c:if>
                                                        <c:if test="${cedula.envio}">
                                                            <strong class="reportado">&nbsp;</strong>
                                                        </c:if>
                                                    </c:if>
                                                </td>
                                                <td align="center">
                                                    <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR}">
                                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;">
                                                            <fmt:formatDate pattern="dd/MM/yyyy" value="${cedula.fechaEnvio}"/>
                                                        </span>
                                                    </c:if>
                                                </td>
                                                <td width="80">
                                                    <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR }">
                                                        <c:if test="${cedula.envio}">
                                                            <c:if test="${not empty cedula.actividad.urlConstancia and cedula.actividad.estadoConstancia }">
                                                            <span>
                                                                <c:url var="url" value="${cedula.actividad.urlConstancia}">
                                                                    <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                    <c:param name="nivel" value="${param.nivel}" />
                                                                </c:url>
                                                             <a class="pdf" href="${url}" target="_blank">
                                                             <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">Obtener </span>
                                                             </a>
                                                            </span>
                                                            </c:if>

                                                        </c:if>
                                                    </c:if>
                                                </td>
                                                <td align="center">


                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <br/>
                        </c:forEach>


                    </div>
                <td>             
            </tr>
        </table>
    </div>


<!-- FIN MODIF INICIO C.E - JCMATAMOROS : 21-03-2011 -->

