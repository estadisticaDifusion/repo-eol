<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html:xhtml />
 <div id="opciones_tablero">
        <div class="opciones" >
            <center>
                <span style="color:white;font-family: Arial, Helvetica,sans-serif;font-size: 9pt;font-weight: Bold;">Menú del Sistema</span>
            </center>
        </div>

        <div id="tablero">
            <ul>
                <logic:notPresent name="LOGGED_USER">
                <li>
                    <html:link page="/">
                        <bean:message key="home" />
                    </html:link>
                </li>
                <li>
                    <html:link action="/login/ce/main">
                        <bean:message key="ce.login" />
                    </html:link>
                </li>
                <li>
                	<html:link action="/login/ugel/main">
                		<bean:message key="ugel.login" />
                    </html:link>
                </li>

                </logic:notPresent>
                <logic:present name="LOGGED_USER">
                    <c:import url="/tiles/sidemenu-${fn:toLowerCase(LOGGED_USER.tipo)}.jsp" />

                    <li><a target="_blank" href="http://escale.minedu.gob.pe/censo_instructivos" >Leer instrucciones</a></li>
                    <li><a target="_blank" href="http://escale.minedu.gob.pe/preguntas-frecuentes">Preguntas frecuentes</a></li>
                    <li><a target="_blank" href="http://escale.minedu.gob.pe/foros/-/message_boards?_19_mbCategoryId=812538">Foro</a></li>
                    <li><a target="_blank" href="http://escale.minedu.gob.pe/censo_instructivos">Ejemplos llenado</a></li>
                    <li><a target="_blank" href="http://escale.minedu.gob.pe/cedulas-borrador">Cédulas borrador (sólo para imprimir)</a></li>
                    <%--   <li><a target="_blank" href="EOL-HTML/html/Index.jsp?codmod=${param.codmod}&anexo=${param.anexo}">Ubicación Geográfica</a></li> --%>
                     <li style="background-color: #C3D9FF">
                        <html:link  action="/logout">
                            <bean:message key="logout" />
                        </html:link>
                    </li>
                    <script>
                      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                      ga('create', 'UA-3419119-31', 'auto');
                      ga('send', 'pageview');

                    </script>

                </logic:present>
            </ul>
        </div>
 </div>