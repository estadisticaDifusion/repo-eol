<%-- <%@page contentType="text/html; charset=UTF-8"%> --%>

<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.security.SecureRandom"%>
<%@ page import="java.security.NoSuchAlgorithmException"%>
<%@ page import="java.net.MalformedURLException"%>
<%@ page import="java.net.URL"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>

<%!SecureRandom prng = null;
	String randomNumext = "";%>

<%
	/*
	Cookie[] arrayC= request.getCookies();
	
	if(arrayC != null){
		
		for(int i= 0; i< arrayC.length; i++){
			arrayC[i].setMaxAge(0);
			arrayC[i].setHttpOnly(true);
			response.addCookie(arrayC[i]);
		}
		
	}*/

	//String randomNumext = "";
	String dominio = "";
	try {

		URL aURL = new URL(request.getRequestURL().toString());

		dominio = aURL.getProtocol() + "://" + aURL.getAuthority();

		//SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
		prng = SecureRandom.getInstance("SHA1PRNG");
		randomNumext = Integer.toHexString(new Integer(prng.nextInt()))
				+ Integer.toHexString(new Integer(prng.nextInt())).substring(0, 2);

	} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	}
	//	"unsafe-eval "  +  dominio + " ;"  +

	String csp = "script-src  'strict-dynamic' 'nonce-" + randomNumext + "' https: http: ; " +
	//"script-src  'strict-dynamic' 'nonce-"+ randomNumext +"' 'unsafe-inline' https: http:  'unsafe-eval' " + dominio + " ; " +
			"media-src 'self' blob:; " +
			//"object-src 'none'  blob:; " +
			"object-src 'self'  blob:; " + "base-uri 'none' ;" + "frame-src https://www.google.com ; "
			+ "child-src 'self'; " + "report-uri " + dominio + " ;" +
			//"unsafe-eval "  +  dominio + " ;"  +
			"img-src  https://www.google-analytics.com  " + dominio + "  ; "
			+ "style-src 'unsafe-inline' https://www.bing.com " + dominio + "  ;" + "font-src " + dominio
			+ " data: blob: ;" +
			//"connect-src http://10.36.136.152:8081/admws/rest/usuario/* http://10.36.136.152:8081/regprogramaws/* http://10.36.136.152:8081/regprogramaws/svt/inSoon http://10.36.136.152:8081/regprogramaws/svt/inMoon ;" + 
			"connect-src https://www.google-analytics.com " + dominio + " ; ";
	//"connect-src 'self' ; ";

	//String sessionid = request.getSession().getId();
	//response.setHeader("X-Frame-Options", "DENY");
	//response.setHeader("X-Content-Type-Options", "nosniff");
	//response.setHeader("X-XSS-Protection", "1; mode=block");
	//response.addHeader( "Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly");

	String sessionid = request.getSession().getId();

	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Expires", "0");
	response.setHeader("Pragma", "no-cache");
	//response.addHeader("Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly ;SameSite=Strict ");
	response.addHeader("Content-Security-Policy", csp);
	
	if (session.getAttribute(Constantes.TIMEOUT_INACTIVE) != null) {
		long timeout = (long) session.getAttribute(Constantes.TIMEOUT_INACTIVE);
		System.out.println("default.jsp - time JSP: " + timeout);
		response.setHeader("Refresh", timeout + "; URL=/estadistica/sesionexpiro.uee");
	}

	
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<html:html xhtml="true">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="stylesheet"
	href="<html:rewrite page='/recursos/css/Refresh.css'/>" type="text/css" />
<link
	href="<c:url value='/recursos/bootstrap-3.3.7-dist/css/bootstrap.min.css'/>"
	rel="stylesheet" />
<script type="text/javascript"
	src="<c:url value='/recursos/jquery/jquery-3.4.1.min.js'/>"
	nonce="<%=randomNumext%>">
	
</script>
<%--<script type="text/javascript"  src="<c:url value='/recursos/bootstrap-3.3.7-dist/js/bootstrap.min.js'/>" nonce="<%= randomNumext%>" > </script> --%>
<script type="text/javascript"
	src="<c:url value='/recursos/bootstrap-4.3.1-dist/js/bootstrap.min.js'/>"
	nonce="<%=randomNumext%>">
	
</script>

<%-- SECCION REUBICADA CE --%>
<%-- <script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script> --%>
<script type="text/javascript"
	src="<c:url value='/recursos/jquery/aes.js'/>"
	nonce="<%=randomNumext%>"></script>
<script type="text/javascript"
	src="<c:url value='/recursos/js/accesoce.js'/>"
	nonce="<%=randomNumext%>"></script>
<%--Agregado para encriptacion --%>
<script type="text/javascript"
	src="<c:url value='/recursos/js/re-cipher.js'/>"
	nonce="<%=randomNumext%>"></script>
<script type="text/javascript"
	src="<c:url value='/recursos/js/crypto-js.js'/>"
	nonce="<%=randomNumext%>"></script>

<%-- SECCION REUBICADA UGEL --%>
<%-- <script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script> --%>
<%-- <script type="text/javascript" src="<c:url value='/recursos/jquery/aes.js'/>"></script> --%>
<script type="text/javascript"
	src="<c:url value='/recursos/js/accesougel.js'/>"
	nonce="<%=randomNumext%>"></script>

<%-- SECCION REUBICADA TABLERO ESTADISTICO --%>
<script type="text/javascript" nonce="<%=randomNumext%>">
	var urlR = '<c:url value="/"/>';
	var urlMsj = '<c:url value="/MensajeServlet"/>';
	var urlEol = '<c:url value="/EolCensoServlet"/>';
	var urlCEol = '<c:url value="/ClaveEolServlet"/>';
</script>
<script type="text/javascript"
	src="<c:url value='/recursos/js/popup_lib.js'/>"
	nonce="<%=randomNumext%>"></script>
<script type="text/javascript"
	src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"
	nonce="<%=randomNumext%>"></script>
<script type="text/javascript"
	src="<c:url value='/recursos/js/datepicker-es.js'/>"
	nonce="<%=randomNumext%>"></script>
<script type="text/javascript" nonce="<%=randomNumext%>">
	var ugel = '${LOGGED_USER.usuario}'
</script>
<c:if test="${fn:contains(header['user-agent'],'MSIE')}">
	<script type="text/javascript"
		src="<c:url value='/recursos/js/json2.js'/>"
		nonce="<%=randomNumext%>"></script>
</c:if>
<script type="text/javascript"
	src="<c:url value='/recursos/js/form_estadistico.js'/>"
	nonce="<%=randomNumext%>"></script>
<%-- PAGE credentials --%>
<c:if test="${LOGGED_USER_PRF != 'ADMIN'}">
	<script type="text/javascript"
		src="<c:url value='/recursos/js/form_credenciales_usuario.js'/>"
		nonce="<%=randomNumext%>"></script>
</c:if>
<c:if test="${LOGGED_USER_PRF eq 'ADMIN'}">
	<%-- SECCION REUBICADA TABLERO GESTION --%>
	<%-- PAGE GESTION --%>
	<script type="text/javascript"
		src="<c:url value='/recursos/js/form_gestion.js'/>"
		nonce="<%=randomNumext%>"></script>
	<%-- PAGE ARCH --%>
	<script type="text/javascript"
		src="<c:url value='/recursos/js/form_file.js'/>"
		nonce="<%=randomNumext%>"></script>
	<%-- PAGE oauth --%>
	<script type="text/javascript"
		src="<c:url value='/recursos/js/form_oauth.js'/>"
		nonce="<%=randomNumext%>"></script>
</c:if>

<%-- SECCION REUBICADA TABLERO DIRECTOR --%>


<title>:::: <bean:message key="logotext.1" /> <bean:message
		key="logotext.2" /> ::::
</title>

</head>
<body style="text-align: center; background: #E9E6D7; margin-top: 0">
	<html:link linkName="top"></html:link>

	<center>
	<div id="wrap">
		<div id="minheader" class="minheader">
			<header role="banner" id="banner"> <hgroup id="heading">
			<h1 class="company-title">
				<a title="Ir a ESCALE - Unidad de Estadística Educativa"
					class="logo"> <span>ESCALE - Unidad de Estadística
						Educativa</span>
				</a>
			</h1>
			</hgroup> </header>
		</div>
		<div id="header">
			<!--                <nav class="navbar navbar-default">
                                        <div class="container-fluid">
                                            <div class="navbar-header">
                                                <a class="navbar-brand" href="#">estadísticaon-line</a>
                                            </div>
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="#">Home</a></li>
                                                <li class="dropdown">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Cédulas
                                                        <span class="caret"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a target="_blank" href="http://escale.minedu.gob.pe/cedulas-borrador">Cédulas borrador (sólo para imprimir)</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <ul class="nav navbar-nav navbar-right">
                                                <li>
                    <html:link  action="/logout">
                        <span class="glyphicon glyphicon-user"></span>
                        <bean:message key="logout" />
                    </html:link>
                </li>
            </ul>
        </div>
    </nav>-->
			<!--  imendoza 20170417 -->
			<h1 id="logo-text">
				<bean:message key="logotext.1" />
				<span class="gray"><bean:message key="logotext.2" /></span>
			</h1>
			<h2 id="slogan">
				<bean:message key="slogan" />
			</h2>

		</div>
		<c:if test="${LOGGED_USER.usuario ne null}">
			<nav class="navbar navbar-default">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<!--   <li class="active"><a href="#">Inicio</a></li> -->
					<!--  
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Cédulas
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a target="_blank" href="http://escale.minedu.gob.pe/cedulas-borrador">Cédulas borrador (sólo para imprimir)</a></li>
                                    </ul>
                                </li>
                                -->

					<c:if test="${LOGGED_USER_PRF eq 'ADMIN'}">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Administrar <span
								class="caret"></span>
						</a>
							<ul class="dropdown-menu">
								<li><html:link action="/gestionActividad">
										<bean:message key="admin.gestion.actividades" />
									</html:link></li>
								<li><html:link action="/gestionArchivos">
										<bean:message key="admin.gestion.archivos" />
									</html:link></li>
								<li><html:link action="/gestionUsuariosClave">
										<bean:message key="admin.gestion.usuarios.clave.cambio" />
									</html:link></li>
							
							</ul>
						</li>
					</c:if>

				</ul>
			
			
			
				<ul class="nav navbar-nav navbar-right">
					<li><html:link action="/logout">
							<span class="glyphicon glyphicon-user"></span>
							<bean:message key="logout" />
						</html:link>
					</li>
				</ul>
			
			
				
				<c:if test="${LOGGED_USER_PRF != 'ADMIN'}">
					<ul class="nav navbar-nav navbar-right">
						<li><html:link action="/gestionCredencialesUsuario">
								<span class="glyphicon glyphicon-user"></span>
								<bean:message key="cambio.credenciales" />
							</html:link>
						</li>

						
					</ul>
					<ul class="nav navbar-nav navbar-left">
						<li>
							
							<span class="text-justify" style="line-height: 10px; position: relative; display: block; padding: 20px 0px 0px 2px; color: #555; font-size: 9pt;">
									<c:if test="${LOGGED_USER_PRF eq 'UGEL'}">
		                                        Perfil de Usuario : Estadístico <br />
									</c:if> <c:if test="${LOGGED_USER_PRF eq 'CE'}">
		                                        Perfil de Usuario : Director de IE <br />
									</c:if> <c:if test="${LOGGED_USER_PRF eq 'ADMIN'}">
		                                        Perfil de Usuario : Administrador <br />
									</c:if> 
							</span>
		                </li>
					
				</ul>
					
			
					
					
				</c:if>

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			</div>
			</nav>
		</c:if>
		<!-- menu -->
		<!--div id="menu"></div-->
		<!-- content-wrap starts here -->
		<div id="content-wrap">
			<c:if test="${param.codmod eq null && LOGGED_USER.usuario eq null}">
				<div id="sidebar">
					<%--h1><bean:message key="usuario.menu" /></h1--%>
					<br />
					<div>
						<tiles:get name="sidemenu" />
					</div>
					<p />
					<div>
						<div id="opciones_tablero">
							<div class="opciones">
								<center> <span
									style="color: white; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; font-weight: Bold;">
									<bean:message key="usuario.enlaces" />
								</span> </center>
							</div>

							<div id="tablero">
								<ul>
									<li><a
										href="<bean:message key="usuario.enlaces.1.enlace" />"
										target="_blank"><bean:message
												key="usuario.enlaces.1.titulo" /></a></li>
									<li><a
										href="<bean:message key="usuario.enlaces.2.enlace" />"
										target="_blank"><bean:message
												key="usuario.enlaces.2.titulo" /></a></li>
								</ul>
							</div>
						</div>
					</div>

					<c:if test="${fn:contains(header['user-agent'],'MSIE')}">
						<div class="left-box" style="text-align: center">
							Este sistema funciona mejor con <strong>Firefox</strong>. <br />
							<a href="http://www.mozilla.com/es/firefox?from=sfx&uid=0&t=341"><img
								border="0" alt="Spreadfirefox Affiliate Button"
								src="http://sfx-images.mozilla.org/affiliates/Buttons/firefox3/Spanish/fx-ES1-110x32.png" /></a>
						</div>
					</c:if>
				</div>
			</c:if>
		</div>
		<!--                    <div id="main">-->
		<div
			id="<c:if test="${param.codmod eq null}">main</c:if><c:if test="${LOGGED_USER.usuario ne null}">main_tablero_2017</c:if>"
			<!--                    Menu estadistico  - inicio   -->

			<!--                    Menu estadistico  - fin   -->

			<c:choose>
				<c:when test="${LOGGED_USER.cambiarContrasenia}">
					<p>
						<bean:message key="usuario.cambiarClave.aviso" />
					</p>
					<tiles:insert page="/pages/login/cambiarContraseniaForm.jsp" />
				</c:when>
				<c:otherwise>
					<tiles:get name="contenido" />
				</c:otherwise>
			</c:choose>
		</div>
		<!-- content-wrap ends here -->
		<!--footer starts here-->
		<div id="footer">
			<div style="border-top: 1.5px solid #055FCD;"></div>
			<p>
				<bean:message key="footer.copy" />
				<strong> <bean:message key="footer.autor" />
				</strong>
			</p>
		</div>
		<!-- wrap ends here -->
	</div>
	</center>
	<%--script type="text/javascript">
                        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
                    </script>
                    <script type="text/javascript">
                        try {
                            var pageTracker = _gat._getTracker("UA-2534992-8");
                            pageTracker._trackPageview();
                        } catch(err) {}</script--%>
	<%-- SECCION REUBICADA CE --%>
	<script type="text/javascript"
		src="<c:url value='/recursos/js/captchacallce.js'/>"
		nonce="<%=randomNumext%>"></script>

	<%-- SECCION REUBICADA UGEL --%>
	<script type="text/javascript"
		src="<c:url value='/recursos/js/captchacallce.js'/>"
		nonce="<%=randomNumext%>"></script>

	<script src="https://www.google.com/recaptcha/api.js" async defer
		nonce="<%=randomNumext%>"></script>
</body>
</html:html>
