<%@page contentType="text/html; charset=UTF-8"%>
<%
            response.setHeader("Cache-control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<html:html xhtml="true">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <link rel="stylesheet"
              href="<html:rewrite page='/recursos/css/Refresh.css'/>"
              type="text/css" />
        <title>:::: <bean:message key="logotext.1" /> <bean:message
                key="logotext.2" /> ::::</title>
        <!--meta name="google-site-verification" content="clYcU5awyx5gR8hbOROZlMfYX_IEUkj8iGbHGiu8tlg" /-->
    </head>
    <body  style="text-align:center;background:  #E9E6D7; margin-top: 0 ">
        <html:link linkName="top"></html:link>
        <!-- wrap starts here -->

        <center>
            <div id="wrap"><!--header -->
                <div id="header">
                    <h1 id="logo-text"><bean:message key="logotext.1" /><span
                            class="gray"><bean:message key="logotext.2" /></span></h1>
                    <h2 id="slogan"><bean:message key="slogan" /></h2>
                </div>
                <div id="menu-top">
                    <tiles:get name="menu-top" />
                    Header
                </div>
                <!-- menu -->
                <!--div id="menu"></div-->
                <!-- content-wrap starts here -->
                <div id="content-wrap">

                    <div id="main"><c:choose>
                            <c:when test="${LOGGED_USER.cambiarContrasenia}">
                                <p><bean:message key="usuario.cambiarClave.aviso" /></p>
                                <tiles:insert page="/pages/login/cambiarContraseniaForm.jsp" />
                            </c:when>
                            <c:otherwise>
                                <tiles:get name="contenido" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                <!-- content-wrap ends here -->
                </div>
                <!--footer starts here-->
                <div id="footer">
                    <div style="border-top: 1.5px solid #055FCD;">

                    </div>
                    <p><bean:message key="footer.copy" /> <strong> <bean:message
                                key="footer.autor" /> </strong> </p>
                </div>
                <!-- wrap ends here -->
            </div>
        </center>
        <%--script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try {
                var pageTracker = _gat._getTracker("UA-2534992-8");
                pageTracker._trackPageview();
            } catch(err) {}</script--%>
    </body>
</html:html>
