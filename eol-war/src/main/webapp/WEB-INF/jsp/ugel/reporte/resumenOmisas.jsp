<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%--@page import="com.liferay.portal.model.User"--%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@page import="java.util.Calendar"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
    <head>
        <title>Resumen de omisos</title>
        <c:choose>
            <c:when test="${param.formato ne 'xls'}">
                <fmt:setLocale value="fr" scope="request" />
            </c:when>
            <c:when test="${param.formato eq 'xls'}">
                <fmt:setLocale value="en" scope="request" />
            </c:when>
        </c:choose>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <c:if test="${param.formato ne 'xls'}">
            <link href="<c:url value='/recursos/css/reporte.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />

            <jsp:include flush="true" page="/recursos/jquery/index.jsp?css=false" />
            <script type="text/javascript" src="<c:url value='/recursos/js/reporte.js'/>"></script>
        </c:if>

        <% java.util.Date hoy = new java.util.Date();
			        Calendar c = Calendar.getInstance();
			        c.setTime(new java.util.Date()); 
			        c.add(Calendar.HOUR, -5);
			        
			        hoy = c.getTime();
			        
                    pageContext.setAttribute("hoy", hoy);
                    String codUGEL = request.getParameter("codUGEL");
                    boolean esDRE = codUGEL.endsWith("00")
                            || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_CALLAO)
                            || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_LIMA_METRO)
                            || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_LIMA_PROV);
                    pageContext.setAttribute("esDRE", esDRE);
                    pageContext.setAttribute("codUGEL", codUGEL);
        %>


    </head>
    <body>    
        <div id="body">
            <h1></h1>
            <c:choose>
                <c:when test="${TipoConsulta eq 'CENSO-MATRICULA'||TipoConsulta eq 'CENSO-RESULTADO'||TipoConsulta eq 'EVALUACION-SIAGIE' || TipoConsulta eq 'CENSO-RESULTADO-RECUPERACION'}">
                    <c:if test="${TipoConsulta eq 'CENSO-MATRICULA'}">
                        <h2 class="pre">Matr&iacute;cula, Docentes, Recursos</h2>
                    </c:if>
                    <c:if test="${TipoConsulta eq 'CENSO-RESULTADO'}">
                        <h2 class="pre">Resultado del ejercicio</h2>
                    </c:if>
                    <c:if test="${TipoConsulta eq 'EVALUACION-SIAGIE'}">
                        <h2 class="pre">Evaluaci&oacute;n del SIAGIE</h2>
                    </c:if>
                    <c:if test="${TipoConsulta eq 'CENSO-RESULTADO-RECUPERACION'}">
                        <h2 class="pre">Resultado del ejercicio - Recuperaci&oacute;n</h2>
                    </c:if>

                    <c:if test="${esDRE}">
                        <h2>C�digo DRE: ${codUGEL}</h2>
                        <h3> S&oacute;lo se considera a las IIEE  de &aacute;mbito de administraci&oacute;n regional</h3>
                    </c:if>
                    <c:if test="${!esDRE}">
                        <h2>C&oacute;digo de UGEL: ${codUGEL}</h2>
                    </c:if>
                    <h3><fmt:formatDate value="${hoy}" pattern="dd-MM-yyyy" /></h3>
                </c:when>
                <c:when test="${TipoConsulta eq 'CENSO-LOCAL' || TipoConsulta eq 'FEN-COSTERO'}">
                    <c:if test="${TipoConsulta eq 'CENSO-LOCAL'}">
                        <h2 class="pre">Local Escolar</h2>
                    </c:if>
                    <c:if test="${TipoConsulta eq 'FEN-COSTERO'}">
                        <h2 class="pre">Evaluaci&oacute;n r&aacute;pida del nivel de afectaci&oacute;n por el fen&oacute;meno del ni�o (FEN) costero 2017</h2>
                    </c:if>
                    <c:if test="${esDRE}">
                        <h2>C&oacute;digo de DRE:${codUGEL}</h2>
                        <h3>S&oacute;lo se considera a los Locales  de &aacute;mbito de administraci&oacute;n regional</h3>
                    </c:if>
                    <c:if test="${!esDRE}">
                        <h2>C&oacute;digo de UGEL:${codUGEL}</h2>
                    </c:if>
                    <h3><fmt:formatDate value="${hoy}" pattern="dd-MM-yyyy" /></h3>
                </c:when>

                <c:when test="${TipoConsulta eq 'CENSO-ID'}">
                    <h2 class="pre">CEDULA ID</h2>
                    <c:if test="${esDRE}">
                        <h2>C&oacute;digo de DRE:${codUGEL}</h2>
                        <h3>S&oacute;lo se considera a las IIEE  de &oacute;mbito de administraci&oacute;n regional</h3>
                    </c:if>
                     <c:if test="${!esDRE}">
                        <h2>C&oacute;digo de UGEL:${codUGEL}</h2>
                    </c:if>
                    <h3><fmt:formatDate value="${hoy}" pattern="dd-MM-yyyy" /></h3>
                </c:when>
            </c:choose>

            <div id="cuadro">
                <c:choose>
                    <c:when test="${TipoConsulta eq 'CENSO-MATRICULA' || TipoConsulta eq 'CENSO-RESULTADO' || TipoConsulta eq 'FEN-COSTERO'}">
                        <h3 class="tituloCuadro">Listado de Servicios Educativos y Programas Omisos</h3>
                        <div align="left" style="color: Black; font-family: Arial; font: x-small; font-style: normal;">
                    		<strong>Cantidad:&nbsp; ${fn:length(resumen_matri_omisos)} &nbsp;Servicios Educativos y Programas Omisos</strong>
                		</div>
                    </c:when>
                    <c:when test="${TipoConsulta eq 'CENSO-LOCAL'}">
                        <h3 class="tituloCuadro">Listado de Locales Escolares Omisos</h3>
                        <div align="left" style="color: Black; font-family: Arial; font: x-small; font-style: normal;">
                    		<strong>Cantidad:&nbsp; ${fn:length(resumen_matri_omisos)} &nbsp; Locales Escolares Omisos</strong>
                		</div>
                    </c:when>
                    <%--
                    <c:when test="${TipoConsulta eq 'CENSO-ID'}">
                        <h3 class="tituloCuadro">Listado de Reporte Cedulas ID Omisas</h3>
                        <div align="left" style="color: Black; font-family: Arial; font: x-small; font-style: normal;">
                    		<strong>Cantidad:&nbsp; ${fn:length(resumen_matri_omisos)} &nbsp;</strong>
                		</div>
                    </c:when>
                        <c:when test="${TipoConsulta eq 'EVALUACION-SIAGIE'}">
                        <h3 class="tituloCuadro">Listado de Servicios Educativos y Programas Omisas</h3>
                        <div align="left" style="color: Black; font-family: Arial; font: x-small; font-style: normal;">
                    		<strong>Cantidad:&nbsp; ${fn:length(resumen_matri_omisos)} &nbsp;</strong>
                		</div>
                    </c:when>
                    <c:when test="${TipoConsulta eq 'FEN-COSTERO'}">
                        <h3 class="tituloCuadro">Listado de Locales Escolares Omisas</h3>
                        <div align="left" style="color: Black; font-family: Arial; font: x-small; font-style: normal;">
                    		<strong>Cantidad:&nbsp; ${fn:length(resumen_matri_omisos)} &nbsp;</strong>
                		</div>
                    </c:when>
                     --%>
                    <c:when test="${TipoConsulta eq 'CENSO-RESULTADO-RECUPERACION'}">
                        <h3 class="tituloCuadro">Listado de Servicios Educativos y Programas Omisos</h3>
                        <div align="left" style="color: Black; font-family: Arial; font: x-small; font-style: normal;">
                    		<strong>Cantidad:&nbsp; ${fn:length(resumen_matri_omisos)} &nbsp;Servicios Educativos y Programas Omisas</strong>
                		</div>
                    </c:when>
                </c:choose>

                <c:choose>
                    <c:when test="${TipoConsulta eq 'CENSO-MATRICULA' || TipoConsulta eq 'CENSO-RESULTADO' || TipoConsulta eq 'CENSO-RESULTADO-RECUPERACION' || TipoConsulta eq 'CENSO-ID' || TipoConsulta eq 'EVALUACION-SIAGIE'}">

                        <table cellpadding="5" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>C&oacute;digo Modular</th>
                                    <th>Anexo</th>
                                    <th>Nivel</th>
                                    <th>C&oacute;digo Local</th>
                                    <th>Instituci&oacute;n Educativa / Programa </th>
                                    <th>Forma</th>
                                    <th>Tipo de IIEE</th>
                                    <th>Gesti&oacute;n</th>
                                    <th>C&oacute;digo Geogr&aacute;fico</th>
                                    <th>Distrito</th>
                                    <th>Cent. Poblado</th>
                                    <c:if test="${FORMATO==1}">
                                        <th>Direcci&oacute;n</th>
                                        <th>Tel&eacute;fono</th>
                                    </c:if>


                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${resumen_matri_omisos}" var="fila">
                                    <tr>
                                        <td align="center">&emsp;${fila[0]}</td>
                                        <td align="center">${fila[1]}</td>
                                        <td align="left">${fila[2]}</td>
                                        <td align="center">${fila[4]}</td>
                                        <td align="left">${fila[5]}</td>
                                        <td align="center">${fila[6]}</td>
                                        <td align="cente">${fila[3]}</td>
                                        <td align="center">${fila[7]}</td>
                                        <td align="center">${fila[8]}</td>
                                        <td align="left">${fila[9]}</td>
                                        <td align="left">${fila[10]}</td>
                                        <c:if test="${FORMATO==1}">
                                            <td align="left">${fila[11]}</td>
                                            <td align="left">${fila[12]}</td>
                                        </c:if>

                                    </tr>


                                </c:forEach>

                            </tbody>
                        </table>
                    </c:when>
                    <c:when test="${TipoConsulta eq 'CENSO-LOCAL' || TipoConsulta eq 'FEN-COSTERO'}">


                        <table cellpadding="5" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>C&oacute;digo Local</th>
                                    <th>Instituci&oacute;n Educativa / Programa </th>
                                    <th>C&oacute;digo Geogr&aacute;fico</th>
                                    <th>Distrito</th>
                                    <th>Centro Poblado</th>
                                    <c:if test="${FORMATO==1}">
                                        <th>Direcci&oacute;n</th>
                                        <th>Tel&oacute;fono</th>
                                    </c:if>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${resumen_matri_omisos}" var="fila">
                                    <tr>
                                        <td align="center">&emsp;${fila[0]}</td>
                                        <td align="left">${fila[1]}</td>
                                        <td align="center">${fila[4]}</td>
                                        <td align="left">${fila[5]}</td>
                                        <td align="left">${fila[6]}</td>

                                        <c:if test="${FORMATO==1}">
                                            <td align="left">${fila[7]}</td>
                                            <td align="left">${fila[8]}</td>
                                        </c:if>
                                    </tr>


                                </c:forEach>

                            </tbody>
                        </table>

                    </c:when>
                </c:choose>

            </div>

        </div>

    </body>
</html>