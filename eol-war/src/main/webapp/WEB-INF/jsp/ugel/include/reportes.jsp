<%@page import="pe.gob.minedu.escale.eol.auth.domain.AuthUsuario"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.enumerations.RoleEnum"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.domain.act.Tablero"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<link href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />
<html:xhtml />

<% java.util.Date hoy = new java.util.Date();
AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
            pageContext.setAttribute("hoy", hoy);            
            pageContext.setAttribute("usuario",usuario.getUsuario() );

%>


<c:forEach items="${tableroEstd.mapActividades}" var="periodo">

    <c:if test="${periodo.key ge 2011 }">
        <div align="left" >
        <strong style="font:150% Arial,Helvetica,Verdana,sans-serif;color: black; font-size: 20px; left: auto">${periodo.key}</strong>
        </div>
        
        <div id="tableContainer" >
            <table  cellpadding="3" cellspacing="1" class="tabla"  width="98%">
               
        
                <tfoot>
                <tr>
                <td colspan="7" >
                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/resumen${periodo.key}.xls"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte consolidado del módulo Matrícula Docentes y Recursos ${periodo.key}</span></a>
                    </div>
                    
                    <%-- 
                    <div style="padding: 5px 15px 5px 0;">
                    <c:url var="urlResum2" value="/ugel/cedula11/resumen${periodo.key}.xls"/>
                    <a class="xls" href="${urlResum2}"><span style="font-size:8pt;">Reporte consolidado del módulo Local Escolar ${periodo.key}</span></a>
                    </div>
                                         
                    <div style="padding: 5px 15px 5px 0;" >
                        
                        <c:url var="url" value="/ugel/resultado/resumenResultado${periodo.key}.xls"/>
                        <a class="xls" href="${url}" <c:if test='${periodo.key eq "2019"}'>style="display: none;"</c:if>  ><span style="font-size:8pt;">Reporte consolidado del módulo Resultado ${periodo.key}</span></a>
                    </div>--%>
                </td>
                </tr>
                </tfoot>
                 
            </table>
        </div>
    </c:if>
    <br/>
</c:forEach>
