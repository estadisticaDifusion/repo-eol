<%@page import="pe.gob.minedu.escale.eol.portlets.domain.act.Tablero"%>
<%@page import="pe.gob.minedu.escale.eol.converter.EolActividadConverter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    Tablero tablero=(Tablero)request.getAttribute("tableroEstd");
        if(tablero.getRol().getCod()==3){
            pageContext.setAttribute("esDRE", true);
        }
    //EolActividadConverter act=(EolActividadConverter)request.getAttribute("ActividadMat");
    //String tipo=act.getNombre();
%>
<link href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />

<style type="text/css">
#cuadro_r{
    width:auto;
    margin: 0 auto  ;    
    padding-top: 5px;

}
#cuadro_r table{
    margin: 0 auto  ;
    border-top: 1px black solid;
    border-bottom:  1px black solid;
    font-size: 9pt;
    font-family:    Arial, Helvetica,sans-serif;
}
#cuadro_r table td{
    border-right:   1px black solid;
}
#cuadro_r table thead  th{
    border-bottom:  1px black solid;
    border-right:   1px black solid;
}
#cuadro_r table thead  th blockquote{
    margin: 0.5em;
}
#cuadro_r table tr td:last-child,#cuadro table tr th:last-child{
    border-right:   none;
}
#cuadro_r table tfoot  tr{
    font-weight: bold;
}

</style>
<div align="center">

    <!--strong style="position: relative;left: 0px;text-transform: uppercase;margin-top:10px;font: Bold 12px Verdana, 'Trebuchet MS', Sans-serif;color: #000000;padding: 5px 0 5px 25px;" -->

    <div>
        <table   cellpadding="0" cellspacing="0" border="0" width="450px" >

            <tr>
                <td align="center" style="background-color: navy; color: #ffffff;font-size: 10pt;font-family:    Arial, Helvetica,sans-serif; font-weight: bold; height: 20px ">

                    ${ActividadMat.descripcion} - ${ActividadMat.nombrePeriodo}
                </td>
            </tr>

            <tr>
                <td align="center" style="background-color: #0066cc; color: #ffffff;font-size: 9pt; font-family:    Arial, Helvetica,sans-serif;font-weight: bold;height: 20px  ">
                    <span class="pre1">Cobertura según UGEL</span>
                </td>
            </tr>
        </table>
    </div>

    <c:set var="totalIIEE" value="0" />
    <c:set var="totalMDR" value="0" />
    <c:set var="totalMDRPost" value="0" />
    <div id="cuadro_r">
        <table  width="450px"  cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">UGEL</th>
                    <th rowspan="2">Marco Censal</th>
                    <th colspan="2">Informantes hasta el <fmt:formatDate pattern="dd 'de' MMMM" value="${EsregHuelga ? ActividadMat.fechaTerminohuelga : (EsregSigied ? ActividadMat.fechaTerminosigied : ActividadMat.fechaTermino) }"/></th>
                    <%-- <th colspan="2">Informantes posterior al <fmt:formatDate pattern="dd 'de' MMMM" value="${EsregHuelga ? ActividadMat.fechaTerminohuelga : (EsregSigied ? ActividadMat.fechaTerminosigied : ActividadMat.fechaTermino) }"/></th> --%>
               		<th colspan="2">---</th>
                </tr>
                <tr>
                    <th>Informantes</th>
                    <th>%</th>
                    <th>Informantes</th>
                    <th>%</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${resumenCoberturaMat}" var="fila">
                    <tr>
                        <td>

                            <c:url var="url" value="/ugel/2011/resumenCobertura">
                                <c:param name="codUGEL" value="${fila.idUgel}"/>
                            </c:url>
                            <strong><a href="${url}" target="_blank">${fila.idUgel}</a></strong>
<!--                            <strong>${fila.idUgel}</strong>-->
                        </td>
                        <td align="center">${fila.cuentaCentros}</td>
                        <td align="center">${fila.cuentaEnvios}</td>
                        <td align="right">
                            <fmt:formatNumber  value="${ fila.cuentaCentros != 0 ? fila.cuentaEnvios / fila.cuentaCentros : 0 }" pattern="0.##%"/>
                        </td>
                        <td align="center">${fila.cuentaEnviosPost}</td>
                        <td align="right">
                            <fmt:formatNumber  value="${fila.cuentaCentros != 0 ? fila.cuentaEnviosPost / fila.cuentaCentros : 0}" pattern="0.##%"/>
                        </td>
                    </tr>
                    <c:set var="totalIIEE" value="${totalIIEE+fila.cuentaCentros}" />
                    <c:set var="totalMDR" value="${totalMDR+fila.cuentaEnvios}" />
                    <c:set var="totalMDRPost" value="${totalMDRPost+fila.cuentaEnviosPost}" />
                </c:forEach>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        Total
                    </td>
                    <td align="center">${totalIIEE}</td>
                    <td align="center">${totalMDR}</td>
                    <td align="right">
                        <fmt:formatNumber  value="${totalIIEE !=0 ? totalMDR/totalIIEE:0 }" pattern="0.##%"/>
                    </td>
                    <td align="center">${totalMDRPost}</td>
                    <td align="right">
                        <fmt:formatNumber  value="${totalIIEE !=0 ? totalMDRPost/totalIIEE:0 }" pattern="0.##%"/>
                    </td>
                </tr>
            </tfoot>

        </table>
    </div>
        <table border="0" width="350" cellpadding="0" cellspacing="0" style="font-size: 9pt;    font-family:    Arial, Helvetica,sans-serif;">
            <tr align="left" valign="top" >
                <td align="left">
                    <br>
                    <sub>*</sub>Incluye PRONOEIs
<!--                    <br>
                    <sub>*</sub>Para el cálculo de la cobertura se incluye a los servicios educativos que se reportaron por el SIGIED-->
                    <br><br><br>
                </td>
            </tr>
        </table>
</div>

<div align="center">

    <!--strong style="position: relative;left: 0px;text-transform: uppercase;margin-top:10px;font: Bold 12px Verdana, 'Trebuchet MS', Sans-serif;color: #000000;padding: 5px 0 5px 25px;" -->

    <div>
        <table   cellpadding="0" cellspacing="0" border="0" width="450px" >

            <tr>
                <td align="center" style="background-color: navy; color: #ffffff;font-size: 10pt;font-family:    Arial, Helvetica,sans-serif; font-weight: bold; height: 20px ">

                    ${ActividadLoc.descripcion} - ${ActividadLoc.nombrePeriodo}
                </td>
            </tr>

            <tr>
                <td align="center" style="background-color: #0066cc; color: #ffffff;font-size: 9pt; font-family:    Arial, Helvetica,sans-serif;font-weight: bold;height: 20px  ">
                    <span class="pre1">Cobertura según UGEL</span>
                </td>
            </tr>
        </table>
    </div>

    <c:set var="LtotalIIEE" value="0" />
    <c:set var="LtotalMDR" value="0" />
    <c:set var="LtotalMDRPost" value="0" />
    <div id="cuadro_r">
        <table  width="450px"  cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">UGEL</th>
                    <th rowspan="2">Marco Censal</th>
                    <th colspan="2">Informantes hasta el <fmt:formatDate pattern="dd 'de' MMMM" value="${EsregHuelga ? ActividadLoc.fechaTerminohuelga : (EsregSigied ? ActividadLoc.fechaTerminosigied : ActividadLoc.fechaTermino)}"/></th>
                   <%--  <th colspan="2">Informantes posterior al <fmt:formatDate pattern="dd 'de' MMMM" value="${EsregHuelga ? ActividadLoc.fechaTerminohuelga : (EsregSigied ? ActividadLoc.fechaTerminosigied : ActividadLoc.fechaTermino)}"/></th> --%>
                	 <th colspan="2">---</th>
                </tr>
                <tr>
                    <th>Informantes</th>
                    <th>%</th>
                    <th>Informantes</th>
                    <th>%</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${resumenCoberturaLoc}" var="fila">
                    <tr>
						<td align="center">${fila.idUgel}</td>
                        <td align="center">${fila.cuentaCentros}</td>
                        <td align="center">${fila.cuentaEnvios}</td>
                        <td align="right">
                            <fmt:formatNumber  value="${fila.cuentaCentros !=0 ? fila.cuentaEnvios / fila.cuentaCentros : 0}" pattern="0.##%"/>
                        </td>
                        <td align="center">${fila.cuentaEnviosPost}</td>
                        <td align="right">
                            <fmt:formatNumber  value="${fila.cuentaCentros !=0 ? fila.cuentaEnviosPost / fila.cuentaCentros : 0}" pattern="0.##%"/>
                        </td>
                    </tr>
                    <c:set var="LtotalIIEE" value="${LtotalIIEE+fila.cuentaCentros}" />
                    <c:set var="LtotalMDR" value="${LtotalMDR+fila.cuentaEnvios}" />
                    <c:set var="LtotalMDRPost" value="${LtotalMDRPost+fila.cuentaEnviosPost}" />
                </c:forEach>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        Total
                    </td>
                    <td align="center">${LtotalIIEE}</td>
                    <td align="center">${LtotalMDR}</td>
                    <td align="right">
                        <fmt:formatNumber  value="${LtotalIIEE != 0 ? LtotalMDR/LtotalIIEE : 0}" pattern="0.##%"/>
                    </td>
                    <td align="center">${LtotalMDRPost}</td>
                    <td align="right">
                        <fmt:formatNumber  value="${LtotalIIEE != 0 ? LtotalMDRPost/LtotalIIEE : 0}" pattern="0.##%"/>
                    </td>
                </tr>
            </tfoot>

        </table>
    </div>
        <table border="0" width="350" cellpadding="0" cellspacing="0" style="font-size: 9pt;    font-family:    Arial, Helvetica,sans-serif;">
            <tr align="left" valign="top" >
                <td align="left">
<!--                    <p><sub>*</sub>Para el cálculo de la cobertura se incluye a los locales educativos que se reportaron por el SIGIED</p>-->
                </td>
            </tr>
        </table>

</div>

<%-- Comentado por RESULTADOS 2017 --%>
<c:if test='${resumenCoberturaRest != null}'>

<div align="center" style="margin-top: 30px;">

    <div>
        <table   cellpadding="0" cellspacing="0" border="0" width="350px" >

            <tr>
                <td align="center" style="background-color: navy; color: #ffffff;font-size: 10pt;font-family:    Arial, Helvetica,sans-serif; font-weight: bold; height: 20px ">

                    ${ActividadRest.descripcion}
                </td>
            </tr>

            <tr>
                <td align="center" style="background-color: #0066cc; color: #ffffff;font-size: 9pt; font-family:    Arial, Helvetica,sans-serif;font-weight: bold;height: 20px  ">
                    <span class="pre1">Cobertura según UGEL</span>
                </td>
            </tr>
        </table>
    </div>

    <c:set var="RtotalIIEE" value="0" />
    <c:set var="RtotalMDR" value="0" />
    <div id="cuadro_r">
        <table  width="350px"  cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <th>UGEL</th>
                    <th>Marco Censal</th>
                    <th>Informantes</th>
                    <th>%</th>

                </tr>
            </thead>
            <tbody>
                <c:forEach items="${resumenCoberturaRest}" var="fila">
                    <tr>
			<td align="center">${fila.idUgel}</td>
                        <td align="center">${fila.cuentaCentros}</td>
                        <td align="center">${fila.cuentaEnvios}</td>
                        <td align="right">
                            <fmt:formatNumber  value="${fila.cuentaEnvios / fila.cuentaCentros}" pattern="0.##%"/>
                        </td>
                    </tr>
                    <c:set var="RtotalIIEE" value="${RtotalIIEE+fila.cuentaCentros}" />
                    <c:set var="RtotalMDR" value="${RtotalMDR+fila.cuentaEnvios}" />
                </c:forEach>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        Total
                    </td>
                    <td align="center">${RtotalIIEE}</td>
                    <td align="center">${RtotalMDR}</td>
                    <td align="right">
                        <fmt:formatNumber  value="${RtotalMDR/RtotalIIEE  }" pattern="0.##%"/>
                    </td>
                </tr>
            </tfoot>

        </table>
    </div>
        <table border="0" width="350" cellpadding="0" cellspacing="0" style="font-size: 9pt;    font-family:    Arial, Helvetica,sans-serif;">
            <tr align="left" valign="top" >
                <td align="left">
                    <c:if test='${ActividadRest.nombre == "CENSO-MATRICULA" || ActividadRest.nombre == "CENSO-RESULTADO"}'>
                        <p><sub>*</sub>incluye PRONOEIs </p>
                    </c:if>
                </td>
            </tr>
        </table>
</div>

</c:if>
