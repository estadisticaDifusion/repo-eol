<%@page import="pe.gob.minedu.escale.eol.auth.domain.AuthUsuario"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.enumerations.RoleEnum"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.domain.act.Tablero"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<fmt:setBundle basename="messages" var="msg" />
<link href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />
<html:xhtml />

<% java.util.Date hoy = new java.util.Date();
AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
            pageContext.setAttribute("hoy", hoy);            
            pageContext.setAttribute("usuario",usuario.getUsuario() );
%>

<c:forEach items="${tableroEstd.mapActividades}" var="periodo">

    <c:if test="${periodo.key ge 2011 }">
        <div align="left" >
        <strong style="font:150% Arial,Helvetica,Verdana,sans-serif;color: black; font-size: 20px; left: auto">${periodo.key}</strong>
        </div>
        
        <div id="tableContainer" >
            <table  cellpadding="3" cellspacing="1" class="tabla"  width="98%" border="1" bordercolor="#4297D7">
                <thead>
                    <tr>
                        <th width="30%"><fmt:message bundle="${msg}" key="tablero.ugel.tab_resumen.actividad"/></th>
                        <th width="30%"><fmt:message bundle="${msg}" key="tablero.ugel.tab_resumen.plazo"/></th>
                        <%--<th><fmt:message bundle="${msg}" key="tablero.ugel.tab_resumen.formato"/></th>--%>
                        <%--<th><fmt:message bundle="${msg}" key="tablero.ugel.tab_resumen.cobertura"/></th>--%>
                        <th width="20%"><fmt:message bundle="${msg}" key="tablero.ugel.tab_resumen.situacion"/></th>
                        <th width="20%"><fmt:message bundle="${msg}" key="tablero.ugel.tab_resumen.omisas"/></th>
                        <th width="20%"><fmt:message bundle="${msg}" key="tablero.ugel.tab_resumen.observados"/></th>
                        <%--<th>Archivo</th>--%>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${periodo.value}" var="actividad">
                        <tr>                            
                            <td width="150" height="50" align="left" >
                                <%--<c:choose>
                                    <c:when test="${hoy ge actividad.fechaInicio}">--%>
                                        <c:choose>
                                            <c:when test="${actividad.id eq ActividadActual.id}">
                                                <%--<html:link styleClass="act_sel" action="/ugel/inicio?idActividad=${actividad.id}">
                                                        ${actividad.descripcion}
                                                </html:link>--%>
                                                <div style="padding: 5px 10px">
                                                    ${actividad.descripcion}
                                                </div>
                                                <%--c:url var="urlAct" value="/ugel/tablero">
                                                    <c:param name="idActividad" value="${actividad.id}"/>
                                                </c:url--%>
                                                <!--a class="act_sel" href="{urlAct}">{actividad.descripcion}</a-->
                                                
                                                 <span style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;">
                                                   (<fmt:message bundle="${msg}" key="actividad.estado.${actividad.estadoActividad}"/>)
                                                 </span>
                                                 
                                            </c:when>
                                            <c:otherwise>
                                                <%--c:url var="urlAct" value="/ugel/tablero">
                                                    <c:param name="idActividad" value="${actividad.id}"/>
                                                </c:url>
                                                <a class="act" href="${urlAct}">${actividad.descripcion}</a--%>

                                                <%--<html:link styleClass="act" action="/ugel/inicio?idActividad=${actividad.id}">
                                                        ${actividad.descripcion}
                                                </html:link>--%>
                                                <div style="padding: 5px 10px">
                                                ${actividad.descripcion}</div>
                                                 <span style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;">
                                                     <%--(<fmt:message bundle="${msg}" key="actividad.estado.${actividad.estadoActividad}"/>)--%>
                                                     <%--(<fmt:message bundle="${msg}" key="actividad.estado.${EsregHuelga ? actividad.estadoActividadhuelga : (EsregSigied ? actividad.estadoActividadsigied : actividad.estadoActividad) }"/>)--%>
                                                     (<fmt:message bundle="${msg}" key="actividad.estado.${EsregHuelga ? actividad.estadoActividadhuelga : (EsregSigied ? (actividad.estadoActividadsigied eq 4?5:(actividad.estadoActividadsigied)) : (actividad.estadoActividad eq 4?5:(actividad.estadoActividad))) }"/>)
                                                    <%--(<fmt:message bundle="${msg}" key="actividad.estado.${actividad.estadoActividad}"/>)--%>
                                                 </span>
                                            </c:otherwise>
                                        </c:choose>

                                <%--</c:when>
                                    <c:otherwise>
                                        <div style="padding: 5px 10px">
                                        <font style="" >${actividad.descripcion} NO INICIADO</font></div>
                                    </c:otherwise>
                                </c:choose>--%>
                            </td>
                            <td width="120" align="center" >
                                <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                    <fmt:formatDate pattern="dd MMM yyyy" value="${actividad.fechaInicio}"/>
                                </font>
                                -
                                <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif"><fmt:formatDate pattern="dd MMM yyyy" value="${EsregHuelga ? actividad.fechaTerminohuelga : (EsregSigied ? actividad.fechaTerminosigied : actividad.fechaTermino)}"/>
                                </font>
                            </td>
                            <%--<td  align="center" valign="center">
                              -
                            </td>--%>
                            <%--<td align="center">
                                <c:choose>
                                    <c:when test="${hoy ge actividad.fechaInicio  and actividad.urlCobertura ne '' and actividad.estadoCobertura }">
                                        <c:url var="url" value="/ugel/2011/resumenCobertura">
                                            <c:param name="codUGEL" value="${usuario}"/>
                                            <%--c:choose>
                                                <c:when test="${esDRE}">
                                                    <c:param name="codDRE" value="${codUGEL}"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:param name="codUGEL" value="${codUGEL}"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:url>
                                        <a href="${url}" target="_blank">Obtener</a>

                                    </c:when>
                                    <c:otherwise>
                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif; color: Gray;padding-right:.5em;">
                                            -
                                        </span>
                                    </c:otherwise>
                                </c:choose>

                            </td>--%>
                            <td align="center">
                                <c:choose>
                                    <c:when test="${hoy ge actividad.fechaInicio and actividad.estadoSituacion}">
                                        <c:if test="${not empty actividad.urlSituacion}">
                                        <c:url var="url" value="${actividad.urlSituacion}">                                            
                                                <c:param name="tipo" value="${actividad.nombre}"/>
                                                <c:param name="anio" value="${periodo.key}"/>
                                                <c:param name="codUGEL" value="${usuario}"/>

                                        </c:url>
                                        <a href="${url}" target="_blank">Obtener</a>
                                        </c:if>
                                    </c:when>
                                    <c:when test="${hoy le actividad.fechaInicio}">
                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif; color: Gray;padding-right:.5em;">
                                            Obtener
                                        </span>
                                    </c:when>
                                </c:choose>

                            </td>
                            <td align="center">
                                <c:choose>
                                    <c:when test="${hoy ge actividad.fechaInicio and actividad.estadoOmisos}">
                                        <c:if test="${ not empty actividad.urlOmisos}">
                                        <c:url var="url" value="${actividad.urlOmisos}">
                                                <c:param name="tipo" value="${actividad.nombre}"/>
                                                <c:param name="anio" value="${periodo.key}"/>
                                                <c:param name="codUGEL" value="${usuario}"/>
                                        </c:url>
                                        <a href="${url}" target="_blank">Obtener</a>
                                        </c:if>

                                    </c:when>
                                    <c:when test="${hoy le actividad.fechaInicio}">
                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif; color: Gray;padding-right:.5em;">
                                            Obtener
                                        </span>
                                    </c:when>
                                </c:choose>

                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${hoy ge actividad.fechaInicio and actividad.estadoOmisos}">
                                        <c:if test="${ not empty actividad.urlSituacion and (periodo.key eq 2017 or periodo.key eq 2018) and actividad.nombre eq 'CENSO-RESULTADO'}">
                                        <c:url var="url" value="/ugel/2015/resumenObservado">
                                                <c:param name="tipo" value="${actividad.nombre}"/>
                                                <c:param name="anio" value="${periodo.key}"/>
                                                <c:param name="codUGEL" value="${usuario}"/>
                                        </c:url>
                                        <a href="${url}" target="_blank">Obtener</a>
                                        </c:if>

                                    </c:when>
                                    <c:when test="${hoy le actividad.fechaInicio}">
                                        <span  style="font:100% Arial,Helvetica,Verdana,sans-serif; color: Gray;padding-right:.5em;">
                                            Obtener
                                        </span>
                                    </c:when>
                                </c:choose>

                            </td>
                            <%--<td>
                                <c:if test="${hoy ge actividad.fechaInicio and actividad.estadoSie}">
                                 <c:url var="urlSIE" value="/SieServlet">
                                    <c:param name="idActividad" value="${actividad.id}"/>
                                </c:url>
                                    <a class="archSIE" target="_black" href="${urlSIE}">Data</a>
                                </c:if>
                            </td>--%>
                        </tr>
                    </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                <%--td colspan="7" >
                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/resumen${periodo.key}.xls"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte consolidado del módulo Matrícula Docentes y Recursos ${periodo.key}</span></a>
                    </div>
                    
                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum2" value="/ugel/cedula11/resumen${periodo.key}.xls"/>
                    <a class="xls" href="${urlResum2}"><span style="font-size:8pt;">Reporte consolidado del módulo Local Escolar ${periodo.key}</span></a>
                    </div>
                    
                    <div style="padding: 5px 15px 5px 0 ">
                        <c:url var="url" value="/ugel/resultado/resumenResultado${periodo.key}.xls"/>
                        <a class="xls" href="${url}"><span style="font-size:8pt;">Reporte consolidado del módulo Resultado ${periodo.key}</span></a>
                    </div>
                    
                </td--%>
                </tr>
                </tfoot>
                 
            </table>
        </div>
    </c:if>
    <br/>
</c:forEach>
