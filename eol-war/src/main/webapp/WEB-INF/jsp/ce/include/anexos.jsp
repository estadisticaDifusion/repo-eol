<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html:xhtml />

<div align="left" style="position: relative;left: 17px"  >
    <c:if test="${fn:length(CENTROS_EDUCATIVOS)>1}">
        <c:forEach items="${CENTROS_EDUCATIVOS}" var="ie">            
            <c:choose>
                <c:when test="${ie.anexo eq tablero.ce.anexo && ie.codigoModular eq tablero.ce.codigoModular && ie.nivelModalidad.idCodigo eq tablero.ce.nivelModalidad.idCodigo }">
                    <strong style="margin-right:.5em;padding-right:.5em; color: #bbbb;font-family:Arial,Helvetica,Verdana,sans-serif;">

                        <c:if test="${ie.anexo eq '0'}">
                            Principal
                        </c:if>
                        <c:if test="${ie.anexo ne '0'}">
                            Anexo (${ie.anexo})
                        </c:if>
                        
                    </strong>
                </c:when>
                <c:otherwise>
                    <span class="top-link last">
                        <c:url var="url" value="/ce/tablero">
                             <c:param name="ie_cod_mod" value="${ie.codigoModular}" />
                             <c:param  name="ie_anexo" value="${ie.anexo}"/>
                             <c:param  name="ie_niv_mod" value="${ie.nivelModalidad.idCodigo}" />
                         </c:url>
                        <%--a  href="<portlet:renderURL>
                                <portlet:param name="ie_cod_mod" value="${ie.codigoModular}" />
                                <portlet:param name="ie_anexo" value="${ie.anexo}" />
                                <portlet:param name="ie_niv_mod" value="${ie.nivelModalidad.idCodigo}" />
                                </portlet:renderURL>"--%>
                        <a href="${url}" >
                            <c:if test="${ie.anexo eq '0'}">
                                <span style="font-family:Arial,Helvetica,Verdana,sans-serif; text-decoration:underline">Principal</span>
                            </c:if>
                            <c:if test="${ie.anexo ne '0'}">
                                <span style="font-family:Arial,Helvetica,Verdana,sans-serif; text-decoration:underline">Anexo (${ie.anexo})</span>
                            </c:if>
                        </a>
                        
                    </span>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </c:if>
</div>


