<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="pe.gob.minedu.escale.padron.domain.CampoComparado"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html:html xhtml="true">
    <head>
        <link href="<html:rewrite page='/recursos/css/padron_import.css'/>" rel="stylesheet" rev="stylesheet" type="text/css"/>

        <script type='text/javascript' src='<html:rewrite page="/dwr/interface/ImportarMonitor.js"/>'></script>
        <script type='text/javascript' src='<html:rewrite page="/dwr/engine.js"/>'></script>

        <script type='text/javascript' src='<html:rewrite page="/dwr/util.js"/>'></script>

        <script type="text/javascript">
            function inicioCarga(){
                DWRUtil.setValue("progreso","0%");
                setTimeout(actualizarProgreso,5000);
            }
            function actualizarProgreso(){
                setTimeout(actualizarProgreso,5000);
                ImportarMonitor.getEstadoImportar(mostrarProgreso);
            }
            function mostrarProgreso(data){
                var actual=Math.round( (data.valorActual / data.valorMaximo *1.0)*100.00);
                DWRUtil.setValue("progreso",actual+"%");
            }
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Comparando campos</title>
        <style type="text/css">
            td.falta{
                font-weight: bold;
                color:red;
            }
        </style>
    </head>
    <body>
        <h1>Actualizando Padr&oacute;n</h1>
        <h2>Comparando campos</h2>
        <%boolean noFalta = true;
        %>
        <table cellpadding="5" cellspacing="1">
            <thead>
                <tr>
                    <th>Campo DBF</th>
                    <th>Campo de Tabla</th>
                </tr>
            </thead>
            <tbody>
                <logic:iterate id="campo" name="camposComparados"
                               type="CampoComparado">
                    <%
                                noFalta = noFalta && !campo.getDb().startsWith("FALTA") && !campo.getDbf().startsWith("FALTA");
                    %>
                    <tr>
                        <td ${fn:startsWith(campo.dbf,"FALTA")?"class='falta'":""}>${campo.dbf
                            }</td>
                        <td ${fn:startsWith(campo.db,"FALTA")?"class='falta'":""}>${campo.db
                            }</td>
                    </tr>
                </logic:iterate>
            </tbody>
        </table>
        <form action="<html:rewrite page='/ImportarRegistrosServlet' />" method="post">
            <input <%=noFalta ? "" : "disabled='disabled'"%> type="submit"
                                                         value="Importar" />
            <div id="progreso"></div>
        </form>
    </body>
</html:html>
