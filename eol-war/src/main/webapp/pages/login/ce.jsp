<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<%@ page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<html:xhtml />
<bean:define id="url">
	<%=session.getAttribute(Constantes.URL_FORWARD)%>
</bean:define>


<bean:define id="url_import" value="/pages/inicioCE.jsp" />
<bean:define id="titulo">
        <bean:message key="ce.loginV1" />
</bean:define>

<logic:equal value="/ce/censo/resumen.uee" name="url">
	<bean:define id="url_import" value="/pages/login/ce_censo.html" />
	<bean:define id="titulo">
		<bean:message key="censo.titulo" />
	</bean:define>
</logic:equal>

<h1>${titulo}</h1>

<%-- SECCION REUBICADA AL DEFAULT 
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/aes.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/js/accesoce.js'/>"></script>
--%>

<html:errors />
<div class="marco_ce"><jsp:include flush="true"
	page="${url_import}" />


<p>El tablero de control es una herramienta Web que informa gráficamente sobre la situación de la I.E. en el reporte de datos de cada módulo del CENSO ESCOLAR, y permite descargar el formato electrónico respectivo, obtener una constancia, o modificar los datos reportados.</p>

<p><strong>Acceda a su Tablero de Control completando los siguientes datos:</strong></p>
<html:form action="/login/ce/login">

    <table id="tbluser">
		<tr>
			<td><label for="usuario"> <bean:message key="ce.usuario" />
			</label></td>
			<td><html:text styleId="usuario" property="usuario" maxlength="16" /></td>
		</tr>
		<tr>
			<td><label for="contrasenia"> <bean:message
				key="ce.contrasenia" /> </label></td>
			<td><html:password styleId="contrasenia" property="contrasenia" maxlength="16" />
			                <!-- cym-ls-va -->
			</td>
		</tr>
                <tr>
                    <td></td>
                    <td>
                        <div id="ReCaptchContainer" class="g-recaptcha" data-sitekey="6LeBbGMUAAAAADIULF6PKF-SFlA2jovbpcpMsE-u" data-callback="reCaptchaCallback"></div>
                        <label id="lblMessage" ></label>
                    </td>
                </tr>
                <tr>
			<td></td>
			<td><html:submit styleClass="button">
		<bean:message key="login.entrar" />
	</html:submit></td>
		</tr>
		<%--   <tr valign="top">
      <td>
        <label for="desafio">
          <bean:message key="captcha.desafio"/>
        </label>
      </td>
      <td>
        <html:text property="desafio" maxlength="desafio"/>
        <img src="<html:rewrite page="/login/captcha"/>" alt="Por seguridad, escriba el texto que vea en esta imagen"/>
      </td>
    </tr>--%>
	</table>
    <br/>
<!--
        <div id="div-wait" style="display: none;"><img src="<html:rewrite page="/resources/images/indicator.gif"/>"/> <strong> <label id="lblcargadatos" style="color: red; font-weight: bold;">Por favor espere mientras se realiza la carga de datos...</label> </strong></div>
-->         	
    <p><strong> <label id="lblcargadatos" style="display: none; color: red; font-weight: bold;" >Por favor espere mientras se realiza la carga de datos...</label> </strong></p>

<br/>
<p><strong> Si a&uacute;n no tiene su clave EOL,
solic&iacute;tela al <em>Estad&iacute;stico de su UGEL.</em> </strong></p>


</html:form>
<!--<script src="https://www.google.com/recaptcha/api.js?onload=renderRecaptcha&render=explicit" async defer></script>-->
<%-- SECCION REUBICADA AL DEFAULT 
<script type="text/javascript" src="<c:url value='/recursos/js/captchacallce.js'/>"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
--%>
</div>
