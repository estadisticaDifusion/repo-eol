<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<p><strong>Si usted es un Estad&iacute;stico de UGEL,</strong> aqu&iacute; podr&aacute;:</p>
<ul style=" padding-left: 10px">
   
    <li><span>Administrar las claves EOL (<strong>E</strong>stad&iacute;stica <strong>O</strong>n-<strong>L</strong>ine)
	para que los directores de Instituciones Educativas puedan acceder a
	este Sistema.</span></li>

        <li><span>Monitorear  los envíos            
            de los formatos del Censo Educativo que han realizado los Directores de las Instituciones Educativas de su jurisdicción.
            </span></li>

        

            <%--<li><span>Revisar el progreso del <em>Piloto de
	Captura de Datos por Alumno</em></span></li> --%>


        <li><span>Consultar en l&iacute;nea sus estad&iacute;sticas
	educativas de su jurisdicci&oacute;n.</span></li>
</ul>
