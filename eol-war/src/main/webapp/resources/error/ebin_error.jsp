<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
<head>
<title><bean:message key="cedula.encabezado.titulo.min" /> <bean:message
	key="cedula.encabezado.anioproceso" /></title>
<link rel="stylesheet"
	href="<html:rewrite page="/resources/css/Refresh.css"/>"
	type="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="<html:rewrite page='/resources/css/ebin.css'/>"
	rel="stylesheet" rev="stylesheet" type="text/css" />
</head>
<body bgcolor="#ffffff">
<br />
<br />
<br />
Por favor haga click en el bot&oacute;n
<strong>"<bean:message key="botones.cerrar" />"</strong>
<br />
<html:button property="cerrar" styleClass="button"
	onclick="javascript:window.close()">
	<bean:message key="botones.cerrar" />
</html:button>
</body>
</html>
