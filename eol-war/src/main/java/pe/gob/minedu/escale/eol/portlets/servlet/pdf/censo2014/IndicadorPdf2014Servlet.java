/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2014;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2014.*;
import pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2019.ConstanciaPdfMatricula2019Servlet;
import pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2014.*;

import java.io.FileInputStream;
//import pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2011.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

/**
 *
 * @author DSILVA
 */
public class IndicadorPdf2014Servlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
    
	private Logger logger = Logger.getLogger(IndicadorPdf2014Servlet.class);
	
	@Resource(name = "eol")
    private DataSource eol;
	
	@EJB
	private EnvioDocumentosFacade envioDocumentosFacade;
	
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	logger.info(":: IndicadorPdf2014Servlet.processRequest :: Starting execution...");
    	
        Connection conn = null;
        try {
        	logger.info("value eol="+eol);
            conn = eol.getConnection();
            logger.info("value conn="+conn);
//            String idEnvio= request.getParameter("idEnvio");
            String nivMod = request.getParameter("nivel");//cEdu.getNivMod();
            String codMod = request.getParameter("codMod");
            String variable = request.getParameter("variable");//cEdu.getNivMod();

            //String nroCed = Funciones.cedulaNivModResultado(nivMod);
            boolean existChar = false;
            List<String> reqParams = new ArrayList<String>();
            reqParams.add(nivMod);
            reqParams.add(codMod);
            reqParams.add(variable);

            existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

            if(!existChar){
            	logger.info("EXISTE INJECCION : "  +  existChar);
            	Map<String, Object> params = new HashMap<String, Object>();
                params.put("COD_MOD", codMod);
                params.put("ID_VARIABLE", variable); 
                InputStream is = null;
                InputStream isAdv = null;
                
                String urlMaster ;
                
                MessageResources res = MessageResources.getMessageResources("eol");
                String rutaRaiz = res.getMessage("eol.path.constancia");
                
                logger.info("params="+params);
                String report = rutaRaiz+"censo2014/indicador/";
                
                //logger.info("SUBREPORT_DIR="+report);
                //params.put("SUBREPORT_DIR", report);

                //urlMaster = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2014/indicador/mensaje.jasper");

                if (nivMod.equalsIgnoreCase("A1") || nivMod.equalsIgnoreCase("A2") || nivMod.equalsIgnoreCase("A3") || nivMod.equalsIgnoreCase("B0") || nivMod.equalsIgnoreCase("F0")) {
                    //urlMaster = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2014/indicador/indicador.jasper");

                    
                	urlMaster = rutaRaiz+"censo2014/indicador/indicador.jasper";
          
                	logger.info("INDICADOR JASPER : "+ urlMaster);
                } else {
                    //urlMaster = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2014/indicador/mensaje.jasper");
                    urlMaster = rutaRaiz+"censo2014/indicador/mensaje.jasper";
                    logger.info("INDICADOR JASPER 0000: "+ urlMaster);
                }
               
                
                is = new FileInputStream(urlMaster);
                
                JasperReport masterReport = (JasperReport) JRLoader.loadObject(is);

                String logoescale = rutaRaiz+"escale.png";
                String logominedu = rutaRaiz+"logo_med.png";
                params.put("LOG_ESC", logoescale);
                params.put("LOG_MIN", logominedu);
                
                
                //-- verificacion de existencia de data  
                List<Object[]> listIndicador= envioDocumentosFacade.verificaIndicadorxCodModxVariable(codMod , variable);
                logger.info("INICIOOOOO : "  + listIndicador.size() );
                if(listIndicador!=null && listIndicador.size()> 0){
           
                    byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
                    logger.info("BUFFER DE PDF :  " + buffer );
                    response.setContentType("application/pdf");
                    response.setHeader("Content-Disposition", "inline; filename=\"indicadores2019.pdf\"");
                    response.setContentLength(buffer.length);
                    ServletOutputStream ouputStream = response.getOutputStream();
                    ouputStream.write(buffer, 0, buffer.length);
                    ouputStream.flush();
                    ouputStream.close();
                }else{
                	urlMaster = rutaRaiz+"censo2014/indicador/notificacion.jasper";
                    isAdv=new FileInputStream(urlMaster);
                    logger.info("VACIOOOOOOOOOOOOOOOOOOOO ");
                    JasperReport masterReportAdv = (JasperReport) JRLoader.loadObject(isAdv);
                    
                	
                	   byte[] buffer = JasperRunManager.runReportToPdf(masterReportAdv, null, conn);
                       logger.info("BUFFER DE PDF :  " + buffer );
                       response.setContentType("application/pdf");
                       response.setHeader("Content-Disposition", "inline; filename=\"indicadores2019.pdf\"");
                       response.setContentLength(buffer.length);
                       ServletOutputStream ouputStream = response.getOutputStream();
                       ouputStream.write(buffer, 0, buffer.length);
                       ouputStream.flush();
                       ouputStream.close();
                	
                }
         

            }else{
            	logger.info("EXISTE INJECCION 0000: "  +  existChar);
            	RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/recursos/error/errorsql.jsp");
                requestDispatcher.include(request, response);
            }

            
        } catch (SQLException ex) {
            log(ex.getMessage(), ex);
        } catch (JRException ex) {
            log(ex.getMessage(), ex);
        } catch (Exception ex) {
            log(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                try {
                    if (!conn.isClosed()) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    log(ex.getMessage(), ex);
                }
            }
        }
        
        logger.info(":: IndicadorPdf2014Servlet.processRequest :: Execution finish.");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
