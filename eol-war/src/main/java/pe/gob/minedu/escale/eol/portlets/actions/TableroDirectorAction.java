/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.converter.EolCedulaConverter;
import pe.gob.minedu.escale.eol.converter.EolIECensoConverter;
import pe.gob.minedu.escale.eol.estadistica.domain.EolIndicadorCifras;
import pe.gob.minedu.escale.eol.estadistica.domain.EolIndicadorVariable;
import pe.gob.minedu.escale.eol.estadistica.dto.InstitucionDTO;
import pe.gob.minedu.escale.eol.estadistica.ejb.EolCifrasFacade;
import pe.gob.minedu.escale.eol.estadistica.ejb.EolIndicadorFacade;
import pe.gob.minedu.escale.eol.portlets.ejb.CensoLocal;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;
import pe.gob.minedu.escale.rest.client.EolCedulasClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativaIE;

/**
 *
 * @author JMATAMOROS
 */
public class TableroDirectorAction extends Action {

    private Logger logger = Logger.getLogger(TableroDirectorAction.class);
    
    static final String PERIODO = "2019";

    //@EJB
    //private CensoFacade censoFacade;
	//@EJB(mappedName = "java:global/eol-war/CensoFacade!pe.gob.minedu.escale.eol.portlets.ejb.CensoLocal")
	//private CensoLocal censoFacade;
    
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.info(":: TableroDirectorAction.execute :: Starting execution...");
        String codmod = null;
        String anexo = null;
        String nivel = null;
        String codlocal = null;
        String codinst = null;
        String mcenso = null;
        String sienvio = null;
        String estado = null;
        
        String caseie = request.getSession().getAttribute("CASEIE").toString();
        logger.info("  CASEIE = " + caseie);
        MessageResources res = MessageResources.getMessageResources("ApplicationResources");
        AuthUsuario usuario = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);
        InstitucionDTO institucionDTO = null;
        
        if(caseie.equals("first")) {
        	
        	institucionDTO = (InstitucionDTO) request.getSession().getAttribute(Constantes.LOGGED_INSTITUCION);
        	logger.info("INSTITUCION ESTADO EN EL PADRON : " + institucionDTO.getEstado() );
        	//request.setAttribute("EstadoPadron", institucionDTO.getEstado() );
            request.getSession().setAttribute("EstadoPadron", institucionDTO.getEstado());
        }if(caseie.equals("second")) {
        	codmod = request.getParameter("codmod");
            anexo = request.getParameter("anexo");
            logger.info("  LISTAIE = " + request.getSession().getAttribute("IIEES"));
        	List<InstitucionEducativaIE> listie = (List<InstitucionEducativaIE>) request.getSession().getAttribute("IIEES");
        	for (InstitucionEducativaIE ie : listie) {
				if(ie.getCodigoModular().equals(codmod) && ie.getAnexo().equals(anexo)) {
					institucionDTO = new InstitucionDTO();
					institucionDTO.setCodigoModular(ie.getCodigoModular());
					institucionDTO.setAnexo(ie.getAnexo());
					institucionDTO.setNivelModalidad(ie.getNivelModalidad().getIdCodigo());
					institucionDTO.setCodigoLocal(ie.getCodlocal());
					institucionDTO.setCodigoInstitucion(ie.getCodinst());
					institucionDTO.setMcenso(ie.getMcenso());
					institucionDTO.setSienvio(ie.getSienvio());
					institucionDTO.setEstado(ie.getEstado().getIdCodigo());
				}
			}
        	request.getSession().setAttribute(Constantes.LOGGED_INSTITUCION, institucionDTO);
        	
        	
        }if(caseie.equals("third")) {
        	
        }
        
        
        logger.info("  institucionDTO = " + institucionDTO);
        String sessionugel = (String) request.getSession().getAttribute("sessionugel");

        if (institucionDTO != null) {
            codmod = institucionDTO.getCodigoModular();
            anexo = institucionDTO.getAnexo();
            nivel = institucionDTO.getNivelModalidad();
            codlocal = institucionDTO.getCodigoLocal();
            codinst = institucionDTO.getCodigoInstitucion();
            mcenso = institucionDTO.getMcenso();
            sienvio = institucionDTO.getSienvio();
            estado = institucionDTO.getEstado();
        } else {
            codmod = request.getParameter("codmod");
            anexo = request.getParameter("anexo");
            nivel = request.getParameter("nivel");
            codlocal = request.getParameter("codlocal");
            codinst = request.getParameter("codinst");
            mcenso = request.getParameter("mcenso");
            sienvio = request.getParameter("sienvio");
            estado = request.getParameter("estado");
        }

        boolean existChar = false;
        List<String> reqParams = new ArrayList<String>();
        reqParams.add(codmod);
        reqParams.add(anexo);
        reqParams.add(nivel);
        reqParams.add(codlocal);
        reqParams.add(codinst);
        reqParams.add(mcenso);
        reqParams.add(sienvio);
        reqParams.add(estado);

        existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);
//        for (String param : reqParams) {
//            existChar = verificacionSqlInject(param);
//            if(existChar)
//                break;
//        }

        if (!existChar) {
            /*VERIFICACION DE LA CEDULA DE MATRICULA PARA EL NIVEL EN CURSO*/
            String preCed = "";
            //InputStream is = null;
            preCed = Funciones.cedulaMatricula2018NivMod(nivel);
            // "/WEB-INF/classes/pe/gob/minedu/escale/eol/portlets/plantilla/censo2018/matricula/CensoMatriculaDocentesRecursosCed" + preCed.toUpperCase() + "_" + PERIODO + ".xls";
            String nameFileTemplate = getProperty("eol.path.templates.xls") + "censo2019/matricula/CensoMatriculaDocentesRecursosCed" + preCed.toUpperCase() + "_" + PERIODO + ".xls";
            logger.info("  nameFileTemplate=" + nameFileTemplate);
            // String rutaRaiz = request.getServletContext().getRealPath(nameFileTemplate);
            // logger.info("  rutaRaiz="+rutaRaiz);

            File f = new File(nameFileTemplate);
            if (f.exists()) {
                try {
                    //is = new FileInputStream(nameFileTemplate);
                    request.setAttribute("ExistFile", "True");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                /*
                if (is != null) {
                    request.setAttribute("ExistFile", "True");
                }*/
            }

            Map<String, List<EolCedulaConverter>> mapAct = new HashMap<String, List<EolCedulaConverter>>();
            int anioInicio = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_INICIO));
            int anioFinal = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_FINAL));

            EolCedulasClient cedClient = new EolCedulasClient();

            //EolIEsCensoClient enviosClient = new EolIEsCensoClient();
            
            // Comments
            CensoLocal censoFacade = lookupCensoFacade();
            
            logger.info("censoFacade = " + censoFacade);
            
            //List<EolIECensoConverter> envios =enviosClient.getEnvios(nivel, codmod, anexo, null).getInstitucion();
            //List<EolIECensoConverter> enviosLocal =enviosClient.getEnvios(null, null, null, codlocal).getInstitucion();

            /*
            List<EolIECensoConverter> envios = censoFacade.cadCensoByCodmod(nivel, codmod, anexo);//enviosClient.getEnvios(nivel, codmod, anexo, null).getInstitucion();
            List<EolIECensoConverter> enviosLocal = censoFacade.cadCensoByCodLocal(codlocal);//new ArrayList();// =enviosClient.getEnvios(null, null, null, codlocal).getInstitucion();
            List<EolIECensoConverter> enviosId  = new ArrayList<EolIECensoConverter>();
            List<EolIECensoConverter> enviosCedS = censoFacade.cadCensoCedulaSByCodmod(nivel, codmod, anexo);
            List<EolIECensoConverter> enviosCedD = censoFacade.cadCedulaDCodLocal(codlocal);

            if (!codinst.isEmpty() && codinst!=null) {
                enviosId = censoFacade.cadCensoByCodIDNivMod(codinst,codmod,nivel);
            }

            if(envios==null){
                envios = new ArrayList<EolIECensoConverter>();
            }
            if(enviosLocal != null)
                envios.addAll(enviosLocal);
            if(enviosId != null)
                envios.addAll(enviosId);
            if(enviosCedS != null)
                envios.addAll(enviosCedS);
            if(enviosCedD != null)
                envios.addAll(enviosCedD);

             */

 /*if (envios != null && enviosLocal != null && enviosId != null ) {
                envios.addAll(enviosLocal);
                envios.addAll(enviosId);

            } else if (enviosLocal != null) {
                envios = enviosLocal;
            } else if (enviosId != null) {
                envios = enviosId;
            }*/
            //        if (envios != null && enviosLocal != null) {
            //            envios.addAll(enviosLocal);
            //        } else if (enviosLocal != null) {
            //            envios = enviosLocal;
            //        }
            List<EolIECensoConverter> envios = censoFacade.cadCensoTotalporCodigos(nivel, codmod, anexo, codlocal, codinst);

            if (envios == null) {
                envios = new ArrayList<EolIECensoConverter>();
            }

            if (envios != null) {
                Collections.sort(envios, buscarEnvio);
            }

            EolIECensoConverter act = new EolIECensoConverter();

            ///**anio *//
            /*String anio = new Integer(anioFinal).toString();
                System.out.println("NIVEL:" + nivel);
                System.out.println("ANIO:" + anio);
                List<EolCedulaConverter> cedulas = cedClient.getCedulasConverter(nivel, anio, null).getItems();

                for (EolCedulaConverter ced : cedulas) {
                    act.setIdActividad(ced.getActividad().getId());
                    if (envios != null) {
                        int pos = Collections.binarySearch(envios, act, buscarEnvio);
                        if (pos >= 0) {
                            EolIECensoConverter envio = envios.get(pos);
                            ced.setEnvio(true);
                            ced.setFechaEnvio(envio.getFechaEnvio());
                            ced.setNroEnvio(envio.getNroEnvio());
                        }
                    }
                }

                mapAct.put(anio, cedulas);

                String anio2 = new Integer(anioInicio).toString();
                System.out.println("NIVEL:" + nivel);
                System.out.println("ANIO:" + anio2);
                List<EolCedulaConverter> cedulas2 = cedClient.getCedulasConverter(nivel, anio2, null).getItems();

            for (EolCedulaConverter ced : cedulas2) {
                    act.setIdActividad(ced.getActividad().getId());
                    if (envios != null) {
                        int pos = Collections.binarySearch(envios, act, buscarEnvio);
                        if (pos >= 0) {
                            EolIECensoConverter envio = envios.get(pos);
                            ced.setEnvio(true);
                            ced.setFechaEnvio(envio.getFechaEnvio());
                            ced.setNroEnvio(envio.getNroEnvio());
                        }
                    }
                }

                mapAct.put(anio2, cedulas2);*/
            logger.info(" valores anioFinal: " + anioFinal);
            for (int i = anioInicio; i <= anioFinal; i++) {
                String anio = new Integer(i).toString();
                //System.out.println("NIVEL:" + nivel);
                //System.out.println("ANIO:" + anio);

                logger.info(" paramenter URL: nivel=" + nivel + "; anio=" + anio);
                List<EolCedulaConverter> cedulas = cedClient.getCedulasConverter(nivel, anio, null).getItems();

                if (cedulas != null) {
                    for (EolCedulaConverter ced : cedulas) {
                        //System.out.println("ACTIVIDAD : "+ced.getActividad().getNombre());
                        act.setIdActividad(ced.getActividad().getId());
                
                        if (envios != null) {
                            int pos = Collections.binarySearch(envios, act, buscarEnvio);
                
                            if (pos >= 0) {
                                EolIECensoConverter envio = envios.get(pos);
                                ced.setEnvio(true);
                                ced.setFechaEnvio(envio.getFechaEnvio());
                                ced.setNroEnvio(envio.getNroEnvio());
                            }
                        }
                    }

                    mapAct.put(anio, cedulas);
                }

            }

            /*configuracion fecha sigied*/
            String regionessigied = res.getMessage("talero.region.sigied");
            String[] arrRegiones = regionessigied.split(",");
            boolean esregSigied = Arrays.asList(arrRegiones).contains(sessionugel.substring(0, 4));

            String regioneshuelga = res.getMessage("talero.region.huelga");
            String[] arrRegHuelga = regioneshuelga.split(",");
            boolean esregHuelga = Arrays.asList(arrRegHuelga).contains(sessionugel.substring(0, 4));
            /*inicio configuracion fecha sigied*/

            EolIndicadorFacade ejb = lookupIndicadorFacade();

            List<EolIndicadorVariable> listIndicador = ejb.listaIndicador();

            request.setAttribute("mapAct", mapAct);
            request.setAttribute("mapListaIndicador", listIndicador);
            //return mapping.findForward("success");

            EolCifrasFacade ejbCif = lookupCifrasFacade();
            List<EolIndicadorCifras> listCifras = ejbCif.listaCifras();

            //request.setAttribute("mapAct", mapAct);
            request.setAttribute("mapListaCifras", listCifras);
            request.setAttribute("EsregSigied", esregSigied);
            request.setAttribute("EsregHuelga", esregHuelga);
         
            
            logger.info(" findForward -> success");
            logger.info(":: TableroDirectorAction.execute :: Execution finish.");
            return mapping.findForward("success");

        } else {//redireccionamos como error de acceso
            logger.info(" findForward -> errorparam");
            logger.info(":: TableroDirectorAction.execute :: Execution finish.");
            return mapping.findForward("errorparam");
        }

    }
    static Comparator<EolIECensoConverter> buscarEnvio = new Comparator<EolIECensoConverter>() {
        public int compare(EolIECensoConverter o1, EolIECensoConverter o2) {
            return (o1.getIdActividad()).compareTo(o2.getIdActividad());
        }
    };
    
    // 
    
/*
    private CensoFacade lookupCensoFacade() {
        try {
            Context c = new InitialContext();
            return (CensoFacade) c.lookup("java:global/eol-war/CensoFacade!pe.gob.minedu.escale.eol.portlets.ejb.CensoFacade");

        } catch (NamingException ne) {
            logger.error("exception caught", ne);
            throw new RuntimeException(ne);
        }

    }*/
    private CensoLocal lookupCensoFacade() {
        try {
            Context c = new InitialContext();
            return (CensoLocal) c.lookup("java:global/eol-war/CensoFacade!pe.gob.minedu.escale.eol.portlets.ejb.CensoLocal");

        } catch (NamingException ne) {
            logger.error("ERROR lookupCensoFacade ", ne);
            throw new RuntimeException(ne);
        }

    }
    
    private EolIndicadorFacade lookupIndicadorFacade() {
        try {
            Context c = new InitialContext();
            return (EolIndicadorFacade) c.lookup("java:global/eol-war/EolIndicadorFacade!pe.gob.minedu.escale.eol.estadistica.ejb.EolIndicadorFacade");

        } catch (NamingException ne) {
        	logger.error("exception caught", ne);
            throw new RuntimeException(ne);
        }

    }

    private EolCifrasFacade lookupCifrasFacade() {
        try {
            Context c = new InitialContext();
            return (EolCifrasFacade) c.lookup("java:global/eol-war/EolCifrasFacade!pe.gob.minedu.escale.eol.estadistica.ejb.EolCifrasFacade");
        } catch (NamingException ne) {
        	logger.error("exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private String getProperty(String propertie) {
        MessageResources properties = MessageResources.getMessageResources("eol");
        return properties.getMessage(propertie);
    }

}
