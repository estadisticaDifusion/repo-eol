package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2017;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;

/**
 *
 * @author JBEDRILLANA
 */
public class CedulaXlsS2017Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(CedulaXlsS2017Servlet.class);

	static final String CEDULA_MATRICULA_DESC = "CensoMatriculaDocentesRecursosCed";
	static final String PERIODO = "2017";
	static final String PATH_DIR = "2017/cedulas";

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// String preCed = "";
		String nivMod = request.getParameter("nivel");
		String anexo = request.getParameter("anexo");
		String codMod = request.getParameter("codmod");
		logger.info("********** DESCARGA  {0}***********" + codMod);
		// preCed = Funciones.cedulaMatricula2017NivMod(nivMod);
		// InputStream is =
		// getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/CensoMatriculaDocentesRecursosCed"
		// + preCed.toUpperCase() + "_" + PERIODO + ".xls");

		String rutaRaiz = getServletContext().getRealPath(
				"/WEB-INF/classes/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/CensoMatriculaDocentesRecursosCedS"
						+ "_" + PERIODO + ".xls");
		InputStream is = new FileInputStream(rutaRaiz);

		CentroEducativo ie = padronFacade.obtainByCodMod(codMod, anexo);

		if (is != null) {
			HSSFWorkbook hssfw = new HSSFWorkbook(is);

			llenadoCedulaS(hssfw, ie, nivMod);

			response.setContentType("application/octet-stream");

			response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\"", "cedulaS_2017.xls"));
			OutputStream os = response.getOutputStream();
			try {
				hssfw.write(os);

			} catch (IOException ioe) {

				ioe.printStackTrace();
			}
			os.close();
			is.close();
		}
	}

	public void llenadoCedulaS(HSSFWorkbook hssfw, CentroEducativo ie, String niv_mod) {
		HSSFSheet sheet100 = hssfw.getSheet("C100");
		// setCell(sheet100, "AR", 1, HOST );
		setCell(sheet100, "F", 10, ie.getCodMod());// Código Modular
		setCell(sheet100, "H", 10, ie.getAnexo());// Código Modular
		setCell(sheet100, "L", 10, ie.getCenEdu());// Nombre

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	@SuppressWarnings("unused")
	private void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.info(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	@SuppressWarnings("unused")
	private void setCell(HSSFSheet sheet, int col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(col).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.info(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	static void setCell(HSSFSheet sheet, int col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(col);
		cell.setCellValue(texto);
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	@SuppressWarnings("unused")
	private EnvioDocumentosFacade lookupEnvioDocumentosFacade() {
		try {
			Context c = new InitialContext();
			return (EnvioDocumentosFacade) c.lookup(
					"java:global/eol-portlet/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.ejb.EnvioDocumentosFacade");

		} catch (NamingException ne) {
			logger.info("ERROR lookupEnvioDocumentosFacade = " + ne.getMessage());
			throw new RuntimeException(ne);
		}

	}

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
