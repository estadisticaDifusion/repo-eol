package pe.gob.minedu.escale.eol.portlets.actions;

//import static pe.gob.minedu.escale.eol.portlets.util.Constantes.URL_FORWARD;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.auth.client.OAuth2Client;
import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.dto.AuthTokenInfoDTO;
import pe.gob.minedu.escale.eol.estadistica.domain.SesionUsuario;
import pe.gob.minedu.escale.eol.estadistica.ejb.SesionUsuarioFacade;
import pe.gob.minedu.escale.eol.payload.oauth2.OAuth2Response;

import pe.gob.minedu.escale.eol.portlets.service.ServiceFactory;

import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.eol.utils.CryptoSecurityUtil;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

public class LogonAction extends Action {

	private Logger logger = Logger.getLogger(LogonAction.class);
	
	private SesionUsuarioFacade usuarioFacade = ServiceFactory.getInstance().getSesionUsuarioFacade();

	public static final Charset UTF_8 = Charset.forName("UTF-8");
	
	public LogonAction() {
		logger.info("iniciando LogonAction");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response)  {
		logger.info(":: LogonAction.execute :: Starting execution...");
		MessageResources properties = MessageResources.getMessageResources("eol-oauth2");

		String tokenUri = properties.getMessage("oauth2.token.uri");
		String grantType = properties.getMessage("oauth2.token.grantType");
		String clientId = properties.getMessage("oauth2.token.clientId");
		String clientSecret = properties.getMessage("oauth2.token.clientSecret");

		logger.info("tokenUri="+tokenUri+"; grantType="+grantType);
		
		logger.info("  usuarioFacade=" + usuarioFacade + "; className: " + usuarioFacade.getClass());

		HttpSession session = request.getSession();
		logger.info(" LogonAction  session: " + session);
		
		logger.info("mapping.getParameter()->" + mapping.getParameter());
		if ("LOGOUT".equals(mapping.getParameter())) {
			
			/*new tk*/
			
			AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
			
			if(usuario != null) {
				AuthTokenInfoDTO authTokenInfo = (AuthTokenInfoDTO) session.getAttribute(Constantes.OAUTH2_SESSION);
				
				String tokenId = authTokenInfo.getAccessToken();
				
				OAuth2Client oauth2Client = new OAuth2Client(tokenUri, clientId, clientSecret);
				
				OAuth2Response responseoauth = oauth2Client.removeTokenEol(tokenId);
			
			    logger.info("Status = " + responseoauth.getStatus());
			}

			session.invalidate();
			logger.info("return mapping ->" + mapping.findForward("inicio"));
			return mapping.findForward("inicio");
		}

		session = request.getSession();
		logger.info("session3: " + session);

		DynaActionForm dForm = (DynaActionForm) form;
		String parameter = mapping.getParameter();
		ActionForward fwdSucc = null;

		String usuario = CryptoSecurityUtil.getDecryptedSecurity(dForm.getString("usuario"), Constantes.SECRET_ENCRYPT);;
		String password = CryptoSecurityUtil.getDecryptedSecurity(dForm.getString("contrasenia"), Constantes.SECRET_ENCRYPT);;

		// FIN
		logger.info("USUARIO :" + usuario);
		logger.info("  values = usuario: " + usuario + " password: " + password);

		boolean existChar = false;
		List<String> reqParams = new ArrayList<String>();
		reqParams.add(usuario);
		reqParams.add(password);
		existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

		logger.info("  parameter = " + parameter + " existChar: " + existChar);
		
		if (parameter != null && !parameter.equals("") && !existChar) {
			// String usuario = Base64.base64Decode(dForm.getString("usuario"));
			// String password = Base64.base64Decode(dForm.getString("contrasenia"));
			// System.out.println("US1 : "+dForm.getString("usuario")+" pass1:
			// "+dForm.getString("contrasenia"));
			// System.out.println("US2 : "+usuario+" pass2: "+password);
			logger.info("searching user...");
			AuthUsuario usr = usuarioFacade.userAuthValid(usuario, password);
			logger.info("search user: " + usr);
			if (usr != null) {
				String tipo = usr.getTipo() != null ? usr.getTipo() : "";
				// if
				// (!(Constantes.USUARIO_CE.equals(tipo)||Constantes.USUARIO_UGEL.equals(tipo)))
				// {
				// imendoza
				if (!(Constantes.USUARIO_CE.equals(tipo) || Constantes.USUARIO_UGEL.equals(tipo)
						|| Constantes.USUARIO_ADMIN.equals(tipo))) {
					logger.info("el usuario logueado no es director ni estadistico");
					return mapping.findForward("error_login");
				}

				// CREAR SESSION DE USUARIO
//				SesionUsuario sUsuario = usuarioFacade.obtenerSesionUsuario(usr.getUsuario());

				logger.info("usuario logeado: " + usr);
				
				/*ASIGNACION DE SESSIONES REMOVIDO AL FINAL*/
				/*session.setAttribute(Constantes.LOGGED_USER, usr);
				session.setAttribute("sessionusuario", sUsuario);
				// imendoza 20170424 - inicio
				session.setAttribute(Constantes.LOGGED_USER_PRF, tipo);
				// imendoza 20170424 - fin
				*/
				
				fwdSucc = mapping.findForward("success");

				logger.info("fwdSucc.getName(): " + fwdSucc.getName() + "; fwdSucc.getPath(): " + fwdSucc.getPath()
						+ "; fwdSucc.getRedirect(): " + fwdSucc.getRedirect());

				// fwdSucc = mapping.findForward("success_pre");
				ActionForward newFwd = new ActionForward(fwdSucc.getName(), fwdSucc.getPath(), fwdSucc.getRedirect());
				String url = (String) session.getAttribute(Constantes.URL_FORWARD);

				logger.info("URL: " + url + "; newFwd=" + newFwd + " value newFwd.getPath()=" + newFwd.getPath());

				// cuando contiene CE setear / "/ce/"
				// cuando contiene UGEL setear / "/ugel/"
				/*
				 * if (url.indexOf("/ce/") > -1) { url = "/ce/"; } else if
				 * (url.indexOf("/ugel/") > -1) { url = "/ugel/"; }
				 */
				if (fwdSucc.getPath().indexOf("/ce/") > -1) {
					url = "/ce/";
				} else if (fwdSucc.getPath().indexOf("/ugel/") > -1) {
					url = "/ugel/";
				}

				if (url != null) {
					logger.info("url no es null==========");
					newFwd.setPath(url);
				}
				logger.info("return path: " + newFwd.getPath());

				// Restore token oauth2
				String usernameToken = usuario;
				String passwordToken = password;
				logger.info("00000000" );
				try {
				OAuth2Client oauth2Client = new OAuth2Client(tokenUri, grantType, clientId, clientSecret, usernameToken, passwordToken);
				logger.info("AUTHCLIENT 2 TOKEN ---> " + oauth2Client );
				// pe.gob.minedu.escale.auth.client.OAuth2Client@26af095e
				AuthTokenInfoDTO authTokenInfo = oauth2Client.getTokenInfoEol();
				logger.info("1111111111 : "  + authTokenInfo );
				authTokenInfo.setUsername(usernameToken);
				logger.info("222222222 : "  + authTokenInfo );
				authTokenInfo.setClave(passwordToken);
				logger.info("accessToken: " + authTokenInfo.getAccessToken());
				
				/*ASIGNACION DE SESIONES*/ 
				session.setAttribute(Constantes.LOGGED_USER, usr);
//				session.setAttribute("sessionusuario", sUsuario);
				session.setAttribute(Constantes.LOGGED_USER_PRF, tipo);
				
				session.setAttribute(Constantes.OAUTH2_SESSION, authTokenInfo);
				}catch(Exception e) {
					
					logger.info("error por : .... "  + e );
					
				}
				return newFwd;
			} 
			else {
				// MHUAMANI [INICIO]
				String path = null;
				if (Constantes.PARAMETER_UGEL.equals(mapping.getParameter())) {
					path = Constantes.CONFIG_PATH_UGEL_ERROR;
				} else if (Constantes.PARAMETER_CE.equals(mapping.getParameter())) {
					path = Constantes.CONFIG_PATH_CE_ERROR;
				}
				
				logger.info("forward " + path);
				return new ActionForward(Constantes.ACTION_NAME_ERROR_USER_PASS, path, false);
				// MHUAMANI [FIN]
			}

		}
		logger.info(":: LogonAction.execute :: Execution finish");
		return mapping.findForward("inicio");
	}


}
