/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.rest.client.InstitucionEducativaClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativa;
import pe.gob.minedu.escale.rest.client.domain.InstitucionesEducativas;

/**
 *
 * @author JBEDRILLANA
 */
public class ResumenResultado2018Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(ResumenResultado2018Servlet.class);

	@EJB
	private EnvioDocumentosFacade envioDocumentosFacade;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		logger.info(":: ResumenResultado2018Servlet.processRequest :: Starting execution...");

		AuthUsuario usr = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);

		MessageResources res = MessageResources.getMessageResources("eol");

		String urlDescarga = res.getMessage("eol.path.templates.xls") + "censo2018/resultado/ResumenResultado2018.xls";

		logger.info("urlDescarga->" + urlDescarga);
		InputStream is = new FileInputStream(urlDescarga);

		HSSFWorkbook hssfw = new HSSFWorkbook(is);

		List<Object[]> lista1B = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "1B");
		List<Object[]> lista2B = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "2B");
		List<Object[]> lista3BP = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "3BP");
		List<Object[]> lista3BS = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "3BS");
		List<Object[]> lista4BI = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "4BI");
		List<Object[]> lista4BA = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "4BA");
		List<Object[]> lista5B = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "5B");
		List<Object[]> lista6B = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "6B");
		List<Object[]> lista7B = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "7B");
		List<Object[]> lista8BI = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "8BI");
		List<Object[]> lista8BP = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "8BP");
		List<Object[]> lista9B = envioDocumentosFacade.getResumeResultado2018ByUgel(usr.getUsuario(), "9B");

		InstitucionEducativaClient client = new InstitucionEducativaClient();
		InstitucionesEducativas ies1B = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "A1", "A2", "A3" });
		InstitucionesEducativas ies2B = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "A5" });
		InstitucionesEducativas ies3BP = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "B0" });
		InstitucionesEducativas ies3BS = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "F0" });
		InstitucionesEducativas ies4BI = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "D0", "D1" });
		InstitucionesEducativas ies4BA = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "D2" });
		InstitucionesEducativas ies5B = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "K0" });
		InstitucionesEducativas ies6B = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "T0" });
		InstitucionesEducativas ies7B = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "M0" });
		InstitucionesEducativas ies8BI = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "E0", "E1" });
		InstitucionesEducativas ies8BP = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "E2" });
		InstitucionesEducativas ies9B = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "L0" });

		InstitucionesEducativas iesProgArti = client.getInstitucionesProgArti(usr.getUsuario(), "1");
		InstitucionesEducativas iesProgIse = client.getInstitucionesProgIse(usr.getUsuario(), "1");
		if (iesProgArti.getItems() != null && !iesProgArti.getItems().isEmpty()) {
			ies1B.getItems().addAll(iesProgArti.getItems());
		}

		if (iesProgIse.getItems() != null && !iesProgIse.getItems().isEmpty()) {
			if (ies6B.getItems() != null)
				ies6B.getItems().addAll(iesProgIse.getItems());
			else
				ies6B.setItems(iesProgIse.getItems());
		}

		llenarResumenResultado(hssfw, ies1B.getItems(), lista1B, "1B", 42, 6, "W");
		llenarResumenResultado(hssfw, ies2B.getItems(), lista2B, "2B", 42, 6, "W");
		llenarResumenResultado(hssfw, ies3BS.getItems(), lista3BS, "3BS", 117, 6, "W");
		llenarResumenResultado(hssfw, ies3BP.getItems(), lista3BP, "3BP", 136, 6, "W");
		llenarResumenResultado(hssfw, ies4BI.getItems(), lista4BI, "4BI", 56, 6, "W");
		llenarResumenResultado(hssfw, ies4BA.getItems(), lista4BA, "4BA", 90, 6, "W");
		llenarResumenResultado(hssfw, ies5B.getItems(), lista5B, "5B", 38, 7, "W");
		llenarResumenResultado(hssfw, ies6B.getItems(), lista6B, "6B", 98, 7, "W");
		llenarResumenResultado(hssfw, ies7B.getItems(), lista7B, "7B", 54, 8, "W");
		llenarResumenResultado(hssfw, ies8BI.getItems(), lista8BI, "8BI", 44, 6, "W");
		llenarResumenResultado(hssfw, ies8BP.getItems(), lista8BP, "8BP", 46, 6, "W");
		llenarResumenResultado(hssfw, ies9B.getItems(), lista9B, "9B", 66, 6, "W");

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=\"ResumenResultado2018.xls\"");
		OutputStream os = response.getOutputStream();
		hssfw.write(os);
		os.close();
		logger.info(":: ResumenResultado2018Servlet.processRequest :: Execution finish.");
	}

	public void llenarResumenResultado(HSSFWorkbook hssfw, List<InstitucionEducativa> ies, List<Object[]> datos,
			String formato, int cantCol, int iniFDatos, String strCDatos) {
		HSSFSheet sheet = hssfw.getSheet(formato);
		if (ies == null)
			return;
		if (datos == null)
			return;

		InstitucionEducativa ieBq = new InstitucionEducativa();
		InstitucionEducativa ie;
		int iniCDatos = Funciones.letter2col(strCDatos);
		int colDatos = -1;
		int rowPrint = 0;
		for (int row = 0; row < datos.size(); row++) {
			ieBq.setCodigoModular((String) datos.get(row)[0]);
			ieBq.setAnexo((String) datos.get(row)[1]);
			int pos = Collections.binarySearch(ies, ieBq, buscarIE);
			colDatos = iniCDatos;
			if (pos >= 0) {
				ie = ies.get(pos);
				for (int x = 0; x < cantCol; x++) {
					int rowDatos = iniFDatos + rowPrint;
					if (x < iniCDatos) {
						switch (x) {
						case 0:
							Funciones.setCell(sheet, "A", rowDatos, ie.getCodigoModular());
							break;
						case 1:
							Funciones.setCell(sheet, "B", rowDatos, ie.getAnexo());
							break;
						case 2:
							Funciones.setCell(sheet, "C", rowDatos, ie.getCodigoLocal());
							break;
						case 3:
							Funciones.setCell(sheet, "D", rowDatos, ie.getDescForma());
							break;
						case 4:
							Funciones.setCell(sheet, "E", rowDatos, ie.getNivelModalidad());
							break;
						case 5:
							Funciones.setCell(sheet, "F", rowDatos, ie.getNombreIE());
							break;
						case 6:
							Funciones.setCell(sheet, "G", rowDatos, ie.getDireccion());
							break;
						case 7:
							Funciones.setCell(sheet, "H", rowDatos, ie.getTelefono());
							break;
						case 8:
							Funciones.setCell(sheet, "I", rowDatos, ie.getDescGestion());
							break;
						case 9:
							Funciones.setCell(sheet, "J", rowDatos, ie.getDescDependencia());
							break;
						case 10:
							Funciones.setCell(sheet, "K", rowDatos, ie.getCorreoElectronico());
							break;
						case 11:
							Funciones.setCell(sheet, "L", rowDatos, ie.getDirector());
							break;
						case 12:
							Funciones.setCell(sheet, "M", rowDatos, ie.getUbigeo());
							break;
						case 13:
							Funciones.setCell(sheet, "N", rowDatos, ie.getDepartamento());
							break;
						case 14:
							Funciones.setCell(sheet, "O", rowDatos, ie.getProvincia());
							break;
						case 15:
							Funciones.setCell(sheet, "P", rowDatos, ie.getDistrito());
							break;
						case 16:
							Funciones.setCell(sheet, "Q", rowDatos, ie.getCentroPoblado());
							break;
						case 17:
							Funciones.setCell(sheet, "R", rowDatos, ie.getDescArea());
							break;
						case 18:
							Funciones.setCell(sheet, "S", rowDatos, ie.getDescTipoSexo());
							break;
						case 19:
							Funciones.setCell(sheet, "T", rowDatos, ie.getDescTurno());
							break;
						case 20:
							Funciones.setCell(sheet, "U", rowDatos, ie.getNomDreUgel());
							break;
						case 21:
							Funciones.setCell(sheet, "V", rowDatos, ie.getDescCaracteristica());
							break;
						}
					} else {
						int pos_val = (x - iniCDatos) + 3;
						if (datos.get(row)[pos_val] != null) {
							if (datos.get(row)[pos_val].getClass().equals(BigDecimal.class)) {
								Funciones.setCell(sheet, colDatos, rowDatos,
										((BigDecimal) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(Integer.class)) {
								Funciones.setCell(sheet, colDatos, rowDatos,
										((Integer) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(Long.class)) {
								Funciones.setCell(sheet, colDatos, rowDatos,
										((Long) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(String.class)) {
								Funciones.setCell(sheet, colDatos, rowDatos, ((String) datos.get(row)[pos_val]));
							}
						}
						colDatos = colDatos + 1;
					}

				}
				rowPrint++;
			}
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private Comparator<InstitucionEducativa> buscarIE = new Comparator<InstitucionEducativa>() {

		public int compare(InstitucionEducativa o1, InstitucionEducativa o2) {
			if (o1.getCodigoModular().compareTo(o2.getCodigoModular()) == 0) {
				return o1.getAnexo().compareTo(o2.getAnexo());
			} else {
				return o1.getCodigoModular().compareTo(o2.getCodigoModular());
			}

		}

	};

	@SuppressWarnings("unused")
	private Comparator<Object[]> buscarIE_envio = new Comparator<Object[]>() {

		public int compare(Object[] o1, Object[] o2) {
			if (((String) o1[0]).compareTo((String) o2[0]) == 0) {
				return ((String) o1[1]).compareTo((String) o2[1]);
			} else {
				return ((String) o1[0]).compareTo((String) o2[0]);
			}

		}

	};
}
