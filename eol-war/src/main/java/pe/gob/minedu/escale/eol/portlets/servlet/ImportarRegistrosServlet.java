//package pe.gob.minedu.escale.eol.portlets.servlet;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ScheduledThreadPoolExecutor;
//import javax.ejb.EJB;
//import javax.servlet.AsyncContext;
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal;
//import pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronSigLocal;
//
///**
// *
// * @author DSILVA
// */
//@WebServlet(name = "ImportarRegistrosServlet", urlPatterns = {"/ImportarRegistrosServlet"}, asyncSupported = true)
//public class ImportarRegistrosServlet extends HttpServlet {
//
//	private static final long serialVersionUID = 1L;
//	
//	static final String URL_TERMINADO = "/WEB-INF/padron/importar/importado.jsp";
//    
//	@EJB(mappedName = "java:global/eol-war/ActualizarPadronFacade!pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal")
//	private ActualizarPadronLocal actualizarPadronLocal;
//	
//	@EJB(mappedName = "java:global/eol-war/ActualizarPadronSigFacade!pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronSigLocal")
//    private ActualizarPadronSigLocal actualizarPadronSigLocal;
//
//    /** 
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        HttpSession session = request.getSession();
//        File archivoCargado = (File) session.getAttribute("archivo_cargado");
//        AsyncContext asyncContext = request.startAsync(request, response);
//        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
//        executor.execute(new ImportarRunnable(asyncContext, archivoCargado));
//        RequestDispatcher requestDispatcher = request.getRequestDispatcher(URL_TERMINADO);
//        requestDispatcher.forward(request, response);
//    }
//
//    class ImportarRunnable implements Runnable {
//
//        AsyncContext context;
//        File file;
//
//        public ImportarRunnable(AsyncContext context, File file) {
//            this.context = context;
//            this.file = file;
//        }
//
//        @Override
//        public void run() {
//            try {
//                List<Map<String,String>> filas=new ArrayList<Map<String,String>>();
//                actualizarPadronLocal.actualizar(file,filas);
//                if(!filas.isEmpty())
//                actualizarPadronSigLocal.actualizar(filas);
//            } catch (IOException ex) {
//                log("Error ejecutando carga...", ex);
//            } finally {
//                context.dispatch(URL_TERMINADO);
//            }
//        }
//    }
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /** 
//     * Handles the HTTP <code>POST</code> method.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /** 
//     * Returns a short description of the servlet.
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//}
