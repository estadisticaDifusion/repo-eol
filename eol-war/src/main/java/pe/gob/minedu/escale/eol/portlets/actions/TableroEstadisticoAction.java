/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.admin.ejb.ActividadLocal;
import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.domain.DireccionRegional;
import pe.gob.minedu.escale.eol.padron.domain.Ugel;
import pe.gob.minedu.escale.eol.padron.ejb.DreLocal;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.padron.ejb.UgelLocal;
import pe.gob.minedu.escale.eol.portlets.domain.act.ResumenCobertura;
import pe.gob.minedu.escale.eol.portlets.domain.act.Tablero;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.eol.portlets.util.enumerations.RoleEnum;
import pe.gob.minedu.escale.rest.client.EolActividadesClient;

/**
 *
 * @author JMATAMOROS
 */

public class TableroEstadisticoAction extends Action {

	private Logger logger = Logger.getLogger(TableroEstadisticoAction.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@EJB
	private PadronLocal padronFacade = lookup(
			"java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@EJB
	private EnvioDocumentosFacade envioDocumentosFacade = lookup(EnvioDocumentosFacade.class);

	@EJB
	private UgelLocal ugelFacade = lookup(
			"java:global/eol-war/UgelFacade!pe.gob.minedu.escale.eol.padron.ejb.UgelLocal");

	@EJB
	private DreLocal dreFacade = lookup("java:global/eol-war/DreFacade!pe.gob.minedu.escale.eol.padron.ejb.DreLocal");

	@EJB
	private ActividadLocal actividadFacade = lookup(
			"java:global/eol-war/ActividadFacade!pe.gob.minedu.escale.eol.admin.ejb.ActividadLocal");

	@SuppressWarnings("unused")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info(":: TableroEstadisticoAction.execute :: Starting execution...");

		logger.info("value padronFacade = " + padronFacade);

		MessageResources res = MessageResources.getMessageResources("ApplicationResources");
		int anioInicio = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_INICIO));
		int anioFinal = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_FINAL));
		int tipoRegion = 1;
		/* 1. EOL 2. SIGIED 3. HUELGA */
		Long idAct = -1L;
		try {
			if (request.getParameter("idActividad") != null) {
				idAct = Long.valueOf(request.getParameter("idActividad"));
			}
		} catch (NumberFormatException ex) {
			logger.error("ERROR EN LA ACTIVIDAD", ex);
			idAct = -1L;
		}

		AuthUsuario usuario = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);
		if (usuario != null) {
			String user_e = usuario.getUsuario();
			boolean esDRE = user_e.endsWith("00") || user_e.equals(Constantes.USUARIO_DRE_CALLAO)
					|| user_e.equals(Constantes.USUARIO_DRE_LIMA_METRO)
					|| user_e.equals(Constantes.USUARIO_DRE_LIMA_PROV);

			if (Constantes.USUARIO_ADMIN.equals(usuario.getTipo())) {
				EolActividadesClient actClient = new EolActividadesClient();
				List<EolActividadConverter> actividades = actClient
						.getActividadesConverter(null, Constantes.PERIODO_CENSAL_ACTUAL).getItems(); // "2019"
				logger.info("  actividades = " + actividades);
				return mapping.findForward("successAdmin");
			}

			/* configuracion fecha sigied */
			String regionessigied = res.getMessage("talero.region.sigied");
			String[] arrRegSiegied = regionessigied.split(",");
			boolean esregSigied = Arrays.asList(arrRegSiegied).contains(user_e.substring(0, 4));
			if (esregSigied) {
				tipoRegion = 2;
			}

			String regioneshuelga = res.getMessage("talero.region.huelga");
			String[] arrRegHuelga = regioneshuelga.split(",");
			boolean esregHuelga = Arrays.asList(arrRegHuelga).contains(user_e.substring(0, 4));
			if (esregHuelga) {
				tipoRegion = 3;
			}
			/* inicio configuracion fecha sigied */

			Tablero tablero = new Tablero();
			if (esDRE) {
				tablero.setRol(RoleEnum.DRE);
				logger.info("dreFacade: " + dreFacade);
				DireccionRegional dre = dreFacade.findById(user_e.substring(0, 4));
				if (dre != null) {
					pe.gob.minedu.escale.rest.client.domain.Dre dreDTO = new pe.gob.minedu.escale.rest.client.domain.Dre();
					dreDTO.setId(dre.getId());
					dreDTO.setNombreDre(dre.getNombreDre());
					tablero.setDre(dreDTO);
				}
				/*
				 * DreClient dreClient = new DreClient(user_e.substring(0, 4)); Dre dre =
				 * dreClient.get_JSON(Dre.class); tablero.setDre(dre);
				 */
			} else {
				tablero.setRol(RoleEnum.UGEL);
				logger.info("ugelFacade: " + ugelFacade);
				Ugel ugel = ugelFacade.findById(user_e);
				logger.info("ugel: " + ugel);
				if (ugel != null) {
					pe.gob.minedu.escale.rest.client.domain.Ugel ugelDTO = new pe.gob.minedu.escale.rest.client.domain.Ugel();
					ugelDTO.setIdUgel(ugel.getIdUgel());
					ugelDTO.setNombreUgel(ugel.getNombreUgel());
					tablero.setUgel(ugelDTO);
				}
			}

			EolActividadesClient actClient = new EolActividadesClient();
			Map<String, List<EolActividadConverter>> mapAct = new HashMap<String, List<EolActividadConverter>>();
			EolActividadConverter actAct = null;

			/* JL */
			EolActividadConverter actMat = null;
			EolActividadConverter actLoc = null;
			EolActividadConverter actResult = null;
			/* JL */

			logger.info(" start mapAct anioFinal=" + anioFinal + "; anioInicio=" + anioInicio);
			for (int i = anioFinal; i >= anioInicio; i--) {
				String anioKey = new Integer(i).toString();
//				List<EolActividadConverter> actividades = actClient.getActividadesConverter(null, anioKey).getItems();
				List<EolActividadConverter> actividades = findActividades(anioKey); 
				mapAct.put(anioKey, actividades);
			}
			logger.info(" end mapAct mapAct=" + mapAct + (mapAct != null ? mapAct.size() + " records" : " not found"));
			tablero.setMapActividades(mapAct);

//			for (Map.Entry<String, List<EolActividadConverter>> entry : mapAct.entrySet()) {
//				logger.info("info map: " + entry.getKey() + "/" + entry.getValue());
//				for (EolActividadConverter actividad : entry.getValue()) {
//					logger.info("  info actividad: fechaInicio(" + actividad.getFechaInicio() + "), estadoSituacion("
//							+ actividad.isEstadoSituacion() + "), urlSituacion(" + actividad.getUrlSituacion() + ")");
//				}
//			}

			List<Object[]> listaCentros = null;
			List<Object[]> listaEnvios = null;
			List<Object[]> listaEnviosPost = null;
			List<ResumenCobertura> resumen = null;
			List<ResumenCobertura> resumenMat = null;
			List<ResumenCobertura> resumenLoc = null;
			List<ResumenCobertura> resumenRest = null;

			listaCentros = null;
			listaEnvios = null;
			listaEnviosPost = null;
			actMat = mapAct.get(new Integer(anioFinal).toString()).get(0);
			logger.info("ACTMAT . ----> " + actMat + "/////" + user_e);

			listaCentros = listarCentros(actMat, user_e);
			listaEnvios = listarEnviosAntesDe(actMat, user_e, tipoRegion);
			listaEnviosPost = listarEnviosPostFecha(actMat, user_e, tipoRegion);
			resumenMat = obtenerResumenCoberturaPostFecha(listaCentros, listaEnvios, listaEnviosPost);

			listaCentros = null;
			listaEnvios = null;
			listaEnviosPost = null;

			for (EolActividadConverter eolconv : mapAct.get(new Integer(anioFinal).toString())) {
				if (eolconv.getNombre().equals("CENSO-LOCAL")) {
					actLoc = eolconv;
					break;
				}
			}

			listaCentros = listarCentros(actLoc, user_e);
			listaEnvios = listarEnviosAntesDe(actLoc, user_e, tipoRegion);
			listaEnviosPost = listarEnviosPostFecha(actLoc, user_e, tipoRegion);
			resumenLoc = obtenerResumenCoberturaPostFecha(listaCentros, listaEnvios, listaEnviosPost);

			// RESULTADO : MOSTRADO PENDIENTE
			// PENDIENTE RESULTADO 2017
			listaCentros = null;
			listaEnvios = null;
			// actResult = mapAct.get(new Integer(anioFinal).toString()).get(3);
			for (EolActividadConverter eolconv : mapAct.get(new Integer(anioFinal).toString())) {
				if (eolconv.getNombre().equals("CENSO-RESULTADO")) {
					actResult = eolconv;
					break;
				}
			}

			listaCentros = listarCentros(actResult, user_e);
			listaEnvios = listarEnvios(actResult, user_e);
			resumenRest = obtenerResumenCobertura(listaCentros, listaEnvios);

			request.setAttribute("resumenCobertura", resumen);
			request.setAttribute("ActividadActual", actAct);
			request.setAttribute("tableroEstd", tablero);
			request.setAttribute("resumenCoberturaMat", resumenMat);
			request.setAttribute("resumenCoberturaLoc", resumenLoc);
			request.setAttribute("resumenCoberturaRest", resumenRest);
			request.setAttribute("ActividadMat", actMat);
			request.setAttribute("ActividadLoc", actLoc);
			request.setAttribute("EsregSigied", esregSigied);
			request.setAttribute("EsregHuelga", esregHuelga);
			request.setAttribute("anioInicioEstadistica", anioInicio);
			request.setAttribute("anioFinalEstadistica", anioFinal);
		}
		logger.info(":: TableroEstadisticoAction.execute :: Execution finish.");
		return mapping.findForward("success");
	}

	@SuppressWarnings("unused")
	private List<ResumenCobertura> obtenerResumenCobertura(List<Object[]> listaCentros, List<Object[]> listaEnvios) {
		List<ResumenCobertura> resumentemp = new ArrayList<ResumenCobertura>();
		if (listaCentros != null) {
			for (Object[] ces : listaCentros) {
				ResumenCobertura rc = new ResumenCobertura(ces[0].toString(), ((Number) ces[1]).intValue());
				int pos = Collections.binarySearch(listaEnvios, ces, buscarLista);
				if (pos >= 0) {
					rc.setCuentaEnvios(((Number) listaEnvios.get(pos)[1]).intValue());
				}
				resumentemp.add(rc);
			}
		}

		return resumentemp;
	}

	private List<ResumenCobertura> obtenerResumenCoberturaPostFecha(List<Object[]> listaCentros,
			List<Object[]> listaEnvios, List<Object[]> listaEnviosPost) {
		logger.info(":: TableroEstadisticoAction.obtenerResumenCoberturaPostFecha :: Starting execution");
		logger.info(" listaCentros = " + (listaCentros != null ? listaCentros.size() : " is null."));
		logger.info(" listaEnvios = " + (listaEnvios != null ? listaEnvios.size() : " is null."));
		logger.info(" listaEnviosPost = " + (listaEnviosPost != null ? listaEnviosPost.size() : " is null."));
		List<ResumenCobertura> resumentemp = new ArrayList<ResumenCobertura>();
		if (listaCentros != null) {
			for (Object[] ces : listaCentros) {
				ResumenCobertura rc = new ResumenCobertura(ces[0].toString(), ((Number) ces[1]).intValue());

				int pos = Collections.binarySearch(listaEnvios, ces, buscarLista);
				if (pos >= 0) {
					rc.setCuentaEnvios(((Number) listaEnvios.get(pos)[1]).intValue());
				}

				int posPF = Collections.binarySearch(listaEnviosPost, ces, buscarLista);
				if (posPF >= 0) {
					rc.setCuentaEnviosPost(((Number) listaEnviosPost.get(posPF)[1]).intValue());
				}

				resumentemp.add(rc);
			}
		}
		logger.info(":: TableroEstadisticoAction.obtenerResumenCoberturaPostFecha :: Execution finish.");
		return resumentemp;
	}

	private List<Object[]> listarCentros(EolActividadConverter activ, String user_e) {
		logger.info(":: TableroEstadisticoAction.listarCentros :: Starting execution");
		List<Object[]> listaCentTemp = null;
		/*
		 * if (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2011")) { listaCentTemp =
		 * padronSrv.getCuentaCentros(user_e, "CENSO-MATRICULA"); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2011")) { listaCentTemp =
		 * padronSrv.getCuentaCentrosByLocal(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2011")) { listaCentTemp =
		 * padronSrv.getCuentaCentros(user_e, "CENSO-RESULTADO"); } else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2012")) { listaCentTemp =
		 * padronSrv.getCuentaCentros2012(user_e, "CENSO-MATRICULA"); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2012")) { listaCentTemp =
		 * padronSrv.getCuentaCentrosByLocal(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2012")) { listaCentTemp =
		 * padronSrv.getCuentaCentros(user_e, "CENSO-RESULTADO"); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2013")) { listaCentTemp =
		 * padronSrv.getCuentaCentros(user_e, "CENSO-RESULTADO"); }else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2013")) { listaCentTemp =
		 * padronSrv.getCuentaCentros2013(user_e, "CENSO-MATRICULA"); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2013")) { listaCentTemp =
		 * padronSrv.getCuentaCentrosByLocal(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2014")) { listaCentTemp =
		 * padronSrv.getCuentaCentros(user_e, "CENSO-RESULTADO"); }else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2014")) { listaCentTemp =
		 * padronSrv.getCuentaCentros2014(user_e, "CENSO-MATRICULA"); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2014")) { listaCentTemp =
		 * padronSrv.getCuentaCentrosByLocal(user_e); }else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2015")) { listaCentTemp =
		 * padronSrv.getCuentaCentros2014(user_e, "CENSO-MATRICULA");//los metodos de
		 * obtencion de datos muestran lo mismo }else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2015")) { listaCentTemp =
		 * padronSrv.getCuentaCentrosByLocal(user_e); }else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2015")) { listaCentTemp =
		 * padronSrv.getCuentaCentros(user_e, "CENSO-RESULTADO"); }else
		 */
		if (activ.getNombre().equals("CENSO-MATRICULA") && activ.getNombrePeriodo().equals("2016")) {
			listaCentTemp = this.padronFacade.getCuentaCentros2014(user_e, "CENSO-MATRICULA");
		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals("2016")) {
			listaCentTemp = this.padronFacade.getCuentaCentrosByLocal(user_e);
		} else if (activ.getNombre().equals("CENSO-RESULTADO") && activ.getNombrePeriodo().equals("2016")) {
			listaCentTemp = this.padronFacade.getCuentaCentros(user_e, "CENSO-RESULTADO");
		} else if (activ.getNombre().equals("CENSO-MATRICULA") && activ.getNombrePeriodo().equals("2017")) {
			listaCentTemp = this.padronFacade.getCuentaCentros2014(user_e, "CENSO-MATRICULA");
		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals("2017")) {
			listaCentTemp = this.padronFacade.getCuentaCentrosByLocal(user_e);
		} else if (activ.getNombre().equals("CENSO-RESULTADO") && activ.getNombrePeriodo().equals("2017")) {
			listaCentTemp = this.padronFacade.getCuentaCentros2014(user_e, "CENSO-RESULTADO");
		} else if (activ.getNombre().equals("CENSO-MATRICULA") && activ.getNombrePeriodo().equals("2018")) {
			listaCentTemp = this.padronFacade.getCuentaCentros2014(user_e, "CENSO-MATRICULA");
		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals("2018")) {
			listaCentTemp = this.padronFacade.getCuentaCentrosByLocal(user_e);
		} else if (activ.getNombre().equals("CENSO-MATRICULA")
				&& activ.getNombrePeriodo().equals(Constantes.ANIO_2019)) { // "2019"
			listaCentTemp = this.padronFacade.getCuentaCentros2014(user_e, "CENSO-MATRICULA");
		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals(Constantes.ANIO_2019)) { // "2019"
			listaCentTemp = this.padronFacade.getCuentaCentrosByLocal(user_e);
		} else if (activ.getNombre().equals("CENSO-RESULTADO")
				&& activ.getNombrePeriodo().equals(Constantes.ANIO_2019)) {
			listaCentTemp = this.padronFacade.getCuentaCentros(user_e, "CENSO-RESULTADO");
		}
		logger.info(":: TableroEstadisticoAction.listarCentros :: Execution finish.");
		return listaCentTemp;
	}

	@SuppressWarnings("unused")
	private List<Object[]> listarEnvios(EolActividadConverter activ, String user_e) {
		List<Object[]> listaEnvTemp = null;
		/*
		 * if (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2011")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioMatDocRec2011(user_e); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2011")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioLocal2011(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2011")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioResultado2011(user_e); } else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2012")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioMatDocRec2012(user_e); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2012")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioLocal2012(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2012")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioResultado2012(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2013")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioResultado2013(user_e); }else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2013")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioMatDocRec2013(user_e); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2013")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioLocal2013(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2014")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioResultado2014(user_e); }else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2014")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioMatDocRec2014(user_e); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2014")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioLocal2014(user_e); }else if
		 * (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2015")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioMatDocRec2015(user_e); }else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2015")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioLocal2015(user_e); } else if
		 * (activ.getNombre().equals("CENSO-RESULTADO") &&
		 * activ.getNombrePeriodo().equals("2015")) { listaEnvTemp =
		 * matriSrv.getResumenEnvioResultado2015(user_e); }else
		 */
		if (activ.getNombre().equals("CENSO-MATRICULA") && activ.getNombrePeriodo().equals("2016")) {
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioMatDocRec2016(user_e);
		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals("2016")) {
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioLocal2016(user_e);
		} /* PENDIENTE RESULTADO 2016 */ else if (activ.getNombre().equals("CENSO-RESULTADO")
				&& activ.getNombrePeriodo().equals("2016")) {
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioResultado2016(user_e);
		} else if (activ.getNombre().equals("CENSO-MATRICULA") && activ.getNombrePeriodo().equals("2017")) {
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioMatDocRec2017(user_e);
		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals("2017")) {
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioLocal2017(user_e);
		} else if (activ.getNombre().equals("CENSO-RESULTADO") && activ.getNombrePeriodo().equals("2017")) {
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioResultado2017(user_e);
		}
//		else if (activ.getNombre().equals("CENSO-MATRICULA") && activ.getNombrePeriodo().equals("2018")) {
//			listaEnvTemp = matriSrv.getResumenEnvioMatDocRec2017(user_e);
//		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals("2018")) {
//			listaEnvTemp = matriSrv.getResumenEnvioLocal2017(user_e);
//		} else if (activ.getNombre().equals("CENSO-RESULTADO") && activ.getNombrePeriodo().equals("2018")) {
//			listaEnvTemp = matriSrv.getResumenEnvioResultado2017(user_e);
//		}else if (activ.getNombre().equals("CENSO-MATRICULA") && activ.getNombrePeriodo().equals("2019")) {
//			listaEnvTemp = matriSrv.getResumenEnvioMatDocRec2017(user_e);
//		} else if (activ.getNombre().equals("CENSO-LOCAL") && activ.getNombrePeriodo().equals("2019")) {
//			listaEnvTemp = matriSrv.getResumenEnvioLocal2017(user_e);
		else if (activ.getNombre().equals("CENSO-RESULTADO") && activ.getNombrePeriodo().equals("2019")) {
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioResultado2019(user_e);
		}

		return listaEnvTemp;
	}

	private List<Object[]> listarEnviosAntesDe(EolActividadConverter activ, String user_e, int tipoRegion) {
		List<Object[]> listaEnvTemp = null;
		logger.info("LLEGAMOS : " + activ);
		Date fechTe = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(tipoRegion == 1 ? activ.getFechaTermino()
				: (tipoRegion == 2 ? activ.getFechaTerminosigied() : activ.getFechaTerminohuelga()));
		c.add(Calendar.DATE, 1);
		fechTe = c.getTime();
		logger.info("LLEGAMOS 1111: " + fechTe);
		/*
		 * if (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2018")) { listaEnvTemp =
		 * envioDocumentosFacade.getResumenEnvioMatDocRec2018AntesFecha(user_e,
		 * dateFormat.format(fechTe)); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2018")) { listaEnvTemp =
		 * envioDocumentosFacade.getResumenEnvioLocal2018AntesFecha(user_e,
		 * dateFormat.format(fechTe)); }
		 */
		if (activ.getNombre().equals("CENSO-MATRICULA")
				&& activ.getNombrePeriodo().equals(Constantes.PERIODO_CENSAL_ACTUAL)) { // "2019"
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioMatDocRec2019AntesFecha(user_e,
					dateFormat.format(fechTe));
		} else if (activ.getNombre().equals("CENSO-LOCAL")
				&& activ.getNombrePeriodo().equals(Constantes.PERIODO_CENSAL_ACTUAL)) { // "2019"
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioLocal2019AntesFecha(user_e, dateFormat.format(fechTe));
		}

		return listaEnvTemp;
	}

	private List<Object[]> listarEnviosPostFecha(EolActividadConverter activ, String user_e, int tipoRegion) {
		List<Object[]> listaEnvTemp = null;
		logger.info("LLEGAMOS 22222: " + activ);
		Date fechTe = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(tipoRegion == 1 ? activ.getFechaTermino()
				: (tipoRegion == 2 ? activ.getFechaTerminosigied() : activ.getFechaTerminohuelga()));
		c.add(Calendar.DATE, 1);
		fechTe = c.getTime();
		logger.info("LLEGAMOS 3333: " + fechTe);
		/*
		 * if (activ.getNombre().equals("CENSO-MATRICULA") &&
		 * activ.getNombrePeriodo().equals("2018")) { listaEnvTemp =
		 * envioDocumentosFacade.getResumenEnvioMatDocRec2018PostFecha(user_e,
		 * dateFormat.format(fechTe)); } else if
		 * (activ.getNombre().equals("CENSO-LOCAL") &&
		 * activ.getNombrePeriodo().equals("2018")) { listaEnvTemp =
		 * envioDocumentosFacade.getResumenEnvioLocal2018PostFecha(user_e,
		 * dateFormat.format(fechTe)); }
		 */
		if (activ.getNombre().equals("CENSO-MATRICULA")
				&& activ.getNombrePeriodo().equals(Constantes.PERIODO_CENSAL_ACTUAL)) { // "2019"
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioMatDocRec2019PostFecha(user_e,
					dateFormat.format(fechTe));
		} else if (activ.getNombre().equals("CENSO-LOCAL")
				&& activ.getNombrePeriodo().equals(Constantes.PERIODO_CENSAL_ACTUAL)) { // "2019"
			listaEnvTemp = envioDocumentosFacade.getResumenEnvioLocal2019PostFecha(user_e, dateFormat.format(fechTe));
		}

		return listaEnvTemp;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> className) {
		String nameFacade = className.getSimpleName().replace("Local", "Facade");
		String nameContextEjb = "java:global/eol-war/" + nameFacade + "!" + className.getName();
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

	private Comparator<Object[]> buscarLista = new Comparator<Object[]>() {

		public int compare(Object[] o1, Object[] o2) {
			return o1[0].toString().compareTo(o2[0].toString());
		}
	};
	
	private List<EolActividadConverter> findActividades(String periodo) {
		logger.info(":: TableroEstadisticoAction.findActividades :: Starting execution...");
		List<EolActividadConverter> actividades = actividadFacade.findActividadByParams(null, periodo);
		logger.info(":: TableroEstadisticoAction.findActividades :: Execution finish.");
		return actividades;
	}
	
	static Comparator<EolActividadConverter> buscarActividad = new Comparator<EolActividadConverter>() {

		public int compare(EolActividadConverter o1, EolActividadConverter o2) {

			return (o1.getId()).compareTo(o2.getId());
		}
	};

	

}
