/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.portlets.domain.act.ResumenCobertura;
import pe.gob.minedu.escale.eol.portlets.domain.act.Tablero;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.eol.portlets.util.enumerations.RoleEnum;
import pe.gob.minedu.escale.rest.client.DreClient;
import pe.gob.minedu.escale.rest.client.EolActividadesClient;
import pe.gob.minedu.escale.rest.client.UgelClient;
import pe.gob.minedu.escale.rest.client.domain.Dre;
import pe.gob.minedu.escale.rest.client.domain.Ugel;

/**
 *
 * @author JMATAMOROS
 */
public class EstadisticoPortletServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(EstadisticoPortletServlet.class);

	@EJB
	private PadronLocal padronSrv = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	public void doView(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("INGRESANDO............|");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		MessageResources res = MessageResources.getMessageResources("ApplicationResources");
		int anioInicio = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_INICIO));
		int anioFinal = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_FINAL));
		Long idAct = -1L;
		try {
			if (request.getParameter("idActividad") != null)
				idAct = Long.valueOf(request.getParameter("idActividad"));
		} catch (NumberFormatException ex) {
			logger.error("ERROR EN LA ACTIVIDAD", ex);
			idAct = -1L;
		}

		try {

			AuthUsuario usuario = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);
			if (usuario != null) {
				String user_e = usuario.getUsuario();
				boolean esDRE = user_e.endsWith("00") || user_e.equals(Constantes.USUARIO_DRE_CALLAO)
						|| user_e.equals(Constantes.USUARIO_DRE_LIMA_METRO)
						|| user_e.equals(Constantes.USUARIO_DRE_LIMA_PROV);

				Tablero tablero = new Tablero();
				if (esDRE) {
					tablero.setRol(RoleEnum.DRE);
					DreClient dreClient = new DreClient(user_e.substring(0, 4));
					Dre dre = dreClient.get_JSON(Dre.class);
					tablero.setDre(dre);
				} else {
					tablero.setRol(RoleEnum.UGEL);
					UgelClient ugelClient = new UgelClient(user_e);
					Ugel ugel = ugelClient.get_JSON(Ugel.class);
					tablero.setUgel(ugel);
				}

				EolActividadesClient actClient = new EolActividadesClient();
				Map<String, List<EolActividadConverter>> mapAct = new HashMap<String, List<EolActividadConverter>>();
				EolActividadConverter actAct = null;

				for (int i = anioFinal; i >= anioInicio; i--) {
					String anioKey = new Integer(i).toString();
					List<EolActividadConverter> actividades = actClient.getActividadesConverter(null, anioKey)
							.getItems();
					mapAct.put(anioKey, actividades);

					if (actAct == null && idAct != -1L) {
						int pos = Collections.binarySearch(actividades, new EolActividadConverter(idAct.intValue()),
								buscarActividad);
						actAct = actividades.get(pos);

					}
				}
				if (actAct == null) {
					actAct = mapAct.get(new Integer(anioFinal).toString()).get(0);
				}

				tablero.setMapActividades(mapAct);

				request.setAttribute("tableroEstd", tablero);

				// PadronFacade padronSrv = lookupPadronFacade();
				EnvioDocumentosFacade matriSrv = lookupEnvioDocumentosFacade();
				List<Object[]> listaCentros = null;
				List<Object[]> listaEnvios = null;

				/*
				 * if (actAct.getNombre().equals("CENSO-MATRICULA") &&
				 * actAct.getNombrePeriodo().equals("2011")) { listaCentros =
				 * padronSrv.getCuentaCentros(user_e,"CENSO-MATRICULA"); listaEnvios =
				 * matriSrv.getResumenEnvioMatDocRec2011(user_e); } else if
				 * (actAct.getNombre().equals("CENSO-LOCAL")&&
				 * actAct.getNombrePeriodo().equals("2011")) { listaCentros =
				 * padronSrv.getCuentaCentrosByLocal(user_e); listaEnvios =
				 * matriSrv.getResumenEnvioLocal2011(user_e); } else if
				 * (actAct.getNombre().equals("CENSO-RESULTADO")&&
				 * actAct.getNombrePeriodo().equals("2011")) { listaCentros =
				 * padronSrv.getCuentaCentros(user_e,"CENSO-RESULTADO"); listaEnvios =
				 * matriSrv.getResumenEnvioResultado2011(user_e); }else if
				 * (actAct.getNombre().equals("CENSO-MATRICULA") &&
				 * actAct.getNombrePeriodo().equals("2012")) { listaCentros =
				 * padronSrv.getCuentaCentros2012(user_e,"CENSO-MATRICULA"); listaEnvios =
				 * matriSrv.getResumenEnvioMatDocRec2012(user_e); }else if
				 * (actAct.getNombre().equals("CENSO-LOCAL") &&
				 * actAct.getNombrePeriodo().equals("2012")) { listaCentros =
				 * padronSrv.getCuentaCentrosByLocal(user_e); listaEnvios =
				 * matriSrv.getResumenEnvioLocal2012(user_e); }else if
				 * (actAct.getNombre().equals("CENSO-MATRICULA") &&
				 * actAct.getNombrePeriodo().equals("2013")) { listaCentros =
				 * padronSrv.getCuentaCentros2013(user_e,"CENSO-MATRICULA"); listaEnvios =
				 * matriSrv.getResumenEnvioMatDocRec2013(user_e); }else if
				 * (actAct.getNombre().equals("CENSO-LOCAL") &&
				 * actAct.getNombrePeriodo().equals("2013")) { listaCentros =
				 * padronSrv.getCuentaCentrosByLocal(user_e); listaEnvios =
				 * matriSrv.getResumenEnvioLocal2013(user_e); }else
				 */ if (actAct.getNombre().equals("CENSO-MATRICULA") && actAct.getNombrePeriodo().equals("2014")) {
					listaCentros = padronSrv.getCuentaCentros2014(user_e, "CENSO-MATRICULA");
					listaEnvios = matriSrv.getResumenEnvioMatDocRec2014(user_e);
				} else if (actAct.getNombre().equals("CENSO-LOCAL") && actAct.getNombrePeriodo().equals("2014")) {
					listaCentros = padronSrv.getCuentaCentrosByLocal(user_e);
					listaEnvios = matriSrv.getResumenEnvioLocal2014(user_e);
				}

				List<ResumenCobertura> resumen = new ArrayList<ResumenCobertura>();
				if (listaCentros != null)
					for (Object[] ces : listaCentros) {
						ResumenCobertura rc = new ResumenCobertura(ces[0].toString(), ((Number) ces[1]).intValue());
						int pos = Collections.binarySearch(listaEnvios, ces, buscarLista);
						if (pos >= 0) {
							rc.setCuentaEnvios(((Number) listaEnvios.get(pos)[1]).intValue());
						}
						resumen.add(rc);
					}
			
				request.setAttribute("resumenCobertura", resumen);
				request.setAttribute("ActividadActual", actAct);
				request.setAttribute("tableroEstd", tablero);
				response.addCookie(new Cookie("COD_UGEL", user_e));
				getServletContext().getRequestDispatcher("/ugel/tablero-estadistico.jsp").forward(request, response);

			} else {
				request.getSession().invalidate();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.close();
		}
		// String usuario="";
		// String
		// screenName=request.getParameter("codUgel")!=null?"e"+request.getParameter("codUgel")
		// :"e150102";//user.getScreenName()

	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			doView(request, response);

		} finally {
			out.close();
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
/*
	private PadronFacade lookupPadronFacade() {
		try {
			Context c = new InitialContext();
			return (PadronFacade) c
					.lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.facade.PadronFacade");

		} catch (NamingException ne) {
			logger.error("exception caught", ne);
			throw new RuntimeException(ne);
		}
	}*/

	private EnvioDocumentosFacade lookupEnvioDocumentosFacade() {
		try {
			Context c = new InitialContext();
			return (EnvioDocumentosFacade) c.lookup(
					"java:global/eol-war/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.ejb.EnvioDocumentosFacade");

		} catch (NamingException ne) {
			logger.error("exception caught", ne);
			throw new RuntimeException(ne);
		}

	}

	private Comparator<Object[]> buscarLista = new Comparator<Object[]>() {

		public int compare(Object[] o1, Object[] o2) {
			return o1[0].toString().compareTo(o2[0].toString());
		}
	};

	static Comparator<EolActividadConverter> buscarActividad = new Comparator<EolActividadConverter>() {
		public int compare(EolActividadConverter o1, EolActividadConverter o2) {

			return (o1.getId()).compareTo(o2.getId());
		}
	};
	
	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
