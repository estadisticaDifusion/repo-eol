package pe.gob.minedu.escale.eol.portlets.actions;

import pe.gob.minedu.escale.eol.portlets.util.Constantes;

import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;

public class SesionExpiroAction extends Action {

	private Logger logger = Logger.getLogger(SesionExpiroAction.class);

	public static final Charset UTF_8 = Charset.forName("UTF-8");

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info(":: SesionExpiroAction.execute :: Starting execution...");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
		logger.info(Constantes.LOGGED_USER + " = " + usuario);
		if (usuario != null) {
			session.removeAttribute(Constantes.LOGGED_USER);
			session.removeAttribute(Constantes.OAUTH2_SESSION);
			session.invalidate();
		}
		logger.info(":: SesionExpiroAction.execute :: Execution finish.");
		return mapping.findForward("success");
	}

}
