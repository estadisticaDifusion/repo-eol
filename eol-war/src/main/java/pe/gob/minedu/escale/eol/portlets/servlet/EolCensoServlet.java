/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.converter.EolCedulaConverter;
import pe.gob.minedu.escale.eol.converter.EolIECensoConverter;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.gson.GsonExclusionStrategy;
import pe.gob.minedu.escale.eol.portlets.ejb.facade.CensoFacade;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.rest.client.EolCedulasClient;
import pe.gob.minedu.escale.rest.client.domain.EolPeriodoConverter;

/**
 *
 * @author JMATAMOROS
 */
public class EolCensoServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(EolCensoServlet.class);

    private static String URL_DOWNLOAD_XLS = "DataXLS?tipo=excel&idCA=";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info(":: EolCensoServlet.processRequest :: Starting execution...");
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String tipo = request.getParameter("TIPO");
        MessageResources res = MessageResources.getMessageResources("ApplicationResources");
        //GsonBuilder builder = new GsonBuilder();
        Gson gson = createGsonFromBuilder(new GsonExclusionStrategy(null)); //builder.create();

        try {
            String nivel = request.getParameter("nivel");
            String anexo = request.getParameter("anexo");
            String codmod = request.getParameter("codmod");
            String codlocal = request.getParameter("codlocal");
            String codinst = request.getParameter("codinst");

            boolean existChar = false;
            List<String> reqParams = new ArrayList<String>();
            reqParams.add(nivel);
            reqParams.add(anexo);
            reqParams.add(codmod);
            reqParams.add(codlocal);
            reqParams.add(codinst);

            existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

            if (!existChar) {

                List<EolPeriodoConverter> periodos = new ArrayList<EolPeriodoConverter>();

                int anioInicio = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_INICIO));
                int anioFinal = Integer.parseInt(res.getMessage(Constantes.CENSO_ANIO_FINAL));

                EolCedulasClient cedClient = new EolCedulasClient();
                CensoFacade censoFacade = lookupCensoFacade();

                List<EolIECensoConverter> envios = censoFacade.cadCensoTotalporCodigos(nivel, codmod, anexo, codlocal, codinst);

                if (envios == null) {
                    envios = new ArrayList<EolIECensoConverter>();
                }

                if (envios != null) {
                    Collections.sort(envios, buscarEnvio);
                }

                EolIECensoConverter act = new EolIECensoConverter();
                for (int i = anioFinal; i >= anioInicio; i--) {
                    EolPeriodoConverter periodo = new EolPeriodoConverter();
                    String anio = new Integer(i).toString();

                    List<EolCedulaConverter> cedulas = cedClient.getCedulasConverter(nivel, anio, null).getItems();

                    for (EolCedulaConverter ced : cedulas) {
                        act.setIdActividad(ced.getActividad().getId());
                        ced.getActividad().setStrFechaInicio(Funciones.getFecha(ced.getActividad().getFechaInicio(), "MMM yyyy"));
                        ced.getActividad().setStrFechaTermino(Funciones.getFecha(ced.getActividad().getFechaTermino(), "MMM yyyy"));
                        ced.getActividad().setStrFechaTerminosigied(Funciones.getFecha(ced.getActividad().getFechaTerminosigied(), "MMM yyyy"));
                        ced.getActividad().setStrFechaTerminohuelga(Funciones.getFecha(ced.getActividad().getFechaTerminohuelga(), "MMM yyyy"));
                        ced.getActividad().getEstadoActividad();
                        ced.getActividad().getEstadoActividadsigied();
                        ced.getActividad().getEstadoActividadhuelga();

                        ced.getActividad().setUrlFormato(ced.getActividad().getUrlFormato().concat("?codmod=").concat(codmod).concat("&anexo=").concat(anexo).concat("&nivel=").concat(nivel).concat("&codlocal=").concat(codlocal).concat("&codinst=").concat(codinst).concat("&nomactiv=").concat(ced.getActividad().getNombre()));

                        int pos = Collections.binarySearch(envios, act, buscarEnvio);
                        if (pos >= 0) {
                            EolIECensoConverter envio = envios.get(pos);
                            ced.setEnvio(true);
                            ced.setFechaEnvio(envio.getFechaEnvio());
                            ced.setStrFechaEnvio(Funciones.getFecha(envio.getFechaEnvio(), "dd/MM/yyyy HH:mm"));
                            ced.setNroEnvio(envio.getNroEnvio());
                            ced.getActividad().setUrlConstancia(ced.getActividad().getUrlConstancia().concat("?nivel=").concat(nivel).concat("&idEnvio=").concat(envio.getNroEnvio().toString()));
                        }

                    }
                    periodo.setAnio(new Integer(i).toString());
                    periodo.setActividades(cedulas);
                    periodos.add(periodo);
                }

                Respuesta rpt = new Respuesta(periodos);
                out.println(gson.toJson(rpt));

            } else {
                logger.info(":: EolCensoServlet.processRequest :: Execution finish.");
                out.println("Se detectó un error en los parámetros de acceso.");
            }

        } finally {
            logger.info(":: EolCensoServlet.processRequest :: Execution finish.");
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    static Gson createGsonFromBuilder(ExclusionStrategy exs) {
        GsonBuilder gsonbuilder = new GsonBuilder();
        gsonbuilder.setExclusionStrategies(exs);
        return gsonbuilder.serializeNulls().create();
    }
    static Comparator<EolIECensoConverter> buscarEnvio = new Comparator<EolIECensoConverter>() {

        public int compare(EolIECensoConverter o1, EolIECensoConverter o2) {

            return (o1.getIdActividad()).compareTo(o2.getIdActividad());
        }
    };

    private CensoFacade lookupCensoFacade() {
        try {
            Context c = new InitialContext();
            return (CensoFacade) c.lookup("java:global/eol-war/CensoFacade!pe.gob.minedu.escale.eol.portlets.ejb.CensoFacade");

        } catch (NamingException ne) {
            logger.error("ERROR Servlet: "+ne.getMessage());
            throw new RuntimeException(ne);
        }

    }
}
