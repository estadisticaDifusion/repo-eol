package pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2017;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.rest.constant.Constantes;

public class ConstanciaPdfID2017Servlet extends HttpServlet {

    @Resource(name = "eol")
    private DataSource eol;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = eol.getConnection();

            String codmod = request.getParameter("codMod");
            String nivMod = request.getParameter("nivel");

            //Long id_envio = verificarRegistro(codmod,nivMod);
            String id_envio= request.getParameter("idEnvio");
            String envio = "0";
            if(!id_envio.equals("") && id_envio != null){
                envio = id_envio;
            }

            Map<String, Object> params = new HashMap();
            params.put("ID_ENVIO", envio);

            System.out.println("COM MOD : "+codmod);
            System.out.println("NIV MOD : "+nivMod);
            System.out.println("ID ENVIO : "+id_envio);

            URL urlMaster;
            String report = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/matricula").getFile();
            String escudo = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/escudom.jpg").getFile();
            params.put("SUBREPORT_DIR", report);
            params.put("escudo", escudo);
            urlMaster = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/matricula/cedula_cid_2017.jasper");
            JasperReport masterReport = (JasperReport) JRLoader.loadObject(urlMaster);

            params.put("REPORT_LOCALE", new Locale("ES", "PE"));

            System.out.println("INICIO JASPER");
            byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
            response.setContentType("application/pdf");
            response.setContentLength(buffer.length);
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer, 0, buffer.length);
            ouputStream.flush();
            ouputStream.close();
            return;
        } catch (SQLException ex) {
            log(ex.getMessage(), ex);
            System.out.println("Error servlet 1");
        } catch (JRException ex) {
            log(ex.getMessage(), ex);
            System.out.println("Error servlet 2");
        } catch (Exception ex) {
            log(ex.getMessage(), ex);
            System.out.println("Error servlet 3");
        } finally {
            if (conn != null) {
                try {
                    if (!conn.isClosed()) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    log(ex.getMessage(), ex);
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }

    private Long verificarRegistro(String codmod,String nivMod) {
        Long idenvio = 0L;
        try {
            String path = Constantes.BASE_URI_EOL_WS+"censo2017/cedulaid?codmod=" + codmod +"&nivel="+nivMod;
            System.out.println("PATH : "+path);
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/text");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;
            while ((output = br.readLine()) != null) {
                idenvio = Long.parseLong(output);
                System.out.println("IN :" + idenvio);
            }
            conn.disconnect();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            idenvio = 0L;
            System.out.println("Error metodo 1");
        } catch (IOException e) {
            e.printStackTrace();
            idenvio = 0L;
            System.out.println("Error metodo 2");
        }
        System.out.println("ID retorno : "+idenvio);
        return idenvio;
    }
}
