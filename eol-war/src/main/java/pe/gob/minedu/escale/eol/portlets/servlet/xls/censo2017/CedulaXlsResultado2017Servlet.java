package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2017;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.util.MessageResources;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Resultado2017Cabecera;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Resultado2017Detalle;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2017.Resultado2017Fila;
import pe.gob.minedu.escale.eol.estadistica.ejb.censo2017.Resultado2017Facade;
import pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml.Campo;
import pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml.Campos;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.rest.client.PadronClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativaIE;

/**
 *
 * @author JBEDRILLANA
 */
public class CedulaXlsResultado2017Servlet extends HttpServlet {

	static final Logger LOGGER = Logger.getLogger(CedulaXlsResultado2017Servlet.class.getName());

	@EJB
	private Resultado2017Facade resultado2017Facade;

	@Resource(name = "eol")
	private DataSource eol;

	// @EJB
	// private EnvioDocumentosFacade envioFacade;

	private MessageResources res = MessageResources.getMessageResources("eol");
	private static String rutaXML = "/pe/gob/minedu/escale/eol/portlets/servlet/xls/censo2017/resource/campos_resultado_%1$s.xml";
	// public static Map<String, Campos> mapCampos = new HashMap<String, Campos>();
	public static Map<String, Campos> mapCampos;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nivMod = request.getParameter("nivel");
		String codMod = request.getParameter("codmod");
		String anexo = request.getParameter("anexo");
		String idEnvio = request.getParameter("idEnvio");
		String nombreactiv = request.getParameter("nomactiv");
		String cedulaPart = "";
		boolean esdecarga = true;

		String nroCed = Funciones.cedulaNivModResultado(nivMod);

		if (nroCed.toUpperCase().equals("3BP") || nroCed.toUpperCase().equals("3BS")
				|| nroCed.toUpperCase().equals("4BI") || nroCed.toUpperCase().equals("4BA")) {
			if (nombreactiv.contains("RECUPERA"))
				cedulaPart = "2";
			else
				cedulaPart = "1";
		}

		PadronClient pCli = new PadronClient(codMod, anexo);
		InstitucionEducativaIE ie = pCli.get_JSON(InstitucionEducativaIE.class);

		// InputStream is =
		// getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/resultado/CensoResultadoCed".concat(nroCed.toLowerCase()).concat(cedulaPart).concat("_2017.xls"));
		String rutaRaiz = getServletContext().getRealPath(
				"/WEB-INF/classes/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/resultado/CensoResultadoCed"
						+ nroCed.toLowerCase() + cedulaPart + "_2017.xls");
		InputStream is = new FileInputStream(rutaRaiz);

		HSSFWorkbook hssfw = new HSSFWorkbook(is);
		llenarCabecera(hssfw, ie, nivMod);

		if (nroCed.toUpperCase().equals("3BP") || nroCed.toUpperCase().equals("3BS")
				|| nroCed.toUpperCase().equals("4BI") || nroCed.toUpperCase().equals("4BA")) {

			if (nombreactiv.contains("RECUPERA")) {
				Resultado2017Cabecera resultado2017 = resultado2017Facade.findByCodModAnNiv(codMod, anexo, nivMod);

				esdecarga = (resultado2017.getVarrec() != null ? (resultado2017.getVarrec().equals("0") ? false : true)
						: false);
/* OM
				if (esdecarga)
					llenarCampos(hssfw, resultado2017.getDetalle(), nivMod);
*/
			}
		}

		if (esdecarga) {
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition",
					"attachment;filename=\"CensoResultadoCed".concat(nroCed).concat("_2017.xls\""));
			OutputStream os = response.getOutputStream();
			hssfw.write(os);
			os.close();
		} else {
			descargarPdf(codMod, response);
		}
		/*
		 * else{ response.setContentType("text/html;charset=UTF-8");
		 * response.sendRedirect(request.getHeader("Referer"));
		 * 
		 * PrintWriter pw = new PrintWriter(response.getOutputStream()); pw.
		 * write("<html><body><script>alert('No es posible realizar la descarga del formato de recuperación debido a que no le corresponde reportar datos de recuperación.')</script></body></html>"
		 * ); pw.close(); response.getOutputStream().flush();
		 * response.getOutputStream().close(); response.flushBuffer();
		 * 
		 * }
		 */
		/*
		 * else{ response.setContentType("text/html;charset=UTF-8");
		 * response.sendRedirect(request.getHeader("Referer")); PrintWriter out =
		 * response.getWriter(); out.println("<script type=\"text/javascript\">"); out.
		 * println("alert('No es posible realizar la descarga del formato de recuperación debido a que no le corresponde reportar datos de recuperación.');"
		 * ); out.println("</script>");
		 * 
		 * }
		 */

	}

	public void descargarPdf(String codigoModular, HttpServletResponse response) {
		Connection conn = null;
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			conn = eol.getConnection();
			// params.put("ID_ENVIO", codigoIns);
			params.put("COD_MOD", codigoModular);
			String report = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/resultado")
					.getFile();
			URL urlMaster = getClass().getResource(
					"/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/resultado/cedula-recuperacion.jasper");
			params.put("SUBREPORT_DIR", report);
			JasperReport masterReport = (JasperReport) JRLoader.loadObject(urlMaster);
			// String escudo =
			// getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/matricula/Escudo.JPG").getFile();
			params.put("REPORT_LOCALE", new java.util.Locale("ES", "PE"));
			// params.put("escudo", escudo);

			byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=\"notificacionresultado.pdf\"");
			response.setContentLength(buffer.length);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(buffer, 0, buffer.length);
			ouputStream.flush();
			ouputStream.close();

		} catch (JRException ex) {
			log(ex.getMessage(), ex);
		} catch (Exception ex) {
			log(ex.getMessage(), ex);
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException ex) {
					log(ex.getMessage(), ex);
				}
			}
		}

	}

	public void llenarCampos(HSSFWorkbook hssfw, Map<String, Resultado2017Detalle> map, String nivMod) {
		mapCampos = new HashMap<String, Campos>();
		String nroCed = Funciones.cedulaNivModResultado(nivMod);
		HSSFSheet sheetCuadros = hssfw.getSheet("Cedula".concat(nroCed.toUpperCase()));

		if (nroCed.equals("3BP"))
			this.llenarDatosPrellenado3BPC201(sheetCuadros, map);
		if (nroCed.equals("3BS"))
			this.llenarDatosPrellenado3BSC201(sheetCuadros, map);
		if (nroCed.equals("4BI"))
			this.llenarDatosPrellenado4BIC201(sheetCuadros, map);
		if (nroCed.equals("4BA"))
			this.llenarDatosPrellenado4BAC201P201(sheetCuadros, map);
		// **********INSERCION************//

		/*
		 * if (!mapCampos.containsKey(nroCed)) { mapCampos.put(nroCed, new
		 * Campos(CedulaXlsResultado2017Servlet.class.getResource(String.format(rutaXML,
		 * nroCed.toUpperCase())).getFile())); }
		 * 
		 * Class<Resultado2017Fila> clazz = Resultado2017Fila.class;
		 * 
		 * Campo campoTmp = new Campo();
		 * 
		 * Object valorH = null; Object valorM = null;
		 * 
		 * if (mapCampos.get(nroCed) != null && mapCampos.get(nroCed).getCampos() !=
		 * null) { for (Campo cuadro : mapCampos.get(nroCed).getCampos()) {
		 * Resultado2017Detalle detalle = map.get(cuadro.getEtiqueta()); List listCampos
		 * = Arrays.asList(cuadro.getDatos()); if (detalle != null) { for
		 * (Resultado2017Fila fila : detalle.getFilas()) {
		 * campoTmp.setPropiedad(fila.getTipdato()); int pos =
		 * Collections.binarySearch(listCampos, campoTmp, buscarCampo);
		 * 
		 * if (pos >= 0) { Campo campoSel = (Campo) listCampos.get(pos); int inicioC =
		 * new Integer(campoSel.getOldValue()); String[] strCol =
		 * campoSel.getCelC().split(":");
		 * 
		 * for (String parCol : strCol) { Method m; String[] strPar = parCol.split("-");
		 * try { String datoFilaH = "getDato".concat(Funciones.getNumFormato(inicioC,
		 * "00")).concat("h"); String datoFilaM =
		 * "getDato".concat(Funciones.getNumFormato(inicioC, "00")).concat("m"); m =
		 * clazz.getMethod(datoFilaH); valorH = m.invoke(fila); m =
		 * clazz.getMethod(datoFilaM); valorM = m.invoke(fila); if (valorH != null) {
		 * setCell(sheetCuadros, strPar[0], campoSel.getCelF(),
		 * Integer.parseInt(valorH.toString())); } if (valorM != null) {
		 * setCell(sheetCuadros, strPar[1], campoSel.getCelF(),
		 * Integer.parseInt(valorM.toString())); }
		 * 
		 * } catch (Exception ex) {
		 * Logger.getLogger(CedulaXlsResultado2017Servlet.class.getName()).log(Level.
		 * SEVERE, null, ex); } inicioC++; } }
		 * 
		 * } } } }
		 */

	}

	public void llenarCabecera(HSSFWorkbook hssfw, InstitucionEducativaIE ie, String nivMod) {
		String nroCed = Funciones.cedulaNivModResultado(nivMod);
		HSSFSheet sheetCabecera = hssfw.getSheet("Cedula".concat(nroCed.toUpperCase()));

		if (nroCed.toUpperCase().equals("1B")) {
			setCell(sheetCabecera, "F", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "F", 15, ie.getCentroEducativo());
			setCell(sheetCabecera, "M", 13, ie.getCodlocal());
			setCell(sheetCabecera, "D",
					nivMod.equals("A1") ? 21 : (nivMod.equals("A2") ? 21 : (nivMod.equals("A3") ? 23 : 0)), "X");
		} else if (nroCed.toUpperCase().equals("2B")) {
			setCell(sheetCabecera, "F", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "F", 15, ie.getCentroEducativo());
			// setCell(sheetCabecera, "D", 25, "X");

			/*
			 * if (ie.getTipoprog() != null) { switch (ie.getTipoprog().charAt(0)) { case
			 * '1': setCell(sheetCabecera, "AH", 33, "X"); break; case '2':
			 * setCell(sheetCabecera, "U", 39, "X"); break; case '3': setCell(sheetCabecera,
			 * "U", 37, "X"); break; case '4': setCell(sheetCabecera, "U", 43, "X"); break;
			 * case '5': setCell(sheetCabecera, "U", 33, "X"); break; case '6':
			 * setCell(sheetCabecera, "AH", 39, "X"); break; case '7':
			 * setCell(sheetCabecera, "AH", 36, "X"); break; case '8':
			 * setCell(sheetCabecera, "AH", 41, "X"); break;
			 * 
			 * } }
			 */

		} else if (nroCed.toUpperCase().equals("3BP")) {
			setCell(sheetCabecera, "G", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "L", 13, ie.getAnexo());
			setCell(sheetCabecera, "S", 13, ie.getCodlocal());
			setCell(sheetCabecera, "G", 15, ie.getCentroEducativo());
			// setCell(sheetCabecera, "D",
			// nivMod.equals(NivelModalidadEnum.PRIMARIA_DE_MENORES.getCod()) ? 24 :
			// (nivMod.equals(NivelModalidadEnum.SECUNDARIA_DE_MENORES.getCod()) ? 26 : 0),
			// "X");

			// llenarDatosReportados(sheetCabecera,ie.getCodMod(),ie.getAnexo(),ie.getNivMod());
		} else if (nroCed.toUpperCase().equals("3BS")) {
			setCell(sheetCabecera, "F", 13, ie.getCodigoModular());
			// setCell(sheetCabecera, "L", 13, ie.getAnexo());
			setCell(sheetCabecera, "Q", 13, ie.getCodlocal());
			setCell(sheetCabecera, "F", 15, ie.getCentroEducativo());
			// setCell(sheetCabecera, "D",
			// nivMod.equals(NivelModalidadEnum.PRIMARIA_DE_MENORES.getCod()) ? 24 :
			// (nivMod.equals(NivelModalidadEnum.SECUNDARIA_DE_MENORES.getCod()) ? 26 : 0),
			// "X");

			// llenarDatosReportados(sheetCabecera,ie.getCodMod(),ie.getAnexo(),ie.getNivMod());
		} else if (nroCed.toUpperCase().equals("4BA")) {
			setCell(sheetCabecera, "H", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "V", 13, ie.getCodlocal());
			setCell(sheetCabecera, "H", 15, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("4BI")) {
			setCell(sheetCabecera, "H", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "W", 13, ie.getCodlocal());
			setCell(sheetCabecera, "H", 15, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("5B")) {
			setCell(sheetCabecera, "F", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "U", 13, ie.getCodlocal());
			setCell(sheetCabecera, "F", 15, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("6B")) {
			setCell(sheetCabecera, "H", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "W", 13, ie.getCodlocal());
			setCell(sheetCabecera, "H", 15, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("7B")) {
			setCell(sheetCabecera, "E", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "S", 13, ie.getCodlocal());
			setCell(sheetCabecera, "E", 15, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("8BI")) {
			setCell(sheetCabecera, "F", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "Q", 13, ie.getCodlocal());
			setCell(sheetCabecera, "F", 15, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("8BP")) {
			setCell(sheetCabecera, "G", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "R", 13, ie.getCodlocal());
			setCell(sheetCabecera, "G", 15, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("9B")) {
			setCell(sheetCabecera, "G", 13, ie.getCodigoModular());
			setCell(sheetCabecera, "R", 13, ie.getCodlocal());
			setCell(sheetCabecera, "G", 15, ie.getCentroEducativo());
		}
	}

	public void llenarCabeceraEnvio(HSSFWorkbook hssfw, Resultado2017Cabecera resultado2017, String nivMod) {
		String nroCed = Funciones.cedulaNivModResultado(nivMod);
		HSSFSheet sheetCabecera = hssfw.getSheet("Cedula".concat(nroCed.toUpperCase()));

		if (nroCed.toUpperCase().equals("1B")) {
			setCell(sheetCabecera, "F", 18, resultado2017.getDistrito());
			setCell(sheetCabecera, "K", 23, resultado2017.getApeDir());
			setCell(sheetCabecera, "K", 25, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AA", 25, resultado2017.getTelefono());
			// setCell(sheetCabecera, "AO", 37,
			// censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));

		} else if (nroCed.toUpperCase().equals("2B")) {
			setCell(sheetCabecera, "F", 18, resultado2017.getDistrito());
			setCell(sheetCabecera, "K", 23, resultado2017.getApeDir());
			setCell(sheetCabecera, "K", 25, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AM", 23, resultado2017.getTelefono());
			// setCell(sheetCabecera, "AO", 51,
			// censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));

		} else if (nroCed.toUpperCase().equals("3BP") || nroCed.toUpperCase().equals("3BS")) {
			setCell(sheetCabecera, "G", 18, resultado2017.getDistrito());
			setCell(sheetCabecera, "P", 24, resultado2017.getApeDir());
			setCell(sheetCabecera, "P", 26, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AV", 24, resultado2017.getTelefono());
			// if(resultado2017.getSituacion()!=null)
			// setCell(sheetCabecera, "AO", 43,
			// censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));
		} else if (nroCed.toUpperCase().equals("4B")) {
			setCell(sheetCabecera, "H", 18, resultado2017.getDistrito());
			setCell(sheetCabecera, "S", 23, resultado2017.getApeDir());
			setCell(sheetCabecera, "S", 25, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AU", 24, resultado2017.getTelefono());
			// setCell(sheetCabecera, "AO", 30,
			// censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));

		} else if (nroCed.toUpperCase().equals("5B")) {
			setCell(sheetCabecera, "Q", 16, resultado2017.getDistrito());
			setCell(sheetCabecera, "Q", 21, resultado2017.getApeDir());
			setCell(sheetCabecera, "Q", 23, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AU", 23, resultado2017.getTelefono());
			/*
			 * if (resultado2017.getSituacion() != null &&
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())) !=
			 * null) { setCell(sheetCabecera, "AO", 37,
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));
			 * }
			 */
		} else if (nroCed.toUpperCase().equals("6B")) {
			setCell(sheetCabecera, "H", 18, resultado2017.getDistrito());
			setCell(sheetCabecera, "K", 22, resultado2017.getApeDir());
			setCell(sheetCabecera, "T", 22, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AS", 23, resultado2017.getTelefono());
			/*
			 * if (resultado2017.getSituacion() != null &&
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())) !=
			 * null) { setCell(sheetCabecera, "AR", 28,
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));
			 * }
			 */
		} else if (nroCed.toUpperCase().equals("7B")) {
			setCell(sheetCabecera, "F", 19, resultado2017.getDistrito());
			setCell(sheetCabecera, "H", 23, resultado2017.getApeDir());
			setCell(sheetCabecera, "P", 23, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AU", 23, resultado2017.getTelefono());
			/*
			 * if (resultado2017.getSituacion() != null &&
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())) !=
			 * null) { setCell(sheetCabecera, "AO", 30,
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));
			 * }
			 */
		} else if (nroCed.toUpperCase().equals("8BI") || nroCed.toUpperCase().equals("8BS")) {
			setCell(sheetCabecera, "G", 19, resultado2017.getDistrito());
			setCell(sheetCabecera, "P", 24, resultado2017.getApeDir());
			setCell(sheetCabecera, "P", 26, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AZ", 23, resultado2017.getTelefono());
			/*
			 * if (resultado2017.getSituacion() != null &&
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())) !=
			 * null) { setCell(sheetCabecera, "AT", 39,
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));
			 * }
			 */
		} else if (nroCed.toUpperCase().equals("9B")) {
			setCell(sheetCabecera, "G", 18, resultado2017.getDistrito());
			setCell(sheetCabecera, "H", 22, resultado2017.getApeDir());
			setCell(sheetCabecera, "O", 22, resultado2017.getNomDir());
			// setCell(sheetCabecera, "AO", 23, resultado2017.getTelefono());
			/*
			 * if (resultado2017.getSituacion() != null &&
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())) !=
			 * null) { setCell(sheetCabecera, "AP", 30,
			 * censoEtiqueta.getString("situacion.".concat(resultado2017.getSituacion())));
			 * }
			 */
		}

	}

	public void llenarDatosPrellenado1BC101(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 51, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 51, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 51, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 51, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 51, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 51, Integer.parseInt(fila[14].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 52, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 52, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 52, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 52, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 52, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 52, Integer.parseInt(fila[14].toString()));
				break;
			case 3:
				setCell(sheetCabecera, "AD", 53, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 53, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 53, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 53, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 53, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 53, Integer.parseInt(fila[14].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 54, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 54, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 54, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 54, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 54, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 54, Integer.parseInt(fila[14].toString()));
				break;

			}
		}

	}

	public void llenarDatosPrellenado1BC102(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 70, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 70, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 70, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 70, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 70, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 70, Integer.parseInt(fila[14].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 71, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 71, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 71, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 71, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 71, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 71, Integer.parseInt(fila[14].toString()));
				break;
			case 3:
				setCell(sheetCabecera, "AD", 72, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 72, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 72, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 72, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 72, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 72, Integer.parseInt(fila[14].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 73, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 73, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 73, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 73, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 73, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 73, Integer.parseInt(fila[14].toString()));
				break;

			case 7:
				setCell(sheetCabecera, "AD", 74, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 74, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 74, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 74, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 74, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 74, Integer.parseInt(fila[14].toString()));
				break;

			case 8:
				setCell(sheetCabecera, "AD", 76, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 76, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 76, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 76, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 76, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 76, Integer.parseInt(fila[14].toString()));
				break;

			}
		}

	}

	public void llenarDatosPrellenado3BPC201(HSSFSheet sheetCuadro, Map<String, Resultado2017Detalle> map) {

		List<Resultado2017Fila> filas;
		int row = 0;

		Set<String> keys = map.keySet();
		for (Iterator<String> it = keys.iterator(); it.hasNext();) {
			String key = it.next();
/* OM
			filas = map.get(key).getFilas();
			if (key.equals("C101")) {

				row = 24;

				for (Resultado2017Fila fila : filas) {
					String tipDat = fila.getTipdato().toString();
					int tipDatNum = Integer.parseInt(tipDat);
					switch (tipDatNum) {
					case 2:
						setCell(sheetCuadro, "J", row, fila.getDato01h());
						setCell(sheetCuadro, "K", row, fila.getDato01m());
						setCell(sheetCuadro, "L", row, fila.getDato02h());
						setCell(sheetCuadro, "M", row, fila.getDato02m());
						setCell(sheetCuadro, "N", row, fila.getDato03h());
						setCell(sheetCuadro, "O", row, fila.getDato03m());
						setCell(sheetCuadro, "P", row, fila.getDato04h());
						setCell(sheetCuadro, "Q", row, fila.getDato04m());
						setCell(sheetCuadro, "R", row, fila.getDato05h());
						setCell(sheetCuadro, "S", row, fila.getDato05m());
						setCell(sheetCuadro, "T", row, fila.getDato06h());
						setCell(sheetCuadro, "U", row, fila.getDato06m());

						break;

					default:

						break;
					}
				}

			}
*/
		}

	}

	public void llenarDatosPrellenado3BSC201(HSSFSheet sheetCuadro, Map<String, Resultado2017Detalle> map) {

		List<Resultado2017Fila> filas;
		int row = 0;

		Set<String> keys = map.keySet();
		for (Iterator<String> it = keys.iterator(); it.hasNext();) {
			String key = it.next();
/*
			filas = map.get(key).getFilas();

			if (key.equals("C101")) {
				row = 24;

				for (Resultado2017Fila fila : filas) {
					String tipDat = fila.getTipdato().toString();
					int tipDatNum = Integer.parseInt(tipDat);
					switch (tipDatNum) {
					case 2:
						setCell(sheetCuadro, "J", row, fila.getDato01h());
						setCell(sheetCuadro, "K", row, fila.getDato01m());
						setCell(sheetCuadro, "L", row, fila.getDato02h());
						setCell(sheetCuadro, "M", row, fila.getDato02m());
						setCell(sheetCuadro, "N", row, fila.getDato03h());
						setCell(sheetCuadro, "O", row, fila.getDato03m());
						setCell(sheetCuadro, "P", row, fila.getDato04h());
						setCell(sheetCuadro, "Q", row, fila.getDato04m());
						setCell(sheetCuadro, "R", row, fila.getDato05h());
						setCell(sheetCuadro, "S", row, fila.getDato05m());

						break;

					default:

						break;
					}
				}
			}
*/
		}
	}

	public void llenarDatosPrellenado4BIC201(HSSFSheet sheetCuadro, Map<String, Resultado2017Detalle> map) {

		List<Resultado2017Fila> filas;
		int row = 0;

		Set<String> keys = map.keySet();
		for (Iterator<String> it = keys.iterator(); it.hasNext();) {
			String key = it.next();
/*
			filas = map.get(key).getFilas();

			if (key.equals("C101")) {

				row = 27;

				for (Resultado2017Fila fila : filas) {
					String tipDat = fila.getTipdato().toString();
					int tipDatNum = Integer.parseInt(tipDat);
					switch (tipDatNum) {
					case 2:
						setCell(sheetCuadro, "J", row, fila.getDato01h());
						setCell(sheetCuadro, "K", row, fila.getDato01m());
						setCell(sheetCuadro, "L", row, fila.getDato02h());
						setCell(sheetCuadro, "M", row, fila.getDato02m());
						setCell(sheetCuadro, "N", row, fila.getDato03h());
						setCell(sheetCuadro, "O", row, fila.getDato03m());
						setCell(sheetCuadro, "P", row, fila.getDato04h());
						setCell(sheetCuadro, "Q", row, fila.getDato04m());
						setCell(sheetCuadro, "R", row, fila.getDato05h());
						setCell(sheetCuadro, "S", row, fila.getDato05m());
						setCell(sheetCuadro, "T", row, fila.getDato06h());
						setCell(sheetCuadro, "U", row, fila.getDato06m());
						setCell(sheetCuadro, "V", row, fila.getDato07h());
						setCell(sheetCuadro, "W", row, fila.getDato07m());
						setCell(sheetCuadro, "X", row, fila.getDato08h());
						setCell(sheetCuadro, "Y", row, fila.getDato08m());
						setCell(sheetCuadro, "Z", row, fila.getDato09h());
						setCell(sheetCuadro, "AA", row, fila.getDato09m());
						setCell(sheetCuadro, "AB", row, fila.getDato10h());
						setCell(sheetCuadro, "AC", row, fila.getDato10m());

						break;

					default:

						break;
					}
				}

			}
			
*/
		}

	}

	public void llenarDatosPrellenado4BAC201P201(HSSFSheet sheetCuadro, Map<String, Resultado2017Detalle> map) {

		List<Resultado2017Fila> filas;
		int row = 0;

		Set<String> keys = map.keySet();
		for (Iterator<String> it = keys.iterator(); it.hasNext();) {
			String key = it.next();
/*
			filas = map.get(key).getFilas();

			if (key.equals("C101"))
				row = 25;
			if (key.equals("P101"))
				row = 39;

			if (key.equals("C101") || key.equals("P101")) {

				for (Resultado2017Fila fila : filas) {
					String tipDat = fila.getTipdato().toString();
					int tipDatNum = Integer.parseInt(tipDat);
					switch (tipDatNum) {
					case 2:
						setCell(sheetCuadro, "J", row, fila.getDato01h());
						setCell(sheetCuadro, "K", row, fila.getDato01m());
						setCell(sheetCuadro, "L", row, fila.getDato02h());
						setCell(sheetCuadro, "M", row, fila.getDato02m());
						setCell(sheetCuadro, "N", row, fila.getDato03h());
						setCell(sheetCuadro, "O", row, fila.getDato03m());
						setCell(sheetCuadro, "P", row, fila.getDato04h());
						setCell(sheetCuadro, "Q", row, fila.getDato04m());
						setCell(sheetCuadro, "R", row, fila.getDato05h());
						setCell(sheetCuadro, "S", row, fila.getDato05m());
						setCell(sheetCuadro, "T", row, fila.getDato06h());
						setCell(sheetCuadro, "U", row, fila.getDato06m());
						setCell(sheetCuadro, "V", row, fila.getDato07h());
						setCell(sheetCuadro, "W", row, fila.getDato07m());
						setCell(sheetCuadro, "X", row, fila.getDato08h());
						setCell(sheetCuadro, "Y", row, fila.getDato08m());
						setCell(sheetCuadro, "Z", row, fila.getDato09h());
						setCell(sheetCuadro, "AA", row, fila.getDato09m());
						setCell(sheetCuadro, "AB", row, fila.getDato10h());
						setCell(sheetCuadro, "AC", row, fila.getDato10m());

						break;

					default:

						break;
					}
				}
			}
*/
		}

	}

	public void llenarDatosPrellenado3BC101(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 52, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 52, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 52, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 52, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 52, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 52, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 52, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 52, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 52, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 52, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 52, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 52, Integer.parseInt(fila[16].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 53, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 53, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 53, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 53, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 53, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 53, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 53, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 53, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 53, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 53, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 53, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 53, Integer.parseInt(fila[16].toString()));

				break;
			case 3:
				setCell(sheetCabecera, "AD", 54, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 54, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 54, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 54, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 54, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 54, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 54, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 54, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 54, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 54, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 54, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 54, Integer.parseInt(fila[16].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 55, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 55, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 55, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 55, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 55, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 55, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 55, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 55, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 55, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 55, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 55, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 55, Integer.parseInt(fila[16].toString()));
				break;

			case 5:
				setCell(sheetCabecera, "AD", 56, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 56, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 56, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 56, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 56, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 56, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 56, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 56, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 56, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 56, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 56, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 56, Integer.parseInt(fila[16].toString()));
				break;
			case 6:
				setCell(sheetCabecera, "AD", 57, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 57, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 57, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 57, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 57, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 57, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 57, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 57, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 57, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 57, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 57, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 57, Integer.parseInt(fila[16].toString()));
				break;
			}
		}

	}

	public void llenarDatosPrellenado3BC102(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 71, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 71, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 71, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 71, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 71, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 71, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 71, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 71, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 71, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 71, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 71, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 71, Integer.parseInt(fila[16].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 72, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 72, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 72, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 72, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 72, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 72, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 72, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 72, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 72, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 72, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 72, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 72, Integer.parseInt(fila[16].toString()));

				break;
			case 3:
				setCell(sheetCabecera, "AD", 73, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 73, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 73, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 73, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 73, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 73, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 73, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 73, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 73, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 73, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 73, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 73, Integer.parseInt(fila[16].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 74, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 74, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 74, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 74, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 74, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 74, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 74, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 74, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 74, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 74, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 74, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 74, Integer.parseInt(fila[16].toString()));
				break;

			case 5:
				setCell(sheetCabecera, "AD", 75, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 75, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 75, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 75, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 75, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 75, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 75, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 75, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 75, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 75, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 75, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 75, Integer.parseInt(fila[16].toString()));
				break;
			case 6:
				setCell(sheetCabecera, "AD", 76, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 76, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 76, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 76, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 76, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 76, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 76, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 76, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 76, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 76, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 76, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 76, Integer.parseInt(fila[16].toString()));
				break;
			case 7:
				setCell(sheetCabecera, "AD", 78, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 78, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 78, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 78, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 78, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 78, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 78, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 78, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 78, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 78, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 78, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 78, Integer.parseInt(fila[16].toString()));
				break;
			}
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				LOGGER.log(Level.WARNING,
						String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static void setCell(HSSFSheet sheet, int col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(col).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				LOGGER.log(Level.WARNING,
						String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	static Comparator<Campo> buscarCampo = new Comparator<Campo>() {
		public int compare(Campo o1, Campo o2) {
			return (o1.getPropiedad()).compareTo(o2.getPropiedad());
		}
	};

}
