/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2014;

//import pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2011.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.eol.portlets.util.enumerations.NivelModalidadEnum;
/*import com.liferay.portlet.documentlibrary.model.*;
 import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
 import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
 import com.liferay.portal.kernel.exception.PortalException;
 import com.liferay.portal.kernel.exception.SystemException;
 */

/**
 *
 * @author JMATAMOROS
 */
public class CedulaXlsMatricula2014Servlet extends HttpServlet {

	private Logger logger = Logger.getLogger(CedulaXlsMatricula2014Servlet.class);

	static final String CEDULA_MATRICULA_DESC = "CensoMatriculaDocentesRecursosCed";
	static final String PERIODO = "2014";
	static final String PATH_DIR = "2014/cedulas";

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@EJB
	private EnvioDocumentosFacade envioFacade;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String preCed = "";
		String nivMod = request.getParameter("nivel");
		String anexo = request.getParameter("anexo");
		String codMod = request.getParameter("codmod");
		logger.info("********** DESCARGA  {0}***********" + codMod);
		preCed = Funciones.cedulaMatricula2014NivMod(nivMod);
		InputStream is = getClass().getResourceAsStream(
				"/pe/gob/minedu/escale/eol/portlets/plantilla/censo2014/matricula/CensoMatriculaDocentesRecursosCed"
						+ preCed.toUpperCase() + "_" + PERIODO + ".xls");
		CentroEducativo ie = padronFacade.obtainByCodMod(codMod, anexo);

		if (is != null) {
			HSSFWorkbook hssfw = new HSSFWorkbook(is);
			if (preCed.equals("1A")) {
				llenadoCedula1A(hssfw, ie, nivMod);
			} else if (preCed.equals("2A")) {
				llenadoCedula2A(hssfw, ie);
			} else if (preCed.equals("3A")) {
				llenadoCedula3A(hssfw, ie);
			} else if (preCed.equals("4A")) {
				llenadoCedula4A(hssfw, ie);
			} else if (preCed.equals("5A")) {
				llenadoCedula5A(hssfw, ie);
			} else if (preCed.equals("6A")) {
				llenadoCedula6A(hssfw, ie);
			} else if (preCed.equals("7A")) {
				llenadoCedula7A(hssfw, ie);
			} else if (preCed.equals("8A")) {
				llenadoCedula8A(hssfw, ie);
			} else if (preCed.equals("9A")) {
				llenadoCedula9A(hssfw, ie);
			}

			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", String.format(
					"attachment;filename=\"CensoMatriculaDocentesRecursosCed%s_2014.xls\"", preCed.toUpperCase()));
			OutputStream os = response.getOutputStream();
			try {
				hssfw.write(os);

			} catch (IOException ioe) {

				ioe.printStackTrace();
			}
			os.close();
		}
	}

	public void llenadoCedula1A(HSSFWorkbook hssfw, CentroEducativo ie, String niv_mod) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		HSSFSheet sheet300 = hssfw.getSheet("C200");
		setCell(sheet100_200, "J", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "J", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "AK", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "AI", 30, ie.getTelefono());// Telefono
		if (ie.getNivMod().equals(NivelModalidadEnum.INICIAL_CUNA.getCod())) {
			setCell(sheet100_200, "F", 40, "X");
		} else if (ie.getNivMod().equals(NivelModalidadEnum.INICIAL_JARDIN.getCod())) {
			setCell(sheet100_200, "P", 40, "X");
		} else if (ie.getNivMod().equals(NivelModalidadEnum.INICIAL_CUNA_Y_JARDIN.getCod())) {
			setCell(sheet100_200, "X", 40, "X");
			// }else if("1".equals((ie.getProgarti()))){
			// setCell(sheet100_200, "AK", 56, "X");
		}

		List<Object[]> list301 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(), "c201", niv_mod,
				"c0".concat(Funciones.cedulaMatricula2014NivMod(niv_mod).toLowerCase()), PERIODO);
		List<Object[]> list302 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(), "c202", niv_mod,
				"c0".concat(Funciones.cedulaMatricula2014NivMod(niv_mod).toLowerCase()), PERIODO);
		List<Object[]> list304 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(), "c205", niv_mod,
				"c0".concat(Funciones.cedulaMatricula2014NivMod(niv_mod).toLowerCase()), PERIODO);

		this.llenar301_1A(list301, sheet300);
		this.llenar302_1A(list302, sheet300);
		this.llenar304_1A(list304, sheet300);
	}

	public void llenadoCedula2A(HSSFWorkbook hssfw, CentroEducativo ie) {
		// nomIE = cEdu.getCenEdu() != null ? cEdu.getCenEdu() : "";
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");

		setCell(sheet100_200, "J", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "J", 30, ie.getCenEdu());// Nombre
		// setCell(sheet100_200, "AK", 28, ie.getCodlocal());//Codlocal
		// setCell(sheet100_200, "AI", 30, ie.getTelefono());//Telefono
	}

	public void llenadoCedula3A(HSSFWorkbook hssfw, CentroEducativo ie) {

		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		setCell(sheet100_200, "J", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "W", 28, ie.getAnexo());// Anexo
		setCell(sheet100_200, "J", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "AK", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "AI", 30, ie.getTelefono());// Telefono

		if (ie.getNivMod().equals(NivelModalidadEnum.PRIMARIA_DE_MENORES.getCod())) {
			setCell(sheet100_200, "F", 38, "X");// Primaria
		} else if (ie.getNivMod().equals(NivelModalidadEnum.SECUNDARIA_DE_MENORES.getCod())) {
			setCell(sheet100_200, "F", 40, "X");// Secundaria

		}

		HSSFSheet sheet = hssfw.getSheet("C200");

		// EnvioDocumentosFacade servEnvio = lookupEnvioDocumentosFacade();
		List<Object[]> list301 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(), "c201", ie.getNivMod(),
				"c0".concat(Funciones.cedulaMatricula2014NivMod(ie.getNivMod()).toLowerCase()), PERIODO);
		List<Object[]> list302 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(), "c202", ie.getNivMod(),
				"c0".concat(Funciones.cedulaMatricula2014NivMod(ie.getNivMod()).toLowerCase()), PERIODO);
		List<Object[]> list303 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(), "c203", ie.getNivMod(),
				"c0".concat(Funciones.cedulaMatricula2014NivMod(ie.getNivMod()).toLowerCase()), PERIODO);
		// List<Object[]> list307 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(),
		// "c307", ie.getNivMod(),
		// "c0".concat(Funciones.cedulaMatricula2014NivMod(ie.getNivMod()).toLowerCase()),
		// PERIODO);
		// List<List> list310 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(),
		// "c310", ie.getNivMod(), Funciones.cedulaMatricula2012NivMod(ie.getNivMod()));
		List<Object[]> list311 = envioFacade.getPreLlenadoMatricula(ie.getCodMod(), "c209", ie.getNivMod(),
				"c0".concat(Funciones.cedulaMatricula2014NivMod(ie.getNivMod()).toLowerCase()), PERIODO);

		this.llenar301(list301, sheet);
		this.llenar302(list302, sheet);
		this.llenar303(list303, sheet);
		// this.llenar307(list307, sheet);
		// this.llenar310(list310, sheet);
		this.llenar309(list311, sheet);

	}

	public void llenadoCedula4A(HSSFWorkbook hssfw, CentroEducativo ie) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		setCell(sheet100_200, "J", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "J", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "AK", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "AI", 30, ie.getTelefono());// Telefono
	}

	public void llenadoCedula5A(HSSFWorkbook hssfw, CentroEducativo ie) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		setCell(sheet100_200, "I", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "I", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "AJ", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "AH", 30, ie.getTelefono());// Telefono
	}

	public void llenadoCedula6A(HSSFWorkbook hssfw, CentroEducativo ie) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		setCell(sheet100_200, "G", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "G", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "O", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "N", 30, ie.getTelefono());// Telefono
	}

	public void llenadoCedula7A(HSSFWorkbook hssfw, CentroEducativo ie) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		setCell(sheet100_200, "E", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "E", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "L", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "K", 30, ie.getTelefono());// Telefono
	}

	public void llenadoCedula8A(HSSFWorkbook hssfw, CentroEducativo ie) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		setCell(sheet100_200, "I", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "I", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "AJ", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "AH", 30, ie.getTelefono());// Telefono
	}

	public void llenadoCedula9A(HSSFWorkbook hssfw, CentroEducativo ie) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		setCell(sheet100_200, "H", 28, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "H", 30, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "P", 28, ie.getCodlocal());// Codlocal
		setCell(sheet100_200, "O", 30, ie.getTelefono());// Telefono
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	public void llenar301(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 12;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 14;
		int ubicCampo = 6;
		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "G";
				break;
			case 2:
				nomCol = "H";
				break;
			case 3:
				nomCol = "I";
				break;
			case 4:
				nomCol = "J";
				break;
			case 5:
				nomCol = "K";
				break;
			case 6:
				nomCol = "L";
				break;
			case 7:
				nomCol = "M";
				break;
			case 8:
				nomCol = "N";
				break;
			case 9:
				nomCol = "O";
				break;
			case 10:
				nomCol = "P";
				break;
			case 11:
				nomCol = "Q";
				break;
			case 12:
				nomCol = "R";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					String tipDato = (lista.get(j - 1))[4].toString();
					int ubiTipDato = new Integer(tipDato);

					if (ubiTipDato > 1 && lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int valor = ((Number) (lista.get(j - 1))[ubicCampo + i]).intValue();
						if (valor > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato - 1, valor);
						}
					}
				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}
		/*
		 * for (int j = 1; j <= rowExcCant; j++) { String tipDato = (lista.get(j -
		 * 1)).get(4).toString(); int ubiTipDato = new Integer(tipDato); if (ubiTipDato
		 * > 1) { try { totH = totH + ((Number) (lista.get(j - 1)).get(ubicCampo -
		 * 1)).intValue(); totM = totM + ((Number) (lista.get(j -
		 * 1)).get(ubicCampo)).intValue(); } catch
		 * (java.lang.ArrayIndexOutOfBoundsException e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } } setCell(sheet, "D", 10,
		 * totH); setCell(sheet, "E", 10, totM);
		 */
	}

	public void llenar302(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 13;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 46;
		int ubicCampo = 6;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "H";
				break;
			case 2:
				nomCol = "I";
				break;
			case 3:
				nomCol = "J";
				break;
			case 4:
				nomCol = "K";
				break;
			case 5:
				nomCol = "L";
				break;
			case 6:
				nomCol = "M";
				break;
			case 7:
				nomCol = "N";
				break;
			case 8:
				nomCol = "O";
				break;
			case 9:
				nomCol = "P";
				break;
			case 10:
				nomCol = "Q";
				break;
			case 11:
				nomCol = "R";
				break;
			case 12:
				nomCol = "S";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					String tipDato = (lista.get(j - 1))[4].toString();
					int ubiTipDato = new Integer(tipDato);
					if (ubiTipDato > 1 && lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int valor = ((Number) (lista.get(j - 1))[ubicCampo + i]).intValue();
						if (valor > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato - 1, valor);
						}
					}
					/*
					 * if(lista.get(j-1)!=null && (lista.get(j - 1)).get(ubicCampo + i)!=null) { }
					 */
				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}
		/*
		 * for (int j = 1; j <= rowExcCant; j++) { String tipDato = (lista.get(j -
		 * 1)).get(4).toString(); int ubiTipDato = new Integer(tipDato); if (ubiTipDato
		 * > 1) { try { totH = totH + ((Number) (lista.get(j - 1)).get(ubicCampo -
		 * 1)).intValue(); totM = totM + ((Number) (lista.get(j -
		 * 1)).get(ubicCampo)).intValue(); } catch
		 * (java.lang.ArrayIndexOutOfBoundsException e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } } setCell(sheet, "D", 11,
		 * totH); setCell(sheet, "E", 11, totM);
		 */

	}

	public void llenar303(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.info("********** La lista del prellenado para la tabla 303 esta vacia ***********");
			return;
		}
		int colExcCant = 14;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 61;
		int ubicCampo = 6;
		int totH = 0;
		int totM = 0;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "H";
				break;
			case 2:
				nomCol = "I";
				break;
			case 3:
				nomCol = "J";
				break;
			case 4:
				nomCol = "K";
				break;
			case 5:
				nomCol = "L";
				break;
			case 6:
				nomCol = "M";
				break;
			case 7:
				nomCol = "N";
				break;
			case 8:
				nomCol = "O";
				break;
			case 9:
				nomCol = "P";
				break;
			case 10:
				nomCol = "Q";
				break;
			case 11:
				nomCol = "R";
				break;
			case 12:
				nomCol = "S";
				break;
			case 13:
				nomCol = "T";
				break;
			case 14:
				nomCol = "U";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					String tipDato = (lista.get(j - 1))[4].toString();
					int ubiTipDato = new Integer(tipDato);

					if (ubiTipDato > 1 && lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int valor = ((Number) (lista.get(j - 1))[ubicCampo + i]).intValue();
						if (valor > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato - 1, valor);
						}
					}
					/*
					 * if(lista.get(j-1)!=null && (lista.get(j - 1)).get(ubicCampo + i)!=null) { }
					 */
				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}
		/*
		 * for (int j = 1; j <= rowExcCant; j++) { String tipDato = (lista.get(j -
		 * 1)).get(4).toString(); int ubiTipDato = new Integer(tipDato); if (ubiTipDato
		 * > 1) { try { totH = totH + ((Number) (lista.get(j - 1)).get(ubicCampo -
		 * 1)).intValue(); totM = totM + ((Number) (lista.get(j -
		 * 1)).get(ubicCampo)).intValue(); } catch
		 * (java.lang.ArrayIndexOutOfBoundsException e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } }
		 */
		// setCell(sheet, "D", 12, totH);
		// setCell(sheet, "E", 12, totM);
	}

	public void llenar307(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			return;
		}
		int colExcCant = 12;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 118;
		int ubicCampo = 6;
		int totH = 0;
		int totM = 0;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "H";
				break;
			case 2:
				nomCol = "I";
				break;
			case 3:
				nomCol = "J";
				break;
			case 4:
				nomCol = "K";
				break;
			case 5:
				nomCol = "L";
				break;
			case 6:
				nomCol = "M";
				break;
			case 7:
				nomCol = "N";
				break;
			case 8:
				nomCol = "O";
				break;
			case 9:
				nomCol = "P";
				break;
			case 10:
				nomCol = "Q";
				break;
			case 11:
				nomCol = "R";
				break;
			case 12:
				nomCol = "S";
				break;

			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					String tipDato = (lista.get(j - 1))[4].toString();
					int ubiTipDato = new Integer(tipDato);

					if (ubiTipDato > 1 && lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int valor = ((Number) (lista.get(j - 1))[ubicCampo + i]).intValue();
						if (valor > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato - 1, valor);

						}
					}
					/*
					 * if (ubiTipDato > 1 && lista.get(j - 1) != null && (lista.get(j -
					 * 1))[ubicCampo + i] != null) { int indiceH = 2 * (i) - 1; int indiceM = 2 *
					 * (i); int valorH = ((Number) (lista.get(j - 1))[ubicCampo +
					 * indiceH]).intValue(); int valorM = ((Number) (lista.get(j - 1))[ubicCampo +
					 * indiceM]).intValue();
					 * 
					 * if (valorH + valorM > 0) { setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato
					 * - 1, valorH + valorM); } }
					 */

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}
		/*
		 * for (int j = 1; j <= rowExcCant; j++) { String tipDato = (lista.get(j -
		 * 1)).get(4).toString(); int ubiTipDato = new Integer(tipDato); if (ubiTipDato
		 * > 1) { try { totH = totH + ((Number) (lista.get(j - 1)).get(ubicCampo -
		 * 1)).intValue(); totM = totM + ((Number) (lista.get(j -
		 * 1)).get(ubicCampo)).intValue(); } catch
		 * (java.lang.ArrayIndexOutOfBoundsException e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } } setCell(sheet, "D", 16,
		 * totH); setCell(sheet, "E", 16, totM);
		 */

	}

	public void llenar310(List<List> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			return;
		}
		int totM = 0;
		int totH = 0;
		int colExcCant = 6;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 194;
		int ubicCampo = 6;
		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "AP";
				break;
			case 2:
				nomCol = "AQ";
				break;
			case 3:
				nomCol = "AR";
				break;
			case 4:
				nomCol = "AS";
				break;
			case 5:
				nomCol = "AT";
				break;
			case 6:
				nomCol = "AU";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {

					String tipDato = (lista.get(j - 1)).get(4).toString();
					int ubiTipDato = -1;
					switch (tipDato.trim().charAt(0)) {
					case 'A':
						ubiTipDato = 1;
						break;
					case 'B':
						ubiTipDato = 2;
						break;
					case 'C':
						ubiTipDato = 3;
						break;
					case 'D':
						ubiTipDato = 4;
						break;
					case 'E':
						ubiTipDato = 5;
						break;
					case 'F':
						ubiTipDato = 6;
						break;
					case 'G':
						ubiTipDato = 7;
						break;
					case 'H':
						ubiTipDato = 8;
						break;
					case 'I':
						ubiTipDato = 9;
						break;
					case 'J':
						ubiTipDato = 10;
						break;
					case 'K':
						ubiTipDato = 11;
						break;
					case 'L':
						ubiTipDato = 12;
						break;
					case 'M':
						ubiTipDato = 13;
						break;
					case 'N':
						ubiTipDato = 14;
						break;
					/*
					 * OMMN case 'Ñ': ubiTipDato = 15; break;
					 */
					case 'O':
						ubiTipDato = 16;
						break;
					case 'P':
						ubiTipDato = 17;
						break;
					case 'Q':
						ubiTipDato = 18;
						break;
					case 'R':
						ubiTipDato = 19;
						break;
					case 'S':
						ubiTipDato = 20;
						break;
					case 'T':
						ubiTipDato = 21;
						break;
					case 'U':
						ubiTipDato = 22;
						break;
					case 'V':
						ubiTipDato = 23;
						break;
					case 'W':
						ubiTipDato = 24;
						break;
					case 'X':
						ubiTipDato = 25;
						break;
					case 'Y':
						ubiTipDato = 26;
						break;
					case 'Z':
						ubiTipDato = 27;
						break;
					}

					if (ubiTipDato > 0 && lista.get(j - 1) != null && (lista.get(j - 1)).get(ubicCampo + i) != null) {
						int indiceH = 2 * (i) - 1;
						int indiceM = 2 * (i);
						int valorH = ((Number) (lista.get(j - 1)).get(ubicCampo + indiceH)).intValue();
						int valorM = ((Number) (lista.get(j - 1)).get(ubicCampo + indiceM)).intValue();
						if (valorH + valorM > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato, valorH + valorM);
						}
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}

		/*
		 * for (int j = 1; j <= rowExcCant; j++) {
		 * 
		 * String tipDato = (lista.get(j - 1)).get(4).toString(); if (tipDato != null &&
		 * !tipDato.equals("01")) { try { totH = totH + ((Number) (lista.get(j -
		 * 1)).get(ubicCampo - 1)).intValue(); totM = totM + ((Number) (lista.get(j -
		 * 1)).get(ubicCampo)).intValue(); } catch
		 * (java.lang.ArrayIndexOutOfBoundsException e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } } setCell(sheet, "D", 19,
		 * totH); setCell(sheet, "E", 19, totM);
		 */

	}

	public void llenar309(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.info("********** La lista del prellenado para la tabla 311 esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;
		int colExcCant = 6;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 194;
		int ubicCampo = 6;
		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "F";
				break;
			case 2:
				nomCol = "G";
				break;
			case 3:
				nomCol = "H";
				break;
			case 4:
				nomCol = "I";
				break;
			case 5:
				nomCol = "J";
				break;
			case 6:
				nomCol = "K";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {

				try {
					String tipDato = (lista.get(j - 1))[4].toString();
					int ubiTipDato = new Integer(tipDato);
					if (ubiTipDato > 1 && lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int indiceH = 2 * (i) - 1;
						int indiceM = 2 * (i);
						int valorH = ((Number) (lista.get(j - 1))[ubicCampo + indiceH]).intValue();
						int valorM = ((Number) (lista.get(j - 1))[ubicCampo + indiceM]).intValue();
						if (valorH + valorM > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato - 1, valorH + valorM);
						}

					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}
		/*
		 * for (int j = 1; j <= rowExcCant; j++) { String tipDato = (lista.get(j -
		 * 1)).get(4).toString(); int ubiTipDato = new Integer(tipDato); if (ubiTipDato
		 * > 1) { try { totH = totH + ((Number) (lista.get(j - 1)).get(ubicCampo -
		 * 1)).intValue(); totM = totM + ((Number) (lista.get(j -
		 * 1)).get(ubicCampo)).intValue(); } catch
		 * (java.lang.ArrayIndexOutOfBoundsException e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } } setCell(sheet, "D", 20,
		 * totH); setCell(sheet, "E", 20, totM);
		 */

	}

	public void llenar301_1A(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.info("********** La lista del prellenado para la tabla 301 de la cedula 1A esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 16;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 13;
		int ubicCampo = 6;
		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "D";
				break;
			case 2:
				nomCol = "E";
				break;
			case 3:
				nomCol = "F";
				break;
			case 4:
				nomCol = "G";
				break;
			case 5:
				nomCol = "H";
				break;
			case 6:
				nomCol = "I";
				break;
			case 7:
				nomCol = "J";
				break;
			case 8:
				nomCol = "K";
				break;
			case 9:
				nomCol = "L";
				break;
			case 10:
				nomCol = "M";
				break;
			case 11:
				nomCol = "N";
				break;
			case 12:
				nomCol = "O";
				break;
			case 13:
				nomCol = "P";
				break;
			case 14:
				nomCol = "Q";
				break;
			case 15:
				nomCol = "R";
				break;
			case 16:
				nomCol = "S";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					if (lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int valor = ((Number) (lista.get(j - 1))[ubicCampo + i]).intValue();
						if (valor > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + 1, valor);
						}
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}
		/*
		 * for (int j = 1; j <= rowExcCant; j++) { try { totH = totH + ((Number)
		 * (lista.get(j - 1))[ubicCampo - 1]).intValue(); totM = totM + ((Number)
		 * (lista.get(j - 1))[ubicCampo]).intValue(); } catch
		 * (java.lang.ArrayIndexOutOfBoundsException e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } setCell(sheet, "D", 10, totH);
		 * setCell(sheet, "E", 10, totM);
		 */
	}

	public void llenar302_1A(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.info("********** La lista del prellenado para la tabla 301 de la cedula 1A esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 16;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 21;
		int ubicCampo = 6;
		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "D";
				break;
			case 2:
				nomCol = "E";
				break;
			case 3:
				nomCol = "F";
				break;
			case 4:
				nomCol = "G";
				break;
			case 5:
				nomCol = "H";
				break;
			case 6:
				nomCol = "I";
				break;
			case 7:
				nomCol = "J";
				break;
			case 8:
				nomCol = "K";
				break;
			case 9:
				nomCol = "L";
				break;
			case 10:
				nomCol = "M";
				break;
			case 11:
				nomCol = "N";
				break;
			case 12:
				nomCol = "O";
				break;
			case 13:
				nomCol = "P";
				break;
			case 14:
				nomCol = "Q";
				break;
			case 15:
				nomCol = "R";
				break;
			case 16:
				nomCol = "S";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					if (lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int valor = ((Number) (lista.get(j - 1))[ubicCampo + i]).intValue();
						if (valor > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + 1, valor);
						}
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}

	}

	public void llenar304_1A(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.info("********** La lista del prellenado para la tabla 311 esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;
		int colExcCant = 6;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 101;
		int ubicCampo = 6;
		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "F";
				break;
			case 2:
				nomCol = "G";
				break;
			case 3:
				nomCol = "H";
				break;
			case 4:
				nomCol = "I";
				break;
			case 5:
				nomCol = "J";
				break;
			case 6:
				nomCol = "K";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {

				try {
					String tipDato = (lista.get(j - 1))[4].toString();
					int ubiTipDato = new Integer(tipDato);
					if (ubiTipDato > 1 && lista.get(j - 1) != null && (lista.get(j - 1))[ubicCampo + i] != null) {
						int indiceH = 2 * (i) - 1;
						int indiceM = 2 * (i);
						int valorH = ((Number) (lista.get(j - 1))[ubicCampo + indiceH]).intValue();
						int valorM = ((Number) (lista.get(j - 1))[ubicCampo + indiceM]).intValue();
						if (valorH + valorM > 0) {
							setCell(sheet, nomCol, rowExcEmpieza + ubiTipDato - 1, valorH + valorM);
						}

					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.info(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "311", e.getMessage() }));
					return;
				}
			}
		}
		/*
		 * for (int j = 1; j <= rowExcCant; j++) { String tipDato = (lista.get(j -
		 * 1))[4].toString(); int ubiTipDato = new Integer(tipDato); if (ubiTipDato > 1)
		 * { try { totH = totH + ((Number) (lista.get(j - 1))[ubicCampo -
		 * 1]).intValue(); totM = totM + ((Number) (lista.get(j -
		 * 1))[ubicCampo]).intValue(); } catch (java.lang.ArrayIndexOutOfBoundsException
		 * e) { LOGGER.log(Level.WARNING,
		 * String.format("Error indice de array para el tablero {0} , detalle: {1}", new
		 * Object[]{"311", e.getMessage()})); return; } } } setCell(sheet, "D", 13,
		 * totH); setCell(sheet, "E", 13, totM);
		 */

	}

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	private void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.info(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	private void setCell(HSSFSheet sheet, int col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(col).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.info(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	static void setCell(HSSFSheet sheet, int col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(col);
		cell.setCellValue(texto);
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	private EnvioDocumentosFacade lookupEnvioDocumentosFacade() {
		try {
			Context c = new InitialContext();
			return (EnvioDocumentosFacade) c.lookup(
					"java:global/eol-portlet/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.ejb.EnvioDocumentosFacade");

		} catch (NamingException ne) {
			logger.info("ERROR lookupEnvioDocumentosFacade = " + ne.getMessage());
			throw new RuntimeException(ne);
		}

	}

	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
