package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import pe.gob.minedu.escale.eol.dto.OAuthResponseDTO;

public class OAuth2ResfreshTokenServlet extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8288714996774172594L;

	private Logger logger = Logger.getLogger(OAuth2ResfreshTokenServlet.class);

	private Gson gson = new Gson();

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: OAuth2ResfreshTokenServlet.processRequest :: Starting execution...");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		OAuthResponseDTO oauthResponse = updateRefreshToken(request);

		String tokenRefreshJsonString = this.gson.toJson(oauthResponse);

		PrintWriter out = response.getWriter();
		out.print(tokenRefreshJsonString);
		out.flush();
		logger.info(":: OAuth2ResfreshTokenServlet.processRequest :: Execution finish.");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}
