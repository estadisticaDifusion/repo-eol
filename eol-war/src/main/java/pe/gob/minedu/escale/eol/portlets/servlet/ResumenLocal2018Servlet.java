package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;

/**
 *
 * @author JBEDRILLANA
 */
public class ResumenLocal2018Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private EnvioDocumentosFacade envioDocumentosFacade;

	private static Logger logger = Logger.getLogger(ResumenMatricula2018Servlet.class);

	boolean[] per = { true, true };

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: ResumenLocal2018Servlet.processRequest :: Starting execution...");
		AuthUsuario usr = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);

		MessageResources res = MessageResources.getMessageResources("eol");

		String urlDescarga = res.getMessage("eol.path.templates.xls") + "censo2018/cedula11/ResumenLocal.xls";

		InputStream is = new FileInputStream(urlDescarga);

		HSSFWorkbook hssfw = new HSSFWorkbook(is);
		List<Object[]> listaC11 = envioDocumentosFacade.getResumeLocal2018ByUgel(usr.getUsuario());
		logger.info(" listaC11 = "+(listaC11!=null?listaC11.size():" is null"));
		llenarResumen(hssfw, listaC11, 141, "C11", 2);
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=\"ResumenLocal.xls\"");
		OutputStream os = response.getOutputStream();
		hssfw.write(os);
		logger.info(":: ResumenLocal2018Servlet.processRequest :: Execution finish.");
		os.close();
	}

	public void llenarResumen(HSSFWorkbook hssfw, List<Object[]> datos, int cantidad, String formato, int iniFila) {
		HSSFSheet sheetC11 = hssfw.getSheet(formato);
		if (datos == null)
			return;
		String column = "";
		for (int row = 0; row < datos.size(); row++) {
			for (short x = 0; x < cantidad; x++) {

				if (x % 26 != 0) {
					String pre = "";
					switch (x / 26) {
					case 1:
						pre = "A";
						break;
					case 2:
						pre = "B";
						break;
					case 3:
						pre = "C";
						break;
					case 4:
						pre = "D";
						break;
					case 5:
						pre = "E";
						break;
					default:
						pre = "";
						break;
					}
					column = pre.concat(String.valueOf((char) ('A' + x % 26)));
				} else {
					String pre = "";
					switch (x / 26) {
					case 1:
						pre = "A";
						break;
					case 2:
						pre = "B";
						break;
					case 3:
						pre = "C";
						break;
					case 4:
						pre = "D";
						break;
					case 5:
						pre = "E";
						break;
					default:
						pre = "";
						break;
					}
					column = pre.concat("A");
				}

				int pos_val = (x);
				int num_row = 4 + row;
				if (datos.get(row)[pos_val] != null) {
					if (datos.get(row)[pos_val].getClass().equals(BigDecimal.class)) {
						setCell(sheetC11, column, num_row, ((BigDecimal) datos.get(row)[pos_val]).intValue());
					} else if (datos.get(row)[pos_val].getClass().equals(Integer.class)) {
						setCell(sheetC11, column, num_row, ((Integer) datos.get(row)[pos_val]).intValue());
					} else if (datos.get(row)[pos_val].getClass().equals(String.class)) {
						setCell(sheetC11, column, num_row, ((String) datos.get(row)[pos_val]));
					}
				}

			}

		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(valor == null ? 0 : valor.intValue());
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellValue(valor.intValue());
				}

			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
				cell.setCellValue(valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(texto);
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
					cell.setCellValue(new HSSFRichTextString(texto));
				}
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(new HSSFRichTextString(texto));
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		return iCol;
	}

}
