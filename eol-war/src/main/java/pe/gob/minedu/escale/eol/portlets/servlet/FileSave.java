package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

public class FileSave extends HttpServlet {

    private static final long serialVersionUID = 1L;
//    private final String UPLOAD_DIRECTORY = "D:/Temp/";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
//        String periodo = "2016";
        String actividad = request.getParameter("actividad");
        String codmod = request.getParameter("codmod");
        String anexo = request.getParameter("anexo");
        String anio = request.getParameter("anio");
        String msg = "";

        System.out.println("IE : "+codmod + " : "+anexo);

        if (isMultipart){

            FileItemFactory factory = new DiskFileItemFactory();

            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> multiparts;
            InputStream entrada = null;
            File file = null;

            try {

                Properties propiedades = new Properties();
                propiedades.load( getClass().getResourceAsStream("config.properties") );
                String pathFile = propiedades.getProperty("pathFile");

                String filename = "";

                multiparts = upload.parseRequest(request);

                for (FileItem item : multiparts){

                    if (!item.isFormField() && item.getSize() > 0){
                        file = new File(item.getName());
                        String name = FilenameUtils.removeExtension(file.getName());
                        String ext = FilenameUtils.getExtension(file.getName());
                        //name = File
                        //String rename = name+System.currentTimeMillis()+"."+ext;
                        String rename = anio+"_"+actividad + "_"+codmod+"_"+anexo+"."+ext;
                        String path = pathFile + rename;
                        System.out.println("PATH :"+path);
                        item.write(new File(path));
                        msg = "OK";
                    }

                }


            } catch (Exception e) {

                msg = "ERROR";
                System.out.println("ERROR : "+e.getMessage());

            } finally{

                file = null;

            }

            out.println(msg);

        }



        /*
        String description = request.getParameter("description"); // Retrieves <input type="text" name="description">
        Part filePart = request.getPart("uploadfile"); // Retrieves <input type="file" name="file">
        Collection<Part> lip = request.getParts();
        String fileName = getSubmittedFileName(filePart);
        System.out.println("DESCRIPCION2 : "+description);
        System.out.println("FILE2 : "+fileName);
        String path = "D:/Temp/" + fileName;
        File file = new File(path);
        System.out.println("PATH : "+file.getPath());
        filePart.write("archivo"+System.currentTimeMillis()+".xlsx");
        */
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private static String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

    // helpers
    private static String getFilename(Part part) {
        // courtesy of BalusC : http://stackoverflow.com/a/2424824/281545
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim()
                        .replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1)
                        .substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }
}
