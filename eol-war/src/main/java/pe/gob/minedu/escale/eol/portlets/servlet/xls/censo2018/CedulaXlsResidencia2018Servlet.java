package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2018;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2018.Matricula2018Cabecera;
import pe.gob.minedu.escale.eol.estadistica.ejb.censo2018.Matricula2018Facade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;

public class CedulaXlsResidencia2018Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(CedulaXlsResidencia2018Servlet.class);

	static final String CEDULA_MATRICULA_RESIDENCIA = "CensoMatriculaResidencia3AS";
	static final String PERIODO = "2018";
	static final String PATH_DIR = "2018/cedulas";

	@Resource(name = "eol")
	private DataSource eol;

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@EJB
	private Matricula2018Facade matricula2018Facade;

	@SuppressWarnings("unused")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: CedulaXlsResidencia2018Servlet.processRequest :: Starting execution...");
		String preCed = "";
		InputStream is = null;
		String nivMod = request.getParameter("nivel");
		String anexo = request.getParameter("anexo");
		String codMod = request.getParameter("codmod");
		logger.info("********** DESCARGA  {0}***********" + codMod);
		// preCed = Funciones.cedulaMatricula2018NivMod(nivMod);
		// InputStream is =
		// getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2018/matricula/CensoMatriculaDocentesRecursosCed"
		// + preCed.toUpperCase() + "_" + PERIODO + ".xls");

		// VERIFICACION SI LE CORRESPONDE O NO HACER EL ENVIO
		Matricula2018Cabecera matricula2018 = matricula2018Facade.findByCodModAnNiv(codMod, anexo, nivMod);

		if (matricula2018.getP131().equals("1"))// Requiere registrar datos de identificacion de residencia
		{
			String rutaRaiz = getServletContext()
					.getRealPath("/WEB-INF/classes/pe/gob/minedu/escale/eol/portlets/plantilla/censo2018/matricula/"
							+ CEDULA_MATRICULA_RESIDENCIA + "_" + PERIODO + ".xls");
			try {
				is = new FileInputStream(rutaRaiz);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			if (is != null) {

				CentroEducativo ie = padronFacade.obtainByCodMod(codMod, anexo);

				HSSFWorkbook hssfw = new HSSFWorkbook(is);
				llenadoCedularResidencia3AS(hssfw, ie, nivMod);

				response.setContentType("application/octet-stream");

				response.setHeader("Content-Disposition", String
						.format("attachment;filename=\"CensoMatriculaResidencia%s_2018.xls\"", preCed.toUpperCase()));
				OutputStream os = response.getOutputStream();
				try {
					hssfw.write(os);

				} catch (IOException ioe) {

					ioe.printStackTrace();
				}
				os.close();
				is.close();
			} else {
				descargarPdf("1", response);
			}

		} else {// 2 no le corresponde a la IE por lo tanto se tiene que registrar un
				// pseudoenvio
				// Ejecutar el SP para registrar en EOL_CENSO
			List<Object[]> listObj = null;
			Long flag = 0L;

			listObj = matricula2018Facade.registrarEnvioIdentificacion(codMod, anexo, nivMod);

			if (listObj != null && listObj.size() > 0) {
				System.out.println("REGISTRO DE NUEVO ENVIO DE REGISTRO DE SOLICITUD");
				/*
				 * for (int i = 0; i < listObj.size(); i++) { if(listObj.get(i)[0] != null){
				 * System.out.println("REGISTRO DE NUEVO ENVIO DE REGISTRO DE SOLICITUD"); } }
				 */
			}
			// if(flag >0){
			// System.out.println("REGISTRO DE NUEVO ENVIO DE REGISTRO DE SOLICITUD");
			// }

			// REGISTRAR CON IDENVIO = 0 PARA VALIDAR LA CONSTANCIA EN EL TABLERO DEL
			// DIRECTOR

			// Mostrar mensaje PDF
			descargarPdf("2", response);
		}
		logger.info(":: CedulaXlsResidencia2018Servlet.processRequest :: Execution finish.");
	}

	public void descargarPdf(String situacionmsg, HttpServletResponse response) {
		Connection conn = null;
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			conn = eol.getConnection();
			String cedNombre = "";
			// params.put("ID_ENVIO", codigoIns);
			// params.put("COD_MOD", codigoModular);
			if (situacionmsg.equals("1"))// 1. Mensaje para notificar que aun no existe la cedula
				cedNombre = "cedula-notificacion";
			else// 2. Mensaje para notificar que no requiere declarar datos de la cedula de
				// anexo
				cedNombre = "cedula-identificacion";

			String report = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2018/matricula")
					.getFile();
			URL urlMaster = getClass().getResource(
					"/pe/gob/minedu/escale/eol/portlets/constancia/censo2018/matricula/" + cedNombre + ".jasper");
			params.put("SUBREPORT_DIR", report);
			JasperReport masterReport = (JasperReport) JRLoader.loadObject(urlMaster);
			// String escudo =
			// getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/matricula/Escudo.JPG").getFile();
			params.put("REPORT_LOCALE", new java.util.Locale("ES", "PE"));
			// params.put("escudo", escudo);

			byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=\"notificacionidentificacion.pdf\"");
			response.setContentLength(buffer.length);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(buffer, 0, buffer.length);
			ouputStream.flush();
			ouputStream.close();

		} catch (JRException ex) {
			log(ex.getMessage(), ex);
		} catch (Exception ex) {
			log(ex.getMessage(), ex);
		}

	}

	@SuppressWarnings("unused")
	public void llenadoCedularResidencia3AS(HSSFWorkbook hssfw, CentroEducativo ie, String niv_mod) {
		HSSFSheet sheet100_200 = hssfw.getSheet("C100");
		HSSFSheet sheet200 = hssfw.getSheet("C200");
		// setCell(sheet100_200, "AR", 1, HOST );
		setCell(sheet100_200, "H", 13, ie.getCodMod());// Código Modular
		setCell(sheet100_200, "O", 13, ie.getCenEdu());// Nombre
		setCell(sheet100_200, "H", 15, ie.getCodlocal());// Codlocal
		// setCell(sheet100_200, "AI", 30, ie.getTelefono());//Telefono

	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	static void setCell(HSSFSheet sheet, int col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(col);
		cell.setCellValue(texto);
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
