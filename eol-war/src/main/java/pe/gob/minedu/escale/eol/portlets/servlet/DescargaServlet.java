/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author IMENDOZA
 */
public class DescargaServlet extends HttpServlet {
   
    private static final long serialVersionUID = 1L;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String actividad = request.getParameter("actividad");
        String codmod = request.getParameter("codmod");
        String anexo = request.getParameter("anexo");
        String enviado = request.getParameter("idEnvio");
        String codlocal = request.getParameter("codlocal");
        String  idActividad = request.getParameter("idActividad");
        String  codinst = request.getParameter("codinst");
        String  anio = request.getParameter("anio");

        String msg = "";
        String path = "";
        if (enviado!=null) {
            ServletOutputStream outp = response.getOutputStream();
            try {
                Properties propiedades = new Properties();
                propiedades.load( getClass().getResourceAsStream("config.properties") );
                String pathFile = propiedades.getProperty("pathFile");

                switch (Integer.valueOf(idActividad)) {
                    case 19://CENSO-MATRICULA
                    case 21://CENSO-RESULTADO                    
                        path = anio + "_" + actividad + "_" + codmod + "_" + anexo;
                       break;
                    case 22:  //CENSO-ID
                        path = anio+"_"+actividad+"_"+codinst+"_0";
                        break;
                    case 20://CENSO-LOCAL
                        path = anio+"_"+actividad+"_"+codlocal+"_0";
                        break;
                  default:
                        path = "000";
                        break;
                }
                
                File file = new File(pathFile);
                File fichero = buscarArchivoEnDirectorio(path, file);
                FileInputStream fis = new FileInputStream(fichero);
                byte[] bytes = new byte[1000];
                int read = 0;

                 String fileName = fichero.getName();
                 String contentType = "application/vnd.ms-excel";
                 response.setContentType(contentType);
                 response.setHeader("Content-Disposition","attachment;filename=\"" + fileName + "\"");


                   while ((read = fis.read(bytes)) != -1) {
                        outp.write(bytes, 0, read);
                   }

                   outp.flush();

                   System.out.println("\nDescargado\n");
            } catch (Exception e) {
                msg = "ERROR";
                System.out.println("ERROR DESCARGANDO: "+e.getMessage());
            }finally{
                outp.close();
                return;
            }

        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public File buscarArchivoEnDirectorio(String name,File file)
    {
        File[] list = file.listFiles();
        int i = 0;
        if(list!=null){
            for (File fil : list){

                 System.out.println("Nombre de archivo: " + FilenameUtils.removeExtension(fil.getName()) +" --- "+name);
                if (fil.isDirectory()){
                    buscarArchivoEnDirectorio(name,fil);
                }
                else if (name.equalsIgnoreCase(FilenameUtils.removeExtension(fil.getName()))){
                    break;
                }else{
                    i++;
                }
            }
        }

       return list[i];

    }


}
