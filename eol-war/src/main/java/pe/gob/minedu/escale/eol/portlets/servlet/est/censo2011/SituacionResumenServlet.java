/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet.est.censo2011;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

/**
 *
 * @author JMATAMOROS
 */
public class SituacionResumenServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(SituacionResumenServlet.class);

	@EJB
	private PadronLocal padronSrv = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: SituacionResumenServlet.processRequest :: Starting execution...");

		response.setContentType("text/html;charset=UTF-8");

		String codUGEL = request.getParameter("codUGEL");
		String strExp = request.getParameter("formato") != null ? request.getParameter("formato") : "";
		String strTipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
		String strAnio = request.getParameter("anio") != null ? request.getParameter("anio") : "";

		boolean existChar = false;
		List<String> reqParams = new ArrayList<String>();
		reqParams.add(codUGEL);
		reqParams.add(strExp);
		reqParams.add(strTipo);
		reqParams.add(strAnio);

		existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

		if (!existChar) {
			logger.info("strTipo: " + strTipo + "; strAnio: " + strAnio);
			
			EnvioDocumentosFacade matriSrv = lookupEnvioDocumentosFacade();

			List<Object[]> listaPadron = null;
			List<Object[]> listSend = null;
			List<List> listCentrosSend = new ArrayList();
			/*
			 * if ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2011"))) {
			 * listSend = matriSrv.getCuentaMatricula2011Envios(codUGEL, "UGEL");
			 * listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL",
			 * "CENSO-MATRICULA"); } else if ((strTipo.equals("CENSO-LOCAL")) &&
			 * (strAnio.equals("2011"))) { listSend =
			 * matriSrv.getCuentaLocal2011Envios(codUGEL, "UGEL"); listaPadron =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL"); } else if
			 * ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2011"))) { listSend
			 * = matriSrv.getCuentaResultado2011Envios(codUGEL, "UGEL"); listaPadron =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO"); } else
			 * if ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2012"))) {
			 * listSend = matriSrv.getCuentaMatricula2012Envios(codUGEL, "UGEL");
			 * listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL",
			 * "CENSO-MATRICULA"); } else if ((strTipo.equals("CENSO-LOCAL")) &&
			 * (strAnio.equals("2012"))) { listaPadron =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL"); listSend =
			 * matriSrv.getCuentaLocal2012Envios(codUGEL, "DRE"); } else if
			 * ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2012"))) { listSend
			 * = matriSrv.getCuentaResultado2012Envios(codUGEL, "UGEL"); listaPadron =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO"); } else
			 * if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2013"))) {
			 * listSend = matriSrv.getCuentaResultado2013Envios(codUGEL, "UGEL");
			 * listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL",
			 * "CENSO-RESULTADO"); } else if ((strTipo.equals("CENSO-MATRICULA")) &&
			 * (strAnio.equals("2013"))) { listSend =
			 * matriSrv.getCuentaMatricula2013Envios(codUGEL, "UGEL"); listaPadron =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-MATRICULA"); } else
			 * if ((strTipo.equals("CENSO-LOCAL")) && (strAnio.equals("2013"))) {
			 * listaPadron = padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL"); listSend =
			 * matriSrv.getCuentaLocal2013Envios(codUGEL, "DRE"); } else if
			 * ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2014"))) { listSend
			 * = matriSrv.getCuentaResultado2014Envios(codUGEL, "UGEL"); listaPadron =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO"); } else
			 * if ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2014"))) {
			 * listSend = matriSrv.getCuentaMatricula2014Envios(codUGEL, "UGEL");
			 * listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL",
			 * "CENSO-MATRICULA"); } else if ((strTipo.equals("CENSO-LOCAL")) &&
			 * (strAnio.equals("2014"))) { listaPadron =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL"); listSend =
			 * matriSrv.getCuentaLocal2014Envios(codUGEL, "DRE"); } else if
			 * ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2015"))) { listSend
			 * = matriSrv.getCuentaMatricula2015Envios(codUGEL, "UGEL"); listaPadron =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-MATRICULA"); } else
			 * if ((strTipo.equals("CENSO-LOCAL")) && (strAnio.equals("2015"))) {
			 * listaPadron = padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL"); listSend =
			 * matriSrv.getCuentaLocal2015Envios(codUGEL, "DRE"); } else if
			 * ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2015"))) { listSend
			 * = matriSrv.getCuentaResultado2015Envios(codUGEL, "UGEL",false); listaPadron =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO"); } else
			 * if ((strTipo.equals("CENSO-ID")) && (strAnio.equals("2015"))) { listSend =
			 * matriSrv.getCuentaID2015Envios(codUGEL, "DRE"); }else
			 */
			/*
			if ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2016"))) {
				listSend = matriSrv.getCuentaMatricula2016Envios(codUGEL, "UGEL");
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-MATRICULA");
			} else if ((strTipo.equals("CENSO-LOCAL")) && (strAnio.equals("2016"))) {
				listaPadron = padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL");
				listSend = matriSrv.getCuentaLocal2016Envios(codUGEL, "DRE");
			} else if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2016"))) {
				listSend = matriSrv.getCuentaResultado2016Envios(codUGEL, "UGEL", false);
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO");
			} else if ((strTipo.equals("CENSO-ID")) && (strAnio.equals("2016"))) {
				listSend = matriSrv.getCuentaID2016Envios(codUGEL, "DRE");
			}  else 
				*/
				if ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2017"))) {
				listSend = matriSrv.getCuentaMatricula2017Envios(codUGEL, "UGEL");
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-MATRICULA");
			} else if ((strTipo.equals("CENSO-LOCAL")) && (strAnio.equals("2017"))) {
				listaPadron = padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL");
				listSend = matriSrv.getCuentaLocal2017Envios(codUGEL, "UGEL");
			} else if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2017"))) {
				listSend = matriSrv.getCuentaResultado2017Envios(codUGEL, "UGEL", false);
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO");
			} else if ((strTipo.equals("CENSO-RESULTADO-RECUPERACION")) && (strAnio.equals("2017"))) {
				listSend = matriSrv.getCuentaResultado2017EnviosReqRecuperacion(codUGEL, "UGEL", false);
				listaPadron = padronSrv.getCuentaCentrosByUGELMarcoRecuperacion(codUGEL, "UGEL",
						"CENSO-RESULTADO-RECUPERACION", "2017");
			} else if ((strTipo.equals("CENSO-ID")) && (strAnio.equals("2017"))) {
				listSend = matriSrv.getCuentaID2017Envios(codUGEL, "DRE");
			} else if ((strTipo.equals("EVALUACION-SIAGIE")) && (strAnio.equals("2017"))) {
				listSend = matriSrv.getCuentaEvalSiagie2017Envios(codUGEL, "UGEL");
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "EVALUACION-SIAGIE");
			} else if ((strTipo.equals("FEN-COSTERO")) && (strAnio.equals("2017"))) {
				listaPadron = padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL");
				listSend = matriSrv.getCuentaFenNino2017Envios(codUGEL, "UGEL");
			} /* imendoza 20170324 inicio */ // 2018
			else if ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2018"))) {
				listSend = matriSrv.getCuentaMatricula2018Envios(codUGEL, "UGEL");
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-MATRICULA");
			} else if ((strTipo.equals("CENSO-LOCAL")) && (strAnio.equals("2018"))) {
				listaPadron = padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL");
				listSend = matriSrv.getCuentaLocal2018Envios(codUGEL, "UGEL");
			} else if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2018"))) {
				listSend = matriSrv.getCuentaResultado2018Envios(codUGEL, "UGEL", false);
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO");
			} else if ((strTipo.equals("CENSO-RESULTADO-RECUPERACION")) && (strAnio.equals("2018"))) {
				listSend = matriSrv.getCuentaResultado2018EnviosReqRecuperacion(codUGEL, "UGEL", false);
				listaPadron = padronSrv.getCuentaCentrosByUGELMarcoRecuperacion(codUGEL, "UGEL",
						"CENSO-RESULTADO-RECUPERACION", "2018");
			}/* jbedrillana */ // 2019
			else if ((strTipo.equals("CENSO-MATRICULA")) && (strAnio.equals("2019"))) {
				logger.info("MATRICULA 2019");
				listSend = matriSrv.getCuentaMatricula2019Envios(codUGEL, "UGEL");
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-MATRICULA");
			} else if ((strTipo.equals("CENSO-LOCAL")) && (strAnio.equals("2019"))) {
				logger.info("LOCAL 2019");
				listaPadron = padronSrv.getCuentaCentrosByLocal(codUGEL, "UGEL");
				listSend = matriSrv.getCuentaLocal2019Envios(codUGEL, "UGEL");
			} else if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2019"))) {
				logger.info("CENSO RESULTADO 2019");
				listSend = matriSrv.getCuentaResultado2019Envios(codUGEL, "UGEL", false);
				listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO");
			} else if ((strTipo.equals("CENSO-RESULTADO-RECUPERACION")) && (strAnio.equals("2019"))) {
				logger.info("CENSO RESULTADO-RECUPERACION 2019");
				listSend = matriSrv.getCuentaResultado2019EnviosReqRecuperacion(codUGEL, "UGEL", false);
				listaPadron = padronSrv.getCuentaCentrosByUGELMarcoRecuperacion(codUGEL, "UGEL","CENSO-RESULTADO-RECUPERACION", "2019");
			}

			if (strTipo.equals("CENSO-ID")) {
				logger.info("00000000000000000000 ");
				for (Object[] cen : listSend) {
					List listTmp = new ArrayList(Arrays.asList(cen));
					logger.info("111111111111111111:  " +  listTmp);
					listCentrosSend.add(listTmp);
				}
			} else {
				logger.info("444444444444444:  " +  strTipo);
				// logger.info(" listSend = "+listSend);
				for (Object[] cen : listSend) {
					int xPos = -1;
					if (strTipo.equals("CENSO-MATRICULA") || strTipo.equals("CENSO-RESULTADO")
							|| strTipo.equals("CENSO-RESULTADO-RECUPERACION")) {
						
						xPos = Collections.binarySearch(listaPadron, cen, buscarEnvioLista);
						logger.info("555555555555555555:  " +  xPos);
					} else if (strTipo.equals("CENSO-LOCAL")) {
						xPos = Collections.binarySearch(listaPadron, cen, buscarEnvioListaLocal);
					} else if (strTipo.equals("EVALUACION-SIAGIE")) {
						xPos = Collections.binarySearch(listaPadron, cen, buscarEnvioListaLocal);// solamente se compara
																									// el codigo modular
					} else if (strTipo.equals("FEN-COSTERO")) {
						xPos = Collections.binarySearch(listaPadron, cen, buscarEnvioListaLocal);// similar a local
					}

					if (xPos >= 0) {
						List listTmp = new ArrayList(Arrays.asList((Object[]) listaPadron.get(xPos)));
						listTmp.add(cen[3]);
						logger.info("2222222222222222222:  " +  listTmp);
						listCentrosSend.add(listTmp);
					}
				} 
			}
			logger.info("EXCEL ENVIA CENTROS: " + listCentrosSend );
			logger.info("333333333333333333:  " +  listCentrosSend);
			request.setAttribute("resumen_matri_envios", listCentrosSend);
			logger.info("EXCEL ENVIA TIPOS: " + strTipo );
			request.setAttribute("TipoConsulta", strTipo);
			if ("xls".equals(strExp)) {
				logger.info("EXCEL ENVIA XLS: " + strExp );
				response.setHeader("Content-type", "application/octet-stream");
				response.addHeader("Content-Disposition", "attachment; filename=\"cuadroCobertura.xls\"");
			}
			logger.info(":: SituacionResumenServlet.processRequest :: Ecution finish (1).");
			RequestDispatcher requestDispatcher = getServletContext()
					.getRequestDispatcher("/WEB-INF/jsp/ugel/reporte/resumenEnvios.jsp");
			requestDispatcher.include(request, response);
		} else {
			logger.info(":: SituacionResumenServlet.processRequest :: Ecution finish (2).");
			RequestDispatcher requestDispatcher = getServletContext()
					.getRequestDispatcher("/recursos/error/errorsql.jsp");
			requestDispatcher.include(request, response);
		}

	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

	private EnvioDocumentosFacade lookupEnvioDocumentosFacade() {
		try {
			Context c = new InitialContext();
			return (EnvioDocumentosFacade) c.lookup(
					"java:global/eol-war/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade");

		} catch (NamingException ne) {
			logger.info("ERROR lookupEnvioDocumentosFacade = " + ne.getMessage());
			throw new RuntimeException(ne);
		}

	}
//	ie = ies.stream().filter(x -> (x.getCodMod().equals(ieBq.getCodMod()) && x.getAnexo().equals(ieBq.getAnexo()))).findFirst().orElse(null);
	
	Comparator<Object[]> buscarEnvioLista = new Comparator<Object[]>() {

		public int compare(Object[] o1, Object[] o2) {
//		      logger.info("o1[0]: "+o1[0]+"; o2[0]: "+o2[0]);
//			  logger.info("o1[1]: "+o1[1]+"; o2[1]: "+o2[1]);
			if (((String) o1[0]).compareTo((String) o2[0]) == 0) {
				if (((Character) o1[1]).compareTo((Character) o2[1]) == 0) {
					return ((String) o1[2]).substring(0, 1).compareTo(((String) o2[2]).substring(0, 1));
				} else {
//					return ((String) o1[1]).compareTo((String) o2[1]);
					return ((Character) o1[1]).compareTo((Character) o2[1]);
				}
			} else {
				return ((String) o1[0]).compareTo((String) o2[0]);
			}
		}

		/*
		 * public int compare(Object[] o1, Object[] o2) { if (((String)
		 * o1[0]).compareTo((String) o2[0]) == 0) { if (((String)
		 * o1[1]).compareTo((String) o2[1]) == 0) { return ((String) o1[2]).substring(0,
		 * 1).compareTo(((String) o2[2]).substring(0, 1)); } else { return ((String)
		 * o1[1]).compareTo((String) o2[1]); } } else { return ((String)
		 * o1[0]).compareTo((String) o2[0]); } }
		 */
	};

	static Comparator<Object[]> buscarEnvioListaLocal = new Comparator<Object[]>() {
		public int compare(Object[] o1, Object[] o2) {

			return ((String) o1[0]).compareTo((String) o2[0]);
		}
	};
}
