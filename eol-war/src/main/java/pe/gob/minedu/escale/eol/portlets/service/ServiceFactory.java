package pe.gob.minedu.escale.eol.portlets.service;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.ejb.SesionUsuarioFacade;
//import pe.gob.minedu.escale.eol.ejb.SesionUsuarioFacade;

public class ServiceFactory {

    private static final Logger logger = Logger.getLogger(ServiceFactory.class);
    private static ServiceFactory instance;

    private ServiceFactory() {

    }

    public static ServiceFactory getInstance() {
        if (instance == null) {
            instance = new ServiceFactory();
        }
        return instance;
    }

    public static SesionUsuarioFacade getSesionUsuarioFacade() {
        logger.info(":: SesionUsuarioFacade.getSesionUsuarioFacade :: Starting execution...");
        try {
            Context c = new InitialContext();
            logger.info(":: SesionUsuarioFacade.getSesionUsuarioFacade :: Execution finish.");
            return (SesionUsuarioFacade) c.lookup("java:global/eol-war/SesionUsuarioFacade!pe.gob.minedu.escale.eol.estadistica.ejb.SesionUsuarioFacade");
        } catch (NamingException ne) {
            logger.error(ne.getMessage());
            throw new RuntimeException(ne);
        }

    }
}
