/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.dto.InstitucionDTO;
import pe.gob.minedu.escale.eol.padron.ejb.InstitucionLocal;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.rest.client.domain.Distrito;
import pe.gob.minedu.escale.rest.client.domain.EstadoPadron;
import pe.gob.minedu.escale.rest.client.domain.Gestion;
import pe.gob.minedu.escale.rest.client.domain.GestionDependencia;
import pe.gob.minedu.escale.rest.client.domain.Igel;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.rest.client.domain.NivelModalidad;
import pe.gob.minedu.escale.rest.client.domain.Ugel;

/**
 *
 * @author JMATAMOROS
 */
public class PageCEAction extends Action {

	private Logger logger = Logger.getLogger(PageCEAction.class);

	@EJB
	private InstitucionLocal institucionFacade = lookup(
			"java:global/eol-war/InstitucionFacade!pe.gob.minedu.escale.eol.padron.ejb.InstitucionLocal");

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info(":: PageCEAction.execute :: Starting execution...");

		logger.info("institucionFacade: " + institucionFacade);

		HttpSession session = request.getSession();
		session.setAttribute("IIEESession", null);
		
//		ActionForward actControl=new ActionForward();

		AuthUsuario usuario = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);
		if (usuario != null) {
			String codMod = usuario.getUsuario();
			// PadronClient padronClientWS = new PadronClient();

			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("codmod", codMod);
			parameters.put("orderBy", "anexo");
			
			//Filtro de activos
			List<String> estados = Arrays.asList("1");

//			List<pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO> institucionesDTO = institucionFacade
//					.getInstitucionList(codMod, null, null, null, null, null, null, null, null, null, null, null, null,
//							null, null, null, null, estados, null, null, 0, 50, "anexo");
	/// Se comento los estados, permitiendose que pasen las instituciones activas e inactivas provenientes del padron 
			List<pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO> institucionesDTO = institucionFacade
					.getInstitucionList(codMod, null, null, null, null, null, null, null, null, null, null, null, null,
							null, null, null, null, null, null, null, 0, 50, "anexo");

			
			
			
			
			
			
			
			logger.info("institucionesDTO=" + institucionesDTO + "/////"  + institucionesDTO.size()    );

			if (institucionesDTO != null && institucionesDTO.size()>0   ) {
				List<InstitucionEducativaIE> institucionList = convertInstitucion(institucionesDTO);
				logger.info("list convert instiucionList=" + institucionList + "; IE: "
						+ (institucionList != null ? institucionList.size() : " is null")  );

				String sessionugel = institucionList.get(0).getUgel().getIdUgel();
				session.setAttribute("sessionugel", sessionugel);
				if (institucionList.size() > 1) {
					logger.info("condition 1...");
					List<InstitucionEducativaIE> list = new ArrayList<InstitucionEducativaIE>();
					for (InstitucionEducativaIE ie : institucionList) { // instituciones.getInstitucion()
						if (ie.getNivelModalidad().getIdCodigo().startsWith("B")) {
							list.add(ie);
						}
					}
					if (list.size() > 0) {
						session.setAttribute("IIEES", list);
						session.setAttribute("CASEIE", "second");
						
//						actControl=mapping.findForward("success_anexo");
						logger.info(" 000000000 : "  );
						
						return mapping.findForward("success_anexo");
//						return actControl;
					} else {
						InstitucionEducativaIE var1 = institucionList.get(0);

						request.setAttribute("IIEE", institucionList.get(0));
						session.setAttribute("IIEESession", var1);
						session.setAttribute("CASEIE", "first");
					}
				} else if (institucionList.size() == 1
						&& (institucionList.get(0).getProgise().equals(InstitucionEducativaIE.ISPCONIST_SI))) {
					logger.info("condition 2...");
					request.setAttribute("IIEE", institucionList.get(0));
					session.setAttribute("CASEIE", "third");
					
//					actControl=mapping.findForward("success_ispist");
					logger.info(" 111111111111 : " );
					
					
					return mapping.findForward("success_ispist");
//					return actControl;
				} else {
					logger.info("condition 3...");
					InstitucionEducativaIE var1 = new InstitucionEducativaIE();
					var1 = institucionList.get(0);
					logger.info("  var1=" + var1);
					// &mcenso=${IIEESession.mcenso}&sienvio=${IIEESession.sienvio}&estado=${IIEESession.estado.idCodigo}"
					InstitucionDTO institucionDTO = new InstitucionDTO();
					institucionDTO.setCodigoModular(var1.getCodigoModular());
					institucionDTO.setAnexo(var1.getAnexo());
					institucionDTO.setNivelModalidad(var1.getNivelModalidad().getIdCodigo());
					institucionDTO.setCodigoLocal(var1.getCodlocal());
					institucionDTO.setCodigoInstitucion(var1.getCodinst());
					institucionDTO.setMcenso(var1.getMcenso());
					institucionDTO.setSienvio(var1.getSienvio());
					institucionDTO.setEstado(var1.getEstado().getIdCodigo());

					request.setAttribute("IIEE", institucionList.get(0));
					session.setAttribute("IIEESession", var1);
					session.setAttribute(Constantes.LOGGED_INSTITUCION, institucionDTO);
					session.setAttribute("CASEIE", "first");
					logger.info("  var1=" + var1);
				}
				/*

				logger.info("consume web service Institucion");
				Instituciones instituciones = padronClientWS.consumeGetResourceXml(Instituciones.class,
						"post/getInstituciones", parameters);
				logger.info("consume web service Institucion resultado=" + instituciones);
				
				String sessionugel = ((InstitucionEducativaIE) instituciones.getInstitucion().toArray()[0]).getUgel()
						.getIdUgel();
				session.setAttribute("sessionugel", sessionugel);
				if (instituciones.getInstitucion().size() > 1) {
					logger.info("condition 1...");
					List<InstitucionEducativaIE> list = new ArrayList<InstitucionEducativaIE>();
					for (InstitucionEducativaIE ie : instituciones.getInstitucion()) {
						if (ie.getNivelModalidad().getIdCodigo().startsWith("B")) {
							list.add(ie);
						}
					}
					if (list.size() > 0) {
						request.setAttribute("IIEES", list);
						return mapping.findForward("success_anexo");
					} else {
						InstitucionEducativaIE var1 = new InstitucionEducativaIE();
						var1 = (InstitucionEducativaIE) instituciones.getInstitucion().toArray()[0];

						request.setAttribute("IIEE", instituciones.getInstitucion().toArray()[0]);
						session.setAttribute("IIEESession", var1);
					}
				} else if (instituciones.getInstitucion().size() == 1
						&& ((InstitucionEducativaIE) instituciones.getInstitucion().toArray()[0]).getProgise()
								.equals(InstitucionEducativaIE.ISPCONIST_SI)) {
					logger.info("condition 2...");
					request.setAttribute("IIEE", instituciones.getInstitucion().toArray()[0]);
					return mapping.findForward("success_ispist");
				} else {
					logger.info("condition 3...");
					InstitucionEducativaIE var1 = new InstitucionEducativaIE();
					var1 = (InstitucionEducativaIE) instituciones.getInstitucion().toArray()[0];
					logger.info("  var1=" + var1);
					// &mcenso=${IIEESession.mcenso}&sienvio=${IIEESession.sienvio}&estado=${IIEESession.estado.idCodigo}"
					InstitucionDTO institucionDTO = new InstitucionDTO();
					institucionDTO.setCodigoModular(var1.getCodigoModular());
					institucionDTO.setAnexo(var1.getAnexo());
					institucionDTO.setNivelModalidad(var1.getNivelModalidad().getIdCodigo());
					institucionDTO.setCodigoLocal(var1.getCodlocal());
					institucionDTO.setCodigoInstitucion(var1.getCodinst());
					institucionDTO.setMcenso(var1.getMcenso());
					institucionDTO.setIndicadorEnvio(var1.getSienvio());
					institucionDTO.setEstado(var1.getEstado().getIdCodigo());

					request.setAttribute("IIEE", instituciones.getInstitucion().toArray()[0]);
					session.setAttribute("IIEESession", var1);
					session.setAttribute(Constantes.LOGGED_INSTITUCION, institucionDTO);
					logger.info("  var1=" + var1);
				}*/
			}
			else {
				logger.info("HAY ERRORR EN LA INSTICUIONES LISTADO");
				request.setAttribute("IIEE", institucionesDTO.size());
				
//				actControl=mapping.findForward("errorparam");
				logger.info(" 2222222222222222222 : "  );
				
				
//				return mapping.findForward("errorparam");
				//return actControl;
			}
		}
		logger.info(":: PageCEAction.execute :: Execution finish.");
		
//		if(actControl!=null)
//		actControl=mapping.findForward("success");
		logger.info(" 3333333333333333 : "  );
		
		
		return mapping.findForward("success");
		
//			return actControl;	
	}

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

	private List<InstitucionEducativaIE> convertInstitucion(
			List<pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO> listDTO) {
		List<InstitucionEducativaIE> list = new ArrayList<InstitucionEducativaIE>();
		if (listDTO != null) {
			InstitucionEducativaIE institucionEducativaIE = null;
			for (pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO ie : listDTO) {
				institucionEducativaIE = new InstitucionEducativaIE();
				institucionEducativaIE.setAnexo(ie.getAnexo());
				institucionEducativaIE.setCodigoModular(ie.getCodMod());
				institucionEducativaIE.setCentroEducativo(ie.getCenEdu());
				institucionEducativaIE.setProgarti(ie.getProgarti());
				institucionEducativaIE.setCodlocal(ie.getCodlocal());
				institucionEducativaIE.setTipoprog(ie.getTipoprog());
				institucionEducativaIE.setProgise(ie.getProgise());
				institucionEducativaIE.setCodinst(ie.getCodinst());
				institucionEducativaIE.setMcenso(ie.getMcenso());
				institucionEducativaIE.setSienvio(ie.getSienvio());
				institucionEducativaIE.setGestion(ie.getGestion().getValor());
				institucionEducativaIE.setIgel(convertIgel(ie.getIgel()));
				institucionEducativaIE.setNivelModalidad(convertNivelModalidad(ie.getNivelModalidad()));
				institucionEducativaIE.setGestionDependencia(convertGestionDependencia(ie.getGestionDependencia()));
				institucionEducativaIE.setEstado(convertEstadoPadron(ie.getEstado()));
				institucionEducativaIE.setUgel(convertUgel(ie.getUgel()));
				institucionEducativaIE.setGestionIe(convertGestion(ie.getGestion()));
				institucionEducativaIE.setDistrito(convertDistrito(ie.getDistrito()));
				list.add(institucionEducativaIE);
			}
		}
		return list;
	}

	private Igel convertIgel(pe.gob.minedu.escale.eol.dto.entities.IgelDTO igelDTO) {
		Igel igel = new Igel();
		if (igelDTO != null) {
			igel.setIdIgel(igelDTO.getIdIgel());
			igel.setNombreUgel(igelDTO.getNombreUgel());
		}

		return igel;
	}

	private NivelModalidad convertNivelModalidad(
			pe.gob.minedu.escale.eol.dto.entities.NivelModalidadDTO nivelModalidadDTO) {
		NivelModalidad nivelModalidad = new NivelModalidad();
		if (nivelModalidadDTO != null) {
			nivelModalidad.setIdCodigo(nivelModalidadDTO.getIdCodigo());
			nivelModalidad.setValor(nivelModalidadDTO.getValor());
		}

		return nivelModalidad;
	}

	private GestionDependencia convertGestionDependencia(
			pe.gob.minedu.escale.eol.dto.entities.GestionDependenciaDTO gestionDependenciaDTO) {
		GestionDependencia gestionDependencia = new GestionDependencia();
		if (gestionDependenciaDTO != null) {
			gestionDependencia.setIdCodigo(gestionDependenciaDTO.getIdCodigo());
			gestionDependencia.setValor(gestionDependenciaDTO.getValor());
		}

		return gestionDependencia;
	}

	private EstadoPadron convertEstadoPadron(pe.gob.minedu.escale.eol.dto.entities.EstadoDTO estadoDTO) {
		EstadoPadron estadoPadron = new EstadoPadron();
		if (estadoDTO != null) {
			estadoPadron.setIdCodigo(estadoDTO.getIdCodigo());
			estadoPadron.setValor(estadoDTO.getValor());
		}

		return estadoPadron;
	}

	private Ugel convertUgel(pe.gob.minedu.escale.eol.dto.entities.UgelDTO ugelDTO) {
		Ugel ugel = new Ugel();
		if (ugelDTO != null) {
			ugel.setIdUgel(ugelDTO.getIdUgel());
			ugel.setNombreUgel(ugelDTO.getNombreUgel());
		}

		return ugel;
	}

	private Gestion convertGestion(pe.gob.minedu.escale.eol.dto.entities.GestionDTO gestionDTO) {
		Gestion gestion = new Gestion();
		if (gestionDTO != null) {
			gestion.setIdCodigo(gestionDTO.getIdCodigo());
			gestion.setValor(gestionDTO.getValor());
		}

		return gestion;
	}

	private Distrito convertDistrito(pe.gob.minedu.escale.eol.dto.entities.DistritoDTO distritoDTO) {
		Distrito distrito = new Distrito();
		if (distritoDTO != null) {
			distrito.setIdDistrito(distritoDTO.getIdDistrito());
			distrito.setNombreDistrito(distritoDTO.getNombreDistrito());
		}

		return distrito;
	}
}
