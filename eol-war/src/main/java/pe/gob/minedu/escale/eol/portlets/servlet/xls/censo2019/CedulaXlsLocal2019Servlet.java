package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2019;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.dto.entities.NivelModalidadDTO;
import pe.gob.minedu.escale.eol.padron.ejb.InstitucionLocal;
import pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml.Campos;
import pe.gob.minedu.escale.eol.portlets.util.MensajeJS;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;
import pe.gob.minedu.escale.rest.client.PadronClient;


public class CedulaXlsLocal2019Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(CedulaXlsLocal2019Servlet.class);
	
	static final Logger LOGGER = Logger.getLogger(CedulaXlsLocal2019Servlet.class.getName());
    static final String CEDULA11_DESC = "CensoEscolarCed11_2019.xls";
    static final String PATH_DIR = "2019/cedulas";
    static final String[] listNiv = {"A1", "A2", "A3", "B0", "F0", "D0", "K0", "T0", "M0", "E0", "L0"};
    static final String[] listEsp = {"1", "2", "3", "4", "5"};
    static final String[] listEstA = {"1", "2", "3"};
    static final String[] list402I4 = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
    static final String[] list402I5 = {"1", "2"};
    private Campos campos;
    
    @EJB
	private InstitucionLocal institucionFacade = lookup(
			"java:global/eol-war/InstitucionFacade!pe.gob.minedu.escale.eol.padron.ejb.InstitucionLocal");
       
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    	    throws ServletException, IOException {
    	logger.info(":: CedulaXlsLocal2019Servlet.processRequest :: Starting execution...");
        MessageResources res = MessageResources.getMessageResources("eol");
        
        //Instituciones ies = null;
        InputStream is = null;

        List<InstitucionDTO> liInst = null;
        //List<String> estados = new ArrayList<String>(Arrays.asList("1"));
                
        PadronClient pCli = new PadronClient();
        String nivMod = request.getParameter("nivel");
        String codMod = request.getParameter("codmod");
        String anexo = request.getParameter("anexo");
        String codLocal = request.getParameter("codlocal");

        boolean existChar = false;
        List<String> reqParams = new ArrayList<String>();
        reqParams.add(nivMod);
        reqParams.add(codMod);
        reqParams.add(anexo);
        reqParams.add(codLocal);

        existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

        if(!existChar){
            
            try {
            	
            	liInst = institucionFacade.getInstitucionList(null, codLocal, 
            			null, null, null, null, null, null, null, null, null, 
            			null, null, null, null, null, null, new ArrayList<String>(Arrays.asList("1")), null, null, 0, 50, 
            			"anexo");
            	
	            // ies = pCli.get_JSON(Instituciones.class, String.format("codlocal=%s", codLocal), "estados=1");

	    	    // InputStream is = getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2019/cedula11/" + CEDULA11_DESC);

	            //String rutaRaiz = getServletContext().getRealPath("/WEB-INF/classes/pe/gob/minedu/escale/eol/portlets/plantilla/censo2019/cedula11/" + CEDULA11_DESC);
	            String rutaRaiz = res.getMessage("eol.path.templates.xls")+"censo2019/cedula11/"+CEDULA11_DESC;
	            
	            
				is = new FileInputStream(rutaRaiz);
				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
            
            if (is != null) {
                HSSFWorkbook hssfw = new HSSFWorkbook(is);
                //if (ies.getInstitucion() == null) {
                if (liInst == null) {
                    response.setContentType("text/html;charset=UTF-8");
                    PrintWriter out = response.getWriter();
                    try {
                        out.print(MensajeJS.alerta("No se encontraros Instituciones para el código de local" + codLocal));
                        out.print(MensajeJS.pagina_anterior());

                    } finally {
                        out.close();
                        is.close();
                    }
                } else {
                    //llenarSeccion104(hssfw, codMod, anexo, codLocal, nivMod, (List<InstitucionEducativaIE>) ies.getInstitucion());
                	llenarSeccion104(hssfw, codMod, anexo, codLocal, nivMod, liInst);

                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\"", CEDULA11_DESC));
                    OutputStream os = response.getOutputStream();
                    hssfw.write(os);
                    os.close();
                }
            }

        }else{
            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/recursos/error/errorsql.jsp");
            requestDispatcher.include(request, response);
        }
        
        logger.info(":: Fin CedulaXlsLocal2019Servlet.processRequest :: Finish execution...");
    }
    
	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

    	    //public void llenarSeccion104(HSSFWorkbook hssfw, String codMod, String anexo, String codLocal, String nivMod, List<InstitucionEducativaIE> centros) {
	public void llenarSeccion104(HSSFWorkbook hssfw, String codMod, String anexo, String codLocal, String nivMod, List<InstitucionDTO> ieslocal) {

    	        //Collections.sort(centros, buscarCentro);
				Collections.sort(ieslocal, buscarIE);
    	        /*InstitucionEducativaIE tmpCE = new InstitucionEducativaIE();
    	        tmpCE.setCodigoModular(codMod);
    	        tmpCE.setAnexo(anexo);
    	        tmpCE.setNivelModalidad(new NivelModalidad());
    	        tmpCE.getNivelModalidad().setIdCodigo(nivMod);*/
    	        
    	        InstitucionDTO tmpIE = new InstitucionDTO();
    	        tmpIE.setCodMod(codMod);
    	        tmpIE.setAnexo(anexo);
    	        tmpIE.setNivelModalidad(new NivelModalidadDTO());
    	        tmpIE.getNivelModalidad().setIdCodigo(nivMod);

    	        codLocal = codLocal != null ? codLocal : "";
    	        anexo = anexo != null ? anexo : "";
    	        codMod = codMod != null ? codMod : "";

    	        int pos = Collections.binarySearch(ieslocal, tmpIE, buscarIE);

    	        if (pos > -1) {
    	        	tmpIE = ieslocal.get(pos);
    	            HSSFSheet sheetSEC104 = hssfw.getSheet("C100");
    	            setCell(sheetSEC104, "X", 6, codLocal);//Código Local
    	            //INICIO PRIMERA FILA SECCION 104
    	            int filaInicio = 93;
    	            setCell(sheetSEC104, "H", filaInicio, tmpIE.getCodMod());//Código Modular
    	            setCell(sheetSEC104, "J", filaInicio, tmpIE.getAnexo());//Anexo
    	            setCell(sheetSEC104, "K", filaInicio, tmpIE.getCenEdu());//Nombre de la I.E.
    	            //setCell(sheetSEC104, "V", filaInicio, "0"+tmpIE.getGestion().getIdCodigo()+" : " +tmpIE.getGestion().getValor());
    	            //setCell(sheetSEC104, "AB", filaInicio, tmpIE.getNivelModalidad().getIdCodigo()+" : " +tmpIE.getNivelModalidad().getValor());
//    	            setCell(sheetSEC104, "U", filaInicio, tmpCE.getGestion()+" : " +tmpCE.getGestion());
    	            
    	            //SETEO DE UBICACION
    	            setCell(sheetSEC104, "P", 13, tmpIE.getDistrito().getIdDistrito());//Anexo
    	            setCell(sheetSEC104, "P", 15, tmpIE.getUgel().getIdUgel());//Anexo

    	            //FIN PRIMERA FILA SECCION 104
    	            ieslocal.remove(pos);
    	            for (InstitucionDTO ce : ieslocal) {

    	                if(ce.getNivelModalidad()!=null)
    	                {   filaInicio = filaInicio + 1;
    	                    setCell(sheetSEC104, "H", filaInicio, ce.getCodMod());//Código Modular
    	                    setCell(sheetSEC104, "J", filaInicio, ce.getAnexo());//Anexo
    	                    setCell(sheetSEC104, "K", filaInicio, ce.getCenEdu());//Nombre I.E
    	                    //setCell(sheetSEC104, "V", filaInicio, "0"+ce.getGestion().getIdCodigo()+" : " +ce.getGestion().getValor());
    	                    //setCell(sheetSEC104, "AB", filaInicio, ce.getNivelModalidad().getIdCodigo() +" : " +ce.getNivelModalidad().getValor());//Nivel de I.E
    	                    
    	                }
    	            }
    	        }
    	    }

    	    public void llenarCampo(HSSFWorkbook hssfw, String hoja, int rowCel, String colCel, String type, Object valor) {
    	        HSSFSheet sheet = hssfw.getSheet(hoja);
    	        if (type.equals("Integer")) {
    	            setCell(sheet, colCel, rowCel, (Integer) valor);
    	        } else if (type.equals("String")) {
    	            setCell(sheet, colCel, rowCel, valor.toString());
    	        }
    	    }

    	    public static boolean existeCampoMetodo(String nombre, Method[] metodos) {
    	        for (Method m : metodos) {
    	            if (m.getName().equals(nombre)) {
    	                return true;
    	            }
    	        }
    	        return false;
    	    }

    	    @Override
    	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    	    throws ServletException, IOException {
    	        processRequest(request, response);
    	    }

    	    @Override
    	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    	    throws ServletException, IOException {
    	        processRequest(request, response);
    	    }

    	    @Override
    	    public String getServletInfo() {
    	        return "Short description";
    	    }// </editor-fold>

    	    static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
    	        setCell(sheet, col, fil, String.valueOf(texto));
    	    }

    	    static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

    	        HSSFRow row = sheet.getRow(fil - 1);
    	        if (row != null) {
    	            try {
    	                row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
    	            } catch (java.lang.NullPointerException e) {
    	                //LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
    	                //logger.error("problem " + ioe.getMessage());
    	            	e.printStackTrace();
    	            }
    	        }
    	    }

    	    static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
    	        HSSFRow row = sheet.getRow(fil - 1);
    	        HSSFCell cell = row.getCell(letter2col(col));
    	        cell.setCellValue(texto);
    	    }

    	    static int letter2col(String col) {
    	        int iCol = 0;
    	        short delta = 'Z' - 'A' + 1;

    	        if (col.length() < 2) {
    	            return (col.charAt(0) - 'A');
    	        }
    	        iCol = col.charAt(1) - 'A';
    	        iCol += (col.charAt(0) - 'A' + 1) * delta;
    	        /*for (int i = col.length(); i > 0; i--) {
    	        char chr = col.charAt(i - 1);
    	        short iChr = (short) (chr - 'A');
    	        iCol += (iChr * (col.length() - i + 1) * delta);
    	        }*/
    	        return iCol;
    	    }

    	    /*
    	    public Comparator<InstitucionEducativaIE> buscarCentro = new Comparator<InstitucionEducativaIE>() {

    	        public int compare(InstitucionEducativaIE o1, InstitucionEducativaIE o2) {
    	            if ((o1.getCodigoModular()).compareTo(o2.getCodigoModular()) == 0) {
    	                if (o1.getAnexo().compareTo(o2.getAnexo()) == 0) {
    	                    return o1.getNivelModalidad().getIdCodigo().compareTo(o2.getNivelModalidad().getIdCodigo());
    	                } else {
    	                    return ((String) o1.getAnexo()).compareTo((String) o2.getAnexo());
    	                }
    	            } else {
    	                return o1.getCodigoModular().compareTo(o2.getCodigoModular());
    	            }
    	        }
    	    };*/
    	    
    	    public Comparator<InstitucionDTO> buscarIE = new Comparator<InstitucionDTO>() {

    	        public int compare(InstitucionDTO o1, InstitucionDTO o2) {
    	            if ((o1.getCodMod()).compareTo(o2.getCodMod()) == 0) {
    	                if (o1.getAnexo().compareTo(o2.getAnexo()) == 0) {
    	                    return o1.getNivelModalidad().getIdCodigo().compareTo(o2.getNivelModalidad().getIdCodigo());
    	                } else {
    	                    return ((String) o1.getAnexo()).compareTo((String) o2.getAnexo());
    	                }
    	            } else {
    	                return o1.getCodMod().compareTo(o2.getCodMod());
    	            }
    	        }
    	    };
    	    
    	    

}
