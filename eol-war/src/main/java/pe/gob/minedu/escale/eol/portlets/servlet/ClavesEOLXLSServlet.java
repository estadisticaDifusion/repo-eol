/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.ejb.SesionUsuarioFacade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;

/**
 *
 * @author DSILVA
 */
public class ClavesEOLXLSServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// private Set<String> codigos;

	private Logger logger = Logger.getLogger(ClavesEOLXLSServlet.class);

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@EJB
	private SesionUsuarioFacade usuarioFacade;

	private Map<String, CentroEducativo> codigosMap;
	private ResourceBundle mensajes;

	static Comparator<Map<String, String>> codigoModularComparator = new Comparator<Map<String, String>>() {

		public int compare(Map<String, String> o1, Map<String, String> o2) {
			return o1.get("codigoModular").compareTo(o2.get("codigoModular"));
		}
	};

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: ClavesEOLXLSServlet.processRequest :: Starting execution...");
		HttpSession session = request.getSession();
		AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "atachment;file=claves_eol-" + usuario.getUsuario() + ".xls");
		OutputStream os = response.getOutputStream();
		String idUgel = usuario.getUsuario();
		if (idUgel.endsWith("00") || idUgel.equals("150101") || idUgel.equals("070101")) {
			idUgel = usuario.getUsuario().substring(0, 4) + "%";
		}

		List<CentroEducativo> listaCentros = padronFacade.getCentrosByUgel(idUgel);
		List<AuthUsuario> listaUsuarios = usuarioFacade.getListaCentrosRegistrados(idUgel);
		codigosMap = extractCodigosMap(listaCentros);
		mensajes = ResourceBundle.getBundle("ApplicationResources");
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet hojaIIEE = wb.createSheet("IIEE Y Programas");

		ponerLista(usuario, new String[] { "Instituciones Educativas", "Código modular", "Nombre de I.E./Pro", "Nivel",
				"Funciona" }, hojaIIEE, wb, listaUsuarios);
		// ponerLista(usuario, new String[]{"Coordinaciones", "Código coord.", "Nombre
		// coordinación", "", "Funciona"}, hojaCoor, wb, progs);
		wb.write(os);
		os.close();
		logger.info(":: ClavesEOLXLSServlet.processRequest :: Execution finish.");
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private void ponerBordes(HSSFCellStyle style) {
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(HSSFColor.BLACK.index);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setLeftBorderColor(HSSFColor.BLACK.index);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setRightBorderColor(HSSFColor.BLACK.index);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setTopBorderColor(HSSFColor.BLACK.index);
	}

	private void ponerLista(AuthUsuario usuario, String[] string, HSSFSheet sheet, HSSFWorkbook wb,
			List<AuthUsuario> lista) {
		// titulo
		HSSFFont titleFont = wb.createFont();
		titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		titleFont.setFontHeightInPoints((short) 25);
		HSSFRow titleRow = sheet.createRow((short) 0);
		HSSFCell titleCell = titleRow.createCell(0);
		HSSFRichTextString titleText = new HSSFRichTextString("Claves EOL de " + usuario.getNombre());
		titleText.applyFont(titleFont);
		titleCell.setCellValue(titleText);
		// subtitulo
		HSSFRow subTitleRow = sheet.createRow((short) 1);
		HSSFCell subtitleCell = subTitleRow.createCell(0);
		HSSFFont subtitleFont = wb.createFont();
		subtitleFont.setFontHeightInPoints((short) 15);
		HSSFRichTextString subtitleText = new HSSFRichTextString(string[0]);
		subtitleText.applyFont(subtitleFont);
		subtitleCell.setCellValue(subtitleText);
		// cabecera de tabla
		HSSFRow headerRow = sheet.createRow(3);
		HSSFCellStyle headerStyle = wb.createCellStyle();
		ponerBordes(headerStyle);

		headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFCell headerCell0 = headerRow.createCell(0);
		HSSFFont headerFont = wb.createFont();
		headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		HSSFRichTextString header0 = new HSSFRichTextString(string[1]);
		header0.applyFont(headerFont);
		headerCell0.setCellValue(header0);
		headerCell0.setCellStyle(headerStyle);
		HSSFCell headerCell1 = headerRow.createCell(1);
		HSSFRichTextString header1 = new HSSFRichTextString(string[2]);
		header1.applyFont(headerFont);
		headerCell1.setCellValue(header1);
		headerCell1.setCellStyle(headerStyle);

		HSSFCell headerCell2 = headerRow.createCell(2);
		HSSFRichTextString header2 = new HSSFRichTextString("Clave EOL");
		header2.applyFont(headerFont);
		headerCell2.setCellValue(header2);
		headerCell2.setCellStyle(headerStyle);

		HSSFCell headerCell3 = headerRow.createCell(3);
		HSSFRichTextString header3 = new HSSFRichTextString(string[3]);
		header3.applyFont(headerFont);
		headerCell3.setCellValue(header3);
		headerCell3.setCellStyle(headerStyle);

		HSSFCell headerCell4 = headerRow.createCell(4);
		HSSFRichTextString header4 = new HSSFRichTextString("Tipo de Programa");
		header4.applyFont(headerFont);
		headerCell4.setCellValue(header4);
		headerCell4.setCellStyle(headerStyle);

		HSSFCell headerCell5 = headerRow.createCell(5);
		HSSFRichTextString header5 = new HSSFRichTextString(string[4]);
		header5.applyFont(headerFont);
		headerCell5.setCellValue(header5);
		headerCell5.setCellStyle(headerStyle);

		HSSFCell headerCell6 = headerRow.createCell(6);
		HSSFRichTextString header6 = new HSSFRichTextString("UGEL");
		header6.applyFont(headerFont);
		headerCell6.setCellValue(header6);
		headerCell6.setCellStyle(headerStyle);

		sheet.setColumnWidth(0, (short) 5000);
		AuthUsuario item;
		HSSFCellStyle borderStyle = wb.createCellStyle();
		ponerBordes(borderStyle);

		for (int i = 0; i < lista.size(); i++) {
			item = lista.get(i);
			Map<String, String> key = new HashMap<String, String>();
			key.put("codigoModular", item.getUsuario());
			CentroEducativo ce = codigosMap.get(item.getUsuario());

			boolean funciona = (ce != null && ce.getEstado().equals("1"));

			HSSFRow row = sheet.createRow(i + 4);
			HSSFCell cell0 = row.createCell(0, HSSFCell.CELL_TYPE_STRING);
			cell0.setCellValue(new HSSFRichTextString(item.getUsuario()));
			cell0.setCellStyle(borderStyle);

			HSSFCell cell1 = row.createCell(1, HSSFCell.CELL_TYPE_STRING);
			cell1.setCellValue(new HSSFRichTextString(item.getNombre()));
			cell1.setCellStyle(borderStyle);

			HSSFCell cell2 = row.createCell(2, HSSFCell.CELL_TYPE_STRING);
			cell2.setCellValue(new HSSFRichTextString(item.getContrasenia()));
			cell2.setCellStyle(borderStyle);

			HSSFCell cell3 = row.createCell(3, HSSFCell.CELL_TYPE_STRING);
			if (ce != null) {
				cell3.setCellValue(new HSSFRichTextString(ce.getNivMod() != null
						? mensajes.getString(String.format("cedula.nivelModalidad.%s", ce.getNivMod()))
						: ""));
			} else {
				cell3.setCellValue(new HSSFRichTextString(""));
			}
			cell3.setCellStyle(borderStyle);

			HSSFCell cell4 = row.createCell(4, HSSFCell.CELL_TYPE_STRING);
			if (ce != null) {
				if (ce.getTipoprog() != null && !ce.getTipoprog().trim().equals("")) {
					cell4.setCellValue(new HSSFRichTextString(
							mensajes.getString(String.format("cedula.tipProg.%s", ce.getTipoprog()))));
				} else {
					cell4.setCellValue(new HSSFRichTextString(""));
				}
			} else {
				cell4.setCellValue(new HSSFRichTextString(""));
			}
			cell4.setCellStyle(borderStyle);

			HSSFCell cell5 = row.createCell(5, HSSFCell.CELL_TYPE_STRING);
			cell5.setCellValue(new HSSFRichTextString(funciona ? "Sí" : "No"));
			cell5.setCellStyle(borderStyle);

			HSSFCell cell6 = row.createCell(6, HSSFCell.CELL_TYPE_STRING);
			cell6.setCellValue(new HSSFRichTextString(item.getIdRegistrador()));
			cell6.setCellStyle(borderStyle);

		}

		sheet.autoSizeColumn((short) 1);
		sheet.autoSizeColumn((short) 2);
	}

	public static Map<String, CentroEducativo> extractCodigosMap(List<CentroEducativo> centros) {
		Map<String, CentroEducativo> codigos = new HashMap<String, CentroEducativo>();
		for (CentroEducativo ce : centros) {
			codigos.put(ce.getCodMod(), ce);
		}
		return codigos;
	}

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
