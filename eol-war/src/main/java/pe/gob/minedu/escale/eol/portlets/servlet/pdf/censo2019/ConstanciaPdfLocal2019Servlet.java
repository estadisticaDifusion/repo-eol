package pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2019;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

/**
 * Servlet implementation class ConstanciaPdfLocal2019Servlet
 */
public class ConstanciaPdfLocal2019Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(ConstanciaPdfLocal2019Servlet.class);
       
	@Resource(name = "eol")
	private DataSource eol;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: ConstanciaPdfLocal2019Servlet.processRequest :: Starting execution...");
		Connection conn = null;
		try {
			conn = eol.getConnection();

			String idEnvio = request.getParameter("idEnvio");

			boolean existChar = false;
			List<String> reqParams = new ArrayList<String>();
			reqParams.add(idEnvio);

			existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

			if (!existChar) {

				Map<String, Object> params = new HashMap<String, Object>();
				MessageResources res = MessageResources.getMessageResources("eol");
				String rutaRaiz = res.getMessage("eol.path.constancia");
				InputStream is = null;
                InputStream resourcei = null;

				params.put("IdEnvio", idEnvio);
				
				logger.info("params="+params);
                String report = rutaRaiz+"censo2019/cedula11/";
				
				params.put("SUBREPORT_DIR", report);

				String urlMaster = rutaRaiz+"censo2019/cedula11/Cedula-11-2019.jasper";
                /*String fileName = res.getMessage("eol.path.constancia") + "censo2019/cedula11/Cedula-11-2019.jasper";
                logger.info("fileName="+fileName);
                InputStream reportStream = new FileInputStream(fileName);
                logger.info("reportStream="+reportStream);*/

				//JasperReport masterReport = (JasperReport) JRLoader.loadObject(reportStream);
				is = new FileInputStream(urlMaster);
                JasperReport masterReport = (JasperReport) JRLoader.loadObject(is);
                
                resourcei = new FileInputStream(rutaRaiz+"censo2019/cedula11/etiquetas.properties");
                ResourceBundle bundle = new PropertyResourceBundle(resourcei);
                
				String escudo = rutaRaiz+"escudom.jpg";
				// String escudo = res.getMessage("eol.path.constancia") + "escudom.jpg";
				params.put("REPORT_LOCALE", new java.util.Locale("ES", "PE"));
				params.put("REPORT_RESOURCE_BUNDLE", bundle);
				params.put("escudo", escudo);

				byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
				response.setContentType("application/pdf");
				response.setContentLength(buffer.length);
				ServletOutputStream ouputStream = response.getOutputStream();
				ouputStream.write(buffer, 0, buffer.length);
				ouputStream.flush();
				ouputStream.close();

			} else {
				RequestDispatcher requestDispatcher = getServletContext()
						.getRequestDispatcher("/recursos/error/errorsql.jsp");
				requestDispatcher.include(request, response);
			}

		} catch (SQLException ex) {
			log(ex.getMessage(), ex);
		} catch (JRException ex) {
			log(ex.getMessage(), ex);
		} catch (Exception ex) {
			log(ex.getMessage(), ex);
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException ex) {
					log(ex.getMessage(), ex);
				}
			}
		}
		logger.info(":: ConstanciaPdfLocal2019Servlet.processRequest :: Execution finish.");
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>


}
