package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2017;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.eol.estadistica.ejb.PrecargaCedulaIDFacade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;

public class CedulaXlsID2017Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(CedulaXlsID2017Servlet.class);

	static final String CEDULA_MATRICULA_DESC = "IdentificacionIECedID_";
	static final String PERIODO = "2017";
	static final String PATH_DIR = "2017/cedulas";

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@EJB
	private PrecargaCedulaIDFacade padroncedulaIDFacade;
	@Resource(name = "eol")
	private DataSource eol;

	@SuppressWarnings("unused")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: CedulaXlsID2017Servlet.processRequest :: Starting execution...");
		String preCed = "";
		String codigoModularDominante = "";
		String nivMod = request.getParameter("nivel");
		String anexo = request.getParameter("anexo");
		String codMod = request.getParameter("codmod");
		String codied = request.getParameter("codinst");
		// InputStream is =
		// getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/IdentificacionIECedID_2017.xls");
		CentroEducativo ie = padronFacade.obtainByCodMod(codMod, anexo);
		Object[] cabeceraID = padroncedulaIDFacade.getPrellenadoCabeceraCedulaID2017(codied);
		if (cabeceraID != null) {
			// Caso 1.1: Es dominante.
			if (ie.getCodMod().equals(cabeceraID[16].toString())) {
				descargarExcel(ie, codied, cabeceraID, response);
			} else {
				// Caso 1.2: No es dominante. descargar archivo PDF
				descargarPdf(codied, codMod, Boolean.TRUE, response);
			}
		} else {
			codigoModularDominante = encontrarCodigoDominante(codied);
			if (codigoModularDominante != null && !codigoModularDominante.isEmpty()) {
				if (codMod.equals(codigoModularDominante)) {
					// Caso 2.1: Descargar Excel
					descargarExcel(ie, codied, cabeceraID, response);
				} else {
					// Caso 2.2: No es dominante. descargar archivo PDF
					descargarPdf(codigoModularDominante, codMod, Boolean.FALSE, response);
				}
			}
		}
		logger.info(":: CedulaXlsID2017Servlet.processRequest :: Execution finish.");
	}

	public void descargarExcel(CentroEducativo ie, String codied, Object[] cabeceraID, HttpServletResponse response)
			throws IOException {
		// Inicio - Descargar archivo excel
		// InputStream is =
		// getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/IdentificacionIECedID_2017.xls");
		String rutaRaiz = getServletContext().getRealPath(
				"/WEB-INF/classes/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/IdentificacionIECedID_2017.xls");
		InputStream is = new FileInputStream(rutaRaiz);
		if (is != null) {
			HSSFWorkbook hssfw = new HSSFWorkbook(is);
			llenadoCedulaID(hssfw, ie, codied, cabeceraID);
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition",
					String.format("attachment;filename=\"IdentificacionIECedID_%s.xls\"", new Object[] { "2017" }));
			OutputStream os = response.getOutputStream();
			try {
				hssfw.write(os);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} finally {
				os.close();
				is.close();
			}

		}
		// Fin - Descargar archivo excel
	}

	public void descargarPdf(String codigoIns, String codigoModular, Boolean prellenado, HttpServletResponse response) {
		Connection conn = null;
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			conn = eol.getConnection();
			params.put("ID_ENVIO", codigoIns);
			params.put("COD_MOD", codigoModular);
			String report = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/matricula")
					.getFile();
			URL urlMaster = getClass().getResource(
					"/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/matricula/cedula_cid_2017_prellenado"
							+ (prellenado ? "" : "_padron") + ".jasper");
			params.put("SUBREPORT_DIR", report);
			JasperReport masterReport = (JasperReport) JRLoader.loadObject(urlMaster);
			String escudo = getClass()
					.getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2017/matricula/Escudo.JPG")
					.getFile();
			params.put("REPORT_LOCALE", new java.util.Locale("ES", "PE"));
			params.put("escudo", escudo);

			byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
			response.setContentType("application/pdf");
			response.setContentLength(buffer.length);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(buffer, 0, buffer.length);
			ouputStream.flush();
			ouputStream.close();

		} catch (JRException ex) {
			log(ex.getMessage(), ex);
		} catch (Exception ex) {
			log(ex.getMessage(), ex);
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException ex) {
					log(ex.getMessage(), ex);
				}
			}
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}

	private void llenadoCedulaID(HSSFWorkbook hssfw, CentroEducativo ie, String codied, Object[] cabeceraID) {
		HSSFSheet sheetC100 = hssfw.getSheet("C100");
		setCell(sheetC100, "H", 7, codied);
		setCell(sheetC100, "D", 1, ie.getCodigel());
		if (cabeceraID != null) {
			setCell(sheetC100, "C", 1, ie.getCodMod());
			List<Object[]> listaDetID = padroncedulaIDFacade.getPrellenadoDetalleCedulaID2017(codied);
			setCell(sheetC100, "D", 16, cabeceraID[2].toString());
			if (cabeceraID[3] != null)
				setCell(sheetC100,
						cabeceraID[3].toString().equals("1") ? "D" : (cabeceraID[3].toString().equals("2") ? "G" : "N"),
						18, "X");
			setCell(sheetC100, "D", 20, cabeceraID[5] != null ? cabeceraID[5].toString() : "");
			setCell(sheetC100, "D", 22, cabeceraID[6] != null ? cabeceraID[6].toString() : "");
			setCell(sheetC100, "D", 24, cabeceraID[7] != null ? cabeceraID[7].toString() : "");
			setCell(sheetC100, "K", 24, cabeceraID[8] != null ? cabeceraID[8].toString() : "");
			setCell(sheetC100, "D", 30, cabeceraID[9] != null ? cabeceraID[9].toString() : "");
			setCell(sheetC100, "H", 30, cabeceraID[10] != null ? cabeceraID[10].toString() : "");
			setCell(sheetC100, "K", 30, cabeceraID[11] != null ? cabeceraID[11].toString() : "");
			setCell(sheetC100, "D", 33, cabeceraID[12] != null ? cabeceraID[12].toString() : "");
			setCell(sheetC100, "F", 41, cabeceraID[13] != null ? cabeceraID[13].toString() : "");
			setCell(sheetC100, "F", 43, cabeceraID[14] != null ? cabeceraID[14].toString() : "");
			setCell(sheetC100, "G", 45, cabeceraID[15] != null ? cabeceraID[15].toString() : "");
			setCell(sheetC100, "C", 1, cabeceraID[16] != null ? cabeceraID[16].toString() : "");

			if (listaDetID != null) {
				int cuadro = Integer.parseInt(String.valueOf(cabeceraID[4]));
				switch (cuadro) {
				case 1:
					HSSFSheet sheetC201 = hssfw.getSheet("C201");
					llenadoC201(sheetC201, listaDetID);
					setCell(sheetC100, "A", 1, "1");
					break;
				case 2:
					HSSFSheet sheetC202 = hssfw.getSheet("C202");
					llenadoC202(sheetC202, listaDetID);
					setCell(sheetC100, "A", 1, "2");
					break;
				default:
					HSSFSheet sheetC203 = hssfw.getSheet("C203");
					llenadoC203(sheetC203, listaDetID);
					setCell(sheetC100, "A", 1, "3");
					break;
				}
			}
		} else {
			String codigoModular = "";
			codigoModular = encontrarCodigoDominante(codied);
			if (codigoModular != null && !codigoModular.isEmpty()) {
				setCell(sheetC100, "C", 1, codigoModular);
			}
			setCell(sheetC100, "A", 1, "0");
		}
	}

	private void llenadoC201(HSSFSheet sheetC201, List<Object[]> listaDetID) {
		int itemIni = 12;
		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);
			setCell(sheetC201, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC201, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC201, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC201, "G", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC201, "H", itemIni, object[7] != null ? object[7].toString() : "");
			setCell(sheetC201, "H", itemIni + 1, object[8] != null ? object[8].toString() : "");
			setCell(sheetC201, "I", itemIni, object[9] != null ? object[9].toString() : "");
			setCell(sheetC201, "K", itemIni, object[10] != null ? object[10].toString() : "");
			setCell(sheetC201, "Q", itemIni, object[11] != null ? object[11].toString() : "");
			setCell(sheetC201, "V", itemIni, object[12] != null ? object[12].toString() : "");
			setCell(sheetC201, "AA", itemIni, object[13] != null ? object[13].toString() : "");
			setCell(sheetC201, "AE", itemIni, object[14] != null ? object[14].toString() : "");
			setCell(sheetC201, "AF", itemIni, object[15] != null ? object[15].toString() : "");
			setCell(sheetC201, "AG", itemIni, object[16] != null ? object[16].toString() : "");
			itemIni = itemIni + 2;
		}
	}

	private void llenadoC202(HSSFSheet sheetC202, List<Object[]> listaDetID) {
		int itemIni = 12;
		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);
			setCell(sheetC202, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC202, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC202, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC202, "G", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC202, "H", itemIni, object[7] != null ? object[7].toString() : "");
			setCell(sheetC202, "H", itemIni + 1, object[8] != null ? object[8].toString() : "");
			setCell(sheetC202, "I", itemIni, object[17] != null ? object[17].toString() : "");
			setCell(sheetC202, "J", itemIni, object[18] != null ? object[18].toString() : "");
			setCell(sheetC202, "K", itemIni, object[19] != null ? object[19].toString() : "");
			setCell(sheetC202, "L", itemIni, object[16] != null ? object[16].toString() : "");
			itemIni = itemIni + 2;
		}
	}

	private void llenadoC203(HSSFSheet sheetC203, List<Object[]> listaDetID) {
		int itemIni = 12;
		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);
			setCell(sheetC203, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC203, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC203, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC203, "G", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC203, "H", itemIni, object[7] != null ? object[7].toString() : "");
			setCell(sheetC203, "H", itemIni + 1, object[8] != null ? object[8].toString() : "");
			setCell(sheetC203, "I", itemIni, object[16] != null ? object[16].toString() : "");
			itemIni = itemIni + 2;
		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		if (texto != null && !texto.isEmpty()) {
			cell.setCellValue(texto);
		}

	}

	static void setCell(HSSFSheet sheet, int col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(col);
		cell.setCellValue(texto);
	}

	@SuppressWarnings("unused")
	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			int bc = (col.charAt(0) - 'A');
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String encontrarCodigoDominante(String codied) {
		// Busco si es dominante
		String codigoModular = "";
		int valorMayor = 0, valor = 0;

		List<CentroEducativo> listaCentroEducativo = padronFacade.getCentrosByCodInst(codied);
		// Llenar los niveles, datos alcanzados hasta Marzo 2017.
		Map<String, Integer> listaNivMod = new HashMap();
		listaNivMod.put("L0", 1);
		listaNivMod.put("D1", 2);
		listaNivMod.put("D2", 3);
		listaNivMod.put("E1", 4);
		listaNivMod.put("E2", 5);
		listaNivMod.put("A1", 6);
		listaNivMod.put("A2", 7);
		listaNivMod.put("A3", 8);
		listaNivMod.put("B0", 9);
		listaNivMod.put("F0", 10);
		listaNivMod.put("M0", 11);
		listaNivMod.put("K0", 12);
		listaNivMod.put("T0", 13);
		// Verificar si es dominante segun regla: L0 < D1 < D2 < E1 < E2 < AX < B0 < F0
		// < M0 < K0 < T0
		if (!listaCentroEducativo.isEmpty()) {
			if (listaCentroEducativo.size() > 1) {
				for (CentroEducativo ce : listaCentroEducativo) {
					valor = listaNivMod.get(ce.getNivMod());
					if (valor > valorMayor) {
						valorMayor = valor;
						codigoModular = ce.getCodMod();
					}
				}
			} else {
				codigoModular = listaCentroEducativo.get(0).getCodMod();
			}
		} else {
			codigoModular = "";// Agregar Excepcion
		}
		return codigoModular;
	}

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
