/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.servlet.est.censo2011;

import static pe.gob.minedu.escale.eol.padron.constant.Constantes.USUARIO_DRE;
import static pe.gob.minedu.escale.eol.padron.constant.Constantes.USUARIO_UGEL;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.portlets.domain.act.ResumenCoberturaMatricula;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;
import pe.gob.minedu.escale.rest.client.EolActividadesClient;

/**
 *
 * @author JMATAMOROS
 */

public class CoberturaResumenServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4532389888866061605L;
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */

	private Logger logger = Logger.getLogger(CoberturaResumenServlet.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@EJB
	private PadronLocal padronSrv = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: CoberturaResumenServlet.processRequest :: Starting execution...");
		String formato = request.getParameter("formato") != null ? request.getParameter("formato") : "";
		boolean enExcel = "xls".equals(formato);
		if (enExcel) {
			response.setHeader("Content-type", "application/octet-stream");
			response.addHeader("Content-Disposition", "attachment; filename=\"cuadro.xls\"");
		} else {
			response.setContentType("text/html;charset=UTF-8");
		}

		String codUGEL = request.getParameter("codUGEL");
		String codDRE = request.getParameter("codDRE");

		boolean existChar = false;
		List<String> reqParams = new ArrayList<String>();
		reqParams.add(formato);
		reqParams.add(codUGEL);
		reqParams.add(codDRE);

		existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

		if (!existChar) {

			try {
				// PadronFacade padronSrv = lookupPadronFacade();

				boolean esDRE = codDRE != null && !codDRE.isEmpty();

				/* ACTIVIDAD ADICIONAL MATRICULA INICIO */
				MessageResources res = MessageResources.getMessageResources("ApplicationResources");
				String regionessigied = res.getMessage("talero.region.sigied");
				String[] arrRegiones = regionessigied.split(",");
				boolean esregSigied = Arrays.asList(arrRegiones).contains((esDRE ? codDRE : codUGEL).substring(0, 4));

				String regioneshuelga = res.getMessage("talero.region.huelga");
				String[] arrRegHuelga = regioneshuelga.split(",");
				boolean esregHuelga = Arrays.asList(arrRegHuelga).contains((esDRE ? codDRE : codUGEL).substring(0, 4));

				EolActividadesClient actClient = new EolActividadesClient();
				List<EolActividadConverter> actividades = actClient.getActividadesConverter(null, "2019").getItems();
				EolActividadConverter actMatDet = actividades.get(0);

				Date fechTe = new Date();
				Calendar c = Calendar.getInstance();
				c.setTime(esregHuelga ? actMatDet.getFechaTerminohuelga()
						: (esregSigied ? actMatDet.getFechaTerminosigied() : actMatDet.getFechaTermino()));
//				c.add(Calendar.DATE, 1);
				c.add(Calendar.HOUR, -15);
				fechTe = c.getTime();

				/* ACTIVIDAD ADICIONAL FIN */

				request.setAttribute("NOMBRE_IGEL", esDRE ? codDRE : codUGEL);
				List<Object[]> lista = padronSrv.getCuentaCentrosGroupNivModMCenso(esDRE ? codDRE : codUGEL,
						esDRE ? USUARIO_DRE : USUARIO_UGEL);
				EnvioDocumentosFacade matriSrv = lookupEnvioDocumentosFacade();
				List<Object[]> listaMatri = matriSrv.getCuentaMatriculaMcenso2019(esDRE ? codDRE : codUGEL,
						esDRE ? USUARIO_DRE : USUARIO_UGEL, dateFormat.format(fechTe));
				List<Object[]> listaMatriPost = matriSrv.getCuentaMatriculaMcensoPost2019(esDRE ? codDRE : codUGEL,
						esDRE ? USUARIO_DRE : USUARIO_UGEL, dateFormat.format(fechTe));

				List<ResumenCoberturaMatricula> resumenMatri = new ArrayList<ResumenCoberturaMatricula>();
				ResumenCoberturaMatricula rcNT = new ResumenCoberturaMatricula("Total", "NT");
				ResumenCoberturaMatricula rcN1 = new ResumenCoberturaMatricula("Básica Regular", "N1");
				ResumenCoberturaMatricula rcN2 = new ResumenCoberturaMatricula("Inicial - Cuna y Jardin", "N2");
				ResumenCoberturaMatricula rcN3 = new ResumenCoberturaMatricula("Inicial - Programas", "N3");
				ResumenCoberturaMatricula rcINI_ART = new ResumenCoberturaMatricula("Inicial - Articulación", "N4");

				ResumenCoberturaMatricula rcN4 = new ResumenCoberturaMatricula("Primaria", "N5");
				ResumenCoberturaMatricula rcN5 = new ResumenCoberturaMatricula("Secundaria", "N6");
				ResumenCoberturaMatricula rcN6I = new ResumenCoberturaMatricula(
						"Básica Alternativa - Incial Intermedio", "N7");
				ResumenCoberturaMatricula rcN6A = new ResumenCoberturaMatricula("Básica Alternativa - Avanzado", "N7");
				ResumenCoberturaMatricula rcN7 = new ResumenCoberturaMatricula("Básica Especial", "N8");
				ResumenCoberturaMatricula rcN8 = new ResumenCoberturaMatricula("Técnico-Productiva", "N9");
				ResumenCoberturaMatricula rcN9 = new ResumenCoberturaMatricula("Superior No Universitaria", "N10");
				ResumenCoberturaMatricula rcN10 = new ResumenCoberturaMatricula("Pedagógica", "N11");
				ResumenCoberturaMatricula rcN11 = new ResumenCoberturaMatricula("Tecnológica", "N12");
				ResumenCoberturaMatricula rcN12 = new ResumenCoberturaMatricula("Artistica", "N13");

				rcNT.setNegrita(Boolean.TRUE);
				rcN1.setNegrita(Boolean.TRUE);
				rcN6I.setNegrita(Boolean.TRUE);
				rcN6A.setNegrita(Boolean.TRUE);
				rcN7.setNegrita(Boolean.TRUE);
				rcN8.setNegrita(Boolean.TRUE);
				rcN9.setNegrita(Boolean.TRUE);

				for (Object[] ces : lista) {
					rcNT.setCuentaCentros(rcNT.getCuentaCentros() + ((Number) ces[2]).intValue());

					if (ces[1].toString().equals("A1") || ces[1].toString().equals("A2")
							|| ces[1].toString().equals("A3")) {
						rcN2.setCuentaCentros(rcN2.getCuentaCentros() + ((Number) ces[2]).intValue());
					}
					if (ces[1].toString().equals("A4")) {
						rcINI_ART.setCuentaCentros(rcINI_ART.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("A5")) {
						rcN3.setCuentaCentros(rcN3.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("B0")) {
						rcN4.setCuentaCentros(rcN4.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("F0")) {
						rcN5.setCuentaCentros(rcN5.getCuentaCentros() + ((Number) ces[2]).intValue());
					}
					/*
					 * else if (ces[1].toString().equals("D0") || ces[1].toString().equals("C0") ||
					 * ces[1].toString().equals("G0")) {
					 * rcN6.setCuentaCentros(rcN6.getCuentaCentros() + ((Number)
					 * ces[2]).intValue()); }
					 */
					else if (ces[1].toString().equals("D1")) {
						rcN6I.setCuentaCentros(rcN6I.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("D2")) {
						rcN6A.setCuentaCentros(rcN6A.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("E0") || ces[1].toString().equals("E1")
							|| ces[1].toString().equals("E2")) {
						rcN7.setCuentaCentros(rcN7.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("L0")) {
						rcN8.setCuentaCentros(rcN8.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("K0")) {
						rcN10.setCuentaCentros(rcN10.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("T0")) {
						rcN11.setCuentaCentros(rcN11.getCuentaCentros() + ((Number) ces[2]).intValue());
					} else if (ces[1].toString().equals("M0")) {
						rcN12.setCuentaCentros(rcN12.getCuentaCentros() + ((Number) ces[2]).intValue());
					}
					if (listaMatri != null && ces != null) {
						int pos = Collections.binarySearch(listaMatri, ces, buscarLista);

						if (pos >= 0) {
							rcNT.setCuentaMatRecProf(
									rcNT.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							if (ces[1].toString().equals("A1") || ces[1].toString().equals("A2")
									|| ces[1].toString().equals("A3")) {
								rcN2.setCuentaMatRecProf(
										rcN2.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());
							}
							if (ces[1].toString().equals("A4")) {
								rcINI_ART.setCuentaMatRecProf(
										rcINI_ART.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());
							} else if (ces[1].toString().equals("A5")) {
								rcN3.setCuentaMatRecProf(
										rcN3.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							} else if (ces[1].toString().equals("B0")) {
								rcN4.setCuentaMatRecProf(
										rcN4.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							} else if (ces[1].toString().equals("F0")) {
								rcN5.setCuentaMatRecProf(
										rcN5.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());
							}
							/*
							 * else if (ces[1].toString().equals("D0") || ces[1].toString().equals("C0") ||
							 * ces[1].toString().equals("G0")) {
							 * rcN6.setCuentaMatRecProf(rcN6.getCuentaMatRecProf() + ((Number)
							 * listaMatri.get(pos)[2]).intValue()); }
							 */
							else if (ces[1].toString().equals("D1")) {
								rcN6I.setCuentaMatRecProf(
										rcN6I.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());
							} else if (ces[1].toString().equals("D2")) {
								rcN6A.setCuentaMatRecProf(
										rcN6A.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							} else if (ces[1].toString().equals("E0") || ces[1].toString().equals("E1")
									|| ces[1].toString().equals("E2")) {
								rcN7.setCuentaMatRecProf(
										rcN7.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							} else if (ces[1].toString().equals("L0")) {
								rcN8.setCuentaMatRecProf(
										rcN8.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							} else if (ces[1].toString().equals("K0")) {
								rcN10.setCuentaMatRecProf(
										rcN10.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							} else if (ces[1].toString().equals("T0")) {
								rcN11.setCuentaMatRecProf(
										rcN11.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							} else if (ces[1].toString().equals("M0")) {
								rcN12.setCuentaMatRecProf(
										rcN12.getCuentaMatRecProf() + ((Number) listaMatri.get(pos)[2]).intValue());

							}
						}

						int postpos = Collections.binarySearch(listaMatriPost, ces, buscarLista);
						if (postpos >= 0) {
							rcNT.setCuentaMatEnvioPost(rcNT.getCuentaMatEnvioPost()
									+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							if (ces[1].toString().equals("A1") || ces[1].toString().equals("A2")
									|| ces[1].toString().equals("A3")) {
								rcN2.setCuentaMatEnvioPost(rcN2.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());
							}
							if (ces[1].toString().equals("A4")) {
								rcINI_ART.setCuentaMatEnvioPost(rcINI_ART.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());
							} else if (ces[1].toString().equals("A5")) {
								rcN3.setCuentaMatEnvioPost(rcN3.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							} else if (ces[1].toString().equals("B0")) {
								rcN4.setCuentaMatEnvioPost(rcN4.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							} else if (ces[1].toString().equals("F0")) {
								rcN5.setCuentaMatEnvioPost(rcN5.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());
							}
							/*
							 * else if (ces[1].toString().equals("D0") || ces[1].toString().equals("C0") ||
							 * ces[1].toString().equals("G0")) {
							 * rcN6.setCuentaMatRecProf(rcN6.getCuentaMatRecProf() + ((Number)
							 * listaMatri.get(pos)[2]).intValue()); }
							 */
							else if (ces[1].toString().equals("D1")) {
								rcN6I.setCuentaMatEnvioPost(rcN6I.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());
							} else if (ces[1].toString().equals("D2")) {
								rcN6A.setCuentaMatEnvioPost(rcN6A.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							} else if (ces[1].toString().equals("E0") || ces[1].toString().equals("E1")
									|| ces[1].toString().equals("E2")) {
								rcN7.setCuentaMatEnvioPost(rcN7.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							} else if (ces[1].toString().equals("L0")) {
								rcN8.setCuentaMatEnvioPost(rcN8.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							} else if (ces[1].toString().equals("K0")) {
								rcN10.setCuentaMatEnvioPost(rcN10.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							} else if (ces[1].toString().equals("T0")) {
								rcN11.setCuentaMatEnvioPost(rcN11.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							} else if (ces[1].toString().equals("M0")) {
								rcN12.setCuentaMatEnvioPost(rcN12.getCuentaMatEnvioPost()
										+ ((Number) listaMatriPost.get(postpos)[2]).intValue());

							}
						}

					}
				}
				rcN1.setCuentaCentros(rcN2.getCuentaCentros() + rcN3.getCuentaCentros() + rcINI_ART.getCuentaCentros()
						+ rcN4.getCuentaCentros() + rcN5.getCuentaCentros());// BASICA REGULAR
				rcN9.setCuentaCentros(rcN10.getCuentaCentros() + rcN11.getCuentaCentros() + rcN12.getCuentaCentros());// SUPERIOR
																														// NO
																														// UNIVERSITARIA

				rcN1.setCuentaMatRecProf(rcN2.getCuentaMatRecProf() + rcN3.getCuentaMatRecProf()
						+ rcINI_ART.getCuentaMatRecProf() + rcN4.getCuentaMatRecProf() + rcN5.getCuentaMatRecProf());// BASICA
																														// REGULAR
				rcN9.setCuentaMatRecProf(
						rcN10.getCuentaMatRecProf() + rcN11.getCuentaMatRecProf() + rcN12.getCuentaMatRecProf());// SUPERIOR
																													// NO
																													// UNIVERSITARIA

				rcN1.setCuentaMatEnvioPost(
						rcN2.getCuentaMatEnvioPost() + rcN3.getCuentaMatEnvioPost() + rcINI_ART.getCuentaMatEnvioPost()
								+ rcN4.getCuentaMatEnvioPost() + rcN5.getCuentaMatEnvioPost());// BASICA REGULAR
				rcN9.setCuentaMatEnvioPost(
						rcN10.getCuentaMatEnvioPost() + rcN11.getCuentaMatEnvioPost() + rcN12.getCuentaMatEnvioPost());// SUPERIOR
																														// NO
																														// UNIVERSITARIA

				resumenMatri.add(rcNT);
				resumenMatri.add(rcN1);
				resumenMatri.add(rcN2);
				resumenMatri.add(rcN3);
				// resumenMatri.add(rcINI_ART);
				resumenMatri.add(rcN4);
				resumenMatri.add(rcN5);
				resumenMatri.add(rcN6I);
				resumenMatri.add(rcN6A);
				resumenMatri.add(rcN7);
				resumenMatri.add(rcN8);
				resumenMatri.add(rcN9);
				resumenMatri.add(rcN10);
				resumenMatri.add(rcN11);
				resumenMatri.add(rcN12);

				request.setAttribute("resumen_matri", resumenMatri);
				request.setAttribute("ActividadMatDet", actMatDet);
				request.setAttribute("EsregSigiedDet", esregSigied);
				request.setAttribute("EsregHuelgaDet", esregHuelga);

			} finally {
				logger.info("Dispatcher = /WEB-INF/jsp/ugel/reporte/resumenCoberturaXNivel.jsp");

				RequestDispatcher requestDispatcher = getServletContext()
						.getRequestDispatcher("/WEB-INF/jsp/ugel/reporte/resumenCoberturaXNivel.jsp");

				logger.info(":: CoberturaResumenServlet.processRequest :: Execution finish.");
				requestDispatcher.include(request, response);
			}
		} else {
			logger.info("Dispatcher = /recursos/error/errorsql.jsp");
			RequestDispatcher requestDispatcher = getServletContext()
					.getRequestDispatcher("/recursos/error/errorsql.jsp");

			logger.info(":: CoberturaResumenServlet.processRequest :: Execution finish.");
			requestDispatcher.include(request, response);
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	/*
	 * private PadronFacade lookupPadronFacade() { try { Context c = new
	 * InitialContext(); return (PadronFacade) c .lookup(
	 * "java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.ejb.common.PadronFacade"
	 * );
	 * 
	 * } catch (NamingException ne) { logger.info("ERROR lookupPadronFacade = " +
	 * ne.getMessage()); throw new RuntimeException(ne); } }
	 */

	private EnvioDocumentosFacade lookupEnvioDocumentosFacade() {
		try {
			Context c = new InitialContext();
			return (EnvioDocumentosFacade) c.lookup(
					"java:global/eol-war/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade");

		} catch (NamingException ne) {
			logger.info("ERROR lookupEnvioDocumentosFacade = " + ne.getMessage());
			throw new RuntimeException(ne);
		}

	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private Comparator<Object[]> buscarLista = new Comparator<Object[]>() {

		public int compare(Object[] o1, Object[] o2) {

			return ((String) o1[1]).compareTo((String) o2[1]);
		}
	};

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

}
