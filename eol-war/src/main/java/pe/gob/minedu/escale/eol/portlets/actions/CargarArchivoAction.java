///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package pe.gob.minedu.escale.eol.portlets.actions;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.zip.ZipEntry;
//import java.util.zip.ZipInputStream;
//
//import javax.ejb.EJB;
//import javax.naming.InitialContext;
//import javax.naming.NamingException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.struts.action.Action;
//import org.apache.struts.action.ActionForm;
//import org.apache.struts.action.ActionForward;
//import org.apache.struts.action.ActionMapping;
//import org.apache.struts.action.DynaActionForm;
//import org.apache.struts.upload.FormFile;
//import org.apache.struts.util.MessageResources;
//
//import pe.gob.minedu.escale.eol.dbf.DbfFile;
//import pe.gob.minedu.escale.eol.dbf.DbfFile.Field;
//import pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal;
//import pe.gob.minedu.escale.eol.padron.importar.dao.domain.CampoComparado;
//import pe.gob.minedu.escale.eol.padron.importar.dao.domain.TableField;
//
///**
// *
// * @author DSILVA
// */
//public class CargarArchivoAction extends Action {
//
//	private static final Logger LOGGER = Logger.getLogger(CargarArchivoAction.class.getName());
//	
//	@EJB//(mappedName = "java:global/eol-war/ActualizarPadronFacade!pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal")
//	private ActualizarPadronLocal actualizarPadronLocal = lookup("java:global/eol-war/ActualizarPadronFacade!pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal");
//    
//    /* forward name="success" path="" */
//    private final static String SUCCESS = "success";
//
//    /**
//     * This is the action called from the Struts framework.
//     * @param mapping The ActionMapping used to select this instance.
//     * @param form The optional ActionForm bean for this request.
//     * @param request The HTTP Request we are processing.
//     * @param response The HTTP Response we are processing.
//     * @throws java.lang.Exception
//     * @return
//     */
//    @Override
//    public ActionForward execute(ActionMapping mapping, ActionForm actionForm,
//            HttpServletRequest request, HttpServletResponse response)
//            throws Exception {
//
//        DynaActionForm form = (DynaActionForm) actionForm;
//        FormFile file = (FormFile) form.get("file");
//        if (file.getFileName().toUpperCase().endsWith(".DBF") || file.getFileName().toUpperCase().endsWith(".ZIP")) {
//            cargarData(request, file);
//
//            LOGGER.log(Level.FINE, "contentType:{0}", file.getContentType());
//            return mapping.findForward(SUCCESS);
//        }
//
//        return mapping.findForward("failure");
//
//    }
//
//    private void cargarData(HttpServletRequest request, FormFile file) {
//        HttpSession session = request.getSession();
//        try {
//            String fileName = file.getFileName();
//            LOGGER.log(Level.FINE, "Guardando archivo:{0}", fileName);
//
//            File f = File.createTempFile("padron", fileName.substring(fileName.length() - 3));
//            java.io.FileOutputStream fos = new FileOutputStream(f);
//            fos.write(file.getFileData());
//            fos.close();
//            request.setAttribute("fileUploadedName", file.getFileName());
//            if (file.getFileName().toUpperCase().endsWith(".ZIP")) {
//                f = extraerArchivo(request, f);
//            }
//            if (f != null) {
//                LOGGER.fine("archivo guardado");
//                session.setAttribute("archivo_cargado", f);
//
//                DbfFile.Field[] camposDBF = actualizarPadronLocal.obtenerCampos(f);
//                List<TableField> camposDB = actualizarPadronLocal.obtenerCamposTabla("padron");
//
//                List<CampoComparado> lista = compararCampos(request, camposDBF,
//                        camposDB);
//                request.setAttribute("camposDBF", camposDBF);
//                request.setAttribute("camposDB", camposDB);
//                request.setAttribute("camposComparados", lista);
//            }
//        } catch (IOException e) {
//            LOGGER.log(Level.SEVERE, null, e);
//        }
//
//    }
//
//    private List<CampoComparado> compararCampos(HttpServletRequest request,
//            Field[] camposDBF, List<TableField> camposDB) {
//        MessageResources msgs = getResources(request);
//        String omitir = msgs.getMessage("padron.omitir");
//        String omitirDBF=msgs.getMessage("padron.omitir.dbf");
//        String[] camposOmitir = omitir.split(",");
//        String[] camposOmitirDBF=omitirDBF.split(",");        
//        Arrays.sort(camposOmitir);
//        Arrays.sort(camposOmitirDBF);
//
//        List<CampoComparado> ret = new ArrayList<CampoComparado>();
//
//        Comparator<Field> fieldComparator = new Comparator<Field>() {
//
//            @Override
//            public int compare(Field o1, Field o2) {
//                return o1.getName().toUpperCase().compareTo(
//                        o2.getName().toUpperCase());
//            }
//        };
//        Comparator<TableField> tableFieldComparator = new Comparator<TableField>() {
//
//            @Override
//            public int compare(TableField o1, TableField o2) {
//                return o1.getName().toUpperCase().compareTo(
//                        o2.getName().toUpperCase());
//            }
//        };
//
//        Arrays.sort(camposDBF, fieldComparator);
//        for (TableField campoDB : camposDB) {
//            if (Arrays.binarySearch(camposOmitir, campoDB.getName()) < 0) {
//                CampoComparado campoComparado = new CampoComparado();
//                campoComparado.setDb(campoDB.getName());
//
//                Field key = new Field();
//                key.setName(campoDB.getName().toUpperCase());
//                int pos = Arrays.binarySearch(camposDBF, key, fieldComparator);
//                campoComparado.setDbf(pos >= 0 ? camposDBF[pos].getName()
//                        : "FALTA");
//                ret.add(campoComparado);
//            }
//        }
//        Collections.sort(camposDB, tableFieldComparator);
//        for (Field campoDBF : camposDBF) {            
//            if(Arrays.binarySearch(camposOmitirDBF,campoDBF.getName().toLowerCase())<0)
//            {   TableField key = new TableField();
//                key.setName(campoDBF.getName().toUpperCase());
//                int pos = Collections.binarySearch(camposDB, key,
//                        tableFieldComparator);
//                if (pos < 0) {
//                    CampoComparado campoComparado = new CampoComparado(campoDBF.getName(), "FALTA");
//                    ret.add(campoComparado);
//                }
//            }
//        }
//
//        return ret;
//    }
//
//    private static File extraerArchivo(HttpServletRequest request, File f) {
//        final int BUFFER = 2048;
//        try {
//            FileInputStream fis = new FileInputStream(f);
//            ZipInputStream zin = new ZipInputStream(
//                    new BufferedInputStream(fis));
//            File fExtracted = File.createTempFile("padron", "dbf");
//            for (ZipEntry entry = zin.getNextEntry(); entry != null; entry = zin.getNextEntry()) {
//                request.setAttribute("fileUploadedName", entry.getName());
//
//                FileOutputStream fos = new FileOutputStream(fExtracted);
//                BufferedOutputStream dest = new BufferedOutputStream(fos,
//                        BUFFER);
//                int count;
//                byte[] data = new byte[BUFFER];
//                while ((count = zin.read(data, 0, BUFFER)) != -1) {
//                    dest.write(data, 0, count);
//                }
//                dest.flush();
//                dest.close();
//                zin.close();
//                return fExtracted;
//            }
//        } catch (FileNotFoundException e) {
//             LOGGER.log(Level.SEVERE, null, e);
//        } catch (IOException e) {
//           LOGGER.log(Level.SEVERE, null, e);
//        } finally {
//            f.delete();
//        }
//        return null;
//    }
//    
//	@SuppressWarnings("unchecked")
//	private <T> T lookup(String nameContextEjb) {
//		LOGGER.info("search EJB for consume: " + nameContextEjb);
//		try {
//
//			javax.naming.Context c = new InitialContext();
//			return (T) c.lookup(nameContextEjb);
//		} catch (NamingException ne) {
////			LOGGER.error(ne);
//			throw new RuntimeException(ne);
//		}
//	}
//    
//}
