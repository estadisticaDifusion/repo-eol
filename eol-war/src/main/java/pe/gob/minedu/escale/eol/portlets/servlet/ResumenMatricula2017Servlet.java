/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.rest.client.InstitucionEducativaClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativa;
import pe.gob.minedu.escale.rest.client.domain.InstitucionesEducativas;

/**
 *
 * @author Administrador
 */
public class ResumenMatricula2017Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -769286821011386813L;

	private static Logger logger = Logger.getLogger(ResumenMatricula2017Servlet.class);

	@EJB
	private EnvioDocumentosFacade matriSrv;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AuthUsuario usr = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);
		String urlDescarga = "/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/ResumenMatricula.xls";

		InputStream is = getClass().getResourceAsStream(urlDescarga);
		HSSFWorkbook hssfw = new HSSFWorkbook(is);

		List<Object[]> lista1A = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "1a");
		logger.info("OBTENER LISTA1A");
		List<Object[]> lista2A = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "2a");
		logger.info("OBTENER LISTA2A");
		List<Object[]> lista3AP = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "3ap");
		List<Object[]> lista3AS = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "3as");
		logger.info("OBTENER LISTA3A");
		List<Object[]> lista4AI = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "4ai");
		logger.info("OBTENER LISTA4AI");
		List<Object[]> lista4AA = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "4aa");
		logger.info("OBTENER LISTA4AA");
		List<Object[]> lista5A = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "5a");
		logger.info("OBTENER LISTA5A");
		List<Object[]> lista6A = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "6a");
		logger.info("OBTENER LISTA6A");
		List<Object[]> lista7A = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "7a");
		logger.info("OBTENER LISTA7A");
		List<Object[]> lista8AI = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "8ai");
		List<Object[]> lista8AP = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "8ap");
		logger.info("OBTENER LISTA8A");
		List<Object[]> lista9A = matriSrv.getResumeMatricula2017ByUgel(usr.getUsuario(), "9a");
		logger.info("OBTENER LISTA9A");

		InstitucionEducativaClient client = new InstitucionEducativaClient();
		InstitucionesEducativas ies1A = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "A1", "A2", "A3" });
		logger.info("OBTENER INSTITUCIONES LISTA1A");
		InstitucionesEducativas ies2A = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "A5" });
		logger.info("OBTENER INSTITUCIONES LISTA2A");
		InstitucionesEducativas ies3AP = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "B0" });
		InstitucionesEducativas ies3AS = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "F0" });
		logger.info("OBTENER INSTITUCIONES LISTA3A");
		InstitucionesEducativas ies4AI = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "D1" });
		logger.info("OBTENER INSTITUCIONES LISTA4AI");
		InstitucionesEducativas ies4AA = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "D2" });
		logger.info("OBTENER INSTITUCIONES LISTA4AA");
		InstitucionesEducativas ies5A = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "K0" });
		logger.info("OBTENER INSTITUCIONES LISTA5A");
		InstitucionesEducativas ies6A = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "T0" });
		logger.info("OBTENER INSTITUCIONES LISTA6A");
		InstitucionesEducativas ies7A = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "M0" });
		logger.info("OBTENER INSTITUCIONES LISTA7A");
		InstitucionesEducativas ies8AI = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "E0", "E1" });
		InstitucionesEducativas ies8AP = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "E2" });
		logger.info("OBTENER INSTITUCIONES LISTA8A");
		InstitucionesEducativas ies9A = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "L0" });
		logger.info("OBTENER INSTITUCIONES LISTA9A");
		InstitucionesEducativas iesProgArti = client.getInstitucionesProgArti(usr.getUsuario(), "1");
		InstitucionesEducativas iesProgIse = client.getInstitucionesProgIse(usr.getUsuario(), "1");

		if (iesProgArti.getItems() != null && !iesProgArti.getItems().isEmpty()) {
			ies1A.getItems().addAll(iesProgArti.getItems());
		}

		if (iesProgIse.getItems() != null && !iesProgIse.getItems().isEmpty()) {
			if (ies6A.getItems() != null)
				ies6A.getItems().addAll(iesProgIse.getItems());
			else
				ies6A.setItems(iesProgIse.getItems());
		}

		llenarResumen(hssfw, ies1A.getItems(), lista1A, 38, "1A", 6);
		llenarResumen(hssfw, ies2A.getItems(), lista2A, 37, "2A", 6);
		llenarResumen(hssfw, ies3AP.getItems(), lista3AP, 35, "3AP", 6);
		llenarResumen(hssfw, ies3AS.getItems(), lista3AS, 32, "3AS", 6);
		llenarResumen(hssfw, ies4AI.getItems(), lista4AI, 60, "4AI", 6);
		llenarResumen(hssfw, ies4AA.getItems(), lista4AA, 63, "4AA", 6);
		llenarResumen(hssfw, ies5A.getItems(), lista5A, 45, "5A", 6);
		llenarResumen(hssfw, ies6A.getItems(), lista6A, 51, "6A", 6);
		llenarResumen(hssfw, ies7A.getItems(), lista7A, 55, "7A", 6);
		llenarResumen(hssfw, ies8AI.getItems(), lista8AI, 48, "8AI", 6);
		llenarResumen(hssfw, ies8AP.getItems(), lista8AP, 42, "8AP", 6);
		llenarResumen(hssfw, ies9A.getItems(), lista9A, 21, "9A", 6);

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=\"ResumenMatricula.xls\"");
		OutputStream os = response.getOutputStream();
		hssfw.write(os);
		os.close();
	}

	public void llenarResumen(HSSFWorkbook hssfw, List<InstitucionEducativa> ies, List<Object[]> datos, int cantidad,
			String formato, int iniFila) {
		HSSFSheet sheet1A = hssfw.getSheet(formato);
		if (ies == null)
			return;
		if (datos == null)
			return;

		InstitucionEducativa ieBq = new InstitucionEducativa();
		InstitucionEducativa ie;
		String column = "";
		int filaExcel = 0;
		for (int row = 0; row < datos.size(); row++) {
			ieBq.setCodigoModular((String) datos.get(row)[0]);
			ieBq.setAnexo((String) datos.get(row)[1]);
			int pos = Collections.binarySearch(ies, ieBq, buscarIE);

			if (pos >= 0) {
				ie = ies.get(pos);
				for (short x = 0; x < cantidad; x++) {
					if (x % 26 != 0) {
						String pre = "";
						switch (x / 26) {
						case 1:
							pre = "A";
							break;
						case 2:
							pre = "B";
							break;
						case 3:
							pre = "C";
							break;
						case 4:
							pre = "D";
							break;
						case 5:
							pre = "E";
							break;
						default:
							pre = "";
							break;
						}
						column = pre.concat(String.valueOf((char) ('A' + x % 26)));
					} else {
						String pre = "";
						switch (x / 26) {
						case 1:
							pre = "A";
							break;
						case 2:
							pre = "B";
							break;
						case 3:
							pre = "C";
							break;
						case 4:
							pre = "D";
							break;
						case 5:
							pre = "E";
							break;
						default:
							pre = "";
							break;
						}
						column = pre.concat("A");
					}

					if (x < 13) {
						switch (x) {
						case 0:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCodDreUgel());
							break;
						case 1:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getUbigeo());
							break;
						case 2:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getProvincia());
							break;
						case 3:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDistrito());
							break;
						case 4:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getNombreIE());
							break;
						case 5:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCodigoModular());
							break;
						case 6:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getAnexo());
							break;
						case 7:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCodigoLocal());
							break;
						case 8:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getNivelModalidad());
							break;
						case 9:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDescDependencia());
							break;
						case 10:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDescEstado());
							break;
						case 11:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDireccion());
							break;
						case 12:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCentroPoblado());
							break;
						}
					} else {
						int pos_val = 0;
						if (formato.equals("2A"))
							pos_val = (x - 13) + 3;
						else
							pos_val = (x - 13) + 4;

						// int num_row = 6 + filaExcel;
						if (datos.get(row)[pos_val] != null) {
							if (datos.get(row)[pos_val].getClass().equals(BigDecimal.class)) {
								setCell(sheet1A, column, iniFila + filaExcel,
										((BigDecimal) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(Integer.class)) {
								setCell(sheet1A, column, iniFila + filaExcel,
										((Integer) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(String.class)) {
								setCell(sheet1A, column, iniFila + filaExcel, ((String) datos.get(row)[pos_val]));
							}
						}
					}

				}

				filaExcel++;

			}

		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(valor == null ? 0 : valor.intValue());
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellValue(valor.intValue());
				}

			} catch (java.lang.NullPointerException e) {
				// row.setCell(cell);
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
				cell.setCellValue(valor.intValue());
			} catch (java.lang.NullPointerException e) {
				// row.setCell(cell);
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(texto);
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
					cell.setCellValue(new HSSFRichTextString(texto));
				}
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(new HSSFRichTextString(texto));
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	private Comparator<InstitucionEducativa> buscarIE = new Comparator<InstitucionEducativa>() {

		public int compare(InstitucionEducativa o1, InstitucionEducativa o2) {
			if (o1.getCodigoModular().compareTo(o2.getCodigoModular()) == 0) {
				return o1.getAnexo().compareTo(o2.getAnexo());
			} else {
				return o1.getCodigoModular().compareTo(o2.getCodigoModular());
			}

		}

	};
}
