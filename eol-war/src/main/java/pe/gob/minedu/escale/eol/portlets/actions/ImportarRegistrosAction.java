///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package pe.gob.minedu.escale.eol.portlets.actions;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import javax.ejb.EJB;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.log4j.Logger;
//import org.apache.struts.action.ActionForm;
//import org.apache.struts.action.ActionForward;
//import org.apache.struts.action.ActionMapping;
//
//import pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal;
//import pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronSigLocal;
//import pe.gob.minedu.escale.eol.padron.importar.dao.domain.ImportarInfo;
//
///**
// *
// * @author DSILVA
// */
//public class ImportarRegistrosAction extends org.apache.struts.action.Action {
//
//    static final Logger logger = Logger.getLogger(ImportarRegistrosAction.class);
//    /* forward name="success" path="" */
//    private final static String SUCCESS = "success";
//    
//	@EJB(mappedName = "java:global/eol-war/ActualizarPadronFacade!pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronLocal")
//	private ActualizarPadronLocal actualizarPadronLocal;
//	
//	@EJB(mappedName = "java:global/eol-war/ActualizarPadronSigFacade!pe.gob.minedu.escale.eol.padron.ejb.ActualizarPadronSigLocal")
//    private ActualizarPadronSigLocal actualizarPadronSigLocal;
//
//    /**
//     * This is the action called from the Struts framework.
//     * @param mapping The ActionMapping used to select this instance.
//     * @param form The optional ActionForm bean for this request.
//     * @param request The HTTP Request we are processing.
//     * @param response The HTTP Response we are processing.
//     * @throws java.lang.Exception
//     * @return
//     */
//    @Override
//    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        HttpSession session = request.getSession();
//        File archivoCargado = (File) session.getAttribute("archivo_cargado");
//        final ImportarInfo info = new ImportarInfo();
//        session.setAttribute("importarInfo", info);
//        logger.debug("iniciando importar");
//
//        List<Map<String,String>> filas=new ArrayList<Map<String,String>>();
//        this.actualizarPadronLocal.actualizar(archivoCargado,filas);
//        if(!filas.isEmpty())
//        	this.actualizarPadronSigLocal.actualizar(filas);
//        
//        logger.debug("fin de importar");
//        return mapping.findForward(SUCCESS);
//    }
//
//}
