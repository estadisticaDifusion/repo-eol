/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.domain.act;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.eol.portlets.util.enumerations.RoleEnum;
import pe.gob.minedu.escale.rest.client.domain.Dre;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.rest.client.domain.Ugel;

/**
 *
 * @author JMATAMOROS
 */
public class Tablero implements Serializable {

    
    private String codMod;
    private String anexo;
    private String nivel;
    private String codLocal;
    
    private InstitucionEducativaIE ce;
    private Ugel ugel;
    private Dre dre;
    private RoleEnum rol;
    private Map<String,List<EolActividadConverter>> mapActividades;

  
  
    public String getCodMod() {
        return codMod;
    }

    /**
     * @param codMod the codMod to set
     */
    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    /**
     * @return the anexo
     */
    public String getAnexo() {
        return anexo;
    }

    /**
     * @param anexo the anexo to set
     */
    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    /**
     * @return the nivel
     */
    public String getNivel() {
        return nivel;
    }

    
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

   

    public String getCodLocal() {
        return codLocal;
    }

    
    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    
    public InstitucionEducativaIE getCe() {
        return ce;
    }

    
    public void setCe(InstitucionEducativaIE ce) {
        this.ce = ce;
    }

    
    public Ugel getUgel() {
        return ugel;
    }

    /**
     * @param ugel the ugel to set
     */
    public void setUgel(Ugel ugel) {
        this.ugel = ugel;
    }

    /**
     * @return the rol
     */
    public RoleEnum getRol() {
        return rol;
    }


    public void setRol(RoleEnum rol) {
        this.rol = rol;
    }

    
    public Dre getDre() {
        return dre;
    }


    public void setDre(Dre dre) {
        this.dre = dre;
    }

    public Map<String, List<EolActividadConverter>> getMapActividades() {
        return mapActividades;
    }

    public void setMapActividades(Map<String, List<EolActividadConverter>> mapActividades) {
        this.mapActividades = mapActividades;
    }
}
