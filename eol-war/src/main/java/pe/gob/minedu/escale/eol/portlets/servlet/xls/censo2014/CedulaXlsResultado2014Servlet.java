/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2014;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Resultado2014Cabecera;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Resultado2014Detalle;
import pe.gob.minedu.escale.eol.estadistica.domain.censo2014.Resultado2014Fila;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.estadistica.ejb.censo2014.Resultado2014Facade;
import pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml.Campo;
import pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml.Campos;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.eol.portlets.util.enumerations.NivelModalidadEnum;
import pe.gob.minedu.escale.rest.client.PadronClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativaIE;

/**
 *
 * @author JMATAMOROS
 */
public class CedulaXlsResultado2014Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final Logger LOGGER = Logger.getLogger(CedulaXlsResultado2014Servlet.class.getName());
	@EJB
	private Resultado2014Facade resultado2014Facade;

	@EJB
	private EnvioDocumentosFacade envioFacade;
	
	private MessageResources res = MessageResources.getMessageResources("eol");
	private static String rutaXML = "/pe/gob/minedu/escale/eol/portlets/servlet/xls/censo2014/resource/campos_resultado_%1$s.xml";
	// private MessageResources censoEtiqueta =
	// MessageResources.getMessageResources("eol");
	ResourceBundle censoEtiqueta = ResourceBundle
			.getBundle("/pe/gob/minedu/escale/eol/portlets/constancia/censo2014/resultado/etiquetas");
	public static Map<String, Campos> mapCampos = new HashMap<String, Campos>();

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nivMod = request.getParameter("nivel");
		String codMod = request.getParameter("codmod");
		String anexo = request.getParameter("anexo");
		String idEnvio = request.getParameter("idEnvio");

		String nroCed = Funciones.cedulaNivModResultado(nivMod);

		PadronClient pCli = new PadronClient(codMod, anexo);
		InstitucionEducativaIE ie = pCli.get_JSON(InstitucionEducativaIE.class);

		InputStream is = getClass().getResourceAsStream(
				"/pe/gob/minedu/escale/eol/portlets/plantilla/censo2014/resultado/CensoResultadoCed"
						.concat(nroCed.toLowerCase()).concat("_2014.xls"));
		HSSFWorkbook hssfw = new HSSFWorkbook(is);
		llenarCabecera(hssfw, ie, nivMod);

		if (idEnvio != null && !idEnvio.isEmpty()) {
			Resultado2014Cabecera resultado2014 = resultado2014Facade.find(new Long(idEnvio));
			/*
			 * OM llenarCampos(hssfw, resultado2014.getDetalle(), nivMod);
			 * 
			 */
			llenarCabeceraEnvio(hssfw, resultado2014, nivMod);
		}

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition",
				"attachment;filename=\"CensoResultadoCed".concat(nroCed).concat("_2014.xls\""));
		OutputStream os = response.getOutputStream();
		hssfw.write(os);
		os.close();
	}

	public void llenarCampos(HSSFWorkbook hssfw, Map<String, Resultado2014Detalle> map, String nivMod) {
		String nroCed = Funciones.cedulaNivModResultado(nivMod);
		HSSFSheet sheetCuadros = hssfw.getSheet("Cedula".concat(nroCed.toUpperCase()));

		if (!mapCampos.containsKey(nroCed)) {
			mapCampos.put(nroCed, new Campos(CedulaXlsResultado2014Servlet.class
					.getResource(String.format(rutaXML, nroCed.toUpperCase())).getFile()));
		}

		Class<Resultado2014Fila> clazz = Resultado2014Fila.class;

		Campo campoTmp = new Campo();

		Object valorH = null;
		Object valorM = null;

		if (mapCampos.get(nroCed) != null && mapCampos.get(nroCed).getCampos() != null) {
			for (Campo cuadro : mapCampos.get(nroCed).getCampos()) {
				Resultado2014Detalle detalle = map.get(cuadro.getEtiqueta());
				List listCampos = Arrays.asList(cuadro.getDatos());
				if (detalle != null) {
					/*
					 * OM for (Resultado2014Fila fila : detalle.getFilas()) {
					 * campoTmp.setPropiedad(fila.getTipdato()); int pos =
					 * Collections.binarySearch(listCampos, campoTmp, buscarCampo);
					 * 
					 * if (pos >= 0) { Campo campoSel = (Campo) listCampos.get(pos); int inicioC =
					 * new Integer(campoSel.getOldValue()); String[] strCol =
					 * campoSel.getCelC().split(":");
					 * 
					 * for (String parCol : strCol) { Method m; String[] strPar = parCol.split("-");
					 * try { String datoFilaH = "getDato".concat(Funciones.getNumFormato(inicioC,
					 * "00")) .concat("h"); String datoFilaM =
					 * "getDato".concat(Funciones.getNumFormato(inicioC, "00")) .concat("m"); m =
					 * clazz.getMethod(datoFilaH); valorH = m.invoke(fila); m =
					 * clazz.getMethod(datoFilaM); valorM = m.invoke(fila); if (valorH != null) {
					 * setCell(sheetCuadros, strPar[0], campoSel.getCelF(),
					 * Integer.parseInt(valorH.toString())); } if (valorM != null) {
					 * setCell(sheetCuadros, strPar[1], campoSel.getCelF(),
					 * Integer.parseInt(valorM.toString())); }
					 * 
					 * } catch (Exception ex) {
					 * Logger.getLogger(CedulaXlsResultado2014Servlet.class.getName()).log(Level.
					 * SEVERE, null, ex); } inicioC++; } } }
					 */

				}
			}
		}

	}

	public void llenarCabecera(HSSFWorkbook hssfw, InstitucionEducativaIE ie, String nivMod) {
		String nroCed = Funciones.cedulaNivModResultado(nivMod);
		HSSFSheet sheetCabecera = hssfw.getSheet("Cedula".concat(nroCed.toUpperCase()));

		if (nroCed.toUpperCase().equals("1B")) {
			setCell(sheetCabecera, "AA", 23, ie.getCodigoModular());
			setCell(sheetCabecera, "AA", 27, ie.getCentroEducativo());
			setCell(sheetCabecera, "AS", 23, ie.getCodlocal());
			setCell(sheetCabecera, nivMod.equals("A1") ? "S"
					: (nivMod.equals("A2") ? "Z" : (nivMod.equals("A3") ? "AD" : (nivMod.equals("A4") ? "AN" : ""))),
					32, "X");
		} else if (nroCed.toUpperCase().equals("2B")) {
			setCell(sheetCabecera, "AA", 23, ie.getCodigoModular());
			setCell(sheetCabecera, "AA", 25, ie.getCentroEducativo());

			if (ie.getTipoprog() != null) {
				switch (ie.getTipoprog().charAt(0)) {
				case '1':
					setCell(sheetCabecera, "AH", 33, "X");
					break;
				case '2':
					setCell(sheetCabecera, "U", 39, "X");
					break;
				case '3':
					setCell(sheetCabecera, "U", 37, "X");
					break;
				case '4':
					setCell(sheetCabecera, "U", 43, "X");
					break;
				case '5':
					setCell(sheetCabecera, "U", 33, "X");
					break;
				case '6':
					setCell(sheetCabecera, "AH", 39, "X");
					break;
				case '7':
					setCell(sheetCabecera, "AH", 36, "X");
					break;
				case '8':
					setCell(sheetCabecera, "AH", 41, "X");
					break;

				}
			}

		} else if (nroCed.toUpperCase().equals("3B")) {
			setCell(sheetCabecera, "AA", 24, ie.getCodigoModular());
			setCell(sheetCabecera, "AF", 24, ie.getAnexo());
			setCell(sheetCabecera, "AM", 24, ie.getCodlocal());
			setCell(sheetCabecera, "AA", 26, ie.getCentroEducativo());
			setCell(sheetCabecera, "S", nivMod.equals(NivelModalidadEnum.PRIMARIA_DE_MENORES.getCod()) ? 33
					: (nivMod.equals(NivelModalidadEnum.SECUNDARIA_DE_MENORES.getCod()) ? 35 : 0), "X");

			// llenarDatosReportados(sheetCabecera,ie.getCodMod(),ie.getAnexo(),ie.getNivMod());
		} else if (nroCed.toUpperCase().equals("4B")) {
			setCell(sheetCabecera, "AA", 24, ie.getCodigoModular());
			setCell(sheetCabecera, "AL", 24, ie.getCodlocal());
			setCell(sheetCabecera, "AA", 26, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("5B")) {
			setCell(sheetCabecera, "AA", 23, ie.getCodigoModular());
			setCell(sheetCabecera, "AK", 23, ie.getCodlocal());
			setCell(sheetCabecera, "AA", 25, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("6B")) {
			setCell(sheetCabecera, "AB", 23, ie.getCodigoModular());
			setCell(sheetCabecera, "AL", 23, ie.getCodlocal());
			setCell(sheetCabecera, "AB", 25, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("7B")) {
			setCell(sheetCabecera, "AA", 23, ie.getCodigoModular());
			setCell(sheetCabecera, "AL", 23, ie.getCodlocal());
			setCell(sheetCabecera, "AA", 25, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("8B")) {
			setCell(sheetCabecera, "AA", 23, ie.getCodigoModular());
			setCell(sheetCabecera, "AN", 23, ie.getCodlocal());
			setCell(sheetCabecera, "AA", 25, ie.getCentroEducativo());
		} else if (nroCed.toUpperCase().equals("9B")) {
			setCell(sheetCabecera, "AA", 23, ie.getCodigoModular());
			setCell(sheetCabecera, "AI", 23, ie.getCodlocal());
			setCell(sheetCabecera, "AA", 25, ie.getCentroEducativo());
		}
	}

	public void llenarCabeceraEnvio(HSSFWorkbook hssfw, Resultado2014Cabecera resultado2014, String nivMod) {
		String nroCed = Funciones.cedulaNivModResultado(nivMod);
		HSSFSheet sheetCabecera = hssfw.getSheet("Cedula".concat(nroCed.toUpperCase()));

		if (nroCed.toUpperCase().equals("1B")) {
			setCell(sheetCabecera, "AM", 27, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 37, resultado2014.getApeDir());
			setCell(sheetCabecera, "AC", 37, resultado2014.getNomDir());
			setCell(sheetCabecera, "AA", 25, resultado2014.getTelefono());
			setCell(sheetCabecera, "AO", 37,
					censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));

		} else if (nroCed.toUpperCase().equals("2B")) {
			setCell(sheetCabecera, "AM", 25, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 51, resultado2014.getApeDir());
			setCell(sheetCabecera, "AC", 51, resultado2014.getNomDir());
			setCell(sheetCabecera, "AM", 23, resultado2014.getTelefono());
			setCell(sheetCabecera, "AO", 51,
					censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));

		} else if (nroCed.toUpperCase().equals("3B")) {
			setCell(sheetCabecera, "AM", 26, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 43, resultado2014.getApeDir());
			setCell(sheetCabecera, "AC", 43, resultado2014.getNomDir());
			setCell(sheetCabecera, "AV", 24, resultado2014.getTelefono());
			// if(resultado2014.getSituacion()!=null)
			setCell(sheetCabecera, "AO", 43,
					censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));
		} else if (nroCed.toUpperCase().equals("4B")) {
			setCell(sheetCabecera, "AM", 26, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 30, resultado2014.getApeDir());
			setCell(sheetCabecera, "AC", 30, resultado2014.getNomDir());
			setCell(sheetCabecera, "AU", 24, resultado2014.getTelefono());
			setCell(sheetCabecera, "AO", 30,
					censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));

		} else if (nroCed.toUpperCase().equals("5B")) {
			setCell(sheetCabecera, "AM", 25, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 37, resultado2014.getApeDir());
			setCell(sheetCabecera, "AC", 37, resultado2014.getNomDir());
			setCell(sheetCabecera, "AU", 23, resultado2014.getTelefono());
			if (resultado2014.getSituacion() != null
					&& censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())) != null) {
				setCell(sheetCabecera, "AO", 37,
						censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));
			}
		} else if (nroCed.toUpperCase().equals("6B")) {
			setCell(sheetCabecera, "AP", 25, resultado2014.getDistrito());
			setCell(sheetCabecera, "Z", 28, resultado2014.getApeDir());
			setCell(sheetCabecera, "AF", 28, resultado2014.getNomDir());
			setCell(sheetCabecera, "AS", 23, resultado2014.getTelefono());
			if (resultado2014.getSituacion() != null
					&& censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())) != null) {
				setCell(sheetCabecera, "AR", 28,
						censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));
			}
		} else if (nroCed.toUpperCase().equals("7B")) {
			setCell(sheetCabecera, "AM", 25, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 30, resultado2014.getApeDir());
			setCell(sheetCabecera, "AC", 30, resultado2014.getNomDir());
			setCell(sheetCabecera, "AU", 23, resultado2014.getTelefono());
			if (resultado2014.getSituacion() != null
					&& censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())) != null) {
				setCell(sheetCabecera, "AO", 30,
						censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));
			}
		} else if (nroCed.toUpperCase().equals("8B")) {
			setCell(sheetCabecera, "AR", 25, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 39, resultado2014.getApeDir());
			setCell(sheetCabecera, "AD", 39, resultado2014.getNomDir());
			setCell(sheetCabecera, "AZ", 23, resultado2014.getTelefono());
			if (resultado2014.getSituacion() != null
					&& censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())) != null) {
				setCell(sheetCabecera, "AT", 39,
						censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));
			}
		} else if (nroCed.toUpperCase().equals("9B")) {
			setCell(sheetCabecera, "AL", 25, resultado2014.getDistrito());
			setCell(sheetCabecera, "W", 30, resultado2014.getApeDir());
			setCell(sheetCabecera, "AG", 30, resultado2014.getNomDir());
			setCell(sheetCabecera, "AO", 23, resultado2014.getTelefono());
			if (resultado2014.getSituacion() != null
					&& censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())) != null) {
				setCell(sheetCabecera, "AP", 30,
						censoEtiqueta.getString("situacion.".concat(resultado2014.getSituacion())));
			}
		}

	}

	public void llenarDatosPrellenado1BC101(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 51, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 51, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 51, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 51, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 51, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 51, Integer.parseInt(fila[14].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 52, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 52, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 52, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 52, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 52, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 52, Integer.parseInt(fila[14].toString()));
				break;
			case 3:
				setCell(sheetCabecera, "AD", 53, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 53, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 53, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 53, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 53, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 53, Integer.parseInt(fila[14].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 54, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 54, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 54, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 54, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 54, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 54, Integer.parseInt(fila[14].toString()));
				break;

			}
		}

	}

	public void llenarDatosPrellenado1BC102(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 70, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 70, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 70, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 70, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 70, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 70, Integer.parseInt(fila[14].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 71, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 71, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 71, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 71, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 71, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 71, Integer.parseInt(fila[14].toString()));
				break;
			case 3:
				setCell(sheetCabecera, "AD", 72, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 72, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 72, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 72, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 72, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 72, Integer.parseInt(fila[14].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 73, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 73, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 73, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 73, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 73, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 73, Integer.parseInt(fila[14].toString()));
				break;

			case 7:
				setCell(sheetCabecera, "AD", 74, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 74, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 74, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 74, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 74, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 74, Integer.parseInt(fila[14].toString()));
				break;

			case 8:
				setCell(sheetCabecera, "AD", 76, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AF", 76, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AH", 76, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AJ", 76, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AL", 76, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AN", 76, Integer.parseInt(fila[14].toString()));
				break;

			}
		}

	}

	public void llenarDatosPrellenado3BC101(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 52, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 52, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 52, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 52, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 52, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 52, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 52, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 52, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 52, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 52, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 52, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 52, Integer.parseInt(fila[16].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 53, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 53, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 53, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 53, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 53, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 53, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 53, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 53, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 53, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 53, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 53, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 53, Integer.parseInt(fila[16].toString()));

				break;
			case 3:
				setCell(sheetCabecera, "AD", 54, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 54, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 54, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 54, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 54, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 54, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 54, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 54, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 54, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 54, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 54, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 54, Integer.parseInt(fila[16].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 55, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 55, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 55, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 55, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 55, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 55, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 55, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 55, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 55, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 55, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 55, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 55, Integer.parseInt(fila[16].toString()));
				break;

			case 5:
				setCell(sheetCabecera, "AD", 56, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 56, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 56, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 56, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 56, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 56, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 56, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 56, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 56, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 56, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 56, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 56, Integer.parseInt(fila[16].toString()));
				break;
			case 6:
				setCell(sheetCabecera, "AD", 57, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 57, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 57, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 57, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 57, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 57, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 57, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 57, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 57, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 57, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 57, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 57, Integer.parseInt(fila[16].toString()));
				break;
			}
		}

	}

	public void llenarDatosPrellenado3BC102(HSSFSheet sheetCabecera, List<Object[]> listSIAGIE) {
		for (Object[] fila : listSIAGIE) {
			String tipDat = fila[2].toString();
			int tipDatNum = Integer.parseInt(tipDat);
			switch (tipDatNum) {
			case 1:
				setCell(sheetCabecera, "AD", 71, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 71, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 71, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 71, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 71, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 71, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 71, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 71, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 71, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 71, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 71, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 71, Integer.parseInt(fila[16].toString()));
				break;

			case 2:
				setCell(sheetCabecera, "AD", 72, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 72, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 72, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 72, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 72, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 72, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 72, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 72, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 72, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 72, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 72, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 72, Integer.parseInt(fila[16].toString()));

				break;
			case 3:
				setCell(sheetCabecera, "AD", 73, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 73, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 73, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 73, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 73, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 73, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 73, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 73, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 73, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 73, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 73, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 73, Integer.parseInt(fila[16].toString()));
				break;

			case 4:
				setCell(sheetCabecera, "AD", 74, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 74, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 74, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 74, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 74, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 74, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 74, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 74, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 74, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 74, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 74, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 74, Integer.parseInt(fila[16].toString()));
				break;

			case 5:
				setCell(sheetCabecera, "AD", 75, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 75, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 75, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 75, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 75, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 75, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 75, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 75, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 75, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 75, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 75, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 75, Integer.parseInt(fila[16].toString()));
				break;
			case 6:
				setCell(sheetCabecera, "AD", 76, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 76, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 76, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 76, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 76, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 76, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 76, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 76, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 76, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 76, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 76, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 76, Integer.parseInt(fila[16].toString()));
				break;
			case 7:
				setCell(sheetCabecera, "AD", 78, Integer.parseInt(fila[5].toString()));
				setCell(sheetCabecera, "AF", 78, Integer.parseInt(fila[6].toString()));
				setCell(sheetCabecera, "AH", 78, Integer.parseInt(fila[7].toString()));
				setCell(sheetCabecera, "AJ", 78, Integer.parseInt(fila[8].toString()));
				setCell(sheetCabecera, "AL", 78, Integer.parseInt(fila[9].toString()));
				setCell(sheetCabecera, "AN", 78, Integer.parseInt(fila[10].toString()));
				setCell(sheetCabecera, "AP", 78, Integer.parseInt(fila[11].toString()));
				setCell(sheetCabecera, "AR", 78, Integer.parseInt(fila[12].toString()));
				setCell(sheetCabecera, "AT", 78, Integer.parseInt(fila[13].toString()));
				setCell(sheetCabecera, "AV", 78, Integer.parseInt(fila[14].toString()));
				setCell(sheetCabecera, "AX", 78, Integer.parseInt(fila[15].toString()));
				setCell(sheetCabecera, "AZ", 78, Integer.parseInt(fila[16].toString()));
				break;
			}
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				LOGGER.log(Level.WARNING,
						String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static void setCell(HSSFSheet sheet, int col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(col).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				LOGGER.log(Level.WARNING,
						String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	static Comparator<Campo> buscarCampo = new Comparator<Campo>() {
		public int compare(Campo o1, Campo o2) {
			return (o1.getPropiedad()).compareTo(o2.getPropiedad());
		}
	};
}
