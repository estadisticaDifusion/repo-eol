/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.converter.EolActividadConverter;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.rest.client.EolActividadClient;

/**
 *
 * @author JMATAMOROS
 */
public class SieServlet extends HttpServlet {

    static final String URI = "http://localhost:8080/EnvioCenso%sService/EnvioCenso%s";
    //static final String URI = "http://localhost:9013/EnvioCenso%sService/EnvioCenso%s";
    //static final String URI = "http://192.168.210.10:8080/EnvioCenso%sService/EnvioCenso%s";
    static final String CONTENT_TYPE_REQUEST = "text/xml";
    static final Logger logger = Logger.getLogger(SieServlet.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        try {

            HttpSession session = request.getSession();
            AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
            Integer id = -1;
            try {
                id = new Integer(request.getParameter("idActividad"));

            } catch (NumberFormatException ef) {
                logger.log(Level.WARNING, "Error al obtener el parametro id de la actividad", ef.getMessage());
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                writeActError(out);
                return;
            }

            EolActividadClient actClient = new EolActividadClient(id);
            EolActividadConverter act = actClient.getActividad();

            if (act != null) {
                String[] nomAct = act.getNombre().split("-");
                String lCed = nomAct[1].substring(0, 1).toUpperCase().concat(nomAct[1].substring(1, nomAct[1].length()).toLowerCase());
                URL url = new URL(String.format(URI, lCed, lCed));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("content-type", CONTENT_TYPE_REQUEST);
                connection.setRequestProperty("SOAPAction", URI);
                OutputStream os = connection.getOutputStream();
                Document docXML = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
                docXML.setXmlStandalone(true);
                Element envelElem = docXML.createElement("S:Envelope");
                envelElem.setAttribute("xmlns:S", "http://schemas.xmlsoap.org/soap/envelope/");
                Element headerElem = docXML.createElement("S:Header");
                Element bodyElem = docXML.createElement("S:Body");

                System.out.println("LCED:" + lCed);
                Element methodElem = docXML.createElement(String.format("ns2:dataCedula%s", lCed));

                methodElem.setAttribute("xmlns:ns2", String.format("http://censo%s.ws.sie.eol.escale.minedu.gob.pe/", act.getNombrePeriodo()));

                Element paramElem = docXML.createElement("codUgel");
                paramElem.setTextContent(usuario.getUsuario());
                methodElem.appendChild(paramElem);
                bodyElem.appendChild(methodElem);
                envelElem.appendChild(headerElem);
                envelElem.appendChild(bodyElem);
                docXML.appendChild(envelElem);
                Source source = new DOMSource(docXML);
                Result result = new StreamResult(os);
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.transform(source, result);
                os.flush();
                os.close();
                response.setContentType("application/zip");
                response.setHeader("Content-Disposition", String.format("attachment;filename=\"DATA_%s_%s.zip\"", act.getNombre(), act.getNombrePeriodo()));

                InputStream is = connection.getInputStream();
                ZipOutputStream zos = new ZipOutputStream(response.getOutputStream());

                ZipEntry zipEntry = new ZipEntry("data.xml");
                zos.putNextEntry(zipEntry);
                byte[] buffer = new byte[2048];
                int cuenta = 0;
                while ((cuenta = is.read(buffer)) >= 0) {
                    zos.write(buffer, 0, cuenta);
                }
                is.close();
                zos.closeEntry();
                zos.close();
            } else {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                writeActError(out);
            }

        } catch (ParserConfigurationException ex) {
            log("error convirtiendo xml", ex);
        } catch (TransformerException ex) {

            log("error convirtiendo xml", ex);

        }
    }

    public void writeActError(PrintWriter out) {
        out.println("<body bgcolor=\"#ffffff\">");
        out.println("<h1>No se ha seleccionado una actividad correctamente ó la falta implementar la funcionalidad para esta actividad !!</h1>");
        out.println("</body>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
