package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2017;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.estadistica.ejb.PrecargaCedulaIDFacade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;

public class SCedulaIDProcess extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(SCedulaIDProcess.class);

	static final String CEDULA_MATRICULA_DESC = "IdentificacionIECedID_";
	static final String PERIODO = "2017";
	static final String PATH_DIR = "2017/cedulas";
	static final String PATH_EXCEL = "D:\\zexport\\";

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@EJB
	private PrecargaCedulaIDFacade padroncedulaIDFacade;

	@SuppressWarnings("unused")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: SCedulaIDProcess.processRequest :: Starting execution...");
		String preCed = "";
		String nivMod = request.getParameter("nivel");
		// String anexo = request.getParameter("anexo");
		// String codMod = request.getParameter("codmod");
		// String codied = request.getParameter("codinst");
		// codied = "IE046830";
		// LOGGER.log(Level.WARNING, "********** DESCARGA {0}***********", null);8
		logger.info("INCIANDO");

		// InputStream is =
		// getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2016/matricula/IdentificacionIECedID_2016.xls");

		List<Object[]> listaOMID = padroncedulaIDFacade.getListaOmisosID();
		if (listaOMID != null && listaOMID.size() > 0) {
			for (int i = 0; i < listaOMID.size(); i++) {
				Object[] obj = listaOMID.get(i);

				// if(i < 100){
				cargarArchivos(obj);
				// System.gc();
				// }

				// System.out.println("ITEM "+ (i+1)+ " : "+ obj[0].toString() +" -
				// "+obj[1].toString());
			}

		}

		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			out.println("Proceso Finalizado");
		} finally {
			out.close();
		}

		/*
		 * String preCed = ""; String nivMod = request.getParameter("nivel"); String
		 * anexo = request.getParameter("anexo"); String codMod =
		 * request.getParameter("codmod"); LOGGER.log(Level.WARNING,
		 * "********** DESCARGA  {0}***********", codMod); InputStream is =
		 * getClass().getResourceAsStream(
		 * "/pe/gob/minedu/escale/eol/portlets/plantilla/censo2016/matricula/IdentificacionIECedID_2016.xls"
		 * ); if (is != null) { HSSFWorkbook hssfw = new HSSFWorkbook(is);
		 * response.setContentType("application/octet-stream");
		 * 
		 * response.setHeader("Content-Disposition",
		 * String.format("attachment;filename=\"IdentificacionIECedID_%s.xls\"", new
		 * Object[] { "2016" })); OutputStream os = response.getOutputStream(); try {
		 * hssfw.write(os); } catch (IOException ioe) { ioe.printStackTrace(); }
		 * os.close(); }
		 */
		logger.info(":: SCedulaIDProcess.processRequest :: Execution finish.");
	}

	private void cargarArchivos(Object[] obj) {

		try {
			InputStream is = getClass().getResourceAsStream(
					"/pe/gob/minedu/escale/eol/portlets/plantilla/censo2016/matricula/IdentificacionIECedID_2016.xls");

			CentroEducativo ie = padronFacade.obtainByCodMod(obj[1].toString(), obj[2].toString());

			if (is != null) {
				HSSFWorkbook hssfw = new HSSFWorkbook(is);

				try {

					llenadoCedulaID(hssfw, ie, obj[0].toString());

					// creando carpeta
					String finalPath = PATH_EXCEL + obj[3].toString() + "_" + obj[4].toString().replace(' ', '_');
					File nuevoDirectorio = new File(finalPath);
					if (!nuevoDirectorio.exists()) {
						nuevoDirectorio.mkdir();
					}
					// creando carpeta

					// FileOutputStream fileOut = new
					// FileOutputStream("D:\\zexport\\test"+obj[1].toString()+".xls");
					FileOutputStream fileOut = new FileOutputStream(
							finalPath + "\\" + obj[1].toString() + "_CedulaID_2016.xls");

					hssfw.write(fileOut);
					fileOut.flush();
					fileOut.close();
					is.close();

				} catch (Exception e) {
					System.out.println("ERROR INTERNO");
					e.printStackTrace();
				} finally {

				}

			}

		} catch (Exception e) {
			System.out.println("ERROR EXTERNO");
			e.printStackTrace();
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}

	private void llenadoCedulaID(HSSFWorkbook hssfw, CentroEducativo ie, String codied) {
		Object[] cabeceraID = padroncedulaIDFacade.getPrellenadoCabeceraCedulaID2017(codied);

		HSSFSheet sheetC100 = hssfw.getSheet("C100");
		setCell(sheetC100, "H", 7, codied);
		setCell(sheetC100, "A", 8, ie.getCodMod());

		if (cabeceraID != null) {
			List<Object[]> listaDetID = padroncedulaIDFacade.getPrellenadoDetalleCedulaID2017(codied);

			/****** seccion C100 *******/

			// setCell(sheetC100, "D", 29, ie.getCenEdu());//Nombre
			setCell(sheetC100, "D", 16, cabeceraID[2].toString());
			setCell(sheetC100,
					cabeceraID[3].toString().equals("1") ? "D" : (cabeceraID[3].toString().equals("2") ? "G" : "N"), 18,
					"X");
			setCell(sheetC100, "D", 20, cabeceraID[5] != null ? cabeceraID[5].toString() : "");
			setCell(sheetC100, "D", 22, cabeceraID[6] != null ? cabeceraID[6].toString() : "");
			setCell(sheetC100, "D", 24, cabeceraID[7] != null ? cabeceraID[7].toString() : "");
			setCell(sheetC100, "K", 24, cabeceraID[8] != null ? cabeceraID[8].toString() : "");
			setCell(sheetC100, "D", 30, cabeceraID[9] != null ? cabeceraID[9].toString() : "");
			setCell(sheetC100, "H", 30, cabeceraID[10] != null ? cabeceraID[10].toString() : "");
			setCell(sheetC100, "K", 30, cabeceraID[11] != null ? cabeceraID[11].toString() : "");
			setCell(sheetC100, "D", 33, cabeceraID[12] != null ? cabeceraID[12].toString() : "");
			setCell(sheetC100, "F", 41, cabeceraID[13] != null ? cabeceraID[13].toString() : "");
			setCell(sheetC100, "F", 43, cabeceraID[14] != null ? cabeceraID[14].toString() : "");
			setCell(sheetC100, "G", 45, cabeceraID[15] != null ? cabeceraID[15].toString() : "");

			if (listaDetID != null) {
				int cuadro = Integer.parseInt(String.valueOf(cabeceraID[4]));
				switch (cuadro) {
				case 1:
					HSSFSheet sheetC201 = hssfw.getSheet("C201");
					llenadoC201(sheetC201, listaDetID);
					setCell(sheetC100, "A", 1, "1");
					break;
				case 2:
					HSSFSheet sheetC202 = hssfw.getSheet("C202");
					llenadoC202(sheetC202, listaDetID);
					setCell(sheetC100, "A", 1, "2");
					break;
				default:
					HSSFSheet sheetC203 = hssfw.getSheet("C203");
					llenadoC203(sheetC203, listaDetID);
					setCell(sheetC100, "A", 1, "3");
					break;
				}
			}
		}
	}

	private void llenadoC201(HSSFSheet sheetC201, List<Object[]> listaDetID) {
		int itemIni = 12;
		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);
			setCell(sheetC201, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC201, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC201, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC201, "G", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC201, "H", itemIni, object[7] != null ? object[7].toString() : "");
			setCell(sheetC201, "H", itemIni + 1, object[18] != null ? object[18].toString() : "");
			setCell(sheetC201, "I", itemIni, object[8] != null ? object[8].toString() : "");
			setCell(sheetC201, "K", itemIni, object[9] != null ? object[9].toString() : "");
			setCell(sheetC201, "Q", itemIni, object[10] != null ? object[10].toString() : "");
			setCell(sheetC201, "V", itemIni, object[11] != null ? object[11].toString() : "");
			setCell(sheetC201, "AA", itemIni, object[12] != null ? object[12].toString() : "");
			setCell(sheetC201, "AE", itemIni, object[13] != null ? object[13].toString() : "");
			setCell(sheetC201, "AF", itemIni, object[19] != null ? object[19].toString() : "");
			setCell(sheetC201, "AG", itemIni, object[14] != null ? object[14].toString() : "");
			itemIni = itemIni + 2;
		}
	}

	private void llenadoC202(HSSFSheet sheetC202, List<Object[]> listaDetID) {
		int itemIni = 12;
		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);
			setCell(sheetC202, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC202, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC202, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC202, "G", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC202, "H", itemIni, object[7] != null ? object[7].toString() : "");
			setCell(sheetC202, "H", itemIni + 1, object[18] != null ? object[18].toString() : "");
			setCell(sheetC202, "I", itemIni, object[15] != null ? object[15].toString() : "");
			setCell(sheetC202, "J", itemIni, object[16] != null ? object[16].toString() : "");
			setCell(sheetC202, "K", itemIni, object[17] != null ? object[17].toString() : "");
			setCell(sheetC202, "L", itemIni, object[14] != null ? object[14].toString() : "");
			itemIni = itemIni + 2;
		}
	}

	private void llenadoC203(HSSFSheet sheetC203, List<Object[]> listaDetID) {
		int itemIni = 12;
		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);
			setCell(sheetC203, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC203, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC203, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC203, "G", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC203, "H", itemIni, object[7] != null ? object[7].toString() : "");
			setCell(sheetC203, "H", itemIni + 1, object[18] != null ? object[18].toString() : "");
			setCell(sheetC203, "I", itemIni, object[14] != null ? object[14].toString() : "");
			itemIni = itemIni + 2;
		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	static void setCell(HSSFSheet sheet, int col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(col);
		cell.setCellValue(texto);
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
