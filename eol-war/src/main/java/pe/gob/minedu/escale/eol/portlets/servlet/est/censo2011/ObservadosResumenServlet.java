package pe.gob.minedu.escale.eol.portlets.servlet.est.censo2011;

import static pe.gob.minedu.escale.eol.portlets.util.Constantes.USUARIO_UGEL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;

public class ObservadosResumenServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(ObservadosResumenServlet.class);

	@EJB
	private PadronLocal padronSrv = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");

		String codUGEL = request.getParameter("codUGEL");
		String strExp = request.getParameter("formato") != null ? request.getParameter("formato") : "";
		String strTipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
		String strAnio = request.getParameter("anio") != null ? request.getParameter("anio") : "";
		// PadronFacade padronSrv = lookupPadronFacade();
		EnvioDocumentosFacade matriSrv = lookupEnvioDocumentosFacade();

		List<Object[]> listaPadron = null;
		List<Object[]> listSend = null;
		List<List> listCentrosSend = new ArrayList();
		/*
		 * if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2015"))) {
		 * listSend = matriSrv.getCuentaResultado2015Envios(codUGEL, "UGEL",true);
		 * listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL",
		 * "CENSO-RESULTADO");
		 * 
		 * }else if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2016"))) {
		 * listSend = matriSrv.getCuentaResultado2016Envios(codUGEL, "UGEL",true);
		 * listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL",
		 * "CENSO-RESULTADO");
		 * 
		 * }else
		 */
		if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2017"))) {
			listSend = matriSrv.getCuentaResultado2017Envios(codUGEL, "UGEL", true);
			listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-RESULTADO");
		} else if ((strTipo.equals("CENSO-RESULTADO")) && (strAnio.equals("2018"))) {
			listaPadron = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "CENSO-RESULTADO");
			listSend = matriSrv.getCuentaResultado2018Envios(codUGEL, USUARIO_UGEL, true);
		}

		for (Object[] cen : listSend) {
			int xPos = -1;
			if (strTipo.equals("CENSO-RESULTADO")) {
				xPos = Collections.binarySearch(listaPadron, cen, buscarEnvioLista);
			}
			if (xPos >= 0) {
				List listTmp = new ArrayList(Arrays.asList((Object[]) listaPadron.get(xPos)));
				listTmp.add(cen[3]);
				listCentrosSend.add(listTmp);
			}
		}

		request.setAttribute("resumen_matri_envios", listCentrosSend);
		request.setAttribute("TipoConsulta", strTipo);
		if ("xls".equals(strExp)) {
			response.setHeader("Content-type", "application/octet-stream");
			response.addHeader("Content-Disposition", "attachment; filename=\"cuadroCobertura.xls\"");
		}

		RequestDispatcher requestDispatcher = getServletContext()
				.getRequestDispatcher("/WEB-INF/jsp/ugel/reporte/resumenObservados.jsp");
		requestDispatcher.include(request, response);
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	/*
	 * private PadronFacade lookupPadronFacade() { try { Context c = new
	 * InitialContext(); //
	 * java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.ejb.common.
	 * PadronFacade return (PadronFacade) c.lookup(
	 * "java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.facade.PadronFacade"
	 * );
	 * 
	 * } catch (NamingException ne) { logger.info(Level.SEVERE, "exception caught",
	 * ne); throw new RuntimeException(ne); } }
	 */

	private EnvioDocumentosFacade lookupEnvioDocumentosFacade() {
		try {
			Context c = new InitialContext();
			// java:global/eol-war/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.ejb.EnvioDocumentosFacade
			return (EnvioDocumentosFacade) c.lookup(
					"java:global/eol-war/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade");

		} catch (NamingException ne) {
			logger.info("ERROR lookupEnvioDocumentosFacade = " + ne.getMessage());
			throw new RuntimeException(ne);
		}
	}

	private Comparator<Object[]> buscarEnvioLista = new Comparator<Object[]>() {

		public int compare(Object[] o1, Object[] o2) {
			if (((String) o1[0]).compareTo((String) o2[0]) == 0) {
				if (((Character) o1[1]).compareTo((Character) o2[1]) == 0) {
					return ((String) o1[2]).substring(0, 1).compareTo(((String) o2[2]).substring(0, 1));
				} else {
					return ((String) o1[1]).compareTo((String) o2[1]);
				}
			} else {
				return ((String) o1[0]).compareTo((String) o2[0]);
			}
		}
	};

	static Comparator<Object[]> buscarEnvioListaLocal = new Comparator<Object[]>() {
		public int compare(Object[] o1, Object[] o2) {

			return ((String) o1[0]).compareTo((String) o2[0]);
		}
	};

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
