/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pe.gob.minedu.escale.eol.estadistica.ejb.SesionUsuarioFacade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.portlets.ejb.MensajeriaEOLFacade;
import pe.gob.minedu.escale.eol.portlets.ejb.MensajeriaUsuarioEOLFacade;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.EolMensajeria;
import pe.gob.minedu.escale.eol.portlets.ejb.domain.EolMensajeriaUsuario;

/**
 *
 * @author JMATAMOROS
 */
public class MensajeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(MensajeServlet.class);

	@EJB
	private SesionUsuarioFacade usuarioFacade;

	@EJB
	MensajeriaEOLFacade mensajeEOLFacade;

	@EJB
	MensajeriaUsuarioEOLFacade mensajeUsuarioEOLFacade;

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	private static int rowMax = 10;

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@SuppressWarnings("unused")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: MensajeServlet.processRequest :: Starting execution...");
		response.setHeader("Cache-control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		String opcion = request.getParameter("OPC");
		String tipo = request.getParameter("TIPO");
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		// String usuario=(String)request.getParameter("USUARIO");

		try {
			/*
			 * if(usuario==null || usuario.isEmpty()) { out.println("-1"); return; }
			 */
			if (tipo.equals("UGEL")) {
				String userUGEL = request.getParameter("COD_UGEL");

				if (opcion.equals("C"))// CANTIDAD DE MENSAJES RECIBIDOS
				{
					int cantidad = mensajeEOLFacade.countFindByusuarioDestino(userUGEL);
					int cantidadDel = mensajeUsuarioEOLFacade.findMsjsDeleteByBusuario(userUGEL).intValue();
					out.println(String.valueOf(cantidad - cantidadDel));
				} else if (opcion.equals("I"))// ENVIAR MENSAJE
				{ // String asunto=request.getParameter("asunto");
					String msj = request.getParameter("mensaje");
					// String userDestino=request.getParameter("destino");

					Integer nivel = 1;
					Date fecEnvio = new Date();
					EolMensajeria mensaje = gson.fromJson(msj, EolMensajeria.class);// new EolMensajeria();

					// mensaje.setAsunto(asunto);
					mensaje.setArchivoAdjunto(null);
					mensaje.setEstado(1);
					mensaje.setFechaCrea(fecEnvio);
					// mensaje.setMensaje(msj);
					mensaje.setUsuarioOrigen(userUGEL);
					if (mensaje.getUsuarioDestino() != null && mensaje.getUsuarioDestino().isEmpty())
						mensaje.setUsuarioDestino(null);
					if (mensaje.getUsuarioDestino() != null) {
						if (usuarioFacade.userAuthValid(mensaje.getUsuarioDestino()) == null) {
							Respuesta rpt = new Respuesta("error", String
									.format("No existe un usuario con el código %s !!", mensaje.getUsuarioDestino()));
							out.println(gson.toJson(rpt));
							return;
						}
					}
					mensaje.setImportancia(nivel);
					mensajeEOLFacade.create(mensaje);
					Respuesta rpt = new Respuesta("ok");
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("M"))// MENSAJES RECIBIDOS
				{
					Integer rowIni = Integer.parseInt(request.getParameter("start"));
					List<EolMensajeria> listMsj = new ArrayList<EolMensajeria>();
					List<EolMensajeria> listMTmp = new ArrayList<EolMensajeria>();
					boolean falta = true;
					List<EolMensajeriaUsuario> listMsjL = mensajeUsuarioEOLFacade.findMsjsLeidosByUsuario(userUGEL);
					EolMensajeriaUsuario eolMU = new EolMensajeriaUsuario();
					eolMU.setUsuario(userUGEL);
					Collections.sort(listMsjL, buscarMsgLeido);

					while (falta && (!(listMTmp = mensajeEOLFacade.findByUsuarioDestino(userUGEL, rowIni, rowMax))
							.isEmpty())) {
						for (EolMensajeria msj : listMTmp) {
							if (listMsj.size() < rowMax) {
								eolMU.setMensaje(msj);
								int pos = Collections.binarySearch(listMsjL, eolMU, buscarMsgLeido);
								if (pos >= 0) {
									EolMensajeriaUsuario eolTmp = listMsjL.get(pos);
									if (eolTmp.getEstado().equals(1)) {
										msj.setLeido(true);
										listMsj.add(msj);
									}
								} else {
									listMsj.add(msj);
								}
							} else {
								falta = false;
								break;
							}
						}
						rowIni = rowIni + rowMax;
					}

					Respuesta rpt = new Respuesta(listMsj);
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("L"))// LEER MENSAJE
				{
					Integer idMsj = Integer.parseInt(request.getParameter("idMsj"));
					EolMensajeria eolM = mensajeEOLFacade.find(idMsj);
					List<EolMensajeriaUsuario> listMsjL = mensajeUsuarioEOLFacade.findMsjsLeidosByUsuario(userUGEL);
					EolMensajeriaUsuario eolMU = new EolMensajeriaUsuario();
					eolMU.setMensaje(eolM);
					eolMU.setUsuario(userUGEL);
					Collections.sort(listMsjL, buscarMsgLeido);
					if (listMsjL != null) {
						int pos = Collections.binarySearch(listMsjL, eolMU, buscarMsgLeido);
						if (pos < 0) {
							eolMU.setUsuario(userUGEL);
							eolMU.setMensaje(eolM);
							eolMU.setEstado(1);
							eolMU.setFecha(new Date());
							mensajeUsuarioEOLFacade.create(eolMU);
						}
						Respuesta rpt = new Respuesta(eolM);
						out.println(gson.toJson(rpt));
					}
				} else if (opcion.equals("Q"))// QUITAR MENSAJE
				{
					Integer idMsj = Integer.parseInt(request.getParameter("idMsj"));
					EolMensajeria eolM = mensajeEOLFacade.find(idMsj);
					List<EolMensajeriaUsuario> listMsjL = mensajeUsuarioEOLFacade
							.findMsjsLeidosByUsuarioAndMsj(userUGEL, idMsj);
					EolMensajeriaUsuario eolMU = new EolMensajeriaUsuario();
					eolMU.setMensaje(eolM);
					eolMU.setUsuario(userUGEL);
					Collections.sort(listMsjL, buscarMsgLeido);
					int pos = Collections.binarySearch(listMsjL, eolMU, buscarMsgLeido);
					if (pos >= 0) {
						eolMU = listMsjL.get(pos);
						eolMU.setEstado(2);
						eolMU.setFecha(new Date());
						mensajeUsuarioEOLFacade.edit(eolMU);
					} else {
						eolMU.setUsuario(userUGEL);
						eolMU.setMensaje(eolM);
						eolMU.setEstado(2);
						eolMU.setFecha(new Date());
						mensajeUsuarioEOLFacade.create(eolMU);
					}
					Respuesta rpt = new Respuesta(eolM);
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("M_ENV"))// MENSAJES ENVIADOS
				{
					Integer rowIni = Integer.parseInt(request.getParameter("start"));
					List<EolMensajeria> listMsj = mensajeEOLFacade.findByUsuarioOrigen(userUGEL, rowIni, rowMax);
					Respuesta rpt = new Respuesta(listMsj);
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("C_ENV"))// CANTIDAD DE MENSAJES ENVIADOS
				{
					int cantidad = mensajeEOLFacade.countFindByUsuarioOrigenAll(userUGEL);
					out.println(String.valueOf(cantidad));
				} else if (opcion.equals("L_ENV"))// LEER MENSAJE ENVIADO
				{
					Integer idMsj = Integer.parseInt(request.getParameter("idMsj"));
					EolMensajeria eolM = mensajeEOLFacade.find(idMsj);
					Respuesta rpt = new Respuesta(eolM);
					out.println(gson.toJson(rpt));
				}

			} else if (tipo.equals("IE")) {
				String userIE = request.getParameter("COD_MOD");

				// PadronClient pCli = new PadronClient(userIE,"0");
				// InstitucionEducativa ie = pCli.get_JSON(InstitucionEducativa.class);
				CentroEducativo ie = padronFacade.obtainByCodMod(userIE, "0");

				String userUGEL = ie.getCodooii();

				if (opcion.equals("C")) {
					int countMsj = mensajeEOLFacade.countFindByUsuarioOrigenAndDestino(userUGEL, userIE);
					int countMsjByUgel = mensajeEOLFacade.countFindByUsuarioOrigenAndDestino(userUGEL, null);
					int cantidadDel = mensajeUsuarioEOLFacade.findMsjsDeleteByBusuario(userIE).intValue();

					out.println(String.valueOf(countMsj + countMsjByUgel - cantidadDel));
				}
				if (opcion.equals("I")) {
					String asunto = request.getParameter("asunto");
					String msj = request.getParameter("mensaje");
					Integer nivel = 1;
					Date fecEnvio = new Date();
					EolMensajeria mensaje = gson.fromJson(msj, EolMensajeria.class);// new EolMensajeria();

					// mensaje.setAsunto(asunto);
					mensaje.setArchivoAdjunto(null);
					mensaje.setEstado(1);
					mensaje.setFechaCrea(fecEnvio);
					// mensaje.setMensaje(msj);
					mensaje.setUsuarioOrigen(userIE);
					mensaje.setUsuarioDestino(userUGEL);
					mensaje.setImportancia(nivel);
					mensajeEOLFacade.create(mensaje);
					Respuesta rpt = new Respuesta("ok");
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("M")) {
					Integer rowIni = Integer.parseInt(request.getParameter("start"));
					List<EolMensajeria> listM1 = mensajeEOLFacade.findByUsuarioOrigenAndDestino(userUGEL, userIE,
							rowIni, rowMax);
					List<EolMensajeria> listM2 = mensajeEOLFacade.findByUsuarioOrigenAndDestino(userUGEL, null, rowIni,
							rowMax);
					List<EolMensajeriaUsuario> listMsjL = mensajeUsuarioEOLFacade.findMsjsLeidosByUsuario(userIE);
					List<EolMensajeria> listMsj = new ArrayList<EolMensajeria>();
					listM1.addAll(listM2);

					EolMensajeriaUsuario eolMU = new EolMensajeriaUsuario();
					eolMU.setUsuario(userIE);
					Collections.sort(listMsjL, buscarMsgLeido);
					boolean falta = true;

					while (falta && (!listM1.isEmpty())) {
						for (EolMensajeria msj : listM1) {
							if (listMsj.size() < rowMax) {
								eolMU.setMensaje(msj);
								int pos = Collections.binarySearch(listMsjL, eolMU, buscarMsgLeido);
								if (pos >= 0) {
									EolMensajeriaUsuario eolTmp = listMsjL.get(pos);
									if (eolTmp.getEstado().equals(1)) {
										msj.setLeido(true);
										listMsj.add(msj);
									}
								} else {
									listMsj.add(msj);
								}
							} else {
								falta = false;
								break;
							}
						}
						rowIni = rowIni + rowMax;
						listM1 = mensajeEOLFacade.findByUsuarioOrigenAndDestino(userUGEL, userIE, rowIni, rowMax);
						listM2 = mensajeEOLFacade.findByUsuarioOrigenAndDestino(userUGEL, null, rowIni, rowMax);
						listM1.addAll(listM2);
					}

					Respuesta rpt = new Respuesta(listMsj);
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("L")) {
					Integer idMsj = Integer.parseInt(request.getParameter("idMsj"));
					EolMensajeria eolM = mensajeEOLFacade.find(idMsj);
					List<EolMensajeriaUsuario> listMsjL = mensajeUsuarioEOLFacade.findMsjsLeidosByUsuario(userIE);
					EolMensajeriaUsuario eolMU = new EolMensajeriaUsuario();
					eolMU.setMensaje(eolM);
					eolMU.setUsuario(userIE);
					Collections.sort(listMsjL, buscarMsgLeido);
					int pos = Collections.binarySearch(listMsjL, eolMU, buscarMsgLeido);
					// EolMensajeriaUsuario eolMU= new EolMensajeriaUsuario();
					if (pos < 0) {
						eolMU.setUsuario(userIE);
						eolMU.setMensaje(eolM);
						eolMU.setEstado(1);
						eolMU.setFecha(new Date());
						mensajeUsuarioEOLFacade.create(eolMU);
					}
					Respuesta rpt = new Respuesta(eolM);
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("Q"))// QUITAR MENSAJE
				{
					Integer idMsj = Integer.parseInt(request.getParameter("idMsj"));
					EolMensajeria eolM = mensajeEOLFacade.find(idMsj);
					List<EolMensajeriaUsuario> listMsjL = mensajeUsuarioEOLFacade.findMsjsLeidosByUsuarioAndMsj(userIE,
							idMsj);
					EolMensajeriaUsuario eolMU = new EolMensajeriaUsuario();
					eolMU.setMensaje(eolM);
					eolMU.setUsuario(userIE);
					Collections.sort(listMsjL, buscarMsgLeido);
					int pos = Collections.binarySearch(listMsjL, eolMU, buscarMsgLeido);
					if (pos >= 0) {
						eolMU = listMsjL.get(pos);
						eolMU.setEstado(2);
						eolMU.setFecha(new Date());
						mensajeUsuarioEOLFacade.edit(eolMU);
					} else {
						eolMU.setUsuario(userUGEL);
						eolMU.setMensaje(eolM);
						eolMU.setEstado(2);
						eolMU.setFecha(new Date());
						mensajeUsuarioEOLFacade.create(eolMU);
					}
					Respuesta rpt = new Respuesta(eolM);
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("M_ENV"))// MENSAJES ENVIADOS
				{
					Integer rowIni = Integer.parseInt(request.getParameter("start"));
					List<EolMensajeria> listMsj = mensajeEOLFacade.findByUsuarioOrigen(userIE, rowIni, rowMax);
					Respuesta rpt = new Respuesta(listMsj);
					out.println(gson.toJson(rpt));
				} else if (opcion.equals("C_ENV"))// CANTIDAD DE MENSAJES ENVIADOS
				{
					int cantidad = mensajeEOLFacade.countFindByUsuarioOrigenAll(userIE);
					out.println(String.valueOf(cantidad));
				} else if (opcion.equals("L_ENV"))// LEER MENSAJE ENVIADO
				{
					Integer idMsj = Integer.parseInt(request.getParameter("idMsj"));
					EolMensajeria eolM = mensajeEOLFacade.find(idMsj);
					Respuesta rpt = new Respuesta(eolM);
					out.println(gson.toJson(rpt));
				}

			}
		} finally {
			out.close();
		}
		logger.info(":: MensajeServlet.processRequest :: Execution finish.");
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	public Comparator<EolMensajeriaUsuario> buscarMsgLeido = new Comparator<EolMensajeriaUsuario>() {

		@Override
		public int compare(EolMensajeriaUsuario o1, EolMensajeriaUsuario o2) {
			if ((o1.getMensaje().getId()).compareTo(o2.getMensaje().getId()) == 0) {
				return (o1.getUsuario()).compareTo(o2.getUsuario());
			} else {
				return (o1.getMensaje().getId()).compareTo(o2.getMensaje().getId());
			}

		}
	};

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
