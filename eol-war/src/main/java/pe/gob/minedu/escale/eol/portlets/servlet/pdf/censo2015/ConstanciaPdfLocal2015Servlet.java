package pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2015;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

public class ConstanciaPdfLocal2015Servlet extends HttpServlet {

    @Resource(name = "eol")
    private DataSource eol;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = eol.getConnection();

            String idEnvio= request.getParameter("idEnvio");

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("IdEnvio", idEnvio);
            String report = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2015/cedula11").getFile();
            params.put("SUBREPORT_DIR", report);

            URL urlMaster = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/censo2015/cedula11/Cedula-11-2015.jasper");

            JasperReport masterReport = (JasperReport) JRLoader.loadObject(urlMaster);


            ResourceBundle bundle = ResourceBundle.getBundle("/pe/gob/minedu/escale/eol/portlets/constancia/censo2015/cedula11/etiquetas");
            String escudo = getClass().getResource("/pe/gob/minedu/escale/eol/portlets/constancia/escudo.png").getFile();
            params.put("REPORT_LOCALE", new java.util.Locale("ES", "PE"));
            params.put("REPORT_RESOURCE_BUNDLE", bundle);
            params.put("escudo", escudo);

            byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
            response.setContentType("application/pdf");
            response.setContentLength(buffer.length);
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer, 0, buffer.length);
            ouputStream.flush();
            ouputStream.close();
        } catch (SQLException ex) {
            log(ex.getMessage(), ex);
        } catch (JRException ex) {
            log(ex.getMessage(), ex);
        } catch (Exception ex) {
            log(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                try {
                    if (!conn.isClosed()) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    log(ex.getMessage(), ex);
                }
            }
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
