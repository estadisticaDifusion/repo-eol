package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;


public class ReporteDetallado2016Servlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(ReporteDetallado2016Servlet.class);
	
    static final String PERIODO = "2016";
    static String nroCedula1A = "c01a";
    static String nroCedula2A = "c02a";
    static String nroCedula3A = "c03a";
    static String nroCedula4AI = "c04ai";
    static String nroCedula4AA = "c04aa";
    static String nroCedula5A = "c05a";
    static String nroCedula6A = "c06a";
    static String nroCedula7A = "c07a";
    static String nroCedula8A = "c08a";
    static String nroCedula9A = "c09a";
    static String cuadro201 = "C201";
    static String cuadro202 = "C202";
    static String cuadro203 = "C203";
    static String cuadro204 = "C204";
    static String cuadro206 = "C206";
    static String cuadro207 = "C207";
    static String cuadro208 = "C208";
    static String cuadro209 = "C209";

    @EJB
    private EnvioDocumentosFacade envioFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
            String tipoCedula = request.getParameter("tipced");
            String codUgel = request.getParameter("codugel");//1693910
            String urlDescarga = "/pe/gob/minedu/escale/eol/portlets/plantilla/censo2016/matricula/reporteDetallado_";
            InputStream is = getClass().getResourceAsStream( urlDescarga
                    + tipoCedula.toUpperCase() 
                    + "_" + PERIODO + ".xls");
            
            if (is != null) {
                HSSFWorkbook hssfw = new HSSFWorkbook(is);
                if (tipoCedula.equals("1A")) {
                    llenadoCedula1A(hssfw, codUgel);
                } else if (tipoCedula.equals("2A")) {
                    llenadoCedula2A(hssfw,codUgel);
                } else if (tipoCedula.equals("3A")) {
                    llenadoCedula3A(hssfw, codUgel);
                }else if (tipoCedula.equals("4AI")) {
                    llenadoCedula4AI(hssfw, codUgel);
                }else if (tipoCedula.equals("4AA")) {
                    llenadoCedula4AA(hssfw, codUgel);
                }else if (tipoCedula.equals("5A")) {
                    llenadoCedula5A(hssfw, codUgel);
                }else if (tipoCedula.equals("6A")) {
                    llenadoCedula6A(hssfw, codUgel);
                }else if (tipoCedula.equals("7A")) {
                    llenadoCedula7A(hssfw, codUgel);
                }else if (tipoCedula.equals("8A")) {
                    llenadoCedula8A(hssfw, codUgel);
                }else if (tipoCedula.equals("9A")) {
                    llenadoCedula9A(hssfw, codUgel);
                }
//                else if (tipoCedula.equals("4AA")) {
//                    llenadoCedula3A(hssfw, codUgel);
//                }else if (tipoCedula.equals("5A")) {
//                    llenadoCedula3A(hssfw, codUgel);
//                }else if (tipoCedula.equals("6A")) {
//                    llenadoCedula3A(hssfw, codUgel);
//                }else if (tipoCedula.equals("7A")) {
//                    llenadoCedula3A(hssfw, codUgel);
//                }else if (tipoCedula.equals("8A")) {
//                    llenadoCedula3A(hssfw, codUgel);
//                }}else if (tipoCedula.equals("9A")) {
//                    llenadoCedula3A(hssfw, codUgel);
//                }

            
            response.setContentType("application/octet-stream");

            response.setHeader("Content-Disposition", String.format("attachment;filename=\"reporteDetalllado_%s_2016.xls\"",tipoCedula));
            OutputStream os = response.getOutputStream();
            try {
                hssfw.write(os);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally{
                os.close();
            }
        }
        
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void llenadoCedula1A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet202 = hssfw.getSheet("C202");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet202, "D", 3, codUgel);
        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula1A);
        List<Object[]> list202 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro202,nroCedula1A);
        this.llenar201_1A(list201, sheet201);
        this.llenar202_1A(list202, sheet202);        
    }

    public void llenadoCedula2A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet202 = hssfw.getSheet("C202");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet202, "D", 3, codUgel);

        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula2A);
        List<Object[]> list202 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro202,nroCedula2A);


        this.llenar201_1A(list201, sheet201);
        this.llenar202_1A(list202, sheet202);


    }

    public void llenadoCedula3A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet207 = hssfw.getSheet("C207");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet207, "D", 3, codUgel);

        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula3A);
        List<Object[]> list207 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro207,nroCedula3A);

        this.llenarTablaMatricula(list201, sheet201,15,11);
        this.llenar207_3A(list207, sheet207);
    }

    public void llenadoCedula4AI(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet202 = hssfw.getSheet("C202");
        HSSFSheet sheet208 = hssfw.getSheet("C208");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet202, "D", 3, codUgel);
        setCell(sheet208, "D", 3, codUgel);

        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula4AI);
        List<Object[]> list202 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro202,nroCedula4AI);
        List<Object[]> list208 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro208,nroCedula4AI);

        this.llenarTablaMatricula(list201, sheet201,23,11);
        this.llenarTablaMatricula(list202, sheet202,13,11);
        this.llenarTablaMatricula(list208, sheet208,16,11);
    }

    public void llenadoCedula4AA(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet202 = hssfw.getSheet("C202");
        HSSFSheet sheet209 = hssfw.getSheet("C209");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet202, "D", 3, codUgel);
        setCell(sheet209, "D", 3, codUgel);

        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula4AA);
        List<Object[]> list202 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro202,nroCedula4AA);
        List<Object[]> list209 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro209,nroCedula4AA);

        this.llenarTablaMatricula(list201, sheet201,19,11);
        this.llenarTablaMatricula(list202, sheet202,19,11);
        this.llenarTablaMatricula(list209, sheet209,11,11);
    }
    public void llenadoCedula5A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet206 = hssfw.getSheet("C206");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet206, "D", 3, codUgel);
        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula5A);
        List<Object[]> list206 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro206,nroCedula5A);
        this.llenarTablaMatricula(list201, sheet201,23,11);
        this.llenarTablaMatricula(list206, sheet206,5,11);        
    }
    public void llenadoCedula6A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet209 = hssfw.getSheet("C209");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet209, "D", 3, codUgel);
        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula6A);
        List<Object[]> list209 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro209,nroCedula6A);
        this.llenarTablaMatricula(list201, sheet201,19,11);
        this.llenarTablaMatricula(list209, sheet209,5,11);
    }
    public void llenadoCedula7A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet202 = hssfw.getSheet("C202");
        HSSFSheet sheet209 = hssfw.getSheet("C209");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet202, "D", 3, codUgel);
        setCell(sheet209, "D", 3, codUgel);
        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula7A);
        List<Object[]> list202 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro202,nroCedula7A);
        List<Object[]> list209 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro209,nroCedula7A);
        this.llenarTablaMatricula(list201, sheet201,23,11);
        this.llenarTablaMatricula(list202, sheet202,23,11);
        this.llenarTablaMatricula(list209, sheet209,7,11);
    }
    public void llenadoCedula8A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet203 = hssfw.getSheet("C203");
        HSSFSheet sheet204 = hssfw.getSheet("C204");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet203, "D", 3, codUgel);
        setCell(sheet204, "D", 3, codUgel);
        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula8A);
        List<Object[]> list203 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro203,nroCedula8A);
        List<Object[]> list204 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro204,nroCedula8A);
        this.llenarTablaMatricula(list201, sheet201,17,11);
        this.llenarTablaMatricula(list203, sheet203,22,11);
        this.llenarTablaMatricula(list204, sheet204,22,11);
    }
    public void llenadoCedula9A(HSSFWorkbook hssfw, String codUgel) {
        HSSFSheet sheet201 = hssfw.getSheet("C201");
        HSSFSheet sheet204 = hssfw.getSheet("C204");
        setCell(sheet201, "D", 3, codUgel);
        setCell(sheet204, "D", 3, codUgel);
        List<Object[]> list201 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro201,nroCedula9A);
        List<Object[]> list204 = envioFacade.reporteDetallado2016ByUgel(codUgel,cuadro204,nroCedula9A);
        this.llenarTablaMatricula(list201, sheet201,7,11);
        this.llenarTablaMatricula(list204, sheet204,7,11);
    }
    static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
        HSSFRow row = sheet.getRow(fil - 1);
        HSSFCell cell = row.getCell(letter2col(col));
        cell.setCellValue(texto);
    }

    private void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
            } catch (java.lang.NullPointerException e) {
                logger.error(String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }
        }
    }

    static int letter2col(String col) {
        int iCol = 0;
        short delta = 'Z' - 'A' + 1;
        if (col.length() < 2) {
            return (col.charAt(0) - 'A');
        }
        iCol = col.charAt(1) - 'A';
        iCol += (col.charAt(0) - 'A' + 1) * delta;
        return iCol;
    }

    public void llenar201_1A(List<Object[]> lista, HSSFSheet sheet) {
        if (lista.isEmpty()) {
            logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
            return;
        }
        int totH = 0;
        int totM = 0;

        int colExcCant = 16;
        int rowExcCant = lista.size();
        int rowExcEmpieza = 10;

        String nomCol = "";
        for (int i = 1; i <= colExcCant; i++) {
            switch (i) {
                case 1:
                    nomCol = "B";
                    break;
                case 2:
                    nomCol = "C";
                    break;
                case 3:
                    nomCol = "D";
                    break;
                case 4:
                    nomCol = "E";
                    break;
                case 5:
                    nomCol = "F";
                    break;
                case 6:
                    nomCol = "G";
                    break;
                case 7:
                    nomCol = "H";
                    break;
                case 8:
                    nomCol = "I";
                    break;
                case 9:
                    nomCol = "J";
                    break;
                case 10:
                    nomCol = "K";
                    break;
                case 11:
                    nomCol = "L";
                    break;
                case 12:
                    nomCol = "M";
                    break;
                case 13:
                    nomCol = "N";
                    break;
                case 14:
                    nomCol = "O";
                    break;
                case 15:
                    nomCol = "P";
                    break;
                case 16:
                    nomCol = "Q";
                    break;
            }
            for (int j = 1; j <= rowExcCant; j++) {
                try {
                     Object[] elemento = lista.get(j-1);
                     //System.out.println(((elemento)[i-1]).getClass().toString());
                     if((elemento)[i-1] != null){
                         if (((elemento)[i-1]).getClass().isAssignableFrom(String.class)) {
                            String valors = ((String) (elemento)[i-1]).toString();
                            if (valors != null ) {
                                setCell(sheet, nomCol, rowExcEmpieza + j , valors);
                            }
                         }else
                             if(((elemento)[i-1]).getClass().isAssignableFrom(BigDecimal.class) ||
                                ((elemento)[i-1]).getClass().isAssignableFrom(Integer.class)){
                             int valor = ((Number) (elemento)[i-1]).intValue();
                             if (valor > 0) {
                                setCell(sheet, nomCol, rowExcEmpieza + j, valor);
                             }
                         }
                     }
                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                	logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}", new Object[]{"202", e.getMessage()}));
                    return;
                }
            }
        }
    }

    public void llenar202_1A(List<Object[]> lista, HSSFSheet sheet) {
        if (lista.isEmpty()) {
        	logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
            return;
        }
        int totH = 0;
        int totM = 0;

        int colExcCant = 12;
        int rowExcCant = lista.size();
        int rowExcEmpieza = 10;

        String nomCol = "";
        for (int i = 1; i <= colExcCant; i++) {
            switch (i) {
                case 1:
                    nomCol = "B";
                    break;
                case 2:
                    nomCol = "C";
                    break;
                case 3:
                    nomCol = "G";
                    break;
                case 4:
                    nomCol = "H";
                    break;
                case 5:
                    nomCol = "I";
                    break;
                case 6:
                    nomCol = "J";
                    break;
                case 7:
                    nomCol = "K";
                    break;
                case 8:
                    nomCol = "L";
                    break;
                case 9:
                    nomCol = "M";
                    break;
                case 10:
                    nomCol = "N";
                    break;
                case 11:
                    nomCol = "O";
                    break;
                case 12:
                    nomCol = "P";
                    break;
            }
            for (int j = 1; j <= rowExcCant; j++) {
                try {
                     Object[] elemento = lista.get(j-1);
                     //System.out.println(((elemento)[i-1]).getClass().toString());
                     if((elemento)[i-1] != null){
                        if (((elemento)[i-1]).getClass().isAssignableFrom(String.class)) {
                            String valors = ((String) (elemento)[i-1]).toString();
                            setCell(sheet, nomCol, rowExcEmpieza + j, valors);
    //                        if (valors != null ) {
    //                            setCell(sheet, nomCol, rowExcEmpieza + 1, valors);
    //                        }
                         }else
                             if(((elemento)[i-1]).getClass().isAssignableFrom(BigDecimal.class) ||
                                ((elemento)[i-1]).getClass().isAssignableFrom(Integer.class)){
                             int valor = ((Number) (elemento)[i-1]).intValue();
                             setCell(sheet, nomCol, rowExcEmpieza + j, valor);
    //                         if (valor > 0) {
    //                            setCell(sheet, nomCol, rowExcEmpieza + 1, valor);
    //                         }
                         }
                     }
                     
                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                	logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}", new Object[]{"202", e.getMessage()}));
                    return;
                }
            }
        }
    }

    public void llenar207_3A(List<Object[]> lista, HSSFSheet sheet) {
        if (lista.isEmpty()) {
        	logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
            return;
        }
        int totH = 0;
        int totM = 0;

        int colExcCant = 16;
        int rowExcCant = lista.size();
        int rowExcEmpieza = 10;

        String nomCol = "";
        for (int i = 1; i <= colExcCant; i++) {
            switch (i) {
                case 1:
                    nomCol = "B";
                    break;
                case 2:
                    nomCol = "C";
                    break;
                case 3:
                    nomCol = "G";
                    break;
                case 4:
                    nomCol = "H";
                    break;
                case 5:
                    nomCol = "I";
                    break;
                case 6:
                    nomCol = "J";
                    break;
                case 7:
                    nomCol = "K";
                    break;
                case 8:
                    nomCol = "L";
                    break;
                case 9:
                    nomCol = "M";
                    break;
                case 10:
                    nomCol = "N";
                    break;
                case 11:
                    nomCol = "O";
                    break;
                case 12:
                    nomCol = "P";
                    break;
                case 13:
                    nomCol = "Q";
                    break;
                case 14:
                    nomCol = "R";
                    break;
                case 15:
                    nomCol = "S";
                    break;
                case 16:
                    nomCol = "T";
                    break;
            }
            for (int j = 1; j <= rowExcCant; j++) {
                try {
                     Object[] elemento = lista.get(j-1);
                     if((elemento)[i-1] != null){
                         //System.out.println(((elemento)[i-1]).getClass().toString());
                         if (((elemento)[i-1]).getClass().isAssignableFrom(String.class)) {
                            String valors = ((String) (elemento)[i-1]).toString();
                            if (valors != null ) {
                                setCell(sheet, nomCol, rowExcEmpieza + j, valors);
                            }
                         }else
                             if(((elemento)[i-1]).getClass().isAssignableFrom(BigDecimal.class) ||
                                ((elemento)[i-1]).getClass().isAssignableFrom(Integer.class)){
                             int valor = ((Number) (elemento)[i-1]).intValue();
                             if (valor > 0) {
                                setCell(sheet, nomCol, rowExcEmpieza + j, valor);
                             }
                         }
                     }
                     
                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                	logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}", new Object[]{"202", e.getMessage()}));
                    return;
                }
            }
        }
    }

    public void llenarTablaMatricula(List<Object[]> lista, HSSFSheet sheet, int cantidadColumnasTabla, int nroFilaComienzaTabla ) {
        if (lista.isEmpty()) {
        	logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
            return;
        }
        int totH = 0;
        int totM = 0;

        int colExcCant = cantidadColumnasTabla;
        int rowExcCant = lista.size();
        int rowExcEmpieza = nroFilaComienzaTabla - 1;

        String nomCol = "";
        for (int i = 1; i <= colExcCant; i++) {
            switch (i) {
                case 1:
                    nomCol = "B";
                    break;
                case 2:
                    nomCol = "D";
                    break;
                case 3:
                    nomCol = "E";
                    break;
                case 4:
                    nomCol = "F";
                    break;
                case 5:
                    nomCol = "G";
                    break;
                case 6:
                    nomCol = "H";
                    break;
                case 7:
                    nomCol = "I";
                    break;
                case 8:
                    nomCol = "J";
                    break;
                case 9:
                    nomCol = "K";
                    break;
                case 10:
                    nomCol = "L";
                    break;
                case 11:
                    nomCol = "M";
                    break;
                case 12:
                    nomCol = "N";
                    break;
                case 13:
                    nomCol = "O";
                    break;
                case 14:
                    nomCol = "P";
                    break;
                case 15:
                    nomCol = "Q";
                    break;
                case 16:
                    nomCol = "R";
                    break;
                case 17:
                    nomCol = "S";
                    break;
                case 18:
                    nomCol = "T";
                    break;
                case 19:
                    nomCol = "U";
                    break;
                case 20:
                    nomCol = "V";
                    break;
                case 21:
                    nomCol = "W";
                    break;
                case 22:
                    nomCol = "X";
                    break;
                case 23:
                    nomCol = "Y";
                    break;
                case 24:
                    nomCol = "Z";
                    break;
            }
            for (int j = 1; j <= rowExcCant; j++) {
                try {
                     Object[] elemento = lista.get(j-1);
                     //System.out.println(((elemento)[i-1]).getClass().toString());
                     if((elemento)[i-1] != null){
                         if (((elemento)[i-1]).getClass().isAssignableFrom(String.class)) {
                            String valors = ((String) (elemento)[i-1]).toString();
                            if (valors != null ) {
                                setCell(sheet, nomCol, rowExcEmpieza + j, valors);
                            }
                         }else
                             if(((elemento)[i-1]).getClass().isAssignableFrom(BigDecimal.class) ||
                                ((elemento)[i-1]).getClass().isAssignableFrom(Integer.class)){
                             int valor = ((Number) (elemento)[i-1]).intValue();
                             if (valor > 0) {
                                setCell(sheet, nomCol, rowExcEmpieza + j, valor);
                             }
                         }
                     }
                     
                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                	logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}", new Object[]{"202", e.getMessage()}));
                    return;
                }
            }
        }
    }
}
