/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.rest.client.InstitucionEducativaClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativa;
import pe.gob.minedu.escale.rest.client.domain.InstitucionesEducativas;

/**
 *
 * @author JMATAMOROS
 */
public class ResumenMatricula2013Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(ResumenMatricula2013Servlet.class);

	@EJB
	private EnvioDocumentosFacade matriSrv;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		AuthUsuario usr = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);
		String urlDescarga = "/pe/gob/minedu/escale/eol/portlets/plantilla/censo2013/matricula/ResumenMatricula.xls";

		InputStream is = getClass().getResourceAsStream(urlDescarga);
		HSSFWorkbook hssfw = new HSSFWorkbook(is);

		List<Object[]> lista1A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "1A");
		logger.info("OBTENER LISTA1A");
		List<Object[]> lista2A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "2A");
		logger.info("OBTENER LISTA2A");
		List<Object[]> lista3A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "3A");
		logger.info("OBTENER LISTA3A");
		List<Object[]> lista4A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "4A");
		logger.info("OBTENER LISTA4A");
		List<Object[]> lista5A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "5A");
		logger.info("OBTENER LISTA5A");
		List<Object[]> lista6A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "6A");
		logger.info("OBTENER LISTA6A");
		List<Object[]> lista7A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "7A");
		logger.info("OBTENER LISTA7A");
		List<Object[]> lista8A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "8A");
		logger.info("OBTENER LISTA8A");
		List<Object[]> lista9A = matriSrv.getResumeMatricula2013ByUgel(usr.getUsuario(), "9A");
		logger.info("OBTENER LISTA9A");

		InstitucionEducativaClient client = new InstitucionEducativaClient();
		InstitucionesEducativas ies1A = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "A1", "A2", "A3" });
		logger.info("OBTENER INSTITUCIONES LISTA1A");
		InstitucionesEducativas ies2A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "A5" });
		logger.info("OBTENER INSTITUCIONES LISTA2A");
		InstitucionesEducativas ies3A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "B0", "F0" });
		logger.info("OBTENER INSTITUCIONES LISTA3A");
		InstitucionesEducativas ies4A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "D0" });
		logger.info("OBTENER INSTITUCIONES LISTA4A");
		InstitucionesEducativas ies5A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "K0" });
		logger.info("OBTENER INSTITUCIONES LISTA5A");
		InstitucionesEducativas ies6A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "T0" });
		logger.info("OBTENER INSTITUCIONES LISTA6A");
		InstitucionesEducativas ies7A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "M0" });
		logger.info("OBTENER INSTITUCIONES LISTA7A");
		InstitucionesEducativas ies8A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "E0" });
		logger.info("OBTENER INSTITUCIONES LISTA8A");
		InstitucionesEducativas ies9A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "L0" });
		logger.info("OBTENER INSTITUCIONES LISTA9A");
		InstitucionesEducativas iesProgArti = client.getInstitucionesProgArti(usr.getUsuario(), "1");
		InstitucionesEducativas iesProgIse = client.getInstitucionesProgIse(usr.getUsuario(), "1");
		if (iesProgArti.getItems() != null && !iesProgArti.getItems().isEmpty()) {
			ies1A.getItems().addAll(iesProgArti.getItems());
		}

		if (iesProgIse.getItems() != null && !iesProgIse.getItems().isEmpty()) {
			if (ies6A.getItems() != null)
				ies6A.getItems().addAll(iesProgIse.getItems());
			else
				ies6A.setItems(iesProgIse.getItems());
		}

		llenarResumen(hssfw, ies1A.getItems(), lista1A, 40, "1A", 5);
		llenarResumen(hssfw, ies2A.getItems(), lista2A, 39, "2A", 5);
		llenarResumen(hssfw, ies3A.getItems(), lista3A, 36, "3A", 5);
		llenarResumen(hssfw, ies4A.getItems(), lista4A, 45, "4A", 5);

		llenarResumen(hssfw, ies5A.getItems(), lista5A, 47, "5A", 5);
		llenarResumen(hssfw, ies6A.getItems(), lista6A, 65, "6A", 5);
		llenarResumen(hssfw, ies7A.getItems(), lista7A, 57, "7A", 5);
		llenarResumen(hssfw, ies8A.getItems(), lista8A, 88, "8A", 5);
		llenarResumen(hssfw, ies9A.getItems(), lista9A, 23, "9A", 5);

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=\"ResumenMatricula.xls\"");
		OutputStream os = response.getOutputStream();
		hssfw.write(os);
		os.close();

	}

	public void llenarResumen(HSSFWorkbook hssfw, List<InstitucionEducativa> ies, List<Object[]> datos, int cantidad,
			String formato, int iniFila) {
		HSSFSheet sheet1A = hssfw.getSheet(formato);
		if (ies == null)
			return;
		if (datos == null)
			return;

		InstitucionEducativa ieBq = new InstitucionEducativa();
		InstitucionEducativa ie;
		String column = "";
		for (int row = 0; row < datos.size(); row++) {
			ieBq.setCodigoModular((String) datos.get(row)[0]);
			ieBq.setAnexo((String) datos.get(row)[1]);
			int pos = Collections.binarySearch(ies, ieBq, buscarIE);

			if (pos >= 0) {
				ie = ies.get(pos);
				for (short x = 0; x < cantidad; x++) {
					if (x % 26 != 0) {
						String pre = "";
						switch (x / 26) {
						case 1:
							pre = "A";
							break;
						case 2:
							pre = "B";
							break;
						case 3:
							pre = "C";
							break;
						case 4:
							pre = "D";
							break;
						case 5:
							pre = "E";
							break;
						default:
							pre = "";
							break;
						}
						column = pre.concat(String.valueOf((char) ('A' + x % 26)));
					} else {
						String pre = "";
						switch (x / 26) {
						case 1:
							pre = "A";
							break;
						case 2:
							pre = "B";
							break;
						case 3:
							pre = "C";
							break;
						case 4:
							pre = "D";
							break;
						case 5:
							pre = "E";
							break;
						default:
							pre = "";
							break;
						}
						column = pre.concat("A");
					}

					if (x < 11) {
						switch (x) {
						case 0:
							setCell(sheet1A, column, iniFila + row, ie.getCodDreUgel());
							break;
						case 1:
							setCell(sheet1A, column, iniFila + row, ie.getUbigeo());
							break;
						case 2:
							setCell(sheet1A, column, iniFila + row, ie.getProvincia());
							break;
						case 3:
							setCell(sheet1A, column, iniFila + row, ie.getDistrito());
							break;
						case 4:
							setCell(sheet1A, column, iniFila + row, ie.getNombreIE());
							break;
						case 5:
							setCell(sheet1A, column, iniFila + row, ie.getCodigoModular());
							break;
						case 6:
							setCell(sheet1A, column, iniFila + row, ie.getAnexo());
							break;
						case 7:
							setCell(sheet1A, column, iniFila + row, ie.getNivelModalidad());
							break;
						case 8:
							setCell(sheet1A, column, iniFila + row, ie.getDescGestion().substring(0, 7));
							break;
						case 9:
							setCell(sheet1A, column, iniFila + row, ie.getDescEstado());
							break;
						case 10:
							setCell(sheet1A, column, iniFila + row, ie.getDireccion());
							break;
						}
					} else {
						int pos_val = (x - 11) + 3;
						int num_row = 5 + row;
						if (datos.get(row)[pos_val] != null) {
							if (datos.get(row)[pos_val].getClass().equals(BigDecimal.class)) {
								setCell(sheet1A, column, num_row, ((BigDecimal) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(Integer.class)) {
								setCell(sheet1A, column, num_row, ((Integer) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(String.class)) {
								setCell(sheet1A, column, num_row, ((String) datos.get(row)[pos_val]));
							}
						} /*
							 * else{ setCell(sheet1A, column, num_row, (new Integer(0)).intValue()); }
							 */
					}

				}

			}

		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(valor == null ? 0 : valor.intValue());
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellValue(valor.intValue());
				}

			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
				cell.setCellValue(valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(texto);
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
					cell.setCellValue(new HSSFRichTextString(texto));
				}
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(new HSSFRichTextString(texto));
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		return iCol;
	}

	private Comparator<InstitucionEducativa> buscarIE = new Comparator<InstitucionEducativa>() {

		public int compare(InstitucionEducativa o1, InstitucionEducativa o2) {
			if (o1.getCodigoModular().compareTo(o2.getCodigoModular()) == 0) {
				return o1.getAnexo().compareTo(o2.getAnexo());
			} else {
				return o1.getCodigoModular().compareTo(o2.getCodigoModular());
			}

		}

	};

}
