package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2015;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class CedulaXlsID2015Servlet extends HttpServlet
{
  static final Logger LOGGER = Logger.getLogger(CedulaXlsID2015Servlet.class.getName());
  static final String CEDULA_MATRICULA_DESC = "IdentificacionIECedID_";
  static final String PERIODO = "2015";
  static final String PATH_DIR = "2015/cedulas";
  
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    String preCed = "";
    String nivMod = request.getParameter("nivel");
    String anexo = request.getParameter("anexo");
    String codMod = request.getParameter("codmod");
    LOGGER.log(Level.WARNING, "********** DESCARGA  {0}***********", codMod);
    InputStream is = getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2015/matricula/IdentificacionIECedID_2015.xls");
    if (is != null)
    {
      HSSFWorkbook hssfw = new HSSFWorkbook(is);
      response.setContentType("application/octet-stream");
      
      response.setHeader("Content-Disposition", String.format("attachment;filename=\"IdentificacionIECedID_%s.xls\"", new Object[] { "2015" }));
      OutputStream os = response.getOutputStream();
      try
      {
        hssfw.write(os);
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
      os.close();
    }
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  public String getServletInfo()
  {
    return "Short description";
  }
}
