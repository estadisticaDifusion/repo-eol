package pe.gob.minedu.escale.eol.portlets.actions;

import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;

public class CambioCredencialAction extends Action {

	private Logger logger = Logger.getLogger(CambioCredencialAction.class);

	public static final Charset UTF_8 = Charset.forName("UTF-8");

	public CambioCredencialAction() {

	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info(":: CambioCredencialAction.execute :: Starting execution...");

		HttpSession session = request.getSession();

		AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);

		logger.info("PARAMETRO : " + mapping.getParameter());

		if (usuario == null) {
			logger.info(":: cambio de crendenciales fuera de sesion");
			return mapping.findForward("inicio");
		}

		logger.info(":: CambioCredencialAction.execute :: Execution finish.");
		return mapping.findForward("success");
	}

}
