package pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2018;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.struts.util.MessageResources;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;

/**
 *
 * @author JBEDRILLANA
 */
public class ConstanciaPdfResultado2018Servlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	//private Logger logger = Logger.getLogger(ConstanciaPdfMatricula2018Servlet.class);
	
    @Resource(name = "eol")
    private DataSource eol;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = eol.getConnection();

            String idEnvio= request.getParameter("idEnvio");
            String nivMod = request.getParameter("nivel");//cEdu.getNivMod();
            String nroCed = Funciones.cedulaNivModResultado(nivMod);

            InputStream is = null;
            InputStream resourcei = null;
            
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("ID_ENVIO", idEnvio);
            
            MessageResources res = MessageResources.getMessageResources("eol");
            String rutaRaiz = res.getMessage("eol.path.constancia");
            
            String report = rutaRaiz+"censo2018/resultado/";
            
            params.put("SUBREPORT_DIR", report);

            String urlMaster = rutaRaiz+"censo2018/resultado/cedula".concat(nroCed.toLowerCase()).concat("2018.jasper");
            is = new FileInputStream(urlMaster);
            JasperReport masterReport = (JasperReport) JRLoader.loadObject(is);
            
            resourcei = new FileInputStream(rutaRaiz+"censo2018/resultado/etiquetas.properties");
            ResourceBundle bundle = new PropertyResourceBundle(resourcei);
            
            String escudo = rutaRaiz+"escudom.jpg";
            params.put("REPORT_LOCALE", new java.util.Locale("ES", "PE"));
            params.put("REPORT_RESOURCE_BUNDLE", bundle);
            params.put("escudo", escudo);

            byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
            response.setContentType("application/pdf");
            response.setContentLength(buffer.length);
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer, 0, buffer.length);
            ouputStream.flush();
            ouputStream.close();
        } catch (SQLException ex) {
            log(ex.getMessage(), ex);
        } catch (JRException ex) {
            log(ex.getMessage(), ex);
        } catch (Exception ex) {
            log(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                try {
                    if (!conn.isClosed()) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    log(ex.getMessage(), ex);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
