package pe.gob.minedu.escale.eol.portlets.servlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.auth.client.OAuth2Client;
import pe.gob.minedu.escale.eol.constant.CoreConstant;
import pe.gob.minedu.escale.eol.dto.AuthTokenInfoDTO;
import pe.gob.minedu.escale.eol.dto.OAuthResponseDTO;
import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionRequest;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
import pe.gob.minedu.escale.rest.client.InstitucionEducativaClient;

public class AbstractServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(AbstractServlet.class);

	public OAuthResponseDTO updateRefreshToken(HttpServletRequest request) {

		logger.info(":: AbstractServlet.updateRefreshToken :: Starting execution...");

		OAuthResponseDTO oauthResponse = new OAuthResponseDTO();

		HttpSession session = request.getSession();

		oauthResponse.setStatus(CoreConstant.RESULT_FAILURE);
		oauthResponse.setCode("E0001");
		oauthResponse.setMessage("Access not allowed");

		if (session.getAttribute(Constantes.OAUTH2_SESSION) != null) {
			AuthTokenInfoDTO authTokenInfo = (AuthTokenInfoDTO) session.getAttribute(Constantes.OAUTH2_SESSION);

			MessageResources properties = MessageResources.getMessageResources("eol-oauth2");

			String tokenUri = properties.getMessage("oauth2.token.uri");
			String grantType = properties.getMessage("oauth2.token.grantType");
			String clientId = properties.getMessage("oauth2.token.clientId");
			String clientSecret = properties.getMessage("oauth2.token.clientSecret");

			OAuth2Client oauth2Client = new OAuth2Client(tokenUri);

			String refreshToken = authTokenInfo.getRefreshToken();
			String authorization = authTokenInfo.getAuthorization();

			AuthTokenInfoDTO authRefreshTokenInfo = oauth2Client.getRefreshTokenInfoEol(refreshToken, authorization);
			logger.info("New authRefreshTokenInfo = " + authRefreshTokenInfo);
			if (authRefreshTokenInfo != null) {
				oauthResponse.setStatus(CoreConstant.RESULT_SUCCESS);
				oauthResponse.setCode("S0001");
				oauthResponse.setMessage("The transaction has been refresh token successfully executed");
				oauthResponse.setNewToken(authRefreshTokenInfo.getAccessToken());
				session.setAttribute(Constantes.OAUTH2_SESSION, authRefreshTokenInfo);
				logger.info("token new = " + authRefreshTokenInfo.getAccessToken());
			} else {
				String usernameToken = authTokenInfo.getUsername();
				String passwordToken = authTokenInfo.getClave();
				logger.info("get new token for user = " + usernameToken + "; passwordToken = " + passwordToken);
				oauth2Client = new OAuth2Client(tokenUri, grantType, clientId, clientSecret, usernameToken,
						passwordToken);
				authTokenInfo = oauth2Client.getTokenInfoEol();
				oauthResponse.setStatus(CoreConstant.RESULT_SUCCESS);
				oauthResponse.setCode("S0001");
				oauthResponse.setMessage("The transaction has been get new token successfully executed");
				oauthResponse.setNewToken(authTokenInfo.getAccessToken());
				logger.info("accessToken: " + authTokenInfo.getAccessToken());

				authTokenInfo.setUsername(usernameToken);
				authTokenInfo.setClave(passwordToken);
				session.setAttribute(Constantes.OAUTH2_SESSION, authTokenInfo);
				logger.info("new Token: " + authTokenInfo.getAccessToken());
			}
		}

		logger.info(":: AbstractServlet.updateRefreshToken :: Execution finish.");
		return oauthResponse;
	}

	@SuppressWarnings("unchecked")
	public List<InstitucionDTO> getListInstitucionRest(HttpServletRequest request, String restPadronUri,
			InstitucionRequest institucionRequest) {
		logger.info(":: AbstractServlet.getListInstitucionRest :: Starting execution...");
		logger.info("restPadronUri = " + restPadronUri);
		logger.info("institucionRequest = " + institucionRequest);

		List<InstitucionDTO> listInstitucion = new ArrayList<InstitucionDTO>();
		InstitucionEducativaClient client = new InstitucionEducativaClient();
		HttpSession session = request.getSession();
		if (session.getAttribute(Constantes.OAUTH2_SESSION) != null) {

			AuthTokenInfoDTO authTokenInfo = (AuthTokenInfoDTO) session.getAttribute(Constantes.OAUTH2_SESSION);
			String accessToken = authTokenInfo.getAccessToken();

			Map<String, Object> result = client.getClientRestInstituciones(restPadronUri, accessToken,
					institucionRequest);
			String status = (String) result.get("status");

			if (CoreConstant.RESULT_FAILURE.equals(status)) {
				String message = (String) result.get("message");
				logger.info("return message: " + message);
				if (message != null && "401 Unauthorized".equals(message)) {

					OAuthResponseDTO oauthResponse = updateRefreshToken(request);
					String newToken = oauthResponse.getNewToken();

					result = client.getClientRestInstituciones(restPadronUri, newToken, institucionRequest);
					status = (String) result.get("status");
				}
			}

			if (CoreConstant.RESULT_SUCCESS.equals(status)) {
				listInstitucion = (List<InstitucionDTO>) result.get("resultData");
			}
		}
		
		logger.info(":: AbstractServlet.getListInstitucionRest :: Execution finish.");
		return listInstitucion;
	}
}
