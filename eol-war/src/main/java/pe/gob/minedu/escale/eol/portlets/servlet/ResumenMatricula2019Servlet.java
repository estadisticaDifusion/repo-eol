package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.util.MessageResources;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.dto.entities.InstitucionDTO;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.payload.padron.InstitucionRequest;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;

/**
 * Servlet implementation class ResumenMatricula2019Servlet
 */ 
public class ResumenMatricula2019Servlet extends AbstractServlet {
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(ResumenMatricula2019Servlet.class);
	
	@EJB
	private EnvioDocumentosFacade envioDocumentosFacade;
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: ResumenMatricula2019Servlet.processRequest :: Starting execution...");
		AuthUsuario usr = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);

		MessageResources res = MessageResources.getMessageResources("eol");
		MessageResources apis = MessageResources.getMessageResources("eol-apis");

		String urlDescarga = res.getMessage("eol.path.templates.xls") + "censo2019/matricula/ResumenMatricula.xls";

		logger.info("urlDescarga->" + urlDescarga);
		InputStream is = new FileInputStream(urlDescarga);

		HSSFWorkbook hssfw = new HSSFWorkbook(is);

		List<Object[]> lista1A = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "1a");
		logger.info("OBTENER LISTA1A");
		List<Object[]> lista2A = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "2a");
		logger.info("OBTENER LISTA2A");
		List<Object[]> lista3AP = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "3ap");
		List<Object[]> lista3AS = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "3as");
		logger.info("OBTENER LISTA3A");
		List<Object[]> lista4AI = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "4ai");
		logger.info("OBTENER LISTA4AI");
		List<Object[]> lista4AA = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "4aa");
		logger.info("OBTENER LISTA4AA");
		List<Object[]> lista5A = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "5a");
		logger.info("OBTENER LISTA5A");
		List<Object[]> lista6A = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "6a");
		logger.info("OBTENER LISTA6A");
		List<Object[]> lista7A = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "7a");
		logger.info("OBTENER LISTA7A");
		List<Object[]> lista8AI = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "8ai");
		List<Object[]> lista8AP = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "8ap");
		logger.info("OBTENER LISTA8A");
		List<Object[]> lista9A = envioDocumentosFacade.getResumeMatricula2019ByUgel(usr.getUsuario(), "9a");
		logger.info("OBTENER LISTA9A");

		logger.info("OBTENER INSTITUCIONES LISTA1A: usr: " + usr.getUsuario());

		String restPadronUri = apis.getMessage("rest.padron.uri");
		restPadronUri += "institucionList";

		List<String> niveles = new ArrayList<String>();
		niveles.add("A1");
		niveles.add("A2");
		niveles.add("A3");
		InstitucionRequest institucionRequest = new InstitucionRequest();
		institucionRequest.setCodooii(usr.getUsuario());
		institucionRequest.setNiveles(niveles);
		institucionRequest.setMax(5000);
		institucionRequest.setEstados(Arrays.asList("1"));
		List<InstitucionDTO> ies1A = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("A5");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies2A = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("B0");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies3AP = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("F0");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies3AS = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("D1");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies4AI = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("D2");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies4AA = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("K0");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies5A = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("T0");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies6A = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("M0");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies7A = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("E0");
		niveles.add("E1");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies8AI = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("E2");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies8AP = getListInstitucionRest(request, restPadronUri, institucionRequest);

		niveles = new ArrayList<String>();
		niveles.add("L0");
		institucionRequest.setNiveles(niveles);
		List<InstitucionDTO> ies9A = getListInstitucionRest(request, restPadronUri, institucionRequest);

		institucionRequest.setNiveles(null);
		institucionRequest.setProgarti("1");
		List<InstitucionDTO> iesProgArti = getListInstitucionRest(request, restPadronUri, institucionRequest);

		institucionRequest.setNiveles(null);
		institucionRequest.setProgarti(null);
		institucionRequest.setProgise("1");
		List<InstitucionDTO> iesProgIse = getListInstitucionRest(request, restPadronUri, institucionRequest);
		
		/*
		 * InstitucionesEducativas ies1A =
		 * client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "A1", "A2",
		 * "A3" });
		 
		InstitucionesEducativas ies2A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "A5" });
		logger.info("OBTENER INSTITUCIONES LISTA2A");
		
		InstitucionesEducativas ies3AP = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "B0" });
		
		InstitucionesEducativas ies3AS = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "F0" });
		logger.info("OBTENER INSTITUCIONES LISTA3A");
		
		InstitucionesEducativas ies4AI = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "D1" });
		logger.info("OBTENER INSTITUCIONES LISTA4AI");
		
		InstitucionesEducativas ies4AA = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "D2" });
		logger.info("OBTENER INSTITUCIONES LISTA4AA");
		
		InstitucionesEducativas ies5A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "K0" });
		logger.info("OBTENER INSTITUCIONES LISTA5A");
		
		InstitucionesEducativas ies6A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "T0" });
		logger.info("OBTENER INSTITUCIONES LISTA6A");
		
		InstitucionesEducativas ies7A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "M0" });
		logger.info("OBTENER INSTITUCIONES LISTA7A");
		
		InstitucionesEducativas ies8AI = client.getInstitucionesByDreUgel(usr.getUsuario(),
				new String[] { "E0", "E1" });
				
		InstitucionesEducativas ies8AP = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "E2" });
		logger.info("OBTENER INSTITUCIONES LISTA8A");
		
		InstitucionesEducativas ies9A = client.getInstitucionesByDreUgel(usr.getUsuario(), new String[] { "L0" });
		logger.info("OBTENER INSTITUCIONES LISTA9A");
		

		InstitucionesEducativas iesProgArti = client.getInstitucionesProgArti(usr.getUsuario(), "1");
		InstitucionesEducativas iesProgIse = client.getInstitucionesProgIse(usr.getUsuario(), "1");
*/
		if (iesProgArti != null && !iesProgArti.isEmpty()) {
			ies1A.addAll(iesProgArti);
		}

		if (iesProgIse != null && !iesProgIse.isEmpty()) {
			if (ies6A != null) {
				ies6A.addAll(iesProgIse);
			} else {
				ies6A = iesProgIse;
			}
		}

		llenarResumen(hssfw, ies1A, lista1A, 38, "1A", 6);
		llenarResumen(hssfw, ies2A, lista2A, 37, "2A", 6);
		llenarResumen(hssfw, ies3AP, lista3AP, 35, "3AP", 6);
		llenarResumen(hssfw, ies3AS, lista3AS, 32, "3AS", 6);
		llenarResumen(hssfw, ies4AI, lista4AI, 60, "4AI", 6);
		llenarResumen(hssfw, ies4AA, lista4AA, 63, "4AA", 6);
		llenarResumen(hssfw, ies5A, lista5A, 45, "5A", 6);
		llenarResumen(hssfw, ies6A, lista6A, 51, "6A", 6);
		llenarResumen(hssfw, ies7A, lista7A, 55, "7A", 6);
		llenarResumen(hssfw, ies8AI, lista8AI, 48, "8AI", 6);
		llenarResumen(hssfw, ies8AP, lista8AP, 42, "8AP", 6);
		llenarResumen(hssfw, ies9A, lista9A, 21, "9A", 6);
		// }

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=\"ResumenMatricula.xls\"");
		OutputStream os = response.getOutputStream();
		hssfw.write(os);
		os.close();
		logger.info(":: ResumenMatricula2019Servlet.processRequest :: Execution finish.");
	}
/*
	@SuppressWarnings("unchecked")
	private List<InstitucionDTO> getListInstitucionRest(HttpServletRequest request, String restPadronUri,
			InstitucionRequest institucionRequest) {
		logger.info(":: ResumenMatricula2019Servlet.getListInstitucionRest :: Starting execution...");
		logger.info("restPadronUri = " + restPadronUri);
		logger.info("institucionRequest = " + institucionRequest);
		List<InstitucionDTO> listInstitucion = new ArrayList<InstitucionDTO>();

		InstitucionEducativaClient client = new InstitucionEducativaClient();

		HttpSession session = request.getSession();
		if (session.getAttribute(Constantes.OAUTH2_SESSION) != null) {

			AuthTokenInfoDTO authTokenInfo = (AuthTokenInfoDTO) session.getAttribute(Constantes.OAUTH2_SESSION);
			String accessToken = authTokenInfo.getAccessToken();

			Map<String, Object> result = client.getClientRestInstituciones(restPadronUri, accessToken,
					institucionRequest);
			String status = (String) result.get("status");

			if (CoreConstant.RESULT_FAILURE.equals(status)) {
				String message = (String) result.get("message");
				logger.info("return message: " + message);
				if (message != null && "401 Unauthorized".equals(message)) {

					OAuthResponseDTO oauthResponse = updateRefreshToken(request);
					String newToken = oauthResponse.getNewToken();

					result = client.getClientRestInstituciones(restPadronUri, newToken, institucionRequest);
					status = (String) result.get("status");
				}
			}

			if (CoreConstant.RESULT_SUCCESS.equals(status)) {
				listInstitucion = (List<InstitucionDTO>) result.get("resultData");
			}
		}
		logger.info(":: ResumenMatricula2019Servlet.getListInstitucionRest :: Execution finish.");
		return listInstitucion;
	}
*/
	public void llenarResumen(HSSFWorkbook hssfw, List<InstitucionDTO> ies, List<Object[]> datos, int cantidad,
			String formato, int iniFila) {
		logger.info(":: ResumenMatricula2019Servlet.llenarResumen :: Starting execution...");
		logger.info("size ies: " + (ies != null ? ies.size() : " es Null") + " size datos: "
				+ (datos != null ? datos.size() : " es Null"));
		HSSFSheet sheet1A = hssfw.getSheet(formato);
		if (ies == null) {
			return;
		}
		if (datos == null) {
			return;
		}

		InstitucionDTO ieBq = new InstitucionDTO();
		InstitucionDTO ie;
		String column = "";
		int filaExcel = 0;
		for (int row = 0; row < datos.size(); row++) {
			ieBq.setCodMod((String) datos.get(row)[0]);
			ieBq.setAnexo(datos.get(row)[1].toString());
			
			ie = ies.stream().filter(x -> (x.getCodMod().equals(ieBq.getCodMod()) && x.getAnexo().equals(ieBq.getAnexo()))).findFirst().orElse(null);
			//int pos = Collections.binarySearch(ies, ieBq, buscarIE);

			//if (pos >= 0) {
			if (ie != null) {
				//ie = ies.get(pos);
				
				for (short x = 0; x < cantidad; x++) {
					if (x % 26 != 0) {
						String pre = "";
						switch (x / 26) {
						case 1:
							pre = "A";
							break;
						case 2:
							pre = "B";
							break;
						case 3:
							pre = "C";
							break;
						case 4:
							pre = "D";
							break;
						case 5:
							pre = "E";
							break;
						default:
							pre = "";
							break;
						}
						column = pre.concat(String.valueOf((char) ('A' + x % 26)));
					} else {
						String pre = "";
						switch (x / 26) {
						case 1:
							pre = "A";
							break;
						case 2:
							pre = "B";
							break;
						case 3:
							pre = "C";
							break;
						case 4:
							pre = "D";
							break;
						case 5:
							pre = "E";
							break;
						default:
							pre = "";
							break;
						}
						column = pre.concat("A");
					}

					if (x < 13) {
						switch (x) {
						case 0:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getUgel().getIdUgel());
							break;
						case 1:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDistrito().getIdDistrito());
							break;
						case 2:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDistrito().getProvincia().getNombreProvincia());
							break;
						case 3:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDistrito().getNombreDistrito());
							break;
						case 4:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCenEdu());
							break;
						case 5:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCodMod());
							break;
						case 6:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getAnexo());
							break;
						case 7:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCodlocal());
							break;
						case 8:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getNivelModalidad().getValor());
							break;
						case 9:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getGestionDependencia().getValor());
							break;
						case 10:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getEstado().getValor());
							break;
						case 11:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getDirCen());
							break;
						case 12:
							setCell(sheet1A, column, iniFila + filaExcel, ie.getCenPob());
							break;
						}
					} else {
						int pos_val = 0;
						if (formato.equals("2A")) {
							pos_val = (x - 13) + 3;
						} else {
							pos_val = (x - 13) + 4;
						}

						// int num_row = 6 + filaExcel;
						if (datos.get(row)[pos_val] != null) {
							if (datos.get(row)[pos_val].getClass().equals(BigDecimal.class)) {
								setCell(sheet1A, column, iniFila + filaExcel,
										((BigDecimal) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(Integer.class)) {
								setCell(sheet1A, column, iniFila + filaExcel,
										((Integer) datos.get(row)[pos_val]).intValue());
							} else if (datos.get(row)[pos_val].getClass().equals(String.class)) {
								setCell(sheet1A, column, iniFila + filaExcel, ((String) datos.get(row)[pos_val]));
							}
						}
					}

				}

				filaExcel++;

			}

		}
		logger.info(":: ResumenMatricula2019Servlet.llenarResumen :: Execution finish.");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
		setCell(sheet, col, fil, String.valueOf(texto));
	}

	static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(valor == null ? 0 : valor.intValue());
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellValue(valor.intValue());
				}

			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
				cell.setCellValue(valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		}
	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				HSSFCell cell = row.getCell(letter2col(col));
				if (cell != null) {
					cell.setCellValue(texto);
				} else {
					cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
					cell.setCellValue(new HSSFRichTextString(texto));
				}
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}

		} else {
			try {
				row = sheet.createRow(fil - 1);
				HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(new HSSFRichTextString(texto));
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		return iCol;
	}

	@SuppressWarnings("unused")
	private Comparator<InstitucionDTO> buscarIE = new Comparator<InstitucionDTO>() {
		public int compare(InstitucionDTO o1, InstitucionDTO o2) {
			logger.info("PREVIO CM1 :" + o1.getCodMod() + "CM2 :"+ o2.getCodMod());
			if (o1.getCodMod().compareTo(o2.getCodMod()) == 0) {
				logger.info("CM1 :" + o1.getCodMod() + "CM2 :"+ o2.getCodMod());
				return o1.getAnexo().compareTo(o2.getAnexo());
			} else {
				return o1.getCodMod().compareTo(o2.getCodMod());
			}

		}

	};
	
	@SuppressWarnings("unused")
	private Comparator<Object[]> buscarLista = new Comparator<Object[]>() {
		public int compare(Object[] o1, Object[] o2) {
			if (((String) o1[0]).compareTo((String) o2[0]) == 0) {
				if (((Character) o1[1]).compareTo((Character) o2[1]) == 0) {
					return ((String) o1[2]).substring(0, 1).compareTo(((String) o2[2]).substring(0, 1));
				} else {
					return ((String) o1[1]).compareTo((String) o2[1]);
				}
			} else {
				return ((String) o1[0]).compareTo((String) o2[0]);
			}
		}
	};

}
