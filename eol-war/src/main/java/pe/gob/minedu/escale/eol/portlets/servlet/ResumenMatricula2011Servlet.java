/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;
/**
 *
 * @author JMATAMOROS
 */
public class ResumenMatricula2011Servlet extends HttpServlet {

    @EJB
    private EnvioDocumentosFacade matriSrv;
    static final Logger LOGGER = Logger.getLogger(ResumenMatricula2011Servlet.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        AuthUsuario usr = (AuthUsuario) request.getSession().getAttribute(Constantes.LOGGED_USER);
        String urlDescarga = "/pe/gob/minedu/escale/eol/portlets/plantilla/censo2011/matricula/ResumenMatricula.xls";

        InputStream is = getClass().getResourceAsStream(urlDescarga);
        HSSFWorkbook hssfw = new HSSFWorkbook(is);
        

        List<Object[]> lista1A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c01a");
        List<Object[]> lista2A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c02a");
        List<Object[]> lista3A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c03a");
        List<Object[]> lista4A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c04a");
        List<Object[]> lista5A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c05a");
        List<Object[]> lista6A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c06a");
        List<Object[]> lista7A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c07a");
        List<Object[]> lista8A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c08a");
        List<Object[]> lista9A = matriSrv.getResumenMatriculaByUgel(usr.getUsuario(),"c09a");

        llenar1A(hssfw, lista1A, 35,"1A");
        llenar1A(hssfw, lista2A, 34,"2A");
        llenar1A(hssfw, lista3A, 31,"3A");
        llenar1A(hssfw, lista4A, 46,"4A");
        llenar1A(hssfw, lista5A, 42,"5A");
        llenar1A(hssfw, lista6A, 60,"6A");
        llenar1A(hssfw, lista7A, 68,"7A");
        llenar1A(hssfw, lista8A, 114,"8A");
        llenar1A(hssfw, lista9A, 18,"9A");

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=\"ResumenMatricula.xls\"");
        OutputStream os = response.getOutputStream();
        hssfw.write(os);
        os.close();
    }

    public void llenar1A(HSSFWorkbook hssfw, List<Object[]> datos, int cantidad, String formato) {
        HSSFSheet sheet1A = hssfw.getSheet(formato);
        //System.out.println("FORMATO:"+formato);
        String column = "";
        for (int row = 0; row < datos.size(); row++) {
            for (short x = 0; x < cantidad; x++) {
                if (x % 26 != 0) {
                    String pre = "";
                    switch (x / 26) {
                        case 1:
                            pre = "A";
                            break;
                        case 2:
                            pre = "B";
                            break;
                        case 3:
                            pre = "C";
                            break;
                        case 4:
                            pre = "D";
                            break;
                        case 5:
                            pre = "E";
                            break;
                        default:
                            pre = "";
                            break;
                    }
                    column = pre.concat(String.valueOf((char) ('A' + x % 26)));
                    //System.out.println(x+ " - " + x%26 + " - " + col );
                } else {
                    String pre = "";
                    switch (x / 26) {
                        case 1:
                            pre = "A";
                            break;
                        case 2:
                            pre = "B";
                            break;
                        case 3:
                            pre = "C";
                            break;
                        case 4:
                            pre = "D";
                            break;
                        case 5:
                            pre = "E";
                            break;
                        default:
                            pre = "";
                            break;
                    }
                    column = pre.concat("A");
                }

                
                if (datos.get(row) != null && datos.get(row)[x] != null) {
                    if (x < 10) {
                        switch(x)
                        {case 8:setCell(sheet1A, column, 5 + row, datos.get(row)[x].toString().startsWith("A")?"Público":"Prívado");break;
                            case 9:
                                setCell(sheet1A, column, 5 + row, datos.get(row)[x].toString().equals("1") ? "Activo" :"Inactivo");
                                break;
                            default:
                                setCell(sheet1A, column, 5 + row, datos.get(row)[x].toString());
                        }


                    } else {
                        
                        setCell(sheet1A, column, 5 + row, (datos.get(row)[x]!=null?(java.math.BigDecimal)datos.get(row)[x]:new BigDecimal(0)).intValue());
                    }
                }


            }

        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
        setCell(sheet, col, fil, String.valueOf(texto));
    }

    static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                HSSFCell cell = row.getCell(letter2col(col));
                if (cell != null) {
                    cell.setCellValue(valor == null ? 0 : valor.intValue());
                }else
                {   cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(valor.intValue());
                }
                
            } catch (java.lang.NullPointerException e) {
                //row.setCell(cell);
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        } else {
            try {
                row = sheet.createRow(fil - 1);
                HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
                cell.setCellValue(valor.intValue());
            } catch (java.lang.NullPointerException e) {
                //row.setCell(cell);
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        }
    }

    static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                HSSFCell cell = row.getCell(letter2col(col));
                if (cell != null) {
                    cell.setCellValue(texto);
                } else {
                    cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
                    cell.setCellValue(new HSSFRichTextString(texto));
                }
            } catch (java.lang.NullPointerException e) {
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        } else {
            try {
                row = sheet.createRow(fil - 1);
                HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
                cell.setCellValue(new HSSFRichTextString(texto));
            } catch (java.lang.NullPointerException e) {
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }
        }
    }

    static int letter2col(String col) {
        int iCol = 0;
        short delta = 'Z' - 'A' + 1;

        if (col.length() < 2) {
            return (col.charAt(0) - 'A');
        }
        iCol = col.charAt(1) - 'A';
        iCol += (col.charAt(0) - 'A' + 1) * delta;
        /*for (int i = col.length(); i > 0; i--) {
        char chr = col.charAt(i - 1);
        short iChr = (short) (chr - 'A');
        iCol += (iChr * (col.length() - i + 1) * delta);
        }*/
        return iCol;
    }
}
