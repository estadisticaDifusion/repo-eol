/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet;

/**
 *
 * @author DSILVA
 */
public class Respuesta {

    private String status;
    private String mensaje;
    private long id;
    private Object data;

    public Respuesta() {
    }

    public Respuesta(long id) {
        this.status = "ok";
        this.id = id;
    }

    public Respuesta(String status) {
        this.status = status;
    }

    public Respuesta(Object data) {
        this.status = "ok";
        this.data = data;
    }

    public Respuesta(String status, String mensaje) {
        this.status = status;
        this.mensaje = mensaje;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
