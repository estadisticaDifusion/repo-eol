package pe.gob.minedu.escale.eol.portlets.servlet.pdf.censo2019;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.minedu.escale.eol.portlets.util.Funciones;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

/**
 * Servlet implementation class ConstanciaPdfResultado2019Servlet
 */
public class ConstanciaPdfResultado2019Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	private Logger logger = Logger.getLogger(ConstanciaPdfResultado2019Servlet.class);
	
	@Resource(name = "eol")
    private DataSource eol;
	
	/**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	logger.info(":: ConstanciaPdfResultado2019Servlet.processRequest :: Starting execution...");
        Connection conn = null;
        try {
        	logger.info("value eol="+eol);
            conn = eol.getConnection();
            
            logger.info("value conn="+conn);
            
            String nivMod = request.getParameter("nivel");
            String preCed = Funciones.cedulaNivModResultado(nivMod);
            String idEnvio = request.getParameter("idEnvio");

            boolean existChar = false;
            List<String> reqParams = new ArrayList<String>();
            reqParams.add(nivMod);
            reqParams.add(idEnvio);

            existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

            if (!existChar) {            	
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("NIVMOD", nivMod);
                params.put("ID_ENVIO", idEnvio);
                InputStream is = null;
                InputStream resourcei = null;
                
                MessageResources res = MessageResources.getMessageResources("eol");
                
                String rutaRaiz = res.getMessage("eol.path.constancia");
                
                logger.info("params="+params);
                //String report = getClass().getResource(rutaRaiz+"censo2019/matricula").getFile();
                String report = rutaRaiz+"censo2019/resultado/";
           
                logger.info("SUBREPORT_DIR="+report);
                params.put("SUBREPORT_DIR", report);
                
                //URL urlMaster = getClass().getResource(rutaRaiz+"censo2019/matricula/cedula-c0" + preCed.toLowerCase() + "-2019.jasper");
                //String urlMaster = rutaRaiz+"censo2019/resultado/cedula-c0" + preCed.toLowerCase() + "-2019.jasper";
                String urlMaster = rutaRaiz+"censo2019/resultado/cedula".concat(preCed.toLowerCase()).concat("2019.jasper");
                
                logger.info("urlMaster="+urlMaster);
                is = new FileInputStream(urlMaster);
                
                JasperReport masterReport = (JasperReport) JRLoader.loadObject(is);
                
                resourcei = new FileInputStream(rutaRaiz+"censo2019/resultado/etiquetas.properties");
                //ResourceBundle bundle = ResourceBundle.getBundle(rutaRaiz+"censo2019/matricula/etiquetas.properties",Locale.getDefault());
                ResourceBundle bundle = new PropertyResourceBundle(resourcei);
                //String escudo = getClass().getResource(rutaRaiz+"escudom.jpg").getFile();
                String escudo = rutaRaiz+"escudom.jpg";
                params.put("REPORT_LOCALE", new java.util.Locale("ES", "PE"));
                params.put("REPORT_RESOURCE_BUNDLE", bundle);
                params.put("escudo", escudo);

                byte[] buffer = JasperRunManager.runReportToPdf(masterReport, params, conn);
                
                //logger.info("buffer="+buffer);
                
                response.setContentType("application/pdf");
                response.setHeader("Content-Disposition", "inline; filename=\"report2019.pdf\"");
                response.setContentLength(buffer.length);

                ServletOutputStream ouputStream = response.getOutputStream();
                ouputStream.write(buffer, 0, buffer.length);
                ouputStream.flush();
                ouputStream.close();

            } else {
                RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/recursos/error/errorsql.jsp");
                requestDispatcher.include(request, response);
            }

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
/*        } catch (JRException ex) {
        	logger.error(ex.getMessage(), ex);*/
        } catch (Exception ex) {
        	logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                try {
                    if (!conn.isClosed()) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                	logger.error(ex.getMessage(), ex);
                }
            }
        }
        logger.info(":: ConstanciaPdfResultado2019Servlet.processRequest :: Execution finish.");
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
