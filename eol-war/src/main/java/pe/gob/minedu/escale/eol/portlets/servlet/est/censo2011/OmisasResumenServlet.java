/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.servlet.est.censo2011;

import static pe.gob.minedu.escale.eol.portlets.util.Constantes.USUARIO_DRE;
import static pe.gob.minedu.escale.eol.portlets.util.Constantes.USUARIO_UGEL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;
import pe.gob.minedu.escale.eol.utils.VerifSQLInject;

/**
 *
 * @author JMATAMOROS
 */
public class OmisasResumenServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(OmisasResumenServlet.class);

	@EJB
	private PadronLocal padronSrv = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");

	@SuppressWarnings("rawtypes")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: OmisasResumenServlet.processRequest :: Starting execution...");
		response.setContentType("text/html;charset=UTF-8");

		String codUGEL = request.getParameter("codUGEL");
		String strExp = request.getParameter("formato") != null ? request.getParameter("formato") : "";
		String strTipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
		String strAnio = request.getParameter("anio") != null ? request.getParameter("anio") : "";

		boolean existChar = false;
		List<String> reqParams = new ArrayList<String>();
		reqParams.add(codUGEL);
		reqParams.add(strExp);
		reqParams.add(strTipo);
		reqParams.add(strAnio);

		existChar = VerifSQLInject.verificacionSqlInjectList(reqParams);

		if (!existChar) {
			// PadronFacade padronSrv = lookupPadronFacade();
			EnvioDocumentosFacade matriSrv = lookupEnvioDocumentosFacade();

			List<Object[]> lista = null;
			List<Object[]> listSend = null;
			/*
			 * if(strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2011")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-MATRICULA");
			 * listSend=matriSrv.getCuentaMatricula2011Envios(codUGEL,USUARIO_UGEL);
			 * //Collections.sort(lista, buscarLista); }else
			 * if(strTipo.equals("CENSO-LOCAL")&& strAnio.equals("2011")) { lista =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL,USUARIO_UGEL); listSend =
			 * matriSrv.getCuentaLocal2011Envios(codUGEL, USUARIO_UGEL);
			 * //Collections.sort(lista, buscarEnvioListaLocal); }else
			 * if(strTipo.equals("CENSO-RESULTADO")&& strAnio.equals("2011")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-RESULTADO");
			 * listSend=matriSrv.getCuentaResultado2011Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2012")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-MATRICULA");
			 * listSend=matriSrv.getCuentaMatricula2012Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-LOCAL")&& strAnio.equals("2012")) { lista =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL,USUARIO_UGEL); listSend =
			 * matriSrv.getCuentaLocal2012Envios(codUGEL, USUARIO_DRE);
			 * //Collections.sort(lista, buscarEnvioListaLocal); }else
			 * if(strTipo.equals("CENSO-RESULTADO")&& strAnio.equals("2012")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-RESULTADO");
			 * listSend=matriSrv.getCuentaResultado2012Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2013")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-MATRICULA");
			 * listSend=matriSrv.getCuentaMatricula2013Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-LOCAL")&& strAnio.equals("2013")) { lista =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL,USUARIO_UGEL); listSend =
			 * matriSrv.getCuentaLocal2013Envios(codUGEL, USUARIO_DRE); }else
			 * if(strTipo.equals("CENSO-RESULTADO")&& strAnio.equals("2013")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-RESULTADO");
			 * listSend=matriSrv.getCuentaResultado2013Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2014")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-MATRICULA");
			 * listSend=matriSrv.getCuentaMatricula2014Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-LOCAL")&& strAnio.equals("2014")) { lista =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL,USUARIO_UGEL); listSend =
			 * matriSrv.getCuentaLocal2014Envios(codUGEL, USUARIO_DRE); }else
			 * if(strTipo.equals("CENSO-RESULTADO")&& strAnio.equals("2014")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-RESULTADO");
			 * listSend=matriSrv.getCuentaResultado2014Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2015")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-MATRICULA");
			 * listSend=matriSrv.getCuentaMatricula2015Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-LOCAL")&& strAnio.equals("2015")) { lista =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL,USUARIO_UGEL); listSend =
			 * matriSrv.getCuentaLocal2015Envios(codUGEL, USUARIO_DRE); }else
			 * if(strTipo.equals("CENSO-RESULTADO")&& strAnio.equals("2015")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-RESULTADO");
			 * listSend=matriSrv.getCuentaResultado2015Envios(codUGEL,USUARIO_UGEL,false);
			 * }else if ((strTipo.equals("CENSO-ID")) && (strAnio.equals("2015"))) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-ID"); listSend =
			 * matriSrv.getCuentaID2015Envios(codUGEL, "DRE"); }else
			 * if(strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2016")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-MATRICULA");
			 * listSend=matriSrv.getCuentaMatricula2016Envios(codUGEL,USUARIO_UGEL); }else
			 * if(strTipo.equals("CENSO-LOCAL")&& strAnio.equals("2016")) { lista =
			 * padronSrv.getCuentaCentrosByLocal(codUGEL,USUARIO_UGEL); listSend =
			 * matriSrv.getCuentaLocal2016Envios(codUGEL, USUARIO_DRE); }else
			 * if(strTipo.equals("CENSO-RESULTADO")&& strAnio.equals("2016")) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL,USUARIO_UGEL,"CENSO-RESULTADO");
			 * listSend=matriSrv.getCuentaResultado2016Envios(codUGEL,USUARIO_UGEL,false);
			 * }else if ((strTipo.equals("CENSO-ID")) && (strAnio.equals("2016"))) { lista =
			 * padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-ID"); listSend =
			 * matriSrv.getCuentaID2016Envios(codUGEL, "DRE"); }
			 */
			/* imendoza 20170324 inicio */
			if (strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2017")) {
			
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "CENSO-MATRICULA");
				listSend = matriSrv.getCuentaMatricula2017Envios(codUGEL, USUARIO_UGEL);
			} else if (strTipo.equals("CENSO-LOCAL") && strAnio.equals("2017")) {
			
				lista = padronSrv.getCuentaCentrosByLocal(codUGEL, USUARIO_UGEL);
				listSend = matriSrv.getCuentaLocal2017Envios(codUGEL, USUARIO_DRE);
			} else if ((strTipo.equals("CENSO-ID")) && (strAnio.equals("2017"))) {
			
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, "UGEL", "CENSO-ID");
				listSend = matriSrv.getCuentaID2017Envios(codUGEL, "DRE");
			} else if (strTipo.equals("EVALUACION-SIAGIE") && strAnio.equals("2017")) {
		
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "EVALUACION-SIAGIE");
				listSend = matriSrv.getCuentaEvalSiagie2017Envios(codUGEL, USUARIO_UGEL);
			} else if (strTipo.equals("FEN-COSTERO") && strAnio.equals("2017")) {
			
				lista = padronSrv.getCuentaCentrosByLocal(codUGEL, USUARIO_UGEL, "FEN-COSTERO");
				listSend = matriSrv.getCuentaFenNino2017Envios(codUGEL, USUARIO_DRE);
			} else if (strTipo.equals("CENSO-RESULTADO") && strAnio.equals("2017")) {
			
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "CENSO-RESULTADO");
				listSend = matriSrv.getCuentaResultado2017Envios(codUGEL, USUARIO_UGEL, false);
			} else if (strTipo.equals("CENSO-RESULTADO-RECUPERACION") && strAnio.equals("2017")) {
			
				lista = padronSrv.getCuentaCentrosByUGELMarcoRecuperacion(codUGEL, USUARIO_UGEL,
						"CENSO-RESULTADO-RECUPERACION", "2017");
				listSend = matriSrv.getCuentaResultado2017EnviosReqRecuperacion(codUGEL, USUARIO_UGEL, false);
			} /* imendoza 20170324 fin */ // 2018
			else if (strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2018")) {
			
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "CENSO-MATRICULA");
				listSend = matriSrv.getCuentaMatricula2018Envios(codUGEL, USUARIO_UGEL);
			} else if (strTipo.equals("CENSO-LOCAL") && strAnio.equals("2018")) {
			
				lista = padronSrv.getCuentaCentrosByLocal(codUGEL, USUARIO_UGEL);
				listSend = matriSrv.getCuentaLocal2018Envios(codUGEL, USUARIO_DRE);
			} else if (strTipo.equals("CENSO-RESULTADO") && strAnio.equals("2018")) {
			
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "CENSO-RESULTADO");
				listSend = matriSrv.getCuentaResultado2018Envios(codUGEL, USUARIO_UGEL, false);
			} else if (strTipo.equals("CENSO-RESULTADO-RECUPERACION") && strAnio.equals("2018")) {
				
				lista = padronSrv.getCuentaCentrosByUGELMarcoRecuperacion(codUGEL, USUARIO_UGEL,
						"CENSO-RESULTADO-RECUPERACION", "2018");
				listSend = matriSrv.getCuentaResultado2018EnviosReqRecuperacion(codUGEL, USUARIO_UGEL, false);
			}//jbedrillana 2019
			else if (strTipo.equals("CENSO-MATRICULA") && strAnio.equals("2019")) {
		
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "CENSO-MATRICULA");
				listSend = matriSrv.getCuentaMatricula2019Envios(codUGEL, USUARIO_UGEL);
			} else if (strTipo.equals("CENSO-LOCAL") && strAnio.equals("2019")) {
				
				lista = padronSrv.getCuentaCentrosByLocal(codUGEL, USUARIO_UGEL);
//				logger.info("VERIFICANDO LA LISTA PREVIA 2019 : " + lista.size() );
				listSend = matriSrv.getCuentaLocal2019Envios(codUGEL, USUARIO_DRE);
//				logger.info("VERIFICANDO LA LISTA SEND PREVIA 2019 : " + listSend.size() );
				
			} else if (strTipo.equals("CENSO-RESULTADO") && strAnio.equals("2019")) {
			
				lista = padronSrv.getCuentaCentrosByUGEL(codUGEL, USUARIO_UGEL, "CENSO-RESULTADO");
				listSend = matriSrv.getCuentaResultado2019Envios(codUGEL, USUARIO_UGEL, false);
			} else if (strTipo.equals("CENSO-RESULTADO-RECUPERACION") && strAnio.equals("2019")) {
				
				lista = padronSrv.getCuentaCentrosByUGELMarcoRecuperacion(codUGEL, USUARIO_UGEL,
						"CENSO-RESULTADO-RECUPERACION", "2019");
				listSend = matriSrv.getCuentaResultado2019EnviosReqRecuperacion(codUGEL, USUARIO_UGEL, false);
			}

//			for(int j=0 ; j<lista.size();j++){
//				logger.info("LISTA INICIAL : "  + lista.get(j).toString() );
//			}
			
			List<List> listOmisos = new ArrayList<List>();
			
			for (Object[] cen : lista) {
				int pos = 0;
				if (strTipo.equals("CENSO-MATRICULA") || strTipo.equals("CENSO-RESULTADO")
						|| strTipo.equals("CENSO-RESULTADO-RECUPERACION")) {
//					logger.info("ENTRO 111"  + " /// " + cen + " ///  " + buscarLista );
					pos = Collections.binarySearch(listSend, cen, buscarLista);
				} else if (strTipo.equals("CENSO-LOCAL")) {
					pos = Collections.binarySearch(listSend, cen, buscarEnvioListaLocal);
					//logger.info("INDICE "  + " /// " + pos );////////////////////////////////////////////////////
				} else if (strTipo.equals("CENSO-ID")) {
//					logger.info("ENTRO 333"  + " /// " + cen + " ///  " + buscarIDEnvioLista );
					pos = Collections.binarySearch(listSend, cen, buscarIDEnvioLista);
				} else if (strTipo.equals("EVALUACION-SIAGIE")) {
//					logger.info("ENTRO 444"  + " /// " + cen + " ///  " + buscarEnvioListaLocal );
					pos = Collections.binarySearch(listSend, cen, buscarEnvioListaLocal);// COMPARA SOLO EL CODIGO
																							// MODULAR
				} else if (strTipo.equals("FEN-COSTERO")) {
//					logger.info("ENTRO 555"  + " /// " + cen + " ///  " + buscarEnvioListaLocal );
					pos = Collections.binarySearch(listSend, cen, buscarEnvioListaLocal);// SIMILAR A LOCAL
				}

				// Collections.binarySearch(listSend,cen,buscarLista);
			
				if (!(pos >= 0)) {
//					logger.info("ENTRO A INEXISTENTE : " + cen.length + " //// " + cen  );
					listOmisos.add(Arrays.asList(cen));
				}
//				logger.info("LISTADO DE OMISOS : " + listOmisos + " /// " + listOmisos.size()  );
			}
			
			
//			
//			for(int i=0;i<listOmisos.size();i++){
//				logger.info("LISTADO DE OMISOS OUT : " + listOmisos.get(i) + " /// " + listOmisos.size()  );
//			}
			
			
			request.setAttribute("resumen_matri_omisos", listOmisos);
			request.setAttribute("TipoConsulta", strTipo);
			if ("xls".equals(strExp)) {
				request.setAttribute("FORMATO", 1);
				response.setHeader("Content-type", "application/octet-stream");
				response.setCharacterEncoding("UTF-8");
				response.addHeader("Content-Disposition", "attachment; filename=\"cuadroCobertura.xls\"");
			} else {
				request.setAttribute("FORMATO", 0);
			}

			logger.info("requestDispatcher /WEB-INF/jsp/ugel/reporte/resumenOmisas.jsp");
			RequestDispatcher requestDispatcher = getServletContext()
					.getRequestDispatcher("/WEB-INF/jsp/ugel/reporte/resumenOmisas.jsp");

			logger.info(":: OmisasResumenServlet.processRequest :: Execution finish.");
			requestDispatcher.include(request, response);
		} else {
			logger.info("requestDispatcher /recursos/error/errorsql.jsp");
			RequestDispatcher requestDispatcher = getServletContext()
					.getRequestDispatcher("/recursos/error/errorsql.jsp");

			logger.info(":: OmisasResumenServlet.processRequest :: Execution finish.");
			requestDispatcher.include(request, response);
		}

	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
	/*
	 * private PadronFacade lookupPadronFacade() { try { Context c = new
	 * InitialContext(); return (PadronFacade) c.lookup(
	 * "java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.facade.PadronFacade"
	 * );
	 * 
	 * } catch (NamingException ne) { LOGGER.log(Level.SEVERE, "exception caught",
	 * ne); throw new RuntimeException(ne); } }
	 */

	private EnvioDocumentosFacade lookupEnvioDocumentosFacade() {
		try {
			Context c = new InitialContext();
			return (EnvioDocumentosFacade) c.lookup(
					"java:global/eol-war/EnvioDocumentosFacade!pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade");

		} catch (NamingException ne) {
			logger.info("ERROR lookupEnvioDocumentosFacade = " + ne.getMessage());
			throw new RuntimeException(ne);
		}

	}

	private Comparator<Object[]> buscarLista = new Comparator<Object[]>() {

		public int compare(Object[] o1, Object[] o2) {
			if (((String) o1[0]).compareTo((String) o2[0]) == 0) {
				if (((Character) o1[1]).compareTo((Character) o2[1]) == 0) {
					return ((String) o1[2]).substring(0, 1).compareTo(((String) o2[2]).substring(0, 1));
				} else {
//					return ((String) o1[1]).compareTo((String) o2[1]);
					return ((Character) o1[1]).compareTo((Character) o2[1]);
				}
			} else {
				return ((String) o1[0]).compareTo((String) o2[0]);
			}
		}
	};
	static Comparator<Object[]> buscarEnvioListaLocal = new Comparator<Object[]>() {
		public int compare(Object[] o1, Object[] o2) {

			return ((String) o1[0]).compareTo((String) o2[0]);
		}
	};

	static Comparator<Object[]> buscarIDEnvioLista = new Comparator<Object[]>() {
		public int compare(Object[] o1, Object[] o2) {
			return ((String) o1[0]).compareTo((String) o2[0]);
		}
	};

	@SuppressWarnings("unchecked")
	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
