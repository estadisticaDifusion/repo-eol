/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.util;

/**
 *
 * @author JMATAMOROS
 */
public class MensajeJS {

    public static String alerta(String mens) {
        String mensaje = "<script>"
                + "alert('" + mens + "')"
                + "</script>";
        return mensaje;
    }

    public static String redireccionar(String url) {
        String mensaje = "<script> "
                + "document.location.href = '" + url + "'"
                + " </script>";
        return mensaje;
    }

    public static String pagina_anterior() {
        String mensaje = "<script> "
                + "history.go(-1);"
                + " </script>";
        return mensaje;
    }

    public static String cerrar_pagina() {
        String mensaje = "<script type='text/javascript'>"
                + "window.close();"
                + "</script>";
        return mensaje;
    }
}
