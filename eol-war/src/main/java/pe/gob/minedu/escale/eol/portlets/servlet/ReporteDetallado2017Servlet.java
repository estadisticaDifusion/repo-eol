package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.estadistica.ejb.EnvioDocumentosFacade;

public class ReporteDetallado2017Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(ReporteDetallado2017Servlet.class);

	static final String PERIODO = "2017";
	final static String NROCEDULA_1A = "c01a";
	final static String NROCEDULA_2A = "c02a";
	final static String NROCEDULA_3AP = "c03ap";
	final static String NROCEDULA_3AS = "c03as";
	final static String NROCEDULA_4AI = "c04ai";
	final static String NROCEDULA_4AA = "c04aa";
	final static String NROCEDULA_5A = "c05a";
	final static String NROCEDULA_6A = "c06a";
	final static String NROCEDULA_7A = "c07a";
	final static String NROCEDULA_8AI = "c08ai";
	final static String NROCEDULA_8AP = "c08ap";
	final static String NROCEDULA_9A = "c09a";
	final static String cuadro201 = "C201";
	final static String cuadro202 = "C202";
	final static String cuadro203 = "C203";
	final static String cuadro204 = "C204";
	final static String cuadro206 = "C206";
	final static String cuadro207 = "C207";
	final static String cuadro208 = "C208";
	final static String cuadro209 = "C209";
	final static String URL_DESCARGA = "/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/reporteDetallado_";

	@EJB
	private EnvioDocumentosFacade envioFacade;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");

		String tipoCedula = request.getParameter("tipced");
		String codUgel = request.getParameter("codugel");// 1693910
		InputStream is = getClass()
				.getResourceAsStream(URL_DESCARGA + tipoCedula.toUpperCase() + "_" + PERIODO + ".xls");

		if (is != null) {
			HSSFWorkbook hssfw = new HSSFWorkbook(is);
			if (tipoCedula.equals("1A")) {
				llenadoCedula1A(hssfw, codUgel);
			} else if (tipoCedula.equals("2A")) {
				llenadoCedula2A(hssfw, codUgel);
			} else if (tipoCedula.equals("3AP")) {
				llenadoCedula3AP(hssfw, codUgel);
			} else if (tipoCedula.equals("3AS")) {
				llenadoCedula3AS(hssfw, codUgel);
			} else if (tipoCedula.equals("4AI")) {
				llenadoCedula4AI(hssfw, codUgel);
			} else if (tipoCedula.equals("4AA")) {
				llenadoCedula4AA(hssfw, codUgel);
			} else if (tipoCedula.equals("5A")) {
				llenadoCedula5A(hssfw, codUgel);
			} else if (tipoCedula.equals("6A")) {
				llenadoCedula6A(hssfw, codUgel);
			} else if (tipoCedula.equals("7A")) {
				llenadoCedula7A(hssfw, codUgel);
			} else if (tipoCedula.equals("8AI")) {
				llenadoCedula8AI(hssfw, codUgel);
			} else if (tipoCedula.equals("8AP")) {
				llenadoCedula8AP(hssfw, codUgel);
			} else if (tipoCedula.equals("9A")) {
				llenadoCedula9A(hssfw, codUgel);
			}
			response.setContentType("application/octet-stream");

			response.setHeader("Content-Disposition",
					String.format("attachment;filename=\"reporteDetalllado_%s_2017.xls\"", tipoCedula));
			OutputStream os = response.getOutputStream();
			try {
				hssfw.write(os);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} finally {
				os.close();
			}
		}

	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	public void llenadoCedula1A(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet202 = hssfw.getSheet("C202");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet202, "D", 3, codUgel);
		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_1A);
		List<Object[]> list202 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro202, NROCEDULA_1A);
		this.llenar201_1A(list201, sheet201);
		this.llenar202_1A(list202, sheet202);
	}

	public void llenadoCedula2A(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet202 = hssfw.getSheet("C202");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet202, "D", 3, codUgel);

		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_2A);
		List<Object[]> list202 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro202, NROCEDULA_2A);

		this.llenar201_1A(list201, sheet201);
		this.llenar202_1A(list202, sheet202);

	}

	public void llenadoCedula3AP(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet204 = hssfw.getSheet("C204");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet204, "D", 3, codUgel);

		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_3AP);
		List<Object[]> list204 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro204, NROCEDULA_3AP);

		this.llenarTablaMatricula(list201, sheet201, 15, 11);
		this.llenar204_3AP(list204, sheet204);
	}

	public void llenadoCedula3AS(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet206 = hssfw.getSheet("C206");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet206, "D", 3, codUgel);

		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_3AS);
		List<Object[]> list206 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro206, NROCEDULA_3AS);

		this.llenarTablaMatricula(list201, sheet201, 15, 11);
		this.llenar206_3AS(list206, sheet206);
	}

	public void llenadoCedula4AI(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet202 = hssfw.getSheet("C202");
		HSSFSheet sheet208 = hssfw.getSheet("C208");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet202, "D", 3, codUgel);
		setCell(sheet208, "D", 3, codUgel);

		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_4AI);
		List<Object[]> list202 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro202, NROCEDULA_4AI);
		List<Object[]> list208 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro208, NROCEDULA_4AI);

		this.llenarTablaMatricula(list201, sheet201, 23, 11);
		this.llenarTablaMatricula(list202, sheet202, 13, 11);
		this.llenarTablaMatricula(list208, sheet208, 16, 11);
	}

	public void llenadoCedula4AA(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet202 = hssfw.getSheet("C202");
		HSSFSheet sheet209 = hssfw.getSheet("C209");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet202, "D", 3, codUgel);
		setCell(sheet209, "D", 3, codUgel);

		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_4AA);
		List<Object[]> list202 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro202, NROCEDULA_4AA);
		List<Object[]> list209 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro209, NROCEDULA_4AA);

		this.llenarTablaMatricula(list201, sheet201, 19, 11);
		this.llenarTablaMatricula(list202, sheet202, 19, 11);
		this.llenarTablaMatricula(list209, sheet209, 11, 11);
	}

	public void llenadoCedula5A(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet206 = hssfw.getSheet("C206");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet206, "D", 3, codUgel);
		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_5A);
		List<Object[]> list206 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro206, NROCEDULA_5A);
		this.llenarTablaMatricula(list201, sheet201, 23, 11);
		this.llenarTablaMatricula(list206, sheet206, 5, 11);
	}

	public void llenadoCedula6A(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet209 = hssfw.getSheet("C209");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet209, "D", 3, codUgel);
		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_6A);
		List<Object[]> list209 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro209, NROCEDULA_6A);
		this.llenarTablaMatricula(list201, sheet201, 19, 11);
		this.llenarTablaMatricula(list209, sheet209, 5, 11);
	}

	public void llenadoCedula7A(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet202 = hssfw.getSheet("C202");
		HSSFSheet sheet209 = hssfw.getSheet("C209");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet202, "D", 3, codUgel);
		setCell(sheet209, "D", 3, codUgel);
		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_7A);
		List<Object[]> list202 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro202, NROCEDULA_7A);
		List<Object[]> list209 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro209, NROCEDULA_7A);
		this.llenarTablaMatricula(list201, sheet201, 23, 11);
		this.llenarTablaMatricula(list202, sheet202, 23, 11);
		this.llenarTablaMatricula(list209, sheet209, 7, 11);
	}

	public void llenadoCedula8AI(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet202 = hssfw.getSheet("C202");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet202, "D", 3, codUgel);
		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_8AI);
		List<Object[]> list202 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro202, NROCEDULA_8AI);
		this.llenarTablaMatricula(list201, sheet201, 19, 11);
		this.llenarTablaMatricula(list202, sheet202, 22, 11);
	}

	public void llenadoCedula8AP(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet202 = hssfw.getSheet("C202");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet202, "D", 3, codUgel);
		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_8AP);
		List<Object[]> list202 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro202, NROCEDULA_8AP);
		this.llenarTablaMatricula(list201, sheet201, 17, 11);
		this.llenarTablaMatricula(list202, sheet202, 22, 11);
	}

	public void llenadoCedula9A(HSSFWorkbook hssfw, String codUgel) {
		HSSFSheet sheet201 = hssfw.getSheet("C201");
		HSSFSheet sheet204 = hssfw.getSheet("C204");
		setCell(sheet201, "D", 3, codUgel);
		setCell(sheet204, "D", 3, codUgel);
		List<Object[]> list201 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro201, NROCEDULA_9A);
		List<Object[]> list204 = envioFacade.reporteDetallado2017ByUgel(codUgel, cuadro204, NROCEDULA_9A);
		this.llenarTablaMatricula(list201, sheet201, 7, 11);
		this.llenarTablaMatricula(list204, sheet204, 7, 11);
	}

	private void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	private void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

		HSSFRow row = sheet.getRow(fil - 1);
		if (row != null) {
			try {
				row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
			} catch (java.lang.NullPointerException e) {
				logger.error(String.format("Error de dato nulo detalle: {0}", new Object[] { e.getMessage() }));
			}
		}
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;
		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		return iCol;
	}

	public void llenar201_1A(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 16;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 10;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "B";
				break;
			case 2:
				nomCol = "C";
				break;
			case 3:
				nomCol = "D";
				break;
			case 4:
				nomCol = "E";
				break;
			case 5:
				nomCol = "F";
				break;
			case 6:
				nomCol = "G";
				break;
			case 7:
				nomCol = "H";
				break;
			case 8:
				nomCol = "I";
				break;
			case 9:
				nomCol = "J";
				break;
			case 10:
				nomCol = "K";
				break;
			case 11:
				nomCol = "L";
				break;
			case 12:
				nomCol = "M";
				break;
			case 13:
				nomCol = "N";
				break;
			case 14:
				nomCol = "O";
				break;
			case 15:
				nomCol = "P";
				break;
			case 16:
				nomCol = "Q";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					Object[] elemento = lista.get(j - 1);
					// System.out.println(((elemento)[i-1]).getClass().toString());
					if ((elemento)[i - 1] != null) {
						if (((elemento)[i - 1]).getClass().isAssignableFrom(String.class)) {
							String valors = ((String) (elemento)[i - 1]).toString();
							if (valors != null) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valors);
							}
						} else if (((elemento)[i - 1]).getClass().isAssignableFrom(BigDecimal.class)
								|| ((elemento)[i - 1]).getClass().isAssignableFrom(Integer.class)) {
							int valor = ((Number) (elemento)[i - 1]).intValue();
							if (valor > 0) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valor);
							}
						}
					}
				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "202", e.getMessage() }));
					return;
				}
			}
		}
	}

	public void llenar202_1A(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 12;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 10;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "B";
				break;
			case 2:
				nomCol = "C";
				break;
			case 3:
				nomCol = "G";
				break;
			case 4:
				nomCol = "H";
				break;
			case 5:
				nomCol = "I";
				break;
			case 6:
				nomCol = "J";
				break;
			case 7:
				nomCol = "K";
				break;
			case 8:
				nomCol = "L";
				break;
			case 9:
				nomCol = "M";
				break;
			case 10:
				nomCol = "N";
				break;
			case 11:
				nomCol = "O";
				break;
			case 12:
				nomCol = "P";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					Object[] elemento = lista.get(j - 1);
					// System.out.println(((elemento)[i-1]).getClass().toString());
					if ((elemento)[i - 1] != null) {
						if (((elemento)[i - 1]).getClass().isAssignableFrom(String.class)) {
							String valors = ((String) (elemento)[i - 1]).toString();
							setCell(sheet, nomCol, rowExcEmpieza + j, valors);
							// if (valors != null ) {
							// setCell(sheet, nomCol, rowExcEmpieza + 1, valors);
							// }
						} else if (((elemento)[i - 1]).getClass().isAssignableFrom(BigDecimal.class)
								|| ((elemento)[i - 1]).getClass().isAssignableFrom(Integer.class)) {
							int valor = ((Number) (elemento)[i - 1]).intValue();
							setCell(sheet, nomCol, rowExcEmpieza + j, valor);
							// if (valor > 0) {
							// setCell(sheet, nomCol, rowExcEmpieza + 1, valor);
							// }
						}
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "202", e.getMessage() }));
					return;
				}
			}
		}
	}

	public void llenar204_3AP(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 15;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 10;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "B";
				break;
			case 2:
				nomCol = "D";
				break;
			case 3:
				nomCol = "E";
				break;
			case 4:
				nomCol = "F";
				break;
			case 5:
				nomCol = "G";
				break;
			case 6:
				nomCol = "H";
				break;
			case 7:
				nomCol = "I";
				break;
			case 8:
				nomCol = "J";
				break;
			case 9:
				nomCol = "K";
				break;
			case 10:
				nomCol = "L";
				break;
			case 11:
				nomCol = "M";
				break;
			case 12:
				nomCol = "N";
				break;
			case 13:
				nomCol = "O";
				break;
			case 14:
				nomCol = "P";
				break;
			case 15:
				nomCol = "Q";
				break;
			case 16:
				nomCol = "R";
				break;
			case 17:
				nomCol = "S";
				break;
			case 18:
				nomCol = "T";
				break;
			case 19:
				nomCol = "U";
				break;
			case 20:
				nomCol = "V";
				break;
			case 21:
				nomCol = "W";
				break;
			case 22:
				nomCol = "X";
				break;
			case 23:
				nomCol = "Y";
				break;
			case 24:
				nomCol = "Z";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					Object[] elemento = lista.get(j - 1);
					if ((elemento)[i - 1] != null) {
						// System.out.println(((elemento)[i-1]).getClass().toString());
						if (((elemento)[i - 1]).getClass().isAssignableFrom(String.class)) {
							String valors = ((String) (elemento)[i - 1]).toString();
							if (valors != null) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valors);
							}
						} else if (((elemento)[i - 1]).getClass().isAssignableFrom(BigDecimal.class)
								|| ((elemento)[i - 1]).getClass().isAssignableFrom(Integer.class)) {
							int valor = ((Number) (elemento)[i - 1]).intValue();
							if (valor > 0) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valor);
							}
						}
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "202", e.getMessage() }));
					return;
				}
			}
		}
	}

	public void llenar206_3AS(List<Object[]> lista, HSSFSheet sheet) {
		if (lista.isEmpty()) {
			logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = 14;
		int rowExcCant = lista.size();
		int rowExcEmpieza = 10;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "B";
				break;
			case 2:
				nomCol = "C";
				break;
			case 3:
				nomCol = "G";
				break;
			case 4:
				nomCol = "H";
				break;
			case 5:
				nomCol = "I";
				break;
			case 6:
				nomCol = "J";
				break;
			case 7:
				nomCol = "K";
				break;
			case 8:
				nomCol = "L";
				break;
			case 9:
				nomCol = "M";
				break;
			case 10:
				nomCol = "N";
				break;
			case 11:
				nomCol = "O";
				break;
			case 12:
				nomCol = "P";
				break;
			case 13:
				nomCol = "Q";
				break;
			case 14:
				nomCol = "R";
				break;
			case 15:
				nomCol = "S";
				break;
			case 16:
				nomCol = "T";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					Object[] elemento = lista.get(j - 1);
					if ((elemento)[i - 1] != null) {
						// System.out.println(((elemento)[i-1]).getClass().toString());
						if (((elemento)[i - 1]).getClass().isAssignableFrom(String.class)) {
							String valors = ((String) (elemento)[i - 1]).toString();
							if (valors != null) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valors);
							}
						} else if (((elemento)[i - 1]).getClass().isAssignableFrom(BigDecimal.class)
								|| ((elemento)[i - 1]).getClass().isAssignableFrom(Integer.class)) {
							int valor = ((Number) (elemento)[i - 1]).intValue();
							if (valor > 0) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valor);
							}
						}
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "202", e.getMessage() }));
					return;
				}
			}
		}
	}

	public void llenarTablaMatricula(List<Object[]> lista, HSSFSheet sheet, int cantidadColumnasTabla,
			int nroFilaComienzaTabla) {
		if (lista.isEmpty()) {
			logger.error("********** La lista del prellenado para la tabla 201 de la cedula 1A esta vacia ***********");
			return;
		}
		int totH = 0;
		int totM = 0;

		int colExcCant = cantidadColumnasTabla;
		int rowExcCant = lista.size();
		int rowExcEmpieza = nroFilaComienzaTabla - 1;

		String nomCol = "";
		for (int i = 1; i <= colExcCant; i++) {
			switch (i) {
			case 1:
				nomCol = "B";
				break;
			case 2:
				nomCol = "D";
				break;
			case 3:
				nomCol = "E";
				break;
			case 4:
				nomCol = "F";
				break;
			case 5:
				nomCol = "G";
				break;
			case 6:
				nomCol = "H";
				break;
			case 7:
				nomCol = "I";
				break;
			case 8:
				nomCol = "J";
				break;
			case 9:
				nomCol = "K";
				break;
			case 10:
				nomCol = "L";
				break;
			case 11:
				nomCol = "M";
				break;
			case 12:
				nomCol = "N";
				break;
			case 13:
				nomCol = "O";
				break;
			case 14:
				nomCol = "P";
				break;
			case 15:
				nomCol = "Q";
				break;
			case 16:
				nomCol = "R";
				break;
			case 17:
				nomCol = "S";
				break;
			case 18:
				nomCol = "T";
				break;
			case 19:
				nomCol = "U";
				break;
			case 20:
				nomCol = "V";
				break;
			case 21:
				nomCol = "W";
				break;
			case 22:
				nomCol = "X";
				break;
			case 23:
				nomCol = "Y";
				break;
			case 24:
				nomCol = "Z";
				break;
			}
			for (int j = 1; j <= rowExcCant; j++) {
				try {
					Object[] elemento = lista.get(j - 1);
					// System.out.println(((elemento)[i-1]).getClass().toString());
					if ((elemento)[i - 1] != null) {
						if (((elemento)[i - 1]).getClass().isAssignableFrom(String.class)) {
							String valors = ((String) (elemento)[i - 1]).toString();
							if (valors != null) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valors);
							}
						} else if (((elemento)[i - 1]).getClass().isAssignableFrom(BigDecimal.class)
								|| ((elemento)[i - 1]).getClass().isAssignableFrom(Integer.class)) {
							int valor = ((Number) (elemento)[i - 1]).intValue();
							if (valor > 0) {
								setCell(sheet, nomCol, rowExcEmpieza + j, valor);
							}
						}
					}

				} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					logger.error(String.format("Error indice de array para el tablero {0} , detalle: {1}",
							new Object[] { "202", e.getMessage() }));
					return;
				}
			}
		}
	}
}
