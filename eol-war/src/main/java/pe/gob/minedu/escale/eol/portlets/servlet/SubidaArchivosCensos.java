/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import com.lowagie.text.pdf.codec.Base64.InputStream;

/**
 *
 * @author IMENDOZA
 */
@WebServlet("/SubidaArchivosCensos")
public class SubidaArchivosCensos extends HttpServlet {
   
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(SubidaArchivosCensos.class);

	/** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubidaArchivosCensos</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubidaArchivosCensos at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @SuppressWarnings("unused")
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();

        String msg = "";
        String filename = "";
        int state = 0;
        String json = "";
        Long sizeFile = 3145728L;
        boolean save = true;
        String nombreArchivo = null;
        String nombreCarpeta = null;
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
        	
        	MessageResources res = MessageResources.getMessageResources("eol");
        	String rutaRaiz = res.getMessage("eol.path.templates.xls");
        	
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            //Properties propiedades = new Properties();
            String pathFile = "";
            InputStream entrada = null;
            try {
                //propiedades.load( getClass().getResourceAsStream("config.properties") );
                //String pathFile = request.getServletContext().getRealPath("");
                //String x = "WEB-INF\\classes\\pe\\gob\\minedu\\escale\\eol\\portlets\\plantilla\\censo2016";
                
            	List<FileItem> multiparts = upload.parseRequest(request);
                for (FileItem item : multiparts) {
                    if (item.isFormField()) {
                        if(item.getFieldName().equals("nombreArchivo")) {
                            nombreArchivo = item.getString();
                        }else if(item.getFieldName().equals("carpeta")){
                            nombreCarpeta = item.getString();
                        }
                    }
                }
                pathFile = rutaRaiz  + "censo2019" + File.separator + nombreCarpeta+ File.separator + nombreArchivo; // El archivo debe subirse con la extension
                logger.info("ruta archivo = "+pathFile);
                                    //nombreCarpeta +  File.separator + nombreArchivo + ".xls";
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        if(item.getSize() > sizeFile){
                            state = 0;
                            save = false;
                            msg = "El tamaño del archivo excede el limite permitido.";
                            break;
                        }else if (save){
                            String path = pathFile;

                            item.write(new File(path));
                            filename = nombreArchivo;
                            msg = "El archivo se inserto correctamente.";
                            state = 1;
                        }
                    }
                }
            } catch (Exception e) {
                state = 0;
                save = false;
                msg = "Ocurrio un error al intentar guardar el archivo.";
                e.printStackTrace();
            }finally{
                if (entrada != null) {
                    try {
                        entrada.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        request.setAttribute("message", msg);
        request.setAttribute("state", state);
        request.getRequestDispatcher("/gestionArchivos.uee").forward(request, response);
    }
        
    


    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
