package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2016;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.gob.minedu.escale.eol.estadistica.ejb.PrecargaCedulaIDFacade;
import pe.gob.minedu.escale.eol.padron.domain.CentroEducativo;
import pe.gob.minedu.escale.eol.padron.ejb.PadronLocal;

public class CedulaXlsID2016Servlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(CedulaXlsID2016Servlet.class);
	
	static final String CEDULA_MATRICULA_DESC = "IdentificacionIECedID_";
	static final String PERIODO = "2016";
	static final String PATH_DIR = "2016/cedulas";

	@EJB
	private PadronLocal padronFacade = lookup("java:global/eol-war/PadronFacade!pe.gob.minedu.escale.eol.padron.ejb.PadronLocal");
	
	@EJB
	private PrecargaCedulaIDFacade padroncedulaIDFacade;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info(":: CedulaXlsID2016Servlet.processRequest :: Starting execution...");
		String preCed = "";
		String nivMod = request.getParameter("nivel");
		String anexo = request.getParameter("anexo");
		String codMod = request.getParameter("codmod");
		String codied = request.getParameter("codinst");
		// codied = "IE046830";
		logger.info("********** DESCARGA  {0}***********" + codMod);
		InputStream is = getClass().getResourceAsStream(
				"/pe/gob/minedu/escale/eol/portlets/plantilla/censo2016/matricula/IdentificacionIECedID_2016.xls");
		CentroEducativo ie = padronFacade.obtainByCodMod(codMod, anexo);

		if (is != null) {
			HSSFWorkbook hssfw = new HSSFWorkbook(is);

			llenadoCedulaID(hssfw, ie, codied);

			response.setContentType("application/octet-stream");

			response.setHeader("Content-Disposition",
					String.format("attachment;filename=\"IdentificacionIECedID_%s.xls\"", new Object[] { "2016" }));
			OutputStream os = response.getOutputStream();
			try {
				hssfw.write(os);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			os.close();
		}

		/*
		 * String preCed = ""; String nivMod = request.getParameter("nivel"); String
		 * anexo = request.getParameter("anexo"); String codMod =
		 * request.getParameter("codmod"); LOGGER.log(Level.WARNING,
		 * "********** DESCARGA  {0}***********", codMod); InputStream is =
		 * getClass().getResourceAsStream(
		 * "/pe/gob/minedu/escale/eol/portlets/plantilla/censo2016/matricula/IdentificacionIECedID_2016.xls"
		 * ); if (is != null) { HSSFWorkbook hssfw = new HSSFWorkbook(is);
		 * response.setContentType("application/octet-stream");
		 * 
		 * response.setHeader("Content-Disposition",
		 * String.format("attachment;filename=\"IdentificacionIECedID_%s.xls\"", new
		 * Object[] { "2016" })); OutputStream os = response.getOutputStream(); try {
		 * hssfw.write(os); } catch (IOException ioe) { ioe.printStackTrace(); }
		 * os.close(); }
		 */
		logger.info(":: CedulaXlsID2016Servlet.processRequest :: Execution finish.");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}

	private void llenadoCedulaID(HSSFWorkbook hssfw, CentroEducativo ie, String codied) {
		Object[] cabeceraID = padroncedulaIDFacade.getPrellenadoCabeceraCedulaID(codied);

		HSSFSheet sheetC100 = hssfw.getSheet("C100");
		setCell(sheetC100, "J", 7, codied);
		setCell(sheetC100, "A", 8, ie.getCodMod());

		if (cabeceraID != null) {
			List<Object[]> listaDetID = padroncedulaIDFacade.getPrellenadoDetalleCedulaID(codied);

			/****** seccion C100 *******/

			// setCell(sheetC100, "D", 29, ie.getCenEdu());//Nombre
			setCell(sheetC100, "D", 29, cabeceraID[2].toString());
			setCell(sheetC100,
					cabeceraID[3].toString().equals("1") ? "D" : (cabeceraID[3].toString().equals("2") ? "I" : "N"), 31,
					"X");
			setCell(sheetC100, "D", 33, cabeceraID[5] != null ? cabeceraID[5].toString() : "");
			setCell(sheetC100, "D", 35, cabeceraID[6] != null ? cabeceraID[6].toString() : "");
			setCell(sheetC100, "D", 37, cabeceraID[7] != null ? cabeceraID[7].toString() : "");
			setCell(sheetC100, "Q", 37, cabeceraID[8] != null ? cabeceraID[8].toString() : "");
			setCell(sheetC100, "D", 41, cabeceraID[9] != null ? cabeceraID[9].toString() : "");
			setCell(sheetC100, "J", 41, cabeceraID[10] != null ? cabeceraID[10].toString() : "");
			setCell(sheetC100, "O", 41, cabeceraID[11] != null ? cabeceraID[11].toString() : "");
			setCell(sheetC100, "D", 44, cabeceraID[12] != null ? cabeceraID[12].toString() : "");
			setCell(sheetC100, "D", 50, cabeceraID[13] != null ? cabeceraID[13].toString() : "");
			setCell(sheetC100, "D", 52, cabeceraID[14] != null ? cabeceraID[14].toString() : "");
			setCell(sheetC100, "K", 52, cabeceraID[15] != null ? cabeceraID[15].toString() : "");

			if (listaDetID != null) {
				int cuadro = Integer.parseInt(String.valueOf(cabeceraID[4]));
				switch (cuadro) {
				case 201:
					HSSFSheet sheetC210 = hssfw.getSheet("C210");
					llenadoC210(sheetC210, listaDetID);
					setCell(sheetC100, "A", 1, "1");
					break;
				case 202:
					HSSFSheet sheetC220 = hssfw.getSheet("C220");
					llenadoC220(sheetC220, listaDetID);
					setCell(sheetC100, "A", 1, "2");
					break;
				default:
					HSSFSheet sheetC230 = hssfw.getSheet("C230");
					llenadoC230(sheetC230, listaDetID);
					setCell(sheetC100, "A", 1, "3");
					break;
				}
			}
		}

	}

	private void llenadoC210(HSSFSheet sheetC210, List<Object[]> listaDetID) {

		int itemIni = 12;

		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);

			setCell(sheetC210, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC210, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC210, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC210, "H", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC210, "I", itemIni, object[7] != null ? object[7].toString() : "");
			setCell(sheetC210, "J", itemIni, object[8] != null ? object[8].toString() : "");
			setCell(sheetC210, "L", itemIni, object[9] != null ? object[9].toString() : "");
			setCell(sheetC210, "R", itemIni, object[10] != null ? object[10].toString() : "");
			setCell(sheetC210, "W", itemIni, object[11] != null ? object[11].toString() : "");
			setCell(sheetC210, "AB", itemIni, object[12] != null ? object[12].toString() : "");
			setCell(sheetC210, "AF", itemIni, object[13] != null ? object[13].toString() : "");
			setCell(sheetC210, "AG", itemIni, object[14] != null ? object[14].toString() : "");

			itemIni = itemIni + 3;
		}
	}

	private void llenadoC220(HSSFSheet sheetC220, List<Object[]> listaDetID) {

		int itemIni = 11;

		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);

			setCell(sheetC220, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC220, "J", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC220, "K", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC220, "M", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC220, "N", itemIni, object[7] != null ? object[7].toString() : "");

			setCell(sheetC220, "O", itemIni, object[15] != null ? object[15].toString() : "");
			setCell(sheetC220, "V", itemIni, object[16] != null ? object[16].toString() : "");
			setCell(sheetC220, "AC", itemIni, object[17] != null ? object[17].toString() : "");
			setCell(sheetC220, "AJ", itemIni, object[14] != null ? object[14].toString() : "");

			itemIni = itemIni + 2;
		}

	}

	private void llenadoC230(HSSFSheet sheetC230, List<Object[]> listaDetID) {

		int itemIni = 11;

		for (int i = 0; i < listaDetID.size(); i++) {
			Object[] object = listaDetID.get(i);

			setCell(sheetC230, "D", itemIni, object[3] != null ? object[3].toString() : "");
			setCell(sheetC230, "E", itemIni, object[4] != null ? object[4].toString() : "");
			setCell(sheetC230, "F", itemIni, object[5] != null ? object[5].toString() : "");
			setCell(sheetC230, "H", itemIni, object[6] != null ? object[6].toString() : "");
			setCell(sheetC230, "I", itemIni, object[7] != null ? object[7].toString() : "");

			setCell(sheetC230, "J", itemIni, object[14] != null ? object[14].toString() : "");

			itemIni = itemIni + 3;
		}

	}

	static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(letter2col(col));
		cell.setCellValue(texto);
	}

	static void setCell(HSSFSheet sheet, int col, int fil, String texto) {
		HSSFRow row = sheet.getRow(fil - 1);
		HSSFCell cell = row.getCell(col);
		cell.setCellValue(texto);
	}

	static int letter2col(String col) {
		int iCol = 0;
		short delta = 'Z' - 'A' + 1;

		if (col.length() < 2) {
			return (col.charAt(0) - 'A');
		}
		iCol = col.charAt(1) - 'A';
		iCol += (col.charAt(0) - 'A' + 1) * delta;
		/*
		 * for (int i = col.length(); i > 0; i--) { char chr = col.charAt(i - 1); short
		 * iChr = (short) (chr - 'A'); iCol += (iChr * (col.length() - i + 1) * delta);
		 * }
		 */
		return iCol;
	}

	private <T> T lookup(String nameContextEjb) {
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}
}
