/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.domain.act;

/**
 *
 * @author JMATAMOROS
 */
public class ResumenCobertura {

    private String idUgel;
    private int cuentaCentros;
    private int cuentaEnvios;
    private int cuentaEnviosPost;

    public ResumenCobertura(String idUgel, int cuentaCentros) {
        this.idUgel = idUgel;
        this.cuentaCentros = cuentaCentros;
    }

    public ResumenCobertura(String idUgel, int cuentaCentros, int cuentaEnvios) {
        this.idUgel = idUgel;
        this.cuentaCentros = cuentaCentros;
        this.cuentaEnvios = cuentaEnvios;
    }

    public ResumenCobertura(String idUgel, int cuentaCentros, int cuentaEnvios, int cuentaEnviosPost) {
        this.idUgel = idUgel;
        this.cuentaCentros = cuentaCentros;
        this.cuentaEnvios = cuentaEnvios;
        this.cuentaEnviosPost = cuentaEnviosPost;
    }

    public int getCuentaCentros() {
        return cuentaCentros;
    }

    public void setCuentaCentros(int cuentaCentros) {
        this.cuentaCentros = cuentaCentros;
    }



    public String getIdUgel() {
        return idUgel;
    }

    public void setIdUgel(String idUgel) {
        this.idUgel = idUgel;
    }

    public double getPorcentaje() {
        return ((cuentaEnvios * 1.0) / (cuentaCentros * 1.0))*100;
    }

    /**
     * @return the cuentaEnvios
     */
    public int getCuentaEnvios() {
        return cuentaEnvios;
    }

    /**
     * @param cuentaEnvios the cuentaEnvios to set
     */
    public void setCuentaEnvios(int cuentaEnvios) {
        this.cuentaEnvios = cuentaEnvios;
    }

    public int getCuentaEnviosPost() {
        return cuentaEnviosPost;
    }

    public void setCuentaEnviosPost(int cuentaEnviosPost) {
        this.cuentaEnviosPost = cuentaEnviosPost;
    }
}
