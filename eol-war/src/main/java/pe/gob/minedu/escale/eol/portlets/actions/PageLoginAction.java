package pe.gob.minedu.escale.eol.portlets.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PageLoginAction extends Action {

	private Logger logger = Logger.getLogger(PageLoginAction.class);

	public PageLoginAction() {
		logger.info("iniciando LogonAction");
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info(":: PageLoginAction.execute :: Starting execution...");
		HttpSession session = request.getSession();
		logger.info("session: " + session);
		logger.info(":: PageLoginAction.execute :: Execution finish");
		return mapping.findForward("success");
	}
}
