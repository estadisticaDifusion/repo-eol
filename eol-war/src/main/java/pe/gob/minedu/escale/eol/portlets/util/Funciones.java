/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

/**
 *
 * @author jmatamoros
 */
public abstract class Funciones {
    static final Logger LOGGER = Logger.getLogger(Funciones.class.getName());
    public static String cedulaMatricula2011NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0") || nivMod.equals("F0")) {
            preCed = "3A";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0")) {
            preCed = "8A";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }
    public static String cedulaMatricula2012NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0") || nivMod.equals("F0")) {
            preCed = "3A";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0")) {
            preCed = "8A";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }

     public static String cedulaMatricula2013NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0") || nivMod.equals("F0")) {
            preCed = "3A";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0")) {
            preCed = "8A";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }
    
    public static String cedulaMatricula2014NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0") || nivMod.equals("F0")) {
            preCed = "3A";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("D1")) {
            preCed = "4AI";
        }
        if (nivMod.equals("D2")) {
            preCed = "4AA";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0")) {
            preCed = "8A";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    } 

    public static String cedulaMatricula2017NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0")) {
            preCed = "3AP";
        }
        if (nivMod.equals("F0")) {
            preCed = "3AS";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("D1")) {
            preCed = "4AI";
        }
        if (nivMod.equals("D2")) {
            preCed = "4AA";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0") || nivMod.equals("E1")) {
            preCed = "8AI";
        }
        if (nivMod.equals("E2")) {
            preCed = "8AP";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }

    public static String cedulaMatricula2018NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0")) {
            preCed = "3AP";
        }
        if (nivMod.equals("F0")) {
            preCed = "3AS";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("D1")) {
            preCed = "4AI";
        }
        if (nivMod.equals("D2")) {
            preCed = "4AA";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0") || nivMod.equals("E1")) {
            preCed = "8AI";
        }
        if (nivMod.equals("E2")) {
            preCed = "8AP";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }
    
    public static String cedulaMatricula2019NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0")) {
            preCed = "3AP";
        }
        if (nivMod.equals("F0")) {
            preCed = "3AS";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("D1")) {
            preCed = "4AI";
        }
        if (nivMod.equals("D2")) {
            preCed = "4AA";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0") || nivMod.equals("E1")) {
            preCed = "8AI";
        }
        if (nivMod.equals("E2")) {
            preCed = "8AP";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }
    
    public static String cedulaMatricula2015NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0") || nivMod.equals("F0")) {
            preCed = "3A";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0")) {
            preCed = "8A";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }

     public static String cedulaMatricula2016NivMod(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1A";
        } else if (nivMod.equals("A4")) {
            preCed = "1A";
        }
        if (nivMod.equals("A5")) {
            preCed = "2A";
        }
        if (nivMod.equals("B0") || nivMod.equals("F0")) {
            preCed = "3A";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0")) {
            preCed = "4A";
        }
        if (nivMod.equals("D1")) {
            preCed = "4AI";
        }
        if (nivMod.equals("D2")) {
            preCed = "4AA";
        }
        if (nivMod.equals("K0")) {
            preCed = "5A";
        }
        if (nivMod.equals("T0")) {
            preCed = "6A";
        }
        if (nivMod.equals("M0")) {
            preCed = "7A";
        }
        if (nivMod.equals("E0")) {
            preCed = "8A";
        }
        if (nivMod.startsWith("L")) {
            preCed = "9A";
        }
        return preCed;
    }

    public static String cedulaNivModResultado(String nivMod) {
        String preCed = "";
        if (nivMod == null) {
            return "";
        }

        if (nivMod.equals("A1") || nivMod.equals("A2") || nivMod.equals("A3")) {
            preCed = "1b";
        } else if (nivMod.equals("A4")) {
            preCed = "1b";
        }
        if (nivMod.equals("A5")) {
            preCed = "2b";
        }
        if (nivMod.equals("B0")) {
            preCed = "3bp";
        }
        if (nivMod.equals("F0")) {
            
        	preCed = "3bs";
        }
        if (nivMod.equals("G0") || nivMod.equals("C0") || nivMod.equals("D0") || nivMod.equals("D1")) {
            preCed = "4bi";
        }
        if (nivMod.equals("D2")) {
            preCed = "4ba";
        }
        if (nivMod.equals("K0")) {
            preCed = "5b";
        }
        if (nivMod.equals("T0")) {
            preCed = "6b";
        }
        if (nivMod.equals("M0")) {
            preCed = "7b";
        }
        if (nivMod.equals("E0") || nivMod.equals("E1")) {
            preCed = "8bi";
        }
        if (nivMod.equals("E2") ) {
            preCed = "8bp";
        }
        if (nivMod.equals("L0")) {
            preCed = "9b";
        }
        return preCed;
    }

    public static String getFecha(Date date, String format) {        
        //SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formato = new SimpleDateFormat(format);
        return formato.format(date);
    }
    public static boolean isNumeri(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String getNumFormato(Integer num,String formato){
        DecimalFormat df = new DecimalFormat(formato);
        return df.format(num);

    }
    //INICIO FUNCIONES EXCEL



    public static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
        setCell(sheet, col, fil, String.valueOf(texto));
    }

    public static void setCell(HSSFSheet sheet, int col, int fil, char texto) {
        setCell(sheet, col, fil, String.valueOf(texto));
    }
   public static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                HSSFCell cell = row.getCell(letter2col(col));
                if (cell != null) {
                    cell.setCellValue(valor == null ? 0 : valor.intValue());
                }else
                {   cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(valor.intValue());
                }

            } catch (java.lang.NullPointerException e) {
                //row.setCell(cell);
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        } else {
            try {
                row = sheet.createRow(fil - 1);
                HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_NUMERIC);
                cell.setCellValue(valor.intValue());
            } catch (java.lang.NullPointerException e) {
                //row.setCell(cell);
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        }
    }

    public static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                HSSFCell cell = row.getCell(letter2col(col));
                if (cell != null) {
                    cell.setCellValue(texto);
                } else {
                    cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
                    cell.setCellValue(new HSSFRichTextString(texto));
                }
            } catch (java.lang.NullPointerException e) {
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        } else {
            try {
                row = sheet.createRow(fil - 1);
                HSSFCell cell = row.createCell(letter2col(col), HSSFCell.CELL_TYPE_STRING);
                cell.setCellValue(new HSSFRichTextString(texto));
            } catch (java.lang.NullPointerException e) {
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }
        }
    }


    public static void setCell(HSSFSheet sheet, int col, int fil, Integer valor) {

        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                HSSFCell cell = row.getCell(col);
                if (cell != null) {
                    cell.setCellValue(valor == null ? 0 : valor.intValue());
                }else
                {   cell = row.createCell(col, HSSFCell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(valor.intValue());
                }

            } catch (java.lang.NullPointerException e) {
                //row.setCell(cell);
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        } else {
            try {
                row = sheet.createRow(fil - 1);
                HSSFCell cell = row.createCell(col, HSSFCell.CELL_TYPE_NUMERIC);
                cell.setCellValue(valor.intValue());
            } catch (java.lang.NullPointerException e) {
                //row.setCell(cell);
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        }
    }

   public static void setCell(HSSFSheet sheet, int col, int fil, String texto) {
        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                HSSFCell cell = row.getCell(col);
                if (cell != null) {
                    cell.setCellValue(texto);
                } else {
                    cell = row.createCell(col, HSSFCell.CELL_TYPE_STRING);
                    cell.setCellValue(new HSSFRichTextString(texto));
                }
            } catch (java.lang.NullPointerException e) {
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }

        } else {
            try {
                row = sheet.createRow(fil - 1);
                HSSFCell cell = row.createCell(col, HSSFCell.CELL_TYPE_STRING);
                cell.setCellValue(new HSSFRichTextString(texto));
            } catch (java.lang.NullPointerException e) {
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }
        }
    }




    public static int letter2col(String col) {
        int iCol = 0;
        short delta = 'Z' - 'A' + 1;

        if (col.length() < 2) {
            return (col.charAt(0) - 'A');
        }
        iCol = col.charAt(1) - 'A';
        iCol += (col.charAt(0) - 'A' + 1) * delta;
        /*for (int i = col.length(); i > 0; i--) {
        char chr = col.charAt(i - 1);
        short iChr = (short) (chr - 'A');
        iCol += (iChr * (col.length() - i + 1) * delta);
        }*/
        return iCol;
    }
    //FIN FUNCIONES EXCEL

}
