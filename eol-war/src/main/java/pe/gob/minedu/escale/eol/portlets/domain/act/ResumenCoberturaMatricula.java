/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.minedu.escale.eol.portlets.domain.act;

/**
 *
 * @author JMATAMOROS
 */
public class ResumenCoberturaMatricula {

    private String idUgel;
    private String descri;
    private String codigo;
    private Boolean negrita=Boolean.FALSE;
    private int cuentaCentros;
    private int cuentaMatRecProf;
    private int cuentaMatEnvioPost;

    public ResumenCoberturaMatricula(String descri,String codigo) {
        this.descri = descri;
        this.codigo=codigo;
    }
    public ResumenCoberturaMatricula(String idUgel, int cuentaCentros) {
        this.idUgel = idUgel;
        this.cuentaCentros = cuentaCentros;
    }

    public ResumenCoberturaMatricula(String idUgel, int cuentaCentros, int cuentaMatRecProf) {
        this.idUgel = idUgel;
        this.cuentaCentros = cuentaCentros;
        this.cuentaMatRecProf = cuentaMatRecProf;
    }

    public ResumenCoberturaMatricula(String idUgel, int cuentaCentros, int cuentaMatRecProf,int cuentaMatEnvioPost) {
        this.idUgel = idUgel;
        this.cuentaCentros = cuentaCentros;
        this.cuentaMatRecProf = cuentaMatRecProf;
        this.cuentaMatEnvioPost = cuentaMatEnvioPost;
    }

    public int getCuentaCentros() {
        return cuentaCentros;
    }

    public void setCuentaCentros(int cuentaCentros) {
        this.cuentaCentros = cuentaCentros;
    }



    public String getIdUgel() {
        return idUgel;
    }

    public void setIdUgel(String idUgel) {
        this.idUgel = idUgel;
    }

    public double getPorcentaje() {
        return ((getCuentaMatRecProf() * 1.0) / (cuentaCentros * 1.0))*100;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

    /**
     * @param descri the descri to set
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }

    /**
     * @return the cuentaMatRecProf
     */
    public int getCuentaMatRecProf() {
        return cuentaMatRecProf;
    }

    /**
     * @param cuentaMatRecProf the cuentaMatRecProf to set
     */
    public void setCuentaMatRecProf(int cuentaMatRecProf) {
        this.cuentaMatRecProf = cuentaMatRecProf;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the negrita
     */
    public Boolean getNegrita() {
        return negrita;
    }

    /**
     * @param negrita the negrita to set
     */
    public void setNegrita(Boolean negrita) {
        this.negrita = negrita;
    }

    public int getCuentaMatEnvioPost() {
        return cuentaMatEnvioPost;
    }

    public void setCuentaMatEnvioPost(int cuentaMatEnvioPost) {
        this.cuentaMatEnvioPost = cuentaMatEnvioPost;
    }

}
