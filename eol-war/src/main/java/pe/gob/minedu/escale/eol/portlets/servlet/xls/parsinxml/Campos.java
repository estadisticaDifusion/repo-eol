package pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import javax.xml.parsers.*;


import org.w3c.dom.*;
import org.xml.sax.*;

public class Campos {
    private Document doc;
    private static final Logger LOGGER = Logger.getLogger(Campos.class.getName());
    public Campos(String f)  {
        DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();
            doc=parser.parse(f);
        } catch (ParserConfigurationException ex) {
            LOGGER.warning(ex.getMessage());
        } catch (IOException ex) {
            LOGGER.warning(ex.getMessage());
        } catch (SAXException ex) {
            LOGGER.warning(ex.getMessage());
        }

    }

    public Campo[] getCampos() {
        List<Campo> lista = new ArrayList<Campo>();
        if(doc==null)
            return null;
        NodeList camposNL=doc.getElementsByTagName("campo");
        Campo campo = null;
        if(camposNL!=null)
        for (int i=0;i<camposNL.getLength() ;i++ ) {
            Node node=camposNL.item(i);
            if (node!=null && node instanceof Element){
                campo=new Campo();
                campo.readXML((Element)node);
                if(node.getChildNodes()!=null)
                {
                    campo.setDatos(getSubCampos(node.getChildNodes()));
                }
                lista.add(campo);
            }

        }
        Campo[] campos = lista.toArray(new Campo[0]);
        return campos;
    }
    public Campo[] getSubCampos(NodeList camposNL) {

        List<Campo> lista = new ArrayList<Campo>();
        Campo campo = null;
        for (int i=0;i<camposNL.getLength() ;i++ ) {
            Node node=camposNL.item(i);
            if (node!=null && node instanceof Element){
                campo=new Campo();
                campo.readXML((Element)node);

                lista.add(campo);
            }

        }
        Campo[] campos = lista.toArray(new Campo[0]);
        return campos;
    }

}
