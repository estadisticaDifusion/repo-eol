package pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml;

import java.io.*;
import java.util.logging.Logger;

import org.w3c.dom.*;

public class Campo implements Serializable {
    
    private static final long serialVersionUID = 6918845821752485626L;
    static final Logger LOGGER = Logger.getLogger(Campo.class.getName());
    private String propiedad;
    private String etiqueta;
    private boolean mostrar;
    private String estiloXLS = null;
    private String campo;
    private String alinear;
    private String propiedadRead;
    private boolean multiplePropiedad;
    private boolean tabla;
    private String propiedadTabla;
    private String[] propiedades;
    private int numeroPropiedad;
    private String type;
    private String sheet;
    private String celC;
    private int celF;
    private String oldValue;
    private String value;
    private Campo[] datos;
    private int longitud;

    public int getNumeroPropiedad() {
		return numeroPropiedad;
	}

	public void setNumeroPropiedad(int numeroPropiedad) {
		this.numeroPropiedad = numeroPropiedad;
	}

	public boolean isMultiplePropiedad() {
		return multiplePropiedad;
	}

	public void setMultiplePropiedad(boolean multiplePropiedad) {
		this.multiplePropiedad = multiplePropiedad;
	}

	public Campo() {
        super();
    }

    public void readXML(Element elem) {    	
        setPropiedad(elem.getAttribute("propiedad"));
        setEtiqueta(elem.getAttribute("etiqueta"));
        String aMostrar = elem.getAttribute("mostrar");
        setMostrar(aMostrar == null || "true".equals(aMostrar));
        
        setAlinear(elem.getAttribute("alinear"));
        
        setEstiloXLS(elem.getAttribute("estiloXLS"));
        setCampo(elem.getAttribute("campo"));
        setType(elem.getAttribute("type"));
        setSheet(elem.getAttribute("sheet"));
        if(elem.getAttribute("celF")!=null)
        setCelF(new Integer(elem.getAttribute("celF")).intValue());
        setCelC(elem.getAttribute("celC"));
        setValue(elem.getAttribute("value"));
        setOldValue(elem.getAttribute("oldValue"));
        if(!elem.getAttribute("longitud").isEmpty())
        setLongitud(new Integer(elem.getAttribute("longitud")).intValue());


    }

    public void setPropiedad(String propiedad) {
        if ((this.propiedad = propiedad)!=null && this.propiedad.length()>0){
        	propiedadRead="get"+propiedad.substring(0,1).toUpperCase()+propiedad.substring(1);
        }
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public void setMostrar(boolean mostrar) {
        this.mostrar = mostrar;
    }

    public void setEstiloXLS(String estiloXLS) {
        this.estiloXLS = estiloXLS;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getPropiedad() {
        return propiedad;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public boolean isMostrar() {
        return mostrar;
    }

    public String getEstiloXLS() {
        return estiloXLS;
    }

    public String getCampo() {
        return campo;
    }

	public String getPropiedadRead() {

		return propiedadRead;
	}

	public String getAlinear() {
		return alinear;
	}

	public void setAlinear(String alinear) {
		this.alinear = alinear;
	}

	public String[] getPropiedades() {
		return propiedades;
	}

	public void setPropiedades(String[] propiedades) {
		this.propiedades = propiedades;
	}

	public boolean isTabla() {
		return tabla;
	}

	public void setTabla(boolean tabla) {
		this.tabla = tabla;
	}

	public String getPropiedadTabla() {
		return propiedadTabla;
	}

	public void setPropiedadTabla(String campoMultipleFila) {
		this.propiedadTabla = campoMultipleFila;
	}

    /**
     * @return the celC
     */
    public String getCelC() {
        return celC;
    }

    /**
     * @param celC the celC to set
     */
    public void setCelC(String celC) {
        this.celC = celC;
    }

    /**
     * @return the celF
     */
    public int getCelF() {
        return celF;
    }

    /**
     * @param celF the celF to set
     */
    public void setCelF(int celF) {
        this.celF = celF;
    }

    /**
     * @return the sheet
     */
    public String getSheet() {
        return sheet;
    }

    /**
     * @param sheet the sheet to set
     */
    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the datos
     */
    public Campo[] getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(Campo[] datos) {
        this.datos = datos;
    }

    /**
     * @return the oldValue
     */
    public String getOldValue() {
        return oldValue;
    }

    /**
     * @param oldValue the oldValue to set
     */
    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    
}
