package pe.gob.minedu.escale.eol.portlets.servlet.xls.censo2017;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCell;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import org.apache.struts.util.MessageResources;
import pe.gob.minedu.escale.eol.portlets.servlet.xls.parsinxml.Campos;
import pe.gob.minedu.escale.eol.portlets.util.MensajeJS;
import pe.gob.minedu.escale.rest.client.PadronClient;
import pe.gob.minedu.escale.rest.client.domain.InstitucionEducativaIE;
import pe.gob.minedu.escale.rest.client.domain.Instituciones;
import pe.gob.minedu.escale.rest.client.domain.NivelModalidad;

public class CedulaXlsD2017Servlet extends HttpServlet {


    static final Logger LOGGER = Logger.getLogger(CedulaXlsLocal2017Servlet.class.getName());
    static final String PERIODO = "2017";
    static final String PATH_DIR = "2017/cedulas";
    static final String[] listNiv = {"A1", "A2", "A3", "B0", "F0", "D0", "K0", "T0", "M0", "E0", "L0"};
    static final String[] listEsp = {"1", "2", "3", "4", "5"};
    static final String[] listEstA = {"1", "2", "3"};
    static final String[] list402I4 = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
    static final String[] list402I5 = {"1", "2"};
    private Campos campos;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        MessageResources res = MessageResources.getMessageResources("eol");

        PadronClient pCli = new PadronClient();
        String nivMod = request.getParameter("nivel");
        String codMod = request.getParameter("codmod");
        String anexo = request.getParameter("anexo");
        String codLocal = request.getParameter("codlocal");

        Instituciones ies = pCli.get_JSON(Instituciones.class, String.format("codlocal=%s", codLocal), "estados=1");

//        InputStream is = getClass().getResourceAsStream("/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/cedula11/" + CEDULA11_DESC);

        String rutaRaiz = getServletContext().getRealPath("/WEB-INF/classes/pe/gob/minedu/escale/eol/portlets/plantilla/censo2017/matricula/CensoMatriculaDocentesRecursosCedD" + "_" + PERIODO + ".xls");
        InputStream is = new FileInputStream(rutaRaiz);
        if (is != null) {
            HSSFWorkbook hssfw = new HSSFWorkbook(is);
            if (ies.getInstitucion() == null) {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                try {
                    out.print(MensajeJS.alerta("No se encontraros Instituciones para el código de local" + codLocal));
                    out.print(MensajeJS.pagina_anterior());

                } finally {
                    out.close();
                    is.close();
                }
            } else {
                llenarSeccion104(hssfw, codMod, anexo, codLocal, nivMod, (List<InstitucionEducativaIE>) ies.getInstitucion());

                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\"", "cedulaD_2017.xls"));
                OutputStream os = response.getOutputStream();
                hssfw.write(os);
                os.close();
            }
        }
    }

    public void llenarSeccion104(HSSFWorkbook hssfw, String codMod, String anexo, String codLocal, String nivMod, List<InstitucionEducativaIE> centros) {

        Collections.sort(centros, buscarCentro);
        InstitucionEducativaIE tmpCE = new InstitucionEducativaIE();
        tmpCE.setCodigoModular(codMod);
        tmpCE.setAnexo(anexo);
        tmpCE.setNivelModalidad(new NivelModalidad());
        tmpCE.getNivelModalidad().setIdCodigo(nivMod);

        codLocal = codLocal != null ? codLocal : "";
        anexo = anexo != null ? anexo : "";
        codMod = codMod != null ? codMod : "";

        int pos = Collections.binarySearch(centros, tmpCE, buscarCentro);

        HSSFSheet sheetSEC104 = hssfw.getSheet("C100");
        setCell(sheetSEC104, "F", 10, codLocal);//Código Local
        setCell(sheetSEC104, "A", 1, codMod);//Código Modular

        //llenado de campos de validación
        setCell(sheetSEC104, "A", 2, obtenerIndicarNivel(centros,"A"));//inicial
        setCell(sheetSEC104, "A", 3, obtenerIndicarNivel(centros,"B"));//primaria
        setCell(sheetSEC104, "A", 4, obtenerIndicarNivel(centros,"F"));//secundaria
        setCell(sheetSEC104, "A", 5, obtenerIndicarNivel(centros,"E1"));//
        setCell(sheetSEC104, "A", 6, obtenerIndicarNivel(centros,"E2"));//
        
        if (pos > -1) {
            tmpCE = centros.get(pos);
            
            
            //INICIO PRIMERA FILA SECCION 104
            int filaInicio = 8;
            setCell(sheetSEC104, "J", filaInicio, tmpCE.getCodigoModular());//Código Modular
            setCell(sheetSEC104, "L", filaInicio, tmpCE.getAnexo());//Anexo
            setCell(sheetSEC104, "M", filaInicio, tmpCE.getNivelModalidad().getIdCodigo());
            setCell(sheetSEC104, "N", filaInicio, tmpCE.getCentroEducativo());
            
            //setCell(sheetSEC104, "U", filaInicio, tmpCE.getGestionIe().getIdCodigo()+" : " +tmpCE.getGestionIe().getValor());
            //FIN PRIMERA FILA SECCION 104
            centros.remove(pos);

            Set<String> nivmods = new HashSet<String>();
            nivmods.add("A1");
            nivmods.add("A2");
            nivmods.add("A3");
            nivmods.add("A4");
            nivmods.add("B0");
            nivmods.add("F0");
            nivmods.add("E1");
            nivmods.add("E2");

            for (InstitucionEducativaIE ce : centros) {

                if(ce.getNivelModalidad()!=null && nivmods.contains(ce.getNivelModalidad().getIdCodigo()))
                {   filaInicio = filaInicio + 1;
                    setCell(sheetSEC104, "J", filaInicio, ce.getCodigoModular());//Código Modular
                    setCell(sheetSEC104, "L", filaInicio, ce.getAnexo());//Anexo
                    setCell(sheetSEC104, "M", filaInicio, ce.getNivelModalidad().getIdCodigo());//Nivel de I.E
                    setCell(sheetSEC104, "N", filaInicio, ce.getCentroEducativo());//Nombre I.E
                    
                    //setCell(sheetSEC104, "U", filaInicio, ce.getGestionIe().getIdCodigo()+" : " +ce.getGestionIe().getValor());
                }
            }
        }
    }

    public void llenarCampo(HSSFWorkbook hssfw, String hoja, int rowCel, String colCel, String type, Object valor) {
        HSSFSheet sheet = hssfw.getSheet(hoja);
        if (type.equals("Integer")) {
            setCell(sheet, colCel, rowCel, (Integer) valor);
        } else if (type.equals("String")) {
            setCell(sheet, colCel, rowCel, valor.toString());
        }
    }

    public static boolean existeCampoMetodo(String nombre, Method[] metodos) {
        for (Method m : metodos) {
            if (m.getName().equals(nombre)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    static void setCell(HSSFSheet sheet, String col, int fil, char texto) {
        setCell(sheet, col, fil, String.valueOf(texto));
    }

    static void setCell(HSSFSheet sheet, String col, int fil, Integer valor) {

        HSSFRow row = sheet.getRow(fil - 1);
        if (row != null) {
            try {
                row.getCell(letter2col(col)).setCellValue(valor == null ? 0 : valor.intValue());
            } catch (java.lang.NullPointerException e) {
                LOGGER.log(Level.WARNING, String.format("Error de dato nulo detalle: {0}", new Object[]{e.getMessage()}));
            }
        }
    }

    static void setCell(HSSFSheet sheet, String col, int fil, String texto) {
        HSSFRow row = sheet.getRow(fil - 1);
        HSSFCell cell = row.getCell(letter2col(col));
        cell.setCellValue(texto);
    }

    static int letter2col(String col) {
        int iCol = 0;
        short delta = 'Z' - 'A' + 1;

        if (col.length() < 2) {
            return (col.charAt(0) - 'A');
        }
        iCol = col.charAt(1) - 'A';
        iCol += (col.charAt(0) - 'A' + 1) * delta;
        /*for (int i = col.length(); i > 0; i--) {
        char chr = col.charAt(i - 1);
        short iChr = (short) (chr - 'A');
        iCol += (iChr * (col.length() - i + 1) * delta);
        }*/
        return iCol;
    }

    public Comparator<InstitucionEducativaIE> buscarCentro = new Comparator<InstitucionEducativaIE>() {

        public int compare(InstitucionEducativaIE o1, InstitucionEducativaIE o2) {
            if ((o1.getCodigoModular()).compareTo(o2.getCodigoModular()) == 0) {
                if (o1.getAnexo().compareTo(o2.getAnexo()) == 0) {
                    return o1.getNivelModalidad().getIdCodigo().compareTo(o2.getNivelModalidad().getIdCodigo());
                } else {
                    return ((String) o1.getAnexo()).compareTo((String) o2.getAnexo());
                }
            } else {
                return o1.getCodigoModular().compareTo(o2.getCodigoModular());
            }
        }
    };

    private int obtenerIndicarNivel(List<InstitucionEducativaIE> centros,String nivel) {

        int indicador = 0;

        if(centros.size()>0){
            for (InstitucionEducativaIE ie : centros) {
                if(nivel.length() < 2){//A,B,F
                    if(nivel.equals(ie.getNivelModalidad().getIdCodigo().substring(0,1))){
                        indicador = 1;
                    }
                }else{
                    if(nivel.equals(ie.getNivelModalidad().getIdCodigo())){
                        indicador = 1;
                    }
                }
            }
        }

        return indicador;
    }
}
