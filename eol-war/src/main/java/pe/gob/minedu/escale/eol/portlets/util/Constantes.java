/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.portlets.util;

/**
 *
 * @author JMATAMOROS
 */
public class Constantes {

	public static final String URL_FORWARD = "pe.gob.minedu.escale.eol.portlets.util.Constantes.URL_FORWARD";
	public static final String LOGGED_USER_ROL = "pe.gob.minedu.escale.eol.portlets.util.Constantes.LOGGED_USER_ROL";
	public static final String ROL_NO_PERMITIDO_PATH = "/resources/error/norol.jsp";
	public static final String LOGGED_USER = "LOGGED_USER";
	public static final String LOGGED_INSTITUCION = "LOGGED_INSTITUCION";
	public static final String LOGGED_USER_PRF = "LOGGED_USER_PRF";// imendoza 20170424 - inicio
	public static final String USUARIO_UGEL = "UGEL";
	public static final String USUARIO_CE = "CE";
	public static final String USUARIO_AGP = "AGP";
	// imendoza
	public static final String USUARIO_ADMIN = "ADMIN";
	// imendoza
	public static final String USUARIO_DRE = "DRE";

	public static final String USUARIO_DRE_LIMA_METRO = "150101";
	public static final String USUARIO_DRE_LIMA_PROV = "150200";
	public static final String USUARIO_DRE_CALLAO = "070101";
	public static final String ACTIVIDAD_TIPO_ESCUELA = "1";
	public static final String ACTIVIDAD_TIPO_LOCALE = "2";

	public static final String CENSO_ANIO_INICIO = "eol.censo.anio_inicio";
	public static final String CENSO_ANIO_FINAL = "eol.censo.anio_final";
	
	public static final String PERIODO_CENSAL_ACTUAL = "2019";
	
	public static final String ANIO_2019 = "2019";

	public static final String OAUTH2_SESSION = "OAUTH2_SESSION";
	
	public static final String SECRET_ENCRYPT = "eol2018";
	
    //MHUAMANI [INICIO]
    public static final String ACTION_NAME_ERROR_USER_PASS = "login_error";
    public static final String PARAMETER_UGEL = "ugel";
    public static final String PARAMETER_CE = "ce";
    public static final String CONFIG_PATH_UGEL_ERROR = "/login/def_ugel/error";
    public static final String CONFIG_PATH_CE_ERROR = "/login/def_ce/error";
    //MHUAMANI [INICIO]
    public static final String TIMEOUT_INACTIVE = "TIMEOUT_INACTIVE";
    public static final String EXPIRATION_DATE = "EXPIRATION_DATE";
}
