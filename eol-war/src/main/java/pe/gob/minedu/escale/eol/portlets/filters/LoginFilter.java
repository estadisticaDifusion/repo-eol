package pe.gob.minedu.escale.eol.portlets.filters;

import static pe.gob.minedu.escale.eol.portlets.util.Constantes.LOGGED_USER;
import static pe.gob.minedu.escale.eol.portlets.util.Constantes.ROL_NO_PERMITIDO_PATH;
import static pe.gob.minedu.escale.eol.portlets.util.Constantes.URL_FORWARD;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.auth.domain.AuthUsuario;
import pe.gob.minedu.escale.eol.portlets.util.Constantes;

@WebFilter
public class LoginFilter implements Filter {

    private Logger logger = Logger.getLogger(LoginFilter.class);

    private FilterConfig filterConfig;

    private static String INIT_PARAMETER_ROLES_PERMITIDOS = "roles";
    private List<String> rolesPermitidos;
    private String doLogin;
    private static final String INIT_PARAMETER_LOGIN = "login";
    // Handle the passed-in FilterConfig
    /**
     * Used for Script Nonce
     */
    private SecureRandom prng = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        iniciarParametros();
    }

    private void iniciarParametros() {
        logger.info("iniciando parametros de filtro:" + filterConfig.getFilterName());
        String roles = filterConfig.getInitParameter(INIT_PARAMETER_ROLES_PERMITIDOS);
        if (roles != null) {
            logger.info("ROLES:" + roles);
            String[] aRoles = roles.split(",");
            rolesPermitidos = Arrays.asList(aRoles);
        }
        if (roles == null || rolesPermitidos == null || rolesPermitidos.isEmpty()) {
            throw new RuntimeException("No se ha configurado parametro 'roles'");
        }

        doLogin = filterConfig.getInitParameter(INIT_PARAMETER_LOGIN);
        if (doLogin == null) {
            throw new RuntimeException("No se ha configurado parametro 'login'");
        }
    }

    // Process the request/response pair
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) {

    	HttpServletRequest req = (HttpServletRequest) request;
    	
        //HttpServletRequest req = null;
        try {
            //HttpSession session = (req = (HttpServletRequest) request).getSession();
        	HttpSession session = req.getSession();
            logger.info("get sesion1: "+session);
            
            isSessionExpiredByIdletime(session);
            
            logger.info(Constantes.LOGGED_USER+": " + session.getAttribute(Constantes.LOGGED_USER));
            String url = req.getRequestURI().substring(req.getContextPath().length());
            logger.info("set attribute URL_FORWARD: " + url);
            session.setAttribute(URL_FORWARD, url);
            AuthUsuario usuario = (AuthUsuario) session.getAttribute(LOGGED_USER);
            logger.info("checking user: "+usuario);

            //verificaSecurity(request, response);  

            if (usuario == null) {
                logger.info("no ha iniciado sesion");
                
                if ("/ce/retrieveInoo".equals(url) || "/ugel/retrieveInoo".equals(url)) {
                	logger.info("viene de actualizar de token");
                	HttpServletResponse httpServletResponse = (HttpServletResponse) response;

                	StringBuilder sb = new StringBuilder();
                	sb.append("");
                	httpServletResponse.setHeader("Cache-Control", "no-cache");
                	httpServletResponse.setCharacterEncoding("UTF-8");
                	httpServletResponse.setContentType("text/xml");
                	httpServletResponse.setStatus(403);
                	PrintWriter pw = response.getWriter();
                	pw.println(sb.toString());
                	pw.flush();
                	return;
                }

                session.invalidate(); ///////////////////////////////////////
                forward(doLogin, request, response);
            } else if (rolesPermitidos.indexOf(usuario.getTipo()) < 0) {
                logger.info("el rol no le corresponde");
                forward(ROL_NO_PERMITIDO_PATH, request, response);
            } else if (usuario.getTipo().equals(Constantes.USUARIO_UGEL) && url.indexOf("/ce/inicio.uee") != -1) {
                logger.info("rol ugel desea ingresar a carpeta CE");
                session.invalidate();
                forward(doLogin, request, response);
            } else {
                logger.info("continua con la peticion : "   + request + " ///  " + response  );
                filterChain.doFilter(request, response);
            }
        } catch (ServletException sx) {
            filterConfig.getServletContext().log(sx.getMessage());
            sx.printStackTrace();
        } catch (IOException iox) {
            filterConfig.getServletContext().log(iox.getMessage());
            iox.printStackTrace();
        }

    }

    public void verificaSecurity(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException {

        try {

            this.prng = SecureRandom.getInstance("SHA1PRNG");

            // Add Script Nonce CSP Policy
            // --Generate a random number
            // String randomNum = new Integer(this.prng.nextInt()).toString();
            String randomNumext = Integer.toHexString(new Integer(this.prng.nextInt()))
                    + Integer.toHexString(new Integer(this.prng.nextInt())).charAt(0);
            // String randomNum =new Double(this.prng.nextGaussian()).toString();
            
            /*
			 * String csp = " 'nonce-"+ randomNumext +"' ; "+
			 * //" 'unsafe-inline' http: https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc; "
			 * + //"default-src 'self';  " + //"script-src 'self'; "+
			 * //"script-src https://ajax.googleapis.com/ ;"+ //"style-src self ; "+
			 * "font-src 'self'; "+ "frame-src 'self' ; "+ "img-src 'self'; "+
			 * "media-src; "+ "object-src 'none'; "+ "base-uri 'none'; "+
			 * "report-uri https://www.google.com/recaptcha/api.js https://www.bing.com/api/maps/mapcontrol?branch=release; "
			 * + //"sandbox; " + //"script-src 'strict-dynamic' "+
			 * //"script-src 'self'  https://www.google.com/; "+
			 * //"script-src 'self' 'unsafe-inline' ; "+
			 * //" 'unsafe-inline' http: https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc; "
			 * + //"style-src 'self'; "+ //"style-src 'self' https://www.google.com/ ; "+
			 * //"style-src 'self' 'unsafe-inline' ; "+ //"upgrade-insecure-requests; "+
			 * //"connect-src 'none' "+ //"upgrade-insecure-requests "+ "";
             */  
            String csp
                    = // " object-src 'none';"+
                    // agregados
                    // "script-src 'self' https://www.google.com/recaptcha/"+
                    "frame-src 'self' https://www.google.com/recaptcha/ ;" + "style-src 'self' 'unsafe-inline' ;"
                    + // "default-src 'self'; " +
                    // "script-src 'self'; "+
                    // "script-src 'nonce-"+ randomNumext +"' 'unsafe-inline' 'unsafe-eval'
                    // 'strict-dynamic' https: http:;"+
                    "font-src 'self'; "
                    + // "frame-src *://*.google.com; "+
                    "img-src 'self'; " + "media-src; " + "base-uri 'none'; " + "default-src 'nonce-"
                    + randomNumext + "' 'unsafe-inline' 'strict-dynamic' https: http:;"
                    + // "report-uri https://www.google.com/recaptcha/api.js
                    // https://www.bing.com/api/maps/mapcontrol?branch=release; "+
                    "report-uri *://*.google.com https://www.bing.com/api/maps/mapcontrol?branch=release ; "
                    + // "upgrade-insecure-requests; "+
                    "connect-src 'self' ;" + "object-src 'self' 'unsafe-inline' ;"
                  //  + "script-src  'strict-dynamic' 'nonce-"+ randomNumext +"' https: http: ; "
                    + // "sandbox; script-src;"+
                    "";
            

            HttpServletResponse response = (HttpServletResponse) servletResponse;
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            String sessionid = request.getSession().getId();

            // REQUEST INICIO
            /*
			 * String remoteAddress = request.getRemoteAddr(); String forwardedFor =
			 * request.getHeader("X-Forwarded-For"); String realIP =
			 * request.getHeader("X-Real-IP"); String remoteHost = request.getRemoteHost();
			 * 
			 * System.out.println("New login request (X-Real-IP: " + realIP +
			 * ", X-Forwarded-For:" + forwardedFor + ", remote address: " + remoteAddress +
			 * ", remote host: " + remoteHost);
			 * 
			 * if( realIP == null ) realIP = forwardedFor; if( realIP == null ) realIP =
			 * remoteAddress; if( realIP == null ) realIP = remoteHost;
			 * 
			 * System.out.println("Real-IP: " + realIP);
             */

            
            // REQUEST FIN
            // response.addHeader("X-FRAME-OPTIONS", "DENY");
            // response.addHeader("X-Content-Type-Options", "nosniff");
            // response.addHeader("X-XSS-Protection", "1; mode=block");
            // response.addHeader("Content-Security-Policy", csp);
            
            response.setHeader("X-XSS-Protection", "1; mode=block");
            response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains"); 
            response.setHeader("X-Content-Type-Options", "nosniff"); 
            response.setHeader("Cache-control", "no-store, no-cache"); 
            response.setHeader("X-Frame-Options", "DENY"); 
            response.setHeader("Set-Cookie", "XSRF-TOKEN=" + sessionid + "; Path=/; Secure; HttpOnly");
//          response.getHeader( "Set-Cookie", "key=value; HttpOnly; SameSite=strict");
//        	response.addHeader("Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly ;SameSite=Strict ");
            response.setHeader("Content-Security-Policy", csp);
            
            
            
            
            
            // response.setHeader("Set-Cookie", "XSRF-TOKEN=NDKDdfdsfkldsfNd3SZAJfwLsTl5WUgOkE; Path=/; Secure;HttpOnly");

            
            // response.addHeader( "Content-Security-Policy", "default-src 'self'; font-src;
            // frame-src; img-src; media-src; object-src; report-uri; sandbox; script-src;
            // style-src; upgrade-insecure-requests; connect-src 'none';
            // upgrade-insecure-requests" );
            // response.addHeader( "Content-Security-Policy", csp );
            //response.addHeader("Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly");
            

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void forward(String fwd, ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        logger.info("redireccionando a " + fwd);
        RequestDispatcher rd = request.getRequestDispatcher(fwd);
        rd.forward(request, response);
    }

    // Clean up resources
    public void destroy() {
    	this.filterConfig = null;
        logger.info("terminando filtro");
    }
    
    private boolean isSessionExpiredByIdletime(HttpSession session) {
    	Date expirationDate = (Date) session.getAttribute(Constantes.EXPIRATION_DATE);

    	// Solo para primera vez
    	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    	boolean isNewTimeout = false;
    	if (expirationDate == null) {
    		expirationDate = new Date(System.currentTimeMillis() + 1000000);
    		isNewTimeout = true;
    	}

    	if (isNewTimeout) {
    		long timeoutInactive = session.getMaxInactiveInterval(); 
    		long timeExpired = System.currentTimeMillis() + session.getMaxInactiveInterval() * 1000;
    		logger.info("session.getMaxInactiveInterval() = " + session.getMaxInactiveInterval());
    		Date date = new Date(timeExpired);
    		session.setAttribute(Constantes.TIMEOUT_INACTIVE, timeoutInactive);
    		session.setAttribute(Constantes.EXPIRATION_DATE, date);

    		String dateString = format.format(date);
    		logger.info("set timeExpired = " + dateString);
    	} else {
    		logger.info("Time in milliseconds expirared: " + expirationDate.getTime());
    		logger.info("Time in milliseconds now: " + (new Date()).getTime());
    		long restaSec = (expirationDate.getTime() - (new Date()).getTime()) / 1000;
    		logger.info("restaSec: " + restaSec);
    	}

    	logger.info("expirationDate = " + (expirationDate != null ? format.format(expirationDate) : " is null"));
    	return (expirationDate.before(new Date()));
    }

    
}
