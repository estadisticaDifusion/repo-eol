package pe.gob.minedu.escale.eol.portlets.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.MappingDispatchAction;


public class DisparadorAction extends MappingDispatchAction{

    public ActionForward gestionarActividades(ActionMapping mapping,ActionForm form,
            HttpServletRequest request,HttpServletResponse response)
            throws Exception {

        return mapping.findForward("successAdminActividad");
    }

    public ActionForward gestionarUsuariosClave(ActionMapping mapping,ActionForm form,
            HttpServletRequest request,HttpServletResponse response)
            throws Exception {

        return mapping.findForward("successAdminUsuarios");
    }
    
    //INICIO [MHUAMANI]
    public ActionForward gestionarCredencialesUsuario(ActionMapping mapping,ActionForm form,
            HttpServletRequest request,HttpServletResponse response)
            throws Exception {

        return mapping.findForward("successCredencialUsuario");
    }
    //FIN [MHUAMANI]
    
//imendoza 20170217 inicio
    public ActionForward gestionarArchivos(ActionMapping mapping,ActionForm form,
            HttpServletRequest request,HttpServletResponse response)
            throws Exception {

        return mapping.findForward("successAdminArchivos");
    }
//imendoza 20170217 fin
}
