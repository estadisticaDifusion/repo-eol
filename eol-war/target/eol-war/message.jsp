<%-- 
    Document   : message
    Created on : 24/04/2017, 10:08:03 AM
    Author     : IMENDOZA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${requestScope.state eq 1}">
            <div class="alert alert-success">
              <strong>Exito!</strong> ${requestScope.message}
            </div>
        </c:if>
        <c:if test="${requestScope.state eq 0}">
            <div class="alert alert-danger">
              <strong>Error!</strong> ${requestScope.message}
            </div>
        </c:if>
    </body>
</html>
