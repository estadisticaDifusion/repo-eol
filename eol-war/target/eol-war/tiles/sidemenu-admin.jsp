<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>

<li>
    <html:link action="/gestionActividad">
        <bean:message key="admin.gestion.actividades" />
    </html:link>
</li>
<!--imendoza 20170217 inicio-->
<li>
    <html:link action="/gestionArchivos">
        <bean:message key="admin.gestion.archivos" />
    </html:link>
</li>
<!--imendoza 20170217 fin-->
<li>
    <html:link action="/gestionUsuariosClave">
        <bean:message key="admin.gestion.usuarios.clave.cambio" />
    </html:link>
</li>
