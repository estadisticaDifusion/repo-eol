<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<html:html xhtml="true">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="stylesheet"
	href="<html:rewrite page='/recursos/css/Refresh.css'/>" type="text/css" />

<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="text-align: center; background: #E9E6D7; margin-top: 0">
	<div id="wrap">
		<div id="minheader" class="minheader">
			<header role="banner" id="banner">
				<hgroup id="heading">
					<h1 class="company-title">
						<a title="Ir a ESCALE - Unidad de Estad&iacute;stica Educativa"
							class="logo"> <span>ESCALE - Unidad de
								Estad&iacute;stica Educativa</span>
						</a>
					</h1>
				</hgroup>
			</header>
		</div>
		<div id="header">
			<h1 id="logo-text">
				<bean:message key="logotext.1" />
				<span class="gray"><bean:message key="logotext.2" /></span>
			</h1>
			<h2 id="slogan">
				<bean:message key="slogan" />
			</h2>
		</div>
		<div id="content-wrap" style="height: 150px">
			<br>
			<br> <label style="font-size: 12pt; font-family: Arial;">
				La sesi&oacute;n ha expirado, por favor vuelva a ingresar al
				sistema, haga clic <html:link action="/inicio">aqu&iacute;</html:link>
			</label>
		</div>
		<div id="footer">
			<div style="border-top: 1.5px solid #055FCD;"></div>
			<p>
				<bean:message key="footer.copy" />
				<strong> <bean:message key="footer.autor" />
				</strong>
			</p>
		</div>
	</div>
</body>
</html:html>