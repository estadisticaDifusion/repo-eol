<%@page import="java.util.Date"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<fmt:setBundle basename="messages" var="msg" />
<fmt:setBundle basename="ApplicationResources" var="aplmsg" />
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
    java.util.Date hoy = new java.util.Date();
    java.util.Date fechac = new java.util.Date("2019/06/15");
    int fechaent = 1;
    if( hoy.after(fechac) )
        fechaent = 1;
    else
        fechaent = 2;

    pageContext.setAttribute("fechaent", fechaent);
%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link type="text/css" href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/popup.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/padron.css'/>" rel="stylesheet" />

<%-- SECCION REUBICADA AL DEFAULT
<script type="text/javascript" src="<c:url value='/recursos/js/popup_lib.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"></script>

<c:if test="${fn:contains(header['user-agent'],'MSIE')}">
    <script type="text/javascript" src="<c:url value='/recursos/js/json2.js'/>"></script>
</c:if>
<script type="text/javascript" src="<c:url value='/recursos/js/form_estadistico.js'/>"></script>
--%>
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.structure.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.theme.min.css'/>" rel="stylesheet" />

    <div class="titulo">

        <input type="hidden" id="urlR" name="urlR" value="<c:url value="/"/>">
        <input type="hidden" id="urlMsj" name="urlMsj" value="<c:url value="/MensajeServlet"/>">
        <input type="hidden" id="urlEol" name="urlEol" value="<c:url value="/EolCensoServlet"/>">
        <input type="hidden" id="urlCEol" name="urlCEol" value="<c:url value="/ClaveEolServlet"/>">

        <input type="hidden" id="valueUGEL" name="valueUGEL" value="${LOGGED_USER.usuario}">
        <input type="hidden" id="esregSigied" name="esregSigied" value="${EsregSigied}">
        <input type="hidden" id="esregHuelga" name="esregHuelga" value="${EsregHuelga}">

        <div style="font:10pt Arial,Helvetica,Verdana,sans-serif;margin: 0 0 5px 17px;border-bottom:0px dashed #055FCD;font-size:17px;text-transform: uppercase; color:  #055FCD ;font-weight: bold;">
            <c:if test="${tableroEstd.rol.cod==2}">
                Bienvenido Estadístico(a) de la ${tableroEstd.ugel.nombreUgel}
            </c:if>
            <c:if test="${tableroEstd.rol.cod==3}">
                Bienvenido Estadístico(a) de la ${tableroEstd.dre.nombreDre}
            </c:if>
        </div>
        <span style="margin: 0 0 0 17px;color: #333333;font-size: 12px; font-family: Arial,Helvetica,Verdana,sans-serif; font-weight: bold">Código:&nbsp;</span><span style="font-size: 11px;font-family: Arial,Helvetica,Verdana,sans-serif; " >${LOGGED_USER.usuario}&nbsp;&nbsp;&nbsp;</span>
        <br/>
        <div style=" padding: 0 55px 10px 17px ;font-size: 12px;font-family: Arial,Helvetica,Verdana,sans-serif;">
            El tablero de control facilita el monitoreo de los tres módulos del Censo Educativo a través de los siguientes servicios:
            reporte de cobertura, situación de cada institución educativa a la fecha y relación de omisas.
            Para iniciar haga CLIC en la celda correspondiente.
            <br/>
            <c:url var="urlEOL" value="/ugel/clavesEOL.xls"/>
            <c:url var="urlResum" value="/ugel/matricula/resumen2011.xls"/>
            <c:url var="urlResum2012" value="/ugel/matricula/resumen2012.xls"/>
            <c:url var="urlResum2013" value="/ugel/matricula/resumen2013.xls"/>

        </div>
    </div>

        <div style=" width: 100%" >

        <div class="clear"></div>
        <table class="body_content_tablero"  border=0>
            <tr>
                <td class="tablero">
                    <div id="contenido" >
                        <div id="tabs" >
                            <ul>
                                <li><a href="#tabs-1">Actividades</a></li>
                                <li><a href="#tabs-2">Reportes</a></li>
                                <li><a href="#tabs-3">Buscar IIEE</a></li>
                                <li><a href="#tabs-4">Cobertura - <fmt:message bundle="${aplmsg}" key="eol.censo.anio_final"/></a></li>
                                <li><a href="#tabs-5">Reporte detallado</a></li>
                            </ul>
                            <div id="tabs-1" style="margin: -5px -15px 0 -15px;">
                                <c:import url="/WEB-INF/jsp/ugel/include/actividades.jsp" />
                            </div>
                            
                            <div id="tabs-2" style="margin: -5px -15px 0 -15px;">
                                <c:import url="/WEB-INF/jsp/ugel/include/reportes.jsp" />                                
                            </div>

                            <div id="tabs-3" style="margin: 0px 0px 0 0px;" >
                                <div id="capaBqIE" style=" width: 100%">
                                    <form action="" id="buscarIIEEForm" style="background-color: #FAFAFA;">
                                        <input type="hidden" id="eolinfo" name="eolinfo" value="${OAUTH2_SESSION.accessToken}">
                                        <input type="hidden" id="anioInicio" name="anioInicio" value="${anioInicioEstadistica}">
                                        <input type="hidden" id="anioFinal" name="anioFinal" value="${anioFinalEstadistica}">
                                        <table id="tableForm" style="margin: 0 0 0 0 " cellpadding="0" cellspacing="5" border="0">
                                            <tr class="controles" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;" >
                                                <td class="celeste" id="input-izquierdo"  >
                                                    <table>
                                                        <tr>
                                                            <td>
                                                             <div class="input-line">
                                                                <label for="nombreIE"><fmt:message bundle="${msg}" key="nombreIE"/></label>
                                                                <input type="text" size="25" id="nombreIE" name="nombreIE" />
                                                            </div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                           <div class="input-line">
                                                            <label for="cod_mod"><fmt:message bundle="${msg}" key="cod_mod"/></label>
                                                            <input type="text"  size="7" maxlength="7" id="cod_mod" name="cod_mod" />
                                                            </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                    <div class="input-line">
                                                        <label for="codlocal_input"><fmt:message bundle="${msg}" key="codlocal"/></label>
                                                        <input type="text"  size="6" maxlength="6"  id="codlocal" name="codlocal" />
                                                    </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                    <div class="input-line">
                                                        <label for="nombreCP"><fmt:message bundle="${msg}" key="nombreCP"/></label>
                                                        <input type="text" size="20" id="nombreCP" name="nombreCP" />
                                                    </div>       
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>

                                                    <div class="input-line">
                                                        <label for="nivelI"><fmt:message bundle="${msg}" key="nivelescolar"/></label>
                                                        <select name="nivelI" id="nivelI" style="width: 270px;">
                                                          <option value="00">--Todos--</option>
                                                          <option value="A1">A1 - INICIAL CUNA</option>
                                                          <option value="A2">A2 - INICIAL JARDIN</option>
                                                          <option value="A3">A3 - INICIAL CUNA Y JARDIN</option>
                                                          <option value="B0">B0 - PRIMARIA EBR</option>
                                                          <option value="F0">F0 - SECUNDARIA EBR</option>
                                                          <option value="K0">K0 - FORMACION MAGISTERIAL ISP</option>
                                                          <option value="T0">T0 - SUPERIOR TECNOLOGICA IST</option>
                                                          <option value="M0">M0 - ESCUELAS DE FORMACIÓN ARTÍSTICA</option>
                                                          <option value="E0">E0 - EDUCACION ESPECIAL</option>
                                                          <option value="L0">L0 - CETPRO</option>
                                                          <option value="A5">A5 - INICIAL NO ESCOLARIZADO</option>
                                                          <option value="D1">D1 - BASICA ALTERNATIVA INICIAL - INTERMEDIO</option>
                                                          <option value="D2">D2 - BASICA ALTERNATIVA AVANZADA</option>
                                                        </select>
                                                    </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>

                                                    <div class="input-line">
                                                        <label for="gestionI"><fmt:message bundle="${msg}" key="gestion"/></label>
                                                        <select name="gestionI" id="gestionI" style="width: 270px;">
                                                          <option value="00">--Todos--</option>
                                                          <option value="A1">PÚBLICA - SECTOR EDUCACIÓN</option>
                                                          <option value="A2">PÚBLICA - OTRO SECTOR PÚBLICO</option>
                                                          <option value="A3">PÚBLICA - MUNICIPALIDAD</option>
                                                          <option value="A4">PÚBLICA - EN CONVENIO</option>
                                                          <option value="B1">PRIVADA - COOPERATIVA</option>
                                                          <option value="B2">PRIVADA - PARROQUIAL</option>
                                                          <option value="B3">PRIVADA - COMUNAL</option>
                                                          <option value="B4">PRIVADA - PARTICULAR</option>
                                                          <option value="B5">PRIVADA - FISCALIZADA</option>
                                                          <option value="B6">PRIVADA - INSTITUCIONES BENÉFICAS</option>
                                                        </select>
                                                    </div>
                                                            </td>
                                                        </tr>
                                                        <td>
                                                    <div class="input-line">
                                                        <label for="codUbigeo"><fmt:message bundle="${msg}" key="codigo.ubigeo"/></label>
                                                        <input type="text" size="6" maxlength="6" id="codUbigeo" name="codUbigeo" />
                                                    </div>
                                                            </td>
                                                        
                                                    </table>

                                                </td>

                                            </tr>
                                            <tr id="botonera" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;">
                                                <td >
                                                    <img src="<c:url value='/recursos/images/ajax-loader.gif'/>" alt="Cargando..." id="img_cargando" />
                                                    <div id="cuenta">

                                                    </div>
                                                    <div id="botones">
                                                        <button type="button" id="buscar">Buscar</button>
                                                        <button type="button" id="limpiar">Limpiar</button>
                                                    </div>
                                                    <div>
                                                        <span id="alert" class="hide">Se ha detectado un error</span>
                                                    </div>

                                                    <div id="opciones">
                                                    </div>
                                                    <div class="clear"></div>
                                                </td>
                                            </tr>
                                            <tr id="filaAvisoBq" >
                                                <td  height="15px">
                                                </td>
                                            </tr>
                                            <tr valign="top" >
                                                <td >
                                                    <div class="paginador_ie"></div>
                                                    <div>
<!--                                                    <div id="listadoCentros">-->
                                                        <table id="listadoIIEE" cellpadding="5" cellspacing="1" >
                                                            <thead style="font:10px Arial,Helvetica,Verdana,sans-serif;color: #000000; font-weight: bold" >
                                                                <tr>
                                                                </tr>
                                                            </thead>
                                                            <tbody style="font:10px Arial,Helvetica,Verdana,sans-serif;color: #000000; ">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="paginador_ie"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div> 
                                <div id="capaTabIE">
                                    <span style="font-size:13px; font-weight: bold; padding-left: 15px; ">Tablero de control del director(a)</span>
                                    <br/>
                                    <div align="left" style="position: relative;left: 15px;font:12px Arial,Helvetica,Verdana,sans-serif;padding-bottom:5px;padding-top: 5px;"  >
                                        <span style="color: #333333; font-weight: bold">Código modular:&nbsp;</span><span style="font-size: 11px " id="IE_CODMOD"></span>
                                        <span style="color: #333333; font-weight: bold">&nbsp;&nbsp;&nbsp;Nivel educativo:&nbsp;</span><span style="font-size: 11px " id="IE_NIVMOD"></span>
                                        <span style="color: #333333; font-weight: bold">&nbsp;&nbsp;&nbsp;Código de local:&nbsp;</span><span style="font-size: 11px " id="IE_CODLOCAL"></span>

                                    </div>
                                    <a href="#" id="volverBq" style="font:11px Arial,Helvetica,Verdana,sans-serif; font-weight: bold">Volver</a>
                                    <div id="tableContainer" >

                                        <table id="tableAct" cellpadding="1" cellspacing="1" class="tabla"  width="100%">
                                            <thead>
                                                <tr>
                                                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.actividad"/></th>
                                                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.plazo"/></th>
                                                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.formato"/></th>
                                                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.situacion"/></th>
                                                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.fecha"/></th>
                                                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.constancia"/></th>
                                                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.datrep"/></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!--a href="#" id="volverBq" style="font:11px Arial,Helvetica,Verdana,sans-serif; font-weight: bold">Volver</a-->
                                </div>
                            </div>

                            <div id="tabs-4" style="margin: 0px 0px 0 0px;" >
                                <c:import url="/WEB-INF/jsp/ugel/include/resumenCoberturaDetalle.jsp" />
                            </div>

                            <div id="tabs-5" style="margin: -5px -15px 0 -15px;">
                                <c:import url="/WEB-INF/jsp/ugel/include/reportesDetallados2017.jsp" />
                            </div>
                        </div>

                    </div>
                </td>

            </tr>
        </table>
    </div>
