<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>

<head>
<link href="<html:rewrite page='/recursos/css/padron_import.css'/>" rel="stylesheet" rev="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Error</title>
</head>
<body>
<h1>Actualizando Padr&oacute;n</h1>

<h2>Error</h2>
<p>El archivo cargado debe ser un archivo <strong>.DBF</strong> o <strong>.ZIP</strong>
<br/>
[<html:link action="/cargar/form">&lt;--- Regresar</html:link> ]
</p>
</body>
</html>