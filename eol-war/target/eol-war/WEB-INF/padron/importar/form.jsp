<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html:html xhtml="true">
    <html:xhtml />
    <head>
        <link href="<html:rewrite page='/recursos/css/padron_import.css'/>" rel="stylesheet" rev="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Cargar archivo</title>
    </head>
    <body>
        <h1>Actualizando Padr&oacute;n</h1>
        
        <h2>Cargar archivo</h2>
        <html:form action="/cargar/cargar" enctype="multipart/form-data">
            Archivo: <html:file property="file" size="50" />
            <br />
            <html:submit styleId="uploadbutton" />
        </html:form>
        
    </body>
</html:html>
