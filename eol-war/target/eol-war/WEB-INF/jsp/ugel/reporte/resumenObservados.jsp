<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%--@page import="com.liferay.portal.model.User"--%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
    <head>
        <title>Resumen Observados</title>
        <c:choose>
            <c:when test="${param.formato ne 'xls'}">
                <fmt:setLocale value="fr" scope="request" />
            </c:when>
            <c:when test="${param.formato eq 'xls'}">
                <fmt:setLocale value="en" scope="request" />
            </c:when>
        </c:choose>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <c:if test="${param.formato ne 'xls'}">
            <link href="<c:url value='/recursos/css/reporte.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />

            <jsp:include flush="true" page="/recursos/jquery/index.jsp?css=false" />
            <script type="text/javascript" src="<c:url value='/recursos/js/reporte.js'/>"></script>
        </c:if>
        <% java.util.Date hoy = new java.util.Date();
                    pageContext.setAttribute("hoy", hoy);
                    String codUGEL = request.getParameter("codUGEL");
                    boolean esDRE = codUGEL.endsWith("00")
                            || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_CALLAO)
                            || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_LIMA_METRO)
                            || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_LIMA_PROV);
                    pageContext.setAttribute("esDRE", esDRE);
                    pageContext.setAttribute("codUGEL", codUGEL);
        %>


    </head>
    <body>

        <div id="body">
            <h1></h1>

            <c:choose>
                <c:when test="${TipoConsulta eq 'CENSO-MATRICULA'||TipoConsulta eq 'CENSO-RESULTADO'}">
                    <c:if test="${TipoConsulta eq 'CENSO-MATRICULA'}">
                        <h2 class="pre">Matr�cula, Docentes, Recursos</h2>
                    </c:if>
                    <c:if test="${TipoConsulta eq 'CENSO-RESULTADO'}">
                        <h2 class="pre">Resultado del ejercicio</h2>    
                    </c:if>

                    <c:if test="${esDRE}">
                        <h2>C�digo de DRE: ${codUGEL}</h2>
                        <h3>S�lo se considera a las IIEE  de �mbito de administraci�n regional</h3>
                    </c:if>
                    <c:if test="${!esDRE}">
                        <h2>C�digo de UGEL: ${codUGEL}</h2>
                    </c:if>
                    <h3><fmt:formatDate value="${hoy}" pattern="dd-MM-yyyy" /></h3>

                </c:when>
                <c:when test="${TipoConsulta eq 'CENSO-LOCAL'}">
                    <h2 class="pre">Local Escolar</h2>                    
                    <c:if test="${esDRE}">
                        <h2>C�digo de DRE:${codUGEL}</h2>
                        <h3>S�lo se considera a los Localese de �mbito de administraci�n regional</h3>
                    </c:if>
                     <c:if test="${!esDRE}">
                        <h2>C�digo de UGEL:${codUGEL}</h2>
                    </c:if>
                    <h3><fmt:formatDate value="${hoy}" pattern="dd-MM-yyyy" /></h3>
                </c:when>
            </c:choose>


            <div id="cuadro" >


                <c:choose>
                    <c:when test="${TipoConsulta eq 'CENSO-MATRICULA' || TipoConsulta eq 'CENSO-RESULTADO'}">
                        <h3 class="tituloCuadro">Reporte de Env�os Observados de Instituciones Educativas</h3>
                    </c:when>
                    <c:when test="${TipoConsulta eq 'CENSO-LOCAL'}">
                        <h3 class="tituloCuadro">Listado de Situaci�n de Reporte de Locales Escolares</h3>
                    </c:when>
                    <c:when test="${TipoConsulta eq 'CENSO-ID'}">
                        <h3 class="tituloCuadro">Listado de Situaci�n de Reporte de la C�dula ID</h3>
                    </c:when>
                </c:choose>

                <div align="left" style="color: Black; font-family: Arial; font: x-small; font-style: normal;">
                    <strong>Cantidad:&nbsp; ${fn:length(resumen_matri_envios)}</strong>
                </div>

                <c:choose>
                    <c:when test="${TipoConsulta eq 'CENSO-MATRICULA' || TipoConsulta eq 'CENSO-RESULTADO'}">
                        <table cellpadding="5" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>C&oacute;digo Modular</th>
                                    <th>Anexo</th>
                                    <th>Nivel</th>
                                    <th>C&oacute;digo Local</th>
                                    <th>Instituci&oacute;n Educativa / Programa </th>
                                    <th>Forma</th>
                                    <th>Tipo de IIEE</th>
                                    <th>Gesti&oacute;n</th>
                                    <th>C&oacute;digo Geogr&aacute;fico</th>
                                    <th>Distrito</th>
                                    <th>Fecha Env&iacute;o</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${resumen_matri_envios}" var="fila">
                                    <tr>
                                        <td align="center">&emsp;${fila[0]}</td>
                                        <td align="center">${fila[1]}</td>
                                        <td align="left"><sub>${fila[2]}</sub></td>
                                        <td align="center">${fila[4]}</td>
                                        <td align="left">${fila[5]}</td>
                                        <td align="center">${fila[6]}</td>
                                        <td align="center">${fila[3]}</td>
                                        <td align="center">${fila[7]}</td>
                                        <td align="center">${fila[8]}</td>
                                        <td align="center">${fila[9]}</td>
                                        <td align="center"><fmt:formatDate value="${fila[13]}" pattern="dd-MM-yyyy hh:mm"/> </td>

                                    </tr>


                                </c:forEach>

                            </tbody>
                        </table>
                    </c:when>
                    <c:when test="${TipoConsulta eq 'CENSO-LOCAL'}">

                        <table cellpadding="5" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>C�digo Local</th>
                                    <th>Instituci�n Educativa / Programa </th>
                                    <th>C�digo Geogr�fico</th>
                                    <th>Distrito</th>
                                    <th>Centro Poblado</th>
                                    <th>Fecha Env�o</th>
                                    <th>Direcci�n</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${resumen_matri_envios}" var="fila">
                                    <tr>
                                        <td align="center">&emsp;${fila[0]}</td>
                                        <td align="left">${fila[1]}</td>
                                        <td align="center">${fila[4]}</td>
                                        <td align="left">${fila[5]}</td>
                                        <td align="left">${fila[6]}</td>

                                        <td align="center"><fmt:formatDate value="${fila[9]}" pattern="dd-MM-yyyy hh:mm"/></td>
                                        <td align="left">${fila[7]}</td>

                                    </tr>
                                </c:forEach>

                            </tbody>
                        </table>
                    </c:when>
                </c:choose>
            </div>

        </div>
    </body>
</html>