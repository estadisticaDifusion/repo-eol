<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@page import="java.util.Calendar"%>	
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>

    <title>Resumen de cobertura</title>
    <c:choose>
        <c:when test="${param.formato ne 'xls'}">
            <fmt:setLocale value="es" scope="request" />
        </c:when>
        <c:when test="${param.formato eq 'xls'}">
            <fmt:setLocale value="es" scope="request" />
        </c:when>
    </c:choose>
    <c:if test="${param.formato ne 'xls'}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link href="<c:url value='/recursos/css/reporte.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />

        <jsp:include flush="true" page="/recursos/jquery/index.jsp?css=false" />
        <script type="text/javascript" src="<c:url value='/recursos/js/reporte.js'/>"></script>
    </c:if>
    <% java.util.Date hoy = new java.util.Date();
			    Calendar c = Calendar.getInstance();
			    c.setTime(new java.util.Date()); 
			    c.add(Calendar.HOUR, -5);
			    
			    hoy = c.getTime();
    			pageContext.setAttribute("hoy", hoy);
                String codUGEL = request.getParameter("codUGEL");
                boolean esDRE = codUGEL.endsWith("00")
                        || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_CALLAO)
                        || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_LIMA_METRO)
                        || codUGEL.substring(1).equals(Constantes.USUARIO_DRE_LIMA_PROV);
                pageContext.setAttribute("esDRE", esDRE);
                pageContext.setAttribute("codUGEL", codUGEL.substring(1));
    %>


</head>
<body>

    <div id="body" >
        <h1></h1>
        <h2 class="pre">Matr&iacute;cula, Docentes, Recursos </h2>
        <c:if test="${esDRE}">
            <h2>Código de DRE:${NOMBRE_IGEL}</h2>
        </c:if>
        <c:if test="${!esDRE}">
            <h2>C&oacute;digo de UGEL:${NOMBRE_IGEL}</h2>
        </c:if>
        
        

        <div id="cuadro">
            <h3 class="tituloCuadro">Cobertura al <fmt:formatDate value="${hoy}" pattern="dd-MM-yyyy" /></h3>
            <table cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">Nivel Educativo</th>
                        <th rowspan="2">Marco Censal</th>
                        <th colspan="2">Informantes Hasta el <fmt:formatDate pattern="dd 'de' MMMM" value="${EsregHuelgaDet ? ActividadMatDet.fechaTerminohuelga : (EsregSigiedDet ? ActividadMatDet.fechaTerminosigied : ActividadMatDet.fechaTermino)}"/></th>
                        <th colspan="2">Informantes posterior al <fmt:formatDate pattern="dd 'de' MMMM" value="${EsregHuelgaDet ? ActividadMatDet.fechaTerminohuelga : (EsregSigiedDet ? ActividadMatDet.fechaTerminosigied : ActividadMatDet.fechaTermino)}"/></th>
                    </tr>
                    <tr>
                        <th>Informantes</th>
                        <th>%</th>
                        <th>Informantes</th>
                        <th>%</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${resumen_matri}" var="fila">
                        <tr>
                            <c:if test="${fila.negrita}">
                                <td><strong> ${fila.descri} </strong></td>
                            </c:if>
                            <c:if test="${!fila.negrita}">
                                <td>${fila.descri}</td>
                            </c:if>

                            <c:if test="${fila.negrita}">
                                <td align="center"><strong>${fila.cuentaCentros}</strong></td>
                            </c:if>
                            <c:if test="${!fila.negrita}">
                                <td align="center">${fila.cuentaCentros}</td>
                            </c:if>
                            <c:if test="${fila.negrita}">
                                <td align="center"><strong>${fila.cuentaMatRecProf}</strong></td>
                            </c:if>
                            <c:if test="${!fila.negrita}">
                                <td align="center">${fila.cuentaMatRecProf}</td>
                            </c:if>
                            <c:if test="${fila.negrita}">
                                <td align="right"><strong>
                                <c:if test="${fila.cuentaCentros!=0}">
                                    <fmt:formatNumber  value="${fila.cuentaMatRecProf / fila.cuentaCentros}" pattern="0.##%"/>
                                </c:if>
                                <c:if test="${fila.cuentaCentros==0}">
                                    0%
                                </c:if>
                                </strong>
                                </td>
                            </c:if>
                            <c:if test="${!fila.negrita}">
                                <td align="right">
                                <c:if test="${fila.cuentaCentros!=0}">
                                    <fmt:formatNumber  value="${fila.cuentaMatRecProf / fila.cuentaCentros}" pattern="0.##%"/>
                                </c:if>
                                <c:if test="${fila.cuentaCentros==0}">
                                    0%
                                </c:if>
                                </td>
                            </c:if>
                            <!--adicionales-->
                            <c:if test="${fila.negrita}">
                                <td align="center"><strong>${fila.cuentaMatEnvioPost}</strong></td>
                            </c:if>
                            <c:if test="${!fila.negrita}">
                                <td align="center">${fila.cuentaMatEnvioPost}</td>
                            </c:if>
                                 <c:if test="${fila.negrita}">
                                <td align="right"><strong>
                                <c:if test="${fila.cuentaCentros!=0}">
                                    <fmt:formatNumber  value="${fila.cuentaMatEnvioPost / fila.cuentaCentros}" pattern="0.##%"/>
                                </c:if>
                                <c:if test="${fila.cuentaCentros==0}">
                                    0%
                                </c:if>
                                </strong>
                                </td>
                            </c:if>
                            <c:if test="${!fila.negrita}">
                                <td align="right">
                                <c:if test="${fila.cuentaCentros!=0}">
                                    <fmt:formatNumber  value="${fila.cuentaMatEnvioPost / fila.cuentaCentros}" pattern="0.##%"/>
                                </c:if>
                                <c:if test="${fila.cuentaCentros==0}">
                                    0%
                                </c:if>
                                </td>
                            </c:if>

                            
                        </tr>


                    </c:forEach>

                </tbody>
            </table>
        </div>
        <div class="leyenda">
            <table border="0"  width="400">
                <tr>
                    <td>
                        <!--<sub>1/ Incluye Educación de Adultos</sub><br/>
                        <sub>2/ Incluye Educación Ocupacional</sub><br/>-->
                        <sub>Fuente:  MINISTERIO DE EDUCACI&oacute;N - Estad&iacute;stica B&aacute;sica.</sub><br/>
                        <sub>Nota:  El marco censal incluye a los ISE del nivel tecnol&oacute;gico.</sub>

                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>