<%@page import="pe.gob.minedu.escale.eol.portlets.domain.act.Tablero"%>
<%@page import="pe.gob.minedu.escale.eol.converter.EolActividadConverter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--@page import="com.liferay.portal.model.User"--%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    Tablero tablero=(Tablero)request.getAttribute("tableroEstd");            
        if(tablero.getRol().getCod()==3){
            pageContext.setAttribute("esDRE", true);
        }
    EolActividadConverter act=(EolActividadConverter)request.getAttribute("ActividadActual");
    String tipo=act.getNombre();
%>
<link href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />

<style type="text/css">
#cuadro_r{
    width:auto;
    margin: 0 auto  ;    
    padding-top: 5px;
    
}
#cuadro_r table{
    margin: 0 auto  ;
    border-top: 1px black solid;
    border-bottom:  1px black solid;
    font-size: 9pt;
    font-family:    Arial, Helvetica,sans-serif;
}
#cuadro_r table td{
    border-right:   1px black solid;
}
#cuadro_r table thead  th{
    border-bottom:  1px black solid;
    border-right:   1px black solid;
}
#cuadro_r table thead  th blockquote{
    margin: 0.5em;
}
#cuadro_r table tr td:last-child,#cuadro table tr th:last-child{
    border-right:   none;
}
#cuadro_r table tfoot  tr{
    font-weight: bold;
}

</style>
<div align="center">

    <!--strong style="position: relative;left: 0px;text-transform: uppercase;margin-top:10px;font: Bold 12px Verdana, 'Trebuchet MS', Sans-serif;color: #000000;padding: 5px 0 5px 25px;" -->

    <div>
        <table   cellpadding="0" cellspacing="0" border="0" width="350px" >

            <tr>
                <td align="center" style="background-color: navy; color: #ffffff;font-size: 10pt;font-family:    Arial, Helvetica,sans-serif; font-weight: bold; height: 20px ">
                    
                    ${ActividadActual.descripcion} - ${ActividadActual.nombrePeriodo}
                </td>
            </tr>

            <tr>
                <td align="center" style="background-color: #0066cc; color: #ffffff;font-size: 9pt; font-family:    Arial, Helvetica,sans-serif;font-weight: bold;height: 20px  ">
                    <span class="pre1">Cobertura según UGEL </span>
                </td>
            </tr>
        </table>
    </div>

    <c:set var="totalIIEE" value="0" />
    <c:set var="totalMDR" value="0" />
    <div id="cuadro_r">
        <table  width="350px"  cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <th>UGEL</th>
                    <th>Marco Censal</th>
                    <th>Informantes</th>
                    <th>%</th>

                </tr>
            </thead>
            <tbody>
                <c:forEach items="${resumenCobertura}" var="fila">
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${esDRE && tipo=='CENSO-MATRICULA'}" >
                                    <c:url var="url" value="/ugel/2011/resumenCobertura">
                                        <c:param name="codUGEL" value="${fila.idUgel}"/>
                                    </c:url>
                                    <a href="${url}" target="_blank">${fila.idUgel}</a>
                                </c:when>
                                <c:otherwise>
                                    ${fila.idUgel}
                                </c:otherwise>
                            </c:choose>

                        </td>
                        <td align="center">${fila.cuentaCentros}</td>
                        <td align="center">${fila.cuentaEnvios}</td>
                        <td align="right">
                            <fmt:formatNumber  value="${fila.cuentaEnvios / fila.cuentaCentros}" pattern="0.##%"/>
                        </td>
                    </tr>
                    <c:set var="totalIIEE" value="${totalIIEE+fila.cuentaCentros}" />
                    <c:set var="totalMDR" value="${totalMDR+fila.cuentaEnvios}" />
                </c:forEach>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        Total
                    </td>
                    <td align="center">${totalIIEE}</td>
                    <td align="center">${totalMDR}</td>
                    <td align="right">
                        <fmt:formatNumber  value="${totalMDR/totalIIEE  }" pattern="0.##%"/>
                    </td>
                </tr>
            </tfoot>

        </table>
    </div>
        <table border="0" width="350" cellpadding="0" cellspacing="0" style="font-size: 9pt;    font-family:    Arial, Helvetica,sans-serif;">
            <tr align="left" valign="top" >
                <td align="left">
                     <% if (tipo.equals("CENSO-MATRICULA")||tipo.equals("CENSO-RESULTADO")) {%>
                        <p><sub>*</sub>incluye PRONOEIs</p>
                        <% }%>

                </td>
            </tr>
        </table>
</div>