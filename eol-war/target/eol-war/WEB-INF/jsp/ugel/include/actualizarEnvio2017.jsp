<%@page import="pe.gob.minedu.escale.eol.auth.domain.AuthUsuario"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.enumerations.RoleEnum"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.domain.act.Tablero"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<link href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />
<%-- <link href="<c:url value='/recursos/bootstrap-4.3.1-dist/css/bootstrap.min.css'/>" rel="stylesheet"/> --%>
<link href="<c:url value='/recursos/bootstrap-3.3.7-dist/css/bootstrap.min.css'/>" rel="stylesheet"/>
<html:xhtml />

<% java.util.Date hoy = new java.util.Date();
AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
            pageContext.setAttribute("hoy", hoy);            
            pageContext.setAttribute("usuario",usuario.getUsuario() );

%>
<div ng-app="eolApp" class="container col-md-12" >
        <div ng-controller="padronActualizarEnvioCtrl" class="col-md-12" >
            <div class="well well-sm">
                <fieldset>
                    <legend class="text-center header">Actualizar envio 2017</legend>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3">
                                <span class="help-block text-muted small-font" >Código Modular</span>
                                <input type="hidden"  ng-model="formModel.codigoUgel.value" ng-init="formModel.codigoUgel.value='${LOGGED_USER.usuario}'"/>
                                <input type="text" class="form-control input-sm" ng-model="formModel.codMod1.value" 
                                       id="idCodMod1" ng-keypress = "servIEKeyp($event)" maxlength="7" />
                            </div>
                            <div class="col-md-2" >
                                <span class="help-block text-muted small-font" >Anexo</span>
                                <input type="text" class="form-control input-sm" ng-model="formModel.anexo1.value" disabled="true"/>
                            </div>
                            <div class="col-md-7" >
                                <span class="help-block text-muted small-font" >Nombre IE</span>
                                <input type="text" class="form-control input-sm" ng-model="formModel.nombreIe1.value" disabled="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.codMod2.value"
                                       id="idCodMod2" ng-keypress = "servIEKeyp($event)" maxlength="7"/>
                            </div>
                            <div class="col-md-2" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.anexo2.value" disabled="true"/>
                            </div>
                            <div class="col-md-7" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.nombreIe2.value" disabled="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.codMod3.value"
                                       id="idCodMod3" ng-keypress = "servIEKeyp($event)" maxlength="7"/>
                            </div>
                            <div class="col-md-2" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.anexo3.value" disabled="true"/>
                            </div>
                            <div class="col-md-7" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.nombreIe3.value" disabled="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.codMod4.value"
                                       id="idCodMod4" ng-keypress = "servIEKeyp($event)" maxlength="7"/>
                            </div>
                            <div class="col-md-2" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.anexo4.value" disabled="true"/>
                            </div>
                            <div class="col-md-7" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.nombreIe4.value" disabled="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.codMod5.value"
                                       id="idCodMod5" ng-keypress = "servIEKeyp($event)" maxlength="7"/>
                            </div>
                            <div class="col-md-2" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.anexo5.value"  disabled="true"/>
                            </div>
                            <div class="col-md-7" >
                                <input type="text" class="form-control input-sm" ng-model="formModel.nombreIe5.value" disabled="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <input type="submit"  class="btn btn-success" ng-click="actualizar.actualizarPadronEnvio()" value="Actualizar" />
                                <input type="button"  class="btn btn-danger" ng-click="formModel.limpiar()" value="Limpiar" />
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
</div>

<script src="<c:url value='/recursos/angular/angular.min.js'/>" > </script>
<script src="<c:url value='/recursos/angular/angular-strap/dist/angular-strap.js'/>" > </script>
<script src="<c:url value='/recursos/angular/angular-strap/dist/angular-strap.tpl.js'/>" > </script>
<script src="<c:url value='/recursos/angular/app.js'/>" > </script>
<!--<script src="<c:url value='/recursos/angular/services/messagesService.js'/>" > </script>-->
<script src="<c:url value='/recursos/angular/services/padronIeService.js'/>" > </script>
<script src="<c:url value='/recursos/angular/services/actionPadronService.js'/>" > </script>
<script src="<c:url value='/recursos/angular/controllers/padronActualizarEnvioCtrl.js'/>" > </script>

