<%@page import="pe.gob.minedu.escale.eol.auth.domain.AuthUsuario"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.enumerations.RoleEnum"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.domain.act.Tablero"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<link href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />
<html:xhtml />

<% java.util.Date hoy = new java.util.Date();
AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
            pageContext.setAttribute("hoy", hoy);            
            pageContext.setAttribute("usuario",usuario.getUsuario() );

%>


<c:forEach items="${tableroEstd.mapActividades}" var="periodo">

    <c:if test="${periodo.key ge 2016 }">
        <div align="left" >
        <strong style="font:150% Arial,Helvetica,Verdana,sans-serif;color: black; font-size: 20px; left: auto">${periodo.key}</strong>
        </div>
        
        <div id="tableContainer" >
            <table  cellpadding="3" cellspacing="1" class="tabla"  width="98%">
               
        
                <tfoot>
                <tr>
                <td colspan="7" >
                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=1A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 1A ${periodo.key}</span></a>
                    </div>
                    
                    <div style="padding: 5px 15px 5px 0;">
                    <c:url var="urlResum2" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=2A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum2}"><span style="font-size:8pt;">Reporte 2A ${periodo.key}</span></a>
                    </div>
                    
                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=3A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 3A ${periodo.key}</span></a>
                    </div>

                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=4AI&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 4AI ${periodo.key}</span></a>
                    </div>

                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=4AA&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 4AA ${periodo.key}</span></a>
                    </div>

                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=5A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 5A ${periodo.key}</span></a>
                    </div>

                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=6A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 6A ${periodo.key}</span></a>
                    </div>

                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=7A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 7A ${periodo.key}</span></a>
                    </div>

                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=8A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 8A ${periodo.key}</span></a>
                    </div>

                    <div style="padding: 5px 15px 5px 0 ">
                    <c:url var="urlResum" value="/ugel/matricula/reporteDetallado${periodo.key}?tipced=9A&codugel=${LOGGED_USER.usuario}"/>
                    <a class="xls" href="${urlResum}"><span style="font-size:8pt;">Reporte 9A ${periodo.key}</span></a>
                    </div>
                    
                </td>
                </tr>
                </tfoot>
                 
            </table>
        </div>
    </c:if>
    <br/>
</c:forEach>
