 <%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.List"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />
<link href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" rev="stylesheet" type="text/css" />
<style type="text/css">

.fileinput-button input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  border: solid transparent;
  border-width: 0 0 100px 200px;
  opacity: 0;
  filter: alpha(opacity=0);
  -moz-transform: translate(-300px, 0) scale(4);
  direction: ltr;
  cursor: pointer;
}

.fileinput-button {
  position: relative;
  overflow: hidden;
  float: left;
  margin-right: 4px;
}
.fileupload-progressbar {
 float: right;
 width: 400px;
 margin-top: 4px;
}
</style>
<html:xhtml />
<portlet:defineObjects />
<theme:defineObjects />

<% java.util.Date hoy = new java.util.Date();
            pageContext.setAttribute("hoy", hoy);
            /*List<Group> groups =user.getGroups();
            for(Group group:groups)
             { if(group.getName().equals("Guest"))
               {
                   pageContext.setAttribute("groupId", group.getGroupId());
                   break;
               }
            }*/
%>

<c:forEach items="${tablero.eolPeriodos}" var="periodo">
    <strong style="font:150% Arial,Helvetica,Verdana,sans-serif;color:black; font-size: 20px;">${periodo.anio}</strong>
    <div id="tableContainer" >

        <table  cellpadding="1" cellspacing="1" class="tabla"  width="100%">
            <thead>
                <tr>
                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.actividad"/></th>
                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.plazo"/></th>
                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.formato"/></th>
                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.situacion"/></th>
                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.fecha"/></th>
                    <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.constancia"/></th>
                    <th>Archivo</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${periodo.eolActividadList}" var="actividad">
                    <tr>
                        <td width="150" height="50">
                            <span >${actividad.descripcion}</span>
                            <span style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;">
                                                   <br/> (<fmt:message bundle="${msg}" key="actividad.estado.${actividad.estadoActividad}"/>)
                            </span>
                        </td>
                        <td width="120" align="center" >
                            <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                <fmt:formatDate pattern="MMM yyyy" value="${actividad.fechaInicio}"/>
                            </font>
                            -
                            <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif"><fmt:formatDate pattern="MMM yyyy" value="${actividad.fechaTermino}"/>
                            </font>
                        </td>
                        <td width="90" align="center" valign="center">

                            <c:choose>
                                <c:when test="${(actividad.estadoActividad==2 || actividad.estadoActividad==4) &&  actividad.estadoFormato}">
                                    <c:if test="${not empty actividad.urlFormatoStr}">
                                    <span>
                                        <c:url var="url" value="${actividad.urlFormatoStr}"/>
                                        <a class="xls" href="${url}"> Descargar </a>
                                    </span>
                                    </c:if>

                                </c:when>
                                <c:otherwise>

                                    <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;color: Gray;vertical-align:middle;" >
                                        <strong class="xls_finish">Descargar</strong>
                                    </span>
                                </c:otherwise>
                                
                            </c:choose>

                        </td>
                        <td align="center">
                            <c:if test="${actividad.estadoActividad!=1 }">
                                <c:if test="${!actividad.envio}">
                                    <strong class="pendiente">&nbsp;</strong>
                                </c:if>
                                <c:if test="${actividad.envio}">
                                    <strong class="reportado">&nbsp;</strong>
                                </c:if>
                            </c:if>
                        </td>
                        <td align="center">
                            <c:if test="${actividad.estadoActividad!=1 }">
                                <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;">
                                    <fmt:formatDate pattern="dd/MM/yyyy" value="${actividad.fechaEnvio}"/>
                                </span>
                            </c:if>
                        </td>
                        <td width="80">
                            <c:if test="${actividad.estadoActividad!=1 }">
                                <c:if test="${actividad.envio}">
                                    <c:if test="${not empty actividad.urlConstancia and actividad.estadoConstancia }">
                                    <span>
                                        <c:url var="url" value="${actividad.urlConstanciaStr}"/>
                                     <a class="pdf" href="${url}" target="_blank">
                                     <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">Obtener </span>
                                     </a>
                                    </span>
                                    </c:if>                                    

                                </c:if>
                            </c:if>
                        </td>
                        <td align="center">

                            <%--c:url var="urlArch" value="/archivos/form">
                                        <c:param  name="idAct" value="${actividad.id}"/>
                                        <c:param  name="codMod" value="${tablero.ce.codigoModular}"/>
                                        <c:param  name="anexo" value="${tablero.ce.anexo}"/>
                                        <c:param name="codlocal" value="${tablero.ce.codlocal}"/>
                                        <c:param name="nivMod" value="${tablero.ce.nivelModalidad.idCodigo}"/>
                                     </c:url>
                                    <a   href="${urlArch}" onclick="window.open(this.href, this.target, 'width=620,height=510,top=100,left=250'); return false;"><img src="<c:url value="/recursos/images/botwin.jpg"/>" alt="Archivo" width="18" height="18" border="0" align="absmiddle" title="Cédula electrónica" />
                                    </a--%>
                               

                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <br/>
</c:forEach>



