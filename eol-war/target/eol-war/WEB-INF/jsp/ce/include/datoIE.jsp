<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html:xhtml />
<div align="left" style="position: relative;left: 15px;font:12px Arial,Helvetica,Verdana,sans-serif;padding-bottom:7px;"  >
    <span style="color: #333333; font-weight: bold">Código modular:&nbsp;</span><span style="font-size: 11px " id="COD_MOD">${tablero.ce.codigoModular}&nbsp;&nbsp;&nbsp;</span>
    <span style="color: #333333; font-weight: bold">Nivel educativo:&nbsp;</span><span style="font-size: 11px ">${tablero.ce.nivelModalidad.valor}&nbsp;&nbsp;&nbsp;</span>
    <span style="color: #333333; font-weight: bold">Código de local:&nbsp;</span><span style="font-size: 11px ">&nbsp;${tablero.ce.codlocal}&nbsp;&nbsp;&nbsp;</span>
    <span style="color: #333333; font-weight: bold">Estado:&nbsp;</span><span style="font-size: 11px ">${tablero.ce.estado.valor}&nbsp;&nbsp;&nbsp;</span>        
</div>
