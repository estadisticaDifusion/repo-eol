<%-- 
    Document   : archivos
    Created on : 09/11/2010, 10:43:29 PM
    Author     : Diego Silva
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
            /*String idCA=request.getParameter("idCA");
            if(idCA!=null && !idCA.isEmpty())
            request.setAttribute("idCA", idCA);
            request.setAttribute("idAct",request.getParameter("idAct"));
            request.setAttribute("codMod",request.getParameter("codMod"));
            request.setAttribute("anexo",request.getParameter("anexo"));
            request.setAttribute("codlocal",request.getParameter("codlocal"));*/
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Cargar archivos</title>
        <link type="text/css" href="<c:url value='/recursos/css/style.css'/>" rel="stylesheet" />
        <script type="text/javascript">
            function cancelar(){
                window.close()
            }
        </script>

    </head>
    <body>        
        <c:choose>
            <c:when test="${idCA!=null}">
                <c:url var="urlExcel" value="/DataXLS">
                    <c:param name="tipo" value="excel" />
                    <c:param name="idCA" value="${idCA}" />
                </c:url>
                <c:url var="urlDataXLS" value="/DataXLS">
                    <c:param name="tipo" value="quit" />
                    <c:param name="idCA" value="${idCA}" />
                </c:url>

                <div id="pFormView">
                    <div id="pFormViewTitle"  >
                    Cédula Electrónica de la Actividad  "${actividad.descripcion}"
                    </div>
                    <div id="pFormViewContent">

                        <table border=0 width="100%"   >
                            <tr>
                                <td>
                                    <div class="outputLine" >
                                        <span class="labelField">Nombre:</span>
                                        <span class="valueField">${archivo.nomArch} (<a href="${urlExcel}">Descargar</a>) (<a href="${urlDataXLS}">Quitar</a>)</span>
                                    </div>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <div class="outputLine" >
                                        <span class="labelField">Fecha de envío:</span>
                                        <span class="valueField">${archivo.fechaCarga}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div class="outputCnt">
                                        <button type="button" onclick="cancelar()">Cerrar</button>
                                    </div>

                                </td></tr>


                        </table>

                    </div>

                </div>
            </c:when>
            <c:otherwise>
                <c:url var="urlUpXLS" value="/archivos/submit"/>
                <%--form id="form_archivo" action="${urlUpXLS}" enctype="multipart/form-data" method="post">
                   <input type="hidden"  name="idActividad" value="${idAct}"/>
                   <input type="hidden"  name="codMod" value="${codMod}"/>
                   <input type="hidden"  name="anexo" value="${anexo}"/>
                   <input type="hidden"  name="codLocal" value="${codlocal}"/>
                   <div>
                       <input size="5"   type="file" id="fileXLS" name="fileXLS"/>
                       <input type="submit" id="save_archivo" name="save_archivo" value="Cargar" />
                   </div>
                       <button type="button" onclick="cancelar()">Cancelar</button>
               </form--%>


                <div id="pForm">
                    <div id="pFormTitle"  >
                        Cargar la Cédula Electrónica de la Actividad  "${actividad.descripcion}"
                    </div>
                    <div id="pFormContent">
                        <form id="form_archivo" action="${urlUpXLS}" enctype="multipart/form-data" method="post">
                            <input type="hidden"  name="idActividad" value="${actividad.id}"/>
                            <input type="hidden"  name="codMod" value="${codMod}"/>
                            <input type="hidden"  name="anexo" value="${anexo}"/>
                            <input type="hidden"  name="codLocal" value="${codlocal}"/>
                            <input type="hidden"  name="nivMod" value="${nivMod}"/>
                            <table border=0 width="100%">
                                <tr>
                                    <td>
                                        <div class="inputLine"  >
                                            <label for="fileXLS" >Seleccionar Archivo:</label>
                                            <input size="15"   type="file" id="fileXLS" name="fileXLS"/>
                                        </div>
                                    </td>
                                </tr>                               
                                <tr>
                                    <td align="center">
                                        <div class="inputCnt">
                                            <input type="submit" id="save_archivo" name="save_archivo" value="Enviar" />
                                            <button type="button" onclick="cancelar()">Cancelar</button>
                                        </div>

                                    </td></tr>
                            </table>
                        </form>
                    </div>
                </div>


            </c:otherwise>
        </c:choose>


        <%--form action="<c:url value='/cargarArchivos/submit'/>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="idCuadro" value="${cuadro.idCuadro}"/>
            <fieldset>
                <div class="input-line">
                    <label for="descripcion">Definición</label>
                    <textarea id="descripcion" name="descripcion" cols="50" rows="5">${cuadro.descripcion}</textarea>
                </div>
                <div class="input-line">
                    <label for="fuente">Fuente</label>
                    <textarea id="fuente" name="fuente" cols="50" rows="5">${cuadro.fuente}</textarea>
                </div>
                <div class="input-line">
                    <label for="xls">Archivo .xls</label>
                    <input id="xls"  name="xls" type="file" />

                    <c:choose>
                        <c:when test="${cuadro.tamanioExcel gt 0}">
                            <input name="xlsNull" type="checkbox" value="true" id="xlsNull"/>
                            <label for="xlsNull">Borrar</label>
                            -
                            <c:url var="url" value="/servlet/indicadores/archivo">
                                <c:param name="tipo" value="excel"/>
                                <c:param name="idCuadro" value="${cuadro.idCuadro}" />
                            </c:url>
                            <a href="${url}">Ver archivo cargado</a>

                        </c:when>
                        <c:otherwise>
                            Sin archivo
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="input-line">
                    <label for="cuadro">Cuadro indicador</label>
                    <input id="cuadro"  name="cuadro" type="file" />

                    <c:choose>
                        <c:when test="${cuadro.tamanioCuadro gt 0}">
                            <input name="cuadroNull" type="checkbox" value="true" id="cuadroNull"/>
                            <label for="cuadroNull">Borrar</label>
                              -
                            <c:url var="url" value="/servlet/indicadores/archivo">
                                <c:param name="tipo" value="cuadro"/>
                                <c:param name="idCuadro" value="${cuadro.idCuadro}" />
                            </c:url>
                            <a target="_blank" href="${url}">Ver archivo cargado</a>
                        </c:when>
                        <c:otherwise>
                            Sin archivo
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="input-line">
                    <label for="error">Cuadro errores</label>
                    <input id="error"  name="error" type="file" />


                    <c:choose>
                        <c:when test="${cuadro.tamanioError gt 0}">
                            <input name="errorNull" type="checkbox" value="true" id="errorNull"/>
                            <label for="errorNull">Borrar</label>
                               -
                            <c:url var="url" value="/servlet/indicadores/archivo">
                                <c:param name="tipo" value="error"/>
                                <c:param name="idCuadro" value="${cuadro.idCuadro}" />
                            </c:url>
                            <a target="_blank" href="${url}" >Ver archivo cargado</a>
                        </c:when>
                        <c:otherwise>
                            Sin archivo
                        </c:otherwise>
                    </c:choose>
                </div>
            </fieldset>
            <button type="submit">Cargar</button>
            <button type="button" onclick="cancelar()">Cancelar</button>
        </form--%>

    </body>
</html>
