<%@page import="java.util.Date"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<fmt:setBundle basename="messages" var="msg" />
<fmt:setBundle basename="ApplicationResources" var="aplmsg" />
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	/*java.util.Date hoy = new java.util.Date();
	java.util.Date fechac = new java.util.Date("2019/06/01");
	int fechaent = 1;

	if (hoy.after(fechac))
		fechaent = 1;
	else
		fechaent = 2;

	pageContext.setAttribute("fechaent", fechaent);
	
	if(request.getSession().getAttribute("LOGGED_USER") == null){
    	System.out.println("REDIRECCIONANDO AL LOGIN");
    	request.getRequestDispatcher("/index.jsp");
    }else{
    	System.out.println("CONTINUA LA NAVEGACION");
    }*/
%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link type="text/css" href="<c:url value='/recursos/css/style.css'/>"
	rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/popup.css'/>"
	rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/css/padron.css'/>"
	rel="stylesheet" />

<%-- SECCION REUBICADA AL DEFAULT
<script type="text/javascript" src="<c:url value='/recursos/js/popup_lib.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"></script>

<c:if test="${fn:contains(header['user-agent'],'MSIE')}">
    <script type="text/javascript" src="<c:url value='/recursos/js/json2.js'/>"></script>
</c:if>
<script type="text/javascript" src="<c:url value='/recursos/js/form_credenciales_usuario.js'/>"></script>
--%>


<link type="text/css"
	href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.css'/>"
	rel="stylesheet" />
<link type="text/css"
	href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.structure.min.css'/>"
	rel="stylesheet" />
<link type="text/css"
	href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.theme.min.css'/>"
	rel="stylesheet" />

<div class="titulo">
	<input type="hidden" id="valueType" name="valueType"
		value="${LOGGED_USER.tipo}"> <input type="hidden"
		id="valueName" name="valueName" value="${LOGGED_USER.nombre}">
	<input type="hidden" id="valueUser" name="valueUser"
		value="${LOGGED_USER.usuario}">

	<div
		style="font: 10pt Arial, Helvetica, Verdana, sans-serif; margin: 0 0 5px 17px; border-bottom: 0px dashed #055FCD; font-size: 17px; text-transform: uppercase; color: #055FCD; font-weight: bold;">ACTUALIZACI&Oacute;N
		DE CREDENCIALES DE USUARIO</div>

	<br />
</div>

<form class="form-horizontal" id="formCredentials"
	name="formCredentials" method="post">
	<input type="hidden" id="thogeter" name="thogeter"
		value="${OAUTH2_SESSION.accessToken}"> <br />
	<div class="form-group">
		<label class="control-label col-sm-3" for="institucion"><bean:message
				key="cambiocredenciales.institucion" /></label>
		<div class="col-sm-5">
			<input class="form-control" id="institucion" name="institucion"
				type="text" disabled>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="nomInstitucion"><bean:message
				key="cambiocredenciales.nombre.institution" /></label>
		<div class="col-sm-5">
			<input class="form-control" id="nomInstitucion" name="nomInstitucion"
				type="text" disabled>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="usuario"><bean:message
				key="cambiocredenciales.usuario" /></label>
		<div class="col-sm-5">
			<input class="form-control" id="soldier" name="soldier" type="text"
				disabled>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="today"><bean:message
				key="cambiocredenciales.contrasenia.actual" /></label>
		<div class="col-sm-5">
			<input class="form-control" id="today" name="today"
				type="password" placeholder="Ingresar su contraseña actual..">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="tomorrow"><bean:message
				key="cambiocredenciales.contrasenia.nueva" /></label>
		<div class="col-sm-5">
			<input class="form-control" id="tomorrow" name="tomorrow"
				type="password" placeholder="Ingresar la nueva constraseña..">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="tomorrowtwo"><bean:message
				key="cambiocredenciales.repetir.contrasenia" /></label>
		<div class="col-sm-5">
			<input class="form-control" id="tomorrowtwo"
				name="tomorrowtwo" type="password"
				placeholder="Vuelva a ingresar la nueva contraseña..">
		</div>
	</div>
	<br />
	<div class="form-group">

		<!-- <div class="col-sm-5">
			<label id="msgActualizacion">Espere por favor. La
				actualización de las credenciales podría demorar algunos segundos.</label>
		</div>
		 -->
	 
		 <div class="col-sm-3"> 
			 
		 </div>
		<div class="col-sm-5" id="msgActualizacion">	
				
		</div>
		<div class="col-sm-3">
		</div>	 
	</div>
	
 <div class="form-group">
			<div class="col-sm-3"> 
			 <div id="ctlMens"></div>
			</div>
			<div class="col-sm-5">	
				<div id="mensaje">
				</div>
			</div>
			<div class="col-sm-3">
			</div>	
	</div>
	<br />
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-2">
			<button id="btnSaveCU" class="btn btn-default">ACTUALIZAR
				CONTRASEÑA</button>
		</div>
		<div class="col-sm-offset-2 col-sm-2">
			<button id="btnCleanCU"  class="btn btn-default">LIMPIAR
				CONTRASEÑA</button>
		</div>
	</div>
	<div class="form-group">
   
        <div class="col-sm-offset-4 col-sm-5">
			     
             <c:if test="${LOGGED_USER_PRF eq 'UGEL'}">
             
			     <input id="pathforw" name="pathforw" type="hidden" value="<html:rewrite action="/ugel/inicio" />">
			     <input id="envus" name="envus" type="hidden" value="ugel">
			     <button id="btnCancelCU" class="btn btn-default">RETORNAR AL TABLERO</button>
<!-- 
                 <html:link action="/ugel/inicio" styleClass="btn btn-default"  >
                    CANCELAR
                 </html:link>
                 -->
            </c:if>
            <c:if test="${LOGGED_USER_PRF eq 'CE'}">
			     
			     <input id="pathforw" name="pathforw" type="hidden" value="<html:rewrite action="/ce/tablero" />">
			     <input id="envus" name="envus" type="hidden" value="ce">
			     <button id="btnCancelCU" class="btn btn-default">RETORNAR AL TABLERO</button>

<!-- 
                  <html:link action="/ce/tablero" styleClass="btn btn-default"  >
                     CANCELAR
                  </html:link>
                  -->
           </c:if>
      </div>

	</div>
</form>
