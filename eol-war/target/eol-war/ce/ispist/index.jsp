<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<h1>Seleccione el tablero de control</h1>
<p> La Institución Educativa de Educación Superior Pedagógica - IESP cuenta con carreras Técnicas.  Para acceder al tablero de  ambos niveles cuenta con los siguientes enlaces:</p>
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
<ul>
 
    <li>
        <p>
        	
            <html:link action="/ce/tablero?codmod=${IIEE.codigoModular}&anexo=${IIEE.anexo}&nivel=${IIEE.nivelModalidad.idCodigo}&codlocal=${IIEE.codlocal}&codinst=${IIEE.codinst}&mcenso=${IIEE.mcenso}&sienvio=${IIEE.sienvio}&estado=${IIEE.estado.idCodigo}">
                Tablero de control del nivel de Educación Superior Pedagógica - IESP
            </html:link>
        	
        </p>
    </li>

    <li>
        <p>
            <html:link action="/ce/tablero?codmod=${IIEE.codigoModular}&anexo=${IIEE.anexo}&nivel=T0&codlocal=${IIEE.codlocal}&codinst=${IIEE.codinst}&mcenso=${IIEE.mcenso}&sienvio=${IIEE.sienvio}&estado=${IIEE.estado.idCodigo}">
                Tablero de control del nivel de Educación Superior Tecnológica - IEST
            </html:link>
        </p>
    </li>
</ul>


