<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="pe.gob.minedu.escale.eol.enums.admin.EstadoActividadEnum"%>
<%@page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>
<%@page import="pe.gob.minedu.escale.eol.auth.domain.AuthUsuario"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<% java.util.Date hoy = new java.util.Date();
    AuthUsuario usuario = (AuthUsuario) session.getAttribute(Constantes.LOGGED_USER);
    pageContext.setAttribute("usuario", usuario.getUsuario());
    pageContext.setAttribute("nombre", usuario.getNombre());

    pageContext.setAttribute("ENUM_INICIADO", EstadoActividadEnum.INICIADO.toString());
    pageContext.setAttribute("ENUM_EXTENPORANEO", EstadoActividadEnum.EXTENPORANEO.toString());
    pageContext.setAttribute("ENUM_SININICIAR", EstadoActividadEnum.SIN_INICIAR.toString());


%>

<head>
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" type="text/css" href="../ce/EOL-HTML/css/styles.css" />
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>


    <script src="../ce/EOL-HTML/scripts/arcgislink-min.js" type="text/javascript"></script>

    <!--
    <link rel="stylesheet" type="text/css" href="../ce/EOL-HTML/css/redmond/jquery-ui-1.10.4.custom.css" />
    -->


</head>

<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />
<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/popup.css'/>" />
<link type="text/css" rel="stylesheet" href="<c:url value='/recursos/css/style.css'/>" />
<fmt:setBundle basename="pe.gob.minedu.escale.eol.portlets.messages" var="msg" />

<script type="text/javascript" src="<c:url value='/recursos/js/popup_lib.js'/>"></script>


<%--<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-1.4.4.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui-1.8.8.custom.min.js'/>"></script>--%>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.js'/>"></script>

<script type="text/javascript">var ugel='${LOGGED_USER.usuario}'</script>
<script type="text/javascript" src="<c:url value='/recursos/js/form_estadistico.js'/>"></script>

<%--
<link type="text/css" href="<c:url value='/recursos/jquery/css/jquery-ui-1.8.8.custom.css'/>" rel="stylesheet" />
<script src="../ce/EOL-HTML/scripts/jquery-2.1.0.js"></script>
<script src="../ce/EOL-HTML/js/jquery-ui-1.10.4.custom.js"></script>
<script src="../ce/EOL-HTML/scripts/mapa.js"></script> 
--%>
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.structure.min.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/recursos/jquery/jquery-ui/jquery-ui.theme.min.css'/>" rel="stylesheet" />

<h1>Tablero de Control del Director de Institución Educativa</h1>
<p>

<div align="left" style="position: relative;left: 15px;font:12px Arial,Helvetica,Verdana,sans-serif;padding-bottom:7px;"  >
    <span style="color: #333333; font-weight: bold">Ud. está visualizando el tablero de control de la I.E con código modular:${param.codmod} anexo:${param.anexo} y nivel: <fmt:message bundle="${msg}" key="nivel.${param.nivel}"/> </span>
</div>

<p>


    <html:link action="/ce/tablero?codmod=${param.codmod}&anexo=${param.anexo}&nivel=${param.nivel}&codlocal=${param.codlocal} ">
        Actualizar tablero
    </html:link>

<div style=" width: 100%" >

    <div class="clear"></div>
    <table class="body_content_tablero"  border=0>
        <tr>
            <td class="tablero">
                <div id="contenido" > 
                    <%int a = 1;%>
                    <div id="tabs" >
                        <ul>
                            <li id="t1"><a href="#tabs-1">Tablero</a></li>
                            <li id="t2"><a href="#tabs-2">Indicadores de Gestión</a></li>
                        </ul>
                        <div id="tabs-1" style="margin: -5px -15px 0 -15px;">

                            <table class="body_content_tablero"  border=0>
                                <tr>
                                    <td class="tablero">
                                        <div id="cuadros">
                                            <!--
                                            <ul>-->
                                            <li class="pendiente_ley">Pendiente</li>
                                            <li class="reportado_ley">Reportado</li>
                                            <!--</ul>-->
                                        </div>
                                        <div id="contenido">

                                            <c:forEach items="${mapAct}" var="mp">
                                                <strong style="font:150% Arial,Helvetica,Verdana,sans-serif;color:black; font-size: 20px;">${mp.key}</strong>
                                                <div id="tableContainer" >

                                                    <table  cellpadding="1" cellspacing="1" class="tabla"  width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.actividad"/></th>
                                                                <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.plazo"/></th>
                                                                <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.formato"/></th>
                                                                <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.situacion"/></th>
                                                                <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.fecha"/></th>
                                                                <th><fmt:message bundle="${msg}" key="tablero.ce.tab_resumen.constancia"/></th>
                                                                <th>Archivo</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <c:forEach items="${mp.value}" var="cedula">
                                                                <tr>
                                                                    <td width="150" height="50">
                                                                        <span >${cedula.actividad.nombre}</span>
                                                                        <span style="font-size: 8pt;font-family:Arial;color: #4682b4;padding: 5px 10px;">
                                                                            <!--  <br/> (<fmt:message bundle="${msg}" key="actividad.estado.${cedula.actividad.estadoActividad}"/>) -->
                                                                        </span>

                                                                    </td>
                                                                    <td width="120" align="center" >

                                                                        <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                                                        <fmt:formatDate pattern="MMM yyyy" value="${cedula.actividad.fechaInicio}"/>
                                                                        </font>
                                                                        -
                                                                        <font style="text-transform: capitalize;font-family:Arial,Helvetica,Verdana,sans-serif">
                                                                        <fmt:formatDate pattern="MMM yyyy" value="${cedula.actividad.fechaTermino}"/>                                                        
                                                                        </font>

                                                                    </td>
                                                                    <td width="90" align="center" valign="center">
                                                                        <c:choose>
                                                                            <c:when test="${(cedula.actividad.estadoActividad==ENUM_INICIADO ||
                                                                                            cedula.actividad.estadoActividad==ENUM_EXTENPORANEO)
                                                                                            &&  cedula.actividad.estadoFormato}">
                                                                                <c:if test="${not empty cedula.actividad.urlFormato}">
                                                                                    <span>

                                                                                        <c:url var="url" value="${cedula.actividad.urlFormato}">
                                                                                            <c:param name="codmod" value="${param.codmod}" />
                                                                                            <c:param name="anexo" value="${param.anexo}" />
                                                                                            <c:param name="nivel" value="${param.nivel}" />
                                                                                            <c:param name="codlocal" value="${param.codlocal}" />
                                                                                            <c:if test="${not empty cedula.nroEnvio}">
                                                                                                <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                                            </c:if>

                                                                                        </c:url>

                                                                                        <a class="xls" href="${url}"> Descargar </a>
                                                                                    </span>
                                                                                </c:if>

                                                                            </c:when>
                                                                            <c:otherwise>

                                                                                <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;color: Gray;vertical-align:middle;" >
                                                                                    <strong class="xls_finish">Descargar </strong>

                                                                                </span>
                                                                            </c:otherwise>

                                                                        </c:choose>

                                                                    </td>
                                                                    <td align="center">
                                                                        <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR}">
                                                                            <c:if test="${!cedula.envio}">
                                                                                <strong class="pendiente">&nbsp;</strong>
                                                                            </c:if>
                                                                            <c:if test="${cedula.envio}">
                                                                                <strong class="reportado">&nbsp;</strong>
                                                                            </c:if>
                                                                        </c:if>
                                                                    </td>
                                                                    <td align="center">
                                                                        <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR}">
                                                                            <span  style="font:100% Arial,Helvetica,Verdana,sans-serif;">
                                                                                <fmt:formatDate pattern="dd/MM/yyyy" value="${cedula.fechaEnvio}"/>
                                                                            </span>
                                                                        </c:if>
                                                                    </td>
                                                                    <td width="80">
                                                                        <c:if test="${cedula.actividad.estadoActividad!=ENUM_SININICIAR }">
                                                                            <c:if test="${cedula.envio}">
                                                                                <c:if test="${not empty cedula.actividad.urlConstancia and cedula.actividad.estadoConstancia }">
                                                                                    <span>
                                                                                        <c:url var="url" value="${cedula.actividad.urlConstancia}">
                                                                                            <c:param name="idEnvio" value="${cedula.nroEnvio}" />
                                                                                            <c:param name="nivel" value="${param.nivel}" />
                                                                                        </c:url>
                                                                                        <a class="pdf" href="${url}" target="_blank">
                                                                                            <span style="font:100% Arial,Helvetica,Verdana,sans-serif;text-decoration: underline ">Obtener </span>
                                                                                        </a>
                                                                                    </span>
                                                                                </c:if>

                                                                            </c:if>
                                                                        </c:if>
                                                                    </td>
                                                                    <td align="center">


                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <br/>
                                            </c:forEach>


                                        </div>
                                    <td>             
                                </tr>
                            </table>
                        </div>

                        <div id="tabs-2" style="margin: 0px 0px 0 0px;" >                            

                            <div id="contenido" style="height: 550px; width:690px;overflow:Scroll; margin-left: 2px; margin-bottom: 5px; margin-top: 2px" >

                                <div id="container">

                                    <div id="dialog" title="Ubique su local escolar">
                                        <p align='justify'>
                                            Esta opción le permitirá indicar la ubicación geográfica correcta de su local escolar. Para ello debe usar el mapa ubicado en la parte inferior, acercarse lo máximo posible para marcar la posición del local escolar, los pasos para realizar esta acción se detallan a continuación.
                                        </p>
                                    </div>

                                    <div>
                                        <div id="dconfirm_ubic" class="inst">
                                            <table class="tpasos">
                                                <tbody>
                                                    <tr>
                                                        <td class="npaso"></td>


                                                        <td class="tpaso">
                                                            <!--
                                    <ul>
                                                            -->
                                                            <!--<li>-->
                                                            <div id="msg00">
                                                                <li>El marcador rojo <img src="../ce/EOL-HTML/img/red-dot.png" /> indica la sugerencia de la ubicación del local.</li>
                                                                <!--</br>-->
                                                            </div>
                                                <li id="obs_dyn_map"></li></br>
                                                <li id="msg01">Por favor, confirme si está de acuerdo con la localización indicada.</li>
                                                <div id="msg02">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="r_u_correct" id="r_u_correct_si" /><b id="bold01">Si estoy de acuerdo</b>&nbsp;
                                                    <input type="radio" name="r_u_correct" id="r_u_correct_no" /><b id="bold02">No estoy de acuerdo</b></br></br>
                                                </div>
                                                <!--</li>-->

                                                <!--
                                                <li>
                                                        Si no hay localización disponible, no aparecerá el marcador y la imagen se focalizará en el distrito donde está registrada la escuela. Si ese es su caso, o si no está de acuerdo con la posición marcada, indique  <b>“No estoy de acuerdo”</b>.
                                                </li>
                                                -->
                                                <li id="msg03">
                                                    Al hacer click en <b>"No estoy de acuerdo"</b> aparecerá en la imagen un marcador verde <img src="../ce/EOL-HTML/img/green-dot.png" />, 
                                                    el cual podrá desplazar hasta la localización correcta del local escolar.
                                                </li>
                                                <li>
                                                    Si Ud. tiene habilitado el marcador verde <img src="../ce/EOL-HTML/img/green-dot.png" />, arrástrelo presionando el botón izquierdo del mouse, hasta la posición correcta del local.
                                                </li>
                                                </br>
                                                <!--
                </ul>
                                                -->
                                                </td>

                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="dmap_instruction" class="inst">
                                            <table class="tpasos">
                                                <tbody>
                                                    <tr>
                                                        <td class="npaso"></td>
                                                        <td class="tpaso">
                                                            <!--
                                                            <ul>
                                                            -->
                                                <li>
                                                    Si necesita acercarse, puede aumentar el zoom haciendo doble click en el lugar de la imagen que desea ver con más detalle. También puede hacer click en los íconos 
                                                    de zoom ubicados a la izquierda de la imagen, que le permiten acercarse<img src="../ce/EOL-HTML/img/aumenta-zoom.png" /> o alejarse <img src="../ce/EOL-HTML/img/disminuye-zoom.png" />
                                                </li>
                                                <li id="msgyaubicado" style="display:none;">
                                                    Si el director de otra escuela del mismo local escolar ha confirmado o re-ubicado la localización. Ud. recibirá un mensaje de que el local ya cuenta con una localización confirmada.
                                                </li>
                                                <li id="obs_dyn_map2" style="display:none;">
                                                </li>
                                                <!--
                                                 </ul>
                                                --> 
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div>
                                            <div id="divcontent"></div>
                                        </div>
                                        <div id="toc"></div>
                                    </div>
                                    <div>

                                        <div>
                                            <div id="dtipoobs_instruccion" class="inst">
                                                <table class="tpasos">
                                                    <tbody>
                                                        <tr>
                                                            <td class="npaso"></td>
                                                            <td class="tpaso">
                                                                <!--
                                                                <ul>
                                                                -->
                                                    <li>
                                                        Sobre el procedimiento de localización, responda seleccionando por lo menos una opción de la lista siguiente. Podrá modificar sus observaciones después de guardarlas.
                                                    </li>
                                                    <!--
                                                     </ul>
                                                    --> 
                                                    </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="cont_tipoobs" style="height:120px;">
                                                <!--<ul></ul>-->
                                            </div>
                                            <div id="botones">
                                                <input type="button" id="btngrabar" value="Grabar" />
                                                <!--
                                                <input type="button" id="btncancelar" value="Cancelar" />
                                                -->
                                            </div>
                                            <div>
                                            </div>
                                        </div>


                                        <div>             
                                            <%
                                                String codmod2 = request.getParameter("codmod");
                                                String anexo2 = request.getParameter("anexo");
                                            %>

                                            <input type="hidden" id="txtcodmod" value="${param.codmod}" />
                                            <input type="hidden" id="txtanexo" value="${param.anexo}" /> 

                                            <%--<input type="button" id="btnbuscar" value="Buscar"/> --%>
                                        </div>
                                    </div>



                                </div>



                            </div>

                        </div>

                    </div>
            </td>

        </tr>
    </table>
</div>



<!-- FIN MODIF INICIO C.E - JCMATAMOROS : 21-03-2011 -->

