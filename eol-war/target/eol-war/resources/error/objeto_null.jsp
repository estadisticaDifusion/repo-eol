<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<html>
<head>
<title>noobjeto</title>
<link rel="stylesheet"
	href="<html:rewrite page="/resources/css/Refresh.css"/>"
	type="text/css" />
<link href="<html:rewrite page='/resources/css/ebin.css'/>"
	rel="stylesheet" rev="stylesheet" type="text/css" />
</head>
<body bgcolor="#ffffff">
<h1>Estimado Usuario por favor cierre la ventana y vuelva a ingresar al sistema.</h1>
<html:button property="cerrar" styleClass="button"
	onclick="javascript:window.close()">
	<bean:message key="botones.cerrar" />
</html:button>
</body>
</html>