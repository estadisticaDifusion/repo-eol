jQuery(document).ready(function() {
        //alert("Iniciando la carga de datos");
        /*jQuery("#contrasenia").focusout(function(){
            var pass = jQuery("#contrasenia").val();
            alert(pass);
            //jQuery(this).val(val);
        });*/

        /*var your_site_key = '6LeBbGMUAAAAADIULF6PKF-SFlA2jovbpcpMsE-u';
        var renderRecaptcha = function () {
            grecaptcha.render('ReCaptchContainer', {
                'sitekey': your_site_key,
                'callback': reCaptchaCallback,
                theme: 'light', //light or dark
                type: 'image',// image or audio
                size: 'normal'//normal or compact
            });
        };*/

        /*var reCaptchaCallback = function (response) {
            if (response !== '') {
                jQuery('#lblMessage').css('color', 'green').html('Success');
            }
        };*/
        /*function reCaptchaCallback(response) {
            if (response !== '') {
                jQuery('#lblMessage').css('color', 'green').html('Success');
            }
        }*/

        jQuery("#usuario").attr("autocomplete", "off");

        jQuery("#loginForm").submit(function( event ){

            var message = 'Please checck the checkbox';
            var response = grecaptcha.getResponse();

            if(response.length == 0){
                message = 'Verificación fallida del Captcha';
                jQuery('#lblMessage').html(message);
                jQuery('#lblMessage').css('color', (message.toLowerCase() == '¡Satisfactorio!') ? "green" : "red");

                if(typeof jQuery('#lblMessageErrorUserPassword').val()  !== 'undefined') {
                	message = '';
                	jQuery('#lblMessageErrorUserPassword').html(message);
                }
                
                return false;
            } else {
                message = '¡Satisfactorio!';
            }

            //console.log("Impimiendo en label");
            jQuery('#lblMessage').html(message);
            jQuery('#lblMessage').css('color', (message.toLowerCase() == 'success!') ? "green" : "red");

            var usr = jQuery("#usuario").val();
            var pass = jQuery("#contrasenia").val();
            var secret = "eol2018";

            //var usrenc = jQuery.base64.encode(usr);
            //var passenc = jQuery.base64.encode(pass);

            /*var text = "The quick brown fox jumps over the lazy dog.";
            var encrypted = CryptoJS.AES.encrypt(text, secret);
            encrypted = encrypted.toString();
            console.log("Cipher text: " + encrypted);*/

            var usrenc = CryptoJS.AES.encrypt(usr, secret);
            usrenc = usrenc.toString();
            var passenc = CryptoJS.AES.encrypt(pass, secret);
            passenc = passenc.toString();

            //console.log("Cipher text: " + usrenc);
            //console.log("Cipher text: " + passenc);

            //alert(""+encrypted);
            var descif = CryptoJS.AES.decrypt(usrenc, secret);
            var utf8descif = descif.toString(CryptoJS.enc.Utf8);
            
            /*console.info("ORIGINAL : "+usr);
            console.info("CIFRADO : "+usrenc);
            console.info("DESCIFRADO : "+descif);
            console.info("TEXT UTF8 : "+utf8descif);*/

            jQuery("#usuario").val(usrenc);
            jQuery("#contrasenia").val(passenc);
            jQuery("#tbluser").hide();
            jQuery("#lblcargadatos").show();

            return true;
        });
    });

    function envioDatos(){

        var senddat = false;
        var usr = jQuery("#usuario").val();
        var pass = jQuery("#contrasenia").val();
        var secret = "eol2018";

        if(usr!="" & pass!="")
            senddat = true;

        var usrenc = CryptoJS.AES.encrypt(usr, secret);
        usrenc = usrenc.toString();
        var passenc = CryptoJS.AES.encrypt(pass, secret);
        passenc = passenc.toString();

        if(senddat){
            jQuery("#usuario").val(usrenc);
            jQuery("#contrasenia").val(passenc);
            jQuery("#tbluser").hide();

            document.getElementById("loginForm").submit();
        }
        else{
            jQuery("#epicaptcha_message").html("Ingresar los datos de usuario");
            jQuery("#epicaptcha_message").css("color", "#cc0000");
        }

    }

/* comentado el 26/10/2018 para que opere EPICAPTCHA descomentarlo
  $(function(){
      ////epicaptcha div////
       $("#epicaptcha").Epicaptcha( {
           buttonID: "epi", // the id of the form button
           theFormID: "loginForm", // the id of the form tag
           submitUrl: "http://www.epicaptcha.com/theme-captcha-submit"
       });
   });
   */