    jQuery(document).ready(function() {
        //alert("Iniciando la carga de datos");
        /*jQuery("#contrasenia").focusout(function(){
            var pass = jQuery("#contrasenia").val();
            alert(pass);
            //jQuery(this).val(val);
        });*/
        jQuery("#usr").attr("autocomplete", "off");

        jQuery("#loginForm").submit(function( event ){

            var message = 'Please checck the checkbox';
            var response = grecaptcha.getResponse();

            if(response.length == 0){
                message = 'Verificación fallida del Captcha';
                jQuery('#lblMessage').html(message);
                jQuery('#lblMessage').css('color', (message.toLowerCase() == '¡Satisfactorio!') ? "green" : "red");

                if(typeof jQuery('#lblMessageErrorUserPassword').val()  !== 'undefined') {
                	message = '';
                	jQuery('#lblMessageErrorUserPassword').html(message);
                }

                return false;
            } else {
                message = '¡Satisfactorio!';
            }

            jQuery('#lblMessage').html(message);
            jQuery('#lblMessage').css('color', (message.toLowerCase() == 'success!') ? "green" : "red");

            var usr = jQuery("#usr").val();
            var pass = jQuery("#pass").val();
            var secret = "eol2018";

            var usrenc = CryptoJS.AES.encrypt(usr, secret);
            usrenc = usrenc.toString();
            var passenc = CryptoJS.AES.encrypt(pass, secret);
            passenc = passenc.toString();

            jQuery("#usr").val(usrenc);
            jQuery("#pass").val(passenc);
            jQuery("#tbluser").hide();
            jQuery("#lblcargadatos").show();
            // jQuery("#div-wait").show();

            return true;
        });
    });

    function envioDatos(){

        var senddat = false;
        var usr = jQuery("#usr").val();
        var pass = jQuery("#pass").val();
        var secret = "eol2018";

        if(usr!="" & pass!="")
            senddat = true;

        var usrenc = CryptoJS.AES.encrypt(usr, secret);
        usrenc = usrenc.toString();
        var passenc = CryptoJS.AES.encrypt(pass, secret);
        passenc = passenc.toString();

        if(senddat){
            jQuery("#usr").val(usrenc);
            jQuery("#pass").val(passenc);
            jQuery("#tbluser").hide();

            document.getElementById("loginForm").submit();
        }
        else{
            jQuery("#epicaptcha_message").html("Ingresar los datos de usuario");
            jQuery("#epicaptcha_message").css("color", "#cc0000");
        }
        
    }

/* comentado el 26/10/2018 para que opere EPICAPTCHA descomentarlo
  $(function(){
      ////epicaptcha div////
       $("#epicaptcha").Epicaptcha( {
           buttonID: "epi", // the id of the form button
           theFormID: "loginForm", // the id of the form tag
           submitUrl: "http://www.epicaptcha.com/theme-captcha-submit"
       });
   });

   */