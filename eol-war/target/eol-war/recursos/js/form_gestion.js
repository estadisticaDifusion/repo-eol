// Funciones para uso de REST
            var actividad;

            var rootwsactividadURL = '/eol-admin-ws/api/resources';
            
            window.onload = findAll();
            jQuery.ajaxSetup({ cache:false });
            jQuery(document).ready(function(){

                jQuery.datepicker.setDefaults(jQuery.datepicker.regional['es']);
                jQuery("#fechaInicio").datepicker({ dateFormat: 'dd/mm/yy' });
                jQuery("#fechaTermino").datepicker({ dateFormat: 'dd/mm/yy' });
                jQuery("#fechaLimite").datepicker({ dateFormat: 'dd/mm/yy' });
                jQuery("#fechaTerminoSigied").datepicker({ dateFormat: 'dd/mm/yy' });
                jQuery("#fechaLimiteSigied").datepicker({ dateFormat: 'dd/mm/yy' });
                jQuery("#fechaTerminoHuelga").datepicker({ dateFormat: 'dd/mm/yy' });
                jQuery("#fechaLimiteHuelga").datepicker({ dateFormat: 'dd/mm/yy' });

                 /*jquery 1.4.4
                 *jQuery('#actividadLista a').live('click', function() {
                        findById(jQuery(this).data('identity'));
                });*/
                jQuery('#actividadLista').on('click','a', function() {
                        //alert("dentro lista");
                        console.log("dentro de live");
                        findById(jQuery(this).data('identity'));
                });
                
                jQuery('#btnSave').on('click',function() {
                	updateActividad();
                });
                
                console.log("DESPUES DE CARGA");
                
            })
            
            function findAll() {
                    jQuery.ajax({
                            type: 'GET',
                            url: rootwsactividadURL + "/buscarActividad",
                            dataType: "json",
                            success: renderizarLista
                    });
            }


            function findById(id) {
                    jQuery.ajax({
                            type: 'GET',
                            url: rootwsactividadURL + '/buscarActividad/' + id,
                            dataType: "json",
                            crossDomain: false,
                            async: false,
                            cache: false,
                            success: function(data){
                                    actividad = data;
                                    renderizarDetalles(actividad);
                            }
                    });
            }

            function updateActividad() {

                    jQuery.ajax({
                            type: 'PUT',
                            contentType: 'application/json',
                            url: rootwsactividadURL + '/buscarActividad/' + jQuery('#actId').val(),
                            dataType: "json",
                            crossDomain: false,
                            async: false,
                            cache: false,
                            data: formToJSON(),
                            success: function(data, textStatus, jqXHR){
                                    alert('La actividad fue actualizada exitosamente');
                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                    alert('Error en actualizar: ' + textStatus);
                            }
                    });
            }


            function renderizarLista(data) {
                    var list = data == null ? [] : (data instanceof Array ? data : [data]);

                    jQuery('#actividadLista li').remove();
                    jQuery.each(list, function(key, actividad) {
                        /*jQuery.each(actividad, function (key1, actividad) {
                            jQuery.each(actividad, function (index, actividad) {*/
                                jQuery('#actividadLista').append('<li><a href="#" data-identity="' + actividad.id + '">'+actividad.nombre+'</a></li>');
                            /*})
                        })*/
                    });
            }

            function renderizarDetalles(actividad) {
                    jQuery('#actId').val(actividad.id);
                    jQuery('#nombre').val(actividad.nombre);
                    jQuery('#descripcion').val(actividad.descripcion);
                    jQuery('#fechaInicio').val(fechaFormatoJavaJson(actividad.fechaInicio));
                    jQuery('#fechaTermino').val(fechaFormatoJavaJson(actividad.fechaTermino));
                    jQuery('#fechaLimite').val(fechaFormatoJavaJson(actividad.fechaLimite));
                    jQuery('#fechaTerminoSigied').val(fechaFormatoJavaJson(actividad.fechaTerminosigied));
                    jQuery('#fechaLimiteSigied').val(fechaFormatoJavaJson(actividad.fechaLimitesigied));
                    jQuery('#fechaTerminoHuelga').val(fechaFormatoJavaJson(actividad.fechaTerminohuelga));
                    jQuery('#fechaLimiteHuelga').val(fechaFormatoJavaJson(actividad.fechaLimitehuelga));

            }

            function fechaFormatoJavaJson(fechaPrimitiva){
                var d = fechaPrimitiva.slice(0, 10).split('-');
                return d[2] +'/'+ d[1] +'/'+ d[0];
            }
            function fechaFormatoJsonJava(fechaEvolutiva){
                var d = fechaEvolutiva.slice(0, 10).split('/');
                var d1 = new Date(d[2], d[1]-1, d[0]);
                return d1;
            }

            function formToJSON() {
                    var actId = jQuery('#actId').val();
                    return JSON.stringify({
                            "id": actId == "" ? null : actId,
                            "nombre": jQuery('#nombre').val(),
                            "descripcion": jQuery('#descripcion').val(),
                            "fechaInicio": fechaFormatoJsonJava(jQuery('#fechaInicio').val()),
                            "fechaTermino": fechaFormatoJsonJava(jQuery('#fechaTermino').val()),
                            "fechaLimite": fechaFormatoJsonJava(jQuery('#fechaLimite').val()),
                            "fechaTerminosigied": fechaFormatoJsonJava(jQuery('#fechaTerminoSigied').val()),
                            "fechaLimitesigied": fechaFormatoJsonJava(jQuery('#fechaLimiteSigied').val()),
                            "fechaTerminohuelga": fechaFormatoJsonJava(jQuery('#fechaTerminoHuelga').val()),
                            "fechaLimitehuelga": fechaFormatoJsonJava(jQuery('#fechaLimiteHuelga').val())
                            });
            }