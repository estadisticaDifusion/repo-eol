'use strict';
angular.module('eolApp')
    .service('actionPadronService', function ($http, $q, URL) {
        this.actualizarPadronEnvio = function(fields){
            var deferred = $q.defer();
            var sUrl = URL.HOST_APP + URL.BASE_PADRON_LOC + URL.SERV_PADRON + URL.SERV_PADRON_ACT_ENV;
            $http.post(sUrl, fields,{
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(function(res){
                deferred.resolve(res);
            }).catch(function(err){
                deferred.reject(err);
            });
            return deferred.promise;
        };
    });

