'use strict';
angular.module('eolApp')
    .controller("padronActualizarEnvioCtrl", function($scope, REGEXP, padronIeService/*, messagesService*/
                                                        ,actionPadronService){
        $scope.rxTexto = REGEXP.TEXTO.toString();
        $scope.rxNumeros = REGEXP.NUMEROS.toString();        
        $scope.formModel = {
            codigoUgel:  {
                value: '',
                selected: false,
                disabled: false
            },
            codMod1: {
                value: '',
                selected: false,
                disabled: false
            },
            codMod2: {
                value: '',
                selected: false,
                disabled: false
            },
            codMod3: {
                value: '',
                selected: false,
                disabled: false
            },
            codMod4: {
                value: '',
                selected: false,
                disabled: false
            },
            codMod5: {
                value: '',
                selected: false,
                disabled: false
            },
            anexo1: {
                value: '',
                selected: false,
                disabled: false
            },
            anexo2: {
                value: '',
                selected: false,
                disabled: false
            },
            anexo3: {
                value: '',
                selected: false,
                disabled: false
            },
            anexo4: {
                value: '',
                selected: false,
                disabled: false
            },
            anexo5: {
                value: '',
                selected: false,
                disabled: false
            },
            nombreIe1: {
                value: '',
                selected: false,
                disabled: false
            },
            nombreIe2: {
                value: '',
                selected: false,
                disabled: false
            },
            nombreIe3: {
                value: '',
                selected: false,
                disabled: false
            },
            nombreIe4: {
                value: '',
                selected: false,
                disabled: false
            },
            nombreIe5: {
                value: '',
                selected: false,
                disabled: false
            },
            limpiar: function(){
                this.codMod1.value = '';
                this.codMod2.value = '';
                this.codMod3.value = '';
                this.codMod4.value = '';
                this.codMod5.value = '';
                this.anexo1.value = '';
                this.anexo2.value = '';
                this.anexo3.value = '';
                this.anexo4.value = '';
                this.anexo5.value = '';
                this.nombreIe1.value = '';
                this.nombreIe2.value = '';
                this.nombreIe3.value = '';
                this.nombreIe4.value = '';
                this.nombreIe5.value = '';
            }
        }

        $scope.servIEKeyp = function($event){
            if($event.charCode === 13){
                switch ($event.target.id) {
                    case 'idCodMod1':
                        padronIeService.fromModularCode($scope.formModel.codMod1.value, $scope.formModel.codigoUgel.value).then(function(res){
                            $scope.formModel.nombreIe1.value = res.data.items.cenEdu;
                            $scope.formModel.anexo1.value = res.data.items.anexo;
                        }).catch(function(err){
                            alert('El código modular no corresponde a su UGEL');
                        });
                        break;
                    case 'idCodMod2':
                        padronIeService.fromModularCode($scope.formModel.codMod2.value, $scope.formModel.codigoUgel.value).then(function(res){
                            $scope.formModel.nombreIe2.value = res.data.items.cenEdu;
                            $scope.formModel.anexo2.value = res.data.items.anexo;
                        }).catch(function(err){
                            alert('El código modular no corresponde a su UGEL');
                        });
                        break;
                    case 'idCodMod3':
                        padronIeService.fromModularCode($scope.formModel.codMod3.value, $scope.formModel.codigoUgel.value).then(function(res){
                            $scope.formModel.nombreIe3.value = res.data.items.cenEdu;
                            $scope.formModel.anexo3.value = res.data.items.anexo;
                        }).catch(function(err){
                            alert('El código modular no corresponde a su UGEL');
                        });
                        break;
                    case 'idCodMod4':
                        padronIeService.fromModularCode($scope.formModel.codMod4.value, $scope.formModel.codigoUgel.value).then(function(res){
                            $scope.formModel.nombreIe4.value = res.data.items.cenEdu;
                            $scope.formModel.anexo4.value = res.data.items.anexo;
                        }).catch(function(err){
                            alert('El código modular no corresponde a su UGEL');
                        });
                        break;
                    case 'idCodMod5':
                        padronIeService.fromModularCode($scope.formModel.codMod5.value, $scope.formModel.codigoUgel.value).then(function(res){
                            $scope.formModel.nombreIe5.value = res.data.items.cenEdu;
                            $scope.formModel.anexo5.value = res.data.items.anexo;
                        }).catch(function(err){
                            alert('El código modular no corresponde a su UGEL');
                        });
                        break;
                    default:
                        break;
                }
            }
        };
        $scope.$watch('formModel.codigoUgel.value', function(newVal){
            if(newVal){
                $scope.formModel.codigoUgel.selected = true;
            }else{
                $scope.formModel.codigoUgel.selected = false;
            }
        });
        $scope.$watch('formModel.nombreIe1.value', function(newVal){
            if(newVal){
                $scope.formModel.nombreIe1.selected = true;
            }else{
                $scope.formModel.nombreIe1.selected = false;
            }
        });
        $scope.$watch('formModel.nombreIe2.value', function(newVal){
            if(newVal){
                $scope.formModel.nombreIe2.selected = true;
            }else{
                $scope.formModel.nombreIe2.selected = false;
            }
        });
        $scope.$watch('formModel.nombreIe3.value', function(newVal){
            if(newVal){
                $scope.formModel.nombreIe3.selected = true;
            }else{
                $scope.formModel.nombreIe3.selected = false;
            }
        });
        $scope.$watch('formModel.nombreIe4.value', function(newVal){
            if(newVal){
                $scope.formModel.nombreIe4.selected = true;
            }else{
                $scope.formModel.nombreIe4.selected = false;
            }
        });
        $scope.$watch('formModel.nombreIe5.value', function(newVal){
            if(newVal){
                $scope.formModel.nombreIe5.selected = true;
            }else{
                $scope.formModel.nombreIe5.selected = false;
            }
        });
        
        $scope.actualizar = {
            buildPadronActualizarEnvio: function(){
                var statePadronActualizarEnvioGroup = {
                    listaCodigoModular: []
                };
                if($scope.formModel.nombreIe1.selected){
                    statePadronActualizarEnvioGroup.listaCodigoModular.push(
                        $scope.formModel.codMod1.value
                    );
                }
                if($scope.formModel.nombreIe2.selected){
                    statePadronActualizarEnvioGroup.listaCodigoModular.push(
                        $scope.formModel.codMod2.value
                    );
                }
                if($scope.formModel.nombreIe3.selected){
                    statePadronActualizarEnvioGroup.listaCodigoModular.push(
                        $scope.formModel.codMod3.value
                    );
                }
                if($scope.formModel.nombreIe4.selected){
                    statePadronActualizarEnvioGroup.listaCodigoModular.push(
                        $scope.formModel.codMod4.value
                    );
                }
                if($scope.formModel.nombreIe5.selected){
                    statePadronActualizarEnvioGroup.listaCodigoModular.push(
                        $scope.formModel.codMod5.value
                    );
                }
                return statePadronActualizarEnvioGroup;
            },
            actualizarPadronEnvio: function(){
                var selfie = this;
                var postData = selfie.buildPadronActualizarEnvio();
                if(postData.listaCodigoModular.length > 0){
                    actionPadronService.actualizarPadronEnvio(postData).then(function(){
                        $scope.formModel.limpiar();
                        alert('Se actualizo con éxito');
                    }).catch(function(err){
                        alert('Ocurrio un problema');
                    });
                }else if(postData.listaCodigoModular.length === 0){
                    alert('Por favor ingrese y/o busque correctamente el código modular.');
                }
                
            }
        }
        
        //messagesService.loadMessages($scope);
    });