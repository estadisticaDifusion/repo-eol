<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<html:xhtml />
<h1><bean:message key="ugel.login" /></h1>

<link href="<c:url value='/recursos/css/epicaptcha.css'/>" rel="stylesheet"/>

<div class="marco_ugel">

    <c:import url="/pages/inicioUGEL.jsp" />

    <p>Ingrese su C&oacute;digo de Estad&iacute;stico y su <bean:message key="ce.contrasenia" />:</p>

<script type="text/javascript" src="<c:url value='/recursos/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/jquery/aes.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/js/accesougel.js'/>"></script>
<script type="text/javascript" src="<c:url value='/recursos/js/epicaptcha.js'/>"></script>

<html:form action="/login/ugel/login">
        <table id="tbluser">
            <tr>
                <td><label for="usr"><bean:message key="ugel.usuario"/></label></td>
                <td><html:text styleId="usr" property="usuario" /></td>
            </tr>
            <tr>
                <td><label for="pass"><bean:message key="ce.contrasenia" /></label></td>
                <td><html:password styleId="pass" property="contrasenia" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <!--<div id="ReCaptchContainer" class="g-recaptcha" data-sitekey="6LeBbGMUAAAAADIULF6PKF-SFlA2jovbpcpMsE-u" data-callback="reCaptchaCallback"></div>
                    <label id="lblMessage" ></label>-->

                    <div id="epicaptcha"></div>
                    <!--<input class="btn btn-info" id="epi" type="button" value="Send"/>-->
                    <div id="contactMessage"></div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <%--<html:submit styleClass="button">Entrar
                    </html:submit>--%>
                    <input class="btn btn-info" id="epi" type="button" value="Entrar"/>
                </td>
            </tr>

            <%--   <tr valign="top">
      <td>
        <label for="desafio">
          <bean:message key="captcha.desafio"/>
        </label>
      </td>
      <td>
        <html:text property="desafio" maxlength="desafio"/>
        <img src="<html:rewrite page="/login/captcha"/>" alt="Por seguridad, escriba el texto que vea en esta imagen"/>
      </td>
            </tr> --%>
        </table>

            <br/>

</html:form>

            <%--
<script type="text/javascript" src="<c:url value='/recursos/js/captchacallce.js'/>"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
            --%>

</div>
