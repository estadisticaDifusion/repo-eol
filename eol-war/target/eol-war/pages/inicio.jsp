<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<%@ page import="java.security.SecureRandom"%>
<%@ page import="java.security.NoSuchAlgorithmException"%>
<%@ page import="java.net.MalformedURLException"%>
<%@ page import="java.net.URL"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="pe.gob.minedu.escale.eol.portlets.util.Constantes"%>

<%!SecureRandom prng = null;
	String randomNumext = "";%>

<%
String dominio = "";
try {

	URL aURL = new URL(request.getRequestURL().toString());

	dominio = aURL.getProtocol() + "://" + aURL.getAuthority();

	//SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
	prng = SecureRandom.getInstance("SHA1PRNG");
	randomNumext = Integer.toHexString(new Integer(prng.nextInt()))
			+ Integer.toHexString(new Integer(prng.nextInt())).substring(0, 2);

} catch (NoSuchAlgorithmException e) {
	e.printStackTrace();
}

String csp = "script-src  'strict-dynamic' 'nonce-" + randomNumext + "' https: http: ; " +
//"script-src  'strict-dynamic' 'nonce-"+ randomNumext +"' 'unsafe-inline' https: http:  'unsafe-eval' " + dominio + " ; " +
		"media-src 'self' blob:; " +
		//"object-src 'none'  blob:; " +
		"object-src 'self'  blob:; " + "base-uri 'none' ;" + "frame-src https://www.google.com ; "
		+ "child-src 'self'; " + "report-uri " + dominio + " ;" +
		//"unsafe-eval "  +  dominio + " ;"  +
		"img-src  https://www.google-analytics.com  " + dominio + "  ; "
		+ "style-src 'unsafe-inline' https://www.bing.com " + dominio + "  ;" + "font-src " + dominio
		+ " data: blob: ;" +
		//"connect-src http://10.36.136.152:8081/admws/rest/usuario/* http://10.36.136.152:8081/regprogramaws/* http://10.36.136.152:8081/regprogramaws/svt/inSoon http://10.36.136.152:8081/regprogramaws/svt/inMoon ;" + 
		"connect-src https://www.google-analytics.com " + dominio + " ; ";

String sessionid = request.getSession().getId();

response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store");
response.setHeader("Expires", "0");
response.setHeader("Pragma", "no-cache");
//response.addHeader("Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly ;SameSite=Strict ");
response.addHeader("Content-Security-Policy", csp);

%>

<h1>estad&iacute;stica.on-line</h1>
<p>El objetivo de este Sistema es permitir el registro,
    publicaci&oacute;n y consulta de estad&iacute;sticas educativas que
    hayan sido remitidas por las Instituciones Educativas.</p>

<div class="marco_ce"><c:import url="/pages/inicioCE.jsp" />

    <p>Haga clic en <html:link action="/login/ce/main">
            <bean:message key="ce.login" />
        </html:link> del men&uacute; de la izquierda.</p>
</div>
<div class="marco_ugel">
    <c:import url="/pages/inicioUGEL.jsp" />
    <p>Haga clic en <html:link action="/login/ugel/main">
            <bean:message key="ugel.login" />
        </html:link> del men&uacute; de la izquierda.</p>
</div>
<p>El <em>Piloto de Captura de Datos por Alumno</em> consiste en la generaci&oacute;n autom&aacute;tica de
    las secciones de <em>Matr&iacute;cula</em> y <em>Resultado del
        Ejercicio Anterior</em> de las C&eacute;dulas Censales utilizando las
    N&oacute;minas de Matr&iacute;cula y las Actas de Evaluaci&oacute;n
    respectivamente. Para ello, es necesario que remita
    electr&oacute;nicamente estos documentos.</p>
<p class="post-footer align-right"><%-- 					<a href="index.html" class="readmore">Read more</a>
<a href="index.html" class="comments">Comments (7)</a>--%> 
<!--<span class="date">01 febrero 2012</span></p>-->
