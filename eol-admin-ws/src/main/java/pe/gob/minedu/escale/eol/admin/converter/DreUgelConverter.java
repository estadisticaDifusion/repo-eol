/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.ws.rs.core.UriBuilder;
import javax.persistence.EntityManager;
import pe.gob.minedu.escale.eol.admin.domain.DreUgel;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "dre_ugel")
public class DreUgelConverter {

    private DreUgel entity;
    private URI uri;
    private int expandLevel;

    /**
     * Creates a new instance of UgelConverter
     */
    public DreUgelConverter() {
        entity = new DreUgel();
    }

    /**
     * Creates a new instance of UgelConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded@param isUriExtendable indicates whether the uri can be
     * extended
     */
    public DreUgelConverter(DreUgel entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getCodigo() + "/").build() : uri;
        this.expandLevel = expandLevel;
        //getDireccionRegional();
    }

    /**
     * Creates a new instance of UgelConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded
     */
    public DreUgelConverter(DreUgel entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    /**
     * Getter for idUgel.
     *
     * @return value for idUgel
     */
    @XmlElement
    public String getIdUgel() {
        return (expandLevel > 0) ? entity.getCodigo() : null;
    }

    /**
     * Getter for ugel.
     *
     * @return value for ugel
     */
    @XmlElement
    public String getNombre() {
        return (expandLevel > 0) ? entity.getNombre() : null;
    }

    /**
     * Setter for ugel.
     *
     * @param value the value to set
     */
    public void setNombre(String value) {
        entity.setNombre(value);
    }

    /**
     * Returns the URI associated with this converter.
     *
     * @return the uri
     */
    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the URI for this reference converter.
     *
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the Ugel entity.
     *
     * @return an entity
     */
    @XmlTransient
    public DreUgel getEntity() {
        if (entity.getCodigo() == null) {
            DreUgelConverter converter = UriResolver.getInstance().resolve(DreUgelConverter.class, uri);
            if (converter != null) {
                entity = converter.getEntity();
            }
        }
        return entity;
    }

    /**
     * Returns the resolved Ugel entity.
     *
     * @return an resolved entity
     */
    public DreUgel resolveEntity(EntityManager em) {
        return entity;
    }

    public void setZoom(int zoom) {
        entity.setZoom(zoom);
    }

    public void setPointY(double pointY) {
        entity.setPointY(pointY);
    }

    public void setPointX(double pointX) {
        entity.setPointX(pointX);
    }

    @XmlElement
    public int getZoom() {
        return entity.getZoom();
    }

    @XmlElement
    public double getPointY() {
        return entity.getPointY();
    }

    @XmlElement
    public double getPointX() {
        return entity.getPointX();
    }

}
