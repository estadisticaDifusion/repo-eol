/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import pe.gob.minedu.escale.rest.client.domain.Areas;
import pe.gob.minedu.escale.rest.client.domain.Codigo;
import pe.gob.minedu.escale.rest.client.domain.Formas;
import pe.gob.minedu.escale.rest.client.domain.Gestiones;
import pe.gob.minedu.escale.rest.client.domain.NivelesModalidades;
import pe.gob.minedu.escale.rest.util.ParmaeClient;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "grupo")
public class IEPadronDistriConverter {

    private URI uri;
    private int expandLevel;
    private String campo;
    private Long cantidad;
    private String tipo;

    public IEPadronDistriConverter() {
    }

    public IEPadronDistriConverter(Object[] entities, String tipo, URI uri, int expandLevel) {
        this.campo = entities[0].toString();
        this.cantidad = (Long) entities[1];
        this.uri = uri;
        this.expandLevel = expandLevel;
        this.tipo = tipo;
    }

    @XmlElement(name = "campo")
    public String getCampo() {
        if (!campo.isEmpty()) {
            Codigo cod = null;
            if (tipo.equals("nivMod")) {
                cod = ParmaeClient.getCodigoMap(NivelesModalidades.class.getName(), "nivelesModalidades", campo);
            } else if (tipo.equals("gestion")) {
                cod = ParmaeClient.getCodigoMap(Gestiones.class.getName(), "gestiones", campo);
            } else if (tipo.equals("areaSig")) {
                cod = ParmaeClient.getCodigoMap(Areas.class.getName(), "areas", campo);
            } else if (tipo.equals("formas")) {
                cod = ParmaeClient.getCodigoMap(Formas.class.getName(), "formas", campo);
            }
            if (cod != null) {
                return cod.getValor();
            }
        }
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    @XmlElement(name = "cantidad")
    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }
}
