/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.eol.enums.admin.EstadoActividadEnum;

@XmlRootElement(name = "actividad")
public class EolActividadConvert {

    private Integer id;
    private String nombre;
    private String descripcion;
    private Date fechaInicio;
    private Date fechaTermino;
    private Date fechaLimite;
    private Date fechaTerminosigied;
    private Date fechaLimitesigied;
    private Date fechaTerminohuelga;
    private Date fechaLimitehuelga;

    private Integer estadoActividad; //imendoza verificar con EolActividad.estado
    private boolean estadoFormato;

    private boolean estadoConstancia;
    private boolean estadoCobertura;
    private boolean estadoSituacion;
    private boolean estadoOmisos;
    private boolean estadoSie;

    private String urlFormato;
    private String urlOmisos;
    private String urlCobertura;
    private String urlConstancia;
    private String urlSituacion;

    private String strFechaEnvio;
    private String strFechaInicio;
    private String strFechaTermino;
    private String strFechaLimite;
    private String strFechaTerminosigied;
    private String strFechaLimitesigied;
    private String strFechaTerminohuelga;
    private String strFechaLimitehuelga;

    private String nombrePeriodo;

    public EolActividadConvert() {
    }

    public EolActividadConvert(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public Date getFechaLimite() {
        return fechaLimite;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public boolean getEstadoFormato() {
        return isEstadoFormato();
    }

    public Integer getEstadoActividad() {
        Date ahora = new Date();
        if (ahora.before(getFechaInicio())) {
            this.estadoActividad = EstadoActividadEnum.SIN_INICIAR.getCod();
        }
        if (ahora.after(getFechaLimite())) {
            this.estadoActividad = EstadoActividadEnum.FINALIZADO.getCod();
        }
        if (ahora.after(getFechaTermino()) && (ahora.before(getFechaLimite()) || ahora.equals(getFechaLimite()))) {
            this.estadoActividad = EstadoActividadEnum.EXTENPORANEO.getCod();
        }
        if (ahora.after(getFechaInicio()) && ahora.before(getFechaTermino())) {
            this.estadoActividad = EstadoActividadEnum.INICIADO.getCod();
        }
        return this.estadoActividad;
    }

    public String getUrlFormato() {
        return urlFormato;
    }

    public String getUrlConstancia() {
        return urlConstancia;
    }

    public String getUrlCobertura() {
        return urlCobertura;
    }

    public String getUrlSituacion() {
        return urlSituacion;
    }

    public String getUrlOmisos() {
        return urlOmisos;
    }

    public void setEstadoActividad(Integer estadoActividad) {
        this.estadoActividad = estadoActividad;
    }

    public boolean isEstadoFormato() {
        return estadoFormato;
    }

    public void setEstadoFormato(boolean estadoFormato) {
        this.estadoFormato = estadoFormato;
    }

    public void setUrlFormato(String urlFormato) {
        this.urlFormato = urlFormato;
    }

    public void setUrlOmisos(String urlOmisos) {
        this.urlOmisos = urlOmisos;
    }

    public void setUrlCobertura(String urlCobertura) {
        this.urlCobertura = urlCobertura;
    }

    public void setUrlConstancia(String urlConstancia) {
        this.urlConstancia = urlConstancia;
    }

    public void setUrlSituacion(String urlSituacion) {
        this.urlSituacion = urlSituacion;
    }

    public boolean isEstadoConstancia() {
        return estadoConstancia;
    }

    public void setEstadoConstancia(boolean estadoConstancia) {
        this.estadoConstancia = estadoConstancia;
    }

    public boolean isEstadoCobertura() {
        return estadoCobertura;
    }

    public void setEstadoCobertura(boolean estadoCobertura) {
        this.estadoCobertura = estadoCobertura;
    }

    public boolean isEstadoSituacion() {
        return estadoSituacion;
    }

    public void setEstadoSituacion(boolean estadoSituacion) {
        this.estadoSituacion = estadoSituacion;
    }

    public boolean isEstadoOmisos() {
        return estadoOmisos;
    }

    public void setEstadoOmisos(boolean estadoOmisos) {
        this.estadoOmisos = estadoOmisos;
    }

    /**
     * @return the strFechaEnvio
     */
    public String getStrFechaEnvio() {
        return strFechaEnvio;
    }

    /**
     * @param strFechaEnvio the strFechaEnvio to set
     */
    public void setStrFechaEnvio(String strFechaEnvio) {
        this.strFechaEnvio = strFechaEnvio;
    }

    /**
     * @return the strFechaInicio
     */
    public String getStrFechaInicio() {
        return strFechaInicio;
    }

    /**
     * @param strFechaInicio the strFechaInicio to set
     */
    public void setStrFechaInicio(String strFechaInicio) {
        this.strFechaInicio = strFechaInicio;
    }

    /**
     * @return the strFechaTermino
     */
    public String getStrFechaTermino() {
        return strFechaTermino;
    }

    /**
     * @param strFechaTermino the strFechaTermino to set
     */
    public void setStrFechaTermino(String strFechaTermino) {
        this.strFechaTermino = strFechaTermino;
    }

    public Date getFechaTerminosigied() {
        return fechaTerminosigied;
    }

    public void setFechaTerminosigied(Date fechaTerminosigied) {
        this.fechaTerminosigied = fechaTerminosigied;
    }

    public String getStrFechaTerminosigied() {
        return strFechaTerminosigied;
    }

    public void setStrFechaTerminosigied(String strFechaTerminosigied) {
        this.strFechaTerminosigied = strFechaTerminosigied;
    }

    public Date getFechaTerminohuelga() {
        return fechaTerminohuelga;
    }

    public void setFechaTerminohuelga(Date fechaTerminohuelga) {
        this.fechaTerminohuelga = fechaTerminohuelga;
    }

    public String getStrFechaTerminohuelga() {
        return strFechaTerminohuelga;
    }

    public void setStrFechaTerminohuelga(String strFechaTerminohuelga) {
        this.strFechaTerminohuelga = strFechaTerminohuelga;
    }

    /**
     * @return the strFechaLimite
     */
    public String getStrFechaLimite() {
        return strFechaLimite;
    }

    public String getStrFechaLimitehuelga() {
        return strFechaLimitehuelga;
    }

    public void setStrFechaLimitehuelga(String strFechaLimitehuelga) {
        this.strFechaLimitehuelga = strFechaLimitehuelga;
    }

    public String getStrFechaLimitesigied() {
        return strFechaLimitesigied;
    }

    public void setStrFechaLimitesigied(String strFechaLimitesigied) {
        this.strFechaLimitesigied = strFechaLimitesigied;
    }

    public Date getFechaLimitehuelga() {
        return fechaLimitehuelga;
    }

    public void setFechaLimitehuelga(Date fechaLimitehuelga) {
        this.fechaLimitehuelga = fechaLimitehuelga;
    }

    public Date getFechaLimitesigied() {
        return fechaLimitesigied;
    }

    public void setFechaLimitesigied(Date fechaLimitesigied) {
        this.fechaLimitesigied = fechaLimitesigied;
    }

    /**
     * @param strFechaLimite the strFechaLimite to set
     */
    public void setStrFechaLimite(String strFechaLimite) {
        this.strFechaLimite = strFechaLimite;
    }

    public String getNombrePeriodo() {
        return nombrePeriodo;
    }

    public void setNombrePeriodo(String nombrePeriodo) {
        this.nombrePeriodo = nombrePeriodo;
    }

    public boolean isEstadoSie() {
        return estadoSie;
    }

    public void setEstadoSie(boolean estadoSie) {
        this.estadoSie = estadoSie;
    }
}
