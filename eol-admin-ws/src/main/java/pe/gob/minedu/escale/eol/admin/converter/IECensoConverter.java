/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import pe.gob.minedu.escale.eol.admin.domain.EolCenso;

import pe.gob.minedu.escale.rest.client.domain.Codigo;
import pe.gob.minedu.escale.rest.client.domain.NivelesModalidades;
import pe.gob.minedu.escale.rest.util.ParmaeClient;

/**
 *
 * @author jmatamoros
 */
@XmlRootElement(name = "institucion")
public class IECensoConverter {

    private URI uri;
    private int expandLevel;
    private EolCenso entity;

    public IECensoConverter() {
    }

    public IECensoConverter(EolCenso entity, URI uri, int expandLevel) {
        this.entity = entity;
        this.uri = uri;
        this.expandLevel = expandLevel;
    }

    @XmlElement(name = "idActividad")
    public int getIdActividad() {
        return entity.getEolActividad().getId();
    }

    /*@XmlElement(name = "actividad")
    public ActividadConverter getActividad() {
    if (entity.getEolActividad() != null) {
    return new ActividadConverter(entity.getEolActividad(), uri, expandLevel);
    } else {
    return null;
    }
    }*/
    @XmlElement(name = "nroEnvio")
    public int getNroEnvio() {
        return entity.getNroEnvio();
    }

    @XmlElement(name = "fechaEnvio")
    public Date getFechaEnvio() {
        return entity.getFechaEnvio();
    }

    @XmlElement(name = "codooii")
    public String getCodugel() {
        return entity.getCodooii();
    }

    @XmlElement(name = "situacion")
    public String getSituacion() {
        return entity.getEstado();
    }

    @XmlElement(name = "codMod")
    public String getCodMod() {
        if (entity != null && entity.getEolPadron() != null) {
            return entity.getEolPadron().getCodMod();
        } else {
            return null;
        }
    }

    @XmlElement(name = "anexo")
    public String getAnexo() {
        if (entity != null && entity.getEolPadron() != null) {
            return entity.getEolPadron().getAnexo();
        } else {
            return null;
        }
    }

    @XmlElement(name = "nivMod")
    public String getNivMod() {
        return entity.getNivMod();
    }

    @XmlElement(name = "codlocal")
    public String getCodLocal() {
        if (entity != null && entity.getEolPadron() != null) {
            return entity.getEolPadron().getCodlocal();
        } else {
            return null;
        }
    }

    @XmlElement(name = "cenEdu")
    public String getcenEdu() {
        if (entity.getEolPadron() != null) {
            return entity.getEolPadron().getCenEdu();
        } else {
            return null;
        }
    }

    @XmlElement(name = "nivelModalidad")
    public String getNivelModalidad() {
        if (entity.getNivMod() != null && !entity.getNivMod().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(NivelesModalidades.class.getName(), "nivelesModalidades", entity.getNivMod());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;
    }

    @XmlElement
    public DistritoConverter getDistrito() {
        //if (expandLevel > 0) {
        if (entity.getDistrito() != null) {
            return new DistritoConverter(entity.getDistrito(), uri.resolve("distrito/"), expandLevel - 1, false);
        }
        //}
        return null;
    }
}
