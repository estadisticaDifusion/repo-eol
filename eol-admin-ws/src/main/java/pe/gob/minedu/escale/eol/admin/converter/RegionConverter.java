/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.ws.rs.core.UriBuilder;
import javax.persistence.EntityManager;
import pe.gob.minedu.escale.eol.admin.domain.Region;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "region")
public class RegionConverter {

    private Region entity;
    private URI uri;
    private int expandLevel;

    /**
     * Creates a new instance of RegionConverter
     */
    public RegionConverter() {
        entity = new Region();
    }

    /**
     * Creates a new instance of RegionConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded@param isUriExtendable indicates whether the uri can be
     * extended
     */
    public RegionConverter(Region entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getIdRegion() + "/").build() : uri;
        this.expandLevel = expandLevel;
    }

    /**
     * Creates a new instance of RegionConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded
     */
    public RegionConverter(Region entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    /**
     * Getter for idRegion.
     *
     * @return value for idRegion
     */
    @XmlElement
    public String getIdRegion() {
        return (expandLevel > 0) ? entity.getIdRegion() : null;
    }

    /**
     * Setter for idRegion.
     *
     * @param value the value to set
     */
    public void setIdRegion(String value) {
        entity.setIdRegion(value);
    }

    /**
     * Getter for nombreRegion.
     *
     * @return value for nombreRegion
     */
    @XmlElement
    public String getNombreRegion() {
        return (expandLevel > 0) ? entity.getNombreRegion() : null;
    }

    /**
     * Setter for nombreRegion.
     *
     * @param value the value to set
     */
    public void setNombreRegion(String value) {
        entity.setNombreRegion(value);
    }

    /**
     * Returns the URI associated with this converter.
     *
     * @return the uri
     */
    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the URI for this reference converter.
     *
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the Region entity.
     *
     * @return an entity
     */
    @XmlTransient
    public Region getEntity() {
        if (entity.getIdRegion() == null) {
            RegionConverter converter = UriResolver.getInstance().resolve(RegionConverter.class, uri);
            if (converter != null) {
                entity = converter.getEntity();
            }
        }
        return entity;
    }

    /**
     * Returns the resolved Region entity.
     *
     * @return an resolved entity
     */
    public Region resolveEntity(EntityManager em) {
        return entity;
    }

    public void setZoom(int zoom) {
        entity.setZoom(zoom);
    }

    public void setPointY(double pointY) {
        entity.setPointY(pointY);
    }

    public void setPointX(double pointX) {
        entity.setPointX(pointX);
    }

    @XmlElement
    public int getZoom() {
        return entity.getZoom();
    }

    @XmlElement
    public double getPointY() {
        return entity.getPointY();
    }

    @XmlElement
    public double getPointX() {
        return entity.getPointX();
    }
}
