package pe.gob.minedu.escale.eol.services;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.admin.converter.ActividadesConverter;
import pe.gob.minedu.escale.eol.admin.converter.EolActividadConvert;
import pe.gob.minedu.escale.eol.admin.domain.EolActividad;
import pe.gob.minedu.escale.eol.admin.ejb.ActividadLocal;

/**
 * Provides a resource Institucion.
 *
 */
@Path("/resources")
public class ActividadService {

    private Logger logger = Logger.getLogger(ActividadService.class);

    @EJB
    private ActividadLocal actividadFacade = lookup(ActividadLocal.class);

    @Context
    private UriInfo uriInfo;

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path(value = "/get/actividades")
    public ActividadesConverter get(
            @QueryParam("nombre") String nombre,
            @QueryParam("periodo") String periodo,
            @QueryParam("expandLevel") @DefaultValue("0") Integer expandLevel
    ) {
        logger.info(":: ActividadService.get :: Starting execution...");

        Collection<EolActividad> actividades = actividadFacade.findActividades(nombre, periodo);
        logger.info(" actividades = " + actividades);

        logger.info(":: ActividadService.get :: Execution finish.");
        return new ActividadesConverter(actividades, uriInfo.getAbsolutePath(), expandLevel);

    }

    @SuppressWarnings({ "unchecked" })
 	private <T> T lookup(Class<T> clase) {
    	//
 		String nameContextEjb = "java:global/eol-admin-ws/ActividadFacade!pe.gob.minedu.escale.eol.admin.ejb.ActividadLocal";
     	logger.info("search EJB for consume: "+nameContextEjb);
 		try {

 			javax.naming.Context c = new InitialContext();
 			return (T) c.lookup(nameContextEjb);
 		} catch (NamingException ne) {
 				logger.error(ne);
 			throw new RuntimeException(ne);
 		}
 	}
    
    @GET
    @Path("/buscarActividad")
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    public List<EolActividadConvert> findAll() {

            List<EolActividad> lista = actividadFacade.findAllActividades("2019");
            List<EolActividadConvert> lista2 = new ArrayList<EolActividadConvert>();
            for (EolActividad eolAct : lista) {
                 EolActividadConvert eolConv = new EolActividadConvert();
                 eolConv.setId(eolAct.getId());
                 eolConv.setNombre(eolAct.getNombre());
                 eolConv.setDescripcion(eolAct.getDescripcion());
                 eolConv.setFechaInicio(eolAct.getFechaInicio());
                 eolConv.setFechaLimite(eolAct.getFechaLimite());
                 eolConv.setFechaTermino(eolAct.getFechaTermino());
                 eolConv.setFechaTerminosigied(eolAct.getFechaTerminosigied());
                 eolConv.setFechaTerminohuelga(eolAct.getFechaTerminohuelga());
                 //System.out.println("TERMINO : "+DATE_FORMAT.format(eolConv.getFechaTermino()));
                 lista2.add(eolConv);
            }

            return lista2 ;
    }
    
    
    @GET
    @Path("/buscarActividad/{id}")
    @Produces({"application/xml", "application/json"})
    public EolActividadConvert findById(@PathParam("id") String id) {

         EolActividad eolAct = actividadFacade.findById(Integer.parseInt(id));
         EolActividadConvert eolConv = new EolActividadConvert();
         eolConv.setId(eolAct.getId());
         eolConv.setNombre(eolAct.getNombre());
         eolConv.setDescripcion(eolAct.getDescripcion());
         eolConv.setFechaInicio(eolAct.getFechaInicio());
         eolConv.setFechaTermino(eolAct.getFechaTermino());
         eolConv.setFechaLimite(eolAct.getFechaLimite());
         eolConv.setFechaTerminosigied(eolAct.getFechaTerminosigied());
         eolConv.setFechaLimitesigied(eolAct.getFechaLimitesigied());
         eolConv.setFechaTerminohuelga(eolAct.getFechaTerminohuelga());
         eolConv.setFechaLimitehuelga(eolAct.getFechaLimitehuelga());
         //System.out.println("TERMINO : "+DATE_FORMAT.format(eolConv.getFechaTermino()));

         return eolConv;
    }

    @PUT
    @Path("/buscarActividad/{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public EolActividadConvert update(EolActividadConvert eolConv, @PathParam("id") int id) {
        EolActividad eolAct = new EolActividad();

        eolAct.setId(id);
        eolAct.setNombre(eolConv.getNombre());
        eolAct.setDescripcion(eolConv.getDescripcion());
        eolAct.setFechaInicio(eolConv.getFechaInicio());
        eolAct.setFechaTermino(eolConv.getFechaTermino());
        eolAct.setFechaLimite(eolConv.getFechaLimite());
        
        eolAct.setFechaTerminosigied(eolConv.getFechaTerminosigied());
        eolAct.setFechaLimitesigied(eolConv.getFechaLimitesigied());
        eolAct.setFechaTerminohuelga(eolConv.getFechaTerminohuelga());
        eolAct.setFechaLimitehuelga(eolConv.getFechaLimitehuelga());

        actividadFacade.update(eolAct);

        return eolConv;
    }
    
}