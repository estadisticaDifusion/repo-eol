/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import pe.gob.minedu.escale.eol.admin.domain.EolCenso;

/**
 *
 * @author jmatamoros
 */
@XmlRootElement(name = "censo")
public class IEsCensoConverter {

    private URI uri;
    private int expandLevel;
    private Collection<EolCenso> entities;
    private Collection<IECensoConverter> items = new ArrayList<IECensoConverter>();

    public IEsCensoConverter() {
    }

    public IEsCensoConverter(Collection<EolCenso> entities, URI uri, int expandLevel) {
        this.entities = entities;
        this.uri = uri;
        this.expandLevel = expandLevel;
    }

    @XmlElement(name = "institucion")
    public Collection<IECensoConverter> getEnvios() {
        if (entities != null && !entities.isEmpty()) {
            for (EolCenso censo : entities) {
                items.add(new IECensoConverter(censo, uri, expandLevel));
            }
        }
        return items;
    }

}
