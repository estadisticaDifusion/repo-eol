/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.ws.rs.core.UriBuilder;
import javax.persistence.EntityManager;
import pe.gob.minedu.escale.eol.admin.domain.Distrito;
import pe.gob.minedu.escale.eol.admin.domain.Provincia;

/**
 *
 * @author DSILVA
 */
@XmlRootElement(name = "distrito")
public class DistritoConverter {

    private Distrito entity;
    private URI uri;
    private int expandLevel;

    /**
     * Creates a new instance of DistritoConverter
     */
    public DistritoConverter() {
        entity = new Distrito();
    }

    /**
     * Creates a new instance of DistritoConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded@param isUriExtendable indicates whether the uri can be
     * extended
     */
    public DistritoConverter(Distrito entity, URI uri, int expandLevel, boolean isUriExtendable) {
        this.entity = entity;
        this.uri = (isUriExtendable) ? UriBuilder.fromUri(uri).path(entity.getIdDistrito() + "/").build() : uri;
        this.expandLevel = expandLevel;
        getProvincia();
    }

    /**
     * Creates a new instance of DistritoConverter.
     *
     * @param entity associated entity
     * @param uri associated uri
     * @param expandLevel indicates the number of levels the entity graph should
     * be expanded
     */
    public DistritoConverter(Distrito entity, URI uri, int expandLevel) {
        this(entity, uri, expandLevel, false);
    }

    /**
     * Getter for idDistrito.
     *
     * @return value for idDistrito
     */
    @XmlElement
    public String getIdDistrito() {
        return (expandLevel > 0) ? entity.getIdDistrito() : null;
    }

    /**
     * Setter for idDistrito.
     *
     * @param value the value to set
     */
    public void setIdDistrito(String value) {
        entity.setIdDistrito(value);
    }

    /**
     * Getter for nombreDistrito.
     *
     * @return value for nombreDistrito
     */
    @XmlElement
    public String getNombreDistrito() {
        return (expandLevel > 0) ? entity.getNombreDistrito() : null;
    }

    /**
     * Setter for nombreDistrito.
     *
     * @param value the value to set
     */
    public void setNombreDistrito(String value) {
        entity.setNombreDistrito(value);
    }

    /**
     * Getter for provincia.
     *
     * @return value for provincia
     */
    @XmlElement
    public ProvinciaConverter getProvincia() {
        if (expandLevel > 0) {
            if (entity.getProvincia() != null) {
                return new ProvinciaConverter(entity.getProvincia(), uri.resolve("provincia/"), expandLevel - 1, false);
            }
        }
        return null;
    }

    /**
     * Setter for provincia.
     *
     * @param value the value to set
     */
    public void setProvincia(ProvinciaConverter value) {
        entity.setProvincia((value != null) ? value.getEntity() : null);
    }

    /**
     * Returns the URI associated with this converter.
     *
     * @return the uri
     */
    @XmlAttribute
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the URI for this reference converter.
     *
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the Distrito entity.
     *
     * @return an entity
     */
    @XmlTransient
    public Distrito getEntity() {
        if (entity.getIdDistrito() == null) {
            DistritoConverter converter = UriResolver.getInstance().resolve(DistritoConverter.class, uri);
            if (converter != null) {
                entity = converter.getEntity();
            }
        }
        return entity;
    }

    /**
     * Returns the resolved Distrito entity.
     *
     * @return an resolved entity
     */
    public Distrito resolveEntity(EntityManager em) {
        Provincia provincia = entity.getProvincia();
        if (provincia != null) {
            entity.setProvincia(em.getReference(Provincia.class, provincia.getIdProvincia()));
        }
        return entity;
    }

    public void setZoom(int zoom) {
        entity.setZoom(zoom);
    }

    public void setPointY(double pointY) {
        entity.setPointY(pointY);
    }

    public void setPointX(double pointX) {
        entity.setPointX(pointX);
    }

    @XmlElement
    public int getZoom() {
        return entity.getZoom();
    }

    @XmlElement
    public double getPointY() {
        return entity.getPointY();
    }

    @XmlElement
    public double getPointX() {
        return entity.getPointX();
    }
}
