/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import pe.gob.minedu.escale.eol.admin.domain.EolPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;

/**
 *
 * @author jmatamoros
 */
@XmlRootElement(name = "censo")
public class IEsPadronConverter {

    private URI uri;
    private int expandLevel;
    private Collection<EolPadron> entities;
    private Collection<IEPadronConverter> items = new ArrayList<IEPadronConverter>();
    private int idActividad;
    private String tipoRep = CoreConstant.SITUACION_REPORTADO;

    public IEsPadronConverter() {
    }

    public IEsPadronConverter(Collection<EolPadron> entities, URI uri, int expandLevel) {
        this.uri = uri;
        this.expandLevel = expandLevel;
        this.entities = entities;
    }

    public IEsPadronConverter(Collection<EolPadron> entities, URI uri, int expandLevel, String idActividad, String tipoRep) {
        this.uri = uri;
        this.expandLevel = expandLevel;
        this.entities = entities;
        this.tipoRep = tipoRep;
        if (idActividad != null && !idActividad.isEmpty()) {
            this.idActividad = Integer.parseInt(idActividad);
        }
    }

    @XmlElement(name = "institucion")
    public Collection<IEPadronConverter> getInstituciones() {
        if (entities != null && !entities.isEmpty()) {
            for (EolPadron ie : entities) {
                IEPadronConverter ieConverter = new IEPadronConverter(ie, uri, expandLevel, idActividad);
                //if(ieConverter.getSituacion().equals(tipoRep))
                items.add(ieConverter);
            }
        }

        return items;
    }
}
