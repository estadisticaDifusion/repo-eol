package pe.gob.minedu.escale.eol.services;

import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.eol.admin.converter.CedulasConverter;
import pe.gob.minedu.escale.eol.admin.domain.EolCedula;
import pe.gob.minedu.escale.eol.admin.ejb.CedulaLocal;

/**
 * Provides a resource Cedula.
 *
 */
@Path("/resources")
public class CedulaService {

	private Logger logger = Logger.getLogger(CedulaService.class);

	@EJB
	private CedulaLocal cedulaFacade = lookup(CedulaLocal.class);

	@Context
	private UriInfo uriInfo;

	private int expandLevel;

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("/get/getCedulas")
	public CedulasConverter get(@QueryParam("idActividad") Integer id,
			@QueryParam("nombreActividad") String nombreActividad, @QueryParam("nivel") String nivel,
			@QueryParam("estado") String estado, @QueryParam("periodo") String periodo) {
		logger.info(":: CedulaService.get :: Starting execution...");

		List<EolCedula> cedulas = cedulaFacade.findCedulas(id, nombreActividad, nivel, estado, periodo);
		logger.info(" cedulas = " + cedulas);
		logger.info(":: CedulaService.get :: Execution finish.");
		return new CedulasConverter(cedulas, uriInfo.getAbsolutePath(), expandLevel);

	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		//
		String nameContextEjb = "java:global/eol-admin-ws/CedulaFacade!pe.gob.minedu.escale.eol.admin.ejb.CedulaLocal";
		logger.info("search EJB for consume: " + nameContextEjb);
		try {

			javax.naming.Context c = new InitialContext();
			return (T) c.lookup(nameContextEjb);
		} catch (NamingException ne) {
			logger.error(ne);
			throw new RuntimeException(ne);
		}
	}

}