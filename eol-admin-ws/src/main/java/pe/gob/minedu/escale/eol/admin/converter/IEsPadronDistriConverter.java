/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JMATAMOROS
 */
@XmlRootElement(name = "grupos")
public class IEsPadronDistriConverter {

    private URI uri;
    private int expandLevel;
    private List<Object[]> entities;
    private List<IEPadronDistriConverter> items = new ArrayList();
    private String descri;

    public IEsPadronDistriConverter() {
    }

    public IEsPadronDistriConverter(List<Object[]> entities, String tipo, URI uri, int expandLevel) {
        this.entities = entities;
        this.uri = uri;
        this.expandLevel = expandLevel;
        for (Object[] entity : entities) {
            items.add(new IEPadronDistriConverter(entity, tipo, uri, expandLevel));
        }
    }

    @XmlElement(name = "grupo")
    public List<IEPadronDistriConverter> getItems() {
        return items;
    }

    public void setItems(List<IEPadronDistriConverter> items) {
        this.setItems(items);
    }

    public void setEntities(List<Object[]> entities) {
        this.entities = entities;
    }

    @XmlElement(name = "descri")
    public String getDescri() {
        return descri;
    }

    public void setDescri(String descri) {
        this.descri = descri;
    }

}
