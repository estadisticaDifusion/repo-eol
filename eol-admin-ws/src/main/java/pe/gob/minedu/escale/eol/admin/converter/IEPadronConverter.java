/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import pe.gob.minedu.escale.eol.admin.domain.EolPadron;
import pe.gob.minedu.escale.eol.constant.CoreConstant;

import pe.gob.minedu.escale.rest.client.domain.Areas;
import pe.gob.minedu.escale.rest.client.domain.Codigo;
import pe.gob.minedu.escale.rest.client.domain.Formas;
import pe.gob.minedu.escale.rest.client.domain.Generos;
import pe.gob.minedu.escale.rest.client.domain.Gestiones;
import pe.gob.minedu.escale.rest.client.domain.GestionesDependencias;
import pe.gob.minedu.escale.rest.client.domain.NivelesModalidades;
import pe.gob.minedu.escale.rest.client.domain.Caracteristicas;
//import pe.gob.minedu.escale.rest.server.util.Constantes;
import pe.gob.minedu.escale.rest.util.ParmaeClient;

/**
 *
 * @author jmatamoros
 */
@XmlRootElement(name = "institucion")
public class IEPadronConverter {

    private int expandLevel;
    private URI uri;
    private EolPadron entity;
    private int idActividad;
    private String situacion = CoreConstant.SITUACION_PENDIENTE;
    private Date fechaEnvio;
    private int nroEnvio;

    public IEPadronConverter() {
    }

    public IEPadronConverter(EolPadron entity, URI uri, int expandLevel) {
        this.entity = entity;
        this.uri = uri;
        this.expandLevel = expandLevel;
    }

    public IEPadronConverter(EolPadron entity, URI uri, int expandLevel, int idActividad) {
        this.entity = entity;
        this.uri = uri;
        this.expandLevel = expandLevel;
        this.idActividad = idActividad;
        /*for (EolCenso s : entity.getEnvios()) {
            if (s.getEolActividad().getId() == idActividad) {
                setFechaEnvio(s.getFechaEnvio());
                setNroEnvio(s.getNroEnvio());
                setSituacion(s.getEstado());
                break;
            }
        }*/

    }

    @XmlElement(name = "codMod")
    public String getCodMod() {
        return entity.getCodMod();
    }

    @XmlElement(name = "anexo")
    public String getAnexo() {
        return entity.getAnexo();
    }

    @XmlElement(name = "codlocal")
    public String getCodlocal() {
        return entity.getCodlocal();
    }

    @XmlElement(name = "nivMod")
    public String getNivMod() {
        return entity.getNivMod();
    }

    @XmlElement(name = "cenEdu")
    public String getCenEdu() {
        return entity.getCenEdu();
    }

    /*@XmlElement(name = "codooii")
    public String getCodooii() {
        return entity.getCodooii();
    }*/
 /*    @XmlElement
    public NivelModalidad getNivel(){
    NivelModalidadClient client = new NivelModalidadClient("A2");
    NivelModalidad niv = client.getNivelModalidad();
    return niv;
    }
     */
    @XmlElement(name = "nivelModalidad")
    public String getNivelModalidad() {
        if (entity.getNivMod() != null && !entity.getNivMod().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(NivelesModalidades.class.getName(), "nivelesModalidades", entity.getNivMod());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;

    }

    @XmlElement(name = "forma")
    public String getForma() {
        if (entity.getFormas() != null && !entity.getFormas().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(Formas.class.getName(), "formas", entity.getFormas());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;
    }

    @XmlElement(name = "genero")
    public String getGenero() {
        if (entity.getTipssexo() != null && !entity.getTipssexo().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(Generos.class.getName(), "generos", entity.getTipssexo());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;
    }

    @XmlElement(name = "gestion")
    public String getGestion() {
        if (entity.getGestion() != null && !entity.getGestion().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(Gestiones.class.getName(), "gestiones", entity.getGestion());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;
    }

    @XmlElement(name = "gestionDependencia")
    public String getGestionDependencia() {
        if (entity.getGesDep() != null && !entity.getGesDep().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(GestionesDependencias.class.getName(), "gestionesDependencias", entity.getGesDep());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;
    }

    @XmlElement(name = "caracteristica")
    public String getCaracteristica() {
        if (entity.getCodCar() != null && !entity.getCodCar().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(Caracteristicas.class.getName(), "caracteristicas", entity.getCodCar());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;
    }

    @XmlElement(name = "estado")
    public String getEstado() {
        if (entity.getEstado() != null && entity.getEstado().equals(CoreConstant.PADRON_CONST_ESTADO_ACTIVO)) {
            return CoreConstant.PADRON_CONST_ESTADO_ACTIVO_DESC;
        } else {
            return CoreConstant.PADRON_CONST_ESTADO_INACTIVO_DESC;
        }
    }

    @XmlElement(name = "director")
    public String getDirector() {
        return entity.getDirector();
    }

    @XmlElement(name = "telefono")
    public String getTelefono() {
        return entity.getTelefono();
    }

    @XmlElement(name = "email")
    public String getEmail() {
        return entity.getEmail();
    }

    @XmlElement(name = "nroEnvio")
    public int getNroEnvio() {
        return nroEnvio;
    }

    @XmlElement(name = "pagweb")
    public String getPagweb() {
        return entity.getPagweb();
    }

    @XmlElement(name = "dirCen")
    public String getDirCen() {
        return entity.getDirCen();
    }

    @XmlElement(name = "localidad")
    public String getLocalidad() {
        return entity.getLocalidad();
    }

    @XmlElement(name = "cenPob")
    public String getCenPob() {
        return entity.getCenPob();
    }

    @XmlElement(name = "areaSig")
    public String getAreaSig() {
        if (entity.getAreaSig() != null && !entity.getAreaSig().isEmpty()) {
            Codigo cod = ParmaeClient.getCodigoMap(Areas.class.getName(), "areas", entity.getAreaSig());
            if (cod != null) {
                return cod.getValor();
            }
        }
        return null;
    }

    @XmlElement
    public DistritoConverter getDistrito() {
        if (expandLevel > 0) {
            if (entity.getDistrito() != null) {
                return new DistritoConverter(entity.getDistrito(), uri.resolve("distrito/"), expandLevel - 1, false);
            }
        }
        return null;
    }

    /*@XmlElement
    public UgelConverter getUgel() {
        if (expandLevel > 0) {
            if (entity.getUgel() != null) {
                return new UgelConverter(entity.getUgel(), uri.resolve("ugel/"), expandLevel - 1, false);
            }
        }
        return null;
    }*/
    @XmlElement(name = "dreUgel")
    public DreUgelConverter getDreUgel() {
        if (expandLevel > 0) {
            if (entity.getDreUgel() != null) {
                return new DreUgelConverter(entity.getDreUgel(), uri.resolve("dreUgel/"), expandLevel - 1, false);
            }
        }
        return null;
    }

    @XmlElement(name = "fechaEnvio")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    @XmlElement(name = "situacion")
    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public void setNroEnvio(int nroEnvio) {
        this.nroEnvio = nroEnvio;
    }

    /*public Comparator<EolCenso> buscarEnvio= new Comparator<EolCenso>(){
    
    public int compare(EolCenso o1, EolCenso o2){
    return o1.getEolActividad().getId().compareTo(o2.getEolActividad().getId());
    }
    
    };*/
    public void setEntity(EolPadron entity) {
        this.entity = entity;
    }

    @XmlTransient
    public EolPadron getEntity() {
        return entity;
    }
}
