/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.eol.admin.converter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import pe.gob.minedu.escale.eol.admin.domain.EolLocal;
import pe.gob.minedu.escale.eol.constant.CoreConstant;

/**
 *
 * @author jmatamoros
 */
@XmlRootElement(name = "censo")
public class LocalesConverter {

    private URI uri;
    private int expandLevel;
    private Collection<EolLocal> entities;
    private Collection<LocalConverter> items = new ArrayList<LocalConverter>();
    private int idActividad;
    private String tipoRep = CoreConstant.SITUACION_REPORTADO;

    public LocalesConverter() {
    }

    public LocalesConverter(Collection<EolLocal> entities, URI uri, int expandLevel) {
        this.uri = uri;
        this.expandLevel = expandLevel;
        this.entities = entities;
    }

    public LocalesConverter(Collection<EolLocal> entities, URI uri, int expandLevel, String idActividad, String tipoRep) {
        this.uri = uri;
        this.expandLevel = expandLevel;
        this.entities = entities;
        this.tipoRep = tipoRep;
        if (idActividad != null && !idActividad.isEmpty()) {
            this.idActividad = Integer.parseInt(idActividad);
        }
    }

    @XmlElement(name = "local")
    public Collection<LocalConverter> getInstituciones() {
        if (entities != null && !entities.isEmpty()) {
            for (EolLocal local : entities) {
                //LocalConverter localConverter=new LocalConverter(local, uri, expandLevel, idActividad);
                //if(localConverter.getSituacion().equals(tipoRep))
                items.add(new LocalConverter(local, uri, expandLevel, idActividad));
            }
        }

        return items;
    }
}
